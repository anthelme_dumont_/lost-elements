var http = require('http');
var fs = require('fs');

var serverHttp = http.createServer(function(req, res) {
	fs.readFile('./index.html', 'utf-8', function(error, content) {
		res.writeHead(200, {"Content-Type": "text/html"});
		res.end(content);
	});
});

var io = require('socket.io').listen(serverHttp, { log: false });
serverHttp.listen(8080);

var net = require('net'), clients = {};
var util = require('util');
var CLIENTS = [];

////
//Relic
////
var serverNet = net.createServer((sock) =>
{

	//set encoding
	sock.setEncoding('utf8');

	sock.on('data', function(data) {
		if(data == 'sun' || data == 'earth' || data == 'water' || data == 'wind' || data == '+' || data == '-') {
			console.log("data : ", data);
			io.sockets.emit('data', data);
		}
	});

	sock.on('error', (err) =>
	{
		// handle errors here
		throw err;

	});

	sock.on('connection', (cb) =>
	{
		clients[cb.fd] = cb; // Add the client, keyed by fd.

		console.log('Relic connected');

		cb.on('close', function() {
			console.log('Relic disconnected');
			delete clients[cb.fd]; // Remove the client.
		});
	});

});
////

////
//Mobile and Relic
////
io.on('connection', function (socketio)
{
	console.log('Mobile or Unity connected');

	socketio.on('data', function(data) {
		if(data == 'sun' || data == 'earth' || data == 'water' || data == 'wind' || data == '+' || data == '-') {
			console.log("data : ", data);
			io.sockets.emit('data', data);
		}
	});
////

	socketio.on('disconnect', function () {
		console.log("Mobile or Unity disconnected");
	});
});

// grab a random port.
serverNet.listen(9000, '0.0.0.0', () =>
{
	address = serverNet.address();
	console.log('opened serverNet on %j', address);
});
