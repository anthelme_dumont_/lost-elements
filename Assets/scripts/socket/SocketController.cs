﻿using UnityEngine;
using System.Collections;
using SocketIOClient;
using System;

public class SocketController : MonoBehaviour {

	public SocketIOClient.Client client;
	//private static string serverUrl = "http://127.0.0.1:8080";
	private static string serverUrl = "http://192.168.43.253:8080";

	void Start() {
		this.client = new Client(serverUrl);

		this.client.Opened += SocketOpened;
		this.client.Message += SocketMessage;

		this.client.Connect();
	}

	private void SocketOpened(object sender, EventArgs e) {
		Debug.Log("Unity connected");
	}

	private void SocketMessage(object sender, MessageEventArgs e) {

		if ( e!= null && e.Message.Event == "data") {
			PhotonDataClass photonData = new PhotonDataClass();
			photonData = JsonUtility.FromJson<PhotonDataClass> (e.Message.MessageText);

			string value = (string)photonData.args[0];

			if (value != null) {
				switch (value) {
				case "water":
					ElementsController.switchToWater();
					break;
				case "wind":
					ElementsController.switchToWind();
					break;
				case "sun":
					ElementsController.switchToSun();
					break;
				case "earth":
					ElementsController.switchToEarth();
					break;
				case "-":
					ElementsController.decrementIntensity();
					break;
				case "+":
					ElementsController.incrementIntensity();
					break;
				case "counterclockwise":
					ElementsController.decrementIntensity();
					break;
				case "clockwise":
					ElementsController.incrementIntensity();
					break;
				default :
					break;
				}
			}
		}
	}
	
	void OnApplicationQuit() {
		this.client.Close();
	}
}