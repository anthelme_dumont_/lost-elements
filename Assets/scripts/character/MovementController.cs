﻿using UnityEngine;
using System;
using System.Collections;

public class MovementController : MonoBehaviour {

	NavMeshAgent agent;
	Vector3 eulerAngleVelocity;
	Rigidbody rigidbody;
	CapsuleCollider collider;
	bool isMoving = false;
	Vector3 target;
	public float movementSpeed = 1F;
	RaycastHit hit;
	Ray ray;

	void Start () {
		agent = GetComponent<NavMeshAgent>();
		rigidbody = GetComponent<Rigidbody>();
		collider = GetComponent<CapsuleCollider> ();
		target = Vector3.zero;
	}

	void FixedUpdate () {
		if (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)) {

			#if UNITY_EDITOR
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			#elif (UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8)
			ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
			#endif
			if (Physics.Raycast(ray,out hit)) {
				if (agent.enabled) {
					agent.SetDestination(hit.point);
					target = hit.point;
					isMoving = true;
				}
				if (rigidbody != null) {
					transform.LookAt(hit.point);
					target = hit.point;
					isMoving = true;
				}
			}
		}

		if(isMoving && rigidbody != null && target != Vector3.zero) {
			float targetX = ExtensionMethods.RoundToNearestQuarter(target.x);
			float targetZ = ExtensionMethods.RoundToNearestQuarter(target.z);
			float objX = ExtensionMethods.RoundToNearestQuarter(transform.position.x);
			float objZ = ExtensionMethods.RoundToNearestQuarter(transform.position.z);

			if(objX != targetX || objZ != targetZ) {
				rigidbody.MovePosition(transform.position + transform.forward * movementSpeed * Time.deltaTime);
			} else {
				isMoving = false;
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Raft") {
			agent.enabled = false;
			transform.parent = other.transform;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Raft") {
			target = Vector3.zero;
			agent.enabled = true;
			transform.parent = null;
		}
	}

	Ray getRay() {
		#if UNITY_EDITOR
		ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		#elif (UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8)
		ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
		#endif

		return ray;
	}
}


