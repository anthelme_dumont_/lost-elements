﻿using UnityEngine;
using System.Collections;

public class FollowController : MonoBehaviour {

	public GameObject player;
	NavMeshAgent agent;

	void Start () {
		agent = gameObject.GetComponent<NavMeshAgent>();
		iTween.MoveAdd(gameObject, iTween.Hash("y", -1F, "easeType", "easeInOutSine", "loopType", "pingPong", "time", 1F));
	}

	void FixedUpdate () {
		Vector3 newPosition = player.transform.position;
		newPosition.x -= 1F;
		newPosition.z -= 1F;
		agent.SetDestination (newPosition);
	}
}
