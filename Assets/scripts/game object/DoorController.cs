﻿using UnityEngine;
using System.Collections;

public class DoorController : MonoBehaviour {

	GameObject[] doors;
	GameObject player;
	public LevelManagerController levelManagerController;
	public float minDistance = 2.5F;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		doors = GameObject.FindGameObjectsWithTag("Door");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		foreach (GameObject door in doors) {
			setEmissionRate (door);
		}
	}

	void nextLevel() {
		if (!levelManagerController.nextLevelInLoad) {
			StartCoroutine(levelManagerController.loadNextLevel());
		}
	}

	void setEmissionRate(GameObject door) {
		float doorDistance = getDistance(door);
		ParticleSystem.EmissionModule emission = door.GetComponentInChildren<ParticleSystem>().emission;
		ParticleSystem.MinMaxCurve rate = emission.rate;

		if (doorDistance < minDistance ) {
			rate.constantMax = 100;
		} else {
			rate.constantMax = 0;
		}
		emission.rate = rate;

		if (doorDistance < 1.5F) {
			nextLevel();
		}
	}

	float getDistance(GameObject door) {
		float distance = Vector3.Distance(player.transform.position, door.transform.position);
		return distance;
	}
}
