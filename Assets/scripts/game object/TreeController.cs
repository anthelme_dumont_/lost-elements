﻿using UnityEngine;
using System.Collections;

public class TreeController : MonoBehaviour {

	GameObject[] trees;
	GameObject[] leaves;
	GameObject[] seeds;
	bool isFell = false;

	// Use this for initialization
	void Awake () {
		trees = GameObject.FindGameObjectsWithTag("Tree");
		leaves = GameObject.FindGameObjectsWithTag("Leaf");
		seeds = GameObject.FindGameObjectsWithTag("Seed");
	}

	void FixedUpdate() {
		string activeElement = ElementsController.activeElement;
		if (activeElement == "wind") {
			float intensity = ElementsController.intensityOfElement;
			shake (intensity);
			if (seeds != null) {
				propagateSeeds (intensity);
			}
		} else {
			propagateSeeds (0F);
		}
	}
	
	public void grow() {
		foreach (GameObject tree in trees) {
			float delay = Random.Range (0F, 1F);
			iTween.ScaleTo(tree.gameObject, iTween.Hash("x", 1F, "y", 1F, "z", 1F, "easeType", "easeInOutElastic", "time", 1.5F, "delay", delay));
			tree.GetComponent<NavMeshObstacle>().enabled = true;
		}

	}

	public void propagateSeeds(float intensity) {
		foreach (GameObject seed in seeds) {
			ParticleSystem particle = seed.GetComponent<ParticleSystem>();
			ParticleSystem.EmissionModule emission = particle.emission;
			ParticleSystem.MinMaxCurve rate = emission.rate;
			rate.constantMax = intensity * 50F;
			emission.rate = rate;
		}
		if (intensity > 0.5F) {
			Chapter3Level1Board1ProgressController progressController =  GameObject.FindGameObjectWithTag("ProgressController").GetComponent<Chapter3Level1Board1ProgressController>();
			progressController.seedsPropagate = true;
		}
	}

	public void shake(float intensity) {
		foreach (GameObject leaf in leaves) {
			float intensityForLeaf = intensity * 10F;
			Vector3 amount = new Vector3 (intensityForLeaf, intensityForLeaf, intensityForLeaf);
			iTween.ShakeRotation(leaf.gameObject, iTween.Hash("amount", amount, "loopType", "none", "time", 0.016666F));
		}
		if (intensity > 0.7F && !isFell) {
			fallDownTrees ();
		}
	}

	void fallDownTrees() {
		isFell = true;
		foreach (GameObject tree in trees) {
			if (tree.transform.localScale.x > 0 && tree.transform.localScale.y > 0 && tree.transform.localScale.z > 0) {
				float delay = Random.Range (0F, 1F);
				float time = Random.Range (0.5F, 1F);
				iTween.RotateAdd(tree.gameObject, iTween.Hash("z", -90F, "time", time, "delay", delay));
			}
		}
	}
}
