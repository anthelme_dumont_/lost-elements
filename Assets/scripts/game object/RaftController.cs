﻿using UnityEngine;
using System.Collections;

public class RaftController : MonoBehaviour {

	GameObject[] rafts;
	public GameObject water;

	// Use this for initialization
	void Start () {
		rafts = GameObject.FindGameObjectsWithTag("Raft");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (water != null && rafts != null) {
			foreach (GameObject raft in rafts) {
				Vector3 newPosition = raft.transform.position;
				newPosition.y = water.transform.position.y + water.GetComponent<Renderer>().bounds.size.y;
				raft.transform.position = newPosition;
			}
		}

	}
}