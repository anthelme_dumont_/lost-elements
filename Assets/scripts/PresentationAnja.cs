﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PresentationAnja : MonoBehaviour {

	void Awake() {
		StartCoroutine(changeSceneWhenFinished ());
	}

	IEnumerator changeSceneWhenFinished() {
		yield return new WaitForEndOfFrame();
		SceneManager.LoadScene("main-menu");
	}
}
