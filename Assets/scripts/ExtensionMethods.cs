﻿using UnityEngine;
using System.Collections;

public static class ExtensionMethods
{
	public static float Map(this float value, float fromSource, float toSource, float fromTarget, float toTarget)
	{
		return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
	}
	public static float Round(float value, int digits)
	{
		float mult = Mathf.Pow(10.0f, (float)digits);
		return Mathf.Round(value * mult) / mult;
	}
	public static float RoundToNearestQuarter(float a)
	{
		return a = Mathf.Round(a * 2f) * 0.25f;
	}
}
