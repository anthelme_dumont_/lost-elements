﻿using UnityEngine;
using System.Collections;

public class EarthController : MonoBehaviour {

	GameObject player;
	ParticleSystem earthParticle;
	GameObject mainCamera;

	void Start() {
		ElementsController.earthController = gameObject;
		player = GameObject.FindGameObjectWithTag ("Player");
		mainCamera = GameObject.FindGameObjectWithTag ("MainCamera");
		earthParticle = player.GetComponentInChildren<ParticleSystem>();
	}

	void FixedUpdate() {
		if (ElementsController.activeElement == "earth") { 
			float intensity = ElementsController.intensityOfElement;
			shakeEarth (intensity);
		} else {
			shakeEarth (0F);
		}
	}

	void shakeEarth(float intensity) {
		ParticleSystem.EmissionModule emission = earthParticle.emission;
		ParticleSystem.ShapeModule shape = earthParticle.shape;
		ParticleSystem.MinMaxCurve rate = emission.rate;
		float radius = shape.radius;
		rate.constantMax = intensity * 100F;
		radius = intensity * 3F;
		emission.rate = rate;
		shape.radius = radius;

		if (intensity > 0) {
			Vector3 amount = new Vector3 (0, intensity / 15F, 0);
			iTween.ShakeRotation(mainCamera.gameObject, iTween.Hash("amount", amount, "loopType", "none", "time", 0.016666F));
		}
	}
}