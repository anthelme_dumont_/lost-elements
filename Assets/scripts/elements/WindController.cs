﻿using UnityEngine;
using System.Collections;

public class WindController : MonoBehaviour {
	GameObject[] clouds;

	void Start() {
		ElementsController.windController = gameObject;
		clouds = GameObject.FindGameObjectsWithTag("Cloud");
	}

	void FixedUpdate() {
		if (ElementsController.activeElement == "wind" && ElementsController.intensityOfElement > 0)	{ 
			float intensity = ElementsController.intensityOfElement;
			changeCloudPosition(intensity);
		}
	}

	void changeCloudPosition(float intensity) {
		if (clouds != null) {
			foreach (GameObject cloud in clouds) {
				Vector3 newPosition = cloud.transform.position;
				newPosition.z -= intensity * 0.08F;
				cloud.transform.position = newPosition;
			}
		}
	}
}