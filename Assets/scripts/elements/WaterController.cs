﻿using UnityEngine;
using System.Collections;

public class WaterController : MonoBehaviour {
	public float waterLevel = 0F;
	public float minScale = 1;
	public float maxScale = 7;
	public GameObject[] waters;
	GameObject[] clouds;

	void Start() {
		ElementsController.waterController = gameObject;
		waters = GameObject.FindGameObjectsWithTag("Water");
		clouds = GameObject.FindGameObjectsWithTag("Cloud");
	}

	void FixedUpdate() {
		if (ElementsController.activeElement == "water" && ElementsController.intensityOfElement > 0) { 
			float intensity = ElementsController.intensityOfElement;
			waterLevel += intensity / 200F;
			changeWaterLevel ();
			makeItRain (intensity);
		} else {
			makeItRain(0f);
		}
	}

	void changeWaterLevel() {
		foreach (GameObject water in waters) {
			Vector3 newScale = water.transform.localScale;
			if (newScale.y < maxScale) {
				newScale.y = minScale + waterLevel;
				water.transform.localScale = newScale;
			}
		}
	}

	void makeItRain(float intensity) {
		foreach (GameObject cloud in clouds) {
			ParticleSystem rain = cloud.GetComponentInChildren<ParticleSystem>();
			ParticleSystem.EmissionModule emission = rain.emission;
			ParticleSystem.MinMaxCurve rate = emission.rate;
			rate.constantMax = intensity * 200;
			emission.rate = rate;
		}
	}
}