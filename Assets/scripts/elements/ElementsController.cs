﻿using UnityEngine;

public class ElementsController {

	public static string activeElement = "water";
	public static float intensityOfElement = 0F;
	static float coefficientIntensityModifier = 0.05F;
	private static float realIntensity = 0F;
	static float timeScale = Time.timeScale;
	public static GameObject earthController = null;
	public static GameObject windController = null;
	public static GameObject waterController = null;
	public static GameObject sunController = null;

	public static void switchToEarth () {
		if (earthController != null) {
			activeElement = "earth";
		} else {
			activeElement = "!earth";
		}
	}

	public static void switchToWind () {
		if (windController != null) {
			activeElement = "wind";
		} else {
			activeElement = "!wind";
		}
	}

	public static void switchToWater () {
		if (waterController != null) {
			activeElement = "water";
		} else {
			activeElement = "!water";
		}
	}

	public static void switchToSun () {
		if (sunController != null) {
			activeElement = "sun";
		} else {
			activeElement = "!sun";
		}
	}

	public static void incrementIntensity () {
		if (realIntensity < 1F) {
			realIntensity += coefficientIntensityModifier;
		} else {
			realIntensity = 1F;
		}
		updateIntensity();
	}

	public static void decrementIntensity () {
		if (realIntensity > coefficientIntensityModifier) {
			realIntensity -= coefficientIntensityModifier;
		} else {
			realIntensity = 0F;
		}
		updateIntensity();
	}

	public static void updateIntensity () {
		if (timeScale == 1) {
			intensityOfElement = realIntensity;
		} else {
			intensityOfElement = 0F;
		}
	}
}