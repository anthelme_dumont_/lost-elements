﻿using UnityEngine;
using System.Collections;

public class EarthParticleSystem : MonoBehaviour {

	GameObject faultRight;
	GameObject faultLeft;
	GameObject faultObstacles;

	void Awake() {
		faultRight = GameObject.Find ("faille_1");
		faultLeft = GameObject.Find ("faille_2");
		faultObstacles = GameObject.Find("FaultObstacles");
		if (faultObstacles != null) {
			faultObstacles.SetActive (false);
		}
	}
 
	void OnParticleCollision(GameObject other) {
		if (other.tag == "Destructible" || other.tag == "DeadTree" || other.tag == "Tree" && other.transform.localScale.x > 0 && other.transform.localScale.y > 0 && other.transform.localScale.z > 0) {
			float intensity = ElementsController.intensityOfElement;
			float delay = Random.Range (0F, 0.5F);
			float time = Random.Range (1F, 3F);
			Vector3 amount = new Vector3 (3F, 3F, 3F);
			iTween.ShakeRotation(other.gameObject, iTween.Hash("amount", amount, "time", time, "delay", delay));
			if (other.gameObject.transform.position.y > -0.8F) {
				iTween.MoveAdd(other.gameObject, iTween.Hash("y", -(intensity / 2F), "time", time, "delay", delay));
			}
		} else if (other.tag == "Fault") {
			float intensity = ElementsController.intensityOfElement;
			float delay = Random.Range (0F, 0.5F);
			Vector3 amountLeft = new Vector3 (0, -intensity, 0);
			Vector3 amountRight = new Vector3 (0, intensity, 0);


			if (faultLeft.transform.localRotation.y >= 0.02F) {
				iTween.ShakeRotation(faultLeft.gameObject, iTween.Hash("amount", amountLeft, "time", 0.016666F, "delay", delay));
				iTween.RotateAdd(faultLeft.gameObject, iTween.Hash("y", -intensity, "time", 0.016666F, "delay", delay));

			}
			if (faultRight.transform.localRotation.y >= -0.998F) {
				iTween.ShakeRotation(faultRight.gameObject, iTween.Hash("amount", amountRight, "time", 0.016666F, "delay", delay));
				iTween.RotateAdd(faultRight.gameObject, iTween.Hash("y", intensity, "time", 0.016666F, "delay", delay));
			}

			if(faultLeft.transform.localRotation.y <= 0.03F || faultRight.transform.localRotation.y <= -0.997F) {
				GameObject chapter3Level1Board3ProgressController = GameObject.FindGameObjectWithTag ("ProgressController");
				chapter3Level1Board3ProgressController.GetComponent<Chapter3Level1Board3ProgressController>().faultIsShaking = true;
			}

			if (faultLeft.transform.localRotation.y <= 0.02F && faultLeft.transform.localPosition.x < -0.3F) {
				iTween.MoveAdd(faultLeft.gameObject, iTween.Hash("x", (intensity / 50F), "y", -(intensity / 50F), "time", 0.016666F, "delay", delay));
				faultObstacles.SetActive (true);
			}
			if (faultRight.transform.localRotation.y <= -0.998F && faultRight.transform.localPosition.x > -3F) {
				iTween.MoveAdd(faultRight.gameObject, iTween.Hash("x", (intensity / 50F), "y", -(intensity / 50F), "time", 0.016666F, "delay", delay));
				faultObstacles.SetActive (true);
			}

		}
	}
}
