﻿using UnityEngine;

public class SetRenderQueue : MonoBehaviour {

	void Start(){
		// get all renderers in this object and its children:
		Renderer[] renders = gameObject.GetComponentsInChildren<Renderer>();
		foreach (Renderer rendr in renders) {
			rendr.material.renderQueue = 2002; // set their renderQueue
		}
	}
}