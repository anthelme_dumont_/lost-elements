﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManagerController : MonoBehaviour {

	public bool nextLevelInLoad = false;

	void Awake () {
		SceneManager.LoadScene("interface", LoadSceneMode.Additive);
	}

	void Start() {
		SceneManager.LoadSceneAsync("SocketManager", LoadSceneMode.Additive);
		if (Application.loadedLevelName == "level-tuto-1") {
			StartCoroutine(startTutorial ());
		}
		if (Application.loadedLevelName == "level-chapter-3-level-1-board-3") {
			StartCoroutine(showAdvice());
		}
	}

	IEnumerator startTutorial() {
		yield return StartCoroutine(Utils.WaitForRealSeconds(2F));
		InterfaceController interfaceController = GameObject.FindGameObjectWithTag("Interface").GetComponent<InterfaceController>();
		interfaceController.pauseGame();
		interfaceController.buttonHowToPlay();
	}

	IEnumerator showAdvice() {
		yield return StartCoroutine(Utils.WaitForRealSeconds(2F));
		InterfaceController interfaceController = GameObject.FindGameObjectWithTag("Interface").GetComponent<InterfaceController>();
		interfaceController.showAdvice("useWater");
	}

	public IEnumerator loadNextLevel() {
		nextLevelInLoad = true;
		InterfaceController interfaceController = GameObject.FindGameObjectWithTag("Interface").GetComponent<InterfaceController>();
		interfaceController.sceneWasUnloaded ();
		yield return StartCoroutine(Utils.WaitForRealSeconds(0.5F));
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}
}
