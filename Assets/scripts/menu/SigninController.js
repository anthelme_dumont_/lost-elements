﻿#pragma strict

static var playerName : String = "";

function begin() {
	if (!PlayerPrefs.HasKey("Player1")) {
		PlayerPrefs.SetString("Player1", playerName);
		PlayerPrefs.SetFloat("Player1Level", 0.0);
		PlayerPrefs.SetString("CurrentPlayer", "Player1");
	} else if (!PlayerPrefs.HasKey("Player2")) {
		PlayerPrefs.SetString("Player2", playerName);
		PlayerPrefs.SetFloat("Player2Level", 0.0);
		PlayerPrefs.SetString("CurrentPlayer", "Player2");
	} else if (!PlayerPrefs.HasKey("Player3")) {
		PlayerPrefs.SetString("Player3", playerName);
		PlayerPrefs.SetFloat("Player3Level", 0.0);
		PlayerPrefs.SetString("CurrentPlayer", "Player3");
	} else if (!PlayerPrefs.HasKey("Player4")) {
		PlayerPrefs.SetString("Player4", playerName);
		PlayerPrefs.SetFloat("Player4Level", 0.0);
		PlayerPrefs.SetString("CurrentPlayer", "Player4");
	}

	SceneManagement.SceneManager.LoadSceneAsync("connection-relic");

}

function setPlayerName(_playerName: String) {
	playerName = _playerName;
}

function back() {
	SceneManagement.SceneManager.LoadScene("main-menu");
}
