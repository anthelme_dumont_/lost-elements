﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InterfaceController : MonoBehaviour {

	public float fadeTime = 0.5f;
	public GameObject buttonMenu;
	public GameObject backgroundLoadingScene;
	public GameObject backgroundMenu;
	public GameObject circularMenu;
	public GameObject intensitySpiral;
	public GameObject activeElement;
	public GameObject map;
	public Sprite[] spriteIntensitySpiral;
	public Sprite[] spriteElements;
	public GameObject popinQuit;
	public GameObject popinSettings;
	public GameObject popinTutoTap;
	public GameObject popinTutoLearn;
	public GameObject popinTutoIntensity;
	public GameObject popinTutoElement;
	public GameObject popinTutoIndicators;
	public GameObject popinMap;
	public GameObject popinActiveElementWater;
	public GameObject popinActiveElementWind;
	public GameObject popinActiveElementEarth;
	public GameObject popinActiveElementSun;
	public GameObject popinNewElementWater;
	public GameObject warningUnavailableElement;
	public GameObject adviceUseWater;
	private bool isPaused = false;
	private GameObject[] popins;
	private string tempActiveElement;
	private float tempIntensity;

	void Awake() {
		popins = GameObject.FindGameObjectsWithTag("Popin");
		StartCoroutine(sceneWasLoaded());
		tempActiveElement = ElementsController.activeElement;
		tempIntensity = ElementsController.intensityOfElement;
	}

	IEnumerator sceneWasLoaded() {
		yield return StartCoroutine(Utils.WaitForRealSeconds(0.5F));
		StartCoroutine(disableGameObject(backgroundLoadingScene));
		unpauseGame();
	}

	public void sceneWasUnloaded() {
		enableGameObject(backgroundLoadingScene);
		pauseGame ();
	}

	void Update() {
		if (!isPaused) {
			changeStepIntensitySpiral();
			changeActiveElement();
		}
	}

	public void pauseGame() {
		isPaused = true;

		Time.timeScale = 0;
	}

	//pause game

	public void unpauseGame() {
		isPaused = false;

		Time.timeScale = 1;
	}

	public bool getIsPaused(){
		return isPaused;
	}

	// show hide menu

	public void showCircularMenu() {
		StartCoroutine(disableGameObject(buttonMenu));
		StartCoroutine(disableGameObject(intensitySpiral));
		StartCoroutine(disableGameObject(activeElement));
		StartCoroutine(disableGameObject(map));

		enableGameObject(backgroundMenu);
		enableGameObject(circularMenu);
	}

	public void hideCircularMenu() {
		enableGameObject(buttonMenu);
		enableGameObject(intensitySpiral);
		enableGameObject(activeElement);
		enableGameObject(map);

		StartCoroutine(disableGameObject(backgroundMenu));
		StartCoroutine(disableGameObject(circularMenu));
	}

	//open close menu
	public void openCircularMenu() {
		pauseGame();

		showCircularMenu ();
	}

	public void closeCircularMenu() {
		unpauseGame();

		hideCircularMenu();
		closeAllPopins();
	}

	//Click button function

	public void buttonHome() {
		if(!popinQuit.activeSelf) {
			enableGameObject(popinQuit);
		}
	}

	public void buttonReset() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void buttonHowToPlay() {
		hideCircularMenu();
		enableGameObject(popinTutoIndicators);
	}

	public void buttonLearnMore() {
		Application.OpenURL("http://lostelements-game.com/learn-more.html");
	}

	public void buttonSettings() {
		if (!popinSettings.activeSelf) {
			hideCircularMenu();
			enableGameObject(popinSettings);
		}
	}

	public void buttonMap() {
		if (!popinMap.activeSelf) {
			pauseGame();
			enableGameObject(popinMap);
		}
	}

	public void buttonGotIt(string nextAction) {
		switch (nextAction) {
		case "popinTap":
			StartCoroutine (disableGameObject(popinTutoIndicators));
			enableGameObject(popinTutoTap);
			break;
		case "popinElement":
			StartCoroutine (delayPopinTutoElement());
			break;
		case "popinIntensity":
			StartCoroutine (delayPopinTutoIntensity());
			break;
		case "fadeOut":
			closeCircularMenu ();
			break;
		}
	}

	IEnumerator delayPopinTutoElement() {
		StartCoroutine (disableGameObject(popinTutoTap));
		unpauseGame ();
		yield return new WaitForSeconds(5F);
		enableGameObject(popinTutoElement);
		pauseGame ();
	}

	IEnumerator delayPopinTutoIntensity() {
		StartCoroutine(disableGameObject(popinTutoElement));
		unpauseGame ();
		yield return new WaitForSeconds(5F);
		enableGameObject(popinTutoIntensity);
		pauseGame ();
	}


	//Elements
	public void changeStepIntensitySpiral() {
		float intensity = ElementsController.intensityOfElement;
		if (intensity != tempIntensity) {
			tempIntensity = intensity;
			float mappedIntensity = ExtensionMethods.Map(intensity, 0F, 1F, 0F, 15F);
			float roundedIntensity = ExtensionMethods.Round(mappedIntensity, 1);

			Image image = intensitySpiral.GetComponent<Image>();
			image.sprite = spriteIntensitySpiral[(int)roundedIntensity];
		}

	}

	public void changeActiveElement() {
		string element = ElementsController.activeElement;
		if (element != tempActiveElement) {
			tempActiveElement = element;
			Image image = activeElement.GetComponent<Image>();

			StartCoroutine(switchElement(element));

			switch (element) {
			case "water":
				image.sprite = spriteElements[0];
				break;
			case "wind":
				image.sprite = spriteElements[1];
				break;
			case "sun":
				image.sprite = spriteElements[2];
				break;
			case "earth":
				image.sprite = spriteElements[3];
				break;
			case "!water":
				image.sprite = spriteElements[0];
				break;
			case "!wind":
				image.sprite = spriteElements[1];
				break;
			case "!sun":
				image.sprite = spriteElements[2];
				break;
			case "!earth":
				image.sprite = spriteElements [3];
				break;
			default:
				break;
			}

			if (element[0] != '!') {
				Color newColor = Color.white;
				newColor.a = 1;
				image.color = newColor;
			} else {
				Color newColor = Color.black;
				newColor.a = 0.25F;
				image.color = newColor;
				showWarning("unavaibleElement");
			}
		}

	}

	IEnumerator switchElement(string element) {
		switch (element) {
		case "water":
			enableGameObject(popinActiveElementWater);
			break;
		case "wind":
			enableGameObject(popinActiveElementWind);
			break;
		case "sun":
			enableGameObject(popinActiveElementSun);
			break;
		case "earth":
			enableGameObject(popinActiveElementEarth);
			break;
		}
		yield return StartCoroutine(Utils.WaitForRealSeconds(0.7F));
		closeAllPopinsActiveElement ();
	}

	public void showPopinNewElement(string element) {
		pauseGame ();
		switch (element) {
		case "water":
			enableGameObject(popinNewElementWater);
			break;
		case "wind":
			
			break;
		case "sun":
			
			break;
		case "earth":
			
			break;
		}
	}

	//Warning

	public void showWarning(string warning) {
		switch (warning) {
		case "unavaibleElement":
			if (!warningUnavailableElement.activeSelf || !popinTutoLearn.activeSelf) {
				StartCoroutine (warningUnavailable ());
			}
			break;
		}
	}

	IEnumerator warningUnavailable() {
		if (Application.loadedLevel != 8) {
			enableGameObject (warningUnavailableElement);
			yield return new WaitForSeconds(2F);
			StartCoroutine (disableGameObject (warningUnavailableElement));
		} else {
			pauseGame ();
			enableGameObject(popinTutoLearn);
			yield return StartCoroutine (Utils.WaitForRealSeconds (0F));
		}
	}

	//Advice

	public void showAdvice(string warning) {
		switch (warning) {
		case "useWater":
			if (!adviceUseWater.activeSelf) {
				StartCoroutine (useWater());
			}
			break;
		}
	}

	IEnumerator useWater() {
		enableGameObject (adviceUseWater);
		yield return new WaitForSeconds(5F);
		StartCoroutine (disableGameObject (adviceUseWater));
	}

	//Redirection
	public void goToMainMenu() {
		SceneManager.LoadScene("main-menu");
	}
		
	public void closeAllPopins() {
		foreach (GameObject popin in popins) {
			StartCoroutine(disableGameObject(popin));
		}
	}

	public void closeAllPopinsActiveElement() {
		StartCoroutine(disableGameObject(popinActiveElementWater));
		StartCoroutine(disableGameObject(popinActiveElementWind));
		StartCoroutine(disableGameObject(popinActiveElementEarth));
		StartCoroutine(disableGameObject(popinActiveElementSun));
	}

	//enable disable
	void enableGameObject(GameObject obj) {
		obj.SetActive(true);
		obj.GetComponent<FadeInOut>().setAlpha(0f);
		obj.GetComponent<FadeInOut>().fadeIn(fadeTime);
	}

	IEnumerator disableGameObject(GameObject obj) {
		obj.GetComponent<FadeInOut>().fadeOut(fadeTime);
		yield return StartCoroutine(Utils.WaitForRealSeconds(fadeTime));
		obj.SetActive(false);
	}
}