﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour {
	
	public bool visibleAtStart = false;

	void Start () {
		if (!visibleAtStart) {
			setAlpha(0f);
			gameObject.SetActive(false);
		}
	}

	public void fadeIn(float fadeTime) {
		if (gameObject.GetComponent<Image>() != null) {
			gameObject.GetComponent<Image>().CrossFadeAlpha(1f, fadeTime, true);
		} else {
			fadeInChildren(fadeTime);
		}
	}

	public void fadeOut(float fadeTime) {
		if (gameObject.GetComponent<Image>() != null) {
			gameObject.GetComponent<Image>().CrossFadeAlpha(0f, fadeTime, true);
		} else {
			fadeOutChildren(fadeTime);
		}
	}

	void fadeInChildren(float fadeTime) {
		Image[] images = gameObject.GetComponentsInChildren<Image>(); 
		Text[] texts = gameObject.GetComponentsInChildren<Text>(); 

		foreach (Image image in images) {
			image.CrossFadeAlpha (1f, fadeTime, true);
		}
		foreach (Text text in texts) {
			text.CrossFadeAlpha (1f, fadeTime, true);
		}
	}

	void fadeOutChildren(float fadeTime) {
		Image[] images = gameObject.GetComponentsInChildren<Image>(); 
		Text[] texts = gameObject.GetComponentsInChildren<Text>(); 

		foreach (Image image in images) {
			image.CrossFadeAlpha (0f, fadeTime, true);
		}
		foreach (Text text in texts) {
			text.CrossFadeAlpha (0f, fadeTime, true);
		}
	}

	public void setAlpha(float value) {
		if (gameObject.GetComponent<CanvasRenderer>() != null) {
			gameObject.GetComponent<CanvasRenderer>().SetAlpha (value);
		} else {
			setAlphaChildren(value);
		}
	}

	void setAlphaChildren(float value) {
		CanvasRenderer[] renderers = gameObject.GetComponentsInChildren<CanvasRenderer>();
		foreach (CanvasRenderer renderer in renderers) {
			renderer.SetAlpha(value);
		}
	}
}
