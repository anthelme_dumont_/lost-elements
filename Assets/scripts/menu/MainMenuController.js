﻿#pragma strict

function newGame () {
	SceneManagement.SceneManager.LoadSceneAsync("signin");
}

function loadGame () {
	SceneManagement.SceneManager.LoadSceneAsync("login");
}

function settings () {
	SceneManagement.SceneManager.LoadSceneAsync("settings");
}

function learnMore () {
	Application.OpenURL("http://lostelements-game.com/learn-more/");
}