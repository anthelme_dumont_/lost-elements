﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoginController : MonoBehaviour {

	public GameObject player1;
	public GameObject player2;
	public GameObject player3;
	public GameObject player4;

	void Start () {
		if(PlayerPrefs.HasKey("Player1")) {
			populateTemplate("Player1", player1);
			player1.SetActive(true);
		}
		if(PlayerPrefs.HasKey("Player2")) {
			populateTemplate("Player2", player2);
			player2.SetActive(true);
		}
		if(PlayerPrefs.HasKey("Player3")) {
			populateTemplate("Player3", player3);
			player3.SetActive(true);
		}
		if(PlayerPrefs.HasKey("Player4")) {
			player4.SetActive(true);
		}

	}

	void populateTemplate(string player, GameObject playerObject) {
		Text[] children = playerObject.GetComponentsInChildren<Text>();

		foreach(Text child in children) {
			if (child.gameObject.name == "Nickname") {
				Text nickname = child.gameObject.GetComponent<Text>();
				nickname.text = PlayerPrefs.GetString (player);
			}
		}
	}

	public void loadChapter3() {
		SceneManager.LoadSceneAsync("intro-chapter");
	}

	public void back() {
		SceneManager.LoadScene("main-menu");
	}
}
