﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class IntroChapter : MonoBehaviour {

	public float wait = 1F;

	void Awake() {
		StartCoroutine(changeScene());
	}

	IEnumerator changeScene() {
		yield return StartCoroutine(Utils.WaitForRealSeconds(wait));
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}
}
