﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ConnectionRelicController : MonoBehaviour {

	public CheckRelic checkRelic;
	public GameObject relicOff;
	public GameObject relicOn;
	public GameObject button;

	public void back() {
		SceneManager.LoadScene("main-menu");
	}

	public void play() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

	void Update() {
		if (checkRelic.isConnected) {
			relicOff.SetActive(false);
			relicOn.SetActive(true);
			button.GetComponent<Button>().interactable = true;
		} else {
			relicOff.SetActive(true);
			relicOn.SetActive(false);
			button.GetComponent<Button>().interactable = false;
		}
	}
}
