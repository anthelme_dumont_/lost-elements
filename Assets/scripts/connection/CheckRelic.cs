﻿using UnityEngine;
using System.Collections;

public class CheckRelic : MonoBehaviour {
	public bool isConnected = false;

	void Start() {
		StartCoroutine (setFakeConnection ());
	}

	IEnumerator setFakeConnection() {
		yield return StartCoroutine(Utils.WaitForRealSeconds(1.5F));
		isConnected = true;
	}

}