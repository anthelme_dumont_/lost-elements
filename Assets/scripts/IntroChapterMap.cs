﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class IntroChapterMap : MonoBehaviour {

	public void loadNextLevel() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}
}
