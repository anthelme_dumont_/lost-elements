﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class IntroductionController : MonoBehaviour {

	void Awake() {
		StartCoroutine(changeSceneWhenFinished ());
	}

	IEnumerator changeSceneWhenFinished() {
		yield return new WaitForEndOfFrame();
		SceneManager.LoadScene("level-tuto-1");
	}
}