﻿using UnityEngine;
using System.Collections;

public class Chapter3Level1Board3ProgressController : MonoBehaviour {

	public GameObject filledWater;
	public GameObject water;
	public GameObject fault;
	GameObject[] clouds;
	public bool faultIsShaking = false;
	bool stopWaterBalance = false;
	bool hasGrow = false;
	WaterController waterController;

	// Use this for initialization
	void Awake () {
		clouds = GameObject.FindGameObjectsWithTag("Cloud");
		waterController = GameObject.Find("_WaterController").GetComponent<WaterController>();
	}

	void FixedUpdate () {
		checkCloudPosition();
		checkFault();
	}

	void checkCloudPosition() {
		float cloudDistance = getDistance(clouds[0]);

		if (cloudDistance <= 9.2F) {
			WaterController waterController = GameObject.Find ("_WaterController").GetComponent<WaterController>();
			water.tag = "Water";
			GameObject[] newWaters;
			newWaters = new GameObject[1];
			newWaters[0] = water;
			waterController.waters = newWaters;
		}
	}

	void checkFault() {
		Vector3 newScaleWater = water.transform.localScale;
		Vector3 newScaleFilledWater = filledWater.transform.localScale;
		if(faultIsShaking && !stopWaterBalance) {
			float intensity = ElementsController.intensityOfElement;
			if (newScaleWater.y < 0.5) {
				newScaleWater.y += waterController.minScale + intensity / 50F;
				water.transform.localScale = newScaleWater;
			}
			if (newScaleFilledWater.y > 0.1F) {
				newScaleFilledWater.y -= intensity / 50F;
				filledWater.transform.localScale = newScaleFilledWater;
			}
		}

		if(newScaleWater.y >= 0.5 && newScaleFilledWater.y <= 0.1F) {
			stopWaterBalance = true;
			WaterController waterController = GameObject.Find ("_WaterController").GetComponent<WaterController>();
			water.tag = "Water";
			GameObject[] newWaters;
			newWaters = new GameObject[2];
			newWaters[0] = water;
			newWaters[1] = filledWater;
			waterController.waters = newWaters;

		}

		//Trees
		if (!hasGrow && water.transform.localScale.y >= 0.8F) {
			hasGrow = true;
			TreeController treeController = GameObject.Find ("_TreeController").GetComponent<TreeController>();
			treeController.grow();
		}
	}

	float getDistance(GameObject cloud) {
		float distance = Vector3.Distance(water.transform.position, cloud.transform.position);
		return distance;
	}
}
