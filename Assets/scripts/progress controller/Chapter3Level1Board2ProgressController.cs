﻿using UnityEngine;
using System.Collections;

public class Chapter3Level1Board2ProgressController : MonoBehaviour {

	public GameObject bridge;
	public GameObject element;
	public GameObject particlesElement;
	public GameObject relic;
	public GameObject doorController;
	InterfaceController interfaceController;

	// Update is called once per frame
	void FixedUpdate () {
		checkElementTime();
		checkBridgeTime();
	}

	void checkElementTime() {
		MegaPointCache megaPointCache = element.GetComponent<MegaPointCache> ();
		if (megaPointCache.time >= 4F && megaPointCache.animated == true) {
			interfaceController = GameObject.FindGameObjectWithTag("Interface").GetComponent<InterfaceController>();
			megaPointCache.animated = false;
			interfaceController.showPopinNewElement ("water");
			StartCoroutine (startBridgeAnimation ());
			element.SetActive (false);
			particlesElement.SetActive (true);
		}
	}

	IEnumerator startBridgeAnimation() {
		yield return new WaitForSeconds(4F);
		MegaPointCache megaPointCache = bridge.GetComponent<MegaPointCache> ();
		megaPointCache.animated = true;
	}

	void checkBridgeTime() {
		MegaPointCache megaPointCache = bridge.GetComponent<MegaPointCache> ();
		if (megaPointCache.time <= 0.3F && megaPointCache.animated == true) {
			megaPointCache.animated = false;
			doorController.SetActive (true);
		}
	}
}
