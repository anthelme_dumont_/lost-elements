﻿using UnityEngine;
using System.Collections;

public class Chapter3Level1Board1ProgressController : MonoBehaviour {

	public GameObject water;
	public GameObject[] destructiblesRocks;
	public bool[] destructiblesRocksProgress;
	public GameObject[] destructiblesTrees;
	public bool[] destructiblesTreesProgress;
	public bool seedsPropagate = false;
	bool hasGrow = false;
	public GameObject doorController;

	// Update is called once per frame
	void FixedUpdate () {
		for (int i = 0; i < destructiblesRocks.Length; i++) {
			if (destructiblesRocks[i].transform.localPosition.y <= -0.3F) {
				destructiblesRocksProgress[i] = true;
			}
		}
		for (int i = 0; i < destructiblesTrees.Length; i++) {
			if (destructiblesTrees[i].transform.localPosition.y <= -1F) {
				destructiblesTreesProgress[i] = true;
			}
		}

		checkForVictory ();
	}

	void checkForVictory() {
		//Rocks
		MegaPointCache megaPointCache = water.GetComponent<MegaPointCache> ();
		if (!destructiblesRocksProgress[0] || !destructiblesRocksProgress[1]) {
			megaPointCache.animated = false;
			megaPointCache.time = 0F;
		} else if (destructiblesRocksProgress[0] && destructiblesRocksProgress[1] && !destructiblesRocksProgress[2] || !destructiblesRocksProgress[3]) {
			if (megaPointCache.time < 0.9F) {
				megaPointCache.animated = true;
			} else {
				megaPointCache.animated = false;
			}
		} else if (destructiblesRocksProgress[0] && destructiblesRocksProgress[1] && destructiblesRocksProgress[2] && destructiblesRocksProgress[3]) {
			megaPointCache.animated = true;
			doorController.SetActive(true);
		}

		//Trees
		if (destructiblesTreesProgress[0] && destructiblesTreesProgress[1] && destructiblesTreesProgress[2] && destructiblesTreesProgress[3] && seedsPropagate && !hasGrow) {
			hasGrow = true;
			TreeController treeController = GameObject.Find ("_TreeController").GetComponent<TreeController>();
			treeController.grow();
			doorController.SetActive(true);
		}
	}
}
