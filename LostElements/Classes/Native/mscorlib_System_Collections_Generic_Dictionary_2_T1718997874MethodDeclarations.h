﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4191540488MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m409010008(__this, ___object, ___method, method) ((  void (*) (Transform_1_t1718997874 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2052388693_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m405497632(__this, ___key, ___value, method) ((  DictionaryEntry_t130027246  (*) (Transform_1_t1718997874 *, Type_t *, SafeDictionary_2_t2250466201 *, const MethodInfo*))Transform_1_Invoke_m757436355_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m4100459723(__this, ___key, ___value, ___callback, ___object, method) ((  Il2CppObject * (*) (Transform_1_t1718997874 *, Type_t *, SafeDictionary_2_t2250466201 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m397518190_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m693994858(__this, ___result, method) ((  DictionaryEntry_t130027246  (*) (Transform_1_t1718997874 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3155601639_gshared)(__this, ___result, method)
