﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Object
struct Il2CppObject;
// System.Net.Sockets.SocketAsyncEventArgs
struct SocketAsyncEventArgs_t970431102;
// System.Net.EndPoint
struct EndPoint_t1294049535;
// SuperSocket.ClientEngine.ConnectedCallback
struct ConnectedCallback_t1584521517;
// System.Net.IPAddress
struct IPAddress_t3220500535;
// SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState
struct DnsConnectState_t2343074160;
// System.Net.Sockets.Socket
struct Socket_t150013987;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// SuperSocket.ClientEngine.ConnectAsyncExtension/ConnectToken
struct ConnectToken_t2068031311;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t3494006030;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// SuperSocket.ClientEngine.ProxyEventArgs
struct ProxyEventArgs_t3602520776;
// System.Exception
struct Exception_t1967233988;
// System.Net.DnsEndPoint
struct DnsEndPoint_t1814926600;
// System.String
struct String_t;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "SuperSocket_ClientEngine_Common_U3CModuleU3E86524790.h"
#include "SuperSocket_ClientEngine_Common_U3CModuleU3E86524790MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_ClientE931740524.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_ClientE931740524MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "System_System_Net_Sockets_SocketAsyncEventArgs970431102.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_EventHandler_1_gen1813008745MethodDeclarations.h"
#include "System_System_Net_Sockets_SocketAsyncEventArgs970431102MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client2068031311MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client1584521517MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client2068031311.h"
#include "mscorlib_System_EventHandler_1_gen1813008745.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client1584521517.h"
#include "System_System_Net_Sockets_Socket150013987.h"
#include "System_System_Net_EndPoint1294049535.h"
#include "SuperSocket_ClientEngine_Common_System_Net_DnsEndP1814926600MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1363551830MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client2343074160MethodDeclarations.h"
#include "System_System_Net_Dns3812891763MethodDeclarations.h"
#include "System_System_Net_Sockets_Socket150013987MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_System_Net_DnsEndP1814926600.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client2343074160.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Boolean211005341.h"
#include "System_System_Net_EndPoint1294049535MethodDeclarations.h"
#include "System_System_Net_Sockets_AddressFamily1632127208.h"
#include "System_System_Net_Sockets_SocketType3987933693.h"
#include "System_System_Net_Sockets_ProtocolType2288572066.h"
#include "System_System_Net_IPAddress3220500535.h"
#include "System_System_Net_IPAddress3220500535MethodDeclarations.h"
#include "System_ArrayTypes.h"
#include "System_System_Net_IPEndPoint1265996582MethodDeclarations.h"
#include "System_System_Net_IPEndPoint1265996582.h"
#include "System_System_Net_Sockets_SocketError291509957.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_ClientEn80775955.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_ClientEn80775955MethodDeclarations.h"
#include "mscorlib_System_Random922188920MethodDeclarations.h"
#include "mscorlib_System_Random922188920.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client3602520776.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client3602520776MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::SocketAsyncEventCompleted(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern TypeInfo* EventHandler_1_t1813008745_il2cpp_TypeInfo_var;
extern TypeInfo* ConnectToken_t2068031311_il2cpp_TypeInfo_var;
extern TypeInfo* Socket_t150013987_il2cpp_TypeInfo_var;
extern const MethodInfo* ConnectAsyncExtension_SocketAsyncEventCompleted_m2832229679_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3542837379_MethodInfo_var;
extern const uint32_t ConnectAsyncExtension_SocketAsyncEventCompleted_m2832229679_MetadataUsageId;
extern "C"  void ConnectAsyncExtension_SocketAsyncEventCompleted_m2832229679 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectAsyncExtension_SocketAsyncEventCompleted_m2832229679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConnectToken_t2068031311 * V_0 = NULL;
	{
		SocketAsyncEventArgs_t970431102 * L_0 = ___e;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)ConnectAsyncExtension_SocketAsyncEventCompleted_m2832229679_MethodInfo_var);
		EventHandler_1_t1813008745 * L_2 = (EventHandler_1_t1813008745 *)il2cpp_codegen_object_new(EventHandler_1_t1813008745_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m3542837379(L_2, NULL, L_1, /*hidden argument*/EventHandler_1__ctor_m3542837379_MethodInfo_var);
		NullCheck(L_0);
		SocketAsyncEventArgs_remove_Completed_m3312204008(L_0, L_2, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_3 = ___e;
		NullCheck(L_3);
		Il2CppObject * L_4 = SocketAsyncEventArgs_get_UserToken_m1440215414(L_3, /*hidden argument*/NULL);
		V_0 = ((ConnectToken_t2068031311 *)CastclassClass(L_4, ConnectToken_t2068031311_il2cpp_TypeInfo_var));
		SocketAsyncEventArgs_t970431102 * L_5 = ___e;
		NullCheck(L_5);
		SocketAsyncEventArgs_set_UserToken_m3057049639(L_5, NULL, /*hidden argument*/NULL);
		ConnectToken_t2068031311 * L_6 = V_0;
		NullCheck(L_6);
		ConnectedCallback_t1584521517 * L_7 = ConnectToken_get_Callback_m655785259(L_6, /*hidden argument*/NULL);
		Il2CppObject * L_8 = ___sender;
		ConnectToken_t2068031311 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = ConnectToken_get_State_m3260382870(L_9, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_11 = ___e;
		NullCheck(L_7);
		ConnectedCallback_Invoke_m3294233759(L_7, ((Socket_t150013987 *)IsInstClass(L_8, Socket_t150013987_il2cpp_TypeInfo_var)), L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Net.Sockets.SocketAsyncEventArgs SuperSocket.ClientEngine.ConnectAsyncExtension::CreateSocketAsyncEventArgs(System.Net.EndPoint,SuperSocket.ClientEngine.ConnectedCallback,System.Object)
extern TypeInfo* SocketAsyncEventArgs_t970431102_il2cpp_TypeInfo_var;
extern TypeInfo* ConnectToken_t2068031311_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_1_t1813008745_il2cpp_TypeInfo_var;
extern const MethodInfo* ConnectAsyncExtension_SocketAsyncEventCompleted_m2832229679_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3542837379_MethodInfo_var;
extern const uint32_t ConnectAsyncExtension_CreateSocketAsyncEventArgs_m2221185602_MetadataUsageId;
extern "C"  SocketAsyncEventArgs_t970431102 * ConnectAsyncExtension_CreateSocketAsyncEventArgs_m2221185602 (Il2CppObject * __this /* static, unused */, EndPoint_t1294049535 * ___remoteEndPoint, ConnectedCallback_t1584521517 * ___callback, Il2CppObject * ___state, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectAsyncExtension_CreateSocketAsyncEventArgs_m2221185602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SocketAsyncEventArgs_t970431102 * V_0 = NULL;
	ConnectToken_t2068031311 * V_1 = NULL;
	{
		SocketAsyncEventArgs_t970431102 * L_0 = (SocketAsyncEventArgs_t970431102 *)il2cpp_codegen_object_new(SocketAsyncEventArgs_t970431102_il2cpp_TypeInfo_var);
		SocketAsyncEventArgs__ctor_m2409327454(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		SocketAsyncEventArgs_t970431102 * L_1 = V_0;
		ConnectToken_t2068031311 * L_2 = (ConnectToken_t2068031311 *)il2cpp_codegen_object_new(ConnectToken_t2068031311_il2cpp_TypeInfo_var);
		ConnectToken__ctor_m805112283(L_2, /*hidden argument*/NULL);
		V_1 = L_2;
		ConnectToken_t2068031311 * L_3 = V_1;
		Il2CppObject * L_4 = ___state;
		NullCheck(L_3);
		ConnectToken_set_State_m3531158087(L_3, L_4, /*hidden argument*/NULL);
		ConnectToken_t2068031311 * L_5 = V_1;
		ConnectedCallback_t1584521517 * L_6 = ___callback;
		NullCheck(L_5);
		ConnectToken_set_Callback_m1292756542(L_5, L_6, /*hidden argument*/NULL);
		ConnectToken_t2068031311 * L_7 = V_1;
		NullCheck(L_1);
		SocketAsyncEventArgs_set_UserToken_m3057049639(L_1, L_7, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_8 = V_0;
		EndPoint_t1294049535 * L_9 = ___remoteEndPoint;
		NullCheck(L_8);
		SocketAsyncEventArgs_set_RemoteEndPoint_m1360575983(L_8, L_9, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_10 = V_0;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)ConnectAsyncExtension_SocketAsyncEventCompleted_m2832229679_MethodInfo_var);
		EventHandler_1_t1813008745 * L_12 = (EventHandler_1_t1813008745 *)il2cpp_codegen_object_new(EventHandler_1_t1813008745_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m3542837379(L_12, NULL, L_11, /*hidden argument*/EventHandler_1__ctor_m3542837379_MethodInfo_var);
		NullCheck(L_10);
		SocketAsyncEventArgs_add_Completed_m504170843(L_10, L_12, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_13 = V_0;
		return L_13;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::ConnectAsyncInternal(System.Net.EndPoint,SuperSocket.ClientEngine.ConnectedCallback,System.Object)
extern TypeInfo* DnsEndPoint_t1814926600_il2cpp_TypeInfo_var;
extern TypeInfo* AsyncCallback_t1363551830_il2cpp_TypeInfo_var;
extern TypeInfo* DnsConnectState_t2343074160_il2cpp_TypeInfo_var;
extern TypeInfo* Dns_t3812891763_il2cpp_TypeInfo_var;
extern TypeInfo* IAsyncResult_t537683269_il2cpp_TypeInfo_var;
extern TypeInfo* Socket_t150013987_il2cpp_TypeInfo_var;
extern const MethodInfo* ConnectAsyncExtension_OnGetHostAddresses_m3640693833_MethodInfo_var;
extern const uint32_t ConnectAsyncExtension_ConnectAsyncInternal_m3327551872_MetadataUsageId;
extern "C"  void ConnectAsyncExtension_ConnectAsyncInternal_m3327551872 (Il2CppObject * __this /* static, unused */, EndPoint_t1294049535 * ___remoteEndPoint, ConnectedCallback_t1584521517 * ___callback, Il2CppObject * ___state, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectAsyncExtension_ConnectAsyncInternal_m3327551872_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DnsEndPoint_t1814926600 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	DnsConnectState_t2343074160 * V_2 = NULL;
	SocketAsyncEventArgs_t970431102 * V_3 = NULL;
	Socket_t150013987 * V_4 = NULL;
	{
		EndPoint_t1294049535 * L_0 = ___remoteEndPoint;
		if (!((DnsEndPoint_t1814926600 *)IsInstClass(L_0, DnsEndPoint_t1814926600_il2cpp_TypeInfo_var)))
		{
			goto IL_0057;
		}
	}
	{
		EndPoint_t1294049535 * L_1 = ___remoteEndPoint;
		V_0 = ((DnsEndPoint_t1814926600 *)CastclassClass(L_1, DnsEndPoint_t1814926600_il2cpp_TypeInfo_var));
		DnsEndPoint_t1814926600 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = DnsEndPoint_get_Host_m1042524665(L_2, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)ConnectAsyncExtension_OnGetHostAddresses_m3640693833_MethodInfo_var);
		AsyncCallback_t1363551830 * L_5 = (AsyncCallback_t1363551830 *)il2cpp_codegen_object_new(AsyncCallback_t1363551830_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_5, NULL, L_4, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_6 = (DnsConnectState_t2343074160 *)il2cpp_codegen_object_new(DnsConnectState_t2343074160_il2cpp_TypeInfo_var);
		DnsConnectState__ctor_m837116908(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		DnsConnectState_t2343074160 * L_7 = V_2;
		DnsEndPoint_t1814926600 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = DnsEndPoint_get_Port_m2270576877(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		DnsConnectState_set_Port_m3846838119(L_7, L_9, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_10 = V_2;
		ConnectedCallback_t1584521517 * L_11 = ___callback;
		NullCheck(L_10);
		DnsConnectState_set_Callback_m2832258125(L_10, L_11, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_12 = V_2;
		Il2CppObject * L_13 = ___state;
		NullCheck(L_12);
		DnsConnectState_set_State_m885005398(L_12, L_13, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_14 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Dns_t3812891763_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = Dns_BeginGetHostAddresses_m2422772154(NULL /*static, unused*/, L_3, L_5, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		Il2CppObject * L_16 = V_1;
		NullCheck(L_16);
		bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(2 /* System.Boolean System.IAsyncResult::get_CompletedSynchronously() */, IAsyncResult_t537683269_il2cpp_TypeInfo_var, L_16);
		if (!L_17)
		{
			goto IL_0078;
		}
	}
	{
		Il2CppObject * L_18 = V_1;
		ConnectAsyncExtension_OnGetHostAddresses_m3640693833(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0057:
	{
		EndPoint_t1294049535 * L_19 = ___remoteEndPoint;
		ConnectedCallback_t1584521517 * L_20 = ___callback;
		Il2CppObject * L_21 = ___state;
		SocketAsyncEventArgs_t970431102 * L_22 = ConnectAsyncExtension_CreateSocketAsyncEventArgs_m2221185602(NULL /*static, unused*/, L_19, L_20, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		EndPoint_t1294049535 * L_23 = ___remoteEndPoint;
		NullCheck(L_23);
		int32_t L_24 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Net.Sockets.AddressFamily System.Net.EndPoint::get_AddressFamily() */, L_23);
		Socket_t150013987 * L_25 = (Socket_t150013987 *)il2cpp_codegen_object_new(Socket_t150013987_il2cpp_TypeInfo_var);
		Socket__ctor_m3306971388(L_25, L_24, 1, 6, /*hidden argument*/NULL);
		V_4 = L_25;
		Socket_t150013987 * L_26 = V_4;
		SocketAsyncEventArgs_t970431102 * L_27 = V_3;
		NullCheck(L_26);
		Socket_ConnectAsync_m327318847(L_26, L_27, /*hidden argument*/NULL);
	}

IL_0078:
	{
		return;
	}
}
// System.Net.IPAddress SuperSocket.ClientEngine.ConnectAsyncExtension::GetNextAddress(SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState,System.Net.Sockets.Socket&)
extern "C"  IPAddress_t3220500535 * ConnectAsyncExtension_GetNextAddress_m2210336608 (Il2CppObject * __this /* static, unused */, DnsConnectState_t2343074160 * ___state, Socket_t150013987 ** ___attempSocket, const MethodInfo* method)
{
	IPAddress_t3220500535 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		V_0 = (IPAddress_t3220500535 *)NULL;
		Socket_t150013987 ** L_0 = ___attempSocket;
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)NULL);
		DnsConnectState_t2343074160 * L_1 = ___state;
		NullCheck(L_1);
		int32_t L_2 = DnsConnectState_get_NextAddressIndex_m3972185532(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_004d;
	}

IL_000e:
	{
		int32_t L_3 = V_1;
		DnsConnectState_t2343074160 * L_4 = ___state;
		NullCheck(L_4);
		IPAddressU5BU5D_t3494006030* L_5 = DnsConnectState_get_Addresses_m1195483209(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		if ((((int32_t)L_3) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		return (IPAddress_t3220500535 *)NULL;
	}

IL_001b:
	{
		DnsConnectState_t2343074160 * L_6 = ___state;
		NullCheck(L_6);
		IPAddressU5BU5D_t3494006030* L_7 = DnsConnectState_get_Addresses_m1195483209(L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		int32_t L_9 = L_8;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_9);
		int32_t L_10 = L_9;
		V_0 = ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
		IPAddress_t3220500535 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = IPAddress_get_AddressFamily_m2364322247(L_11, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)23)))))
		{
			goto IL_003c;
		}
	}
	{
		Socket_t150013987 ** L_13 = ___attempSocket;
		DnsConnectState_t2343074160 * L_14 = ___state;
		NullCheck(L_14);
		Socket_t150013987 * L_15 = DnsConnectState_get_Socket6_m3757123554(L_14, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_13)) = (Il2CppObject *)L_15;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_13), (Il2CppObject *)L_15);
		goto IL_004d;
	}

IL_003c:
	{
		IPAddress_t3220500535 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = IPAddress_get_AddressFamily_m2364322247(L_16, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)2))))
		{
			goto IL_004d;
		}
	}
	{
		Socket_t150013987 ** L_18 = ___attempSocket;
		DnsConnectState_t2343074160 * L_19 = ___state;
		NullCheck(L_19);
		Socket_t150013987 * L_20 = DnsConnectState_get_Socket4_m3757121632(L_19, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_18)) = (Il2CppObject *)L_20;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_18), (Il2CppObject *)L_20);
	}

IL_004d:
	{
		Socket_t150013987 ** L_21 = ___attempSocket;
		if (!(*((Socket_t150013987 **)L_21)))
		{
			goto IL_000e;
		}
	}
	{
		DnsConnectState_t2343074160 * L_22 = ___state;
		int32_t L_23 = V_1;
		NullCheck(L_22);
		DnsConnectState_set_NextAddressIndex_m2086294711(L_22, L_23, /*hidden argument*/NULL);
		IPAddress_t3220500535 * L_24 = V_0;
		return L_24;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::OnGetHostAddresses(System.IAsyncResult)
extern TypeInfo* IAsyncResult_t537683269_il2cpp_TypeInfo_var;
extern TypeInfo* DnsConnectState_t2343074160_il2cpp_TypeInfo_var;
extern TypeInfo* Dns_t3812891763_il2cpp_TypeInfo_var;
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern TypeInfo* SocketAsyncEventArgs_t970431102_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_1_t1813008745_il2cpp_TypeInfo_var;
extern TypeInfo* IPEndPoint_t1265996582_il2cpp_TypeInfo_var;
extern const MethodInfo* ConnectAsyncExtension_SocketConnectCompleted_m1655121833_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3542837379_MethodInfo_var;
extern const uint32_t ConnectAsyncExtension_OnGetHostAddresses_m3640693833_MetadataUsageId;
extern "C"  void ConnectAsyncExtension_OnGetHostAddresses_m3640693833 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectAsyncExtension_OnGetHostAddresses_m3640693833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DnsConnectState_t2343074160 * V_0 = NULL;
	IPAddressU5BU5D_t3494006030* V_1 = NULL;
	Socket_t150013987 * V_2 = NULL;
	IPAddress_t3220500535 * V_3 = NULL;
	SocketAsyncEventArgs_t970431102 * V_4 = NULL;
	IPEndPoint_t1265996582 * V_5 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___result;
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.IAsyncResult::get_AsyncState() */, IAsyncResult_t537683269_il2cpp_TypeInfo_var, L_0);
		V_0 = ((DnsConnectState_t2343074160 *)IsInstClass(L_1, DnsConnectState_t2343074160_il2cpp_TypeInfo_var));
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_2 = ___result;
		IL2CPP_RUNTIME_CLASS_INIT(Dns_t3812891763_il2cpp_TypeInfo_var);
		IPAddressU5BU5D_t3494006030* L_3 = Dns_EndGetHostAddresses_m1749643331(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_002b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0015;
		throw e;
	}

CATCH_0015:
	{ // begin catch(System.Object)
		DnsConnectState_t2343074160 * L_4 = V_0;
		NullCheck(L_4);
		ConnectedCallback_t1584521517 * L_5 = DnsConnectState_get_Callback_m816038482(L_4, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_6 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_7 = DnsConnectState_get_State_m113258909(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ConnectedCallback_Invoke_m3294233759(L_5, (Socket_t150013987 *)NULL, L_7, (SocketAsyncEventArgs_t970431102 *)NULL, /*hidden argument*/NULL);
		goto IL_0047;
	} // end catch (depth: 1)

IL_002b:
	{
		IPAddressU5BU5D_t3494006030* L_8 = V_1;
		if (!L_8)
		{
			goto IL_0034;
		}
	}
	{
		IPAddressU5BU5D_t3494006030* L_9 = V_1;
		NullCheck(L_9);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))) > ((int32_t)0)))
		{
			goto IL_0048;
		}
	}

IL_0034:
	{
		DnsConnectState_t2343074160 * L_10 = V_0;
		NullCheck(L_10);
		ConnectedCallback_t1584521517 * L_11 = DnsConnectState_get_Callback_m816038482(L_10, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_12 = V_0;
		NullCheck(L_12);
		Il2CppObject * L_13 = DnsConnectState_get_State_m113258909(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		ConnectedCallback_Invoke_m3294233759(L_11, (Socket_t150013987 *)NULL, L_13, (SocketAsyncEventArgs_t970431102 *)NULL, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}

IL_0048:
	{
		DnsConnectState_t2343074160 * L_14 = V_0;
		IPAddressU5BU5D_t3494006030* L_15 = V_1;
		NullCheck(L_14);
		DnsConnectState_set_Addresses_m1679752656(L_14, L_15, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_16 = V_0;
		ConnectAsyncExtension_CreateAttempSocket_m1410490880(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_17 = V_0;
		IPAddress_t3220500535 * L_18 = ConnectAsyncExtension_GetNextAddress_m2210336608(NULL /*static, unused*/, L_17, (&V_2), /*hidden argument*/NULL);
		V_3 = L_18;
		IPAddress_t3220500535 * L_19 = V_3;
		if (L_19)
		{
			goto IL_0075;
		}
	}
	{
		DnsConnectState_t2343074160 * L_20 = V_0;
		NullCheck(L_20);
		ConnectedCallback_t1584521517 * L_21 = DnsConnectState_get_Callback_m816038482(L_20, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_22 = V_0;
		NullCheck(L_22);
		Il2CppObject * L_23 = DnsConnectState_get_State_m113258909(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		ConnectedCallback_Invoke_m3294233759(L_21, (Socket_t150013987 *)NULL, L_23, (SocketAsyncEventArgs_t970431102 *)NULL, /*hidden argument*/NULL);
		return;
	}

IL_0075:
	{
		SocketAsyncEventArgs_t970431102 * L_24 = (SocketAsyncEventArgs_t970431102 *)il2cpp_codegen_object_new(SocketAsyncEventArgs_t970431102_il2cpp_TypeInfo_var);
		SocketAsyncEventArgs__ctor_m2409327454(L_24, /*hidden argument*/NULL);
		V_4 = L_24;
		SocketAsyncEventArgs_t970431102 * L_25 = V_4;
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)ConnectAsyncExtension_SocketConnectCompleted_m1655121833_MethodInfo_var);
		EventHandler_1_t1813008745 * L_27 = (EventHandler_1_t1813008745 *)il2cpp_codegen_object_new(EventHandler_1_t1813008745_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m3542837379(L_27, NULL, L_26, /*hidden argument*/EventHandler_1__ctor_m3542837379_MethodInfo_var);
		NullCheck(L_25);
		SocketAsyncEventArgs_add_Completed_m504170843(L_25, L_27, /*hidden argument*/NULL);
		IPAddress_t3220500535 * L_28 = V_3;
		DnsConnectState_t2343074160 * L_29 = V_0;
		NullCheck(L_29);
		int32_t L_30 = DnsConnectState_get_Port_m30529900(L_29, /*hidden argument*/NULL);
		IPEndPoint_t1265996582 * L_31 = (IPEndPoint_t1265996582 *)il2cpp_codegen_object_new(IPEndPoint_t1265996582_il2cpp_TypeInfo_var);
		IPEndPoint__ctor_m613724246(L_31, L_28, L_30, /*hidden argument*/NULL);
		V_5 = L_31;
		SocketAsyncEventArgs_t970431102 * L_32 = V_4;
		IPEndPoint_t1265996582 * L_33 = V_5;
		NullCheck(L_32);
		SocketAsyncEventArgs_set_RemoteEndPoint_m1360575983(L_32, L_33, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_34 = V_4;
		DnsConnectState_t2343074160 * L_35 = V_0;
		NullCheck(L_34);
		SocketAsyncEventArgs_set_UserToken_m3057049639(L_34, L_35, /*hidden argument*/NULL);
		Socket_t150013987 * L_36 = V_2;
		SocketAsyncEventArgs_t970431102 * L_37 = V_4;
		NullCheck(L_36);
		bool L_38 = Socket_ConnectAsync_m327318847(L_36, L_37, /*hidden argument*/NULL);
		if (L_38)
		{
			goto IL_00c0;
		}
	}
	{
		Socket_t150013987 * L_39 = V_2;
		SocketAsyncEventArgs_t970431102 * L_40 = V_4;
		ConnectAsyncExtension_SocketConnectCompleted_m1655121833(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
	}

IL_00c0:
	{
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::SocketConnectCompleted(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern TypeInfo* DnsConnectState_t2343074160_il2cpp_TypeInfo_var;
extern TypeInfo* Socket_t150013987_il2cpp_TypeInfo_var;
extern TypeInfo* IPEndPoint_t1265996582_il2cpp_TypeInfo_var;
extern const uint32_t ConnectAsyncExtension_SocketConnectCompleted_m1655121833_MetadataUsageId;
extern "C"  void ConnectAsyncExtension_SocketConnectCompleted_m1655121833 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectAsyncExtension_SocketConnectCompleted_m1655121833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DnsConnectState_t2343074160 * V_0 = NULL;
	Socket_t150013987 * V_1 = NULL;
	IPAddress_t3220500535 * V_2 = NULL;
	{
		SocketAsyncEventArgs_t970431102 * L_0 = ___e;
		NullCheck(L_0);
		Il2CppObject * L_1 = SocketAsyncEventArgs_get_UserToken_m1440215414(L_0, /*hidden argument*/NULL);
		V_0 = ((DnsConnectState_t2343074160 *)IsInstClass(L_1, DnsConnectState_t2343074160_il2cpp_TypeInfo_var));
		SocketAsyncEventArgs_t970431102 * L_2 = ___e;
		NullCheck(L_2);
		int32_t L_3 = SocketAsyncEventArgs_get_SocketError_m3030139504(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0033;
		}
	}
	{
		SocketAsyncEventArgs_t970431102 * L_4 = ___e;
		ConnectAsyncExtension_ClearSocketAsyncEventArgs_m3348425600(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_5 = V_0;
		NullCheck(L_5);
		ConnectedCallback_t1584521517 * L_6 = DnsConnectState_get_Callback_m816038482(L_5, /*hidden argument*/NULL);
		Il2CppObject * L_7 = ___sender;
		DnsConnectState_t2343074160 * L_8 = V_0;
		NullCheck(L_8);
		Il2CppObject * L_9 = DnsConnectState_get_State_m113258909(L_8, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_10 = ___e;
		NullCheck(L_6);
		ConnectedCallback_Invoke_m3294233759(L_6, ((Socket_t150013987 *)CastclassClass(L_7, Socket_t150013987_il2cpp_TypeInfo_var)), L_9, L_10, /*hidden argument*/NULL);
		return;
	}

IL_0033:
	{
		SocketAsyncEventArgs_t970431102 * L_11 = ___e;
		NullCheck(L_11);
		int32_t L_12 = SocketAsyncEventArgs_get_SocketError_m3030139504(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_12) == ((int32_t)((int32_t)10065))))
		{
			goto IL_0067;
		}
	}
	{
		SocketAsyncEventArgs_t970431102 * L_13 = ___e;
		NullCheck(L_13);
		int32_t L_14 = SocketAsyncEventArgs_get_SocketError_m3030139504(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_14) == ((int32_t)((int32_t)10061))))
		{
			goto IL_0067;
		}
	}
	{
		SocketAsyncEventArgs_t970431102 * L_15 = ___e;
		ConnectAsyncExtension_ClearSocketAsyncEventArgs_m3348425600(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_16 = V_0;
		NullCheck(L_16);
		ConnectedCallback_t1584521517 * L_17 = DnsConnectState_get_Callback_m816038482(L_16, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_18 = V_0;
		NullCheck(L_18);
		Il2CppObject * L_19 = DnsConnectState_get_State_m113258909(L_18, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_20 = ___e;
		NullCheck(L_17);
		ConnectedCallback_Invoke_m3294233759(L_17, (Socket_t150013987 *)NULL, L_19, L_20, /*hidden argument*/NULL);
		return;
	}

IL_0067:
	{
		DnsConnectState_t2343074160 * L_21 = V_0;
		IPAddress_t3220500535 * L_22 = ConnectAsyncExtension_GetNextAddress_m2210336608(NULL /*static, unused*/, L_21, (&V_1), /*hidden argument*/NULL);
		V_2 = L_22;
		IPAddress_t3220500535 * L_23 = V_2;
		if (L_23)
		{
			goto IL_0098;
		}
	}
	{
		SocketAsyncEventArgs_t970431102 * L_24 = ___e;
		ConnectAsyncExtension_ClearSocketAsyncEventArgs_m3348425600(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_25 = ___e;
		NullCheck(L_25);
		SocketAsyncEventArgs_set_SocketError_m4002991197(L_25, ((int32_t)10065), /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_26 = V_0;
		NullCheck(L_26);
		ConnectedCallback_t1584521517 * L_27 = DnsConnectState_get_Callback_m816038482(L_26, /*hidden argument*/NULL);
		DnsConnectState_t2343074160 * L_28 = V_0;
		NullCheck(L_28);
		Il2CppObject * L_29 = DnsConnectState_get_State_m113258909(L_28, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_30 = ___e;
		NullCheck(L_27);
		ConnectedCallback_Invoke_m3294233759(L_27, (Socket_t150013987 *)NULL, L_29, L_30, /*hidden argument*/NULL);
		return;
	}

IL_0098:
	{
		SocketAsyncEventArgs_t970431102 * L_31 = ___e;
		IPAddress_t3220500535 * L_32 = V_2;
		DnsConnectState_t2343074160 * L_33 = V_0;
		NullCheck(L_33);
		int32_t L_34 = DnsConnectState_get_Port_m30529900(L_33, /*hidden argument*/NULL);
		IPEndPoint_t1265996582 * L_35 = (IPEndPoint_t1265996582 *)il2cpp_codegen_object_new(IPEndPoint_t1265996582_il2cpp_TypeInfo_var);
		IPEndPoint__ctor_m613724246(L_35, L_32, L_34, /*hidden argument*/NULL);
		NullCheck(L_31);
		SocketAsyncEventArgs_set_RemoteEndPoint_m1360575983(L_31, L_35, /*hidden argument*/NULL);
		Socket_t150013987 * L_36 = V_1;
		SocketAsyncEventArgs_t970431102 * L_37 = ___e;
		NullCheck(L_36);
		bool L_38 = Socket_ConnectAsync_m327318847(L_36, L_37, /*hidden argument*/NULL);
		if (L_38)
		{
			goto IL_00ba;
		}
	}
	{
		Socket_t150013987 * L_39 = V_1;
		SocketAsyncEventArgs_t970431102 * L_40 = ___e;
		ConnectAsyncExtension_SocketConnectCompleted_m1655121833(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::ClearSocketAsyncEventArgs(System.Net.Sockets.SocketAsyncEventArgs)
extern TypeInfo* EventHandler_1_t1813008745_il2cpp_TypeInfo_var;
extern const MethodInfo* ConnectAsyncExtension_SocketConnectCompleted_m1655121833_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3542837379_MethodInfo_var;
extern const uint32_t ConnectAsyncExtension_ClearSocketAsyncEventArgs_m3348425600_MetadataUsageId;
extern "C"  void ConnectAsyncExtension_ClearSocketAsyncEventArgs_m3348425600 (Il2CppObject * __this /* static, unused */, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectAsyncExtension_ClearSocketAsyncEventArgs_m3348425600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SocketAsyncEventArgs_t970431102 * L_0 = ___e;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)ConnectAsyncExtension_SocketConnectCompleted_m1655121833_MethodInfo_var);
		EventHandler_1_t1813008745 * L_2 = (EventHandler_1_t1813008745 *)il2cpp_codegen_object_new(EventHandler_1_t1813008745_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m3542837379(L_2, NULL, L_1, /*hidden argument*/EventHandler_1__ctor_m3542837379_MethodInfo_var);
		NullCheck(L_0);
		SocketAsyncEventArgs_remove_Completed_m3312204008(L_0, L_2, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_3 = ___e;
		NullCheck(L_3);
		SocketAsyncEventArgs_set_UserToken_m3057049639(L_3, NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::ConnectAsync(System.Net.EndPoint,SuperSocket.ClientEngine.ConnectedCallback,System.Object)
extern "C"  void ConnectAsyncExtension_ConnectAsync_m2701800175 (Il2CppObject * __this /* static, unused */, EndPoint_t1294049535 * ___remoteEndPoint, ConnectedCallback_t1584521517 * ___callback, Il2CppObject * ___state, const MethodInfo* method)
{
	{
		EndPoint_t1294049535 * L_0 = ___remoteEndPoint;
		ConnectedCallback_t1584521517 * L_1 = ___callback;
		Il2CppObject * L_2 = ___state;
		ConnectAsyncExtension_ConnectAsyncInternal_m3327551872(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::CreateAttempSocket(SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState)
extern TypeInfo* Socket_t150013987_il2cpp_TypeInfo_var;
extern const uint32_t ConnectAsyncExtension_CreateAttempSocket_m1410490880_MetadataUsageId;
extern "C"  void ConnectAsyncExtension_CreateAttempSocket_m1410490880 (Il2CppObject * __this /* static, unused */, DnsConnectState_t2343074160 * ___connectState, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectAsyncExtension_CreateAttempSocket_m1410490880_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Socket_t150013987_il2cpp_TypeInfo_var);
		bool L_0 = Socket_get_OSSupportsIPv6_m51913357(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		DnsConnectState_t2343074160 * L_1 = ___connectState;
		Socket_t150013987 * L_2 = (Socket_t150013987 *)il2cpp_codegen_object_new(Socket_t150013987_il2cpp_TypeInfo_var);
		Socket__ctor_m3306971388(L_2, ((int32_t)23), 1, 6, /*hidden argument*/NULL);
		NullCheck(L_1);
		DnsConnectState_set_Socket6_m934655665(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		DnsConnectState_t2343074160 * L_3 = ___connectState;
		Socket_t150013987 * L_4 = (Socket_t150013987 *)il2cpp_codegen_object_new(Socket_t150013987_il2cpp_TypeInfo_var);
		Socket__ctor_m3306971388(L_4, 2, 1, 6, /*hidden argument*/NULL);
		NullCheck(L_3);
		DnsConnectState_set_Socket4_m1444128755(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SuperSocket.ClientEngine.ConnectAsyncExtension/ConnectToken::get_State()
extern "C"  Il2CppObject * ConnectToken_get_State_m3260382870 (ConnectToken_t2068031311 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CStateU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/ConnectToken::set_State(System.Object)
extern "C"  void ConnectToken_set_State_m3531158087 (ConnectToken_t2068031311 * __this, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value;
		__this->set_U3CStateU3Ek__BackingField_0(L_0);
		return;
	}
}
// SuperSocket.ClientEngine.ConnectedCallback SuperSocket.ClientEngine.ConnectAsyncExtension/ConnectToken::get_Callback()
extern "C"  ConnectedCallback_t1584521517 * ConnectToken_get_Callback_m655785259 (ConnectToken_t2068031311 * __this, const MethodInfo* method)
{
	{
		ConnectedCallback_t1584521517 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/ConnectToken::set_Callback(SuperSocket.ClientEngine.ConnectedCallback)
extern "C"  void ConnectToken_set_Callback_m1292756542 (ConnectToken_t2068031311 * __this, ConnectedCallback_t1584521517 * ___value, const MethodInfo* method)
{
	{
		ConnectedCallback_t1584521517 * L_0 = ___value;
		__this->set_U3CCallbackU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/ConnectToken::.ctor()
extern "C"  void ConnectToken__ctor_m805112283 (ConnectToken_t2068031311 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Net.IPAddress[] SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_Addresses()
extern "C"  IPAddressU5BU5D_t3494006030* DnsConnectState_get_Addresses_m1195483209 (DnsConnectState_t2343074160 * __this, const MethodInfo* method)
{
	{
		IPAddressU5BU5D_t3494006030* L_0 = __this->get_U3CAddressesU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_Addresses(System.Net.IPAddress[])
extern "C"  void DnsConnectState_set_Addresses_m1679752656 (DnsConnectState_t2343074160 * __this, IPAddressU5BU5D_t3494006030* ___value, const MethodInfo* method)
{
	{
		IPAddressU5BU5D_t3494006030* L_0 = ___value;
		__this->set_U3CAddressesU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_NextAddressIndex()
extern "C"  int32_t DnsConnectState_get_NextAddressIndex_m3972185532 (DnsConnectState_t2343074160 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CNextAddressIndexU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_NextAddressIndex(System.Int32)
extern "C"  void DnsConnectState_set_NextAddressIndex_m2086294711 (DnsConnectState_t2343074160 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CNextAddressIndexU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_Port()
extern "C"  int32_t DnsConnectState_get_Port_m30529900 (DnsConnectState_t2343074160 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CPortU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_Port(System.Int32)
extern "C"  void DnsConnectState_set_Port_m3846838119 (DnsConnectState_t2343074160 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CPortU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Net.Sockets.Socket SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_Socket4()
extern "C"  Socket_t150013987 * DnsConnectState_get_Socket4_m3757121632 (DnsConnectState_t2343074160 * __this, const MethodInfo* method)
{
	{
		Socket_t150013987 * L_0 = __this->get_U3CSocket4U3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_Socket4(System.Net.Sockets.Socket)
extern "C"  void DnsConnectState_set_Socket4_m1444128755 (DnsConnectState_t2343074160 * __this, Socket_t150013987 * ___value, const MethodInfo* method)
{
	{
		Socket_t150013987 * L_0 = ___value;
		__this->set_U3CSocket4U3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Net.Sockets.Socket SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_Socket6()
extern "C"  Socket_t150013987 * DnsConnectState_get_Socket6_m3757123554 (DnsConnectState_t2343074160 * __this, const MethodInfo* method)
{
	{
		Socket_t150013987 * L_0 = __this->get_U3CSocket6U3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_Socket6(System.Net.Sockets.Socket)
extern "C"  void DnsConnectState_set_Socket6_m934655665 (DnsConnectState_t2343074160 * __this, Socket_t150013987 * ___value, const MethodInfo* method)
{
	{
		Socket_t150013987 * L_0 = ___value;
		__this->set_U3CSocket6U3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Object SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_State()
extern "C"  Il2CppObject * DnsConnectState_get_State_m113258909 (DnsConnectState_t2343074160 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CStateU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_State(System.Object)
extern "C"  void DnsConnectState_set_State_m885005398 (DnsConnectState_t2343074160 * __this, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value;
		__this->set_U3CStateU3Ek__BackingField_5(L_0);
		return;
	}
}
// SuperSocket.ClientEngine.ConnectedCallback SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_Callback()
extern "C"  ConnectedCallback_t1584521517 * DnsConnectState_get_Callback_m816038482 (DnsConnectState_t2343074160 * __this, const MethodInfo* method)
{
	{
		ConnectedCallback_t1584521517 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_Callback(SuperSocket.ClientEngine.ConnectedCallback)
extern "C"  void DnsConnectState_set_Callback_m2832258125 (DnsConnectState_t2343074160 * __this, ConnectedCallback_t1584521517 * ___value, const MethodInfo* method)
{
	{
		ConnectedCallback_t1584521517 * L_0 = ___value;
		__this->set_U3CCallbackU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::.ctor()
extern "C"  void DnsConnectState__ctor_m837116908 (DnsConnectState_t2343074160 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ConnectedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ConnectedCallback__ctor_m3119060258 (ConnectedCallback_t1584521517 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Void SuperSocket.ClientEngine.ConnectedCallback::Invoke(System.Net.Sockets.Socket,System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern "C"  void ConnectedCallback_Invoke_m3294233759 (ConnectedCallback_t1584521517 * __this, Socket_t150013987 * ___socket, Il2CppObject * ___state, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ConnectedCallback_Invoke_m3294233759((ConnectedCallback_t1584521517 *)__this->get_prev_9(),___socket, ___state, ___e, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Socket_t150013987 * ___socket, Il2CppObject * ___state, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___socket, ___state, ___e,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Socket_t150013987 * ___socket, Il2CppObject * ___state, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___socket, ___state, ___e,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___state, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___socket, ___state, ___e,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_ConnectedCallback_t1584521517(Il2CppObject* delegate, Socket_t150013987 * ___socket, Il2CppObject * ___state, SocketAsyncEventArgs_t970431102 * ___e)
{
	// Marshaling of parameter '___socket' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Net.Sockets.Socket'."));
}
// System.IAsyncResult SuperSocket.ClientEngine.ConnectedCallback::BeginInvoke(System.Net.Sockets.Socket,System.Object,System.Net.Sockets.SocketAsyncEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ConnectedCallback_BeginInvoke_m697442090 (ConnectedCallback_t1584521517 * __this, Socket_t150013987 * ___socket, Il2CppObject * ___state, SocketAsyncEventArgs_t970431102 * ___e, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___socket;
	__d_args[1] = ___state;
	__d_args[2] = ___e;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void SuperSocket.ClientEngine.ConnectedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ConnectedCallback_EndInvoke_m1095264934 (ConnectedCallback_t1584521517 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void SuperSocket.ClientEngine.Extensions::.cctor()
extern TypeInfo* Random_t922188920_il2cpp_TypeInfo_var;
extern TypeInfo* Extensions_t80775955_il2cpp_TypeInfo_var;
extern const uint32_t Extensions__cctor_m652573973_MetadataUsageId;
extern "C"  void Extensions__cctor_m652573973 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Extensions__cctor_m652573973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Random_t922188920 * L_0 = (Random_t922188920 *)il2cpp_codegen_object_new(Random_t922188920_il2cpp_TypeInfo_var);
		Random__ctor_m2490522898(L_0, /*hidden argument*/NULL);
		((Extensions_t80775955_StaticFields*)Extensions_t80775955_il2cpp_TypeInfo_var->static_fields)->set_m_Random_0(L_0);
		return;
	}
}
// System.Boolean SuperSocket.ClientEngine.ProxyEventArgs::get_Connected()
extern "C"  bool ProxyEventArgs_get_Connected_m3880444133 (ProxyEventArgs_t3602520776 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CConnectedU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Net.Sockets.Socket SuperSocket.ClientEngine.ProxyEventArgs::get_Socket()
extern "C"  Socket_t150013987 * ProxyEventArgs_get_Socket_m476433455 (ProxyEventArgs_t3602520776 * __this, const MethodInfo* method)
{
	{
		Socket_t150013987 * L_0 = __this->get_U3CSocketU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Exception SuperSocket.ClientEngine.ProxyEventArgs::get_Exception()
extern "C"  Exception_t1967233988 * ProxyEventArgs_get_Exception_m210430180 (ProxyEventArgs_t3602520776 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = __this->get_U3CExceptionU3Ek__BackingField_3();
		return L_0;
	}
}
// System.String System.Net.DnsEndPoint::get_Host()
extern "C"  String_t* DnsEndPoint_get_Host_m1042524665 (DnsEndPoint_t1814926600 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CHostU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void System.Net.DnsEndPoint::set_Host(System.String)
extern "C"  void DnsEndPoint_set_Host_m3137319876 (DnsEndPoint_t1814926600 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CHostU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 System.Net.DnsEndPoint::get_Port()
extern "C"  int32_t DnsEndPoint_get_Port_m2270576877 (DnsEndPoint_t1814926600 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CPortU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void System.Net.DnsEndPoint::set_Port(System.Int32)
extern "C"  void DnsEndPoint_set_Port_m240530280 (DnsEndPoint_t1814926600 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CPortU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void System.Net.DnsEndPoint::.ctor(System.String,System.Int32)
extern "C"  void DnsEndPoint__ctor_m1299091296 (DnsEndPoint_t1814926600 * __this, String_t* ___host, int32_t ___port, const MethodInfo* method)
{
	{
		EndPoint__ctor_m4041742149(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___host;
		DnsEndPoint_set_Host_m3137319876(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___port;
		DnsEndPoint_set_Port_m240530280(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
