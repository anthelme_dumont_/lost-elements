﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<WebSocket4Net.Protocol.WebSocketDataFrame>
struct IList_1_t2389226120;
// WebSocket4Net.Protocol.WebSocketDataFrame
struct WebSocketDataFrame_t222733806;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_WebSocketDataF222733806.h"

// System.Void WebSocket4Net.WebSocketCommandInfo::.ctor()
extern "C"  void WebSocketCommandInfo__ctor_m135330605 (WebSocketCommandInfo_t3536916738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocketCommandInfo::.ctor(System.String)
extern "C"  void WebSocketCommandInfo__ctor_m2432496885 (WebSocketCommandInfo_t3536916738 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocketCommandInfo::.ctor(System.String,System.String)
extern "C"  void WebSocketCommandInfo__ctor_m1488440433 (WebSocketCommandInfo_t3536916738 * __this, String_t* ___key, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocketCommandInfo::.ctor(System.Collections.Generic.IList`1<WebSocket4Net.Protocol.WebSocketDataFrame>)
extern "C"  void WebSocketCommandInfo__ctor_m2019461544 (WebSocketCommandInfo_t3536916738 * __this, Il2CppObject* ___frames, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocketCommandInfo::.ctor(WebSocket4Net.Protocol.WebSocketDataFrame)
extern "C"  void WebSocketCommandInfo__ctor_m784170289 (WebSocketCommandInfo_t3536916738 * __this, WebSocketDataFrame_t222733806 * ___frame, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.WebSocketCommandInfo::get_Key()
extern "C"  String_t* WebSocketCommandInfo_get_Key_m2073169022 (WebSocketCommandInfo_t3536916738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocketCommandInfo::set_Key(System.String)
extern "C"  void WebSocketCommandInfo_set_Key_m3535408597 (WebSocketCommandInfo_t3536916738 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocket4Net.WebSocketCommandInfo::get_Data()
extern "C"  ByteU5BU5D_t58506160* WebSocketCommandInfo_get_Data_m4129809812 (WebSocketCommandInfo_t3536916738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocketCommandInfo::set_Data(System.Byte[])
extern "C"  void WebSocketCommandInfo_set_Data_m2974951275 (WebSocketCommandInfo_t3536916738 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.WebSocketCommandInfo::get_Text()
extern "C"  String_t* WebSocketCommandInfo_get_Text_m101435856 (WebSocketCommandInfo_t3536916738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocketCommandInfo::set_Text(System.String)
extern "C"  void WebSocketCommandInfo_set_Text_m425876161 (WebSocketCommandInfo_t3536916738 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocket4Net.WebSocketCommandInfo::get_CloseStatusCode()
extern "C"  int32_t WebSocketCommandInfo_get_CloseStatusCode_m2849713255 (WebSocketCommandInfo_t3536916738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocketCommandInfo::set_CloseStatusCode(System.Int32)
extern "C"  void WebSocketCommandInfo_set_CloseStatusCode_m1865797974 (WebSocketCommandInfo_t3536916738 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocket4Net.WebSocketCommandInfo::<.ctor>b__0(WebSocket4Net.Protocol.WebSocketDataFrame)
extern "C"  int32_t WebSocketCommandInfo_U3C_ctorU3Eb__0_m1673743135 (Il2CppObject * __this /* static, unused */, WebSocketDataFrame_t222733806 * ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
