﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "SimpleJson_SimpleJson_Reflection_SafeDictionary_2_1091022501MethodDeclarations.h"

// System.Boolean SimpleJson.Reflection.SafeDictionary`2<System.Type,SimpleJson.Reflection.CacheResolver/CtorDelegate>::TryGetValue(TKey,TValue&)
#define SafeDictionary_2_TryGetValue_m1544795075(__this, ___key, ___value, method) ((  bool (*) (SafeDictionary_2_t2099217731 *, Type_t *, CtorDelegate_t2720072665 **, const MethodInfo*))SafeDictionary_2_TryGetValue_m2241969233_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.SafeDictionary`2<System.Type,SimpleJson.Reflection.CacheResolver/CtorDelegate>::GetEnumerator()
#define SafeDictionary_2_GetEnumerator_m2457941448(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t2099217731 *, const MethodInfo*))SafeDictionary_2_GetEnumerator_m3507230980_gshared)(__this, method)
// System.Void SimpleJson.Reflection.SafeDictionary`2<System.Type,SimpleJson.Reflection.CacheResolver/CtorDelegate>::Add(TKey,TValue)
#define SafeDictionary_2_Add_m2117596466(__this, ___key, ___value, method) ((  void (*) (SafeDictionary_2_t2099217731 *, Type_t *, CtorDelegate_t2720072665 *, const MethodInfo*))SafeDictionary_2_Add_m3862242812_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.SafeDictionary`2<System.Type,SimpleJson.Reflection.CacheResolver/CtorDelegate>::.ctor()
#define SafeDictionary_2__ctor_m1982222051(__this, method) ((  void (*) (SafeDictionary_2_t2099217731 *, const MethodInfo*))SafeDictionary_2__ctor_m3700110041_gshared)(__this, method)
