﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// WebSocket4Net.Command.BadRequest
struct BadRequest_t1215901385;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t3650470111;
// System.Object
struct Il2CppObject;
// WebSocket4Net.Command.Binary
struct Binary_t1572461568;
// WebSocket4Net.Command.Close
struct Close_t3942764983;
// WebSocket4Net.Command.Handshake
struct Handshake_t1657241526;
// WebSocket4Net.Command.Ping
struct Ping_t3880049009;
// WebSocket4Net.Command.Pong
struct Pong_t3880054775;
// WebSocket4Net.Command.Text
struct Text_t3880164652;
// WebSocket4Net.Command.WebSocketCommandBase
struct WebSocketCommandBase_t3263712852;
// WebSocket4Net.DataReceivedEventArgs
struct DataReceivedEventArgs_t1979092924;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// WebSocket4Net.MessageReceivedEventArgs
struct MessageReceivedEventArgs_t3871897919;
// WebSocket4Net.Protocol.CloseStatusCodeHybi10
struct CloseStatusCodeHybi10_t635449120;
// WebSocket4Net.Protocol.CloseStatusCodeRfc6455
struct CloseStatusCodeRfc6455_t1268471848;
// WebSocket4Net.Protocol.DraftHybi00DataReader
struct DraftHybi00DataReader_t1509845496;
// WebSocket4Net.Protocol.ReaderBase
struct ReaderBase_t893022310;
// WebSocket4Net.Protocol.DraftHybi00HandshakeReader
struct DraftHybi00HandshakeReader_t1765220979;
// WebSocket4Net.Protocol.DraftHybi00Processor
struct DraftHybi00Processor_t470131883;
// System.Collections.Generic.IEnumerable`1<System.Char>
struct IEnumerable_1_t1355893759;
// System.Func`2<System.Char,System.Boolean>
struct Func_2_t620049934;
// WebSocket4Net.Protocol.DraftHybi10DataReader
struct DraftHybi10DataReader_t1638928215;
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>
struct IClientCommandReader_1_t3175310917;
// SuperSocket.ClientEngine.Protocol.ArraySegmentList
struct ArraySegmentList_t1783467835;
// WebSocket4Net.Protocol.DraftHybi10HandshakeReader
struct DraftHybi10HandshakeReader_t3118530676;
// WebSocket4Net.Protocol.DraftHybi10Processor
struct DraftHybi10Processor_t2968147820;
// WebSocket4Net.Protocol.ICloseStatusCode
struct ICloseStatusCode_t28582816;
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader
struct IDataFramePartReader_t2102555929;
// WebSocket4Net.Protocol.FramePartReader.DataFramePartReader
struct DataFramePartReader_t1881953282;
// WebSocket4Net.Protocol.FramePartReader.ExtendedLenghtReader
struct ExtendedLenghtReader_t3744404771;
// WebSocket4Net.Protocol.WebSocketDataFrame
struct WebSocketDataFrame_t222733806;
// WebSocket4Net.Protocol.FramePartReader.FixPartReader
struct FixPartReader_t3993251252;
// WebSocket4Net.Protocol.FramePartReader.MaskKeyReader
struct MaskKeyReader_t360839071;
// WebSocket4Net.Protocol.FramePartReader.PayloadDataReader
struct PayloadDataReader_t544241252;
// WebSocket4Net.Protocol.HandshakeReader
struct HandshakeReader_t1078082956;
// System.Collections.Generic.IList`1<System.Byte>
struct IList_1_t650218839;
// SuperSocket.ClientEngine.SearchMarkState`1<System.Byte>
struct SearchMarkState_1_t304232942;
// WebSocket4Net.Protocol.ProtocolProcessorBase
struct ProtocolProcessorBase_t894595261;
// WebSocket4Net.Protocol.ProtocolProcessorFactory
struct ProtocolProcessorFactory_t3499308290;
// WebSocket4Net.Protocol.IProtocolProcessor[]
struct IProtocolProcessorU5BU5D_t2571124850;
// System.Linq.IOrderedEnumerable`1<WebSocket4Net.Protocol.IProtocolProcessor>
struct IOrderedEnumerable_1_t369942714;
// System.Collections.Generic.IEnumerable`1<WebSocket4Net.Protocol.IProtocolProcessor>
struct IEnumerable_1_t2785343127;
// System.Func`2<WebSocket4Net.Protocol.IProtocolProcessor,System.Int32>
struct Func_2_t2410725500;
// System.Linq.IOrderedEnumerable`1<System.Object>
struct IOrderedEnumerable_1_t1293860363;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t4146091719;
// WebSocket4Net.Protocol.IProtocolProcessor
struct IProtocolProcessor_t4208156067;
// System.Func`2<WebSocket4Net.Protocol.IProtocolProcessor,System.Boolean>
struct Func_2_t4069283350;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// System.Linq.IOrderedEnumerable`1<System.Int32>
struct IOrderedEnumerable_1_t3304168730;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1424601847;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_t1649583772;
// WebSocket4Net.Protocol.ProtocolProcessorFactory/<>c__DisplayClass3
struct U3CU3Ec__DisplayClass3_t2826400220;
// WebSocket4Net.Protocol.Rfc6455Processor
struct Rfc6455Processor_t2702576183;
// SuperSocket.ClientEngine.TcpClientSession
struct TcpClientSession_t1301539049;
// System.Uri
struct Uri_t2776692961;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct List_1_t2891677073;
// SuperSocket.ClientEngine.IProxyConnector
struct IProxyConnector_t1374585095;
// System.Net.EndPoint
struct EndPoint_t1294049535;
// SuperSocket.ClientEngine.DataEventArgs
struct DataEventArgs_t3216211148;
// SuperSocket.ClientEngine.ErrorEventArgs
struct ErrorEventArgs_t1329266798;
// System.EventArgs
struct EventArgs_t516466188;
// System.EventHandler
struct EventHandler_t247020293;
// System.EventHandler`1<WebSocket4Net.MessageReceivedEventArgs>
struct EventHandler_1_t419508266;
// System.Exception
struct Exception_t1967233988;
// System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>
struct EventHandler_1_t2171844441;
// System.Collections.Generic.IList`1<WebSocket4Net.Protocol.WebSocketDataFrame>
struct IList_1_t2389226120;
// System.Collections.Generic.IEnumerable`1<WebSocket4Net.Protocol.WebSocketDataFrame>
struct IEnumerable_1_t3094888162;
// System.Func`2<WebSocket4Net.Protocol.WebSocketDataFrame,System.Int32>
struct Func_2_t424480421;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "WebSocket4Net_U3CModuleU3E86524790.h"
#include "WebSocket4Net_U3CModuleU3E86524790MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Command_BadRequest1215901385.h"
#include "WebSocket4Net_WebSocket4Net_Command_BadRequest1215901385MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketCommandInfo3536916738.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_StringComparer4058118931MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketCommandInfo3536916738MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Extensions39620836MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_StringComparer4058118931.h"
#include "WebSocket4Net_WebSocket4Net_Extensions39620836.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_StringSplitOptions3963075722.h"
#include "WebSocket4Net_WebSocket4Net_Command_WebSocketComma3263712852MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Command_Binary1572461568.h"
#include "WebSocket4Net_WebSocket4Net_Command_Binary1572461568MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821.h"
#include "WebSocket4Net_WebSocket4Net_Command_Close3942764983.h"
#include "WebSocket4Net_WebSocket4Net_Command_Close3942764983MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketState1654176186.h"
#include "WebSocket4Net_WebSocket4Net_Command_Handshake1657241526.h"
#include "WebSocket4Net_WebSocket4Net_Command_Handshake1657241526MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Command_Ping3880049009.h"
#include "WebSocket4Net_WebSocket4Net_Command_Ping3880049009MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "WebSocket4Net_WebSocket4Net_Command_Pong3880054775.h"
#include "WebSocket4Net_WebSocket4Net_Command_Pong3880054775MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Command_Text3880164652.h"
#include "WebSocket4Net_WebSocket4Net_Command_Text3880164652MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Command_WebSocketComma3263712852.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_DataReceivedEventArgs1979092924.h"
#include "WebSocket4Net_WebSocket4Net_DataReceivedEventArgs1979092924MethodDeclarations.h"
#include "mscorlib_System_EventArgs516466188MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_IO_StringReader2229325051MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader2229325051.h"
#include "mscorlib_System_IO_TextReader1534522647MethodDeclarations.h"
#include "mscorlib_System_IO_TextReader1534522647.h"
#include "WebSocket4Net_WebSocket4Net_MessageReceivedEventAr3871897919.h"
#include "WebSocket4Net_WebSocket4Net_MessageReceivedEventAr3871897919MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_CloseStatusCod635449120.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_CloseStatusCod635449120MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_CloseStatusCo1268471848.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_CloseStatusCo1268471848MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_DraftHybi00Da1509845496.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_DraftHybi00Da1509845496MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_ReaderBase893022310.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_ReaderBase893022310MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1369764433MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding180559927MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Client27518574MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Clie1783467835MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1438485399MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1369764433.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Clie1783467835.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Client27518574.h"
#include "mscorlib_System_Text_Encoding180559927.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_DraftHybi00Ha1765220979.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_DraftHybi00Ha1765220979MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_HandshakeRead1078082956MethodDeclarations.h"
#include "mscorlib_System_Buffer482356213MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_HandshakeRead1078082956.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_DraftHybi00Pro470131883.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_DraftHybi00Pro470131883MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_ProtocolProces894595261MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketVersion3981039553.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3575665668MethodDeclarations.h"
#include "mscorlib_System_Random922188920MethodDeclarations.h"
#include "mscorlib_System_Char2778706699MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3575665668.h"
#include "mscorlib_System_Random922188920.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1301539049.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn3100954378MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn3100954378.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "System_System_Uri2776692961MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2891677073.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104.h"
#include "System_System_Uri2776692961.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2891677073MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Regex3802381858MethodDeclarations.h"
#include "mscorlib_System_Int642847414882MethodDeclarations.h"
#include "System_Core_System_Func_2_gen620049934MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "mscorlib_System_BitConverter3338308296MethodDeclarations.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_MD51557559991MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorithm24372250MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "System_Core_System_Func_2_gen620049934.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "mscorlib_System_Security_Cryptography_MD51557559991.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_ClientEn80775955MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_ClientEn80775955.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_DraftHybi10Da1638928215.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_DraftHybi10Da1638928215MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_WebSocketDataF222733806MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_FramePartRead1881953282MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_WebSocketDataF222733806.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1019692775MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1019692775.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_DraftHybi10Ha3118530676.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_DraftHybi10Ha3118530676MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_DraftHybi10Pr2968147820.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_DraftHybi10Pr2968147820MethodDeclarations.h"
#include "mscorlib_System_Guid2778838590MethodDeclarations.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SHA11560027742MethodDeclarations.h"
#include "mscorlib_System_Guid2778838590.h"
#include "mscorlib_System_Security_Cryptography_SHA11560027742.h"
#include "mscorlib_System_StringComparison1653470895.h"
#include "mscorlib_System_Math2778998461MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_FramePartRead1881953282.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_FramePartRead3993251252MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_FramePartRead3744404771MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_FramePartReade360839071MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_FramePartReade544241252MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_FramePartRead3993251252.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_FramePartRead3744404771.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_FramePartReade360839071.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_FramePartReade544241252.h"
#include "mscorlib_System_SByte2855346064.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_ClientE304232942MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_ClientE304232942.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_ProtocolProces894595261.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_ProtocolProce3499308290.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_ProtocolProce3499308290MethodDeclarations.h"
#include "WebSocket4Net_ArrayTypes.h"
#include "System_Core_System_Func_2_gen2410725500MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2410725500.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_ProtocolProce2826400220MethodDeclarations.h"
#include "System_Core_System_Func_2_gen4069283350MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_ProtocolProce2826400220.h"
#include "System_Core_System_Func_2_gen4069283350.h"
#include "System_Core_System_Func_2_gen1649583772MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1649583772.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_Rfc6455Proces2702576183.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_Rfc6455Proces2702576183MethodDeclarations.h"
#include "System_System_Net_EndPoint1294049535.h"
#include "System_System_Net_IPAddress3220500535MethodDeclarations.h"
#include "System_System_Net_IPEndPoint1265996582MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_System_Net_DnsEndP1814926600MethodDeclarations.h"
#include "System_System_Net_IPAddress3220500535.h"
#include "System_System_Net_IPEndPoint1265996582.h"
#include "SuperSocket_ClientEngine_Common_System_Net_DnsEndP1814926600.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn2860571088MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn2860571088.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn2680237056MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn2680237056.h"
#include "mscorlib_System_EventHandler247020293MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen2171844441MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen4058788791MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2262919787.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2262919787MethodDeclarations.h"
#include "mscorlib_System_EventArgs516466188.h"
#include "mscorlib_System_EventHandler247020293.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1329266798.h"
#include "mscorlib_System_EventHandler_1_gen2171844441.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn3216211148.h"
#include "mscorlib_System_EventHandler_1_gen4058788791.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn3216211148MethodDeclarations.h"
#include "mscorlib_System_Threading_TimerCallback4291881837MethodDeclarations.h"
#include "mscorlib_System_Threading_Timer3546110984MethodDeclarations.h"
#include "mscorlib_System_Threading_TimerCallback4291881837.h"
#include "mscorlib_System_Threading_Timer3546110984.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "mscorlib_System_EventHandler_1_gen419508266.h"
#include "mscorlib_System_EventHandler_1_gen419508266MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen2821670567MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen2821670567.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1329266798MethodDeclarations.h"
#include "mscorlib_System_SByte2855346064MethodDeclarations.h"
#include "System_Core_System_Func_2_gen424480421MethodDeclarations.h"
#include "System_Core_System_Func_2_gen424480421.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketState1654176186MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketVersion3981039553MethodDeclarations.h"

// TValue WebSocket4Net.Extensions::GetValue<System.Object>(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,TValue)
extern "C"  Il2CppObject * Extensions_GetValue_TisIl2CppObject_m46373847_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___valueContainer, String_t* ___name, Il2CppObject * ___defaultValue, const MethodInfo* method);
#define Extensions_GetValue_TisIl2CppObject_m46373847(__this /* static, unused */, ___valueContainer, ___name, ___defaultValue, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, String_t*, Il2CppObject *, const MethodInfo*))Extensions_GetValue_TisIl2CppObject_m46373847_gshared)(__this /* static, unused */, ___valueContainer, ___name, ___defaultValue, method)
// TValue WebSocket4Net.Extensions::GetValue<System.String>(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,TValue)
#define Extensions_GetValue_TisString_t_m2902636613(__this /* static, unused */, ___valueContainer, ___name, ___defaultValue, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, String_t*, String_t*, const MethodInfo*))Extensions_GetValue_TisIl2CppObject_m46373847_gshared)(__this /* static, unused */, ___valueContainer, ___name, ___defaultValue, method)
// System.Int32 System.Linq.Enumerable::Count<System.Char>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  int32_t Enumerable_Count_TisChar_t2778706699_m2693248879_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t620049934 * p1, const MethodInfo* method);
#define Enumerable_Count_TisChar_t2778706699_m2693248879(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t620049934 *, const MethodInfo*))Enumerable_Count_TisChar_t2778706699_m2693248879_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] SuperSocket.ClientEngine.Extensions::RandomOrder<System.Byte>(!!0[])
extern "C"  ByteU5BU5D_t58506160* Extensions_RandomOrder_TisByte_t2778693821_m2917735209_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* p0, const MethodInfo* method);
#define Extensions_RandomOrder_TisByte_t2778693821_m2917735209(__this /* static, unused */, p0, method) ((  ByteU5BU5D_t58506160* (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t58506160*, const MethodInfo*))Extensions_RandomOrder_TisByte_t2778693821_m2917735209_gshared)(__this /* static, unused */, p0, method)
// System.Int32 SuperSocket.ClientEngine.Extensions::SearchMark<System.Byte>(System.Collections.Generic.IList`1<!!0>,System.Int32,System.Int32,SuperSocket.ClientEngine.SearchMarkState`1<!!0>)
extern "C"  int32_t Extensions_SearchMark_TisByte_t2778693821_m1179758205_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, int32_t p2, SearchMarkState_1_t304232942 * p3, const MethodInfo* method);
#define Extensions_SearchMark_TisByte_t2778693821_m1179758205(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, int32_t, SearchMarkState_1_t304232942 *, const MethodInfo*))Extensions_SearchMark_TisByte_t2778693821_m1179758205_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Linq.IOrderedEnumerable`1<!!0> System.Linq.Enumerable::OrderByDescending<System.Object,System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_OrderByDescending_TisIl2CppObject_TisInt32_t2847414787_m3932475116_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t4146091719 * p1, const MethodInfo* method);
#define Enumerable_OrderByDescending_TisIl2CppObject_TisInt32_t2847414787_m3932475116(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t4146091719 *, const MethodInfo*))Enumerable_OrderByDescending_TisIl2CppObject_TisInt32_t2847414787_m3932475116_gshared)(__this /* static, unused */, p0, p1, method)
// System.Linq.IOrderedEnumerable`1<!!0> System.Linq.Enumerable::OrderByDescending<WebSocket4Net.Protocol.IProtocolProcessor,System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_OrderByDescending_TisIProtocolProcessor_t4208156067_TisInt32_t2847414787_m2419427767(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2410725500 *, const MethodInfo*))Enumerable_OrderByDescending_TisIl2CppObject_TisInt32_t2847414787_m3932475116_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t11523773* Enumerable_ToArray_TisIl2CppObject_m235635230_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m235635230(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m235635230_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<WebSocket4Net.Protocol.IProtocolProcessor>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisIProtocolProcessor_t4208156067_m2091042025(__this /* static, unused */, p0, method) ((  IProtocolProcessorU5BU5D_t2571124850* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m235635230_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m1453134342_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisIl2CppObject_m1453134342(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_FirstOrDefault_TisIl2CppObject_m1453134342_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::FirstOrDefault<WebSocket4Net.Protocol.IProtocolProcessor>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_FirstOrDefault_TisIProtocolProcessor_t4208156067_m1442144529(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t4069283350 *, const MethodInfo*))Enumerable_FirstOrDefault_TisIl2CppObject_m1453134342_gshared)(__this /* static, unused */, p0, p1, method)
// System.Linq.IOrderedEnumerable`1<!!0> System.Linq.Enumerable::OrderByDescending<System.Int32,System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_OrderByDescending_TisInt32_t2847414787_TisInt32_t2847414787_m3192897055_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1649583772 * p1, const MethodInfo* method);
#define Enumerable_OrderByDescending_TisInt32_t2847414787_TisInt32_t2847414787_m3192897055(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1649583772 *, const MethodInfo*))Enumerable_OrderByDescending_TisInt32_t2847414787_TisInt32_t2847414787_m3192897055_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Linq.Enumerable::Sum<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Int32>)
extern "C"  int32_t Enumerable_Sum_TisIl2CppObject_m2024664314_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t4146091719 * p1, const MethodInfo* method);
#define Enumerable_Sum_TisIl2CppObject_m2024664314(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t4146091719 *, const MethodInfo*))Enumerable_Sum_TisIl2CppObject_m2024664314_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Linq.Enumerable::Sum<WebSocket4Net.Protocol.WebSocketDataFrame>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Int32>)
#define Enumerable_Sum_TisWebSocketDataFrame_t222733806_m828120080(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t424480421 *, const MethodInfo*))Enumerable_Sum_TisIl2CppObject_m2024664314_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebSocket4Net.Command.BadRequest::ExecuteCommand(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo)
extern TypeInfo* StringComparer_t4058118931_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern TypeInfo* Extensions_t39620836_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* BadRequest_t1215901385_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t1809983122_il2cpp_TypeInfo_var;
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m197076471_MethodInfo_var;
extern const MethodInfo* Extensions_GetValue_TisString_t_m2902636613_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2197799446;
extern Il2CppCodeGenString* _stringLiteral1877553114;
extern Il2CppCodeGenString* _stringLiteral3896542403;
extern Il2CppCodeGenString* _stringLiteral1315870903;
extern Il2CppCodeGenString* _stringLiteral2181653398;
extern const uint32_t BadRequest_ExecuteCommand_m1364508181_MetadataUsageId;
extern "C"  void BadRequest_ExecuteCommand_m1364508181 (BadRequest_t1215901385 * __this, WebSocket_t713846903 * ___session, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BadRequest_ExecuteCommand_m1364508181_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2474804324 * V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t2956870243* V_2 = NULL;
	Int32U5BU5D_t1809983122* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	bool V_6 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t4058118931_il2cpp_TypeInfo_var);
		StringComparer_t4058118931 * L_0 = StringComparer_get_OrdinalIgnoreCase_m2513153269(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t2474804324 * L_1 = (Dictionary_2_t2474804324 *)il2cpp_codegen_object_new(Dictionary_2_t2474804324_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m197076471(L_1, L_0, /*hidden argument*/Dictionary_2__ctor_m197076471_MethodInfo_var);
		V_0 = L_1;
		WebSocketCommandInfo_t3536916738 * L_2 = ___commandInfo;
		NullCheck(L_2);
		String_t* L_3 = WebSocketCommandInfo_get_Text_m101435856(L_2, /*hidden argument*/NULL);
		Dictionary_2_t2474804324 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_ParseMimeHeader_m1946375443(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Dictionary_2_t2474804324 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_7 = Extensions_GetValue_TisString_t_m2902636613(NULL /*static, unused*/, L_5, _stringLiteral2197799446, L_6, /*hidden argument*/Extensions_GetValue_TisString_t_m2902636613_MethodInfo_var);
		V_1 = L_7;
		WebSocket_t713846903 * L_8 = ___session;
		NullCheck(L_8);
		bool L_9 = WebSocket_get_NotSpecifiedVersion_m3038260680(L_8, /*hidden argument*/NULL);
		V_6 = L_9;
		bool L_10 = V_6;
		if (L_10)
		{
			goto IL_007c;
		}
	}
	{
		String_t* L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_6 = (bool)((((int32_t)L_12) == ((int32_t)0))? 1 : 0);
		bool L_13 = V_6;
		if (L_13)
		{
			goto IL_0059;
		}
	}
	{
		WebSocket_t713846903 * L_14 = ___session;
		Exception_t1967233988 * L_15 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_15, _stringLiteral1877553114, /*hidden argument*/NULL);
		NullCheck(L_14);
		WebSocket_FireError_m4124567456(L_14, L_15, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0059:
	{
		WebSocket_t713846903 * L_16 = ___session;
		String_t* L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3896542403, L_17, /*hidden argument*/NULL);
		Exception_t1967233988 * L_19 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_19, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		WebSocket_FireError_m4124567456(L_16, L_19, /*hidden argument*/NULL);
	}

IL_0070:
	{
		WebSocket_t713846903 * L_20 = ___session;
		NullCheck(L_20);
		WebSocket_CloseWithoutHandshake_m1514871525(L_20, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_007c:
	{
		String_t* L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_6 = (bool)((((int32_t)L_22) == ((int32_t)0))? 1 : 0);
		bool L_23 = V_6;
		if (L_23)
		{
			goto IL_00a9;
		}
	}
	{
		WebSocket_t713846903 * L_24 = ___session;
		Exception_t1967233988 * L_25 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_25, _stringLiteral1315870903, /*hidden argument*/NULL);
		NullCheck(L_24);
		WebSocket_FireError_m4124567456(L_24, L_25, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_26 = ___session;
		NullCheck(L_26);
		WebSocket_CloseWithoutHandshake_m1514871525(L_26, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_00a9:
	{
		String_t* L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(BadRequest_t1215901385_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_28 = ((BadRequest_t1215901385_StaticFields*)BadRequest_t1215901385_il2cpp_TypeInfo_var->static_fields)->get_m_ValueSeparator_0();
		NullCheck(L_27);
		StringU5BU5D_t2956870243* L_29 = String_Split_m459616251(L_27, L_28, 1, /*hidden argument*/NULL);
		V_2 = L_29;
		StringU5BU5D_t2956870243* L_30 = V_2;
		NullCheck(L_30);
		V_3 = ((Int32U5BU5D_t1809983122*)SZArrayNew(Int32U5BU5D_t1809983122_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))));
		V_4 = 0;
		goto IL_00fe;
	}

IL_00c4:
	{
		StringU5BU5D_t2956870243* L_31 = V_2;
		int32_t L_32 = V_4;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		bool L_34 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33))), (&V_5), /*hidden argument*/NULL);
		V_6 = L_34;
		bool L_35 = V_6;
		if (L_35)
		{
			goto IL_00f1;
		}
	}
	{
		WebSocket_t713846903 * L_36 = ___session;
		Exception_t1967233988 * L_37 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_37, _stringLiteral2181653398, /*hidden argument*/NULL);
		NullCheck(L_36);
		WebSocket_FireError_m4124567456(L_36, L_37, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_38 = ___session;
		NullCheck(L_38);
		WebSocket_CloseWithoutHandshake_m1514871525(L_38, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_00f1:
	{
		Int32U5BU5D_t1809983122* L_39 = V_3;
		int32_t L_40 = V_4;
		int32_t L_41 = V_5;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(L_40), (int32_t)L_41);
		int32_t L_42 = V_4;
		V_4 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_00fe:
	{
		int32_t L_43 = V_4;
		StringU5BU5D_t2956870243* L_44 = V_2;
		NullCheck(L_44);
		V_6 = (bool)((((int32_t)L_43) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_44)->max_length))))))? 1 : 0);
		bool L_45 = V_6;
		if (L_45)
		{
			goto IL_00c4;
		}
	}
	{
		WebSocket_t713846903 * L_46 = ___session;
		Int32U5BU5D_t1809983122* L_47 = V_3;
		NullCheck(L_46);
		bool L_48 = WebSocket_GetAvailableProcessor_m3999224164(L_46, L_47, /*hidden argument*/NULL);
		V_6 = L_48;
		bool L_49 = V_6;
		if (L_49)
		{
			goto IL_0133;
		}
	}
	{
		WebSocket_t713846903 * L_50 = ___session;
		Exception_t1967233988 * L_51 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_51, _stringLiteral1315870903, /*hidden argument*/NULL);
		NullCheck(L_50);
		WebSocket_FireError_m4124567456(L_50, L_51, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_52 = ___session;
		NullCheck(L_52);
		WebSocket_CloseWithoutHandshake_m1514871525(L_52, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_0133:
	{
		WebSocket_t713846903 * L_53 = ___session;
		NullCheck(L_53);
		Il2CppObject * L_54 = WebSocket_get_ProtocolProcessor_m1265601209(L_53, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_55 = ___session;
		NullCheck(L_54);
		InterfaceActionInvoker1< WebSocket_t713846903 * >::Invoke(0 /* System.Void WebSocket4Net.Protocol.IProtocolProcessor::SendHandshake(WebSocket4Net.WebSocket) */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_54, L_55);
	}

IL_0140:
	{
		return;
	}
}
// System.String WebSocket4Net.Command.BadRequest::get_Name()
extern "C"  String_t* BadRequest_get_Name_m2834877993 (BadRequest_t1215901385 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		V_1 = ((int32_t)400);
		String_t* L_0 = Int32_ToString_m1286526384((&V_1), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Command.BadRequest::.ctor()
extern "C"  void BadRequest__ctor_m2931563346 (BadRequest_t1215901385 * __this, const MethodInfo* method)
{
	{
		WebSocketCommandBase__ctor_m3945612519(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Command.BadRequest::.cctor()
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* BadRequest_t1215901385_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1396;
extern const uint32_t BadRequest__cctor_m202054299_MetadataUsageId;
extern "C"  void BadRequest__cctor_m202054299 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BadRequest__cctor_m202054299_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	{
		V_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)1));
		StringU5BU5D_t2956870243* L_0 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1396);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1396);
		StringU5BU5D_t2956870243* L_1 = V_0;
		((BadRequest_t1215901385_StaticFields*)BadRequest_t1215901385_il2cpp_TypeInfo_var->static_fields)->set_m_ValueSeparator_0(L_1);
		return;
	}
}
// System.Void WebSocket4Net.Command.Binary::ExecuteCommand(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo)
extern "C"  void Binary_ExecuteCommand_m979543116 (Binary_t1572461568 * __this, WebSocket_t713846903 * ___session, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method)
{
	{
		WebSocket_t713846903 * L_0 = ___session;
		WebSocketCommandInfo_t3536916738 * L_1 = ___commandInfo;
		NullCheck(L_1);
		ByteU5BU5D_t58506160* L_2 = WebSocketCommandInfo_get_Data_m4129809812(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		WebSocket_FireDataReceived_m2475134404(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String WebSocket4Net.Command.Binary::get_Name()
extern "C"  String_t* Binary_get_Name_m2304013792 (Binary_t1572461568 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		V_1 = 2;
		String_t* L_0 = Int32_ToString_m1286526384((&V_1), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Command.Binary::.ctor()
extern "C"  void Binary__ctor_m1144881275 (Binary_t1572461568 * __this, const MethodInfo* method)
{
	{
		WebSocketCommandBase__ctor_m3945612519(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Command.Close::ExecuteCommand(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo)
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern TypeInfo* ICloseStatusCode_t28582816_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2901091385;
extern const uint32_t Close_ExecuteCommand_m1102373333_MetadataUsageId;
extern "C"  void Close_ExecuteCommand_m1102373333 (Close_t3942764983 * __this, WebSocket_t713846903 * ___session, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Close_ExecuteCommand_m1102373333_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		WebSocket_t713846903 * L_0 = ___session;
		NullCheck(L_0);
		int32_t L_1 = WebSocket_get_State_m3481149631(L_0, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_1;
		if (L_2)
		{
			goto IL_007a;
		}
	}
	{
		WebSocketCommandInfo_t3536916738 * L_3 = ___commandInfo;
		NullCheck(L_3);
		int32_t L_4 = WebSocketCommandInfo_get_CloseStatusCode_m2849713255(L_3, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_5 = ___session;
		NullCheck(L_5);
		Il2CppObject * L_6 = WebSocket_get_ProtocolProcessor_m1265601209(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(8 /* WebSocket4Net.Protocol.ICloseStatusCode WebSocket4Net.Protocol.IProtocolProcessor::get_CloseStatusCode() */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_6);
		NullCheck(L_7);
		int32_t L_8 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 WebSocket4Net.Protocol.ICloseStatusCode::get_NormalClosure() */, ICloseStatusCode_t28582816_il2cpp_TypeInfo_var, L_7);
		if ((((int32_t)L_4) == ((int32_t)L_8)))
		{
			goto IL_0043;
		}
	}
	{
		WebSocketCommandInfo_t3536916738 * L_9 = ___commandInfo;
		NullCheck(L_9);
		int32_t L_10 = WebSocketCommandInfo_get_CloseStatusCode_m2849713255(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_10) > ((int32_t)0)))
		{
			goto IL_0040;
		}
	}
	{
		WebSocketCommandInfo_t3536916738 * L_11 = ___commandInfo;
		NullCheck(L_11);
		String_t* L_12 = WebSocketCommandInfo_get_Text_m101435856(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_13));
		goto IL_0041;
	}

IL_0040:
	{
		G_B5_0 = 0;
	}

IL_0041:
	{
		G_B7_0 = G_B5_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B7_0 = 1;
	}

IL_0044:
	{
		V_1 = (bool)G_B7_0;
		bool L_14 = V_1;
		if (L_14)
		{
			goto IL_0071;
		}
	}
	{
		WebSocket_t713846903 * L_15 = ___session;
		WebSocketCommandInfo_t3536916738 * L_16 = ___commandInfo;
		NullCheck(L_16);
		int32_t L_17 = WebSocketCommandInfo_get_CloseStatusCode_m2849713255(L_16, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_18);
		WebSocketCommandInfo_t3536916738 * L_20 = ___commandInfo;
		NullCheck(L_20);
		String_t* L_21 = WebSocketCommandInfo_get_Text_m101435856(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2901091385, L_19, L_21, /*hidden argument*/NULL);
		Exception_t1967233988 * L_23 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_23, L_22, /*hidden argument*/NULL);
		NullCheck(L_15);
		WebSocket_FireError_m4124567456(L_15, L_23, /*hidden argument*/NULL);
	}

IL_0071:
	{
		WebSocket_t713846903 * L_24 = ___session;
		NullCheck(L_24);
		WebSocket_CloseWithoutHandshake_m1514871525(L_24, /*hidden argument*/NULL);
		goto IL_00a8;
	}

IL_007a:
	{
		WebSocketCommandInfo_t3536916738 * L_25 = ___commandInfo;
		NullCheck(L_25);
		int32_t L_26 = WebSocketCommandInfo_get_CloseStatusCode_m2849713255(L_25, /*hidden argument*/NULL);
		V_0 = L_26;
		int32_t L_27 = V_0;
		V_1 = (bool)((((int32_t)L_27) > ((int32_t)0))? 1 : 0);
		bool L_28 = V_1;
		if (L_28)
		{
			goto IL_009a;
		}
	}
	{
		WebSocket_t713846903 * L_29 = ___session;
		NullCheck(L_29);
		Il2CppObject * L_30 = WebSocket_get_ProtocolProcessor_m1265601209(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Il2CppObject * L_31 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(8 /* WebSocket4Net.Protocol.ICloseStatusCode WebSocket4Net.Protocol.IProtocolProcessor::get_CloseStatusCode() */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_30);
		NullCheck(L_31);
		int32_t L_32 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 WebSocket4Net.Protocol.ICloseStatusCode::get_NoStatusCode() */, ICloseStatusCode_t28582816_il2cpp_TypeInfo_var, L_31);
		V_0 = L_32;
	}

IL_009a:
	{
		WebSocket_t713846903 * L_33 = ___session;
		int32_t L_34 = V_0;
		WebSocketCommandInfo_t3536916738 * L_35 = ___commandInfo;
		NullCheck(L_35);
		String_t* L_36 = WebSocketCommandInfo_get_Text_m101435856(L_35, /*hidden argument*/NULL);
		NullCheck(L_33);
		WebSocket_Close_m946219203(L_33, L_34, L_36, /*hidden argument*/NULL);
	}

IL_00a8:
	{
		return;
	}
}
// System.String WebSocket4Net.Command.Close::get_Name()
extern "C"  String_t* Close_get_Name_m3509318595 (Close_t3942764983 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		V_1 = 8;
		String_t* L_0 = Int32_ToString_m1286526384((&V_1), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Command.Close::.ctor()
extern "C"  void Close__ctor_m2113310610 (Close_t3942764983 * __this, const MethodInfo* method)
{
	{
		WebSocketCommandBase__ctor_m3945612519(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Command.Handshake::ExecuteCommand(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo)
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern TypeInfo* ICloseStatusCode_t28582816_il2cpp_TypeInfo_var;
extern const uint32_t Handshake_ExecuteCommand_m2977565908_MetadataUsageId;
extern "C"  void Handshake_ExecuteCommand_m2977565908 (Handshake_t1657241526 * __this, WebSocket_t713846903 * ___session, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Handshake_ExecuteCommand_m2977565908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	{
		WebSocket_t713846903 * L_0 = ___session;
		NullCheck(L_0);
		Il2CppObject * L_1 = WebSocket_get_ProtocolProcessor_m1265601209(L_0, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_2 = ___session;
		WebSocketCommandInfo_t3536916738 * L_3 = ___commandInfo;
		NullCheck(L_1);
		bool L_4 = InterfaceFuncInvoker3< bool, WebSocket_t713846903 *, WebSocketCommandInfo_t3536916738 *, String_t** >::Invoke(1 /* System.Boolean WebSocket4Net.Protocol.IProtocolProcessor::VerifyHandshake(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo,System.String&) */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_1, L_2, L_3, (&V_0));
		V_1 = L_4;
		bool L_5 = V_1;
		if (L_5)
		{
			goto IL_002f;
		}
	}
	{
		WebSocket_t713846903 * L_6 = ___session;
		WebSocket_t713846903 * L_7 = ___session;
		NullCheck(L_7);
		Il2CppObject * L_8 = WebSocket_get_ProtocolProcessor_m1265601209(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Il2CppObject * L_9 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(8 /* WebSocket4Net.Protocol.ICloseStatusCode WebSocket4Net.Protocol.IProtocolProcessor::get_CloseStatusCode() */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_8);
		NullCheck(L_9);
		int32_t L_10 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 WebSocket4Net.Protocol.ICloseStatusCode::get_ProtocolError() */, ICloseStatusCode_t28582816_il2cpp_TypeInfo_var, L_9);
		String_t* L_11 = V_0;
		NullCheck(L_6);
		WebSocket_Close_m946219203(L_6, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0036;
	}

IL_002f:
	{
		WebSocket_t713846903 * L_12 = ___session;
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(4 /* System.Void WebSocket4Net.WebSocket::OnHandshaked() */, L_12);
	}

IL_0036:
	{
		return;
	}
}
// System.String WebSocket4Net.Command.Handshake::get_Name()
extern "C"  String_t* Handshake_get_Name_m2545963074 (Handshake_t1657241526 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		V_1 = (-1);
		String_t* L_0 = Int32_ToString_m1286526384((&V_1), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Command.Handshake::.ctor()
extern "C"  void Handshake__ctor_m3150570739 (Handshake_t1657241526 * __this, const MethodInfo* method)
{
	{
		WebSocketCommandBase__ctor_m3945612519(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Command.Ping::ExecuteCommand(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo)
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern const uint32_t Ping_ExecuteCommand_m3149555197_MetadataUsageId;
extern "C"  void Ping_ExecuteCommand_m3149555197 (Ping_t3880049009 * __this, WebSocket_t713846903 * ___session, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ping_ExecuteCommand_m3149555197_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebSocket_t713846903 * L_0 = ___session;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_1 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		WebSocket_set_LastActiveTime_m147571246(L_0, L_1, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_2 = ___session;
		NullCheck(L_2);
		Il2CppObject * L_3 = WebSocket_get_ProtocolProcessor_m1265601209(L_2, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_4 = ___session;
		WebSocketCommandInfo_t3536916738 * L_5 = ___commandInfo;
		NullCheck(L_5);
		String_t* L_6 = WebSocketCommandInfo_get_Text_m101435856(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceActionInvoker2< WebSocket_t713846903 *, String_t* >::Invoke(6 /* System.Void WebSocket4Net.Protocol.IProtocolProcessor::SendPong(WebSocket4Net.WebSocket,System.String) */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_3, L_4, L_6);
		return;
	}
}
// System.String WebSocket4Net.Command.Ping::get_Name()
extern "C"  String_t* Ping_get_Name_m3923037265 (Ping_t3880049009 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		V_1 = ((int32_t)9);
		String_t* L_0 = Int32_ToString_m1286526384((&V_1), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Command.Ping::.ctor()
extern "C"  void Ping__ctor_m2399566954 (Ping_t3880049009 * __this, const MethodInfo* method)
{
	{
		WebSocketCommandBase__ctor_m3945612519(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Command.Pong::ExecuteCommand(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo)
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t Pong_ExecuteCommand_m3870274435_MetadataUsageId;
extern "C"  void Pong_ExecuteCommand_m3870274435 (Pong_t3880054775 * __this, WebSocket_t713846903 * ___session, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Pong_ExecuteCommand_m3870274435_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebSocket_t713846903 * L_0 = ___session;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_1 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		WebSocket_set_LastActiveTime_m147571246(L_0, L_1, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_2 = ___session;
		WebSocketCommandInfo_t3536916738 * L_3 = ___commandInfo;
		NullCheck(L_3);
		String_t* L_4 = WebSocketCommandInfo_get_Text_m101435856(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		WebSocket_set_LastPongResponse_m668946386(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.String WebSocket4Net.Command.Pong::get_Name()
extern "C"  String_t* Pong_get_Name_m83446231 (Pong_t3880054775 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		V_1 = ((int32_t)10);
		String_t* L_0 = Int32_ToString_m1286526384((&V_1), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Command.Pong::.ctor()
extern "C"  void Pong__ctor_m3174063268 (Pong_t3880054775 * __this, const MethodInfo* method)
{
	{
		WebSocketCommandBase__ctor_m3945612519(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Command.Text::ExecuteCommand(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo)
extern "C"  void Text_ExecuteCommand_m460198200 (Text_t3880164652 * __this, WebSocket_t713846903 * ___session, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method)
{
	{
		WebSocket_t713846903 * L_0 = ___session;
		WebSocketCommandInfo_t3536916738 * L_1 = ___commandInfo;
		NullCheck(L_1);
		String_t* L_2 = WebSocketCommandInfo_get_Text_m101435856(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		WebSocket_FireMessageReceived_m663897554(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String WebSocket4Net.Command.Text::get_Name()
extern "C"  String_t* Text_get_Name_m4160737164 (Text_t3880164652 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		V_1 = 1;
		String_t* L_0 = Int32_ToString_m1286526384((&V_1), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Command.Text::.ctor()
extern "C"  void Text__ctor_m1660271375 (Text_t3880164652 * __this, const MethodInfo* method)
{
	{
		WebSocketCommandBase__ctor_m3945612519(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Command.WebSocketCommandBase::.ctor()
extern "C"  void WebSocketCommandBase__ctor_m3945612519 (WebSocketCommandBase_t3263712852 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.DataReceivedEventArgs::.ctor(System.Byte[])
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t DataReceivedEventArgs__ctor_m2970247150_MetadataUsageId;
extern "C"  void DataReceivedEventArgs__ctor_m2970247150 (DataReceivedEventArgs_t1979092924 * __this, ByteU5BU5D_t58506160* ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataReceivedEventArgs__ctor_m2970247150_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1904770202(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_0 = ___data;
		DataReceivedEventArgs_set_Data_m589273081(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.DataReceivedEventArgs::set_Data(System.Byte[])
extern "C"  void DataReceivedEventArgs_set_Data_m589273081 (DataReceivedEventArgs_t1979092924 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = ___value;
		__this->set_U3CDataU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Extensions::AppendFormatWithCrCf(System.Text.StringBuilder,System.String,System.Object)
extern TypeInfo* Extensions_t39620836_il2cpp_TypeInfo_var;
extern const uint32_t Extensions_AppendFormatWithCrCf_m759365938_MetadataUsageId;
extern "C"  void Extensions_AppendFormatWithCrCf_m759365938 (Il2CppObject * __this /* static, unused */, StringBuilder_t3822575854 * ___builder, String_t* ___format, Il2CppObject * ___arg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Extensions_AppendFormatWithCrCf_m759365938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t3822575854 * L_0 = ___builder;
		String_t* L_1 = ___format;
		Il2CppObject * L_2 = ___arg;
		NullCheck(L_0);
		StringBuilder_AppendFormat_m3723191730(L_0, L_1, L_2, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_3 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_4 = ((Extensions_t39620836_StaticFields*)Extensions_t39620836_il2cpp_TypeInfo_var->static_fields)->get_m_CrCf_0();
		NullCheck(L_3);
		StringBuilder_Append_m2223932948(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Extensions::AppendFormatWithCrCf(System.Text.StringBuilder,System.String,System.Object[])
extern TypeInfo* Extensions_t39620836_il2cpp_TypeInfo_var;
extern const uint32_t Extensions_AppendFormatWithCrCf_m3901244368_MetadataUsageId;
extern "C"  void Extensions_AppendFormatWithCrCf_m3901244368 (Il2CppObject * __this /* static, unused */, StringBuilder_t3822575854 * ___builder, String_t* ___format, ObjectU5BU5D_t11523773* ___args, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Extensions_AppendFormatWithCrCf_m3901244368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t3822575854 * L_0 = ___builder;
		String_t* L_1 = ___format;
		ObjectU5BU5D_t11523773* L_2 = ___args;
		NullCheck(L_0);
		StringBuilder_AppendFormat_m279545936(L_0, L_1, L_2, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_3 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_4 = ((Extensions_t39620836_StaticFields*)Extensions_t39620836_il2cpp_TypeInfo_var->static_fields)->get_m_CrCf_0();
		NullCheck(L_3);
		StringBuilder_Append_m2223932948(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Extensions::AppendWithCrCf(System.Text.StringBuilder,System.String)
extern TypeInfo* Extensions_t39620836_il2cpp_TypeInfo_var;
extern const uint32_t Extensions_AppendWithCrCf_m3522543387_MetadataUsageId;
extern "C"  void Extensions_AppendWithCrCf_m3522543387 (Il2CppObject * __this /* static, unused */, StringBuilder_t3822575854 * ___builder, String_t* ___content, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Extensions_AppendWithCrCf_m3522543387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t3822575854 * L_0 = ___builder;
		String_t* L_1 = ___content;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, L_1, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_2 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_3 = ((Extensions_t39620836_StaticFields*)Extensions_t39620836_il2cpp_TypeInfo_var->static_fields)->get_m_CrCf_0();
		NullCheck(L_2);
		StringBuilder_Append_m2223932948(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Extensions::AppendWithCrCf(System.Text.StringBuilder)
extern TypeInfo* Extensions_t39620836_il2cpp_TypeInfo_var;
extern const uint32_t Extensions_AppendWithCrCf_m3929176351_MetadataUsageId;
extern "C"  void Extensions_AppendWithCrCf_m3929176351 (Il2CppObject * __this /* static, unused */, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Extensions_AppendWithCrCf_m3929176351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t3822575854 * L_0 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_1 = ((Extensions_t39620836_StaticFields*)Extensions_t39620836_il2cpp_TypeInfo_var->static_fields)->get_m_CrCf_0();
		NullCheck(L_0);
		StringBuilder_Append_m2223932948(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocket4Net.Extensions::ParseMimeHeader(System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringReader_t2229325051_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t3650470111_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral9;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral1396;
extern const uint32_t Extensions_ParseMimeHeader_m1946375443_MetadataUsageId;
extern "C"  bool Extensions_ParseMimeHeader_m1946375443 (Il2CppObject * __this /* static, unused */, String_t* ___source, Il2CppObject* ___valueContainer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Extensions_ParseMimeHeader_m1946375443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	StringReader_t2229325051 * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	int32_t V_6 = 0;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	bool V_9 = false;
	bool V_10 = false;
	int32_t G_B6_0 = 0;
	int32_t G_B16_0 = 0;
	{
		Il2CppObject* L_0 = ___valueContainer;
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_1;
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_2;
		String_t* L_3 = ___source;
		StringReader_t2229325051 * L_4 = (StringReader_t2229325051 *)il2cpp_codegen_object_new(StringReader_t2229325051_il2cpp_TypeInfo_var);
		StringReader__ctor_m1181104909(L_4, L_3, /*hidden argument*/NULL);
		V_4 = L_4;
		goto IL_013e;
	}

IL_001c:
	{
		String_t* L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_10 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		bool L_7 = V_10;
		if (L_7)
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_8 = V_1;
		V_2 = L_8;
		goto IL_013e;
	}

IL_0034:
	{
		String_t* L_9 = V_1;
		NullCheck(L_9);
		bool L_10 = String_StartsWith_m1500793453(L_9, _stringLiteral9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0049;
		}
	}
	{
		String_t* L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_12));
		goto IL_004a;
	}

IL_0049:
	{
		G_B6_0 = 1;
	}

IL_004a:
	{
		V_10 = (bool)G_B6_0;
		bool L_13 = V_10;
		if (L_13)
		{
			goto IL_0082;
		}
	}
	{
		Il2CppObject* L_14 = V_0;
		String_t* L_15 = V_3;
		NullCheck(L_14);
		bool L_16 = InterfaceFuncInvoker2< bool, String_t*, Il2CppObject ** >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, L_14, L_15, (&V_5));
		V_10 = L_16;
		bool L_17 = V_10;
		if (L_17)
		{
			goto IL_0068;
		}
	}
	{
		V_9 = (bool)0;
		goto IL_015d;
	}

IL_0068:
	{
		Il2CppObject* L_18 = V_0;
		String_t* L_19 = V_3;
		Il2CppObject * L_20 = V_5;
		String_t* L_21 = V_1;
		NullCheck(L_21);
		String_t* L_22 = String_Trim_m1030489823(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m389863537(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		NullCheck(L_18);
		InterfaceActionInvoker2< String_t*, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Object>::set_Item(!0,!1) */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, L_18, L_19, L_23);
		goto IL_013e;
	}

IL_0082:
	{
		String_t* L_24 = V_1;
		NullCheck(L_24);
		int32_t L_25 = String_IndexOf_m2775210486(L_24, ((int32_t)58), /*hidden argument*/NULL);
		V_6 = L_25;
		String_t* L_26 = V_1;
		int32_t L_27 = V_6;
		NullCheck(L_26);
		String_t* L_28 = String_Substring_m675079568(L_26, 0, L_27, /*hidden argument*/NULL);
		V_7 = L_28;
		String_t* L_29 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_30 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		V_10 = L_30;
		bool L_31 = V_10;
		if (L_31)
		{
			goto IL_00ad;
		}
	}
	{
		String_t* L_32 = V_7;
		NullCheck(L_32);
		String_t* L_33 = String_Trim_m1030489823(L_32, /*hidden argument*/NULL);
		V_7 = L_33;
	}

IL_00ad:
	{
		String_t* L_34 = V_1;
		int32_t L_35 = V_6;
		NullCheck(L_34);
		String_t* L_36 = String_Substring_m2809233063(L_34, ((int32_t)((int32_t)L_35+(int32_t)1)), /*hidden argument*/NULL);
		V_8 = L_36;
		String_t* L_37 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_38 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		if (L_38)
		{
			goto IL_00df;
		}
	}
	{
		String_t* L_39 = V_8;
		NullCheck(L_39);
		bool L_40 = String_StartsWith_m1500793453(L_39, _stringLiteral32, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_00df;
		}
	}
	{
		String_t* L_41 = V_8;
		NullCheck(L_41);
		int32_t L_42 = String_get_Length_m2979997331(L_41, /*hidden argument*/NULL);
		G_B16_0 = ((((int32_t)((((int32_t)L_42) > ((int32_t)1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00e0;
	}

IL_00df:
	{
		G_B16_0 = 1;
	}

IL_00e0:
	{
		V_10 = (bool)G_B16_0;
		bool L_43 = V_10;
		if (L_43)
		{
			goto IL_00f0;
		}
	}
	{
		String_t* L_44 = V_8;
		NullCheck(L_44);
		String_t* L_45 = String_Substring_m2809233063(L_44, 1, /*hidden argument*/NULL);
		V_8 = L_45;
	}

IL_00f0:
	{
		String_t* L_46 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_47 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		V_10 = (bool)((((int32_t)L_47) == ((int32_t)0))? 1 : 0);
		bool L_48 = V_10;
		if (L_48)
		{
			goto IL_0102;
		}
	}
	{
		goto IL_013e;
	}

IL_0102:
	{
		Il2CppObject* L_49 = V_0;
		String_t* L_50 = V_7;
		NullCheck(L_49);
		bool L_51 = InterfaceFuncInvoker2< bool, String_t*, Il2CppObject ** >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, L_49, L_50, (&V_5));
		V_10 = L_51;
		bool L_52 = V_10;
		if (L_52)
		{
			goto IL_0121;
		}
	}
	{
		Il2CppObject* L_53 = V_0;
		String_t* L_54 = V_7;
		String_t* L_55 = V_8;
		NullCheck(L_53);
		InterfaceActionInvoker2< String_t*, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Object>::Add(!0,!1) */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, L_53, L_54, L_55);
		goto IL_013a;
	}

IL_0121:
	{
		Il2CppObject* L_56 = V_0;
		String_t* L_57 = V_7;
		Il2CppObject * L_58 = V_5;
		String_t* L_59 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = String_Concat_m2809334143(NULL /*static, unused*/, L_58, _stringLiteral1396, L_59, /*hidden argument*/NULL);
		NullCheck(L_56);
		InterfaceActionInvoker2< String_t*, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Object>::set_Item(!0,!1) */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, L_56, L_57, L_60);
	}

IL_013a:
	{
		String_t* L_61 = V_7;
		V_3 = L_61;
	}

IL_013e:
	{
		StringReader_t2229325051 * L_62 = V_4;
		NullCheck(L_62);
		String_t* L_63 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.IO.TextReader::ReadLine() */, L_62);
		String_t* L_64 = L_63;
		V_1 = L_64;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_65 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_64, /*hidden argument*/NULL);
		V_10 = (bool)((((int32_t)L_65) == ((int32_t)0))? 1 : 0);
		bool L_66 = V_10;
		if (L_66)
		{
			goto IL_001c;
		}
	}
	{
		V_9 = (bool)1;
		goto IL_015d;
	}

IL_015d:
	{
		bool L_67 = V_9;
		return L_67;
	}
}
// System.Void WebSocket4Net.Extensions::.cctor()
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Extensions_t39620836_il2cpp_TypeInfo_var;
extern const uint32_t Extensions__cctor_m3284150402_MetadataUsageId;
extern "C"  void Extensions__cctor_m3284150402 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Extensions__cctor_m3284150402_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t3416858730* V_0 = NULL;
	{
		V_0 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)2));
		CharU5BU5D_t3416858730* L_0 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)13));
		CharU5BU5D_t3416858730* L_1 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint16_t)((int32_t)10));
		CharU5BU5D_t3416858730* L_2 = V_0;
		((Extensions_t39620836_StaticFields*)Extensions_t39620836_il2cpp_TypeInfo_var->static_fields)->set_m_CrCf_0(L_2);
		return;
	}
}
// System.Void WebSocket4Net.MessageReceivedEventArgs::.ctor(System.String)
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t MessageReceivedEventArgs__ctor_m2681790322_MetadataUsageId;
extern "C"  void MessageReceivedEventArgs__ctor_m2681790322 (MessageReceivedEventArgs_t3871897919 * __this, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageReceivedEventArgs__ctor_m2681790322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1904770202(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message;
		MessageReceivedEventArgs_set_Message_m258412138(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String WebSocket4Net.MessageReceivedEventArgs::get_Message()
extern "C"  String_t* MessageReceivedEventArgs_get_Message_m1329338057 (MessageReceivedEventArgs_t3871897919 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CMessageU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.MessageReceivedEventArgs::set_Message(System.String)
extern "C"  void MessageReceivedEventArgs_set_Message_m258412138 (MessageReceivedEventArgs_t3871897919 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CMessageU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::.ctor()
extern "C"  void CloseStatusCodeHybi10__ctor_m4111104231 (CloseStatusCodeHybi10_t635449120 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		CloseStatusCodeHybi10_set_NormalClosure_m2689937357(__this, ((int32_t)1000), /*hidden argument*/NULL);
		CloseStatusCodeHybi10_set_GoingAway_m114964737(__this, ((int32_t)1001), /*hidden argument*/NULL);
		CloseStatusCodeHybi10_set_ProtocolError_m1698010569(__this, ((int32_t)1002), /*hidden argument*/NULL);
		CloseStatusCodeHybi10_set_NotAcceptableData_m2003109016(__this, ((int32_t)1003), /*hidden argument*/NULL);
		CloseStatusCodeHybi10_set_TooLargeFrame_m481629279(__this, ((int32_t)1004), /*hidden argument*/NULL);
		CloseStatusCodeHybi10_set_InvalidUTF8_m2679473473(__this, ((int32_t)1007), /*hidden argument*/NULL);
		CloseStatusCodeHybi10_set_ViolatePolicy_m128123149(__this, ((int32_t)1000), /*hidden argument*/NULL);
		CloseStatusCodeHybi10_set_ExtensionNotMatch_m3803409994(__this, ((int32_t)1000), /*hidden argument*/NULL);
		CloseStatusCodeHybi10_set_UnexpectedCondition_m1822824579(__this, ((int32_t)1000), /*hidden argument*/NULL);
		CloseStatusCodeHybi10_set_TLSHandshakeFailure_m3799478887(__this, ((int32_t)1000), /*hidden argument*/NULL);
		CloseStatusCodeHybi10_set_NoStatusCode_m3905822443(__this, ((int32_t)1005), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::get_NormalClosure()
extern "C"  int32_t CloseStatusCodeHybi10_get_NormalClosure_m707748958 (CloseStatusCodeHybi10_t635449120 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CNormalClosureU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_NormalClosure(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_NormalClosure_m2689937357 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CNormalClosureU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_GoingAway(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_GoingAway_m114964737 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CGoingAwayU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::get_ProtocolError()
extern "C"  int32_t CloseStatusCodeHybi10_get_ProtocolError_m1205593178 (CloseStatusCodeHybi10_t635449120 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CProtocolErrorU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_ProtocolError(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_ProtocolError_m1698010569 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CProtocolErrorU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_NotAcceptableData(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_NotAcceptableData_m2003109016 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CNotAcceptableDataU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_TooLargeFrame(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_TooLargeFrame_m481629279 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CTooLargeFrameU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_InvalidUTF8(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_InvalidUTF8_m2679473473 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CInvalidUTF8U3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_ViolatePolicy(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_ViolatePolicy_m128123149 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CViolatePolicyU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_ExtensionNotMatch(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_ExtensionNotMatch_m3803409994 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CExtensionNotMatchU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_UnexpectedCondition(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_UnexpectedCondition_m1822824579 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CUnexpectedConditionU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_TLSHandshakeFailure(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_TLSHandshakeFailure_m3799478887 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CTLSHandshakeFailureU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::get_NoStatusCode()
extern "C"  int32_t CloseStatusCodeHybi10_get_NoStatusCode_m1773857912 (CloseStatusCodeHybi10_t635449120 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CNoStatusCodeU3Ek__BackingField_10();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_NoStatusCode(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_NoStatusCode_m3905822443 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CNoStatusCodeU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::.ctor()
extern "C"  void CloseStatusCodeRfc6455__ctor_m917947323 (CloseStatusCodeRfc6455_t1268471848 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		CloseStatusCodeRfc6455_set_NormalClosure_m205733537(__this, ((int32_t)1000), /*hidden argument*/NULL);
		CloseStatusCodeRfc6455_set_GoingAway_m1613287893(__this, ((int32_t)1001), /*hidden argument*/NULL);
		CloseStatusCodeRfc6455_set_ProtocolError_m3508774045(__this, ((int32_t)1002), /*hidden argument*/NULL);
		CloseStatusCodeRfc6455_set_NotAcceptableData_m222792044(__this, ((int32_t)1003), /*hidden argument*/NULL);
		CloseStatusCodeRfc6455_set_TooLargeFrame_m2292392755(__this, ((int32_t)1009), /*hidden argument*/NULL);
		CloseStatusCodeRfc6455_set_InvalidUTF8_m3753982229(__this, ((int32_t)1007), /*hidden argument*/NULL);
		CloseStatusCodeRfc6455_set_ViolatePolicy_m1938886625(__this, ((int32_t)1008), /*hidden argument*/NULL);
		CloseStatusCodeRfc6455_set_ExtensionNotMatch_m2023093022(__this, ((int32_t)1010), /*hidden argument*/NULL);
		CloseStatusCodeRfc6455_set_UnexpectedCondition_m335198295(__this, ((int32_t)1011), /*hidden argument*/NULL);
		CloseStatusCodeRfc6455_set_NoStatusCode_m2855855511(__this, ((int32_t)1005), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::get_NormalClosure()
extern "C"  int32_t CloseStatusCodeRfc6455_get_NormalClosure_m1677157654 (CloseStatusCodeRfc6455_t1268471848 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CNormalClosureU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_NormalClosure(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_NormalClosure_m205733537 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CNormalClosureU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_GoingAway(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_GoingAway_m1613287893 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CGoingAwayU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::get_ProtocolError()
extern "C"  int32_t CloseStatusCodeRfc6455_get_ProtocolError_m2175001874 (CloseStatusCodeRfc6455_t1268471848 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CProtocolErrorU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_ProtocolError(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_ProtocolError_m3508774045 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CProtocolErrorU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_NotAcceptableData(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_NotAcceptableData_m222792044 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CNotAcceptableDataU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_TooLargeFrame(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_TooLargeFrame_m2292392755 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CTooLargeFrameU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_InvalidUTF8(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_InvalidUTF8_m3753982229 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CInvalidUTF8U3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_ViolatePolicy(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_ViolatePolicy_m1938886625 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CViolatePolicyU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_ExtensionNotMatch(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_ExtensionNotMatch_m2023093022 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CExtensionNotMatchU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_UnexpectedCondition(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_UnexpectedCondition_m335198295 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CUnexpectedConditionU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::get_NoStatusCode()
extern "C"  int32_t CloseStatusCodeRfc6455_get_NoStatusCode_m1528034496 (CloseStatusCodeRfc6455_t1268471848 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CNoStatusCodeU3Ek__BackingField_9();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_NoStatusCode(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_NoStatusCode_m2855855511 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CNoStatusCodeU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi00DataReader::.ctor(WebSocket4Net.Protocol.ReaderBase)
extern "C"  void DraftHybi00DataReader__ctor_m2490528791 (DraftHybi00DataReader_t1509845496 * __this, ReaderBase_t893022310 * ___previousCommandReader, const MethodInfo* method)
{
	{
		ReaderBase_t893022310 * L_0 = ___previousCommandReader;
		ReaderBase__ctor_m849562793(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// WebSocket4Net.WebSocketCommandInfo WebSocket4Net.Protocol.DraftHybi00DataReader::GetCommandInfo(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m3593838017_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m3112232588_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m759985295_MethodInfo_var;
extern const MethodInfo* ArraySegmentList_1_AddSegment_m727323212_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m2954236726_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m404638567_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3808413405_MethodInfo_var;
extern const uint32_t DraftHybi00DataReader_GetCommandInfo_m976750997_MetadataUsageId;
extern "C"  WebSocketCommandInfo_t3536916738 * DraftHybi00DataReader_GetCommandInfo_m976750997 (DraftHybi00DataReader_t1509845496 * __this, ByteU5BU5D_t58506160* ___readBuffer, int32_t ___offset, int32_t ___length, int32_t* ___left, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00DataReader_GetCommandInfo_m976750997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	uint8_t V_1 = 0x0;
	uint8_t V_2 = 0x0;
	int32_t V_3 = 0;
	WebSocketCommandInfo_t3536916738 * V_4 = NULL;
	uint8_t V_5 = 0x0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	WebSocketCommandInfo_t3536916738 * V_9 = NULL;
	bool V_10 = false;
	int32_t V_11 = 0;
	int32_t G_B17_0 = 0;
	{
		int32_t* L_0 = ___left;
		*((int32_t*)(L_0)) = (int32_t)0;
		V_0 = 0;
		Nullable_1_t1369764433 * L_1 = __this->get_address_of_m_Type_3();
		bool L_2 = Nullable_1_get_HasValue_m3593838017(L_1, /*hidden argument*/Nullable_1_get_HasValue_m3593838017_MethodInfo_var);
		V_10 = L_2;
		bool L_3 = V_10;
		if (L_3)
		{
			goto IL_002c;
		}
	}
	{
		ByteU5BU5D_t58506160* L_4 = ___readBuffer;
		int32_t L_5 = ___offset;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_1 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		V_0 = 1;
		uint8_t L_7 = V_1;
		Nullable_1_t1369764433  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Nullable_1__ctor_m3112232588(&L_8, L_7, /*hidden argument*/Nullable_1__ctor_m3112232588_MethodInfo_var);
		__this->set_m_Type_3(L_8);
	}

IL_002c:
	{
		Nullable_1_t1369764433 * L_9 = __this->get_address_of_m_Type_3();
		uint8_t L_10 = Nullable_1_get_Value_m759985295(L_9, /*hidden argument*/Nullable_1_get_Value_m759985295_MethodInfo_var);
		V_10 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)128)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_11 = V_10;
		if (L_11)
		{
			goto IL_0137;
		}
	}
	{
		V_2 = ((int32_t)255);
		int32_t L_12 = ___offset;
		int32_t L_13 = V_0;
		V_3 = ((int32_t)((int32_t)L_12+(int32_t)L_13));
		goto IL_0112;
	}

IL_005c:
	{
		ByteU5BU5D_t58506160* L_14 = ___readBuffer;
		int32_t L_15 = V_3;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		uint8_t L_17 = V_2;
		V_10 = (bool)((((int32_t)((((int32_t)((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16)))) == ((int32_t)L_17))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_18 = V_10;
		if (L_18)
		{
			goto IL_010d;
		}
	}
	{
		int32_t* L_19 = ___left;
		int32_t L_20 = ___length;
		int32_t L_21 = V_3;
		int32_t L_22 = ___offset;
		*((int32_t*)(L_19)) = (int32_t)((int32_t)((int32_t)L_20-(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_21-(int32_t)L_22))+(int32_t)1))));
		ArraySegmentList_t1783467835 * L_23 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		int32_t L_24 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_23);
		V_10 = (bool)((((int32_t)L_24) > ((int32_t)0))? 1 : 0);
		bool L_25 = V_10;
		if (L_25)
		{
			goto IL_00c4;
		}
	}
	{
		V_11 = 1;
		String_t* L_26 = Int32_ToString_m1286526384((&V_11), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_27 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_28 = ___readBuffer;
		int32_t L_29 = ___offset;
		int32_t L_30 = V_0;
		int32_t L_31 = V_3;
		int32_t L_32 = ___offset;
		int32_t L_33 = V_0;
		NullCheck(L_27);
		String_t* L_34 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_27, L_28, ((int32_t)((int32_t)L_29+(int32_t)L_30)), ((int32_t)((int32_t)((int32_t)((int32_t)L_31-(int32_t)L_32))-(int32_t)L_33)));
		WebSocketCommandInfo_t3536916738 * L_35 = (WebSocketCommandInfo_t3536916738 *)il2cpp_codegen_object_new(WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var);
		WebSocketCommandInfo__ctor_m1488440433(L_35, L_26, L_34, /*hidden argument*/NULL);
		V_4 = L_35;
		DraftHybi00DataReader_Reset_m222943283(__this, (bool)0, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_36 = V_4;
		V_9 = L_36;
		goto IL_02c4;
	}

IL_00c4:
	{
		ArraySegmentList_t1783467835 * L_37 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_38 = ___readBuffer;
		int32_t L_39 = ___offset;
		int32_t L_40 = V_0;
		int32_t L_41 = V_3;
		int32_t L_42 = ___offset;
		int32_t L_43 = V_0;
		NullCheck(L_37);
		ArraySegmentList_1_AddSegment_m727323212(L_37, L_38, ((int32_t)((int32_t)L_39+(int32_t)L_40)), ((int32_t)((int32_t)((int32_t)((int32_t)L_41-(int32_t)L_42))-(int32_t)L_43)), (bool)0, /*hidden argument*/ArraySegmentList_1_AddSegment_m727323212_MethodInfo_var);
		V_11 = 1;
		String_t* L_44 = Int32_ToString_m1286526384((&V_11), /*hidden argument*/NULL);
		ArraySegmentList_t1783467835 * L_45 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_46 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_45);
		String_t* L_47 = ArraySegmentList_Decode_m4038946727(L_45, L_46, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_48 = (WebSocketCommandInfo_t3536916738 *)il2cpp_codegen_object_new(WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var);
		WebSocketCommandInfo__ctor_m1488440433(L_48, L_44, L_47, /*hidden argument*/NULL);
		V_4 = L_48;
		DraftHybi00DataReader_Reset_m222943283(__this, (bool)1, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_49 = V_4;
		V_9 = L_49;
		goto IL_02c4;
	}

IL_010d:
	{
		int32_t L_50 = V_3;
		V_3 = ((int32_t)((int32_t)L_50+(int32_t)1));
	}

IL_0112:
	{
		int32_t L_51 = V_3;
		int32_t L_52 = ___offset;
		int32_t L_53 = ___length;
		V_10 = (bool)((((int32_t)L_51) < ((int32_t)((int32_t)((int32_t)L_52+(int32_t)L_53))))? 1 : 0);
		bool L_54 = V_10;
		if (L_54)
		{
			goto IL_005c;
		}
	}
	{
		ByteU5BU5D_t58506160* L_55 = ___readBuffer;
		int32_t L_56 = ___offset;
		int32_t L_57 = V_0;
		int32_t L_58 = ___length;
		int32_t L_59 = V_0;
		ReaderBase_AddArraySegment_m3437881651(__this, L_55, ((int32_t)((int32_t)L_56+(int32_t)L_57)), ((int32_t)((int32_t)L_58-(int32_t)L_59)), /*hidden argument*/NULL);
		V_9 = (WebSocketCommandInfo_t3536916738 *)NULL;
		goto IL_02c4;
	}

IL_0137:
	{
		goto IL_01e5;
	}

IL_013d:
	{
		int32_t L_60 = ___length;
		int32_t L_61 = V_0;
		V_10 = (bool)((((int32_t)L_60) > ((int32_t)L_61))? 1 : 0);
		bool L_62 = V_10;
		if (L_62)
		{
			goto IL_0151;
		}
	}
	{
		V_9 = (WebSocketCommandInfo_t3536916738 *)NULL;
		goto IL_02c4;
	}

IL_0151:
	{
		ByteU5BU5D_t58506160* L_63 = ___readBuffer;
		int32_t L_64 = V_0;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, L_64);
		int32_t L_65 = L_64;
		V_5 = ((L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_65)));
		uint8_t L_66 = V_5;
		if (L_66)
		{
			goto IL_0171;
		}
	}
	{
		Nullable_1_t1369764433 * L_67 = __this->get_address_of_m_Type_3();
		uint8_t L_68 = Nullable_1_get_Value_m759985295(L_67, /*hidden argument*/Nullable_1_get_Value_m759985295_MethodInfo_var);
		G_B17_0 = ((((int32_t)((((int32_t)L_68) == ((int32_t)((int32_t)255)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0172;
	}

IL_0171:
	{
		G_B17_0 = 1;
	}

IL_0172:
	{
		V_10 = (bool)G_B17_0;
		bool L_69 = V_10;
		if (L_69)
		{
			goto IL_019b;
		}
	}
	{
		V_11 = 8;
		String_t* L_70 = Int32_ToString_m1286526384((&V_11), /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_71 = (WebSocketCommandInfo_t3536916738 *)il2cpp_codegen_object_new(WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var);
		WebSocketCommandInfo__ctor_m2432496885(L_71, L_70, /*hidden argument*/NULL);
		V_4 = L_71;
		DraftHybi00DataReader_Reset_m222943283(__this, (bool)1, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_72 = V_4;
		V_9 = L_72;
		goto IL_02c4;
	}

IL_019b:
	{
		uint8_t L_73 = V_5;
		V_6 = ((int32_t)((int32_t)L_73&(int32_t)((int32_t)127)));
		int32_t L_74 = __this->get_m_TempLength_4();
		int32_t L_75 = V_6;
		__this->set_m_TempLength_4(((int32_t)((int32_t)((int32_t)((int32_t)L_74*(int32_t)((int32_t)128)))+(int32_t)L_75)));
		int32_t L_76 = V_0;
		V_0 = ((int32_t)((int32_t)L_76+(int32_t)1));
		uint8_t L_77 = V_5;
		V_10 = (bool)((((int32_t)((int32_t)((int32_t)L_77&(int32_t)((int32_t)128)))) == ((int32_t)((int32_t)128)))? 1 : 0);
		bool L_78 = V_10;
		if (L_78)
		{
			goto IL_01e4;
		}
	}
	{
		int32_t L_79 = __this->get_m_TempLength_4();
		Nullable_1_t1438485399  L_80;
		memset(&L_80, 0, sizeof(L_80));
		Nullable_1__ctor_m2954236726(&L_80, L_79, /*hidden argument*/Nullable_1__ctor_m2954236726_MethodInfo_var);
		__this->set_m_Length_5(L_80);
		goto IL_01fc;
	}

IL_01e4:
	{
	}

IL_01e5:
	{
		Nullable_1_t1438485399 * L_81 = __this->get_address_of_m_Length_5();
		bool L_82 = Nullable_1_get_HasValue_m404638567(L_81, /*hidden argument*/Nullable_1_get_HasValue_m404638567_MethodInfo_var);
		V_10 = (bool)((((int32_t)L_82) == ((int32_t)0))? 1 : 0);
		bool L_83 = V_10;
		if (L_83)
		{
			goto IL_013d;
		}
	}

IL_01fc:
	{
		Nullable_1_t1438485399 * L_84 = __this->get_address_of_m_Length_5();
		int32_t L_85 = Nullable_1_get_Value_m3808413405(L_84, /*hidden argument*/Nullable_1_get_Value_m3808413405_MethodInfo_var);
		ArraySegmentList_t1783467835 * L_86 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		NullCheck(L_86);
		int32_t L_87 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_86);
		V_7 = ((int32_t)((int32_t)L_85-(int32_t)L_87));
		int32_t L_88 = ___length;
		int32_t L_89 = V_0;
		V_8 = ((int32_t)((int32_t)L_88-(int32_t)L_89));
		int32_t L_90 = V_8;
		int32_t L_91 = V_7;
		V_10 = (bool)((((int32_t)((((int32_t)L_90) < ((int32_t)L_91))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_92 = V_10;
		if (L_92)
		{
			goto IL_023e;
		}
	}
	{
		ByteU5BU5D_t58506160* L_93 = ___readBuffer;
		int32_t L_94 = V_0;
		int32_t L_95 = ___length;
		int32_t L_96 = V_0;
		ReaderBase_AddArraySegment_m3437881651(__this, L_93, L_94, ((int32_t)((int32_t)L_95-(int32_t)L_96)), /*hidden argument*/NULL);
		V_9 = (WebSocketCommandInfo_t3536916738 *)NULL;
		goto IL_02c4;
	}

IL_023e:
	{
		int32_t* L_97 = ___left;
		int32_t L_98 = V_8;
		int32_t L_99 = V_7;
		*((int32_t*)(L_97)) = (int32_t)((int32_t)((int32_t)L_98-(int32_t)L_99));
		ArraySegmentList_t1783467835 * L_100 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		NullCheck(L_100);
		int32_t L_101 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_100);
		V_10 = (bool)((((int32_t)L_101) > ((int32_t)0))? 1 : 0);
		bool L_102 = V_10;
		if (L_102)
		{
			goto IL_028b;
		}
	}
	{
		V_11 = 1;
		String_t* L_103 = Int32_ToString_m1286526384((&V_11), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_104 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_105 = ___readBuffer;
		int32_t L_106 = ___offset;
		int32_t L_107 = V_0;
		int32_t L_108 = V_7;
		NullCheck(L_104);
		String_t* L_109 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_104, L_105, ((int32_t)((int32_t)L_106+(int32_t)L_107)), L_108);
		WebSocketCommandInfo_t3536916738 * L_110 = (WebSocketCommandInfo_t3536916738 *)il2cpp_codegen_object_new(WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var);
		WebSocketCommandInfo__ctor_m1488440433(L_110, L_103, L_109, /*hidden argument*/NULL);
		V_4 = L_110;
		DraftHybi00DataReader_Reset_m222943283(__this, (bool)0, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_111 = V_4;
		V_9 = L_111;
		goto IL_02c4;
	}

IL_028b:
	{
		ArraySegmentList_t1783467835 * L_112 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_113 = ___readBuffer;
		int32_t L_114 = ___offset;
		int32_t L_115 = V_0;
		int32_t L_116 = V_7;
		NullCheck(L_112);
		ArraySegmentList_1_AddSegment_m727323212(L_112, L_113, ((int32_t)((int32_t)L_114+(int32_t)L_115)), L_116, (bool)0, /*hidden argument*/ArraySegmentList_1_AddSegment_m727323212_MethodInfo_var);
		ArraySegmentList_t1783467835 * L_117 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_118 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_117);
		String_t* L_119 = ArraySegmentList_Decode_m4038946727(L_117, L_118, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_120 = (WebSocketCommandInfo_t3536916738 *)il2cpp_codegen_object_new(WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var);
		WebSocketCommandInfo__ctor_m2432496885(L_120, L_119, /*hidden argument*/NULL);
		V_4 = L_120;
		DraftHybi00DataReader_Reset_m222943283(__this, (bool)1, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_121 = V_4;
		V_9 = L_121;
		goto IL_02c4;
	}

IL_02c4:
	{
		WebSocketCommandInfo_t3536916738 * L_122 = V_9;
		return L_122;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi00DataReader::Reset(System.Boolean)
extern TypeInfo* Nullable_1_t1369764433_il2cpp_TypeInfo_var;
extern TypeInfo* Nullable_1_t1438485399_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegmentList_1_ClearSegements_m3355833128_MethodInfo_var;
extern const uint32_t DraftHybi00DataReader_Reset_m222943283_MetadataUsageId;
extern "C"  void DraftHybi00DataReader_Reset_m222943283 (DraftHybi00DataReader_t1509845496 * __this, bool ___clearBuffer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00DataReader_Reset_m222943283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Nullable_1_t1369764433 * L_0 = __this->get_address_of_m_Type_3();
		Initobj (Nullable_1_t1369764433_il2cpp_TypeInfo_var, L_0);
		Nullable_1_t1438485399 * L_1 = __this->get_address_of_m_Length_5();
		Initobj (Nullable_1_t1438485399_il2cpp_TypeInfo_var, L_1);
		__this->set_m_TempLength_4(0);
		bool L_2 = ___clearBuffer;
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_0;
		if (L_3)
		{
			goto IL_0034;
		}
	}
	{
		ArraySegmentList_t1783467835 * L_4 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArraySegmentList_1_ClearSegements_m3355833128(L_4, /*hidden argument*/ArraySegmentList_1_ClearSegements_m3355833128_MethodInfo_var);
	}

IL_0034:
	{
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi00HandshakeReader::.ctor(WebSocket4Net.WebSocket)
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern TypeInfo* HandshakeReader_t1078082956_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi00HandshakeReader__ctor_m2012279385_MetadataUsageId;
extern "C"  void DraftHybi00HandshakeReader__ctor_m2012279385 (DraftHybi00HandshakeReader_t1765220979 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00HandshakeReader__ctor_m2012279385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_ReceivedChallengeLength_6((-1));
		__this->set_m_ExpectedChallengeLength_7(((int32_t)16));
		__this->set_m_HandshakeCommand_8((WebSocketCommandInfo_t3536916738 *)NULL);
		__this->set_m_Challenges_9(((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16))));
		WebSocket_t713846903 * L_0 = ___websocket;
		IL2CPP_RUNTIME_CLASS_INIT(HandshakeReader_t1078082956_il2cpp_TypeInfo_var);
		HandshakeReader__ctor_m4265408334(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi00HandshakeReader::SetDataReader()
extern TypeInfo* DraftHybi00DataReader_t1509845496_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi00HandshakeReader_SetDataReader_m522541629_MetadataUsageId;
extern "C"  void DraftHybi00HandshakeReader_SetDataReader_m522541629 (DraftHybi00HandshakeReader_t1765220979 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00HandshakeReader_SetDataReader_m522541629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DraftHybi00DataReader_t1509845496 * L_0 = (DraftHybi00DataReader_t1509845496 *)il2cpp_codegen_object_new(DraftHybi00DataReader_t1509845496_il2cpp_TypeInfo_var);
		DraftHybi00DataReader__ctor_m2490528791(L_0, __this, /*hidden argument*/NULL);
		ReaderBase_set_NextCommandReader_m1719152039(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// WebSocket4Net.WebSocketCommandInfo WebSocket4Net.Protocol.DraftHybi00HandshakeReader::GetCommandInfo(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern TypeInfo* HandshakeReader_t1078082956_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi00HandshakeReader_GetCommandInfo_m2610356184_MetadataUsageId;
extern "C"  WebSocketCommandInfo_t3536916738 * DraftHybi00HandshakeReader_GetCommandInfo_m2610356184 (DraftHybi00HandshakeReader_t1765220979 * __this, ByteU5BU5D_t58506160* ___readBuffer, int32_t ___offset, int32_t ___length, int32_t* ___left, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00HandshakeReader_GetCommandInfo_m2610356184_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WebSocketCommandInfo_t3536916738 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	WebSocketCommandInfo_t3536916738 * V_4 = NULL;
	bool V_5 = false;
	{
		int32_t L_0 = __this->get_m_ReceivedChallengeLength_6();
		V_5 = (bool)((((int32_t)((((int32_t)L_0) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_5;
		if (L_1)
		{
			goto IL_0155;
		}
	}
	{
		ByteU5BU5D_t58506160* L_2 = ___readBuffer;
		int32_t L_3 = ___offset;
		int32_t L_4 = ___length;
		int32_t* L_5 = ___left;
		WebSocketCommandInfo_t3536916738 * L_6 = HandshakeReader_GetCommandInfo_m2339654721(__this, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		WebSocketCommandInfo_t3536916738 * L_7 = V_0;
		V_5 = (bool)((((int32_t)((((Il2CppObject*)(WebSocketCommandInfo_t3536916738 *)L_7) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_8 = V_5;
		if (L_8)
		{
			goto IL_0038;
		}
	}
	{
		V_4 = (WebSocketCommandInfo_t3536916738 *)NULL;
		goto IL_0236;
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HandshakeReader_t1078082956_il2cpp_TypeInfo_var);
		String_t* L_9 = ((HandshakeReader_t1078082956_StaticFields*)HandshakeReader_t1078082956_il2cpp_TypeInfo_var->static_fields)->get_BadRequestCode_3();
		WebSocketCommandInfo_t3536916738 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String WebSocket4Net.WebSocketCommandInfo::get_Key() */, L_10);
		NullCheck(L_9);
		bool L_12 = String_Equals_m3541721061(L_9, L_11, /*hidden argument*/NULL);
		V_5 = (bool)((((int32_t)L_12) == ((int32_t)0))? 1 : 0);
		bool L_13 = V_5;
		if (L_13)
		{
			goto IL_0059;
		}
	}
	{
		WebSocketCommandInfo_t3536916738 * L_14 = V_0;
		V_4 = L_14;
		goto IL_0236;
	}

IL_0059:
	{
		__this->set_m_ReceivedChallengeLength_6(0);
		WebSocketCommandInfo_t3536916738 * L_15 = V_0;
		__this->set_m_HandshakeCommand_8(L_15);
		int32_t L_16 = ___offset;
		int32_t L_17 = ___length;
		int32_t* L_18 = ___left;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_16+(int32_t)L_17))-(int32_t)(*((int32_t*)L_18))));
		int32_t* L_19 = ___left;
		int32_t L_20 = __this->get_m_ExpectedChallengeLength_7();
		V_5 = (bool)((((int32_t)((((int32_t)(*((int32_t*)L_19))) < ((int32_t)L_20))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_21 = V_5;
		if (L_21)
		{
			goto IL_00bc;
		}
	}
	{
		int32_t* L_22 = ___left;
		V_5 = (bool)((((int32_t)((((int32_t)(*((int32_t*)L_22))) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_23 = V_5;
		if (L_23)
		{
			goto IL_00b4;
		}
	}
	{
		ByteU5BU5D_t58506160* L_24 = ___readBuffer;
		int32_t L_25 = V_1;
		ByteU5BU5D_t58506160* L_26 = __this->get_m_Challenges_9();
		int32_t* L_27 = ___left;
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_24, L_25, (Il2CppArray *)(Il2CppArray *)L_26, 0, (*((int32_t*)L_27)), /*hidden argument*/NULL);
		int32_t* L_28 = ___left;
		__this->set_m_ReceivedChallengeLength_6((*((int32_t*)L_28)));
		int32_t* L_29 = ___left;
		*((int32_t*)(L_29)) = (int32_t)0;
	}

IL_00b4:
	{
		V_4 = (WebSocketCommandInfo_t3536916738 *)NULL;
		goto IL_0236;
	}

IL_00bc:
	{
		int32_t* L_30 = ___left;
		int32_t L_31 = __this->get_m_ExpectedChallengeLength_7();
		V_5 = (bool)((((int32_t)((((int32_t)(*((int32_t*)L_30))) == ((int32_t)L_31))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_32 = V_5;
		if (L_32)
		{
			goto IL_010d;
		}
	}
	{
		ByteU5BU5D_t58506160* L_33 = ___readBuffer;
		int32_t L_34 = V_1;
		ByteU5BU5D_t58506160* L_35 = __this->get_m_Challenges_9();
		int32_t* L_36 = ___left;
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_33, L_34, (Il2CppArray *)(Il2CppArray *)L_35, 0, (*((int32_t*)L_36)), /*hidden argument*/NULL);
		DraftHybi00HandshakeReader_SetDataReader_m522541629(__this, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_37 = __this->get_m_HandshakeCommand_8();
		ByteU5BU5D_t58506160* L_38 = __this->get_m_Challenges_9();
		NullCheck(L_37);
		WebSocketCommandInfo_set_Data_m2974951275(L_37, L_38, /*hidden argument*/NULL);
		int32_t* L_39 = ___left;
		*((int32_t*)(L_39)) = (int32_t)0;
		WebSocketCommandInfo_t3536916738 * L_40 = __this->get_m_HandshakeCommand_8();
		V_4 = L_40;
		goto IL_0236;
	}

IL_010d:
	{
		ByteU5BU5D_t58506160* L_41 = ___readBuffer;
		int32_t L_42 = V_1;
		ByteU5BU5D_t58506160* L_43 = __this->get_m_Challenges_9();
		int32_t L_44 = __this->get_m_ExpectedChallengeLength_7();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_41, L_42, (Il2CppArray *)(Il2CppArray *)L_43, 0, L_44, /*hidden argument*/NULL);
		int32_t* L_45 = ___left;
		int32_t* L_46 = L_45;
		int32_t L_47 = __this->get_m_ExpectedChallengeLength_7();
		*((int32_t*)(L_46)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_46))-(int32_t)L_47));
		DraftHybi00HandshakeReader_SetDataReader_m522541629(__this, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_48 = __this->get_m_HandshakeCommand_8();
		ByteU5BU5D_t58506160* L_49 = __this->get_m_Challenges_9();
		NullCheck(L_48);
		WebSocketCommandInfo_set_Data_m2974951275(L_48, L_49, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_50 = __this->get_m_HandshakeCommand_8();
		V_4 = L_50;
		goto IL_0236;
	}

IL_0155:
	{
		int32_t L_51 = __this->get_m_ReceivedChallengeLength_6();
		int32_t L_52 = ___length;
		V_2 = ((int32_t)((int32_t)L_51+(int32_t)L_52));
		int32_t L_53 = V_2;
		int32_t L_54 = __this->get_m_ExpectedChallengeLength_7();
		V_5 = (bool)((((int32_t)((((int32_t)L_53) < ((int32_t)L_54))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_55 = V_5;
		if (L_55)
		{
			goto IL_019a;
		}
	}
	{
		ByteU5BU5D_t58506160* L_56 = ___readBuffer;
		int32_t L_57 = ___offset;
		ByteU5BU5D_t58506160* L_58 = __this->get_m_Challenges_9();
		int32_t L_59 = __this->get_m_ReceivedChallengeLength_6();
		int32_t L_60 = ___length;
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_56, L_57, (Il2CppArray *)(Il2CppArray *)L_58, L_59, L_60, /*hidden argument*/NULL);
		int32_t* L_61 = ___left;
		*((int32_t*)(L_61)) = (int32_t)0;
		int32_t L_62 = V_2;
		__this->set_m_ReceivedChallengeLength_6(L_62);
		V_4 = (WebSocketCommandInfo_t3536916738 *)NULL;
		goto IL_0236;
	}

IL_019a:
	{
		int32_t L_63 = V_2;
		int32_t L_64 = __this->get_m_ExpectedChallengeLength_7();
		V_5 = (bool)((((int32_t)((((int32_t)L_63) == ((int32_t)L_64))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_65 = V_5;
		if (L_65)
		{
			goto IL_01e9;
		}
	}
	{
		ByteU5BU5D_t58506160* L_66 = ___readBuffer;
		int32_t L_67 = ___offset;
		ByteU5BU5D_t58506160* L_68 = __this->get_m_Challenges_9();
		int32_t L_69 = __this->get_m_ReceivedChallengeLength_6();
		int32_t L_70 = ___length;
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_66, L_67, (Il2CppArray *)(Il2CppArray *)L_68, L_69, L_70, /*hidden argument*/NULL);
		int32_t* L_71 = ___left;
		*((int32_t*)(L_71)) = (int32_t)0;
		DraftHybi00HandshakeReader_SetDataReader_m522541629(__this, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_72 = __this->get_m_HandshakeCommand_8();
		ByteU5BU5D_t58506160* L_73 = __this->get_m_Challenges_9();
		NullCheck(L_72);
		WebSocketCommandInfo_set_Data_m2974951275(L_72, L_73, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_74 = __this->get_m_HandshakeCommand_8();
		V_4 = L_74;
		goto IL_0236;
	}

IL_01e9:
	{
		int32_t L_75 = __this->get_m_ExpectedChallengeLength_7();
		int32_t L_76 = __this->get_m_ReceivedChallengeLength_6();
		V_3 = ((int32_t)((int32_t)L_75-(int32_t)L_76));
		ByteU5BU5D_t58506160* L_77 = ___readBuffer;
		int32_t L_78 = ___offset;
		ByteU5BU5D_t58506160* L_79 = __this->get_m_Challenges_9();
		int32_t L_80 = __this->get_m_ReceivedChallengeLength_6();
		int32_t L_81 = V_3;
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_77, L_78, (Il2CppArray *)(Il2CppArray *)L_79, L_80, L_81, /*hidden argument*/NULL);
		int32_t* L_82 = ___left;
		int32_t L_83 = ___length;
		int32_t L_84 = V_3;
		*((int32_t*)(L_82)) = (int32_t)((int32_t)((int32_t)L_83-(int32_t)L_84));
		DraftHybi00HandshakeReader_SetDataReader_m522541629(__this, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_85 = __this->get_m_HandshakeCommand_8();
		ByteU5BU5D_t58506160* L_86 = __this->get_m_Challenges_9();
		NullCheck(L_85);
		WebSocketCommandInfo_set_Data_m2974951275(L_85, L_86, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_87 = __this->get_m_HandshakeCommand_8();
		V_4 = L_87;
		goto IL_0236;
	}

IL_0236:
	{
		WebSocketCommandInfo_t3536916738 * L_88 = V_4;
		return L_88;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::.ctor()
extern TypeInfo* CloseStatusCodeHybi10_t635449120_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi00Processor__ctor_m3267995160_MetadataUsageId;
extern "C"  void DraftHybi00Processor__ctor_m3267995160 (DraftHybi00Processor_t470131883 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00Processor__ctor_m3267995160_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CloseStatusCodeHybi10_t635449120 * L_0 = (CloseStatusCodeHybi10_t635449120 *)il2cpp_codegen_object_new(CloseStatusCodeHybi10_t635449120_il2cpp_TypeInfo_var);
		CloseStatusCodeHybi10__ctor_m4111104231(L_0, /*hidden argument*/NULL);
		ProtocolProcessorBase__ctor_m448700961(__this, 0, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::.cctor()
extern TypeInfo* List_1_t3575665668_il2cpp_TypeInfo_var;
extern TypeInfo* DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var;
extern TypeInfo* Random_t922188920_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t2778706699_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3780427798_MethodInfo_var;
extern const uint32_t DraftHybi00Processor__cctor_m2041505941_MetadataUsageId;
extern "C"  void DraftHybi00Processor__cctor_m2041505941 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00Processor__cctor_m2041505941_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	uint16_t V_1 = 0x0;
	ByteU5BU5D_t58506160* V_2 = NULL;
	bool V_3 = false;
	{
		List_1_t3575665668 * L_0 = (List_1_t3575665668 *)il2cpp_codegen_object_new(List_1_t3575665668_il2cpp_TypeInfo_var);
		List_1__ctor_m3780427798(L_0, /*hidden argument*/List_1__ctor_m3780427798_MethodInfo_var);
		((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->set_m_CharLib_3(L_0);
		List_1_t3575665668 * L_1 = (List_1_t3575665668 *)il2cpp_codegen_object_new(List_1_t3575665668_il2cpp_TypeInfo_var);
		List_1__ctor_m3780427798(L_1, /*hidden argument*/List_1__ctor_m3780427798_MethodInfo_var);
		((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->set_m_DigLib_4(L_1);
		Random_t922188920 * L_2 = (Random_t922188920 *)il2cpp_codegen_object_new(Random_t922188920_il2cpp_TypeInfo_var);
		Random__ctor_m2490522898(L_2, /*hidden argument*/NULL);
		((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->set_m_Random_5(L_2);
		V_2 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)2));
		ByteU5BU5D_t58506160* L_3 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)((int32_t)255));
		ByteU5BU5D_t58506160* L_4 = V_2;
		((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->set_CloseHandshake_6(L_4);
		V_0 = ((int32_t)33);
		goto IL_0076;
	}

IL_0039:
	{
		int32_t L_5 = V_0;
		V_1 = (((int32_t)((uint16_t)L_5)));
		uint16_t L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2778706699_il2cpp_TypeInfo_var);
		bool L_7 = Char_IsLetter_m1699036490(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		bool L_8 = V_3;
		if (L_8)
		{
			goto IL_0058;
		}
	}
	{
		List_1_t3575665668 * L_9 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_m_CharLib_3();
		uint16_t L_10 = V_1;
		NullCheck(L_9);
		VirtActionInvoker1< uint16_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Char>::Add(!0) */, L_9, L_10);
		goto IL_0071;
	}

IL_0058:
	{
		uint16_t L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2778706699_il2cpp_TypeInfo_var);
		bool L_12 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_12) == ((int32_t)0))? 1 : 0);
		bool L_13 = V_3;
		if (L_13)
		{
			goto IL_0071;
		}
	}
	{
		List_1_t3575665668 * L_14 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_m_DigLib_4();
		uint16_t L_15 = V_1;
		NullCheck(L_14);
		VirtActionInvoker1< uint16_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Char>::Add(!0) */, L_14, L_15);
	}

IL_0071:
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0076:
	{
		int32_t L_17 = V_0;
		V_3 = (bool)((((int32_t)((((int32_t)L_17) > ((int32_t)((int32_t)126)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_18 = V_3;
		if (L_18)
		{
			goto IL_0039;
		}
	}
	{
		return;
	}
}
// WebSocket4Net.Protocol.ReaderBase WebSocket4Net.Protocol.DraftHybi00Processor::CreateHandshakeReader(WebSocket4Net.WebSocket)
extern TypeInfo* DraftHybi00HandshakeReader_t1765220979_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi00Processor_CreateHandshakeReader_m2629562542_MetadataUsageId;
extern "C"  ReaderBase_t893022310 * DraftHybi00Processor_CreateHandshakeReader_m2629562542 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00Processor_CreateHandshakeReader_m2629562542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ReaderBase_t893022310 * V_0 = NULL;
	{
		WebSocket_t713846903 * L_0 = ___websocket;
		DraftHybi00HandshakeReader_t1765220979 * L_1 = (DraftHybi00HandshakeReader_t1765220979 *)il2cpp_codegen_object_new(DraftHybi00HandshakeReader_t1765220979_il2cpp_TypeInfo_var);
		DraftHybi00HandshakeReader__ctor_m2012279385(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		ReaderBase_t893022310 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean WebSocket4Net.Protocol.DraftHybi00Processor::VerifyHandshake(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo,System.String&)
extern TypeInfo* Extensions_t39620836_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2761632010;
extern Il2CppCodeGenString* _stringLiteral4217613354;
extern Il2CppCodeGenString* _stringLiteral2154023182;
extern const uint32_t DraftHybi00Processor_VerifyHandshake_m2054616951_MetadataUsageId;
extern "C"  bool DraftHybi00Processor_VerifyHandshake_m2054616951 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, WebSocketCommandInfo_t3536916738 * ___handshakeInfo, String_t** ___description, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00Processor_VerifyHandshake_m2054616951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t58506160* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	bool V_3 = false;
	{
		WebSocketCommandInfo_t3536916738 * L_0 = ___handshakeInfo;
		NullCheck(L_0);
		ByteU5BU5D_t58506160* L_1 = WebSocketCommandInfo_get_Data_m4129809812(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t58506160* L_2 = V_0;
		NullCheck(L_2);
		ByteU5BU5D_t58506160* L_3 = V_0;
		NullCheck(L_3);
		V_3 = (bool)((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))))? 1 : 0);
		bool L_4 = V_3;
		if (L_4)
		{
			goto IL_0020;
		}
	}
	{
		String_t** L_5 = ___description;
		*((Il2CppObject **)(L_5)) = (Il2CppObject *)_stringLiteral2761632010;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_5), (Il2CppObject *)_stringLiteral2761632010);
		V_2 = (bool)0;
		goto IL_0082;
	}

IL_0020:
	{
		V_1 = 0;
		goto IL_0047;
	}

IL_0024:
	{
		ByteU5BU5D_t58506160* L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		ByteU5BU5D_t58506160* L_9 = __this->get_m_ExpectedChallenge_7();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		V_3 = (bool)((((int32_t)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8)))) == ((int32_t)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11)))))? 1 : 0);
		bool L_12 = V_3;
		if (L_12)
		{
			goto IL_0042;
		}
	}
	{
		String_t** L_13 = ___description;
		*((Il2CppObject **)(L_13)) = (Il2CppObject *)_stringLiteral4217613354;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_13), (Il2CppObject *)_stringLiteral4217613354);
		V_2 = (bool)0;
		goto IL_0082;
	}

IL_0042:
	{
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_15 = V_1;
		ByteU5BU5D_t58506160* L_16 = __this->get_m_ExpectedChallenge_7();
		NullCheck(L_16);
		V_3 = (bool)((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length))))))? 1 : 0);
		bool L_17 = V_3;
		if (L_17)
		{
			goto IL_0024;
		}
	}
	{
		WebSocketCommandInfo_t3536916738 * L_18 = ___handshakeInfo;
		NullCheck(L_18);
		String_t* L_19 = WebSocketCommandInfo_get_Text_m101435856(L_18, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_20 = ___websocket;
		NullCheck(L_20);
		Il2CppObject* L_21 = WebSocket_get_Items_m1025846418(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		bool L_22 = Extensions_ParseMimeHeader_m1946375443(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		bool L_23 = V_3;
		if (L_23)
		{
			goto IL_0077;
		}
	}
	{
		String_t** L_24 = ___description;
		*((Il2CppObject **)(L_24)) = (Il2CppObject *)_stringLiteral2154023182;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_24), (Il2CppObject *)_stringLiteral2154023182);
		V_2 = (bool)0;
		goto IL_0082;
	}

IL_0077:
	{
		String_t** L_25 = ___description;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_25)) = (Il2CppObject *)L_26;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_25), (Il2CppObject *)L_26);
		V_2 = (bool)1;
		goto IL_0082;
	}

IL_0082:
	{
		bool L_27 = V_2;
		return L_27;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::SendMessage(WebSocket4Net.WebSocket,System.String)
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi00Processor_SendMessage_m3239736752_MetadataUsageId;
extern "C"  void DraftHybi00Processor_SendMessage_m3239736752 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00Processor_SendMessage_m3239736752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t58506160* V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_0 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___message;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m2979997331(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_3 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(18 /* System.Int32 System.Text.Encoding::GetMaxByteCount(System.Int32) */, L_0, L_2);
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)2));
		int32_t L_4 = V_0;
		V_1 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_4));
		ByteU5BU5D_t58506160* L_5 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)0);
		Encoding_t180559927 * L_6 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = ___message;
		String_t* L_8 = ___message;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m2979997331(L_8, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_10 = V_1;
		NullCheck(L_6);
		int32_t L_11 = VirtFuncInvoker5< int32_t, String_t*, int32_t, int32_t, ByteU5BU5D_t58506160*, int32_t >::Invoke(9 /* System.Int32 System.Text.Encoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32) */, L_6, L_7, 0, L_9, L_10, 1);
		V_2 = L_11;
		ByteU5BU5D_t58506160* L_12 = V_1;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)((int32_t)1+(int32_t)L_13)));
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)1+(int32_t)L_13))), (uint8_t)((int32_t)255));
		WebSocket_t713846903 * L_14 = ___websocket;
		NullCheck(L_14);
		TcpClientSession_t1301539049 * L_15 = WebSocket_get_Client_m2705060304(L_14, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_16 = V_1;
		int32_t L_17 = V_2;
		NullCheck(L_15);
		VirtActionInvoker3< ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(5 /* System.Void SuperSocket.ClientEngine.ClientSession::Send(System.Byte[],System.Int32,System.Int32) */, L_15, L_16, 0, ((int32_t)((int32_t)L_17+(int32_t)2)));
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::SendCloseHandshake(WebSocket4Net.WebSocket,System.Int32,System.String)
extern TypeInfo* DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi00Processor_SendCloseHandshake_m1678578429_MetadataUsageId;
extern "C"  void DraftHybi00Processor_SendCloseHandshake_m1678578429 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, int32_t ___statusCode, String_t* ___closeReason, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00Processor_SendCloseHandshake_m1678578429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebSocket_t713846903 * L_0 = ___websocket;
		NullCheck(L_0);
		TcpClientSession_t1301539049 * L_1 = WebSocket_get_Client_m2705060304(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var);
		ByteU5BU5D_t58506160* L_2 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_CloseHandshake_6();
		ByteU5BU5D_t58506160* L_3 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_CloseHandshake_6();
		NullCheck(L_3);
		NullCheck(L_1);
		VirtActionInvoker3< ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(5 /* System.Void SuperSocket.ClientEngine.ClientSession::Send(System.Byte[],System.Int32,System.Int32) */, L_1, L_2, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))));
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::SendPing(WebSocket4Net.WebSocket,System.String)
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi00Processor_SendPing_m815999519_MetadataUsageId;
extern "C"  void DraftHybi00Processor_SendPing_m815999519 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, String_t* ___ping, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00Processor_SendPing_m815999519_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::SendPong(WebSocket4Net.WebSocket,System.String)
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi00Processor_SendPong_m1560439001_MetadataUsageId;
extern "C"  void DraftHybi00Processor_SendPong_m1560439001 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, String_t* ___pong, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00Processor_SendPong_m1560439001_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::SendHandshake(WebSocket4Net.WebSocket)
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern TypeInfo* Extensions_t39620836_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t2776692961_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2284748262_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m753985657_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1226428783;
extern Il2CppCodeGenString* _stringLiteral2510013413;
extern Il2CppCodeGenString* _stringLiteral467998424;
extern Il2CppCodeGenString* _stringLiteral3219802970;
extern Il2CppCodeGenString* _stringLiteral3219803931;
extern Il2CppCodeGenString* _stringLiteral2167348974;
extern Il2CppCodeGenString* _stringLiteral1443686636;
extern Il2CppCodeGenString* _stringLiteral1433713088;
extern Il2CppCodeGenString* _stringLiteral61;
extern Il2CppCodeGenString* _stringLiteral3812715690;
extern Il2CppCodeGenString* _stringLiteral59;
extern Il2CppCodeGenString* _stringLiteral2901091385;
extern const uint32_t DraftHybi00Processor_SendHandshake_m334946756_MetadataUsageId;
extern "C"  void DraftHybi00Processor_SendHandshake_m334946756 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00Processor_SendHandshake_m334946756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	ByteU5BU5D_t58506160* V_2 = NULL;
	StringBuilder_t3822575854 * V_3 = NULL;
	List_1_t2891677073 * V_4 = NULL;
	StringU5BU5D_t2956870243* V_5 = NULL;
	int32_t V_6 = 0;
	KeyValuePair_2_t2094718104  V_7;
	memset(&V_7, 0, sizeof(V_7));
	ByteU5BU5D_t58506160* V_8 = NULL;
	bool V_9 = false;
	ObjectU5BU5D_t11523773* V_10 = NULL;
	StringBuilder_t3822575854 * G_B2_0 = NULL;
	StringBuilder_t3822575854 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	StringBuilder_t3822575854 * G_B3_1 = NULL;
	int32_t G_B8_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_0 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_1 = DraftHybi00Processor_GenerateSecKey_m1117508773(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t58506160* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_0, L_1);
		V_0 = L_2;
		Encoding_t180559927 * L_3 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_4 = DraftHybi00Processor_GenerateSecKey_m1117508773(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t58506160* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_4);
		V_1 = L_5;
		ByteU5BU5D_t58506160* L_6 = DraftHybi00Processor_GenerateSecKey_m2222897782(__this, 8, /*hidden argument*/NULL);
		V_2 = L_6;
		String_t* L_7 = V_0;
		String_t* L_8 = V_1;
		ByteU5BU5D_t58506160* L_9 = V_2;
		ByteU5BU5D_t58506160* L_10 = DraftHybi00Processor_GetResponseSecurityKey_m3262218871(__this, L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->set_m_ExpectedChallenge_7(L_10);
		StringBuilder_t3822575854 * L_11 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_11, /*hidden argument*/NULL);
		V_3 = L_11;
		StringBuilder_t3822575854 * L_12 = V_3;
		WebSocket_t713846903 * L_13 = ___websocket;
		NullCheck(L_13);
		Uri_t2776692961 * L_14 = WebSocket_get_TargetUri_m4211545892(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = Uri_get_PathAndQuery_m3621173943(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_AppendFormatWithCrCf_m759365938(NULL /*static, unused*/, L_12, _stringLiteral1226428783, L_15, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_16 = V_3;
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_16, _stringLiteral2510013413, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_17 = V_3;
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_17, _stringLiteral467998424, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_18 = V_3;
		NullCheck(L_18);
		StringBuilder_Append_m3898090075(L_18, _stringLiteral3219802970, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_19 = V_3;
		String_t* L_20 = V_0;
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_21 = V_3;
		NullCheck(L_21);
		StringBuilder_Append_m3898090075(L_21, _stringLiteral3219803931, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_22 = V_3;
		String_t* L_23 = V_1;
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_24 = V_3;
		NullCheck(L_24);
		StringBuilder_Append_m3898090075(L_24, _stringLiteral2167348974, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_25 = V_3;
		WebSocket_t713846903 * L_26 = ___websocket;
		NullCheck(L_26);
		Uri_t2776692961 * L_27 = WebSocket_get_TargetUri_m4211545892(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		String_t* L_28 = Uri_get_Host_m1446697833(L_27, /*hidden argument*/NULL);
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_25, L_28, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_29 = V_3;
		NullCheck(L_29);
		StringBuilder_Append_m3898090075(L_29, _stringLiteral1443686636, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_30 = V_3;
		WebSocket_t713846903 * L_31 = ___websocket;
		NullCheck(L_31);
		String_t* L_32 = WebSocket_get_Origin_m775398224(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_33 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		G_B1_0 = L_30;
		if (L_33)
		{
			G_B2_0 = L_30;
			goto IL_00d7;
		}
	}
	{
		WebSocket_t713846903 * L_34 = ___websocket;
		NullCheck(L_34);
		String_t* L_35 = WebSocket_get_Origin_m775398224(L_34, /*hidden argument*/NULL);
		G_B3_0 = L_35;
		G_B3_1 = G_B1_0;
		goto IL_00e2;
	}

IL_00d7:
	{
		WebSocket_t713846903 * L_36 = ___websocket;
		NullCheck(L_36);
		Uri_t2776692961 * L_37 = WebSocket_get_TargetUri_m4211545892(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		String_t* L_38 = Uri_get_Host_m1446697833(L_37, /*hidden argument*/NULL);
		G_B3_0 = L_38;
		G_B3_1 = G_B2_0;
	}

IL_00e2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_39 = ___websocket;
		NullCheck(L_39);
		String_t* L_40 = WebSocket_get_SubProtocol_m1721628912(L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_41 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		V_9 = L_41;
		bool L_42 = V_9;
		if (L_42)
		{
			goto IL_0114;
		}
	}
	{
		StringBuilder_t3822575854 * L_43 = V_3;
		NullCheck(L_43);
		StringBuilder_Append_m3898090075(L_43, _stringLiteral1433713088, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_44 = V_3;
		WebSocket_t713846903 * L_45 = ___websocket;
		NullCheck(L_45);
		String_t* L_46 = WebSocket_get_SubProtocol_m1721628912(L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_44, L_46, /*hidden argument*/NULL);
	}

IL_0114:
	{
		WebSocket_t713846903 * L_47 = ___websocket;
		NullCheck(L_47);
		List_1_t2891677073 * L_48 = WebSocket_get_Cookies_m794256048(L_47, /*hidden argument*/NULL);
		V_4 = L_48;
		List_1_t2891677073 * L_49 = V_4;
		if (!L_49)
		{
			goto IL_012f;
		}
	}
	{
		List_1_t2891677073 * L_50 = V_4;
		NullCheck(L_50);
		int32_t L_51 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Count() */, L_50);
		G_B8_0 = ((((int32_t)((((int32_t)L_51) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0130;
	}

IL_012f:
	{
		G_B8_0 = 1;
	}

IL_0130:
	{
		V_9 = (bool)G_B8_0;
		bool L_52 = V_9;
		if (L_52)
		{
			goto IL_01b0;
		}
	}
	{
		List_1_t2891677073 * L_53 = V_4;
		NullCheck(L_53);
		int32_t L_54 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Count() */, L_53);
		V_5 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_54));
		V_6 = 0;
		goto IL_017f;
	}

IL_014a:
	{
		List_1_t2891677073 * L_55 = V_4;
		int32_t L_56 = V_6;
		NullCheck(L_55);
		KeyValuePair_2_t2094718104  L_57 = VirtFuncInvoker1< KeyValuePair_2_t2094718104 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Item(System.Int32) */, L_55, L_56);
		V_7 = L_57;
		StringU5BU5D_t2956870243* L_58 = V_5;
		int32_t L_59 = V_6;
		String_t* L_60 = KeyValuePair_2_get_Key_m2284748262((&V_7), /*hidden argument*/KeyValuePair_2_get_Key_m2284748262_MethodInfo_var);
		String_t* L_61 = KeyValuePair_2_get_Value_m753985657((&V_7), /*hidden argument*/KeyValuePair_2_get_Value_m753985657_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t2776692961_il2cpp_TypeInfo_var);
		String_t* L_62 = Uri_EscapeUriString_m2040573756(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_63 = String_Concat_m1825781833(NULL /*static, unused*/, L_60, _stringLiteral61, L_62, /*hidden argument*/NULL);
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_59);
		ArrayElementTypeCheck (L_58, L_63);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(L_59), (String_t*)L_63);
		int32_t L_64 = V_6;
		V_6 = ((int32_t)((int32_t)L_64+(int32_t)1));
	}

IL_017f:
	{
		int32_t L_65 = V_6;
		List_1_t2891677073 * L_66 = V_4;
		NullCheck(L_66);
		int32_t L_67 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Count() */, L_66);
		V_9 = (bool)((((int32_t)L_65) < ((int32_t)L_67))? 1 : 0);
		bool L_68 = V_9;
		if (L_68)
		{
			goto IL_014a;
		}
	}
	{
		StringBuilder_t3822575854 * L_69 = V_3;
		NullCheck(L_69);
		StringBuilder_Append_m3898090075(L_69, _stringLiteral3812715690, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_70 = V_3;
		StringU5BU5D_t2956870243* L_71 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_72 = String_Join_m2789530325(NULL /*static, unused*/, _stringLiteral59, L_71, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_70, L_72, /*hidden argument*/NULL);
	}

IL_01b0:
	{
		WebSocket_t713846903 * L_73 = ___websocket;
		NullCheck(L_73);
		List_1_t2891677073 * L_74 = WebSocket_get_CustomHeaderItems_m1966151779(L_73, /*hidden argument*/NULL);
		V_9 = (bool)((((Il2CppObject*)(List_1_t2891677073 *)L_74) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_75 = V_9;
		if (L_75)
		{
			goto IL_021e;
		}
	}
	{
		V_6 = 0;
		goto IL_0208;
	}

IL_01c5:
	{
		WebSocket_t713846903 * L_76 = ___websocket;
		NullCheck(L_76);
		List_1_t2891677073 * L_77 = WebSocket_get_CustomHeaderItems_m1966151779(L_76, /*hidden argument*/NULL);
		int32_t L_78 = V_6;
		NullCheck(L_77);
		KeyValuePair_2_t2094718104  L_79 = VirtFuncInvoker1< KeyValuePair_2_t2094718104 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Item(System.Int32) */, L_77, L_78);
		V_7 = L_79;
		StringBuilder_t3822575854 * L_80 = V_3;
		V_10 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		ObjectU5BU5D_t11523773* L_81 = V_10;
		String_t* L_82 = KeyValuePair_2_get_Key_m2284748262((&V_7), /*hidden argument*/KeyValuePair_2_get_Key_m2284748262_MethodInfo_var);
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, 0);
		ArrayElementTypeCheck (L_81, L_82);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_82);
		ObjectU5BU5D_t11523773* L_83 = V_10;
		String_t* L_84 = KeyValuePair_2_get_Value_m753985657((&V_7), /*hidden argument*/KeyValuePair_2_get_Value_m753985657_MethodInfo_var);
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, 1);
		ArrayElementTypeCheck (L_83, L_84);
		(L_83)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_84);
		ObjectU5BU5D_t11523773* L_85 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_AppendFormatWithCrCf_m3901244368(NULL /*static, unused*/, L_80, _stringLiteral2901091385, L_85, /*hidden argument*/NULL);
		int32_t L_86 = V_6;
		V_6 = ((int32_t)((int32_t)L_86+(int32_t)1));
	}

IL_0208:
	{
		int32_t L_87 = V_6;
		WebSocket_t713846903 * L_88 = ___websocket;
		NullCheck(L_88);
		List_1_t2891677073 * L_89 = WebSocket_get_CustomHeaderItems_m1966151779(L_88, /*hidden argument*/NULL);
		NullCheck(L_89);
		int32_t L_90 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Count() */, L_89);
		V_9 = (bool)((((int32_t)L_87) < ((int32_t)L_90))? 1 : 0);
		bool L_91 = V_9;
		if (L_91)
		{
			goto IL_01c5;
		}
	}
	{
	}

IL_021e:
	{
		StringBuilder_t3822575854 * L_92 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_AppendWithCrCf_m3929176351(NULL /*static, unused*/, L_92, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_93 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_94 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_95 = V_2;
		ByteU5BU5D_t58506160* L_96 = V_2;
		NullCheck(L_96);
		NullCheck(L_94);
		String_t* L_97 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_94, L_95, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_96)->max_length)))));
		NullCheck(L_93);
		StringBuilder_Append_m3898090075(L_93, L_97, /*hidden argument*/NULL);
		Encoding_t180559927 * L_98 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_99 = V_3;
		NullCheck(L_99);
		String_t* L_100 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_99);
		NullCheck(L_98);
		ByteU5BU5D_t58506160* L_101 = VirtFuncInvoker1< ByteU5BU5D_t58506160*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_98, L_100);
		V_8 = L_101;
		WebSocket_t713846903 * L_102 = ___websocket;
		NullCheck(L_102);
		TcpClientSession_t1301539049 * L_103 = WebSocket_get_Client_m2705060304(L_102, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_104 = V_8;
		ByteU5BU5D_t58506160* L_105 = V_8;
		NullCheck(L_105);
		NullCheck(L_103);
		VirtActionInvoker3< ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(5 /* System.Void SuperSocket.ClientEngine.ClientSession::Send(System.Byte[],System.Int32,System.Int32) */, L_103, L_104, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_105)->max_length)))));
		return;
	}
}
// System.Byte[] WebSocket4Net.Protocol.DraftHybi00Processor::GetResponseSecurityKey(System.String,System.String,System.Byte[])
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Regex_t3802381858_il2cpp_TypeInfo_var;
extern TypeInfo* DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t620049934_il2cpp_TypeInfo_var;
extern TypeInfo* BitConverter_t3338308296_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern const MethodInfo* DraftHybi00Processor_U3CGetResponseSecurityKeyU3Eb__0_m754971777_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m331937744_MethodInfo_var;
extern const MethodInfo* Enumerable_Count_TisChar_t2778706699_m2693248879_MethodInfo_var;
extern const MethodInfo* DraftHybi00Processor_U3CGetResponseSecurityKeyU3Eb__1_m249413152_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2693538788;
extern const uint32_t DraftHybi00Processor_GetResponseSecurityKey_m3262218871_MetadataUsageId;
extern "C"  ByteU5BU5D_t58506160* DraftHybi00Processor_GetResponseSecurityKey_m3262218871 (DraftHybi00Processor_t470131883 * __this, String_t* ___secKey1, String_t* ___secKey2, ByteU5BU5D_t58506160* ___secKey3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00Processor_GetResponseSecurityKey_m3262218871_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int64_t V_2 = 0;
	int64_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	ByteU5BU5D_t58506160* V_8 = NULL;
	ByteU5BU5D_t58506160* V_9 = NULL;
	ByteU5BU5D_t58506160* V_10 = NULL;
	ByteU5BU5D_t58506160* V_11 = NULL;
	ByteU5BU5D_t58506160* V_12 = NULL;
	ByteU5BU5D_t58506160* V_13 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = ___secKey1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t3802381858_il2cpp_TypeInfo_var);
		String_t* L_2 = Regex_Replace_m2658391113(NULL /*static, unused*/, L_0, _stringLiteral2693538788, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___secKey2;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_5 = Regex_Replace_m2658391113(NULL /*static, unused*/, L_3, _stringLiteral2693538788, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		String_t* L_6 = V_0;
		int64_t L_7 = Int64_Parse_m2426231402(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		String_t* L_8 = V_1;
		int64_t L_9 = Int64_Parse_m2426231402(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		String_t* L_10 = ___secKey1;
		IL2CPP_RUNTIME_CLASS_INIT(DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var);
		Func_2_t620049934 * L_11 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8();
		G_B1_0 = L_10;
		if (L_11)
		{
			G_B2_0 = L_10;
			goto IL_004c;
		}
	}
	{
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)DraftHybi00Processor_U3CGetResponseSecurityKeyU3Eb__0_m754971777_MethodInfo_var);
		Func_2_t620049934 * L_13 = (Func_2_t620049934 *)il2cpp_codegen_object_new(Func_2_t620049934_il2cpp_TypeInfo_var);
		Func_2__ctor_m331937744(L_13, NULL, L_12, /*hidden argument*/Func_2__ctor_m331937744_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var);
		((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8(L_13);
		G_B2_0 = G_B1_0;
		goto IL_004c;
	}

IL_004c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var);
		Func_2_t620049934 * L_14 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8();
		int32_t L_15 = Enumerable_Count_TisChar_t2778706699_m2693248879(NULL /*static, unused*/, G_B2_0, L_14, /*hidden argument*/Enumerable_Count_TisChar_t2778706699_m2693248879_MethodInfo_var);
		V_4 = L_15;
		String_t* L_16 = ___secKey2;
		Func_2_t620049934 * L_17 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9();
		G_B3_0 = L_16;
		if (L_17)
		{
			G_B4_0 = L_16;
			goto IL_0073;
		}
	}
	{
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)DraftHybi00Processor_U3CGetResponseSecurityKeyU3Eb__1_m249413152_MethodInfo_var);
		Func_2_t620049934 * L_19 = (Func_2_t620049934 *)il2cpp_codegen_object_new(Func_2_t620049934_il2cpp_TypeInfo_var);
		Func_2__ctor_m331937744(L_19, NULL, L_18, /*hidden argument*/Func_2__ctor_m331937744_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var);
		((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->set_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9(L_19);
		G_B4_0 = G_B3_0;
		goto IL_0073;
	}

IL_0073:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var);
		Func_2_t620049934 * L_20 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9();
		int32_t L_21 = Enumerable_Count_TisChar_t2778706699_m2693248879(NULL /*static, unused*/, G_B4_0, L_20, /*hidden argument*/Enumerable_Count_TisChar_t2778706699_m2693248879_MethodInfo_var);
		V_5 = L_21;
		int64_t L_22 = V_2;
		int32_t L_23 = V_4;
		V_6 = (((int32_t)((int32_t)((int64_t)((int64_t)L_22/(int64_t)(((int64_t)((int64_t)L_23))))))));
		int64_t L_24 = V_3;
		int32_t L_25 = V_5;
		V_7 = (((int32_t)((int32_t)((int64_t)((int64_t)L_24/(int64_t)(((int64_t)((int64_t)L_25))))))));
		int32_t L_26 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3338308296_il2cpp_TypeInfo_var);
		ByteU5BU5D_t58506160* L_27 = BitConverter_GetBytes_m324200094(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		V_8 = L_27;
		ByteU5BU5D_t58506160* L_28 = V_8;
		Array_Reverse_m3430038334(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_28, /*hidden argument*/NULL);
		int32_t L_29 = V_7;
		ByteU5BU5D_t58506160* L_30 = BitConverter_GetBytes_m324200094(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		V_9 = L_30;
		ByteU5BU5D_t58506160* L_31 = V_9;
		Array_Reverse_m3430038334(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_31, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_32 = ___secKey3;
		V_10 = L_32;
		ByteU5BU5D_t58506160* L_33 = V_8;
		NullCheck(L_33);
		ByteU5BU5D_t58506160* L_34 = V_9;
		NullCheck(L_34);
		ByteU5BU5D_t58506160* L_35 = V_10;
		NullCheck(L_35);
		V_11 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length))))+(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length))))))+(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length))))))));
		ByteU5BU5D_t58506160* L_36 = V_8;
		ByteU5BU5D_t58506160* L_37 = V_11;
		ByteU5BU5D_t58506160* L_38 = V_8;
		NullCheck(L_38);
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_36, 0, (Il2CppArray *)(Il2CppArray *)L_37, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_38)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_39 = V_9;
		ByteU5BU5D_t58506160* L_40 = V_11;
		ByteU5BU5D_t58506160* L_41 = V_8;
		NullCheck(L_41);
		ByteU5BU5D_t58506160* L_42 = V_9;
		NullCheck(L_42);
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_39, 0, (Il2CppArray *)(Il2CppArray *)L_40, (((int32_t)((int32_t)(((Il2CppArray *)L_41)->max_length)))), (((int32_t)((int32_t)(((Il2CppArray *)L_42)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_43 = V_10;
		ByteU5BU5D_t58506160* L_44 = V_11;
		ByteU5BU5D_t58506160* L_45 = V_8;
		NullCheck(L_45);
		ByteU5BU5D_t58506160* L_46 = V_9;
		NullCheck(L_46);
		ByteU5BU5D_t58506160* L_47 = V_10;
		NullCheck(L_47);
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_43, 0, (Il2CppArray *)(Il2CppArray *)L_44, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))))+(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_46)->max_length)))))), (((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))), /*hidden argument*/NULL);
		MD5_t1557559991 * L_48 = MD5_Create_m3551683961(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_49 = V_11;
		NullCheck(L_48);
		ByteU5BU5D_t58506160* L_50 = HashAlgorithm_ComputeHash_m1325366732(L_48, L_49, /*hidden argument*/NULL);
		V_12 = L_50;
		ByteU5BU5D_t58506160* L_51 = V_12;
		V_13 = L_51;
		goto IL_0118;
	}

IL_0118:
	{
		ByteU5BU5D_t58506160* L_52 = V_13;
		return L_52;
	}
}
// System.Byte[] WebSocket4Net.Protocol.DraftHybi00Processor::GenerateSecKey()
extern TypeInfo* DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi00Processor_GenerateSecKey_m1117508773_MetadataUsageId;
extern "C"  ByteU5BU5D_t58506160* DraftHybi00Processor_GenerateSecKey_m1117508773 (DraftHybi00Processor_t470131883 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00Processor_GenerateSecKey_m1117508773_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t58506160* V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var);
		Random_t922188920 * L_0 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_m_Random_5();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_0, ((int32_t)10), ((int32_t)20));
		V_0 = L_1;
		int32_t L_2 = V_0;
		ByteU5BU5D_t58506160* L_3 = DraftHybi00Processor_GenerateSecKey_m2222897782(__this, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_001a;
	}

IL_001a:
	{
		ByteU5BU5D_t58506160* L_4 = V_1;
		return L_4;
	}
}
// System.Byte[] WebSocket4Net.Protocol.DraftHybi00Processor::GenerateSecKey(System.Int32)
extern TypeInfo* DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern TypeInfo* Extensions_t80775955_il2cpp_TypeInfo_var;
extern const MethodInfo* Extensions_RandomOrder_TisByte_t2778693821_m2917735209_MethodInfo_var;
extern const uint32_t DraftHybi00Processor_GenerateSecKey_m2222897782_MetadataUsageId;
extern "C"  ByteU5BU5D_t58506160* DraftHybi00Processor_GenerateSecKey_m2222897782 (DraftHybi00Processor_t470131883 * __this, int32_t ___totalLen, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi00Processor_GenerateSecKey_m2222897782_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ByteU5BU5D_t58506160* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	ByteU5BU5D_t58506160* V_6 = NULL;
	bool V_7 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var);
		Random_t922188920 * L_0 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_m_Random_5();
		int32_t L_1 = ___totalLen;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_0, 1, ((int32_t)((int32_t)((int32_t)((int32_t)L_1/(int32_t)2))+(int32_t)1)));
		V_0 = L_2;
		Random_t922188920 * L_3 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_m_Random_5();
		int32_t L_4 = ___totalLen;
		int32_t L_5 = V_0;
		NullCheck(L_3);
		int32_t L_6 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_3, 3, ((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)1))-(int32_t)L_5)));
		V_1 = L_6;
		int32_t L_7 = ___totalLen;
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)L_7-(int32_t)L_8))-(int32_t)L_9));
		int32_t L_10 = ___totalLen;
		V_3 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_10));
		V_4 = 0;
		V_5 = 0;
		goto IL_0049;
	}

IL_0038:
	{
		ByteU5BU5D_t58506160* L_11 = V_3;
		int32_t L_12 = V_4;
		int32_t L_13 = L_12;
		V_4 = ((int32_t)((int32_t)L_13+(int32_t)1));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (uint8_t)((int32_t)32));
		int32_t L_14 = V_5;
		V_5 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_15 = V_5;
		int32_t L_16 = V_0;
		V_7 = (bool)((((int32_t)L_15) < ((int32_t)L_16))? 1 : 0);
		bool L_17 = V_7;
		if (L_17)
		{
			goto IL_0038;
		}
	}
	{
		V_5 = 0;
		goto IL_008c;
	}

IL_0059:
	{
		ByteU5BU5D_t58506160* L_18 = V_3;
		int32_t L_19 = V_4;
		int32_t L_20 = L_19;
		V_4 = ((int32_t)((int32_t)L_20+(int32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var);
		List_1_t3575665668 * L_21 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_m_CharLib_3();
		Random_t922188920 * L_22 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_m_Random_5();
		List_1_t3575665668 * L_23 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_m_CharLib_3();
		NullCheck(L_23);
		int32_t L_24 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Char>::get_Count() */, L_23);
		NullCheck(L_22);
		int32_t L_25 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_22, 0, ((int32_t)((int32_t)L_24-(int32_t)1)));
		NullCheck(L_21);
		uint16_t L_26 = VirtFuncInvoker1< uint16_t, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Char>::get_Item(System.Int32) */, L_21, L_25);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (uint8_t)(((int32_t)((uint8_t)L_26))));
		int32_t L_27 = V_5;
		V_5 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_5;
		int32_t L_29 = V_1;
		V_7 = (bool)((((int32_t)L_28) < ((int32_t)L_29))? 1 : 0);
		bool L_30 = V_7;
		if (L_30)
		{
			goto IL_0059;
		}
	}
	{
		V_5 = 0;
		goto IL_00cf;
	}

IL_009c:
	{
		ByteU5BU5D_t58506160* L_31 = V_3;
		int32_t L_32 = V_4;
		int32_t L_33 = L_32;
		V_4 = ((int32_t)((int32_t)L_33+(int32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var);
		List_1_t3575665668 * L_34 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_m_DigLib_4();
		Random_t922188920 * L_35 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_m_Random_5();
		List_1_t3575665668 * L_36 = ((DraftHybi00Processor_t470131883_StaticFields*)DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var->static_fields)->get_m_DigLib_4();
		NullCheck(L_36);
		int32_t L_37 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Char>::get_Count() */, L_36);
		NullCheck(L_35);
		int32_t L_38 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_35, 0, ((int32_t)((int32_t)L_37-(int32_t)1)));
		NullCheck(L_34);
		uint16_t L_39 = VirtFuncInvoker1< uint16_t, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Char>::get_Item(System.Int32) */, L_34, L_38);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_33);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_33), (uint8_t)(((int32_t)((uint8_t)L_39))));
		int32_t L_40 = V_5;
		V_5 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00cf:
	{
		int32_t L_41 = V_5;
		int32_t L_42 = V_2;
		V_7 = (bool)((((int32_t)L_41) < ((int32_t)L_42))? 1 : 0);
		bool L_43 = V_7;
		if (L_43)
		{
			goto IL_009c;
		}
	}
	{
		ByteU5BU5D_t58506160* L_44 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t80775955_il2cpp_TypeInfo_var);
		ByteU5BU5D_t58506160* L_45 = Extensions_RandomOrder_TisByte_t2778693821_m2917735209(NULL /*static, unused*/, L_44, /*hidden argument*/Extensions_RandomOrder_TisByte_t2778693821_m2917735209_MethodInfo_var);
		V_6 = L_45;
		goto IL_00e4;
	}

IL_00e4:
	{
		ByteU5BU5D_t58506160* L_46 = V_6;
		return L_46;
	}
}
// System.Boolean WebSocket4Net.Protocol.DraftHybi00Processor::get_SupportPingPong()
extern "C"  bool DraftHybi00Processor_get_SupportPingPong_m2315093118 (DraftHybi00Processor_t470131883 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		goto IL_0005;
	}

IL_0005:
	{
		bool L_0 = V_0;
		return L_0;
	}
}
// System.Boolean WebSocket4Net.Protocol.DraftHybi00Processor::<GetResponseSecurityKey>b__0(System.Char)
extern "C"  bool DraftHybi00Processor_U3CGetResponseSecurityKeyU3Eb__0_m754971777 (Il2CppObject * __this /* static, unused */, uint16_t ___c, const MethodInfo* method)
{
	bool V_0 = false;
	{
		uint16_t L_0 = ___c;
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)((int32_t)32)))? 1 : 0);
		goto IL_0008;
	}

IL_0008:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean WebSocket4Net.Protocol.DraftHybi00Processor::<GetResponseSecurityKey>b__1(System.Char)
extern "C"  bool DraftHybi00Processor_U3CGetResponseSecurityKeyU3Eb__1_m249413152 (Il2CppObject * __this /* static, unused */, uint16_t ___c, const MethodInfo* method)
{
	bool V_0 = false;
	{
		uint16_t L_0 = ___c;
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)((int32_t)32)))? 1 : 0);
		goto IL_0008;
	}

IL_0008:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10DataReader::.ctor()
extern TypeInfo* ArraySegmentList_t1783467835_il2cpp_TypeInfo_var;
extern TypeInfo* WebSocketDataFrame_t222733806_il2cpp_TypeInfo_var;
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi10DataReader__ctor_m1356719760_MetadataUsageId;
extern "C"  void DraftHybi10DataReader__ctor_m1356719760 (DraftHybi10DataReader_t1638928215 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10DataReader__ctor_m1356719760_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_LastPartLength_3(0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		ArraySegmentList_t1783467835 * L_0 = (ArraySegmentList_t1783467835 *)il2cpp_codegen_object_new(ArraySegmentList_t1783467835_il2cpp_TypeInfo_var);
		ArraySegmentList__ctor_m3332344921(L_0, /*hidden argument*/NULL);
		WebSocketDataFrame_t222733806 * L_1 = (WebSocketDataFrame_t222733806 *)il2cpp_codegen_object_new(WebSocketDataFrame_t222733806_il2cpp_TypeInfo_var);
		WebSocketDataFrame__ctor_m2101584072(L_1, L_0, /*hidden argument*/NULL);
		__this->set_m_Frame_1(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = DataFramePartReader_get_NewReader_m3969627167(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_PartReader_2(L_2);
		return;
	}
}
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo> WebSocket4Net.Protocol.DraftHybi10DataReader::get_NextCommandReader()
extern "C"  Il2CppObject* DraftHybi10DataReader_get_NextCommandReader_m1553728881 (DraftHybi10DataReader_t1638928215 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		V_0 = __this;
		goto IL_0005;
	}

IL_0005:
	{
		Il2CppObject* L_0 = V_0;
		return L_0;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10DataReader::AddArraySegment(SuperSocket.ClientEngine.Protocol.ArraySegmentList,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern const MethodInfo* ArraySegmentList_1_AddSegment_m727323212_MethodInfo_var;
extern const uint32_t DraftHybi10DataReader_AddArraySegment_m545309278_MetadataUsageId;
extern "C"  void DraftHybi10DataReader_AddArraySegment_m545309278 (DraftHybi10DataReader_t1638928215 * __this, ArraySegmentList_t1783467835 * ___segments, ByteU5BU5D_t58506160* ___buffer, int32_t ___offset, int32_t ___length, bool ___isReusableBuffer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10DataReader_AddArraySegment_m545309278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArraySegmentList_t1783467835 * L_0 = ___segments;
		ByteU5BU5D_t58506160* L_1 = ___buffer;
		int32_t L_2 = ___offset;
		int32_t L_3 = ___length;
		bool L_4 = ___isReusableBuffer;
		NullCheck(L_0);
		ArraySegmentList_1_AddSegment_m727323212(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/ArraySegmentList_1_AddSegment_m727323212_MethodInfo_var);
		return;
	}
}
// WebSocket4Net.WebSocketCommandInfo WebSocket4Net.Protocol.DraftHybi10DataReader::GetCommandInfo(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern TypeInfo* IDataFramePartReader_t2102555929_il2cpp_TypeInfo_var;
extern TypeInfo* ArraySegmentList_t1783467835_il2cpp_TypeInfo_var;
extern TypeInfo* WebSocketDataFrame_t222733806_il2cpp_TypeInfo_var;
extern TypeInfo* WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1019692775_il2cpp_TypeInfo_var;
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegmentList_1_TrimEnd_m2413284860_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m906535829_MethodInfo_var;
extern const uint32_t DraftHybi10DataReader_GetCommandInfo_m3786662742_MetadataUsageId;
extern "C"  WebSocketCommandInfo_t3536916738 * DraftHybi10DataReader_GetCommandInfo_m3786662742 (DraftHybi10DataReader_t1638928215 * __this, ByteU5BU5D_t58506160* ___readBuffer, int32_t ___offset, int32_t ___length, int32_t* ___left, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10DataReader_GetCommandInfo_m3786662742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	WebSocketCommandInfo_t3536916738 * V_2 = NULL;
	WebSocketCommandInfo_t3536916738 * V_3 = NULL;
	bool V_4 = false;
	int32_t G_B9_0 = 0;
	{
		WebSocketDataFrame_t222733806 * L_0 = __this->get_m_Frame_1();
		NullCheck(L_0);
		ArraySegmentList_t1783467835 * L_1 = WebSocketDataFrame_get_InnerData_m815354334(L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_2 = ___readBuffer;
		int32_t L_3 = ___offset;
		int32_t L_4 = ___length;
		DraftHybi10DataReader_AddArraySegment_m545309278(__this, L_1, L_2, L_3, L_4, (bool)1, /*hidden argument*/NULL);
		Il2CppObject * L_5 = __this->get_m_PartReader_2();
		int32_t L_6 = __this->get_m_LastPartLength_3();
		WebSocketDataFrame_t222733806 * L_7 = __this->get_m_Frame_1();
		NullCheck(L_5);
		int32_t L_8 = InterfaceFuncInvoker3< int32_t, int32_t, WebSocketDataFrame_t222733806 *, Il2CppObject ** >::Invoke(0 /* System.Int32 WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader::Process(System.Int32,WebSocket4Net.Protocol.WebSocketDataFrame,WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader&) */, IDataFramePartReader_t2102555929_il2cpp_TypeInfo_var, L_5, L_6, L_7, (&V_0));
		V_1 = L_8;
		int32_t L_9 = V_1;
		V_4 = (bool)((((int32_t)((((int32_t)L_9) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_10 = V_4;
		if (L_10)
		{
			goto IL_004a;
		}
	}
	{
		int32_t* L_11 = ___left;
		*((int32_t*)(L_11)) = (int32_t)0;
		V_3 = (WebSocketCommandInfo_t3536916738 *)NULL;
		goto IL_018d;
	}

IL_004a:
	{
		int32_t* L_12 = ___left;
		int32_t L_13 = V_1;
		*((int32_t*)(L_12)) = (int32_t)L_13;
		int32_t* L_14 = ___left;
		V_4 = (bool)((((int32_t)((((int32_t)(*((int32_t*)L_14))) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_15 = V_4;
		if (L_15)
		{
			goto IL_0072;
		}
	}
	{
		WebSocketDataFrame_t222733806 * L_16 = __this->get_m_Frame_1();
		NullCheck(L_16);
		ArraySegmentList_t1783467835 * L_17 = WebSocketDataFrame_get_InnerData_m815354334(L_16, /*hidden argument*/NULL);
		int32_t* L_18 = ___left;
		NullCheck(L_17);
		ArraySegmentList_1_TrimEnd_m2413284860(L_17, (*((int32_t*)L_18)), /*hidden argument*/ArraySegmentList_1_TrimEnd_m2413284860_MethodInfo_var);
	}

IL_0072:
	{
		Il2CppObject * L_19 = V_0;
		V_4 = (bool)((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_19) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_20 = V_4;
		if (L_20)
		{
			goto IL_0169;
		}
	}
	{
		WebSocketDataFrame_t222733806 * L_21 = __this->get_m_Frame_1();
		NullCheck(L_21);
		bool L_22 = WebSocketDataFrame_get_FIN_m262651117(L_21, /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_22) == ((int32_t)0))? 1 : 0);
		bool L_23 = V_4;
		if (L_23)
		{
			goto IL_0110;
		}
	}
	{
		List_1_t1019692775 * L_24 = __this->get_m_PreviousFrames_0();
		if (!L_24)
		{
			goto IL_00b3;
		}
	}
	{
		List_1_t1019692775 * L_25 = __this->get_m_PreviousFrames_0();
		NullCheck(L_25);
		int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<WebSocket4Net.Protocol.WebSocketDataFrame>::get_Count() */, L_25);
		G_B9_0 = ((((int32_t)((((int32_t)L_26) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00b4;
	}

IL_00b3:
	{
		G_B9_0 = 1;
	}

IL_00b4:
	{
		V_4 = (bool)G_B9_0;
		bool L_27 = V_4;
		if (L_27)
		{
			goto IL_00f3;
		}
	}
	{
		List_1_t1019692775 * L_28 = __this->get_m_PreviousFrames_0();
		WebSocketDataFrame_t222733806 * L_29 = __this->get_m_Frame_1();
		NullCheck(L_28);
		VirtActionInvoker1< WebSocketDataFrame_t222733806 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<WebSocket4Net.Protocol.WebSocketDataFrame>::Add(!0) */, L_28, L_29);
		ArraySegmentList_t1783467835 * L_30 = (ArraySegmentList_t1783467835 *)il2cpp_codegen_object_new(ArraySegmentList_t1783467835_il2cpp_TypeInfo_var);
		ArraySegmentList__ctor_m3332344921(L_30, /*hidden argument*/NULL);
		WebSocketDataFrame_t222733806 * L_31 = (WebSocketDataFrame_t222733806 *)il2cpp_codegen_object_new(WebSocketDataFrame_t222733806_il2cpp_TypeInfo_var);
		WebSocketDataFrame__ctor_m2101584072(L_31, L_30, /*hidden argument*/NULL);
		__this->set_m_Frame_1(L_31);
		List_1_t1019692775 * L_32 = __this->get_m_PreviousFrames_0();
		WebSocketCommandInfo_t3536916738 * L_33 = (WebSocketCommandInfo_t3536916738 *)il2cpp_codegen_object_new(WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var);
		WebSocketCommandInfo__ctor_m2019461544(L_33, L_32, /*hidden argument*/NULL);
		V_2 = L_33;
		__this->set_m_PreviousFrames_0((List_1_t1019692775 *)NULL);
		goto IL_010d;
	}

IL_00f3:
	{
		WebSocketDataFrame_t222733806 * L_34 = __this->get_m_Frame_1();
		WebSocketCommandInfo_t3536916738 * L_35 = (WebSocketCommandInfo_t3536916738 *)il2cpp_codegen_object_new(WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var);
		WebSocketCommandInfo__ctor_m784170289(L_35, L_34, /*hidden argument*/NULL);
		V_2 = L_35;
		WebSocketDataFrame_t222733806 * L_36 = __this->get_m_Frame_1();
		NullCheck(L_36);
		WebSocketDataFrame_Clear_m2483472352(L_36, /*hidden argument*/NULL);
	}

IL_010d:
	{
		goto IL_0153;
	}

IL_0110:
	{
		List_1_t1019692775 * L_37 = __this->get_m_PreviousFrames_0();
		V_4 = (bool)((((int32_t)((((Il2CppObject*)(List_1_t1019692775 *)L_37) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_38 = V_4;
		if (L_38)
		{
			goto IL_012e;
		}
	}
	{
		List_1_t1019692775 * L_39 = (List_1_t1019692775 *)il2cpp_codegen_object_new(List_1_t1019692775_il2cpp_TypeInfo_var);
		List_1__ctor_m906535829(L_39, /*hidden argument*/List_1__ctor_m906535829_MethodInfo_var);
		__this->set_m_PreviousFrames_0(L_39);
	}

IL_012e:
	{
		List_1_t1019692775 * L_40 = __this->get_m_PreviousFrames_0();
		WebSocketDataFrame_t222733806 * L_41 = __this->get_m_Frame_1();
		NullCheck(L_40);
		VirtActionInvoker1< WebSocketDataFrame_t222733806 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<WebSocket4Net.Protocol.WebSocketDataFrame>::Add(!0) */, L_40, L_41);
		ArraySegmentList_t1783467835 * L_42 = (ArraySegmentList_t1783467835 *)il2cpp_codegen_object_new(ArraySegmentList_t1783467835_il2cpp_TypeInfo_var);
		ArraySegmentList__ctor_m3332344921(L_42, /*hidden argument*/NULL);
		WebSocketDataFrame_t222733806 * L_43 = (WebSocketDataFrame_t222733806 *)il2cpp_codegen_object_new(WebSocketDataFrame_t222733806_il2cpp_TypeInfo_var);
		WebSocketDataFrame__ctor_m2101584072(L_43, L_42, /*hidden argument*/NULL);
		__this->set_m_Frame_1(L_43);
		V_2 = (WebSocketCommandInfo_t3536916738 *)NULL;
	}

IL_0153:
	{
		__this->set_m_LastPartLength_3(0);
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		Il2CppObject * L_44 = DataFramePartReader_get_NewReader_m3969627167(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_PartReader_2(L_44);
		WebSocketCommandInfo_t3536916738 * L_45 = V_2;
		V_3 = L_45;
		goto IL_018d;
	}

IL_0169:
	{
		WebSocketDataFrame_t222733806 * L_46 = __this->get_m_Frame_1();
		NullCheck(L_46);
		ArraySegmentList_t1783467835 * L_47 = WebSocketDataFrame_get_InnerData_m815354334(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		int32_t L_48 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_47);
		int32_t L_49 = V_1;
		__this->set_m_LastPartLength_3(((int32_t)((int32_t)L_48-(int32_t)L_49)));
		Il2CppObject * L_50 = V_0;
		__this->set_m_PartReader_2(L_50);
		V_3 = (WebSocketCommandInfo_t3536916738 *)NULL;
		goto IL_018d;
	}

IL_018d:
	{
		WebSocketCommandInfo_t3536916738 * L_51 = V_3;
		return L_51;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10HandshakeReader::.ctor(WebSocket4Net.WebSocket)
extern TypeInfo* HandshakeReader_t1078082956_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi10HandshakeReader__ctor_m2004520026_MetadataUsageId;
extern "C"  void DraftHybi10HandshakeReader__ctor_m2004520026 (DraftHybi10HandshakeReader_t3118530676 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10HandshakeReader__ctor_m2004520026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebSocket_t713846903 * L_0 = ___websocket;
		IL2CPP_RUNTIME_CLASS_INIT(HandshakeReader_t1078082956_il2cpp_TypeInfo_var);
		HandshakeReader__ctor_m4265408334(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// WebSocket4Net.WebSocketCommandInfo WebSocket4Net.Protocol.DraftHybi10HandshakeReader::GetCommandInfo(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern TypeInfo* HandshakeReader_t1078082956_il2cpp_TypeInfo_var;
extern TypeInfo* DraftHybi10DataReader_t1638928215_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi10HandshakeReader_GetCommandInfo_m4260492983_MetadataUsageId;
extern "C"  WebSocketCommandInfo_t3536916738 * DraftHybi10HandshakeReader_GetCommandInfo_m4260492983 (DraftHybi10HandshakeReader_t3118530676 * __this, ByteU5BU5D_t58506160* ___readBuffer, int32_t ___offset, int32_t ___length, int32_t* ___left, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10HandshakeReader_GetCommandInfo_m4260492983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WebSocketCommandInfo_t3536916738 * V_0 = NULL;
	WebSocketCommandInfo_t3536916738 * V_1 = NULL;
	bool V_2 = false;
	{
		ByteU5BU5D_t58506160* L_0 = ___readBuffer;
		int32_t L_1 = ___offset;
		int32_t L_2 = ___length;
		int32_t* L_3 = ___left;
		WebSocketCommandInfo_t3536916738 * L_4 = HandshakeReader_GetCommandInfo_m2339654721(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		WebSocketCommandInfo_t3536916738 * L_5 = V_0;
		V_2 = (bool)((((int32_t)((((Il2CppObject*)(WebSocketCommandInfo_t3536916738 *)L_5) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_2;
		if (L_6)
		{
			goto IL_001c;
		}
	}
	{
		V_1 = (WebSocketCommandInfo_t3536916738 *)NULL;
		goto IL_0040;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HandshakeReader_t1078082956_il2cpp_TypeInfo_var);
		String_t* L_7 = ((HandshakeReader_t1078082956_StaticFields*)HandshakeReader_t1078082956_il2cpp_TypeInfo_var->static_fields)->get_BadRequestCode_3();
		WebSocketCommandInfo_t3536916738 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String WebSocket4Net.WebSocketCommandInfo::get_Key() */, L_8);
		NullCheck(L_7);
		bool L_10 = String_Equals_m3541721061(L_7, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		bool L_11 = V_2;
		if (L_11)
		{
			goto IL_003c;
		}
	}
	{
		DraftHybi10DataReader_t1638928215 * L_12 = (DraftHybi10DataReader_t1638928215 *)il2cpp_codegen_object_new(DraftHybi10DataReader_t1638928215_il2cpp_TypeInfo_var);
		DraftHybi10DataReader__ctor_m1356719760(L_12, /*hidden argument*/NULL);
		ReaderBase_set_NextCommandReader_m1719152039(__this, L_12, /*hidden argument*/NULL);
	}

IL_003c:
	{
		WebSocketCommandInfo_t3536916738 * L_13 = V_0;
		V_1 = L_13;
		goto IL_0040;
	}

IL_0040:
	{
		WebSocketCommandInfo_t3536916738 * L_14 = V_1;
		return L_14;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::.ctor()
extern TypeInfo* CloseStatusCodeHybi10_t635449120_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2346764800;
extern const uint32_t DraftHybi10Processor__ctor_m2859170935_MetadataUsageId;
extern "C"  void DraftHybi10Processor__ctor_m2859170935 (DraftHybi10Processor_t2968147820 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10Processor__ctor_m2859170935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_ExpectedAcceptKey_3(_stringLiteral2346764800);
		CloseStatusCodeHybi10_t635449120 * L_0 = (CloseStatusCodeHybi10_t635449120 *)il2cpp_codegen_object_new(CloseStatusCodeHybi10_t635449120_il2cpp_TypeInfo_var);
		CloseStatusCodeHybi10__ctor_m4111104231(L_0, /*hidden argument*/NULL);
		ProtocolProcessorBase__ctor_m448700961(__this, 8, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::.ctor(WebSocket4Net.WebSocketVersion,WebSocket4Net.Protocol.ICloseStatusCode)
extern Il2CppCodeGenString* _stringLiteral2346764800;
extern const uint32_t DraftHybi10Processor__ctor_m3986497774_MetadataUsageId;
extern "C"  void DraftHybi10Processor__ctor_m3986497774 (DraftHybi10Processor_t2968147820 * __this, int32_t ___version, Il2CppObject * ___closeStatusCode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10Processor__ctor_m3986497774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_ExpectedAcceptKey_3(_stringLiteral2346764800);
		int32_t L_0 = ___version;
		Il2CppObject * L_1 = ___closeStatusCode;
		ProtocolProcessorBase__ctor_m448700961(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendHandshake(WebSocket4Net.WebSocket)
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* Guid_t2778838590_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t3650470111_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern TypeInfo* Extensions_t39620836_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t2776692961_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2284748262_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m753985657_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3835136033;
extern Il2CppCodeGenString* _stringLiteral1226428783;
extern Il2CppCodeGenString* _stringLiteral2510013413;
extern Il2CppCodeGenString* _stringLiteral467998424;
extern Il2CppCodeGenString* _stringLiteral3256327100;
extern Il2CppCodeGenString* _stringLiteral3429000835;
extern Il2CppCodeGenString* _stringLiteral2167348974;
extern Il2CppCodeGenString* _stringLiteral1443686636;
extern Il2CppCodeGenString* _stringLiteral1433713088;
extern Il2CppCodeGenString* _stringLiteral61;
extern Il2CppCodeGenString* _stringLiteral3812715690;
extern Il2CppCodeGenString* _stringLiteral59;
extern Il2CppCodeGenString* _stringLiteral2901091385;
extern const uint32_t DraftHybi10Processor_SendHandshake_m1468137349_MetadataUsageId;
extern "C"  void DraftHybi10Processor_SendHandshake_m1468137349 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10Processor_SendHandshake_m1468137349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	StringBuilder_t3822575854 * V_2 = NULL;
	List_1_t2891677073 * V_3 = NULL;
	StringU5BU5D_t2956870243* V_4 = NULL;
	int32_t V_5 = 0;
	KeyValuePair_2_t2094718104  V_6;
	memset(&V_6, 0, sizeof(V_6));
	ByteU5BU5D_t58506160* V_7 = NULL;
	Guid_t2778838590  V_8;
	memset(&V_8, 0, sizeof(V_8));
	bool V_9 = false;
	ObjectU5BU5D_t11523773* V_10 = NULL;
	StringBuilder_t3822575854 * G_B2_0 = NULL;
	StringBuilder_t3822575854 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	StringBuilder_t3822575854 * G_B3_1 = NULL;
	int32_t G_B8_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_0 = Encoding_get_ASCII_m1425378925(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t2778838590_il2cpp_TypeInfo_var);
		Guid_t2778838590  L_1 = Guid_NewGuid_m3560729310(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_8 = L_1;
		String_t* L_2 = Guid_ToString_m2528531937((&V_8), /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = String_Substring_m675079568(L_2, 0, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_0);
		ByteU5BU5D_t58506160* L_4 = VirtFuncInvoker1< ByteU5BU5D_t58506160*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_5 = Convert_ToBase64String_m1841808901(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		SHA1_t1560027742 * L_6 = SHA1_Create_m2764862537(NULL /*static, unused*/, /*hidden argument*/NULL);
		Encoding_t180559927 * L_7 = Encoding_get_ASCII_m1425378925(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m138640077(NULL /*static, unused*/, L_8, _stringLiteral3835136033, /*hidden argument*/NULL);
		NullCheck(L_7);
		ByteU5BU5D_t58506160* L_10 = VirtFuncInvoker1< ByteU5BU5D_t58506160*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_7, L_9);
		NullCheck(L_6);
		ByteU5BU5D_t58506160* L_11 = HashAlgorithm_ComputeHash_m1325366732(L_6, L_10, /*hidden argument*/NULL);
		String_t* L_12 = Convert_ToBase64String_m1841808901(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		WebSocket_t713846903 * L_13 = ___websocket;
		NullCheck(L_13);
		Il2CppObject* L_14 = WebSocket_get_Items_m1025846418(L_13, /*hidden argument*/NULL);
		String_t* L_15 = __this->get_m_ExpectedAcceptKey_3();
		String_t* L_16 = V_1;
		NullCheck(L_14);
		InterfaceActionInvoker2< String_t*, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Object>::set_Item(!0,!1) */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, L_14, L_15, L_16);
		StringBuilder_t3822575854 * L_17 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_17, /*hidden argument*/NULL);
		V_2 = L_17;
		StringBuilder_t3822575854 * L_18 = V_2;
		WebSocket_t713846903 * L_19 = ___websocket;
		NullCheck(L_19);
		Uri_t2776692961 * L_20 = WebSocket_get_TargetUri_m4211545892(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = Uri_get_PathAndQuery_m3621173943(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_AppendFormatWithCrCf_m759365938(NULL /*static, unused*/, L_18, _stringLiteral1226428783, L_21, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_22 = V_2;
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_22, _stringLiteral2510013413, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_23 = V_2;
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_23, _stringLiteral467998424, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_24 = V_2;
		NullCheck(L_24);
		StringBuilder_Append_m3898090075(L_24, _stringLiteral3256327100, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_25 = V_2;
		String_t* L_26 = ProtocolProcessorBase_get_VersionTag_m3245039016(__this, /*hidden argument*/NULL);
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_27 = V_2;
		NullCheck(L_27);
		StringBuilder_Append_m3898090075(L_27, _stringLiteral3429000835, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_28 = V_2;
		String_t* L_29 = V_0;
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_30 = V_2;
		NullCheck(L_30);
		StringBuilder_Append_m3898090075(L_30, _stringLiteral2167348974, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_31 = V_2;
		WebSocket_t713846903 * L_32 = ___websocket;
		NullCheck(L_32);
		String_t* L_33 = WebSocket_get_HandshakeHost_m3944612535(L_32, /*hidden argument*/NULL);
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_34 = V_2;
		NullCheck(L_34);
		StringBuilder_Append_m3898090075(L_34, _stringLiteral1443686636, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_35 = V_2;
		WebSocket_t713846903 * L_36 = ___websocket;
		NullCheck(L_36);
		String_t* L_37 = WebSocket_get_Origin_m775398224(L_36, /*hidden argument*/NULL);
		bool L_38 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		G_B1_0 = L_35;
		if (L_38)
		{
			G_B2_0 = L_35;
			goto IL_0102;
		}
	}
	{
		WebSocket_t713846903 * L_39 = ___websocket;
		NullCheck(L_39);
		String_t* L_40 = WebSocket_get_Origin_m775398224(L_39, /*hidden argument*/NULL);
		G_B3_0 = L_40;
		G_B3_1 = G_B1_0;
		goto IL_010d;
	}

IL_0102:
	{
		WebSocket_t713846903 * L_41 = ___websocket;
		NullCheck(L_41);
		Uri_t2776692961 * L_42 = WebSocket_get_TargetUri_m4211545892(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		String_t* L_43 = Uri_get_Host_m1446697833(L_42, /*hidden argument*/NULL);
		G_B3_0 = L_43;
		G_B3_1 = G_B2_0;
	}

IL_010d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_44 = ___websocket;
		NullCheck(L_44);
		String_t* L_45 = WebSocket_get_SubProtocol_m1721628912(L_44, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_46 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		V_9 = L_46;
		bool L_47 = V_9;
		if (L_47)
		{
			goto IL_013f;
		}
	}
	{
		StringBuilder_t3822575854 * L_48 = V_2;
		NullCheck(L_48);
		StringBuilder_Append_m3898090075(L_48, _stringLiteral1433713088, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_49 = V_2;
		WebSocket_t713846903 * L_50 = ___websocket;
		NullCheck(L_50);
		String_t* L_51 = WebSocket_get_SubProtocol_m1721628912(L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_49, L_51, /*hidden argument*/NULL);
	}

IL_013f:
	{
		WebSocket_t713846903 * L_52 = ___websocket;
		NullCheck(L_52);
		List_1_t2891677073 * L_53 = WebSocket_get_Cookies_m794256048(L_52, /*hidden argument*/NULL);
		V_3 = L_53;
		List_1_t2891677073 * L_54 = V_3;
		if (!L_54)
		{
			goto IL_0157;
		}
	}
	{
		List_1_t2891677073 * L_55 = V_3;
		NullCheck(L_55);
		int32_t L_56 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Count() */, L_55);
		G_B8_0 = ((((int32_t)((((int32_t)L_56) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0158;
	}

IL_0157:
	{
		G_B8_0 = 1;
	}

IL_0158:
	{
		V_9 = (bool)G_B8_0;
		bool L_57 = V_9;
		if (L_57)
		{
			goto IL_01d5;
		}
	}
	{
		List_1_t2891677073 * L_58 = V_3;
		NullCheck(L_58);
		int32_t L_59 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Count() */, L_58);
		V_4 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_59));
		V_5 = 0;
		goto IL_01a5;
	}

IL_0171:
	{
		List_1_t2891677073 * L_60 = V_3;
		int32_t L_61 = V_5;
		NullCheck(L_60);
		KeyValuePair_2_t2094718104  L_62 = VirtFuncInvoker1< KeyValuePair_2_t2094718104 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Item(System.Int32) */, L_60, L_61);
		V_6 = L_62;
		StringU5BU5D_t2956870243* L_63 = V_4;
		int32_t L_64 = V_5;
		String_t* L_65 = KeyValuePair_2_get_Key_m2284748262((&V_6), /*hidden argument*/KeyValuePair_2_get_Key_m2284748262_MethodInfo_var);
		String_t* L_66 = KeyValuePair_2_get_Value_m753985657((&V_6), /*hidden argument*/KeyValuePair_2_get_Value_m753985657_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t2776692961_il2cpp_TypeInfo_var);
		String_t* L_67 = Uri_EscapeUriString_m2040573756(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_68 = String_Concat_m1825781833(NULL /*static, unused*/, L_65, _stringLiteral61, L_67, /*hidden argument*/NULL);
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, L_64);
		ArrayElementTypeCheck (L_63, L_68);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(L_64), (String_t*)L_68);
		int32_t L_69 = V_5;
		V_5 = ((int32_t)((int32_t)L_69+(int32_t)1));
	}

IL_01a5:
	{
		int32_t L_70 = V_5;
		List_1_t2891677073 * L_71 = V_3;
		NullCheck(L_71);
		int32_t L_72 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Count() */, L_71);
		V_9 = (bool)((((int32_t)L_70) < ((int32_t)L_72))? 1 : 0);
		bool L_73 = V_9;
		if (L_73)
		{
			goto IL_0171;
		}
	}
	{
		StringBuilder_t3822575854 * L_74 = V_2;
		NullCheck(L_74);
		StringBuilder_Append_m3898090075(L_74, _stringLiteral3812715690, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_75 = V_2;
		StringU5BU5D_t2956870243* L_76 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_77 = String_Join_m2789530325(NULL /*static, unused*/, _stringLiteral59, L_76, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_AppendWithCrCf_m3522543387(NULL /*static, unused*/, L_75, L_77, /*hidden argument*/NULL);
	}

IL_01d5:
	{
		WebSocket_t713846903 * L_78 = ___websocket;
		NullCheck(L_78);
		List_1_t2891677073 * L_79 = WebSocket_get_CustomHeaderItems_m1966151779(L_78, /*hidden argument*/NULL);
		V_9 = (bool)((((Il2CppObject*)(List_1_t2891677073 *)L_79) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_80 = V_9;
		if (L_80)
		{
			goto IL_0243;
		}
	}
	{
		V_5 = 0;
		goto IL_022d;
	}

IL_01ea:
	{
		WebSocket_t713846903 * L_81 = ___websocket;
		NullCheck(L_81);
		List_1_t2891677073 * L_82 = WebSocket_get_CustomHeaderItems_m1966151779(L_81, /*hidden argument*/NULL);
		int32_t L_83 = V_5;
		NullCheck(L_82);
		KeyValuePair_2_t2094718104  L_84 = VirtFuncInvoker1< KeyValuePair_2_t2094718104 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Item(System.Int32) */, L_82, L_83);
		V_6 = L_84;
		StringBuilder_t3822575854 * L_85 = V_2;
		V_10 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		ObjectU5BU5D_t11523773* L_86 = V_10;
		String_t* L_87 = KeyValuePair_2_get_Key_m2284748262((&V_6), /*hidden argument*/KeyValuePair_2_get_Key_m2284748262_MethodInfo_var);
		NullCheck(L_86);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_86, 0);
		ArrayElementTypeCheck (L_86, L_87);
		(L_86)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_87);
		ObjectU5BU5D_t11523773* L_88 = V_10;
		String_t* L_89 = KeyValuePair_2_get_Value_m753985657((&V_6), /*hidden argument*/KeyValuePair_2_get_Value_m753985657_MethodInfo_var);
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, 1);
		ArrayElementTypeCheck (L_88, L_89);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_89);
		ObjectU5BU5D_t11523773* L_90 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_AppendFormatWithCrCf_m3901244368(NULL /*static, unused*/, L_85, _stringLiteral2901091385, L_90, /*hidden argument*/NULL);
		int32_t L_91 = V_5;
		V_5 = ((int32_t)((int32_t)L_91+(int32_t)1));
	}

IL_022d:
	{
		int32_t L_92 = V_5;
		WebSocket_t713846903 * L_93 = ___websocket;
		NullCheck(L_93);
		List_1_t2891677073 * L_94 = WebSocket_get_CustomHeaderItems_m1966151779(L_93, /*hidden argument*/NULL);
		NullCheck(L_94);
		int32_t L_95 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Count() */, L_94);
		V_9 = (bool)((((int32_t)L_92) < ((int32_t)L_95))? 1 : 0);
		bool L_96 = V_9;
		if (L_96)
		{
			goto IL_01ea;
		}
	}
	{
	}

IL_0243:
	{
		StringBuilder_t3822575854 * L_97 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		Extensions_AppendWithCrCf_m3929176351(NULL /*static, unused*/, L_97, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_98 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_99 = V_2;
		NullCheck(L_99);
		String_t* L_100 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_99);
		NullCheck(L_98);
		ByteU5BU5D_t58506160* L_101 = VirtFuncInvoker1< ByteU5BU5D_t58506160*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_98, L_100);
		V_7 = L_101;
		WebSocket_t713846903 * L_102 = ___websocket;
		NullCheck(L_102);
		TcpClientSession_t1301539049 * L_103 = WebSocket_get_Client_m2705060304(L_102, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_104 = V_7;
		ByteU5BU5D_t58506160* L_105 = V_7;
		NullCheck(L_105);
		NullCheck(L_103);
		VirtActionInvoker3< ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(5 /* System.Void SuperSocket.ClientEngine.ClientSession::Send(System.Byte[],System.Int32,System.Int32) */, L_103, L_104, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_105)->max_length)))));
		return;
	}
}
// WebSocket4Net.Protocol.ReaderBase WebSocket4Net.Protocol.DraftHybi10Processor::CreateHandshakeReader(WebSocket4Net.WebSocket)
extern TypeInfo* DraftHybi10HandshakeReader_t3118530676_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi10Processor_CreateHandshakeReader_m2909912431_MetadataUsageId;
extern "C"  ReaderBase_t893022310 * DraftHybi10Processor_CreateHandshakeReader_m2909912431 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10Processor_CreateHandshakeReader_m2909912431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ReaderBase_t893022310 * V_0 = NULL;
	{
		WebSocket_t713846903 * L_0 = ___websocket;
		DraftHybi10HandshakeReader_t3118530676 * L_1 = (DraftHybi10HandshakeReader_t3118530676 *)il2cpp_codegen_object_new(DraftHybi10HandshakeReader_t3118530676_il2cpp_TypeInfo_var);
		DraftHybi10HandshakeReader__ctor_m2004520026(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		ReaderBase_t893022310 * L_2 = V_0;
		return L_2;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendMessage(WebSocket4Net.WebSocket,System.Int32,System.String)
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi10Processor_SendMessage_m712760638_MetadataUsageId;
extern "C"  void DraftHybi10Processor_SendMessage_m712760638 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, int32_t ___opCode, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10Processor_SendMessage_m712760638_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t58506160* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_0 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___message;
		NullCheck(L_0);
		ByteU5BU5D_t58506160* L_2 = VirtFuncInvoker1< ByteU5BU5D_t58506160*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		V_0 = L_2;
		WebSocket_t713846903 * L_3 = ___websocket;
		int32_t L_4 = ___opCode;
		ByteU5BU5D_t58506160* L_5 = V_0;
		ByteU5BU5D_t58506160* L_6 = V_0;
		NullCheck(L_6);
		DraftHybi10Processor_SendDataFragment_m1842464672(__this, L_3, L_4, L_5, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] WebSocket4Net.Protocol.DraftHybi10Processor::EncodeDataFrame(System.Int32,System.Byte[],System.Int32,System.Int32)
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi10Processor_EncodeDataFrame_m854661280_MetadataUsageId;
extern "C"  ByteU5BU5D_t58506160* DraftHybi10Processor_EncodeDataFrame_m854661280 (DraftHybi10Processor_t2968147820 * __this, int32_t ___opCode, ByteU5BU5D_t58506160* ___playloadData, int32_t ___offset, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10Processor_EncodeDataFrame_m854661280_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t58506160* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	ByteU5BU5D_t58506160* V_5 = NULL;
	bool V_6 = false;
	{
		V_1 = 4;
		int32_t L_0 = ___length;
		V_6 = (bool)((((int32_t)((((int32_t)L_0) < ((int32_t)((int32_t)126)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_6;
		if (L_1)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_2 = V_1;
		int32_t L_3 = ___length;
		V_0 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)2+(int32_t)L_2))+(int32_t)L_3))));
		ByteU5BU5D_t58506160* L_4 = V_0;
		int32_t L_5 = ___length;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)(((int32_t)((uint8_t)L_5))));
		goto IL_00bb;
	}

IL_002b:
	{
		int32_t L_6 = ___length;
		V_6 = (bool)((((int32_t)((((int32_t)L_6) < ((int32_t)((int32_t)65536)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_7 = V_6;
		if (L_7)
		{
			goto IL_006a;
		}
	}
	{
		int32_t L_8 = V_1;
		int32_t L_9 = ___length;
		V_0 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)4+(int32_t)L_8))+(int32_t)L_9))));
		ByteU5BU5D_t58506160* L_10 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)((int32_t)126));
		ByteU5BU5D_t58506160* L_11 = V_0;
		int32_t L_12 = ___length;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_12/(int32_t)((int32_t)256)))))));
		ByteU5BU5D_t58506160* L_13 = V_0;
		int32_t L_14 = ___length;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_14%(int32_t)((int32_t)256)))))));
		goto IL_00bb;
	}

IL_006a:
	{
		int32_t L_15 = V_1;
		int32_t L_16 = ___length;
		V_0 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)10)+(int32_t)L_15))+(int32_t)L_16))));
		ByteU5BU5D_t58506160* L_17 = V_0;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)((int32_t)127));
		int32_t L_18 = ___length;
		V_2 = L_18;
		V_3 = ((int32_t)256);
		V_4 = ((int32_t)9);
		goto IL_00af;
	}

IL_008c:
	{
		ByteU5BU5D_t58506160* L_19 = V_0;
		int32_t L_20 = V_4;
		int32_t L_21 = V_2;
		int32_t L_22 = V_3;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_21%(int32_t)L_22))))));
		int32_t L_23 = V_2;
		int32_t L_24 = V_3;
		V_2 = ((int32_t)((int32_t)L_23/(int32_t)L_24));
		int32_t L_25 = V_2;
		V_6 = (bool)((((int32_t)((((int32_t)L_25) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_26 = V_6;
		if (L_26)
		{
			goto IL_00a8;
		}
	}
	{
		goto IL_00ba;
	}

IL_00a8:
	{
		int32_t L_27 = V_4;
		V_4 = ((int32_t)((int32_t)L_27-(int32_t)1));
	}

IL_00af:
	{
		int32_t L_28 = V_4;
		V_6 = (bool)((((int32_t)L_28) > ((int32_t)1))? 1 : 0);
		bool L_29 = V_6;
		if (L_29)
		{
			goto IL_008c;
		}
	}

IL_00ba:
	{
	}

IL_00bb:
	{
		ByteU5BU5D_t58506160* L_30 = V_0;
		int32_t L_31 = ___opCode;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_31|(int32_t)((int32_t)128)))))));
		ByteU5BU5D_t58506160* L_32 = V_0;
		ByteU5BU5D_t58506160* L_33 = V_0;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 1);
		int32_t L_34 = 1;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 1);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_34)))|(int32_t)((int32_t)128)))))));
		ByteU5BU5D_t58506160* L_35 = V_0;
		ByteU5BU5D_t58506160* L_36 = V_0;
		NullCheck(L_36);
		int32_t L_37 = V_1;
		int32_t L_38 = ___length;
		DraftHybi10Processor_GenerateMask_m2250987996(__this, L_35, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_36)->max_length))))-(int32_t)L_37))-(int32_t)L_38)), /*hidden argument*/NULL);
		int32_t L_39 = ___length;
		V_6 = (bool)((((int32_t)((((int32_t)L_39) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_40 = V_6;
		if (L_40)
		{
			goto IL_010c;
		}
	}
	{
		ByteU5BU5D_t58506160* L_41 = ___playloadData;
		int32_t L_42 = ___offset;
		int32_t L_43 = ___length;
		ByteU5BU5D_t58506160* L_44 = V_0;
		ByteU5BU5D_t58506160* L_45 = V_0;
		NullCheck(L_45);
		int32_t L_46 = ___length;
		ByteU5BU5D_t58506160* L_47 = V_0;
		ByteU5BU5D_t58506160* L_48 = V_0;
		NullCheck(L_48);
		int32_t L_49 = V_1;
		int32_t L_50 = ___length;
		DraftHybi10Processor_MaskData_m3859186278(__this, L_41, L_42, L_43, L_44, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))))-(int32_t)L_46)), L_47, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_48)->max_length))))-(int32_t)L_49))-(int32_t)L_50)), /*hidden argument*/NULL);
	}

IL_010c:
	{
		ByteU5BU5D_t58506160* L_51 = V_0;
		V_5 = L_51;
		goto IL_0111;
	}

IL_0111:
	{
		ByteU5BU5D_t58506160* L_52 = V_5;
		return L_52;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendDataFragment(WebSocket4Net.WebSocket,System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  void DraftHybi10Processor_SendDataFragment_m1842464672 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, int32_t ___opCode, ByteU5BU5D_t58506160* ___playloadData, int32_t ___offset, int32_t ___length, const MethodInfo* method)
{
	ByteU5BU5D_t58506160* V_0 = NULL;
	{
		int32_t L_0 = ___opCode;
		ByteU5BU5D_t58506160* L_1 = ___playloadData;
		int32_t L_2 = ___offset;
		int32_t L_3 = ___length;
		ByteU5BU5D_t58506160* L_4 = DraftHybi10Processor_EncodeDataFrame_m854661280(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		WebSocket_t713846903 * L_5 = ___websocket;
		NullCheck(L_5);
		TcpClientSession_t1301539049 * L_6 = WebSocket_get_Client_m2705060304(L_5, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_7 = V_0;
		ByteU5BU5D_t58506160* L_8 = V_0;
		NullCheck(L_8);
		NullCheck(L_6);
		VirtActionInvoker3< ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(5 /* System.Void SuperSocket.ClientEngine.ClientSession::Send(System.Byte[],System.Int32,System.Int32) */, L_6, L_7, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))));
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendMessage(WebSocket4Net.WebSocket,System.String)
extern "C"  void DraftHybi10Processor_SendMessage_m3031038449 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, String_t* ___message, const MethodInfo* method)
{
	{
		WebSocket_t713846903 * L_0 = ___websocket;
		String_t* L_1 = ___message;
		DraftHybi10Processor_SendMessage_m712760638(__this, L_0, 1, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendCloseHandshake(WebSocket4Net.WebSocket,System.Int32,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi10Processor_SendCloseHandshake_m193522878_MetadataUsageId;
extern "C"  void DraftHybi10Processor_SendCloseHandshake_m193522878 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, int32_t ___statusCode, String_t* ___closeReason, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10Processor_SendCloseHandshake_m193522878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t58506160* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	bool V_5 = false;
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___closeReason;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_2 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ___closeReason;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_5 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(18 /* System.Int32 System.Text.Encoding::GetMaxByteCount(System.Int32) */, L_2, L_4);
		G_B3_0 = L_5;
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		V_0 = ((int32_t)((int32_t)G_B3_0+(int32_t)2));
		int32_t L_6 = V_0;
		V_1 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_6));
		int32_t L_7 = ___statusCode;
		V_2 = ((int32_t)((int32_t)L_7/(int32_t)((int32_t)256)));
		int32_t L_8 = ___statusCode;
		V_3 = ((int32_t)((int32_t)L_8%(int32_t)((int32_t)256)));
		ByteU5BU5D_t58506160* L_9 = V_1;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)(((int32_t)((uint8_t)L_10))));
		ByteU5BU5D_t58506160* L_11 = V_1;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)(((int32_t)((uint8_t)L_12))));
		String_t* L_13 = ___closeReason;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		bool L_15 = V_5;
		if (L_15)
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_16 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_17 = ___closeReason;
		String_t* L_18 = ___closeReason;
		NullCheck(L_18);
		int32_t L_19 = String_get_Length_m2979997331(L_18, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_20 = V_1;
		NullCheck(L_16);
		int32_t L_21 = VirtFuncInvoker5< int32_t, String_t*, int32_t, int32_t, ByteU5BU5D_t58506160*, int32_t >::Invoke(9 /* System.Int32 System.Text.Encoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32) */, L_16, L_17, 0, L_19, L_20, 2);
		V_4 = L_21;
		WebSocket_t713846903 * L_22 = ___websocket;
		ByteU5BU5D_t58506160* L_23 = V_1;
		int32_t L_24 = V_4;
		DraftHybi10Processor_SendDataFragment_m1842464672(__this, L_22, 8, L_23, 0, ((int32_t)((int32_t)L_24+(int32_t)2)), /*hidden argument*/NULL);
		goto IL_0085;
	}

IL_0075:
	{
		WebSocket_t713846903 * L_25 = ___websocket;
		ByteU5BU5D_t58506160* L_26 = V_1;
		ByteU5BU5D_t58506160* L_27 = V_1;
		NullCheck(L_27);
		DraftHybi10Processor_SendDataFragment_m1842464672(__this, L_25, 8, L_26, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length)))), /*hidden argument*/NULL);
	}

IL_0085:
	{
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendPing(WebSocket4Net.WebSocket,System.String)
extern "C"  void DraftHybi10Processor_SendPing_m916911486 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, String_t* ___ping, const MethodInfo* method)
{
	{
		WebSocket_t713846903 * L_0 = ___websocket;
		String_t* L_1 = ___ping;
		DraftHybi10Processor_SendMessage_m712760638(__this, L_0, ((int32_t)9), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendPong(WebSocket4Net.WebSocket,System.String)
extern "C"  void DraftHybi10Processor_SendPong_m1661350968 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, String_t* ___pong, const MethodInfo* method)
{
	{
		WebSocket_t713846903 * L_0 = ___websocket;
		String_t* L_1 = ___pong;
		DraftHybi10Processor_SendMessage_m712760638(__this, L_0, ((int32_t)10), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocket4Net.Protocol.DraftHybi10Processor::VerifyHandshake(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo,System.String&)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Extensions_t39620836_il2cpp_TypeInfo_var;
extern const MethodInfo* Extensions_GetValue_TisString_t_m2902636613_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2154023182;
extern Il2CppCodeGenString* _stringLiteral408195354;
extern Il2CppCodeGenString* _stringLiteral639552479;
extern Il2CppCodeGenString* _stringLiteral2931060746;
extern Il2CppCodeGenString* _stringLiteral1783458830;
extern const uint32_t DraftHybi10Processor_VerifyHandshake_m3185403576_MetadataUsageId;
extern "C"  bool DraftHybi10Processor_VerifyHandshake_m3185403576 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, WebSocketCommandInfo_t3536916738 * ___handshakeInfo, String_t** ___description, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10Processor_VerifyHandshake_m3185403576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	bool V_4 = false;
	bool V_5 = false;
	{
		WebSocketCommandInfo_t3536916738 * L_0 = ___handshakeInfo;
		NullCheck(L_0);
		String_t* L_1 = WebSocketCommandInfo_get_Text_m101435856(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_5 = (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_5;
		if (L_4)
		{
			goto IL_0027;
		}
	}
	{
		String_t** L_5 = ___description;
		*((Il2CppObject **)(L_5)) = (Il2CppObject *)_stringLiteral2154023182;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_5), (Il2CppObject *)_stringLiteral2154023182);
		V_4 = (bool)0;
		goto IL_00eb;
	}

IL_0027:
	{
		WebSocketCommandInfo_t3536916738 * L_6 = ___handshakeInfo;
		NullCheck(L_6);
		String_t* L_7 = WebSocketCommandInfo_get_Text_m101435856(L_6, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_8 = ___websocket;
		NullCheck(L_8);
		Il2CppObject* L_9 = WebSocket_get_Items_m1025846418(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		bool L_10 = Extensions_ParseMimeHeader_m1946375443(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		V_5 = L_10;
		bool L_11 = V_5;
		if (L_11)
		{
			goto IL_004e;
		}
	}
	{
		String_t** L_12 = ___description;
		*((Il2CppObject **)(L_12)) = (Il2CppObject *)_stringLiteral2154023182;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_12), (Il2CppObject *)_stringLiteral2154023182);
		V_4 = (bool)0;
		goto IL_00eb;
	}

IL_004e:
	{
		WebSocket_t713846903 * L_13 = ___websocket;
		NullCheck(L_13);
		String_t* L_14 = WebSocket_get_SubProtocol_m1721628912(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_5 = L_15;
		bool L_16 = V_5;
		if (L_16)
		{
			goto IL_0097;
		}
	}
	{
		WebSocket_t713846903 * L_17 = ___websocket;
		NullCheck(L_17);
		Il2CppObject* L_18 = WebSocket_get_Items_m1025846418(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		String_t* L_20 = Extensions_GetValue_TisString_t_m2902636613(NULL /*static, unused*/, L_18, _stringLiteral408195354, L_19, /*hidden argument*/Extensions_GetValue_TisString_t_m2902636613_MethodInfo_var);
		V_1 = L_20;
		WebSocket_t713846903 * L_21 = ___websocket;
		NullCheck(L_21);
		String_t* L_22 = WebSocket_get_SubProtocol_m1721628912(L_21, /*hidden argument*/NULL);
		String_t* L_23 = V_1;
		NullCheck(L_22);
		bool L_24 = String_Equals_m1914628696(L_22, L_23, 5, /*hidden argument*/NULL);
		V_5 = L_24;
		bool L_25 = V_5;
		if (L_25)
		{
			goto IL_0096;
		}
	}
	{
		String_t** L_26 = ___description;
		*((Il2CppObject **)(L_26)) = (Il2CppObject *)_stringLiteral639552479;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_26), (Il2CppObject *)_stringLiteral639552479);
		V_4 = (bool)0;
		goto IL_00eb;
	}

IL_0096:
	{
	}

IL_0097:
	{
		WebSocket_t713846903 * L_27 = ___websocket;
		NullCheck(L_27);
		Il2CppObject* L_28 = WebSocket_get_Items_m1025846418(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t39620836_il2cpp_TypeInfo_var);
		String_t* L_30 = Extensions_GetValue_TisString_t_m2902636613(NULL /*static, unused*/, L_28, _stringLiteral2931060746, L_29, /*hidden argument*/Extensions_GetValue_TisString_t_m2902636613_MethodInfo_var);
		V_2 = L_30;
		WebSocket_t713846903 * L_31 = ___websocket;
		NullCheck(L_31);
		Il2CppObject* L_32 = WebSocket_get_Items_m1025846418(L_31, /*hidden argument*/NULL);
		String_t* L_33 = __this->get_m_ExpectedAcceptKey_3();
		String_t* L_34 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_35 = Extensions_GetValue_TisString_t_m2902636613(NULL /*static, unused*/, L_32, L_33, L_34, /*hidden argument*/Extensions_GetValue_TisString_t_m2902636613_MethodInfo_var);
		V_3 = L_35;
		String_t* L_36 = V_3;
		String_t* L_37 = V_2;
		NullCheck(L_36);
		bool L_38 = String_Equals_m1914628696(L_36, L_37, 5, /*hidden argument*/NULL);
		V_5 = L_38;
		bool L_39 = V_5;
		if (L_39)
		{
			goto IL_00df;
		}
	}
	{
		String_t** L_40 = ___description;
		*((Il2CppObject **)(L_40)) = (Il2CppObject *)_stringLiteral1783458830;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_40), (Il2CppObject *)_stringLiteral1783458830);
		V_4 = (bool)0;
		goto IL_00eb;
	}

IL_00df:
	{
		String_t** L_41 = ___description;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_41)) = (Il2CppObject *)L_42;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_41), (Il2CppObject *)L_42);
		V_4 = (bool)1;
		goto IL_00eb;
	}

IL_00eb:
	{
		bool L_43 = V_4;
		return L_43;
	}
}
// System.Boolean WebSocket4Net.Protocol.DraftHybi10Processor::get_SupportPingPong()
extern "C"  bool DraftHybi10Processor_get_SupportPingPong_m3340585117 (DraftHybi10Processor_t2968147820 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		goto IL_0005;
	}

IL_0005:
	{
		bool L_0 = V_0;
		return L_0;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::GenerateMask(System.Byte[],System.Int32)
extern TypeInfo* DraftHybi10Processor_t2968147820_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi10Processor_GenerateMask_m2250987996_MetadataUsageId;
extern "C"  void DraftHybi10Processor_GenerateMask_m2250987996 (DraftHybi10Processor_t2968147820 * __this, ByteU5BU5D_t58506160* ___mask, int32_t ___offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10Processor_GenerateMask_m2250987996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		int32_t L_0 = ___offset;
		ByteU5BU5D_t58506160* L_1 = ___mask;
		NullCheck(L_1);
		int32_t L_2 = Math_Min_m811624909(NULL /*static, unused*/, ((int32_t)((int32_t)L_0+(int32_t)4)), (((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = ___offset;
		V_1 = L_3;
		goto IL_002b;
	}

IL_0011:
	{
		ByteU5BU5D_t58506160* L_4 = ___mask;
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DraftHybi10Processor_t2968147820_il2cpp_TypeInfo_var);
		Random_t922188920 * L_6 = ((DraftHybi10Processor_t2968147820_StaticFields*)DraftHybi10Processor_t2968147820_il2cpp_TypeInfo_var->static_fields)->get_m_Random_4();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_6, 0, ((int32_t)255));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (uint8_t)(((int32_t)((uint8_t)L_7))));
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = V_0;
		V_2 = (bool)((((int32_t)L_9) < ((int32_t)L_10))? 1 : 0);
		bool L_11 = V_2;
		if (L_11)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::MaskData(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32,System.Byte[],System.Int32)
extern "C"  void DraftHybi10Processor_MaskData_m3859186278 (DraftHybi10Processor_t2968147820 * __this, ByteU5BU5D_t58506160* ___rawData, int32_t ___offset, int32_t ___length, ByteU5BU5D_t58506160* ___outputData, int32_t ___outputOffset, ByteU5BU5D_t58506160* ___mask, int32_t ___maskOffset, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		goto IL_0027;
	}

IL_0005:
	{
		int32_t L_0 = ___offset;
		int32_t L_1 = V_0;
		V_1 = ((int32_t)((int32_t)L_0+(int32_t)L_1));
		ByteU5BU5D_t58506160* L_2 = ___outputData;
		int32_t L_3 = ___outputOffset;
		int32_t L_4 = L_3;
		___outputOffset = ((int32_t)((int32_t)L_4+(int32_t)1));
		ByteU5BU5D_t58506160* L_5 = ___rawData;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		ByteU5BU5D_t58506160* L_8 = ___mask;
		int32_t L_9 = ___maskOffset;
		int32_t L_10 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)L_9+(int32_t)((int32_t)((int32_t)L_10%(int32_t)4)))));
		int32_t L_11 = ((int32_t)((int32_t)L_9+(int32_t)((int32_t)((int32_t)L_10%(int32_t)4))));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)))^(int32_t)((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_11)))))))));
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = ___length;
		V_2 = (bool)((((int32_t)L_13) < ((int32_t)L_14))? 1 : 0);
		bool L_15 = V_2;
		if (L_15)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::.cctor()
extern TypeInfo* Random_t922188920_il2cpp_TypeInfo_var;
extern TypeInfo* DraftHybi10Processor_t2968147820_il2cpp_TypeInfo_var;
extern const uint32_t DraftHybi10Processor__cctor_m2252856854_MetadataUsageId;
extern "C"  void DraftHybi10Processor__cctor_m2252856854 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DraftHybi10Processor__cctor_m2252856854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Random_t922188920 * L_0 = (Random_t922188920 *)il2cpp_codegen_object_new(Random_t922188920_il2cpp_TypeInfo_var);
		Random__ctor_m2490522898(L_0, /*hidden argument*/NULL);
		((DraftHybi10Processor_t2968147820_StaticFields*)DraftHybi10Processor_t2968147820_il2cpp_TypeInfo_var->static_fields)->set_m_Random_4(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::.cctor()
extern TypeInfo* FixPartReader_t3993251252_il2cpp_TypeInfo_var;
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern TypeInfo* ExtendedLenghtReader_t3744404771_il2cpp_TypeInfo_var;
extern TypeInfo* MaskKeyReader_t360839071_il2cpp_TypeInfo_var;
extern TypeInfo* PayloadDataReader_t544241252_il2cpp_TypeInfo_var;
extern const uint32_t DataFramePartReader__cctor_m3471580710_MetadataUsageId;
extern "C"  void DataFramePartReader__cctor_m3471580710 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataFramePartReader__cctor_m3471580710_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FixPartReader_t3993251252 * L_0 = (FixPartReader_t3993251252 *)il2cpp_codegen_object_new(FixPartReader_t3993251252_il2cpp_TypeInfo_var);
		FixPartReader__ctor_m3383315125(L_0, /*hidden argument*/NULL);
		DataFramePartReader_set_FixPartReader_m97364172(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ExtendedLenghtReader_t3744404771 * L_1 = (ExtendedLenghtReader_t3744404771 *)il2cpp_codegen_object_new(ExtendedLenghtReader_t3744404771_il2cpp_TypeInfo_var);
		ExtendedLenghtReader__ctor_m3059678476(L_1, /*hidden argument*/NULL);
		DataFramePartReader_set_ExtendedLenghtReader_m1629640683(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		MaskKeyReader_t360839071 * L_2 = (MaskKeyReader_t360839071 *)il2cpp_codegen_object_new(MaskKeyReader_t360839071_il2cpp_TypeInfo_var);
		MaskKeyReader__ctor_m2819127594(L_2, /*hidden argument*/NULL);
		DataFramePartReader_set_MaskKeyReader_m3664049857(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		PayloadDataReader_t544241252 * L_3 = (PayloadDataReader_t544241252 *)il2cpp_codegen_object_new(PayloadDataReader_t544241252_il2cpp_TypeInfo_var);
		PayloadDataReader__ctor_m191259013(L_3, /*hidden argument*/NULL);
		DataFramePartReader_set_PayloadDataReader_m2933328284(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::get_NewReader()
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t DataFramePartReader_get_NewReader_m3969627167_MetadataUsageId;
extern "C"  Il2CppObject * DataFramePartReader_get_NewReader_m3969627167 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataFramePartReader_get_NewReader_m3969627167_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = DataFramePartReader_get_FixPartReader_m3522342567(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::get_FixPartReader()
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t DataFramePartReader_get_FixPartReader_m3522342567_MetadataUsageId;
extern "C"  Il2CppObject * DataFramePartReader_get_FixPartReader_m3522342567 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataFramePartReader_get_FixPartReader_m3522342567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((DataFramePartReader_t1881953282_StaticFields*)DataFramePartReader_t1881953282_il2cpp_TypeInfo_var->static_fields)->get_U3CFixPartReaderU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_0008;
	}

IL_0008:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::set_FixPartReader(WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader)
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t DataFramePartReader_set_FixPartReader_m97364172_MetadataUsageId;
extern "C"  void DataFramePartReader_set_FixPartReader_m97364172 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataFramePartReader_set_FixPartReader_m97364172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		((DataFramePartReader_t1881953282_StaticFields*)DataFramePartReader_t1881953282_il2cpp_TypeInfo_var->static_fields)->set_U3CFixPartReaderU3Ek__BackingField_0(L_0);
		return;
	}
}
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::get_ExtendedLenghtReader()
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t DataFramePartReader_get_ExtendedLenghtReader_m901854720_MetadataUsageId;
extern "C"  Il2CppObject * DataFramePartReader_get_ExtendedLenghtReader_m901854720 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataFramePartReader_get_ExtendedLenghtReader_m901854720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((DataFramePartReader_t1881953282_StaticFields*)DataFramePartReader_t1881953282_il2cpp_TypeInfo_var->static_fields)->get_U3CExtendedLenghtReaderU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_0008;
	}

IL_0008:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::set_ExtendedLenghtReader(WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader)
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t DataFramePartReader_set_ExtendedLenghtReader_m1629640683_MetadataUsageId;
extern "C"  void DataFramePartReader_set_ExtendedLenghtReader_m1629640683 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataFramePartReader_set_ExtendedLenghtReader_m1629640683_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		((DataFramePartReader_t1881953282_StaticFields*)DataFramePartReader_t1881953282_il2cpp_TypeInfo_var->static_fields)->set_U3CExtendedLenghtReaderU3Ek__BackingField_1(L_0);
		return;
	}
}
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::get_MaskKeyReader()
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t DataFramePartReader_get_MaskKeyReader_m287680978_MetadataUsageId;
extern "C"  Il2CppObject * DataFramePartReader_get_MaskKeyReader_m287680978 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataFramePartReader_get_MaskKeyReader_m287680978_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((DataFramePartReader_t1881953282_StaticFields*)DataFramePartReader_t1881953282_il2cpp_TypeInfo_var->static_fields)->get_U3CMaskKeyReaderU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_0008;
	}

IL_0008:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::set_MaskKeyReader(WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader)
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t DataFramePartReader_set_MaskKeyReader_m3664049857_MetadataUsageId;
extern "C"  void DataFramePartReader_set_MaskKeyReader_m3664049857 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataFramePartReader_set_MaskKeyReader_m3664049857_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		((DataFramePartReader_t1881953282_StaticFields*)DataFramePartReader_t1881953282_il2cpp_TypeInfo_var->static_fields)->set_U3CMaskKeyReaderU3Ek__BackingField_2(L_0);
		return;
	}
}
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::get_PayloadDataReader()
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t DataFramePartReader_get_PayloadDataReader_m2911966679_MetadataUsageId;
extern "C"  Il2CppObject * DataFramePartReader_get_PayloadDataReader_m2911966679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataFramePartReader_get_PayloadDataReader_m2911966679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((DataFramePartReader_t1881953282_StaticFields*)DataFramePartReader_t1881953282_il2cpp_TypeInfo_var->static_fields)->get_U3CPayloadDataReaderU3Ek__BackingField_3();
		V_0 = L_0;
		goto IL_0008;
	}

IL_0008:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::set_PayloadDataReader(WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader)
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t DataFramePartReader_set_PayloadDataReader_m2933328284_MetadataUsageId;
extern "C"  void DataFramePartReader_set_PayloadDataReader_m2933328284 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataFramePartReader_set_PayloadDataReader_m2933328284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		((DataFramePartReader_t1881953282_StaticFields*)DataFramePartReader_t1881953282_il2cpp_TypeInfo_var->static_fields)->set_U3CPayloadDataReaderU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::.ctor()
extern "C"  void DataFramePartReader__ctor_m2067200615 (DataFramePartReader_t1881953282 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 WebSocket4Net.Protocol.FramePartReader.ExtendedLenghtReader::Process(System.Int32,WebSocket4Net.Protocol.WebSocketDataFrame,WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader&)
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern TypeInfo* IDataFramePartReader_t2102555929_il2cpp_TypeInfo_var;
extern const uint32_t ExtendedLenghtReader_Process_m2119835999_MetadataUsageId;
extern "C"  int32_t ExtendedLenghtReader_Process_m2119835999 (ExtendedLenghtReader_t3744404771 * __this, int32_t ___lastLength, WebSocketDataFrame_t222733806 * ___frame, Il2CppObject ** ___nextPartReader, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExtendedLenghtReader_Process_m2119835999_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 2;
		WebSocketDataFrame_t222733806 * L_0 = ___frame;
		NullCheck(L_0);
		int8_t L_1 = WebSocketDataFrame_get_PayloadLenght_m3291416379(L_0, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)((int32_t)126)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_2;
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)2));
		goto IL_001e;
	}

IL_001a:
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)8));
	}

IL_001e:
	{
		WebSocketDataFrame_t222733806 * L_5 = ___frame;
		NullCheck(L_5);
		int32_t L_6 = WebSocketDataFrame_get_Length_m3046518540(L_5, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		V_2 = (bool)((((int32_t)((((int32_t)L_6) < ((int32_t)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_8 = V_2;
		if (L_8)
		{
			goto IL_0036;
		}
	}
	{
		Il2CppObject ** L_9 = ___nextPartReader;
		*((Il2CppObject **)(L_9)) = (Il2CppObject *)__this;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_9), (Il2CppObject *)__this);
		V_1 = (-1);
		goto IL_0099;
	}

IL_0036:
	{
		WebSocketDataFrame_t222733806 * L_10 = ___frame;
		NullCheck(L_10);
		bool L_11 = WebSocketDataFrame_get_HasMask_m399471176(L_10, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_11) == ((int32_t)0))? 1 : 0);
		bool L_12 = V_2;
		if (L_12)
		{
			goto IL_004c;
		}
	}
	{
		Il2CppObject ** L_13 = ___nextPartReader;
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		Il2CppObject * L_14 = DataFramePartReader_get_MaskKeyReader_m287680978(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_13)) = (Il2CppObject *)L_14;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_13), (Il2CppObject *)L_14);
		goto IL_0078;
	}

IL_004c:
	{
		WebSocketDataFrame_t222733806 * L_15 = ___frame;
		NullCheck(L_15);
		int64_t L_16 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_15, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)((((int64_t)L_16) == ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_17 = V_2;
		if (L_17)
		{
			goto IL_0070;
		}
	}
	{
		Il2CppObject ** L_18 = ___nextPartReader;
		*((Il2CppObject **)(L_18)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_18), (Il2CppObject *)NULL);
		WebSocketDataFrame_t222733806 * L_19 = ___frame;
		NullCheck(L_19);
		int32_t L_20 = WebSocketDataFrame_get_Length_m3046518540(L_19, /*hidden argument*/NULL);
		int32_t L_21 = V_0;
		V_1 = (((int32_t)((int32_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_20)))-(int64_t)(((int64_t)((int64_t)L_21))))))));
		goto IL_0099;
	}

IL_0070:
	{
		Il2CppObject ** L_22 = ___nextPartReader;
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		Il2CppObject * L_23 = DataFramePartReader_get_PayloadDataReader_m2911966679(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_22)) = (Il2CppObject *)L_23;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_22), (Il2CppObject *)L_23);
	}

IL_0078:
	{
		WebSocketDataFrame_t222733806 * L_24 = ___frame;
		NullCheck(L_24);
		int32_t L_25 = WebSocketDataFrame_get_Length_m3046518540(L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_0;
		V_2 = (bool)((((int32_t)((((int32_t)L_25) > ((int32_t)L_26))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_27 = V_2;
		if (L_27)
		{
			goto IL_0095;
		}
	}
	{
		Il2CppObject ** L_28 = ___nextPartReader;
		int32_t L_29 = V_0;
		WebSocketDataFrame_t222733806 * L_30 = ___frame;
		Il2CppObject ** L_31 = ___nextPartReader;
		NullCheck((*((Il2CppObject **)L_28)));
		int32_t L_32 = InterfaceFuncInvoker3< int32_t, int32_t, WebSocketDataFrame_t222733806 *, Il2CppObject ** >::Invoke(0 /* System.Int32 WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader::Process(System.Int32,WebSocket4Net.Protocol.WebSocketDataFrame,WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader&) */, IDataFramePartReader_t2102555929_il2cpp_TypeInfo_var, (*((Il2CppObject **)L_28)), L_29, L_30, L_31);
		V_1 = L_32;
		goto IL_0099;
	}

IL_0095:
	{
		V_1 = 0;
		goto IL_0099;
	}

IL_0099:
	{
		int32_t L_33 = V_1;
		return L_33;
	}
}
// System.Void WebSocket4Net.Protocol.FramePartReader.ExtendedLenghtReader::.ctor()
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t ExtendedLenghtReader__ctor_m3059678476_MetadataUsageId;
extern "C"  void ExtendedLenghtReader__ctor_m3059678476 (ExtendedLenghtReader_t3744404771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExtendedLenghtReader__ctor_m3059678476_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		DataFramePartReader__ctor_m2067200615(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 WebSocket4Net.Protocol.FramePartReader.FixPartReader::Process(System.Int32,WebSocket4Net.Protocol.WebSocketDataFrame,WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader&)
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern TypeInfo* IDataFramePartReader_t2102555929_il2cpp_TypeInfo_var;
extern const uint32_t FixPartReader_Process_m94731578_MetadataUsageId;
extern "C"  int32_t FixPartReader_Process_m94731578 (FixPartReader_t3993251252 * __this, int32_t ___lastLength, WebSocketDataFrame_t222733806 * ___frame, Il2CppObject ** ___nextPartReader, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FixPartReader_Process_m94731578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		WebSocketDataFrame_t222733806 * L_0 = ___frame;
		NullCheck(L_0);
		int32_t L_1 = WebSocketDataFrame_get_Length_m3046518540(L_0, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)((((int32_t)L_1) < ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject ** L_3 = ___nextPartReader;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)__this;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)__this);
		V_0 = (-1);
		goto IL_009d;
	}

IL_001c:
	{
		WebSocketDataFrame_t222733806 * L_4 = ___frame;
		NullCheck(L_4);
		int8_t L_5 = WebSocketDataFrame_get_PayloadLenght_m3291416379(L_4, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)((((int32_t)L_5) < ((int32_t)((int32_t)126)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_1;
		if (L_6)
		{
			goto IL_0073;
		}
	}
	{
		WebSocketDataFrame_t222733806 * L_7 = ___frame;
		NullCheck(L_7);
		bool L_8 = WebSocketDataFrame_get_HasMask_m399471176(L_7, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_8) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_1;
		if (L_9)
		{
			goto IL_0044;
		}
	}
	{
		Il2CppObject ** L_10 = ___nextPartReader;
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		Il2CppObject * L_11 = DataFramePartReader_get_MaskKeyReader_m287680978(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_10)) = (Il2CppObject *)L_11;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_10), (Il2CppObject *)L_11);
		goto IL_0070;
	}

IL_0044:
	{
		WebSocketDataFrame_t222733806 * L_12 = ___frame;
		NullCheck(L_12);
		int64_t L_13 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_12, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)((((int64_t)L_13) == ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_14 = V_1;
		if (L_14)
		{
			goto IL_0068;
		}
	}
	{
		Il2CppObject ** L_15 = ___nextPartReader;
		*((Il2CppObject **)(L_15)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_15), (Il2CppObject *)NULL);
		WebSocketDataFrame_t222733806 * L_16 = ___frame;
		NullCheck(L_16);
		int32_t L_17 = WebSocketDataFrame_get_Length_m3046518540(L_16, /*hidden argument*/NULL);
		V_0 = (((int32_t)((int32_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_17)))-(int64_t)(((int64_t)((int64_t)2))))))));
		goto IL_009d;
	}

IL_0068:
	{
		Il2CppObject ** L_18 = ___nextPartReader;
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		Il2CppObject * L_19 = DataFramePartReader_get_PayloadDataReader_m2911966679(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_18)) = (Il2CppObject *)L_19;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_18), (Il2CppObject *)L_19);
	}

IL_0070:
	{
		goto IL_007c;
	}

IL_0073:
	{
		Il2CppObject ** L_20 = ___nextPartReader;
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		Il2CppObject * L_21 = DataFramePartReader_get_ExtendedLenghtReader_m901854720(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_20)) = (Il2CppObject *)L_21;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_20), (Il2CppObject *)L_21);
	}

IL_007c:
	{
		WebSocketDataFrame_t222733806 * L_22 = ___frame;
		NullCheck(L_22);
		int32_t L_23 = WebSocketDataFrame_get_Length_m3046518540(L_22, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)((((int32_t)L_23) > ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_24 = V_1;
		if (L_24)
		{
			goto IL_0099;
		}
	}
	{
		Il2CppObject ** L_25 = ___nextPartReader;
		WebSocketDataFrame_t222733806 * L_26 = ___frame;
		Il2CppObject ** L_27 = ___nextPartReader;
		NullCheck((*((Il2CppObject **)L_25)));
		int32_t L_28 = InterfaceFuncInvoker3< int32_t, int32_t, WebSocketDataFrame_t222733806 *, Il2CppObject ** >::Invoke(0 /* System.Int32 WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader::Process(System.Int32,WebSocket4Net.Protocol.WebSocketDataFrame,WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader&) */, IDataFramePartReader_t2102555929_il2cpp_TypeInfo_var, (*((Il2CppObject **)L_25)), 2, L_26, L_27);
		V_0 = L_28;
		goto IL_009d;
	}

IL_0099:
	{
		V_0 = 0;
		goto IL_009d;
	}

IL_009d:
	{
		int32_t L_29 = V_0;
		return L_29;
	}
}
// System.Void WebSocket4Net.Protocol.FramePartReader.FixPartReader::.ctor()
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t FixPartReader__ctor_m3383315125_MetadataUsageId;
extern "C"  void FixPartReader__ctor_m3383315125 (FixPartReader_t3993251252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FixPartReader__ctor_m3383315125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		DataFramePartReader__ctor_m2067200615(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 WebSocket4Net.Protocol.FramePartReader.MaskKeyReader::Process(System.Int32,WebSocket4Net.Protocol.WebSocketDataFrame,WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader&)
extern TypeInfo* PayloadDataReader_t544241252_il2cpp_TypeInfo_var;
extern TypeInfo* IDataFramePartReader_t2102555929_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegmentList_1_ToArrayData_m509443740_MethodInfo_var;
extern const uint32_t MaskKeyReader_Process_m3881637349_MetadataUsageId;
extern "C"  int32_t MaskKeyReader_Process_m3881637349 (MaskKeyReader_t360839071 * __this, int32_t ___lastLength, WebSocketDataFrame_t222733806 * ___frame, Il2CppObject ** ___nextPartReader, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MaskKeyReader_Process_m3881637349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		int32_t L_0 = ___lastLength;
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)4));
		WebSocketDataFrame_t222733806 * L_1 = ___frame;
		NullCheck(L_1);
		int32_t L_2 = WebSocketDataFrame_get_Length_m3046518540(L_1, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		V_2 = (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)L_3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_2;
		if (L_4)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject ** L_5 = ___nextPartReader;
		*((Il2CppObject **)(L_5)) = (Il2CppObject *)__this;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_5), (Il2CppObject *)__this);
		V_1 = (-1);
		goto IL_007c;
	}

IL_001d:
	{
		WebSocketDataFrame_t222733806 * L_6 = ___frame;
		WebSocketDataFrame_t222733806 * L_7 = ___frame;
		NullCheck(L_7);
		ArraySegmentList_t1783467835 * L_8 = WebSocketDataFrame_get_InnerData_m815354334(L_7, /*hidden argument*/NULL);
		int32_t L_9 = ___lastLength;
		NullCheck(L_8);
		ByteU5BU5D_t58506160* L_10 = ArraySegmentList_1_ToArrayData_m509443740(L_8, L_9, 4, /*hidden argument*/ArraySegmentList_1_ToArrayData_m509443740_MethodInfo_var);
		NullCheck(L_6);
		WebSocketDataFrame_set_MaskKey_m982527904(L_6, L_10, /*hidden argument*/NULL);
		WebSocketDataFrame_t222733806 * L_11 = ___frame;
		NullCheck(L_11);
		int64_t L_12 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_11, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)((((int64_t)L_12) == ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_13 = V_2;
		if (L_13)
		{
			goto IL_0054;
		}
	}
	{
		Il2CppObject ** L_14 = ___nextPartReader;
		*((Il2CppObject **)(L_14)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_14), (Il2CppObject *)NULL);
		WebSocketDataFrame_t222733806 * L_15 = ___frame;
		NullCheck(L_15);
		int32_t L_16 = WebSocketDataFrame_get_Length_m3046518540(L_15, /*hidden argument*/NULL);
		int32_t L_17 = V_0;
		V_1 = (((int32_t)((int32_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_16)))-(int64_t)(((int64_t)((int64_t)L_17))))))));
		goto IL_007c;
	}

IL_0054:
	{
		Il2CppObject ** L_18 = ___nextPartReader;
		PayloadDataReader_t544241252 * L_19 = (PayloadDataReader_t544241252 *)il2cpp_codegen_object_new(PayloadDataReader_t544241252_il2cpp_TypeInfo_var);
		PayloadDataReader__ctor_m191259013(L_19, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_18)) = (Il2CppObject *)L_19;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_18), (Il2CppObject *)L_19);
		WebSocketDataFrame_t222733806 * L_20 = ___frame;
		NullCheck(L_20);
		int32_t L_21 = WebSocketDataFrame_get_Length_m3046518540(L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_0;
		V_2 = (bool)((((int32_t)((((int32_t)L_21) > ((int32_t)L_22))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_23 = V_2;
		if (L_23)
		{
			goto IL_0078;
		}
	}
	{
		Il2CppObject ** L_24 = ___nextPartReader;
		int32_t L_25 = V_0;
		WebSocketDataFrame_t222733806 * L_26 = ___frame;
		Il2CppObject ** L_27 = ___nextPartReader;
		NullCheck((*((Il2CppObject **)L_24)));
		int32_t L_28 = InterfaceFuncInvoker3< int32_t, int32_t, WebSocketDataFrame_t222733806 *, Il2CppObject ** >::Invoke(0 /* System.Int32 WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader::Process(System.Int32,WebSocket4Net.Protocol.WebSocketDataFrame,WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader&) */, IDataFramePartReader_t2102555929_il2cpp_TypeInfo_var, (*((Il2CppObject **)L_24)), L_25, L_26, L_27);
		V_1 = L_28;
		goto IL_007c;
	}

IL_0078:
	{
		V_1 = 0;
		goto IL_007c;
	}

IL_007c:
	{
		int32_t L_29 = V_1;
		return L_29;
	}
}
// System.Void WebSocket4Net.Protocol.FramePartReader.MaskKeyReader::.ctor()
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t MaskKeyReader__ctor_m2819127594_MetadataUsageId;
extern "C"  void MaskKeyReader__ctor_m2819127594 (MaskKeyReader_t360839071 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MaskKeyReader__ctor_m2819127594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		DataFramePartReader__ctor_m2067200615(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 WebSocket4Net.Protocol.FramePartReader.PayloadDataReader::Process(System.Int32,WebSocket4Net.Protocol.WebSocketDataFrame,WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader&)
extern "C"  int32_t PayloadDataReader_Process_m1990250346 (PayloadDataReader_t544241252 * __this, int32_t ___lastLength, WebSocketDataFrame_t222733806 * ___frame, Il2CppObject ** ___nextPartReader, const MethodInfo* method)
{
	int64_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		int32_t L_0 = ___lastLength;
		WebSocketDataFrame_t222733806 * L_1 = ___frame;
		NullCheck(L_1);
		int64_t L_2 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_1, /*hidden argument*/NULL);
		V_0 = ((int64_t)((int64_t)(((int64_t)((int64_t)L_0)))+(int64_t)L_2));
		WebSocketDataFrame_t222733806 * L_3 = ___frame;
		NullCheck(L_3);
		int32_t L_4 = WebSocketDataFrame_get_Length_m3046518540(L_3, /*hidden argument*/NULL);
		int64_t L_5 = V_0;
		V_2 = (bool)((((int32_t)((((int64_t)(((int64_t)((int64_t)L_4)))) < ((int64_t)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_2;
		if (L_6)
		{
			goto IL_0024;
		}
	}
	{
		Il2CppObject ** L_7 = ___nextPartReader;
		*((Il2CppObject **)(L_7)) = (Il2CppObject *)__this;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_7), (Il2CppObject *)__this);
		V_1 = (-1);
		goto IL_0034;
	}

IL_0024:
	{
		Il2CppObject ** L_8 = ___nextPartReader;
		*((Il2CppObject **)(L_8)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_8), (Il2CppObject *)NULL);
		WebSocketDataFrame_t222733806 * L_9 = ___frame;
		NullCheck(L_9);
		int32_t L_10 = WebSocketDataFrame_get_Length_m3046518540(L_9, /*hidden argument*/NULL);
		int64_t L_11 = V_0;
		V_1 = (((int32_t)((int32_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_10)))-(int64_t)L_11)))));
		goto IL_0034;
	}

IL_0034:
	{
		int32_t L_12 = V_1;
		return L_12;
	}
}
// System.Void WebSocket4Net.Protocol.FramePartReader.PayloadDataReader::.ctor()
extern TypeInfo* DataFramePartReader_t1881953282_il2cpp_TypeInfo_var;
extern const uint32_t PayloadDataReader__ctor_m191259013_MetadataUsageId;
extern "C"  void PayloadDataReader__ctor_m191259013 (PayloadDataReader_t544241252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PayloadDataReader__ctor_m191259013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DataFramePartReader_t1881953282_il2cpp_TypeInfo_var);
		DataFramePartReader__ctor_m2067200615(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.HandshakeReader::.cctor()
extern TypeInfo* HandshakeReader_t1078082956_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral397306;
extern const uint32_t HandshakeReader__cctor_m3042142610_MetadataUsageId;
extern "C"  void HandshakeReader__cctor_m3042142610 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HandshakeReader__cctor_m3042142610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)400);
		String_t* L_0 = Int32_ToString_m1286526384((&V_0), /*hidden argument*/NULL);
		((HandshakeReader_t1078082956_StaticFields*)HandshakeReader_t1078082956_il2cpp_TypeInfo_var->static_fields)->set_BadRequestCode_3(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_1 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		ByteU5BU5D_t58506160* L_2 = VirtFuncInvoker1< ByteU5BU5D_t58506160*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_1, _stringLiteral397306);
		((HandshakeReader_t1078082956_StaticFields*)HandshakeReader_t1078082956_il2cpp_TypeInfo_var->static_fields)->set_HeaderTerminator_4(L_2);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.HandshakeReader::.ctor(WebSocket4Net.WebSocket)
extern TypeInfo* HandshakeReader_t1078082956_il2cpp_TypeInfo_var;
extern TypeInfo* SearchMarkState_1_t304232942_il2cpp_TypeInfo_var;
extern const MethodInfo* SearchMarkState_1__ctor_m2012179761_MethodInfo_var;
extern const uint32_t HandshakeReader__ctor_m4265408334_MetadataUsageId;
extern "C"  void HandshakeReader__ctor_m4265408334 (HandshakeReader_t1078082956 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HandshakeReader__ctor_m4265408334_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebSocket_t713846903 * L_0 = ___websocket;
		ReaderBase__ctor_m3143587916(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HandshakeReader_t1078082956_il2cpp_TypeInfo_var);
		ByteU5BU5D_t58506160* L_1 = ((HandshakeReader_t1078082956_StaticFields*)HandshakeReader_t1078082956_il2cpp_TypeInfo_var->static_fields)->get_HeaderTerminator_4();
		SearchMarkState_1_t304232942 * L_2 = (SearchMarkState_1_t304232942 *)il2cpp_codegen_object_new(SearchMarkState_1_t304232942_il2cpp_TypeInfo_var);
		SearchMarkState_1__ctor_m2012179761(L_2, L_1, /*hidden argument*/SearchMarkState_1__ctor_m2012179761_MethodInfo_var);
		__this->set_m_HeadSeachState_5(L_2);
		return;
	}
}
// WebSocket4Net.WebSocketCommandInfo WebSocket4Net.Protocol.HandshakeReader::GetCommandInfo(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern TypeInfo* Extensions_t80775955_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* HandshakeReader_t1078082956_il2cpp_TypeInfo_var;
extern TypeInfo* WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var;
extern const MethodInfo* SearchMarkState_1_get_Matched_m3815954317_MethodInfo_var;
extern const MethodInfo* Extensions_SearchMark_TisByte_t2778693821_m1179758205_MethodInfo_var;
extern const MethodInfo* ArraySegmentList_1_ClearSegements_m3355833128_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3198384639;
extern const uint32_t HandshakeReader_GetCommandInfo_m2339654721_MetadataUsageId;
extern "C"  WebSocketCommandInfo_t3536916738 * HandshakeReader_GetCommandInfo_m2339654721 (HandshakeReader_t1078082956 * __this, ByteU5BU5D_t58506160* ___readBuffer, int32_t ___offset, int32_t ___length, int32_t* ___left, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HandshakeReader_GetCommandInfo_m2339654721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	WebSocketCommandInfo_t3536916738 * V_4 = NULL;
	WebSocketCommandInfo_t3536916738 * V_5 = NULL;
	WebSocketCommandInfo_t3536916738 * V_6 = NULL;
	bool V_7 = false;
	int32_t V_8 = 0;
	{
		int32_t* L_0 = ___left;
		*((int32_t*)(L_0)) = (int32_t)0;
		SearchMarkState_1_t304232942 * L_1 = __this->get_m_HeadSeachState_5();
		NullCheck(L_1);
		int32_t L_2 = SearchMarkState_1_get_Matched_m3815954317(L_1, /*hidden argument*/SearchMarkState_1_get_Matched_m3815954317_MethodInfo_var);
		V_0 = L_2;
		ByteU5BU5D_t58506160* L_3 = ___readBuffer;
		int32_t L_4 = ___offset;
		int32_t L_5 = ___length;
		SearchMarkState_1_t304232942 * L_6 = __this->get_m_HeadSeachState_5();
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t80775955_il2cpp_TypeInfo_var);
		int32_t L_7 = Extensions_SearchMark_TisByte_t2778693821_m1179758205(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_3, L_4, L_5, L_6, /*hidden argument*/Extensions_SearchMark_TisByte_t2778693821_m1179758205_MethodInfo_var);
		V_1 = L_7;
		int32_t L_8 = V_1;
		V_7 = (bool)((((int32_t)((((int32_t)L_8) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_7;
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		ByteU5BU5D_t58506160* L_10 = ___readBuffer;
		int32_t L_11 = ___offset;
		int32_t L_12 = ___length;
		ReaderBase_AddArraySegment_m3437881651(__this, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_6 = (WebSocketCommandInfo_t3536916738 *)NULL;
		goto IL_0146;
	}

IL_0040:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = ___offset;
		V_2 = ((int32_t)((int32_t)L_13-(int32_t)L_14));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_15;
		ArraySegmentList_t1783467835 * L_16 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_16);
		V_7 = (bool)((((int32_t)((((int32_t)L_17) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_18 = V_7;
		if (L_18)
		{
			goto IL_00b2;
		}
	}
	{
		int32_t L_19 = V_2;
		V_7 = (bool)((((int32_t)((((int32_t)L_19) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_20 = V_7;
		if (L_20)
		{
			goto IL_008e;
		}
	}
	{
		ByteU5BU5D_t58506160* L_21 = ___readBuffer;
		int32_t L_22 = ___offset;
		int32_t L_23 = V_2;
		ReaderBase_AddArraySegment_m3437881651(__this, L_21, L_22, L_23, /*hidden argument*/NULL);
		ArraySegmentList_t1783467835 * L_24 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_25 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		String_t* L_26 = ArraySegmentList_Decode_m4038946727(L_24, L_25, /*hidden argument*/NULL);
		V_3 = L_26;
		goto IL_00af;
	}

IL_008e:
	{
		ArraySegmentList_t1783467835 * L_27 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_28 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArraySegmentList_t1783467835 * L_29 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_29);
		int32_t L_31 = V_0;
		NullCheck(L_27);
		String_t* L_32 = ArraySegmentList_Decode_m3168039495(L_27, L_28, 0, ((int32_t)((int32_t)L_30-(int32_t)L_31)), /*hidden argument*/NULL);
		V_3 = L_32;
	}

IL_00af:
	{
		goto IL_00c2;
	}

IL_00b2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_33 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_34 = ___readBuffer;
		int32_t L_35 = ___offset;
		int32_t L_36 = V_2;
		NullCheck(L_33);
		String_t* L_37 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_33, L_34, L_35, L_36);
		V_3 = L_37;
	}

IL_00c2:
	{
		int32_t* L_38 = ___left;
		int32_t L_39 = ___length;
		int32_t L_40 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(HandshakeReader_t1078082956_il2cpp_TypeInfo_var);
		ByteU5BU5D_t58506160* L_41 = ((HandshakeReader_t1078082956_StaticFields*)HandshakeReader_t1078082956_il2cpp_TypeInfo_var->static_fields)->get_HeaderTerminator_4();
		NullCheck(L_41);
		int32_t L_42 = V_0;
		*((int32_t*)(L_38)) = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_39-(int32_t)L_40))-(int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_41)->max_length))))-(int32_t)L_42))));
		ArraySegmentList_t1783467835 * L_43 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		NullCheck(L_43);
		ArraySegmentList_1_ClearSegements_m3355833128(L_43, /*hidden argument*/ArraySegmentList_1_ClearSegements_m3355833128_MethodInfo_var);
		String_t* L_44 = V_3;
		NullCheck(L_44);
		bool L_45 = String_StartsWith_m495172832(L_44, _stringLiteral3198384639, 5, /*hidden argument*/NULL);
		V_7 = L_45;
		bool L_46 = V_7;
		if (L_46)
		{
			goto IL_0119;
		}
	}
	{
		WebSocketCommandInfo_t3536916738 * L_47 = (WebSocketCommandInfo_t3536916738 *)il2cpp_codegen_object_new(WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var);
		WebSocketCommandInfo__ctor_m135330605(L_47, /*hidden argument*/NULL);
		V_4 = L_47;
		WebSocketCommandInfo_t3536916738 * L_48 = V_4;
		V_8 = (-1);
		String_t* L_49 = Int32_ToString_m1286526384((&V_8), /*hidden argument*/NULL);
		NullCheck(L_48);
		WebSocketCommandInfo_set_Key_m3535408597(L_48, L_49, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_50 = V_4;
		String_t* L_51 = V_3;
		NullCheck(L_50);
		WebSocketCommandInfo_set_Text_m425876161(L_50, L_51, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_52 = V_4;
		V_6 = L_52;
		goto IL_0146;
	}

IL_0119:
	{
		WebSocketCommandInfo_t3536916738 * L_53 = (WebSocketCommandInfo_t3536916738 *)il2cpp_codegen_object_new(WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var);
		WebSocketCommandInfo__ctor_m135330605(L_53, /*hidden argument*/NULL);
		V_5 = L_53;
		WebSocketCommandInfo_t3536916738 * L_54 = V_5;
		V_8 = ((int32_t)400);
		String_t* L_55 = Int32_ToString_m1286526384((&V_8), /*hidden argument*/NULL);
		NullCheck(L_54);
		WebSocketCommandInfo_set_Key_m3535408597(L_54, L_55, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_56 = V_5;
		String_t* L_57 = V_3;
		NullCheck(L_56);
		WebSocketCommandInfo_set_Text_m425876161(L_56, L_57, /*hidden argument*/NULL);
		WebSocketCommandInfo_t3536916738 * L_58 = V_5;
		V_6 = L_58;
		goto IL_0146;
	}

IL_0146:
	{
		WebSocketCommandInfo_t3536916738 * L_59 = V_6;
		return L_59;
	}
}
// System.Void WebSocket4Net.Protocol.ProtocolProcessorBase::.ctor(WebSocket4Net.WebSocketVersion,WebSocket4Net.Protocol.ICloseStatusCode)
extern "C"  void ProtocolProcessorBase__ctor_m448700961 (ProtocolProcessorBase_t894595261 * __this, int32_t ___version, Il2CppObject * ___closeStatusCode, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___closeStatusCode;
		ProtocolProcessorBase_set_CloseStatusCode_m3560475338(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___version;
		ProtocolProcessorBase_set_Version_m182824756(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___version;
		V_0 = L_2;
		String_t* L_3 = Int32_ToString_m1286526384((&V_0), /*hidden argument*/NULL);
		ProtocolProcessorBase_set_VersionTag_m2556159721(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// WebSocket4Net.Protocol.ICloseStatusCode WebSocket4Net.Protocol.ProtocolProcessorBase::get_CloseStatusCode()
extern "C"  Il2CppObject * ProtocolProcessorBase_get_CloseStatusCode_m779297321 (ProtocolProcessorBase_t894595261 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U3CCloseStatusCodeU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.ProtocolProcessorBase::set_CloseStatusCode(WebSocket4Net.Protocol.ICloseStatusCode)
extern "C"  void ProtocolProcessorBase_set_CloseStatusCode_m3560475338 (ProtocolProcessorBase_t894595261 * __this, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value;
		__this->set_U3CCloseStatusCodeU3Ek__BackingField_0(L_0);
		return;
	}
}
// WebSocket4Net.WebSocketVersion WebSocket4Net.Protocol.ProtocolProcessorBase::get_Version()
extern "C"  int32_t ProtocolProcessorBase_get_Version_m961545797 (ProtocolProcessorBase_t894595261 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CVersionU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.ProtocolProcessorBase::set_Version(WebSocket4Net.WebSocketVersion)
extern "C"  void ProtocolProcessorBase_set_Version_m182824756 (ProtocolProcessorBase_t894595261 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CVersionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String WebSocket4Net.Protocol.ProtocolProcessorBase::get_VersionTag()
extern "C"  String_t* ProtocolProcessorBase_get_VersionTag_m3245039016 (ProtocolProcessorBase_t894595261 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CVersionTagU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.ProtocolProcessorBase::set_VersionTag(System.String)
extern "C"  void ProtocolProcessorBase_set_VersionTag_m2556159721 (ProtocolProcessorBase_t894595261 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CVersionTagU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.ProtocolProcessorFactory::.ctor(WebSocket4Net.Protocol.IProtocolProcessor[])
extern TypeInfo* ProtocolProcessorFactory_t3499308290_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t2410725500_il2cpp_TypeInfo_var;
extern const MethodInfo* ProtocolProcessorFactory_U3C_ctorU3Eb__0_m1829162418_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m383676062_MethodInfo_var;
extern const MethodInfo* Enumerable_OrderByDescending_TisIProtocolProcessor_t4208156067_TisInt32_t2847414787_m2419427767_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisIProtocolProcessor_t4208156067_m2091042025_MethodInfo_var;
extern const uint32_t ProtocolProcessorFactory__ctor_m557102662_MetadataUsageId;
extern "C"  void ProtocolProcessorFactory__ctor_m557102662 (ProtocolProcessorFactory_t3499308290 * __this, IProtocolProcessorU5BU5D_t2571124850* ___processors, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ProtocolProcessorFactory__ctor_m557102662_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IProtocolProcessorU5BU5D_t2571124850* G_B2_0 = NULL;
	ProtocolProcessorFactory_t3499308290 * G_B2_1 = NULL;
	IProtocolProcessorU5BU5D_t2571124850* G_B1_0 = NULL;
	ProtocolProcessorFactory_t3499308290 * G_B1_1 = NULL;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		IProtocolProcessorU5BU5D_t2571124850* L_0 = ___processors;
		Func_2_t2410725500 * L_1 = ((ProtocolProcessorFactory_t3499308290_StaticFields*)ProtocolProcessorFactory_t3499308290_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_0024;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)ProtocolProcessorFactory_U3C_ctorU3Eb__0_m1829162418_MethodInfo_var);
		Func_2_t2410725500 * L_3 = (Func_2_t2410725500 *)il2cpp_codegen_object_new(Func_2_t2410725500_il2cpp_TypeInfo_var);
		Func_2__ctor_m383676062(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m383676062_MethodInfo_var);
		((ProtocolProcessorFactory_t3499308290_StaticFields*)ProtocolProcessorFactory_t3499308290_il2cpp_TypeInfo_var->static_fields)->set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		goto IL_0024;
	}

IL_0024:
	{
		Func_2_t2410725500 * L_4 = ((ProtocolProcessorFactory_t3499308290_StaticFields*)ProtocolProcessorFactory_t3499308290_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1();
		Il2CppObject* L_5 = Enumerable_OrderByDescending_TisIProtocolProcessor_t4208156067_TisInt32_t2847414787_m2419427767(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B2_0, L_4, /*hidden argument*/Enumerable_OrderByDescending_TisIProtocolProcessor_t4208156067_TisInt32_t2847414787_m2419427767_MethodInfo_var);
		IProtocolProcessorU5BU5D_t2571124850* L_6 = Enumerable_ToArray_TisIProtocolProcessor_t4208156067_m2091042025(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_ToArray_TisIProtocolProcessor_t4208156067_m2091042025_MethodInfo_var);
		NullCheck(G_B2_1);
		G_B2_1->set_m_OrderedProcessors_0(L_6);
		return;
	}
}
// WebSocket4Net.Protocol.IProtocolProcessor WebSocket4Net.Protocol.ProtocolProcessorFactory::GetProcessorByVersion(WebSocket4Net.WebSocketVersion)
extern TypeInfo* U3CU3Ec__DisplayClass3_t2826400220_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t4069283350_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass3_U3CGetProcessorByVersionU3Eb__2_m229891044_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m56891780_MethodInfo_var;
extern const MethodInfo* Enumerable_FirstOrDefault_TisIProtocolProcessor_t4208156067_m1442144529_MethodInfo_var;
extern const uint32_t ProtocolProcessorFactory_GetProcessorByVersion_m1724826559_MetadataUsageId;
extern "C"  Il2CppObject * ProtocolProcessorFactory_GetProcessorByVersion_m1724826559 (ProtocolProcessorFactory_t3499308290 * __this, int32_t ___version, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ProtocolProcessorFactory_GetProcessorByVersion_m1724826559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass3_t2826400220 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CU3Ec__DisplayClass3_t2826400220 * L_0 = (U3CU3Ec__DisplayClass3_t2826400220 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass3_t2826400220_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass3__ctor_m3256459194(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass3_t2826400220 * L_1 = V_0;
		int32_t L_2 = ___version;
		NullCheck(L_1);
		L_1->set_version_0(L_2);
		IProtocolProcessorU5BU5D_t2571124850* L_3 = __this->get_m_OrderedProcessors_0();
		U3CU3Ec__DisplayClass3_t2826400220 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)U3CU3Ec__DisplayClass3_U3CGetProcessorByVersionU3Eb__2_m229891044_MethodInfo_var);
		Func_2_t4069283350 * L_6 = (Func_2_t4069283350 *)il2cpp_codegen_object_new(Func_2_t4069283350_il2cpp_TypeInfo_var);
		Func_2__ctor_m56891780(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m56891780_MethodInfo_var);
		Il2CppObject * L_7 = Enumerable_FirstOrDefault_TisIProtocolProcessor_t4208156067_m1442144529(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_3, L_6, /*hidden argument*/Enumerable_FirstOrDefault_TisIProtocolProcessor_t4208156067_m1442144529_MethodInfo_var);
		V_1 = L_7;
		goto IL_0028;
	}

IL_0028:
	{
		Il2CppObject * L_8 = V_1;
		return L_8;
	}
}
// WebSocket4Net.Protocol.IProtocolProcessor WebSocket4Net.Protocol.ProtocolProcessorFactory::GetPreferedProcessorFromAvialable(System.Int32[])
extern TypeInfo* ProtocolProcessorFactory_t3499308290_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t1649583772_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t1424601847_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t35553939_il2cpp_TypeInfo_var;
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* ProtocolProcessorFactory_U3CGetPreferedProcessorFromAvialableU3Eb__5_m2783378925_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1676027518_MethodInfo_var;
extern const MethodInfo* Enumerable_OrderByDescending_TisInt32_t2847414787_TisInt32_t2847414787_m3192897055_MethodInfo_var;
extern const uint32_t ProtocolProcessorFactory_GetPreferedProcessorFromAvialable_m2607568602_MetadataUsageId;
extern "C"  Il2CppObject * ProtocolProcessorFactory_GetPreferedProcessorFromAvialable_m2607568602 (ProtocolProcessorFactory_t3499308290 * __this, Int32U5BU5D_t1809983122* ___versions, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ProtocolProcessorFactory_GetPreferedProcessorFromAvialable_m2607568602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	IProtocolProcessorU5BU5D_t2571124850* V_5 = NULL;
	int32_t V_6 = 0;
	bool V_7 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Int32U5BU5D_t1809983122* G_B2_0 = NULL;
	Int32U5BU5D_t1809983122* G_B1_0 = NULL;
	{
		Int32U5BU5D_t1809983122* L_0 = ___versions;
		Func_2_t1649583772 * L_1 = ((ProtocolProcessorFactory_t3499308290_StaticFields*)ProtocolProcessorFactory_t3499308290_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001d;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)ProtocolProcessorFactory_U3CGetPreferedProcessorFromAvialableU3Eb__5_m2783378925_MethodInfo_var);
		Func_2_t1649583772 * L_3 = (Func_2_t1649583772 *)il2cpp_codegen_object_new(Func_2_t1649583772_il2cpp_TypeInfo_var);
		Func_2__ctor_m1676027518(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m1676027518_MethodInfo_var);
		((ProtocolProcessorFactory_t3499308290_StaticFields*)ProtocolProcessorFactory_t3499308290_il2cpp_TypeInfo_var->static_fields)->set_CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2(L_3);
		G_B2_0 = G_B1_0;
		goto IL_001d;
	}

IL_001d:
	{
		Func_2_t1649583772 * L_4 = ((ProtocolProcessorFactory_t3499308290_StaticFields*)ProtocolProcessorFactory_t3499308290_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2();
		Il2CppObject* L_5 = Enumerable_OrderByDescending_TisInt32_t2847414787_TisInt32_t2847414787_m3192897055(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B2_0, L_4, /*hidden argument*/Enumerable_OrderByDescending_TisInt32_t2847414787_TisInt32_t2847414787_m3192897055_MethodInfo_var);
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator() */, IEnumerable_1_t1424601847_il2cpp_TypeInfo_var, L_5);
		V_4 = L_6;
	}

IL_002e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_008c;
		}

IL_0030:
		{
			Il2CppObject* L_7 = V_4;
			NullCheck(L_7);
			int32_t L_8 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IEnumerator_1_t35553939_il2cpp_TypeInfo_var, L_7);
			V_0 = L_8;
			IProtocolProcessorU5BU5D_t2571124850* L_9 = __this->get_m_OrderedProcessors_0();
			V_5 = L_9;
			V_6 = 0;
			goto IL_007d;
		}

IL_0047:
		{
			IProtocolProcessorU5BU5D_t2571124850* L_10 = V_5;
			int32_t L_11 = V_6;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			V_1 = ((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
			Il2CppObject * L_13 = V_1;
			NullCheck(L_13);
			int32_t L_14 = InterfaceFuncInvoker0< int32_t >::Invoke(9 /* WebSocket4Net.WebSocketVersion WebSocket4Net.Protocol.IProtocolProcessor::get_Version() */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_13);
			V_2 = L_14;
			int32_t L_15 = V_2;
			int32_t L_16 = V_0;
			V_7 = (bool)((((int32_t)((((int32_t)L_15) < ((int32_t)L_16))? 1 : 0)) == ((int32_t)0))? 1 : 0);
			bool L_17 = V_7;
			if (L_17)
			{
				goto IL_0064;
			}
		}

IL_0062:
		{
			goto IL_008b;
		}

IL_0064:
		{
			int32_t L_18 = V_2;
			int32_t L_19 = V_0;
			V_7 = (bool)((((int32_t)((((int32_t)L_18) > ((int32_t)L_19))? 1 : 0)) == ((int32_t)0))? 1 : 0);
			bool L_20 = V_7;
			if (L_20)
			{
				goto IL_0073;
			}
		}

IL_0071:
		{
			goto IL_0077;
		}

IL_0073:
		{
			Il2CppObject * L_21 = V_1;
			V_3 = L_21;
			IL2CPP_LEAVE(0xB4, FINALLY_009b);
		}

IL_0077:
		{
			int32_t L_22 = V_6;
			V_6 = ((int32_t)((int32_t)L_22+(int32_t)1));
		}

IL_007d:
		{
			int32_t L_23 = V_6;
			IProtocolProcessorU5BU5D_t2571124850* L_24 = V_5;
			NullCheck(L_24);
			V_7 = (bool)((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length))))))? 1 : 0);
			bool L_25 = V_7;
			if (L_25)
			{
				goto IL_0047;
			}
		}

IL_008b:
		{
		}

IL_008c:
		{
			Il2CppObject* L_26 = V_4;
			NullCheck(L_26);
			bool L_27 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_26);
			V_7 = L_27;
			bool L_28 = V_7;
			if (L_28)
			{
				goto IL_0030;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xAF, FINALLY_009b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_009b;
	}

FINALLY_009b:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_29 = V_4;
			V_7 = (bool)((((Il2CppObject*)(Il2CppObject*)L_29) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
			bool L_30 = V_7;
			if (L_30)
			{
				goto IL_00ae;
			}
		}

IL_00a6:
		{
			Il2CppObject* L_31 = V_4;
			NullCheck(L_31);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_31);
		}

IL_00ae:
		{
			IL2CPP_END_FINALLY(155)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(155)
	{
		IL2CPP_JUMP_TBL(0xB4, IL_00b4)
		IL2CPP_JUMP_TBL(0xAF, IL_00af)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00af:
	{
		V_3 = (Il2CppObject *)NULL;
		goto IL_00b4;
	}

IL_00b4:
	{
		Il2CppObject * L_32 = V_3;
		return L_32;
	}
}
// System.Int32 WebSocket4Net.Protocol.ProtocolProcessorFactory::<.ctor>b__0(WebSocket4Net.Protocol.IProtocolProcessor)
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern const uint32_t ProtocolProcessorFactory_U3C_ctorU3Eb__0_m1829162418_MetadataUsageId;
extern "C"  int32_t ProtocolProcessorFactory_U3C_ctorU3Eb__0_m1829162418 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___p, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ProtocolProcessorFactory_U3C_ctorU3Eb__0_m1829162418_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = ___p;
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(9 /* WebSocket4Net.WebSocketVersion WebSocket4Net.Protocol.IProtocolProcessor::get_Version() */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 WebSocket4Net.Protocol.ProtocolProcessorFactory::<GetPreferedProcessorFromAvialable>b__5(System.Int32)
extern "C"  int32_t ProtocolProcessorFactory_U3CGetPreferedProcessorFromAvialableU3Eb__5_m2783378925 (Il2CppObject * __this /* static, unused */, int32_t ___i, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___i;
		V_0 = L_0;
		goto IL_0004;
	}

IL_0004:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.ProtocolProcessorFactory/<>c__DisplayClass3::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3__ctor_m3256459194 (U3CU3Ec__DisplayClass3_t2826400220 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocket4Net.Protocol.ProtocolProcessorFactory/<>c__DisplayClass3::<GetProcessorByVersion>b__2(WebSocket4Net.Protocol.IProtocolProcessor)
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__DisplayClass3_U3CGetProcessorByVersionU3Eb__2_m229891044_MetadataUsageId;
extern "C"  bool U3CU3Ec__DisplayClass3_U3CGetProcessorByVersionU3Eb__2_m229891044 (U3CU3Ec__DisplayClass3_t2826400220 * __this, Il2CppObject * ___p, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass3_U3CGetProcessorByVersionU3Eb__2_m229891044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Il2CppObject * L_0 = ___p;
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(9 /* WebSocket4Net.WebSocketVersion WebSocket4Net.Protocol.IProtocolProcessor::get_Version() */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_0);
		int32_t L_2 = __this->get_version_0();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void WebSocket4Net.Protocol.ReaderBase::set_WebSocket(WebSocket4Net.WebSocket)
extern "C"  void ReaderBase_set_WebSocket_m2856695396 (ReaderBase_t893022310 * __this, WebSocket_t713846903 * ___value, const MethodInfo* method)
{
	{
		WebSocket_t713846903 * L_0 = ___value;
		__this->set_U3CWebSocketU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.ReaderBase::.ctor(WebSocket4Net.WebSocket)
extern TypeInfo* ArraySegmentList_t1783467835_il2cpp_TypeInfo_var;
extern const uint32_t ReaderBase__ctor_m3143587916_MetadataUsageId;
extern "C"  void ReaderBase__ctor_m3143587916 (ReaderBase_t893022310 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReaderBase__ctor_m3143587916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_0 = ___websocket;
		ReaderBase_set_WebSocket_m2856695396(__this, L_0, /*hidden argument*/NULL);
		ArraySegmentList_t1783467835 * L_1 = (ArraySegmentList_t1783467835 *)il2cpp_codegen_object_new(ArraySegmentList_t1783467835_il2cpp_TypeInfo_var);
		ArraySegmentList__ctor_m3332344921(L_1, /*hidden argument*/NULL);
		__this->set_m_BufferSegments_0(L_1);
		return;
	}
}
// SuperSocket.ClientEngine.Protocol.ArraySegmentList WebSocket4Net.Protocol.ReaderBase::get_BufferSegments()
extern "C"  ArraySegmentList_t1783467835 * ReaderBase_get_BufferSegments_m2174645212 (ReaderBase_t893022310 * __this, const MethodInfo* method)
{
	ArraySegmentList_t1783467835 * V_0 = NULL;
	{
		ArraySegmentList_t1783467835 * L_0 = __this->get_m_BufferSegments_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		ArraySegmentList_t1783467835 * L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.ReaderBase::.ctor(WebSocket4Net.Protocol.ReaderBase)
extern "C"  void ReaderBase__ctor_m849562793 (ReaderBase_t893022310 * __this, ReaderBase_t893022310 * ___previousCommandReader, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		ReaderBase_t893022310 * L_0 = ___previousCommandReader;
		NullCheck(L_0);
		ArraySegmentList_t1783467835 * L_1 = ReaderBase_get_BufferSegments_m2174645212(L_0, /*hidden argument*/NULL);
		__this->set_m_BufferSegments_0(L_1);
		return;
	}
}
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo> WebSocket4Net.Protocol.ReaderBase::get_NextCommandReader()
extern "C"  Il2CppObject* ReaderBase_get_NextCommandReader_m3473317212 (ReaderBase_t893022310 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = __this->get_U3CNextCommandReaderU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Il2CppObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.ReaderBase::set_NextCommandReader(SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>)
extern "C"  void ReaderBase_set_NextCommandReader_m1719152039 (ReaderBase_t893022310 * __this, Il2CppObject* ___value, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___value;
		__this->set_U3CNextCommandReaderU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.ReaderBase::AddArraySegment(System.Byte[],System.Int32,System.Int32)
extern const MethodInfo* ArraySegmentList_1_AddSegment_m727323212_MethodInfo_var;
extern const uint32_t ReaderBase_AddArraySegment_m3437881651_MetadataUsageId;
extern "C"  void ReaderBase_AddArraySegment_m3437881651 (ReaderBase_t893022310 * __this, ByteU5BU5D_t58506160* ___buffer, int32_t ___offset, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReaderBase_AddArraySegment_m3437881651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArraySegmentList_t1783467835 * L_0 = ReaderBase_get_BufferSegments_m2174645212(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_1 = ___buffer;
		int32_t L_2 = ___offset;
		int32_t L_3 = ___length;
		NullCheck(L_0);
		ArraySegmentList_1_AddSegment_m727323212(L_0, L_1, L_2, L_3, (bool)1, /*hidden argument*/ArraySegmentList_1_AddSegment_m727323212_MethodInfo_var);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.Rfc6455Processor::.ctor()
extern TypeInfo* CloseStatusCodeRfc6455_t1268471848_il2cpp_TypeInfo_var;
extern TypeInfo* DraftHybi10Processor_t2968147820_il2cpp_TypeInfo_var;
extern const uint32_t Rfc6455Processor__ctor_m2534995212_MetadataUsageId;
extern "C"  void Rfc6455Processor__ctor_m2534995212 (Rfc6455Processor_t2702576183 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rfc6455Processor__ctor_m2534995212_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CloseStatusCodeRfc6455_t1268471848 * L_0 = (CloseStatusCodeRfc6455_t1268471848 *)il2cpp_codegen_object_new(CloseStatusCodeRfc6455_t1268471848_il2cpp_TypeInfo_var);
		CloseStatusCodeRfc6455__ctor_m917947323(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DraftHybi10Processor_t2968147820_il2cpp_TypeInfo_var);
		DraftHybi10Processor__ctor_m3986497774(__this, ((int32_t)13), L_0, /*hidden argument*/NULL);
		return;
	}
}
// SuperSocket.ClientEngine.Protocol.ArraySegmentList WebSocket4Net.Protocol.WebSocketDataFrame::get_InnerData()
extern "C"  ArraySegmentList_t1783467835 * WebSocketDataFrame_get_InnerData_m815354334 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method)
{
	ArraySegmentList_t1783467835 * V_0 = NULL;
	{
		ArraySegmentList_t1783467835 * L_0 = __this->get_m_InnerData_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		ArraySegmentList_t1783467835 * L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.WebSocketDataFrame::.ctor(SuperSocket.ClientEngine.Protocol.ArraySegmentList)
extern const MethodInfo* ArraySegmentList_1_ClearSegements_m3355833128_MethodInfo_var;
extern const uint32_t WebSocketDataFrame__ctor_m2101584072_MetadataUsageId;
extern "C"  void WebSocketDataFrame__ctor_m2101584072 (WebSocketDataFrame_t222733806 * __this, ArraySegmentList_t1783467835 * ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketDataFrame__ctor_m2101584072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_ActualPayloadLength_1((((int64_t)((int64_t)(-1)))));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		ArraySegmentList_t1783467835 * L_0 = ___data;
		__this->set_m_InnerData_0(L_0);
		ArraySegmentList_t1783467835 * L_1 = __this->get_m_InnerData_0();
		NullCheck(L_1);
		ArraySegmentList_1_ClearSegements_m3355833128(L_1, /*hidden argument*/ArraySegmentList_1_ClearSegements_m3355833128_MethodInfo_var);
		return;
	}
}
// System.Boolean WebSocket4Net.Protocol.WebSocketDataFrame::get_FIN()
extern "C"  bool WebSocketDataFrame_get_FIN_m262651117 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ArraySegmentList_t1783467835 * L_0 = __this->get_m_InnerData_0();
		NullCheck(L_0);
		uint8_t L_1 = VirtFuncInvoker1< uint8_t, int32_t >::Invoke(7 /* !0 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Item(System.Int32) */, L_0, 0);
		V_0 = (bool)((((int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)128)))) == ((int32_t)((int32_t)128)))? 1 : 0);
		goto IL_001d;
	}

IL_001d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.SByte WebSocket4Net.Protocol.WebSocketDataFrame::get_OpCode()
extern "C"  int8_t WebSocketDataFrame_get_OpCode_m3260433441 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method)
{
	int8_t V_0 = 0x0;
	{
		ArraySegmentList_t1783467835 * L_0 = __this->get_m_InnerData_0();
		NullCheck(L_0);
		uint8_t L_1 = VirtFuncInvoker1< uint8_t, int32_t >::Invoke(7 /* !0 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Item(System.Int32) */, L_0, 0);
		V_0 = (((int8_t)((int8_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)15))))));
		goto IL_0014;
	}

IL_0014:
	{
		int8_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean WebSocket4Net.Protocol.WebSocketDataFrame::get_HasMask()
extern "C"  bool WebSocketDataFrame_get_HasMask_m399471176 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ArraySegmentList_t1783467835 * L_0 = __this->get_m_InnerData_0();
		NullCheck(L_0);
		uint8_t L_1 = VirtFuncInvoker1< uint8_t, int32_t >::Invoke(7 /* !0 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Item(System.Int32) */, L_0, 1);
		V_0 = (bool)((((int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)128)))) == ((int32_t)((int32_t)128)))? 1 : 0);
		goto IL_001d;
	}

IL_001d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.SByte WebSocket4Net.Protocol.WebSocketDataFrame::get_PayloadLenght()
extern "C"  int8_t WebSocketDataFrame_get_PayloadLenght_m3291416379 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method)
{
	int8_t V_0 = 0x0;
	{
		ArraySegmentList_t1783467835 * L_0 = __this->get_m_InnerData_0();
		NullCheck(L_0);
		uint8_t L_1 = VirtFuncInvoker1< uint8_t, int32_t >::Invoke(7 /* !0 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Item(System.Int32) */, L_0, 1);
		V_0 = (((int8_t)((int8_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)127))))));
		goto IL_0014;
	}

IL_0014:
	{
		int8_t L_2 = V_0;
		return L_2;
	}
}
// System.Int64 WebSocket4Net.Protocol.WebSocketDataFrame::get_ActualPayloadLength()
extern "C"  int64_t WebSocketDataFrame_get_ActualPayloadLength_m2823861539 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method)
{
	int8_t V_0 = 0x0;
	int64_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int64_t V_4 = 0;
	bool V_5 = false;
	{
		int64_t L_0 = __this->get_m_ActualPayloadLength_1();
		V_5 = (bool)((((int64_t)L_0) < ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0);
		bool L_1 = V_5;
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		int64_t L_2 = __this->get_m_ActualPayloadLength_1();
		V_4 = L_2;
		goto IL_00c0;
	}

IL_001e:
	{
		int8_t L_3 = WebSocketDataFrame_get_PayloadLenght_m3291416379(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int8_t L_4 = V_0;
		V_5 = (bool)((((int32_t)((((int32_t)L_4) < ((int32_t)((int32_t)126)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_5 = V_5;
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		int8_t L_6 = V_0;
		__this->set_m_ActualPayloadLength_1((((int64_t)((int64_t)L_6))));
		goto IL_00b6;
	}

IL_003d:
	{
		int8_t L_7 = V_0;
		V_5 = (bool)((((int32_t)((((int32_t)L_7) == ((int32_t)((int32_t)126)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_8 = V_5;
		if (L_8)
		{
			goto IL_0075;
		}
	}
	{
		ArraySegmentList_t1783467835 * L_9 = __this->get_m_InnerData_0();
		NullCheck(L_9);
		uint8_t L_10 = VirtFuncInvoker1< uint8_t, int32_t >::Invoke(7 /* !0 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Item(System.Int32) */, L_9, 2);
		ArraySegmentList_t1783467835 * L_11 = __this->get_m_InnerData_0();
		NullCheck(L_11);
		uint8_t L_12 = VirtFuncInvoker1< uint8_t, int32_t >::Invoke(7 /* !0 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Item(System.Int32) */, L_11, 3);
		__this->set_m_ActualPayloadLength_1((((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10*(int32_t)((int32_t)256)))+(int32_t)L_12))))));
		goto IL_00b6;
	}

IL_0075:
	{
		V_1 = (((int64_t)((int64_t)0)));
		V_2 = 1;
		V_3 = 7;
		goto IL_00a1;
	}

IL_007f:
	{
		int64_t L_13 = V_1;
		ArraySegmentList_t1783467835 * L_14 = __this->get_m_InnerData_0();
		int32_t L_15 = V_3;
		NullCheck(L_14);
		uint8_t L_16 = VirtFuncInvoker1< uint8_t, int32_t >::Invoke(7 /* !0 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Item(System.Int32) */, L_14, ((int32_t)((int32_t)L_15+(int32_t)2)));
		int32_t L_17 = V_2;
		V_1 = ((int64_t)((int64_t)L_13+(int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)L_16*(int32_t)L_17)))))));
		int32_t L_18 = V_2;
		V_2 = ((int32_t)((int32_t)L_18*(int32_t)((int32_t)256)));
		int32_t L_19 = V_3;
		V_3 = ((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_00a1:
	{
		int32_t L_20 = V_3;
		V_5 = (bool)((((int32_t)((((int32_t)L_20) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_21 = V_5;
		if (L_21)
		{
			goto IL_007f;
		}
	}
	{
		int64_t L_22 = V_1;
		__this->set_m_ActualPayloadLength_1(L_22);
	}

IL_00b6:
	{
		int64_t L_23 = __this->get_m_ActualPayloadLength_1();
		V_4 = L_23;
		goto IL_00c0;
	}

IL_00c0:
	{
		int64_t L_24 = V_4;
		return L_24;
	}
}
// System.Byte[] WebSocket4Net.Protocol.WebSocketDataFrame::get_MaskKey()
extern "C"  ByteU5BU5D_t58506160* WebSocketDataFrame_get_MaskKey_m2158284327 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method)
{
	ByteU5BU5D_t58506160* V_0 = NULL;
	{
		ByteU5BU5D_t58506160* L_0 = __this->get_U3CMaskKeyU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		ByteU5BU5D_t58506160* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.Protocol.WebSocketDataFrame::set_MaskKey(System.Byte[])
extern "C"  void WebSocketDataFrame_set_MaskKey_m982527904 (WebSocketDataFrame_t222733806 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = ___value;
		__this->set_U3CMaskKeyU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.WebSocketDataFrame::set_ExtensionData(System.Byte[])
extern "C"  void WebSocketDataFrame_set_ExtensionData_m3707031530 (WebSocketDataFrame_t222733806 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = ___value;
		__this->set_U3CExtensionDataU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void WebSocket4Net.Protocol.WebSocketDataFrame::set_ApplicationData(System.Byte[])
extern "C"  void WebSocketDataFrame_set_ApplicationData_m1919509625 (WebSocketDataFrame_t222733806 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = ___value;
		__this->set_U3CApplicationDataU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int32 WebSocket4Net.Protocol.WebSocketDataFrame::get_Length()
extern "C"  int32_t WebSocketDataFrame_get_Length_m3046518540 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ArraySegmentList_t1783467835 * L_0 = __this->get_m_InnerData_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_0);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void WebSocket4Net.Protocol.WebSocketDataFrame::Clear()
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegmentList_1_ClearSegements_m3355833128_MethodInfo_var;
extern const uint32_t WebSocketDataFrame_Clear_m2483472352_MetadataUsageId;
extern "C"  void WebSocketDataFrame_Clear_m2483472352 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketDataFrame_Clear_m2483472352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArraySegmentList_t1783467835 * L_0 = __this->get_m_InnerData_0();
		NullCheck(L_0);
		ArraySegmentList_1_ClearSegements_m3355833128(L_0, /*hidden argument*/ArraySegmentList_1_ClearSegements_m3355833128_MethodInfo_var);
		WebSocketDataFrame_set_ExtensionData_m3707031530(__this, ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		WebSocketDataFrame_set_ApplicationData_m1919509625(__this, ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		__this->set_m_ActualPayloadLength_1((((int64_t)((int64_t)(-1)))));
		return;
	}
}
// SuperSocket.ClientEngine.TcpClientSession WebSocket4Net.WebSocket::get_Client()
extern "C"  TcpClientSession_t1301539049 * WebSocket_get_Client_m2705060304 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	TcpClientSession_t1301539049 * V_0 = NULL;
	{
		TcpClientSession_t1301539049 * L_0 = __this->get_U3CClientU3Ek__BackingField_17();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		TcpClientSession_t1301539049 * L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_Client(SuperSocket.ClientEngine.TcpClientSession)
extern "C"  void WebSocket_set_Client_m1471644827 (WebSocket_t713846903 * __this, TcpClientSession_t1301539049 * ___value, const MethodInfo* method)
{
	{
		TcpClientSession_t1301539049 * L_0 = ___value;
		__this->set_U3CClientU3Ek__BackingField_17(L_0);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::set_Version(WebSocket4Net.WebSocketVersion)
extern "C"  void WebSocket_set_Version_m3858432938 (WebSocket_t713846903 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CVersionU3Ek__BackingField_18(L_0);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::set_LastActiveTime(System.DateTime)
extern "C"  void WebSocket_set_LastActiveTime_m147571246 (WebSocket_t713846903 * __this, DateTime_t339033936  ___value, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = ___value;
		__this->set_U3CLastActiveTimeU3Ek__BackingField_19(L_0);
		return;
	}
}
// System.Boolean WebSocket4Net.WebSocket::get_EnableAutoSendPing()
extern "C"  bool WebSocket_get_EnableAutoSendPing_m3480068097 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_U3CEnableAutoSendPingU3Ek__BackingField_20();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_EnableAutoSendPing(System.Boolean)
extern "C"  void WebSocket_set_EnableAutoSendPing_m1331589028 (WebSocket_t713846903 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->set_U3CEnableAutoSendPingU3Ek__BackingField_20(L_0);
		return;
	}
}
// System.Int32 WebSocket4Net.WebSocket::get_AutoSendPingInterval()
extern "C"  int32_t WebSocket_get_AutoSendPingInterval_m458397481 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CAutoSendPingIntervalU3Ek__BackingField_21();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_AutoSendPingInterval(System.Int32)
extern "C"  void WebSocket_set_AutoSendPingInterval_m3191023104 (WebSocket_t713846903 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CAutoSendPingIntervalU3Ek__BackingField_21(L_0);
		return;
	}
}
// WebSocket4Net.Protocol.IProtocolProcessor WebSocket4Net.WebSocket::get_ProtocolProcessor()
extern "C"  Il2CppObject * WebSocket_get_ProtocolProcessor_m1265601209 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U3CProtocolProcessorU3Ek__BackingField_22();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_ProtocolProcessor(WebSocket4Net.Protocol.IProtocolProcessor)
extern "C"  void WebSocket_set_ProtocolProcessor_m2566430094 (WebSocket_t713846903 * __this, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value;
		__this->set_U3CProtocolProcessorU3Ek__BackingField_22(L_0);
		return;
	}
}
// System.Uri WebSocket4Net.WebSocket::get_TargetUri()
extern "C"  Uri_t2776692961 * WebSocket_get_TargetUri_m4211545892 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	Uri_t2776692961 * V_0 = NULL;
	{
		Uri_t2776692961 * L_0 = __this->get_U3CTargetUriU3Ek__BackingField_23();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Uri_t2776692961 * L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_TargetUri(System.Uri)
extern "C"  void WebSocket_set_TargetUri_m3334962991 (WebSocket_t713846903 * __this, Uri_t2776692961 * ___value, const MethodInfo* method)
{
	{
		Uri_t2776692961 * L_0 = ___value;
		__this->set_U3CTargetUriU3Ek__BackingField_23(L_0);
		return;
	}
}
// System.String WebSocket4Net.WebSocket::get_SubProtocol()
extern "C"  String_t* WebSocket_get_SubProtocol_m1721628912 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CSubProtocolU3Ek__BackingField_24();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_SubProtocol(System.String)
extern "C"  void WebSocket_set_SubProtocol_m2456245641 (WebSocket_t713846903 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CSubProtocolU3Ek__BackingField_24(L_0);
		return;
	}
}
// System.Collections.Generic.IDictionary`2<System.String,System.Object> WebSocket4Net.WebSocket::get_Items()
extern "C"  Il2CppObject* WebSocket_get_Items_m1025846418 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = __this->get_U3CItemsU3Ek__BackingField_25();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Il2CppObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_Items(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C"  void WebSocket_set_Items_m3678737243 (WebSocket_t713846903 * __this, Il2CppObject* ___value, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___value;
		__this->set_U3CItemsU3Ek__BackingField_25(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> WebSocket4Net.WebSocket::get_Cookies()
extern "C"  List_1_t2891677073 * WebSocket_get_Cookies_m794256048 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	List_1_t2891677073 * V_0 = NULL;
	{
		List_1_t2891677073 * L_0 = __this->get_U3CCookiesU3Ek__BackingField_26();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		List_1_t2891677073 * L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_Cookies(System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern "C"  void WebSocket_set_Cookies_m3968108603 (WebSocket_t713846903 * __this, List_1_t2891677073 * ___value, const MethodInfo* method)
{
	{
		List_1_t2891677073 * L_0 = ___value;
		__this->set_U3CCookiesU3Ek__BackingField_26(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> WebSocket4Net.WebSocket::get_CustomHeaderItems()
extern "C"  List_1_t2891677073 * WebSocket_get_CustomHeaderItems_m1966151779 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	List_1_t2891677073 * V_0 = NULL;
	{
		List_1_t2891677073 * L_0 = __this->get_U3CCustomHeaderItemsU3Ek__BackingField_27();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		List_1_t2891677073 * L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_CustomHeaderItems(System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern "C"  void WebSocket_set_CustomHeaderItems_m3818569704 (WebSocket_t713846903 * __this, List_1_t2891677073 * ___value, const MethodInfo* method)
{
	{
		List_1_t2891677073 * L_0 = ___value;
		__this->set_U3CCustomHeaderItemsU3Ek__BackingField_27(L_0);
		return;
	}
}
// WebSocket4Net.WebSocketState WebSocket4Net.WebSocket::get_State()
extern "C"  int32_t WebSocket_get_State_m3481149631 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CStateU3Ek__BackingField_28();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_State(WebSocket4Net.WebSocketState)
extern "C"  void WebSocket_set_State_m2318924618 (WebSocket_t713846903 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CStateU3Ek__BackingField_28(L_0);
		return;
	}
}
// System.Boolean WebSocket4Net.WebSocket::get_Handshaked()
extern "C"  bool WebSocket_get_Handshaked_m3019381250 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_U3CHandshakedU3Ek__BackingField_29();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_Handshaked(System.Boolean)
extern "C"  void WebSocket_set_Handshaked_m224100325 (WebSocket_t713846903 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->set_U3CHandshakedU3Ek__BackingField_29(L_0);
		return;
	}
}
// SuperSocket.ClientEngine.IProxyConnector WebSocket4Net.WebSocket::get_Proxy()
extern "C"  Il2CppObject * WebSocket_get_Proxy_m857243229 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U3CProxyU3Ek__BackingField_30();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo> WebSocket4Net.WebSocket::get_CommandReader()
extern "C"  Il2CppObject* WebSocket_get_CommandReader_m2191282226 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = __this->get_U3CCommandReaderU3Ek__BackingField_31();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Il2CppObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_CommandReader(SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>)
extern "C"  void WebSocket_set_CommandReader_m748575549 (WebSocket_t713846903 * __this, Il2CppObject* ___value, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___value;
		__this->set_U3CCommandReaderU3Ek__BackingField_31(L_0);
		return;
	}
}
// System.Boolean WebSocket4Net.WebSocket::get_NotSpecifiedVersion()
extern "C"  bool WebSocket_get_NotSpecifiedVersion_m3038260680 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_U3CNotSpecifiedVersionU3Ek__BackingField_32();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_NotSpecifiedVersion(System.Boolean)
extern "C"  void WebSocket_set_NotSpecifiedVersion_m795243219 (WebSocket_t713846903 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->set_U3CNotSpecifiedVersionU3Ek__BackingField_32(L_0);
		return;
	}
}
// System.String WebSocket4Net.WebSocket::get_LastPongResponse()
extern "C"  String_t* WebSocket_get_LastPongResponse_m1283402937 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CLastPongResponseU3Ek__BackingField_33();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_LastPongResponse(System.String)
extern "C"  void WebSocket_set_LastPongResponse_m668946386 (WebSocket_t713846903 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CLastPongResponseU3Ek__BackingField_33(L_0);
		return;
	}
}
// System.String WebSocket4Net.WebSocket::get_HandshakeHost()
extern "C"  String_t* WebSocket_get_HandshakeHost_m3944612535 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CHandshakeHostU3Ek__BackingField_34();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_HandshakeHost(System.String)
extern "C"  void WebSocket_set_HandshakeHost_m3139511650 (WebSocket_t713846903 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CHandshakeHostU3Ek__BackingField_34(L_0);
		return;
	}
}
// System.String WebSocket4Net.WebSocket::get_Origin()
extern "C"  String_t* WebSocket_get_Origin_m775398224 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3COriginU3Ek__BackingField_35();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocket::set_Origin(System.String)
extern "C"  void WebSocket_set_Origin_m1035068891 (WebSocket_t713846903 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3COriginU3Ek__BackingField_35(L_0);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::.cctor()
extern TypeInfo* IProtocolProcessorU5BU5D_t2571124850_il2cpp_TypeInfo_var;
extern TypeInfo* Rfc6455Processor_t2702576183_il2cpp_TypeInfo_var;
extern TypeInfo* DraftHybi10Processor_t2968147820_il2cpp_TypeInfo_var;
extern TypeInfo* DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var;
extern TypeInfo* ProtocolProcessorFactory_t3499308290_il2cpp_TypeInfo_var;
extern TypeInfo* WebSocket_t713846903_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket__cctor_m2314250317_MetadataUsageId;
extern "C"  void WebSocket__cctor_m2314250317 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket__cctor_m2314250317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IProtocolProcessorU5BU5D_t2571124850* V_0 = NULL;
	{
		V_0 = ((IProtocolProcessorU5BU5D_t2571124850*)SZArrayNew(IProtocolProcessorU5BU5D_t2571124850_il2cpp_TypeInfo_var, (uint32_t)3));
		IProtocolProcessorU5BU5D_t2571124850* L_0 = V_0;
		Rfc6455Processor_t2702576183 * L_1 = (Rfc6455Processor_t2702576183 *)il2cpp_codegen_object_new(Rfc6455Processor_t2702576183_il2cpp_TypeInfo_var);
		Rfc6455Processor__ctor_m2534995212(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		IProtocolProcessorU5BU5D_t2571124850* L_2 = V_0;
		DraftHybi10Processor_t2968147820 * L_3 = (DraftHybi10Processor_t2968147820 *)il2cpp_codegen_object_new(DraftHybi10Processor_t2968147820_il2cpp_TypeInfo_var);
		DraftHybi10Processor__ctor_m2859170935(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_3);
		IProtocolProcessorU5BU5D_t2571124850* L_4 = V_0;
		DraftHybi00Processor_t470131883 * L_5 = (DraftHybi00Processor_t470131883 *)il2cpp_codegen_object_new(DraftHybi00Processor_t470131883_il2cpp_TypeInfo_var);
		DraftHybi00Processor__ctor_m3267995160(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_5);
		IProtocolProcessorU5BU5D_t2571124850* L_6 = V_0;
		ProtocolProcessorFactory_t3499308290 * L_7 = (ProtocolProcessorFactory_t3499308290 *)il2cpp_codegen_object_new(ProtocolProcessorFactory_t3499308290_il2cpp_TypeInfo_var);
		ProtocolProcessorFactory__ctor_m557102662(L_7, L_6, /*hidden argument*/NULL);
		((WebSocket_t713846903_StaticFields*)WebSocket_t713846903_il2cpp_TypeInfo_var->static_fields)->set_m_ProtocolProcessorFactory_8(L_7);
		return;
	}
}
// System.Net.EndPoint WebSocket4Net.WebSocket::ResolveUri(System.String,System.Int32,System.Int32&)
extern TypeInfo* Uri_t2776692961_il2cpp_TypeInfo_var;
extern TypeInfo* IPAddress_t3220500535_il2cpp_TypeInfo_var;
extern TypeInfo* IPEndPoint_t1265996582_il2cpp_TypeInfo_var;
extern TypeInfo* DnsEndPoint_t1814926600_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_ResolveUri_m2866265440_MetadataUsageId;
extern "C"  EndPoint_t1294049535 * WebSocket_ResolveUri_m2866265440 (WebSocket_t713846903 * __this, String_t* ___uri, int32_t ___defaultPort, int32_t* ___port, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_ResolveUri_m2866265440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IPAddress_t3220500535 * V_0 = NULL;
	EndPoint_t1294049535 * V_1 = NULL;
	EndPoint_t1294049535 * V_2 = NULL;
	bool V_3 = false;
	{
		String_t* L_0 = ___uri;
		Uri_t2776692961 * L_1 = (Uri_t2776692961 *)il2cpp_codegen_object_new(Uri_t2776692961_il2cpp_TypeInfo_var);
		Uri__ctor_m1721267859(L_1, L_0, /*hidden argument*/NULL);
		WebSocket_set_TargetUri_m3334962991(__this, L_1, /*hidden argument*/NULL);
		int32_t* L_2 = ___port;
		Uri_t2776692961 * L_3 = WebSocket_get_TargetUri_m4211545892(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = Uri_get_Port_m2253782543(L_3, /*hidden argument*/NULL);
		*((int32_t*)(L_2)) = (int32_t)L_4;
		int32_t* L_5 = ___port;
		V_3 = (bool)((((int32_t)(*((int32_t*)L_5))) > ((int32_t)0))? 1 : 0);
		bool L_6 = V_3;
		if (L_6)
		{
			goto IL_0027;
		}
	}
	{
		int32_t* L_7 = ___port;
		int32_t L_8 = ___defaultPort;
		*((int32_t*)(L_7)) = (int32_t)L_8;
	}

IL_0027:
	{
		Uri_t2776692961 * L_9 = WebSocket_get_TargetUri_m4211545892(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = Uri_get_Host_m1446697833(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IPAddress_t3220500535_il2cpp_TypeInfo_var);
		bool L_11 = IPAddress_TryParse_m1385842418(NULL /*static, unused*/, L_10, (&V_0), /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_11) == ((int32_t)0))? 1 : 0);
		bool L_12 = V_3;
		if (L_12)
		{
			goto IL_004b;
		}
	}
	{
		IPAddress_t3220500535 * L_13 = V_0;
		int32_t* L_14 = ___port;
		IPEndPoint_t1265996582 * L_15 = (IPEndPoint_t1265996582 *)il2cpp_codegen_object_new(IPEndPoint_t1265996582_il2cpp_TypeInfo_var);
		IPEndPoint__ctor_m613724246(L_15, L_13, (*((int32_t*)L_14)), /*hidden argument*/NULL);
		V_1 = L_15;
		goto IL_005e;
	}

IL_004b:
	{
		Uri_t2776692961 * L_16 = WebSocket_get_TargetUri_m4211545892(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		String_t* L_17 = Uri_get_Host_m1446697833(L_16, /*hidden argument*/NULL);
		int32_t* L_18 = ___port;
		DnsEndPoint_t1814926600 * L_19 = (DnsEndPoint_t1814926600 *)il2cpp_codegen_object_new(DnsEndPoint_t1814926600_il2cpp_TypeInfo_var);
		DnsEndPoint__ctor_m1299091296(L_19, L_17, (*((int32_t*)L_18)), /*hidden argument*/NULL);
		V_1 = L_19;
	}

IL_005e:
	{
		EndPoint_t1294049535 * L_20 = V_1;
		V_2 = L_20;
		goto IL_0062;
	}

IL_0062:
	{
		EndPoint_t1294049535 * L_21 = V_2;
		return L_21;
	}
}
// SuperSocket.ClientEngine.TcpClientSession WebSocket4Net.WebSocket::CreateClient(System.String)
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* AsyncTcpSession_t2860571088_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral58;
extern const uint32_t WebSocket_CreateClient_m3769000575_MetadataUsageId;
extern "C"  TcpClientSession_t1301539049 * WebSocket_CreateClient_m3769000575 (WebSocket_t713846903 * __this, String_t* ___uri, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_CreateClient_m3769000575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	EndPoint_t1294049535 * V_1 = NULL;
	TcpClientSession_t1301539049 * V_2 = NULL;
	bool V_3 = false;
	{
		String_t* L_0 = ___uri;
		EndPoint_t1294049535 * L_1 = WebSocket_ResolveUri_m2866265440(__this, L_0, ((int32_t)80), (&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		V_3 = (bool)((((int32_t)((((int32_t)L_2) == ((int32_t)((int32_t)80)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_3;
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Uri_t2776692961 * L_4 = WebSocket_get_TargetUri_m4211545892(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = Uri_get_Host_m1446697833(L_4, /*hidden argument*/NULL);
		WebSocket_set_HandshakeHost_m3139511650(__this, L_5, /*hidden argument*/NULL);
		goto IL_004f;
	}

IL_002d:
	{
		Uri_t2776692961 * L_6 = WebSocket_get_TargetUri_m4211545892(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = Uri_get_Host_m1446697833(L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m2809334143(NULL /*static, unused*/, L_7, _stringLiteral58, L_10, /*hidden argument*/NULL);
		WebSocket_set_HandshakeHost_m3139511650(__this, L_11, /*hidden argument*/NULL);
	}

IL_004f:
	{
		EndPoint_t1294049535 * L_12 = V_1;
		AsyncTcpSession_t2860571088 * L_13 = (AsyncTcpSession_t2860571088 *)il2cpp_codegen_object_new(AsyncTcpSession_t2860571088_il2cpp_TypeInfo_var);
		AsyncTcpSession__ctor_m2893573072(L_13, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		goto IL_0058;
	}

IL_0058:
	{
		TcpClientSession_t1301539049 * L_14 = V_2;
		return L_14;
	}
}
// SuperSocket.ClientEngine.TcpClientSession WebSocket4Net.WebSocket::CreateSecureClient(System.String)
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* SslStreamTcpSession_t2680237056_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3516557091;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral713168803;
extern Il2CppCodeGenString* _stringLiteral116076;
extern const uint32_t WebSocket_CreateSecureClient_m3104541832_MetadataUsageId;
extern "C"  TcpClientSession_t1301539049 * WebSocket_CreateSecureClient_m3104541832 (WebSocket_t713846903 * __this, String_t* ___uri, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_CreateSecureClient_m3104541832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	EndPoint_t1294049535 * V_3 = NULL;
	TcpClientSession_t1301539049 * V_4 = NULL;
	bool V_5 = false;
	ObjectU5BU5D_t11523773* V_6 = NULL;
	{
		String_t* L_0 = ___uri;
		NullCheck(_stringLiteral3516557091);
		int32_t L_1 = String_get_Length_m2979997331(_stringLiteral3516557091, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_2 = String_IndexOf_m204546721(L_0, ((int32_t)47), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		V_5 = (bool)((((int32_t)((((int32_t)L_3) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_5;
		if (L_4)
		{
			goto IL_009e;
		}
	}
	{
		String_t* L_5 = ___uri;
		NullCheck(_stringLiteral3516557091);
		int32_t L_6 = String_get_Length_m2979997331(_stringLiteral3516557091, /*hidden argument*/NULL);
		String_t* L_7 = ___uri;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m2979997331(L_7, /*hidden argument*/NULL);
		NullCheck(_stringLiteral3516557091);
		int32_t L_9 = String_get_Length_m2979997331(_stringLiteral3516557091, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_10 = String_IndexOf_m2077558742(L_5, ((int32_t)58), L_6, ((int32_t)((int32_t)L_8-(int32_t)L_9)), /*hidden argument*/NULL);
		V_0 = L_10;
		int32_t L_11 = V_0;
		V_5 = (bool)((((int32_t)((((int32_t)L_11) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_12 = V_5;
		if (L_12)
		{
			goto IL_008b;
		}
	}
	{
		V_6 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		ObjectU5BU5D_t11523773* L_13 = V_6;
		String_t* L_14 = ___uri;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = V_6;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
		ArrayElementTypeCheck (L_15, _stringLiteral58);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral58);
		ObjectU5BU5D_t11523773* L_16 = V_6;
		int32_t L_17 = ((int32_t)443);
		Il2CppObject * L_18 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_18);
		ObjectU5BU5D_t11523773* L_19 = V_6;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		ArrayElementTypeCheck (L_19, _stringLiteral47);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral47);
		ObjectU5BU5D_t11523773* L_20 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3016520001(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		___uri = L_21;
		goto IL_0098;
	}

IL_008b:
	{
		String_t* L_22 = ___uri;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m138640077(NULL /*static, unused*/, L_22, _stringLiteral47, /*hidden argument*/NULL);
		___uri = L_23;
	}

IL_0098:
	{
		goto IL_0134;
	}

IL_009e:
	{
		int32_t L_24 = V_0;
		NullCheck(_stringLiteral3516557091);
		int32_t L_25 = String_get_Length_m2979997331(_stringLiteral3516557091, /*hidden argument*/NULL);
		V_5 = (bool)((((int32_t)((((int32_t)L_24) == ((int32_t)L_25))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_26 = V_5;
		if (L_26)
		{
			goto IL_00c5;
		}
	}
	{
		ArgumentException_t124305799 * L_27 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_27, _stringLiteral713168803, _stringLiteral116076, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
	}

IL_00c5:
	{
		String_t* L_28 = ___uri;
		NullCheck(_stringLiteral3516557091);
		int32_t L_29 = String_get_Length_m2979997331(_stringLiteral3516557091, /*hidden argument*/NULL);
		int32_t L_30 = V_0;
		NullCheck(_stringLiteral3516557091);
		int32_t L_31 = String_get_Length_m2979997331(_stringLiteral3516557091, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_32 = String_IndexOf_m2077558742(L_28, ((int32_t)58), L_29, ((int32_t)((int32_t)L_30-(int32_t)L_31)), /*hidden argument*/NULL);
		V_1 = L_32;
		int32_t L_33 = V_1;
		V_5 = (bool)((((int32_t)((((int32_t)L_33) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_34 = V_5;
		if (L_34)
		{
			goto IL_0133;
		}
	}
	{
		V_6 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		ObjectU5BU5D_t11523773* L_35 = V_6;
		String_t* L_36 = ___uri;
		int32_t L_37 = V_0;
		NullCheck(L_36);
		String_t* L_38 = String_Substring_m675079568(L_36, 0, L_37, /*hidden argument*/NULL);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 0);
		ArrayElementTypeCheck (L_35, L_38);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_38);
		ObjectU5BU5D_t11523773* L_39 = V_6;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 1);
		ArrayElementTypeCheck (L_39, _stringLiteral58);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral58);
		ObjectU5BU5D_t11523773* L_40 = V_6;
		int32_t L_41 = ((int32_t)443);
		Il2CppObject * L_42 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 2);
		ArrayElementTypeCheck (L_40, L_42);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_42);
		ObjectU5BU5D_t11523773* L_43 = V_6;
		String_t* L_44 = ___uri;
		int32_t L_45 = V_0;
		NullCheck(L_44);
		String_t* L_46 = String_Substring_m2809233063(L_44, L_45, /*hidden argument*/NULL);
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 3);
		ArrayElementTypeCheck (L_43, L_46);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_46);
		ObjectU5BU5D_t11523773* L_47 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m3016520001(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		___uri = L_48;
	}

IL_0133:
	{
	}

IL_0134:
	{
		String_t* L_49 = ___uri;
		EndPoint_t1294049535 * L_50 = WebSocket_ResolveUri_m2866265440(__this, L_49, ((int32_t)443), (&V_2), /*hidden argument*/NULL);
		V_3 = L_50;
		int32_t L_51 = V_2;
		V_5 = (bool)((((int32_t)((((int32_t)L_51) == ((int32_t)((int32_t)443)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_52 = V_5;
		if (L_52)
		{
			goto IL_0168;
		}
	}
	{
		Uri_t2776692961 * L_53 = WebSocket_get_TargetUri_m4211545892(__this, /*hidden argument*/NULL);
		NullCheck(L_53);
		String_t* L_54 = Uri_get_Host_m1446697833(L_53, /*hidden argument*/NULL);
		WebSocket_set_HandshakeHost_m3139511650(__this, L_54, /*hidden argument*/NULL);
		goto IL_018a;
	}

IL_0168:
	{
		Uri_t2776692961 * L_55 = WebSocket_get_TargetUri_m4211545892(__this, /*hidden argument*/NULL);
		NullCheck(L_55);
		String_t* L_56 = Uri_get_Host_m1446697833(L_55, /*hidden argument*/NULL);
		int32_t L_57 = V_2;
		int32_t L_58 = L_57;
		Il2CppObject * L_59 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_58);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = String_Concat_m2809334143(NULL /*static, unused*/, L_56, _stringLiteral58, L_59, /*hidden argument*/NULL);
		WebSocket_set_HandshakeHost_m3139511650(__this, L_60, /*hidden argument*/NULL);
	}

IL_018a:
	{
		EndPoint_t1294049535 * L_61 = V_3;
		SslStreamTcpSession_t2680237056 * L_62 = (SslStreamTcpSession_t2680237056 *)il2cpp_codegen_object_new(SslStreamTcpSession_t2680237056_il2cpp_TypeInfo_var);
		SslStreamTcpSession__ctor_m1591305088(L_62, L_61, /*hidden argument*/NULL);
		V_4 = L_62;
		goto IL_0194;
	}

IL_0194:
	{
		TcpClientSession_t1301539049 * L_63 = V_4;
		return L_63;
	}
}
// System.Void WebSocket4Net.WebSocket::Initialize(System.String,System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.String,System.String,WebSocket4Net.WebSocketVersion)
extern TypeInfo* WebSocket_t713846903_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t2891677073_il2cpp_TypeInfo_var;
extern TypeInfo* Handshake_t1657241526_il2cpp_TypeInfo_var;
extern TypeInfo* Text_t3880164652_il2cpp_TypeInfo_var;
extern TypeInfo* Binary_t1572461568_il2cpp_TypeInfo_var;
extern TypeInfo* Close_t3942764983_il2cpp_TypeInfo_var;
extern TypeInfo* Ping_t3880049009_il2cpp_TypeInfo_var;
extern TypeInfo* Pong_t3880054775_il2cpp_TypeInfo_var;
extern TypeInfo* BadRequest_t1215901385_il2cpp_TypeInfo_var;
extern TypeInfo* StringComparer_t4058118931_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_1_t2171844441_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_1_t4058788791_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4107555106_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2__ctor_m261029313_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m197076471_MethodInfo_var;
extern const MethodInfo* WebSocket_client_Connected_m4243894931_MethodInfo_var;
extern const MethodInfo* WebSocket_client_Closed_m2085449848_MethodInfo_var;
extern const MethodInfo* WebSocket_client_Error_m829371704_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3300027855_MethodInfo_var;
extern const MethodInfo* WebSocket_client_DataReceived_m3695014531_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m575740431_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2315398618;
extern Il2CppCodeGenString* _stringLiteral113382206;
extern Il2CppCodeGenString* _stringLiteral3516557091;
extern Il2CppCodeGenString* _stringLiteral713168803;
extern Il2CppCodeGenString* _stringLiteral116076;
extern const uint32_t WebSocket_Initialize_m3081830635_MetadataUsageId;
extern "C"  void WebSocket_Initialize_m3081830635 (WebSocket_t713846903 * __this, String_t* ___uri, String_t* ___subProtocol, List_1_t2891677073 * ___cookies, List_1_t2891677073 * ___customHeaderItems, String_t* ___userAgent, String_t* ___origin, int32_t ___version, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Initialize_m3081830635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Handshake_t1657241526 * V_0 = NULL;
	Text_t3880164652 * V_1 = NULL;
	Binary_t1572461568 * V_2 = NULL;
	Close_t3942764983 * V_3 = NULL;
	Ping_t3880049009 * V_4 = NULL;
	Pong_t3880054775 * V_5 = NULL;
	BadRequest_t1215901385 * V_6 = NULL;
	TcpClientSession_t1301539049 * V_7 = NULL;
	bool V_8 = false;
	int32_t G_B9_0 = 0;
	{
		int32_t L_0 = ___version;
		V_8 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_8;
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		WebSocket_set_NotSpecifiedVersion_m795243219(__this, (bool)1, /*hidden argument*/NULL);
		___version = ((int32_t)13);
	}

IL_001d:
	{
		int32_t L_2 = ___version;
		WebSocket_set_Version_m3858432938(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___version;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t713846903_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = WebSocket_GetProtocolProcessor_m3470676079(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		WebSocket_set_ProtocolProcessor_m2566430094(__this, L_4, /*hidden argument*/NULL);
		List_1_t2891677073 * L_5 = ___cookies;
		WebSocket_set_Cookies_m3968108603(__this, L_5, /*hidden argument*/NULL);
		String_t* L_6 = ___origin;
		WebSocket_set_Origin_m1035068891(__this, L_6, /*hidden argument*/NULL);
		String_t* L_7 = ___userAgent;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_8 = L_8;
		bool L_9 = V_8;
		if (L_9)
		{
			goto IL_007d;
		}
	}
	{
		List_1_t2891677073 * L_10 = ___customHeaderItems;
		V_8 = (bool)((((int32_t)((((Il2CppObject*)(List_1_t2891677073 *)L_10) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_11 = V_8;
		if (L_11)
		{
			goto IL_0068;
		}
	}
	{
		List_1_t2891677073 * L_12 = (List_1_t2891677073 *)il2cpp_codegen_object_new(List_1_t2891677073_il2cpp_TypeInfo_var);
		List_1__ctor_m4107555106(L_12, /*hidden argument*/List_1__ctor_m4107555106_MethodInfo_var);
		___customHeaderItems = L_12;
	}

IL_0068:
	{
		List_1_t2891677073 * L_13 = ___customHeaderItems;
		String_t* L_14 = ___userAgent;
		KeyValuePair_2_t2094718104  L_15;
		memset(&L_15, 0, sizeof(L_15));
		KeyValuePair_2__ctor_m261029313(&L_15, _stringLiteral2315398618, L_14, /*hidden argument*/KeyValuePair_2__ctor_m261029313_MethodInfo_var);
		NullCheck(L_13);
		VirtActionInvoker1< KeyValuePair_2_t2094718104  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::Add(!0) */, L_13, L_15);
	}

IL_007d:
	{
		List_1_t2891677073 * L_16 = ___customHeaderItems;
		if (!L_16)
		{
			goto IL_0090;
		}
	}
	{
		List_1_t2891677073 * L_17 = ___customHeaderItems;
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Count() */, L_17);
		G_B9_0 = ((((int32_t)((((int32_t)L_18) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0091;
	}

IL_0090:
	{
		G_B9_0 = 1;
	}

IL_0091:
	{
		V_8 = (bool)G_B9_0;
		bool L_19 = V_8;
		if (L_19)
		{
			goto IL_00a0;
		}
	}
	{
		List_1_t2891677073 * L_20 = ___customHeaderItems;
		WebSocket_set_CustomHeaderItems_m3818569704(__this, L_20, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		Handshake_t1657241526 * L_21 = (Handshake_t1657241526 *)il2cpp_codegen_object_new(Handshake_t1657241526_il2cpp_TypeInfo_var);
		Handshake__ctor_m3150570739(L_21, /*hidden argument*/NULL);
		V_0 = L_21;
		Dictionary_2_t2262919787 * L_22 = __this->get_m_CommandDict_7();
		Handshake_t1657241526 * L_23 = V_0;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String WebSocket4Net.Command.WebSocketCommandBase::get_Name() */, L_23);
		Handshake_t1657241526 * L_25 = V_0;
		NullCheck(L_22);
		VirtActionInvoker2< String_t*, Il2CppObject* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::Add(!0,!1) */, L_22, L_24, L_25);
		Text_t3880164652 * L_26 = (Text_t3880164652 *)il2cpp_codegen_object_new(Text_t3880164652_il2cpp_TypeInfo_var);
		Text__ctor_m1660271375(L_26, /*hidden argument*/NULL);
		V_1 = L_26;
		Dictionary_2_t2262919787 * L_27 = __this->get_m_CommandDict_7();
		Text_t3880164652 * L_28 = V_1;
		NullCheck(L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String WebSocket4Net.Command.WebSocketCommandBase::get_Name() */, L_28);
		Text_t3880164652 * L_30 = V_1;
		NullCheck(L_27);
		VirtActionInvoker2< String_t*, Il2CppObject* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::Add(!0,!1) */, L_27, L_29, L_30);
		Binary_t1572461568 * L_31 = (Binary_t1572461568 *)il2cpp_codegen_object_new(Binary_t1572461568_il2cpp_TypeInfo_var);
		Binary__ctor_m1144881275(L_31, /*hidden argument*/NULL);
		V_2 = L_31;
		Dictionary_2_t2262919787 * L_32 = __this->get_m_CommandDict_7();
		Binary_t1572461568 * L_33 = V_2;
		NullCheck(L_33);
		String_t* L_34 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String WebSocket4Net.Command.WebSocketCommandBase::get_Name() */, L_33);
		Binary_t1572461568 * L_35 = V_2;
		NullCheck(L_32);
		VirtActionInvoker2< String_t*, Il2CppObject* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::Add(!0,!1) */, L_32, L_34, L_35);
		Close_t3942764983 * L_36 = (Close_t3942764983 *)il2cpp_codegen_object_new(Close_t3942764983_il2cpp_TypeInfo_var);
		Close__ctor_m2113310610(L_36, /*hidden argument*/NULL);
		V_3 = L_36;
		Dictionary_2_t2262919787 * L_37 = __this->get_m_CommandDict_7();
		Close_t3942764983 * L_38 = V_3;
		NullCheck(L_38);
		String_t* L_39 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String WebSocket4Net.Command.WebSocketCommandBase::get_Name() */, L_38);
		Close_t3942764983 * L_40 = V_3;
		NullCheck(L_37);
		VirtActionInvoker2< String_t*, Il2CppObject* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::Add(!0,!1) */, L_37, L_39, L_40);
		Ping_t3880049009 * L_41 = (Ping_t3880049009 *)il2cpp_codegen_object_new(Ping_t3880049009_il2cpp_TypeInfo_var);
		Ping__ctor_m2399566954(L_41, /*hidden argument*/NULL);
		V_4 = L_41;
		Dictionary_2_t2262919787 * L_42 = __this->get_m_CommandDict_7();
		Ping_t3880049009 * L_43 = V_4;
		NullCheck(L_43);
		String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String WebSocket4Net.Command.WebSocketCommandBase::get_Name() */, L_43);
		Ping_t3880049009 * L_45 = V_4;
		NullCheck(L_42);
		VirtActionInvoker2< String_t*, Il2CppObject* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::Add(!0,!1) */, L_42, L_44, L_45);
		Pong_t3880054775 * L_46 = (Pong_t3880054775 *)il2cpp_codegen_object_new(Pong_t3880054775_il2cpp_TypeInfo_var);
		Pong__ctor_m3174063268(L_46, /*hidden argument*/NULL);
		V_5 = L_46;
		Dictionary_2_t2262919787 * L_47 = __this->get_m_CommandDict_7();
		Pong_t3880054775 * L_48 = V_5;
		NullCheck(L_48);
		String_t* L_49 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String WebSocket4Net.Command.WebSocketCommandBase::get_Name() */, L_48);
		Pong_t3880054775 * L_50 = V_5;
		NullCheck(L_47);
		VirtActionInvoker2< String_t*, Il2CppObject* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::Add(!0,!1) */, L_47, L_49, L_50);
		BadRequest_t1215901385 * L_51 = (BadRequest_t1215901385 *)il2cpp_codegen_object_new(BadRequest_t1215901385_il2cpp_TypeInfo_var);
		BadRequest__ctor_m2931563346(L_51, /*hidden argument*/NULL);
		V_6 = L_51;
		Dictionary_2_t2262919787 * L_52 = __this->get_m_CommandDict_7();
		BadRequest_t1215901385 * L_53 = V_6;
		NullCheck(L_53);
		String_t* L_54 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String WebSocket4Net.Command.WebSocketCommandBase::get_Name() */, L_53);
		BadRequest_t1215901385 * L_55 = V_6;
		NullCheck(L_52);
		VirtActionInvoker2< String_t*, Il2CppObject* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::Add(!0,!1) */, L_52, L_54, L_55);
		WebSocket_set_State_m2318924618(__this, (-1), /*hidden argument*/NULL);
		String_t* L_56 = ___subProtocol;
		WebSocket_set_SubProtocol_m2456245641(__this, L_56, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t4058118931_il2cpp_TypeInfo_var);
		StringComparer_t4058118931 * L_57 = StringComparer_get_OrdinalIgnoreCase_m2513153269(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t2474804324 * L_58 = (Dictionary_2_t2474804324 *)il2cpp_codegen_object_new(Dictionary_2_t2474804324_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m197076471(L_58, L_57, /*hidden argument*/Dictionary_2__ctor_m197076471_MethodInfo_var);
		WebSocket_set_Items_m3678737243(__this, L_58, /*hidden argument*/NULL);
		String_t* L_59 = ___uri;
		NullCheck(L_59);
		bool L_60 = String_StartsWith_m495172832(L_59, _stringLiteral113382206, 5, /*hidden argument*/NULL);
		V_8 = (bool)((((int32_t)L_60) == ((int32_t)0))? 1 : 0);
		bool L_61 = V_8;
		if (L_61)
		{
			goto IL_019b;
		}
	}
	{
		String_t* L_62 = ___uri;
		TcpClientSession_t1301539049 * L_63 = WebSocket_CreateClient_m3769000575(__this, L_62, /*hidden argument*/NULL);
		V_7 = L_63;
		goto IL_01ce;
	}

IL_019b:
	{
		String_t* L_64 = ___uri;
		NullCheck(L_64);
		bool L_65 = String_StartsWith_m495172832(L_64, _stringLiteral3516557091, 5, /*hidden argument*/NULL);
		V_8 = (bool)((((int32_t)L_65) == ((int32_t)0))? 1 : 0);
		bool L_66 = V_8;
		if (L_66)
		{
			goto IL_01bd;
		}
	}
	{
		String_t* L_67 = ___uri;
		TcpClientSession_t1301539049 * L_68 = WebSocket_CreateSecureClient_m3104541832(__this, L_67, /*hidden argument*/NULL);
		V_7 = L_68;
		goto IL_01ce;
	}

IL_01bd:
	{
		ArgumentException_t124305799 * L_69 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_69, _stringLiteral713168803, _stringLiteral116076, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_69);
	}

IL_01ce:
	{
		TcpClientSession_t1301539049 * L_70 = V_7;
		IntPtr_t L_71;
		L_71.set_m_value_0((void*)WebSocket_client_Connected_m4243894931_MethodInfo_var);
		EventHandler_t247020293 * L_72 = (EventHandler_t247020293 *)il2cpp_codegen_object_new(EventHandler_t247020293_il2cpp_TypeInfo_var);
		EventHandler__ctor_m4096468317(L_72, __this, L_71, /*hidden argument*/NULL);
		NullCheck(L_70);
		VirtActionInvoker1< EventHandler_t247020293 * >::Invoke(13 /* System.Void SuperSocket.ClientEngine.ClientSession::add_Connected(System.EventHandler) */, L_70, L_72);
		TcpClientSession_t1301539049 * L_73 = V_7;
		IntPtr_t L_74;
		L_74.set_m_value_0((void*)WebSocket_client_Closed_m2085449848_MethodInfo_var);
		EventHandler_t247020293 * L_75 = (EventHandler_t247020293 *)il2cpp_codegen_object_new(EventHandler_t247020293_il2cpp_TypeInfo_var);
		EventHandler__ctor_m4096468317(L_75, __this, L_74, /*hidden argument*/NULL);
		NullCheck(L_73);
		VirtActionInvoker1< EventHandler_t247020293 * >::Invoke(7 /* System.Void SuperSocket.ClientEngine.ClientSession::add_Closed(System.EventHandler) */, L_73, L_75);
		TcpClientSession_t1301539049 * L_76 = V_7;
		IntPtr_t L_77;
		L_77.set_m_value_0((void*)WebSocket_client_Error_m829371704_MethodInfo_var);
		EventHandler_1_t2171844441 * L_78 = (EventHandler_1_t2171844441 *)il2cpp_codegen_object_new(EventHandler_1_t2171844441_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m3300027855(L_78, __this, L_77, /*hidden argument*/EventHandler_1__ctor_m3300027855_MethodInfo_var);
		NullCheck(L_76);
		VirtActionInvoker1< EventHandler_1_t2171844441 * >::Invoke(10 /* System.Void SuperSocket.ClientEngine.ClientSession::add_Error(System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>) */, L_76, L_78);
		TcpClientSession_t1301539049 * L_79 = V_7;
		IntPtr_t L_80;
		L_80.set_m_value_0((void*)WebSocket_client_DataReceived_m3695014531_MethodInfo_var);
		EventHandler_1_t4058788791 * L_81 = (EventHandler_1_t4058788791 *)il2cpp_codegen_object_new(EventHandler_1_t4058788791_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m575740431(L_81, __this, L_80, /*hidden argument*/EventHandler_1__ctor_m575740431_MethodInfo_var);
		NullCheck(L_79);
		VirtActionInvoker1< EventHandler_1_t4058788791 * >::Invoke(16 /* System.Void SuperSocket.ClientEngine.ClientSession::add_DataReceived(System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>) */, L_79, L_81);
		TcpClientSession_t1301539049 * L_82 = V_7;
		WebSocket_set_Client_m1471644827(__this, L_82, /*hidden argument*/NULL);
		WebSocket_set_EnableAutoSendPing_m1331589028(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::client_DataReceived(System.Object,SuperSocket.ClientEngine.DataEventArgs)
extern "C"  void WebSocket_client_DataReceived_m3695014531 (WebSocket_t713846903 * __this, Il2CppObject * ___sender, DataEventArgs_t3216211148 * ___e, const MethodInfo* method)
{
	{
		DataEventArgs_t3216211148 * L_0 = ___e;
		NullCheck(L_0);
		ByteU5BU5D_t58506160* L_1 = DataEventArgs_get_Data_m3597378432(L_0, /*hidden argument*/NULL);
		DataEventArgs_t3216211148 * L_2 = ___e;
		NullCheck(L_2);
		int32_t L_3 = DataEventArgs_get_Offset_m635555377(L_2, /*hidden argument*/NULL);
		DataEventArgs_t3216211148 * L_4 = ___e;
		NullCheck(L_4);
		int32_t L_5 = DataEventArgs_get_Length_m3327941572(L_4, /*hidden argument*/NULL);
		WebSocket_OnDataReceived_m2703049051(__this, L_1, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::client_Error(System.Object,SuperSocket.ClientEngine.ErrorEventArgs)
extern "C"  void WebSocket_client_Error_m829371704 (WebSocket_t713846903 * __this, Il2CppObject * ___sender, ErrorEventArgs_t1329266798 * ___e, const MethodInfo* method)
{
	{
		ErrorEventArgs_t1329266798 * L_0 = ___e;
		WebSocket_OnError_m112179675(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::client_Closed(System.Object,System.EventArgs)
extern "C"  void WebSocket_client_Closed_m2085449848 (WebSocket_t713846903 * __this, Il2CppObject * ___sender, EventArgs_t516466188 * ___e, const MethodInfo* method)
{
	{
		WebSocket_OnClosed_m161702351(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::client_Connected(System.Object,System.EventArgs)
extern "C"  void WebSocket_client_Connected_m4243894931 (WebSocket_t713846903 * __this, Il2CppObject * ___sender, EventArgs_t516466188 * ___e, const MethodInfo* method)
{
	{
		WebSocket_OnConnected_m3753639976(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocket4Net.WebSocket::GetAvailableProcessor(System.Int32[])
extern TypeInfo* WebSocket_t713846903_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_GetAvailableProcessor_m3999224164_MetadataUsageId;
extern "C"  bool WebSocket_GetAvailableProcessor_m3999224164 (WebSocket_t713846903 * __this, Int32U5BU5D_t1809983122* ___availableVersions, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_GetAvailableProcessor_m3999224164_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t713846903_il2cpp_TypeInfo_var);
		ProtocolProcessorFactory_t3499308290 * L_0 = ((WebSocket_t713846903_StaticFields*)WebSocket_t713846903_il2cpp_TypeInfo_var->static_fields)->get_m_ProtocolProcessorFactory_8();
		Int32U5BU5D_t1809983122* L_1 = ___availableVersions;
		NullCheck(L_0);
		Il2CppObject * L_2 = ProtocolProcessorFactory_GetPreferedProcessorFromAvialable_m2607568602(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Il2CppObject * L_3 = V_0;
		V_2 = (bool)((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_2;
		if (L_4)
		{
			goto IL_001c;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0028;
	}

IL_001c:
	{
		Il2CppObject * L_5 = V_0;
		WebSocket_set_ProtocolProcessor_m2566430094(__this, L_5, /*hidden argument*/NULL);
		V_1 = (bool)1;
		goto IL_0028;
	}

IL_0028:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Void WebSocket4Net.WebSocket::Open()
extern "C"  void WebSocket_Open_m1696656814 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		WebSocket_set_State_m2318924618(__this, 0, /*hidden argument*/NULL);
		Il2CppObject * L_0 = WebSocket_get_Proxy_m857243229(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		TcpClientSession_t1301539049 * L_2 = WebSocket_get_Client_m2705060304(__this, /*hidden argument*/NULL);
		Il2CppObject * L_3 = WebSocket_get_Proxy_m857243229(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void SuperSocket.ClientEngine.ClientSession::set_Proxy(SuperSocket.ClientEngine.IProxyConnector) */, L_2, L_3);
	}

IL_0028:
	{
		TcpClientSession_t1301539049 * L_4 = WebSocket_get_Client_m2705060304(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(4 /* System.Void SuperSocket.ClientEngine.ClientSession::Connect() */, L_4);
		return;
	}
}
// WebSocket4Net.Protocol.IProtocolProcessor WebSocket4Net.WebSocket::GetProtocolProcessor(WebSocket4Net.WebSocketVersion)
extern TypeInfo* WebSocket_t713846903_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227842934;
extern const uint32_t WebSocket_GetProtocolProcessor_m3470676079_MetadataUsageId;
extern "C"  Il2CppObject * WebSocket_GetProtocolProcessor_m3470676079 (Il2CppObject * __this /* static, unused */, int32_t ___version, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_GetProtocolProcessor_m3470676079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocket_t713846903_il2cpp_TypeInfo_var);
		ProtocolProcessorFactory_t3499308290 * L_0 = ((WebSocket_t713846903_StaticFields*)WebSocket_t713846903_il2cpp_TypeInfo_var->static_fields)->get_m_ProtocolProcessorFactory_8();
		int32_t L_1 = ___version;
		NullCheck(L_0);
		Il2CppObject * L_2 = ProtocolProcessorFactory_GetProcessorByVersion_m1724826559(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Il2CppObject * L_3 = V_0;
		V_2 = (bool)((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_2;
		if (L_4)
		{
			goto IL_0023;
		}
	}
	{
		ArgumentException_t124305799 * L_5 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_5, _stringLiteral4227842934, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0023:
	{
		Il2CppObject * L_6 = V_0;
		V_1 = L_6;
		goto IL_0027;
	}

IL_0027:
	{
		Il2CppObject * L_7 = V_1;
		return L_7;
	}
}
// System.Void WebSocket4Net.WebSocket::OnConnected()
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern TypeInfo* ICollection_1_t2429167008_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_OnConnected_m3753639976_MetadataUsageId;
extern "C"  void WebSocket_OnConnected_m3753639976 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_OnConnected_m3753639976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Il2CppObject * L_0 = WebSocket_get_ProtocolProcessor_m1265601209(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ReaderBase_t893022310 * L_1 = InterfaceFuncInvoker1< ReaderBase_t893022310 *, WebSocket_t713846903 * >::Invoke(2 /* WebSocket4Net.Protocol.ReaderBase WebSocket4Net.Protocol.IProtocolProcessor::CreateHandshakeReader(WebSocket4Net.WebSocket) */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_0, __this);
		WebSocket_set_CommandReader_m748575549(__this, L_1, /*hidden argument*/NULL);
		Il2CppObject* L_2 = WebSocket_get_Items_m1025846418(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::get_Count() */, ICollection_1_t2429167008_il2cpp_TypeInfo_var, L_2);
		V_0 = (bool)((((int32_t)((((int32_t)L_3) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_0;
		if (L_4)
		{
			goto IL_0035;
		}
	}
	{
		Il2CppObject* L_5 = WebSocket_get_Items_m1025846418(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::Clear() */, ICollection_1_t2429167008_il2cpp_TypeInfo_var, L_5);
	}

IL_0035:
	{
		Il2CppObject * L_6 = WebSocket_get_ProtocolProcessor_m1265601209(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		InterfaceActionInvoker1< WebSocket_t713846903 * >::Invoke(0 /* System.Void WebSocket4Net.Protocol.IProtocolProcessor::SendHandshake(WebSocket4Net.WebSocket) */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_6, __this);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::OnHandshaked()
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern TypeInfo* TimerCallback_t4291881837_il2cpp_TypeInfo_var;
extern TypeInfo* Timer_t3546110984_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocket_OnPingTimerCallback_m125910333_MethodInfo_var;
extern const uint32_t WebSocket_OnHandshaked_m3855060592_MetadataUsageId;
extern "C"  void WebSocket_OnHandshaked_m3855060592 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_OnHandshaked_m3855060592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		WebSocket_set_State_m2318924618(__this, 1, /*hidden argument*/NULL);
		WebSocket_set_Handshaked_m224100325(__this, (bool)1, /*hidden argument*/NULL);
		EventHandler_t247020293 * L_0 = __this->get_m_Opened_11();
		V_0 = (bool)((((int32_t)((((Il2CppObject*)(EventHandler_t247020293 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		goto IL_009f;
	}

IL_0023:
	{
		EventHandler_t247020293 * L_2 = __this->get_m_Opened_11();
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs_t516466188 * L_3 = ((EventArgs_t516466188_StaticFields*)EventArgs_t516466188_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_2);
		EventHandler_Invoke_m1659461265(L_2, __this, L_3, /*hidden argument*/NULL);
		bool L_4 = WebSocket_get_EnableAutoSendPing_m3480068097(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004d;
		}
	}
	{
		Il2CppObject * L_5 = WebSocket_get_ProtocolProcessor_m1265601209(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(7 /* System.Boolean WebSocket4Net.Protocol.IProtocolProcessor::get_SupportPingPong() */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_5);
		G_B5_0 = ((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		goto IL_004e;
	}

IL_004d:
	{
		G_B5_0 = 1;
	}

IL_004e:
	{
		V_0 = (bool)G_B5_0;
		bool L_7 = V_0;
		if (L_7)
		{
			goto IL_009f;
		}
	}
	{
		int32_t L_8 = WebSocket_get_AutoSendPingInterval_m458397481(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_8) > ((int32_t)0))? 1 : 0);
		bool L_9 = V_0;
		if (L_9)
		{
			goto IL_0069;
		}
	}
	{
		WebSocket_set_AutoSendPingInterval_m3191023104(__this, ((int32_t)60), /*hidden argument*/NULL);
	}

IL_0069:
	{
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)WebSocket_OnPingTimerCallback_m125910333_MethodInfo_var);
		TimerCallback_t4291881837 * L_11 = (TimerCallback_t4291881837 *)il2cpp_codegen_object_new(TimerCallback_t4291881837_il2cpp_TypeInfo_var);
		TimerCallback__ctor_m1834223461(L_11, __this, L_10, /*hidden argument*/NULL);
		Il2CppObject * L_12 = WebSocket_get_ProtocolProcessor_m1265601209(__this, /*hidden argument*/NULL);
		int32_t L_13 = WebSocket_get_AutoSendPingInterval_m458397481(__this, /*hidden argument*/NULL);
		int32_t L_14 = WebSocket_get_AutoSendPingInterval_m458397481(__this, /*hidden argument*/NULL);
		Timer_t3546110984 * L_15 = (Timer_t3546110984 *)il2cpp_codegen_object_new(Timer_t3546110984_il2cpp_TypeInfo_var);
		Timer__ctor_m1028438267(L_15, L_11, L_12, ((int32_t)((int32_t)L_13*(int32_t)((int32_t)1000))), ((int32_t)((int32_t)L_14*(int32_t)((int32_t)1000))), /*hidden argument*/NULL);
		__this->set_m_PingTimer_9(L_15);
	}

IL_009f:
	{
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::OnPingTimerCallback(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_OnPingTimerCallback_m125910333_MetadataUsageId;
extern "C"  void WebSocket_OnPingTimerCallback_m125910333 (WebSocket_t713846903 * __this, Il2CppObject * ___state, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_OnPingTimerCallback_m125910333_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	DateTime_t339033936  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = __this->get_m_LastPingRequest_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2 = __this->get_m_LastPingRequest_10();
		String_t* L_3 = WebSocket_get_LastPongResponse_m1283402937(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_4 = String_Equals_m3541721061(L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		V_1 = (bool)G_B3_0;
		bool L_5 = V_1;
		if (L_5)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_0057;
	}

IL_0029:
	{
		Il2CppObject * L_6 = ___state;
		V_0 = ((Il2CppObject *)IsInst(L_6, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_7 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_7;
		String_t* L_8 = DateTime_ToString_m3221907059((&V_2), /*hidden argument*/NULL);
		__this->set_m_LastPingRequest_10(L_8);
		Il2CppObject * L_9 = V_0;
		String_t* L_10 = __this->get_m_LastPingRequest_10();
		NullCheck(L_9);
		InterfaceActionInvoker2< WebSocket_t713846903 *, String_t* >::Invoke(5 /* System.Void WebSocket4Net.Protocol.IProtocolProcessor::SendPing(WebSocket4Net.WebSocket,System.String) */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_9, __this, L_10);
	}

IL_0057:
	{
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::add_Opened(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_add_Opened_m1939620504_MetadataUsageId;
extern "C"  void WebSocket_add_Opened_m1939620504 (WebSocket_t713846903 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_add_Opened_m1939620504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_m_Opened_11();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_Opened_11(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::remove_Opened(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_remove_Opened_m2167545249_MetadataUsageId;
extern "C"  void WebSocket_remove_Opened_m2167545249 (WebSocket_t713846903 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_remove_Opened_m2167545249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_m_Opened_11();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_Opened_11(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::add_MessageReceived(System.EventHandler`1<WebSocket4Net.MessageReceivedEventArgs>)
extern TypeInfo* EventHandler_1_t419508266_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_add_MessageReceived_m1609756021_MetadataUsageId;
extern "C"  void WebSocket_add_MessageReceived_m1609756021 (WebSocket_t713846903 * __this, EventHandler_1_t419508266 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_add_MessageReceived_m1609756021_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t419508266 * L_0 = __this->get_m_MessageReceived_12();
		EventHandler_1_t419508266 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_MessageReceived_12(((EventHandler_1_t419508266 *)CastclassSealed(L_2, EventHandler_1_t419508266_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::remove_MessageReceived(System.EventHandler`1<WebSocket4Net.MessageReceivedEventArgs>)
extern TypeInfo* EventHandler_1_t419508266_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_remove_MessageReceived_m4109017036_MetadataUsageId;
extern "C"  void WebSocket_remove_MessageReceived_m4109017036 (WebSocket_t713846903 * __this, EventHandler_1_t419508266 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_remove_MessageReceived_m4109017036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t419508266 * L_0 = __this->get_m_MessageReceived_12();
		EventHandler_1_t419508266 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_MessageReceived_12(((EventHandler_1_t419508266 *)CastclassSealed(L_2, EventHandler_1_t419508266_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::FireMessageReceived(System.String)
extern TypeInfo* MessageReceivedEventArgs_t3871897919_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m2287782091_MethodInfo_var;
extern const uint32_t WebSocket_FireMessageReceived_m663897554_MetadataUsageId;
extern "C"  void WebSocket_FireMessageReceived_m663897554 (WebSocket_t713846903 * __this, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_FireMessageReceived_m663897554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		EventHandler_1_t419508266 * L_0 = __this->get_m_MessageReceived_12();
		V_0 = (bool)((((int32_t)((((Il2CppObject*)(EventHandler_1_t419508266 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		goto IL_0026;
	}

IL_0013:
	{
		EventHandler_1_t419508266 * L_2 = __this->get_m_MessageReceived_12();
		String_t* L_3 = ___message;
		MessageReceivedEventArgs_t3871897919 * L_4 = (MessageReceivedEventArgs_t3871897919 *)il2cpp_codegen_object_new(MessageReceivedEventArgs_t3871897919_il2cpp_TypeInfo_var);
		MessageReceivedEventArgs__ctor_m2681790322(L_4, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		EventHandler_1_Invoke_m2287782091(L_2, __this, L_4, /*hidden argument*/EventHandler_1_Invoke_m2287782091_MethodInfo_var);
	}

IL_0026:
	{
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::FireDataReceived(System.Byte[])
extern TypeInfo* DataReceivedEventArgs_t1979092924_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m3139923886_MethodInfo_var;
extern const uint32_t WebSocket_FireDataReceived_m2475134404_MetadataUsageId;
extern "C"  void WebSocket_FireDataReceived_m2475134404 (WebSocket_t713846903 * __this, ByteU5BU5D_t58506160* ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_FireDataReceived_m2475134404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		EventHandler_1_t2821670567 * L_0 = __this->get_m_DataReceived_13();
		V_0 = (bool)((((int32_t)((((Il2CppObject*)(EventHandler_1_t2821670567 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		goto IL_0026;
	}

IL_0013:
	{
		EventHandler_1_t2821670567 * L_2 = __this->get_m_DataReceived_13();
		ByteU5BU5D_t58506160* L_3 = ___data;
		DataReceivedEventArgs_t1979092924 * L_4 = (DataReceivedEventArgs_t1979092924 *)il2cpp_codegen_object_new(DataReceivedEventArgs_t1979092924_il2cpp_TypeInfo_var);
		DataReceivedEventArgs__ctor_m2970247150(L_4, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		EventHandler_1_Invoke_m3139923886(L_2, __this, L_4, /*hidden argument*/EventHandler_1_Invoke_m3139923886_MethodInfo_var);
	}

IL_0026:
	{
		return;
	}
}
// System.Boolean WebSocket4Net.WebSocket::EnsureWebSocketOpen()
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2865824205;
extern const uint32_t WebSocket_EnsureWebSocketOpen_m1878363049_MetadataUsageId;
extern "C"  bool WebSocket_EnsureWebSocketOpen_m1878363049 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_EnsureWebSocketOpen_m1878363049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		bool L_0 = WebSocket_get_Handshaked_m3019381250(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		bool L_1 = V_1;
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		Exception_t1967233988 * L_2 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_2, _stringLiteral2865824205, /*hidden argument*/NULL);
		WebSocket_OnError_m1918295319(__this, L_2, /*hidden argument*/NULL);
		V_0 = (bool)0;
		goto IL_0025;
	}

IL_0021:
	{
		V_0 = (bool)1;
		goto IL_0025;
	}

IL_0025:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void WebSocket4Net.WebSocket::Send(System.String)
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_Send_m1750322966_MetadataUsageId;
extern "C"  void WebSocket_Send_m1750322966 (WebSocket_t713846903 * __this, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Send_m1750322966_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		bool L_0 = WebSocket_EnsureWebSocketOpen_m1878363049(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		goto IL_001b;
	}

IL_000d:
	{
		Il2CppObject * L_2 = WebSocket_get_ProtocolProcessor_m1265601209(__this, /*hidden argument*/NULL);
		String_t* L_3 = ___message;
		NullCheck(L_2);
		InterfaceActionInvoker2< WebSocket_t713846903 *, String_t* >::Invoke(3 /* System.Void WebSocket4Net.Protocol.IProtocolProcessor::SendMessage(WebSocket4Net.WebSocket,System.String) */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_2, __this, L_3);
	}

IL_001b:
	{
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::OnClosed()
extern "C"  void WebSocket_OnClosed_m161702351 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	int32_t G_B3_0 = 0;
	{
		V_0 = (bool)0;
		int32_t L_0 = WebSocket_get_State_m3481149631(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = WebSocket_get_State_m3481149631(__this, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((int32_t)L_1) == ((int32_t)1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001b;
	}

IL_001a:
	{
		G_B3_0 = 0;
	}

IL_001b:
	{
		V_1 = (bool)G_B3_0;
		bool L_2 = V_1;
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0021:
	{
		WebSocket_set_State_m2318924618(__this, 3, /*hidden argument*/NULL);
		bool L_3 = V_0;
		V_1 = (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_1;
		if (L_4)
		{
			goto IL_0038;
		}
	}
	{
		WebSocket_FireClosed_m2436478566(__this, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::Close()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_Close_m3186537590_MetadataUsageId;
extern "C"  void WebSocket_Close_m3186537590 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Close_m3186537590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = WebSocket_get_State_m3481149631(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		WebSocket_set_State_m2318924618(__this, 3, /*hidden argument*/NULL);
		WebSocket_OnClosed_m161702351(__this, /*hidden argument*/NULL);
		goto IL_002f;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		WebSocket_Close_m3107084876(__this, L_2, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::Close(System.String)
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern TypeInfo* ICloseStatusCode_t28582816_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_Close_m3107084876_MetadataUsageId;
extern "C"  void WebSocket_Close_m3107084876 (WebSocket_t713846903 * __this, String_t* ___reason, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Close_m3107084876_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = WebSocket_get_ProtocolProcessor_m1265601209(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(8 /* WebSocket4Net.Protocol.ICloseStatusCode WebSocket4Net.Protocol.IProtocolProcessor::get_CloseStatusCode() */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_0);
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 WebSocket4Net.Protocol.ICloseStatusCode::get_NormalClosure() */, ICloseStatusCode_t28582816_il2cpp_TypeInfo_var, L_1);
		String_t* L_3 = ___reason;
		WebSocket_Close_m946219203(__this, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::Close(System.Int32,System.String)
extern TypeInfo* IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_Close_m946219203_MetadataUsageId;
extern "C"  void WebSocket_Close_m946219203 (WebSocket_t713846903 * __this, int32_t ___statusCode, String_t* ___reason, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Close_m946219203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebSocket_set_State_m2318924618(__this, 2, /*hidden argument*/NULL);
		Il2CppObject * L_0 = WebSocket_get_ProtocolProcessor_m1265601209(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___statusCode;
		String_t* L_2 = ___reason;
		NullCheck(L_0);
		InterfaceActionInvoker3< WebSocket_t713846903 *, int32_t, String_t* >::Invoke(4 /* System.Void WebSocket4Net.Protocol.IProtocolProcessor::SendCloseHandshake(WebSocket4Net.WebSocket,System.Int32,System.String) */, IProtocolProcessor_t4208156067_il2cpp_TypeInfo_var, L_0, __this, L_1, L_2);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::CloseWithoutHandshake()
extern "C"  void WebSocket_CloseWithoutHandshake_m1514871525 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	{
		TcpClientSession_t1301539049 * L_0 = WebSocket_get_Client_m2705060304(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(6 /* System.Void SuperSocket.ClientEngine.ClientSession::Close() */, L_0);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::ExecuteCommand(WebSocket4Net.WebSocketCommandInfo)
extern TypeInfo* ICommand_2_t625221883_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_ExecuteCommand_m123150858_MetadataUsageId;
extern "C"  void WebSocket_ExecuteCommand_m123150858 (WebSocket_t713846903 * __this, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_ExecuteCommand_m123150858_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	bool V_1 = false;
	{
		Dictionary_2_t2262919787 * L_0 = __this->get_m_CommandDict_7();
		WebSocketCommandInfo_t3536916738 * L_1 = ___commandInfo;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String WebSocket4Net.WebSocketCommandInfo::get_Key() */, L_1);
		NullCheck(L_0);
		bool L_3 = VirtFuncInvoker2< bool, String_t*, Il2CppObject** >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::TryGetValue(!0,!1&) */, L_0, L_2, (&V_0));
		V_1 = (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_1;
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject* L_5 = V_0;
		WebSocketCommandInfo_t3536916738 * L_6 = ___commandInfo;
		NullCheck(L_5);
		InterfaceActionInvoker2< WebSocket_t713846903 *, WebSocketCommandInfo_t3536916738 * >::Invoke(0 /* System.Void SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>::ExecuteCommand(!0,!1) */, ICommand_2_t625221883_il2cpp_TypeInfo_var, L_5, __this, L_6);
	}

IL_0026:
	{
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::OnDataReceived(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* IClientCommandReader_1_t3175310917_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_OnDataReceived_m2703049051_MetadataUsageId;
extern "C"  void WebSocket_OnDataReceived_m2703049051 (WebSocket_t713846903 * __this, ByteU5BU5D_t58506160* ___data, int32_t ___offset, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_OnDataReceived_m2703049051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	WebSocketCommandInfo_t3536916738 * V_1 = NULL;
	bool V_2 = false;
	{
		goto IL_0063;
	}

IL_0003:
	{
		Il2CppObject* L_0 = WebSocket_get_CommandReader_m2191282226(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_1 = ___data;
		int32_t L_2 = ___offset;
		int32_t L_3 = ___length;
		NullCheck(L_0);
		WebSocketCommandInfo_t3536916738 * L_4 = InterfaceFuncInvoker4< WebSocketCommandInfo_t3536916738 *, ByteU5BU5D_t58506160*, int32_t, int32_t, int32_t* >::Invoke(0 /* !0 SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>::GetCommandInfo(System.Byte[],System.Int32,System.Int32,System.Int32&) */, IClientCommandReader_1_t3175310917_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3, (&V_0));
		V_1 = L_4;
		Il2CppObject* L_5 = WebSocket_get_CommandReader_m2191282226(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(1 /* SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<!0> SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>::get_NextCommandReader() */, IClientCommandReader_1_t3175310917_il2cpp_TypeInfo_var, L_5);
		V_2 = (bool)((((Il2CppObject*)(Il2CppObject*)L_6) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_7 = V_2;
		if (L_7)
		{
			goto IL_0039;
		}
	}
	{
		Il2CppObject* L_8 = WebSocket_get_CommandReader_m2191282226(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Il2CppObject* L_9 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(1 /* SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<!0> SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>::get_NextCommandReader() */, IClientCommandReader_1_t3175310917_il2cpp_TypeInfo_var, L_8);
		WebSocket_set_CommandReader_m748575549(__this, L_9, /*hidden argument*/NULL);
	}

IL_0039:
	{
		WebSocketCommandInfo_t3536916738 * L_10 = V_1;
		V_2 = (bool)((((int32_t)((((Il2CppObject*)(WebSocketCommandInfo_t3536916738 *)L_10) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_11 = V_2;
		if (L_11)
		{
			goto IL_0046;
		}
	}
	{
		goto IL_0067;
	}

IL_0046:
	{
		WebSocketCommandInfo_t3536916738 * L_12 = V_1;
		WebSocket_ExecuteCommand_m123150858(__this, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		V_2 = (bool)((((int32_t)L_13) > ((int32_t)0))? 1 : 0);
		bool L_14 = V_2;
		if (L_14)
		{
			goto IL_0058;
		}
	}
	{
		goto IL_0067;
	}

IL_0058:
	{
		int32_t L_15 = ___offset;
		int32_t L_16 = ___length;
		int32_t L_17 = V_0;
		___offset = ((int32_t)((int32_t)((int32_t)((int32_t)L_15+(int32_t)L_16))-(int32_t)L_17));
		int32_t L_18 = V_0;
		___length = L_18;
	}

IL_0063:
	{
		V_2 = (bool)1;
		goto IL_0003;
	}

IL_0067:
	{
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::FireError(System.Exception)
extern "C"  void WebSocket_FireError_m4124567456 (WebSocket_t713846903 * __this, Exception_t1967233988 * ___error, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___error;
		WebSocket_OnError_m1918295319(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::add_Closed(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_add_Closed_m3528784245_MetadataUsageId;
extern "C"  void WebSocket_add_Closed_m3528784245 (WebSocket_t713846903 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_add_Closed_m3528784245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_m_Closed_14();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_Closed_14(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::remove_Closed(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_remove_Closed_m3756708990_MetadataUsageId;
extern "C"  void WebSocket_remove_Closed_m3756708990 (WebSocket_t713846903 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_remove_Closed_m3756708990_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_m_Closed_14();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_Closed_14(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::FireClosed()
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_FireClosed_m2436478566_MetadataUsageId;
extern "C"  void WebSocket_FireClosed_m2436478566 (WebSocket_t713846903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_FireClosed_m2436478566_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EventHandler_t247020293 * V_0 = NULL;
	bool V_1 = false;
	{
		Timer_t3546110984 * L_0 = __this->get_m_PingTimer_9();
		V_1 = (bool)((((Il2CppObject*)(Timer_t3546110984 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_1;
		if (L_1)
		{
			goto IL_0031;
		}
	}
	{
		Timer_t3546110984 * L_2 = __this->get_m_PingTimer_9();
		NullCheck(L_2);
		Timer_Change_m2492385938(L_2, (-1), (-1), /*hidden argument*/NULL);
		Timer_t3546110984 * L_3 = __this->get_m_PingTimer_9();
		NullCheck(L_3);
		Timer_Dispose_m4229130271(L_3, /*hidden argument*/NULL);
		__this->set_m_PingTimer_9((Timer_t3546110984 *)NULL);
	}

IL_0031:
	{
		EventHandler_t247020293 * L_4 = __this->get_m_Closed_14();
		V_0 = L_4;
		EventHandler_t247020293 * L_5 = V_0;
		V_1 = (bool)((((Il2CppObject*)(EventHandler_t247020293 *)L_5) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_6 = V_1;
		if (L_6)
		{
			goto IL_004d;
		}
	}
	{
		EventHandler_t247020293 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs_t516466188 * L_8 = ((EventArgs_t516466188_StaticFields*)EventArgs_t516466188_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_7);
		EventHandler_Invoke_m1659461265(L_7, __this, L_8, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::add_Error(System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>)
extern TypeInfo* EventHandler_1_t2171844441_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_add_Error_m447077374_MetadataUsageId;
extern "C"  void WebSocket_add_Error_m447077374 (WebSocket_t713846903 * __this, EventHandler_1_t2171844441 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_add_Error_m447077374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t2171844441 * L_0 = __this->get_m_Error_15();
		EventHandler_1_t2171844441 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_Error_15(((EventHandler_1_t2171844441 *)CastclassSealed(L_2, EventHandler_1_t2171844441_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::remove_Error(System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>)
extern TypeInfo* EventHandler_1_t2171844441_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_remove_Error_m1728502727_MetadataUsageId;
extern "C"  void WebSocket_remove_Error_m1728502727 (WebSocket_t713846903 * __this, EventHandler_1_t2171844441 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_remove_Error_m1728502727_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t2171844441 * L_0 = __this->get_m_Error_15();
		EventHandler_1_t2171844441 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_Error_15(((EventHandler_1_t2171844441 *)CastclassSealed(L_2, EventHandler_1_t2171844441_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::OnError(SuperSocket.ClientEngine.ErrorEventArgs)
extern const MethodInfo* EventHandler_1_Invoke_m2407594268_MethodInfo_var;
extern const uint32_t WebSocket_OnError_m112179675_MetadataUsageId;
extern "C"  void WebSocket_OnError_m112179675 (WebSocket_t713846903 * __this, ErrorEventArgs_t1329266798 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_OnError_m112179675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		EventHandler_1_t2171844441 * L_0 = __this->get_m_Error_15();
		V_0 = (bool)((((int32_t)((((Il2CppObject*)(EventHandler_1_t2171844441 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		goto IL_0021;
	}

IL_0013:
	{
		EventHandler_1_t2171844441 * L_2 = __this->get_m_Error_15();
		ErrorEventArgs_t1329266798 * L_3 = ___e;
		NullCheck(L_2);
		EventHandler_1_Invoke_m2407594268(L_2, __this, L_3, /*hidden argument*/EventHandler_1_Invoke_m2407594268_MethodInfo_var);
	}

IL_0021:
	{
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::OnError(System.Exception)
extern TypeInfo* ErrorEventArgs_t1329266798_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_OnError_m1918295319_MetadataUsageId;
extern "C"  void WebSocket_OnError_m1918295319 (WebSocket_t713846903 * __this, Exception_t1967233988 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_OnError_m1918295319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = ___e;
		ErrorEventArgs_t1329266798 * L_1 = (ErrorEventArgs_t1329266798 *)il2cpp_codegen_object_new(ErrorEventArgs_t1329266798_il2cpp_TypeInfo_var);
		ErrorEventArgs__ctor_m1471498585(L_1, L_0, /*hidden argument*/NULL);
		WebSocket_OnError_m112179675(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::.ctor(System.String,System.String,WebSocket4Net.WebSocketVersion)
extern "C"  void WebSocket__ctor_m3811251545 (WebSocket_t713846903 * __this, String_t* ___uri, String_t* ___subProtocol, int32_t ___version, const MethodInfo* method)
{
	{
		String_t* L_0 = ___uri;
		String_t* L_1 = ___subProtocol;
		int32_t L_2 = ___version;
		WebSocket__ctor_m1186845652(__this, L_0, L_1, (List_1_t2891677073 *)NULL, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::.ctor(System.String,System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,WebSocket4Net.WebSocketVersion)
extern TypeInfo* List_1_t2891677073_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4107555106_MethodInfo_var;
extern const uint32_t WebSocket__ctor_m1186845652_MetadataUsageId;
extern "C"  void WebSocket__ctor_m1186845652 (WebSocket_t713846903 * __this, String_t* ___uri, String_t* ___subProtocol, List_1_t2891677073 * ___cookies, int32_t ___version, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket__ctor_m1186845652_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___uri;
		String_t* L_1 = ___subProtocol;
		List_1_t2891677073 * L_2 = ___cookies;
		List_1_t2891677073 * L_3 = (List_1_t2891677073 *)il2cpp_codegen_object_new(List_1_t2891677073_il2cpp_TypeInfo_var);
		List_1__ctor_m4107555106(L_3, /*hidden argument*/List_1__ctor_m4107555106_MethodInfo_var);
		int32_t L_4 = ___version;
		WebSocket__ctor_m2339403475(__this, L_0, L_1, L_2, L_3, (String_t*)NULL, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::.ctor(System.String,System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.String,WebSocket4Net.WebSocketVersion)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket__ctor_m2339403475_MetadataUsageId;
extern "C"  void WebSocket__ctor_m2339403475 (WebSocket_t713846903 * __this, String_t* ___uri, String_t* ___subProtocol, List_1_t2891677073 * ___cookies, List_1_t2891677073 * ___customHeaderItems, String_t* ___userAgent, int32_t ___version, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket__ctor_m2339403475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___uri;
		String_t* L_1 = ___subProtocol;
		List_1_t2891677073 * L_2 = ___cookies;
		List_1_t2891677073 * L_3 = ___customHeaderItems;
		String_t* L_4 = ___userAgent;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_6 = ___version;
		WebSocket__ctor_m253146647(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocket::.ctor(System.String,System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.String,System.String,WebSocket4Net.WebSocketVersion)
extern TypeInfo* StringComparer_t4058118931_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t2262919787_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3514137471_MethodInfo_var;
extern const uint32_t WebSocket__ctor_m253146647_MetadataUsageId;
extern "C"  void WebSocket__ctor_m253146647 (WebSocket_t713846903 * __this, String_t* ___uri, String_t* ___subProtocol, List_1_t2891677073 * ___cookies, List_1_t2891677073 * ___customHeaderItems, String_t* ___userAgent, String_t* ___origin, int32_t ___version, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket__ctor_m253146647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t4058118931_il2cpp_TypeInfo_var);
		StringComparer_t4058118931 * L_0 = StringComparer_get_OrdinalIgnoreCase_m2513153269(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t2262919787 * L_1 = (Dictionary_2_t2262919787 *)il2cpp_codegen_object_new(Dictionary_2_t2262919787_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3514137471(L_1, L_0, /*hidden argument*/Dictionary_2__ctor_m3514137471_MethodInfo_var);
		__this->set_m_CommandDict_7(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___uri;
		String_t* L_3 = ___subProtocol;
		List_1_t2891677073 * L_4 = ___cookies;
		List_1_t2891677073 * L_5 = ___customHeaderItems;
		String_t* L_6 = ___userAgent;
		String_t* L_7 = ___origin;
		int32_t L_8 = ___version;
		WebSocket_Initialize_m3081830635(__this, L_2, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocketCommandInfo::.ctor()
extern "C"  void WebSocketCommandInfo__ctor_m135330605 (WebSocketCommandInfo_t3536916738 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocketCommandInfo::.ctor(System.String)
extern "C"  void WebSocketCommandInfo__ctor_m2432496885 (WebSocketCommandInfo_t3536916738 * __this, String_t* ___key, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___key;
		WebSocketCommandInfo_set_Key_m3535408597(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocketCommandInfo::.ctor(System.String,System.String)
extern "C"  void WebSocketCommandInfo__ctor_m1488440433 (WebSocketCommandInfo_t3536916738 * __this, String_t* ___key, String_t* ___text, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___key;
		WebSocketCommandInfo_set_Key_m3535408597(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___text;
		WebSocketCommandInfo_set_Text_m425876161(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocket4Net.WebSocketCommandInfo::.ctor(System.Collections.Generic.IList`1<WebSocket4Net.Protocol.WebSocketDataFrame>)
extern TypeInfo* IList_1_t2389226120_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* ICollection_1_t688565192_il2cpp_TypeInfo_var;
extern TypeInfo* WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t424480421_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegmentList_1_ToArrayData_m509443740_MethodInfo_var;
extern const MethodInfo* WebSocketCommandInfo_U3C_ctorU3Eb__0_m1673743135_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m4038159913_MethodInfo_var;
extern const MethodInfo* Enumerable_Sum_TisWebSocketDataFrame_t222733806_m828120080_MethodInfo_var;
extern const MethodInfo* ArraySegmentList_1_CopyTo_m815842042_MethodInfo_var;
extern const uint32_t WebSocketCommandInfo__ctor_m2019461544_MetadataUsageId;
extern "C"  void WebSocketCommandInfo__ctor_m2019461544 (WebSocketCommandInfo_t3536916738 * __this, Il2CppObject* ___frames, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketCommandInfo__ctor_m2019461544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int8_t V_0 = 0x0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	WebSocketDataFrame_t222733806 * V_3 = NULL;
	StringBuilder_t3822575854 * V_4 = NULL;
	ByteU5BU5D_t58506160* V_5 = NULL;
	int32_t V_6 = 0;
	WebSocketDataFrame_t222733806 * V_7 = NULL;
	ByteU5BU5D_t58506160* V_8 = NULL;
	int32_t V_9 = 0;
	bool V_10 = false;
	Il2CppObject* G_B24_0 = NULL;
	Il2CppObject* G_B23_0 = NULL;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___frames;
		NullCheck(L_0);
		WebSocketDataFrame_t222733806 * L_1 = InterfaceFuncInvoker1< WebSocketDataFrame_t222733806 *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<WebSocket4Net.Protocol.WebSocketDataFrame>::get_Item(System.Int32) */, IList_1_t2389226120_il2cpp_TypeInfo_var, L_0, 0);
		NullCheck(L_1);
		int8_t L_2 = WebSocketDataFrame_get_OpCode_m3260433441(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = SByte_ToString_m1290989501((&V_0), /*hidden argument*/NULL);
		WebSocketCommandInfo_set_Key_m3535408597(__this, L_3, /*hidden argument*/NULL);
		int8_t L_4 = V_0;
		V_10 = (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)8))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_5 = V_10;
		if (L_5)
		{
			goto IL_01a6;
		}
	}
	{
		Il2CppObject* L_6 = ___frames;
		NullCheck(L_6);
		WebSocketDataFrame_t222733806 * L_7 = InterfaceFuncInvoker1< WebSocketDataFrame_t222733806 *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<WebSocket4Net.Protocol.WebSocketDataFrame>::get_Item(System.Int32) */, IList_1_t2389226120_il2cpp_TypeInfo_var, L_6, 0);
		V_3 = L_7;
		WebSocketDataFrame_t222733806 * L_8 = V_3;
		NullCheck(L_8);
		int64_t L_9 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_8, /*hidden argument*/NULL);
		V_2 = (((int32_t)((int32_t)L_9)));
		WebSocketDataFrame_t222733806 * L_10 = V_3;
		NullCheck(L_10);
		ArraySegmentList_t1783467835 * L_11 = WebSocketDataFrame_get_InnerData_m815354334(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_11);
		int32_t L_13 = V_2;
		V_1 = ((int32_t)((int32_t)L_12-(int32_t)L_13));
		StringBuilder_t3822575854 * L_14 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_14, /*hidden argument*/NULL);
		V_4 = L_14;
		int32_t L_15 = V_2;
		V_10 = (bool)((((int32_t)L_15) < ((int32_t)2))? 1 : 0);
		bool L_16 = V_10;
		if (L_16)
		{
			goto IL_00c7;
		}
	}
	{
		WebSocketDataFrame_t222733806 * L_17 = V_3;
		NullCheck(L_17);
		ArraySegmentList_t1783467835 * L_18 = WebSocketDataFrame_get_InnerData_m815354334(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_18);
		int32_t L_20 = V_2;
		V_1 = ((int32_t)((int32_t)L_19-(int32_t)L_20));
		WebSocketDataFrame_t222733806 * L_21 = V_3;
		NullCheck(L_21);
		ArraySegmentList_t1783467835 * L_22 = WebSocketDataFrame_get_InnerData_m815354334(L_21, /*hidden argument*/NULL);
		int32_t L_23 = V_1;
		NullCheck(L_22);
		ByteU5BU5D_t58506160* L_24 = ArraySegmentList_1_ToArrayData_m509443740(L_22, L_23, 2, /*hidden argument*/ArraySegmentList_1_ToArrayData_m509443740_MethodInfo_var);
		V_5 = L_24;
		ByteU5BU5D_t58506160* L_25 = V_5;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		int32_t L_26 = 0;
		ByteU5BU5D_t58506160* L_27 = V_5;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 1);
		int32_t L_28 = 1;
		WebSocketCommandInfo_set_CloseStatusCode_m1865797974(__this, ((int32_t)((int32_t)((int32_t)((int32_t)((L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_26)))*(int32_t)((int32_t)256)))+(int32_t)((L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28))))), /*hidden argument*/NULL);
		int32_t L_29 = V_2;
		V_10 = (bool)((((int32_t)((((int32_t)L_29) > ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_30 = V_10;
		if (L_30)
		{
			goto IL_00c4;
		}
	}
	{
		StringBuilder_t3822575854 * L_31 = V_4;
		WebSocketDataFrame_t222733806 * L_32 = V_3;
		NullCheck(L_32);
		ArraySegmentList_t1783467835 * L_33 = WebSocketDataFrame_get_InnerData_m815354334(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_34 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_35 = V_1;
		int32_t L_36 = V_2;
		NullCheck(L_33);
		String_t* L_37 = ArraySegmentList_Decode_m3168039495(L_33, L_34, ((int32_t)((int32_t)L_35+(int32_t)2)), ((int32_t)((int32_t)L_36-(int32_t)2)), /*hidden argument*/NULL);
		NullCheck(L_31);
		StringBuilder_Append_m3898090075(L_31, L_37, /*hidden argument*/NULL);
	}

IL_00c4:
	{
		goto IL_00f0;
	}

IL_00c7:
	{
		int32_t L_38 = V_2;
		V_10 = (bool)((((int32_t)((((int32_t)L_38) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_39 = V_10;
		if (L_39)
		{
			goto IL_00f0;
		}
	}
	{
		StringBuilder_t3822575854 * L_40 = V_4;
		WebSocketDataFrame_t222733806 * L_41 = V_3;
		NullCheck(L_41);
		ArraySegmentList_t1783467835 * L_42 = WebSocketDataFrame_get_InnerData_m815354334(L_41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_43 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_44 = V_1;
		int32_t L_45 = V_2;
		NullCheck(L_42);
		String_t* L_46 = ArraySegmentList_Decode_m3168039495(L_42, L_43, L_44, L_45, /*hidden argument*/NULL);
		NullCheck(L_40);
		StringBuilder_Append_m3898090075(L_40, L_46, /*hidden argument*/NULL);
	}

IL_00f0:
	{
		Il2CppObject* L_47 = ___frames;
		NullCheck(L_47);
		int32_t L_48 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<WebSocket4Net.Protocol.WebSocketDataFrame>::get_Count() */, ICollection_1_t688565192_il2cpp_TypeInfo_var, L_47);
		V_10 = (bool)((((int32_t)((((int32_t)L_48) > ((int32_t)1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_49 = V_10;
		if (L_49)
		{
			goto IL_0193;
		}
	}
	{
		V_6 = 1;
		goto IL_017f;
	}

IL_010b:
	{
		Il2CppObject* L_50 = ___frames;
		int32_t L_51 = V_6;
		NullCheck(L_50);
		WebSocketDataFrame_t222733806 * L_52 = InterfaceFuncInvoker1< WebSocketDataFrame_t222733806 *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<WebSocket4Net.Protocol.WebSocketDataFrame>::get_Item(System.Int32) */, IList_1_t2389226120_il2cpp_TypeInfo_var, L_50, L_51);
		V_7 = L_52;
		WebSocketDataFrame_t222733806 * L_53 = V_7;
		NullCheck(L_53);
		ArraySegmentList_t1783467835 * L_54 = WebSocketDataFrame_get_InnerData_m815354334(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		int32_t L_55 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_54);
		WebSocketDataFrame_t222733806 * L_56 = V_7;
		NullCheck(L_56);
		int64_t L_57 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_56, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_55-(int32_t)(((int32_t)((int32_t)L_57)))));
		WebSocketDataFrame_t222733806 * L_58 = V_7;
		NullCheck(L_58);
		int64_t L_59 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_58, /*hidden argument*/NULL);
		V_2 = (((int32_t)((int32_t)L_59)));
		WebSocketDataFrame_t222733806 * L_60 = V_7;
		NullCheck(L_60);
		bool L_61 = WebSocketDataFrame_get_HasMask_m399471176(L_60, /*hidden argument*/NULL);
		V_10 = (bool)((((int32_t)L_61) == ((int32_t)0))? 1 : 0);
		bool L_62 = V_10;
		if (L_62)
		{
			goto IL_015d;
		}
	}
	{
		WebSocketDataFrame_t222733806 * L_63 = V_7;
		NullCheck(L_63);
		ArraySegmentList_t1783467835 * L_64 = WebSocketDataFrame_get_InnerData_m815354334(L_63, /*hidden argument*/NULL);
		WebSocketDataFrame_t222733806 * L_65 = V_7;
		NullCheck(L_65);
		ByteU5BU5D_t58506160* L_66 = WebSocketDataFrame_get_MaskKey_m2158284327(L_65, /*hidden argument*/NULL);
		int32_t L_67 = V_1;
		int32_t L_68 = V_2;
		NullCheck(L_64);
		ArraySegmentList_DecodeMask_m3020502660(L_64, L_66, L_67, L_68, /*hidden argument*/NULL);
	}

IL_015d:
	{
		StringBuilder_t3822575854 * L_69 = V_4;
		WebSocketDataFrame_t222733806 * L_70 = V_7;
		NullCheck(L_70);
		ArraySegmentList_t1783467835 * L_71 = WebSocketDataFrame_get_InnerData_m815354334(L_70, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_72 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_73 = V_1;
		int32_t L_74 = V_2;
		NullCheck(L_71);
		String_t* L_75 = ArraySegmentList_Decode_m3168039495(L_71, L_72, L_73, L_74, /*hidden argument*/NULL);
		NullCheck(L_69);
		StringBuilder_Append_m3898090075(L_69, L_75, /*hidden argument*/NULL);
		int32_t L_76 = V_6;
		V_6 = ((int32_t)((int32_t)L_76+(int32_t)1));
	}

IL_017f:
	{
		int32_t L_77 = V_6;
		Il2CppObject* L_78 = ___frames;
		NullCheck(L_78);
		int32_t L_79 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<WebSocket4Net.Protocol.WebSocketDataFrame>::get_Count() */, ICollection_1_t688565192_il2cpp_TypeInfo_var, L_78);
		V_10 = (bool)((((int32_t)L_77) < ((int32_t)L_79))? 1 : 0);
		bool L_80 = V_10;
		if (L_80)
		{
			goto IL_010b;
		}
	}
	{
	}

IL_0193:
	{
		StringBuilder_t3822575854 * L_81 = V_4;
		NullCheck(L_81);
		String_t* L_82 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_81);
		WebSocketCommandInfo_set_Text_m425876161(__this, L_82, /*hidden argument*/NULL);
		goto IL_0317;
	}

IL_01a6:
	{
		int8_t L_83 = V_0;
		V_10 = (bool)((((int32_t)L_83) == ((int32_t)2))? 1 : 0);
		bool L_84 = V_10;
		if (L_84)
		{
			goto IL_025b;
		}
	}
	{
		StringBuilder_t3822575854 * L_85 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_85, /*hidden argument*/NULL);
		V_4 = L_85;
		V_6 = 0;
		goto IL_0234;
	}

IL_01c0:
	{
		Il2CppObject* L_86 = ___frames;
		int32_t L_87 = V_6;
		NullCheck(L_86);
		WebSocketDataFrame_t222733806 * L_88 = InterfaceFuncInvoker1< WebSocketDataFrame_t222733806 *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<WebSocket4Net.Protocol.WebSocketDataFrame>::get_Item(System.Int32) */, IList_1_t2389226120_il2cpp_TypeInfo_var, L_86, L_87);
		V_7 = L_88;
		WebSocketDataFrame_t222733806 * L_89 = V_7;
		NullCheck(L_89);
		ArraySegmentList_t1783467835 * L_90 = WebSocketDataFrame_get_InnerData_m815354334(L_89, /*hidden argument*/NULL);
		NullCheck(L_90);
		int32_t L_91 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_90);
		WebSocketDataFrame_t222733806 * L_92 = V_7;
		NullCheck(L_92);
		int64_t L_93 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_92, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_91-(int32_t)(((int32_t)((int32_t)L_93)))));
		WebSocketDataFrame_t222733806 * L_94 = V_7;
		NullCheck(L_94);
		int64_t L_95 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_94, /*hidden argument*/NULL);
		V_2 = (((int32_t)((int32_t)L_95)));
		WebSocketDataFrame_t222733806 * L_96 = V_7;
		NullCheck(L_96);
		bool L_97 = WebSocketDataFrame_get_HasMask_m399471176(L_96, /*hidden argument*/NULL);
		V_10 = (bool)((((int32_t)L_97) == ((int32_t)0))? 1 : 0);
		bool L_98 = V_10;
		if (L_98)
		{
			goto IL_0212;
		}
	}
	{
		WebSocketDataFrame_t222733806 * L_99 = V_7;
		NullCheck(L_99);
		ArraySegmentList_t1783467835 * L_100 = WebSocketDataFrame_get_InnerData_m815354334(L_99, /*hidden argument*/NULL);
		WebSocketDataFrame_t222733806 * L_101 = V_7;
		NullCheck(L_101);
		ByteU5BU5D_t58506160* L_102 = WebSocketDataFrame_get_MaskKey_m2158284327(L_101, /*hidden argument*/NULL);
		int32_t L_103 = V_1;
		int32_t L_104 = V_2;
		NullCheck(L_100);
		ArraySegmentList_DecodeMask_m3020502660(L_100, L_102, L_103, L_104, /*hidden argument*/NULL);
	}

IL_0212:
	{
		StringBuilder_t3822575854 * L_105 = V_4;
		WebSocketDataFrame_t222733806 * L_106 = V_7;
		NullCheck(L_106);
		ArraySegmentList_t1783467835 * L_107 = WebSocketDataFrame_get_InnerData_m815354334(L_106, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_108 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_109 = V_1;
		int32_t L_110 = V_2;
		NullCheck(L_107);
		String_t* L_111 = ArraySegmentList_Decode_m3168039495(L_107, L_108, L_109, L_110, /*hidden argument*/NULL);
		NullCheck(L_105);
		StringBuilder_Append_m3898090075(L_105, L_111, /*hidden argument*/NULL);
		int32_t L_112 = V_6;
		V_6 = ((int32_t)((int32_t)L_112+(int32_t)1));
	}

IL_0234:
	{
		int32_t L_113 = V_6;
		Il2CppObject* L_114 = ___frames;
		NullCheck(L_114);
		int32_t L_115 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<WebSocket4Net.Protocol.WebSocketDataFrame>::get_Count() */, ICollection_1_t688565192_il2cpp_TypeInfo_var, L_114);
		V_10 = (bool)((((int32_t)L_113) < ((int32_t)L_115))? 1 : 0);
		bool L_116 = V_10;
		if (L_116)
		{
			goto IL_01c0;
		}
	}
	{
		StringBuilder_t3822575854 * L_117 = V_4;
		NullCheck(L_117);
		String_t* L_118 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_117);
		WebSocketCommandInfo_set_Text_m425876161(__this, L_118, /*hidden argument*/NULL);
		goto IL_0316;
	}

IL_025b:
	{
		Il2CppObject* L_119 = ___frames;
		Func_2_t424480421 * L_120 = ((WebSocketCommandInfo_t3536916738_StaticFields*)WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4();
		G_B23_0 = L_119;
		if (L_120)
		{
			G_B24_0 = L_119;
			goto IL_0277;
		}
	}
	{
		IntPtr_t L_121;
		L_121.set_m_value_0((void*)WebSocketCommandInfo_U3C_ctorU3Eb__0_m1673743135_MethodInfo_var);
		Func_2_t424480421 * L_122 = (Func_2_t424480421 *)il2cpp_codegen_object_new(Func_2_t424480421_il2cpp_TypeInfo_var);
		Func_2__ctor_m4038159913(L_122, NULL, L_121, /*hidden argument*/Func_2__ctor_m4038159913_MethodInfo_var);
		((WebSocketCommandInfo_t3536916738_StaticFields*)WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var->static_fields)->set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4(L_122);
		G_B24_0 = G_B23_0;
		goto IL_0277;
	}

IL_0277:
	{
		Func_2_t424480421 * L_123 = ((WebSocketCommandInfo_t3536916738_StaticFields*)WebSocketCommandInfo_t3536916738_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4();
		int32_t L_124 = Enumerable_Sum_TisWebSocketDataFrame_t222733806_m828120080(NULL /*static, unused*/, G_B24_0, L_123, /*hidden argument*/Enumerable_Sum_TisWebSocketDataFrame_t222733806_m828120080_MethodInfo_var);
		V_8 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_124));
		V_9 = 0;
		V_6 = 0;
		goto IL_02fc;
	}

IL_0290:
	{
		Il2CppObject* L_125 = ___frames;
		int32_t L_126 = V_6;
		NullCheck(L_125);
		WebSocketDataFrame_t222733806 * L_127 = InterfaceFuncInvoker1< WebSocketDataFrame_t222733806 *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<WebSocket4Net.Protocol.WebSocketDataFrame>::get_Item(System.Int32) */, IList_1_t2389226120_il2cpp_TypeInfo_var, L_125, L_126);
		V_7 = L_127;
		WebSocketDataFrame_t222733806 * L_128 = V_7;
		NullCheck(L_128);
		ArraySegmentList_t1783467835 * L_129 = WebSocketDataFrame_get_InnerData_m815354334(L_128, /*hidden argument*/NULL);
		NullCheck(L_129);
		int32_t L_130 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_129);
		WebSocketDataFrame_t222733806 * L_131 = V_7;
		NullCheck(L_131);
		int64_t L_132 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_131, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_130-(int32_t)(((int32_t)((int32_t)L_132)))));
		WebSocketDataFrame_t222733806 * L_133 = V_7;
		NullCheck(L_133);
		int64_t L_134 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_133, /*hidden argument*/NULL);
		V_2 = (((int32_t)((int32_t)L_134)));
		WebSocketDataFrame_t222733806 * L_135 = V_7;
		NullCheck(L_135);
		bool L_136 = WebSocketDataFrame_get_HasMask_m399471176(L_135, /*hidden argument*/NULL);
		V_10 = (bool)((((int32_t)L_136) == ((int32_t)0))? 1 : 0);
		bool L_137 = V_10;
		if (L_137)
		{
			goto IL_02e2;
		}
	}
	{
		WebSocketDataFrame_t222733806 * L_138 = V_7;
		NullCheck(L_138);
		ArraySegmentList_t1783467835 * L_139 = WebSocketDataFrame_get_InnerData_m815354334(L_138, /*hidden argument*/NULL);
		WebSocketDataFrame_t222733806 * L_140 = V_7;
		NullCheck(L_140);
		ByteU5BU5D_t58506160* L_141 = WebSocketDataFrame_get_MaskKey_m2158284327(L_140, /*hidden argument*/NULL);
		int32_t L_142 = V_1;
		int32_t L_143 = V_2;
		NullCheck(L_139);
		ArraySegmentList_DecodeMask_m3020502660(L_139, L_141, L_142, L_143, /*hidden argument*/NULL);
	}

IL_02e2:
	{
		WebSocketDataFrame_t222733806 * L_144 = V_7;
		NullCheck(L_144);
		ArraySegmentList_t1783467835 * L_145 = WebSocketDataFrame_get_InnerData_m815354334(L_144, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_146 = V_8;
		int32_t L_147 = V_1;
		int32_t L_148 = V_9;
		int32_t L_149 = V_2;
		NullCheck(L_145);
		ArraySegmentList_1_CopyTo_m815842042(L_145, L_146, L_147, L_148, L_149, /*hidden argument*/ArraySegmentList_1_CopyTo_m815842042_MethodInfo_var);
		int32_t L_150 = V_6;
		V_6 = ((int32_t)((int32_t)L_150+(int32_t)1));
	}

IL_02fc:
	{
		int32_t L_151 = V_6;
		Il2CppObject* L_152 = ___frames;
		NullCheck(L_152);
		int32_t L_153 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<WebSocket4Net.Protocol.WebSocketDataFrame>::get_Count() */, ICollection_1_t688565192_il2cpp_TypeInfo_var, L_152);
		V_10 = (bool)((((int32_t)L_151) < ((int32_t)L_153))? 1 : 0);
		bool L_154 = V_10;
		if (L_154)
		{
			goto IL_0290;
		}
	}
	{
		ByteU5BU5D_t58506160* L_155 = V_8;
		WebSocketCommandInfo_set_Data_m2974951275(__this, L_155, /*hidden argument*/NULL);
	}

IL_0316:
	{
	}

IL_0317:
	{
		return;
	}
}
// System.Void WebSocket4Net.WebSocketCommandInfo::.ctor(WebSocket4Net.Protocol.WebSocketDataFrame)
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegmentList_1_ToArrayData_m509443740_MethodInfo_var;
extern const uint32_t WebSocketCommandInfo__ctor_m784170289_MetadataUsageId;
extern "C"  void WebSocketCommandInfo__ctor_m784170289 (WebSocketCommandInfo_t3536916738 * __this, WebSocketDataFrame_t222733806 * ___frame, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketCommandInfo__ctor_m784170289_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ByteU5BU5D_t58506160* V_2 = NULL;
	int8_t V_3 = 0x0;
	bool V_4 = false;
	int32_t G_B3_0 = 0;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		WebSocketDataFrame_t222733806 * L_0 = ___frame;
		NullCheck(L_0);
		int8_t L_1 = WebSocketDataFrame_get_OpCode_m3260433441(L_0, /*hidden argument*/NULL);
		V_3 = L_1;
		String_t* L_2 = SByte_ToString_m1290989501((&V_3), /*hidden argument*/NULL);
		WebSocketCommandInfo_set_Key_m3535408597(__this, L_2, /*hidden argument*/NULL);
		WebSocketDataFrame_t222733806 * L_3 = ___frame;
		NullCheck(L_3);
		int64_t L_4 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_3, /*hidden argument*/NULL);
		V_0 = (((int32_t)((int32_t)L_4)));
		WebSocketDataFrame_t222733806 * L_5 = ___frame;
		NullCheck(L_5);
		ArraySegmentList_t1783467835 * L_6 = WebSocketDataFrame_get_InnerData_m815354334(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count() */, L_6);
		WebSocketDataFrame_t222733806 * L_8 = ___frame;
		NullCheck(L_8);
		int64_t L_9 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_8, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_7-(int32_t)(((int32_t)((int32_t)L_9)))));
		WebSocketDataFrame_t222733806 * L_10 = ___frame;
		NullCheck(L_10);
		bool L_11 = WebSocketDataFrame_get_HasMask_m399471176(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_12 = V_0;
		G_B3_0 = ((((int32_t)((((int32_t)L_12) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_004b;
	}

IL_004a:
	{
		G_B3_0 = 1;
	}

IL_004b:
	{
		V_4 = (bool)G_B3_0;
		bool L_13 = V_4;
		if (L_13)
		{
			goto IL_0067;
		}
	}
	{
		WebSocketDataFrame_t222733806 * L_14 = ___frame;
		NullCheck(L_14);
		ArraySegmentList_t1783467835 * L_15 = WebSocketDataFrame_get_InnerData_m815354334(L_14, /*hidden argument*/NULL);
		WebSocketDataFrame_t222733806 * L_16 = ___frame;
		NullCheck(L_16);
		ByteU5BU5D_t58506160* L_17 = WebSocketDataFrame_get_MaskKey_m2158284327(L_16, /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		int32_t L_19 = V_0;
		NullCheck(L_15);
		ArraySegmentList_DecodeMask_m3020502660(L_15, L_17, L_18, L_19, /*hidden argument*/NULL);
	}

IL_0067:
	{
		WebSocketDataFrame_t222733806 * L_20 = ___frame;
		NullCheck(L_20);
		int8_t L_21 = WebSocketDataFrame_get_OpCode_m3260433441(L_20, /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)((((int32_t)L_21) == ((int32_t)8))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_22 = V_4;
		if (L_22)
		{
			goto IL_00e6;
		}
	}
	{
		int32_t L_23 = V_0;
		V_4 = (bool)((((int32_t)L_23) < ((int32_t)2))? 1 : 0);
		bool L_24 = V_4;
		if (L_24)
		{
			goto IL_00e5;
		}
	}
	{
		WebSocketDataFrame_t222733806 * L_25 = ___frame;
		NullCheck(L_25);
		ArraySegmentList_t1783467835 * L_26 = WebSocketDataFrame_get_InnerData_m815354334(L_25, /*hidden argument*/NULL);
		int32_t L_27 = V_1;
		NullCheck(L_26);
		ByteU5BU5D_t58506160* L_28 = ArraySegmentList_1_ToArrayData_m509443740(L_26, L_27, 2, /*hidden argument*/ArraySegmentList_1_ToArrayData_m509443740_MethodInfo_var);
		V_2 = L_28;
		ByteU5BU5D_t58506160* L_29 = V_2;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 0);
		int32_t L_30 = 0;
		ByteU5BU5D_t58506160* L_31 = V_2;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 1);
		int32_t L_32 = 1;
		WebSocketCommandInfo_set_CloseStatusCode_m1865797974(__this, ((int32_t)((int32_t)((int32_t)((int32_t)((L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_30)))*(int32_t)((int32_t)256)))+(int32_t)((L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32))))), /*hidden argument*/NULL);
		int32_t L_33 = V_0;
		V_4 = (bool)((((int32_t)((((int32_t)L_33) > ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_34 = V_4;
		if (L_34)
		{
			goto IL_00d5;
		}
	}
	{
		WebSocketDataFrame_t222733806 * L_35 = ___frame;
		NullCheck(L_35);
		ArraySegmentList_t1783467835 * L_36 = WebSocketDataFrame_get_InnerData_m815354334(L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_37 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_38 = V_1;
		int32_t L_39 = V_0;
		NullCheck(L_36);
		String_t* L_40 = ArraySegmentList_Decode_m3168039495(L_36, L_37, ((int32_t)((int32_t)L_38+(int32_t)2)), ((int32_t)((int32_t)L_39-(int32_t)2)), /*hidden argument*/NULL);
		WebSocketCommandInfo_set_Text_m425876161(__this, L_40, /*hidden argument*/NULL);
		goto IL_00e3;
	}

IL_00d5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		WebSocketCommandInfo_set_Text_m425876161(__this, L_41, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		goto IL_0160;
	}

IL_00e5:
	{
	}

IL_00e6:
	{
		WebSocketDataFrame_t222733806 * L_42 = ___frame;
		NullCheck(L_42);
		int8_t L_43 = WebSocketDataFrame_get_OpCode_m3260433441(L_42, /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_43) == ((int32_t)2))? 1 : 0);
		bool L_44 = V_4;
		if (L_44)
		{
			goto IL_012d;
		}
	}
	{
		int32_t L_45 = V_0;
		V_4 = (bool)((((int32_t)((((int32_t)L_45) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_46 = V_4;
		if (L_46)
		{
			goto IL_011e;
		}
	}
	{
		WebSocketDataFrame_t222733806 * L_47 = ___frame;
		NullCheck(L_47);
		ArraySegmentList_t1783467835 * L_48 = WebSocketDataFrame_get_InnerData_m815354334(L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_49 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_50 = V_1;
		int32_t L_51 = V_0;
		NullCheck(L_48);
		String_t* L_52 = ArraySegmentList_Decode_m3168039495(L_48, L_49, L_50, L_51, /*hidden argument*/NULL);
		WebSocketCommandInfo_set_Text_m425876161(__this, L_52, /*hidden argument*/NULL);
		goto IL_012a;
	}

IL_011e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		WebSocketCommandInfo_set_Text_m425876161(__this, L_53, /*hidden argument*/NULL);
	}

IL_012a:
	{
		goto IL_015f;
	}

IL_012d:
	{
		int32_t L_54 = V_0;
		V_4 = (bool)((((int32_t)((((int32_t)L_54) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_55 = V_4;
		if (L_55)
		{
			goto IL_0151;
		}
	}
	{
		WebSocketDataFrame_t222733806 * L_56 = ___frame;
		NullCheck(L_56);
		ArraySegmentList_t1783467835 * L_57 = WebSocketDataFrame_get_InnerData_m815354334(L_56, /*hidden argument*/NULL);
		int32_t L_58 = V_1;
		int32_t L_59 = V_0;
		NullCheck(L_57);
		ByteU5BU5D_t58506160* L_60 = ArraySegmentList_1_ToArrayData_m509443740(L_57, L_58, L_59, /*hidden argument*/ArraySegmentList_1_ToArrayData_m509443740_MethodInfo_var);
		WebSocketCommandInfo_set_Data_m2974951275(__this, L_60, /*hidden argument*/NULL);
		goto IL_015e;
	}

IL_0151:
	{
		WebSocketCommandInfo_set_Data_m2974951275(__this, ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_015e:
	{
	}

IL_015f:
	{
	}

IL_0160:
	{
		return;
	}
}
// System.String WebSocket4Net.WebSocketCommandInfo::get_Key()
extern "C"  String_t* WebSocketCommandInfo_get_Key_m2073169022 (WebSocketCommandInfo_t3536916738 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CKeyU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocketCommandInfo::set_Key(System.String)
extern "C"  void WebSocketCommandInfo_set_Key_m3535408597 (WebSocketCommandInfo_t3536916738 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CKeyU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Byte[] WebSocket4Net.WebSocketCommandInfo::get_Data()
extern "C"  ByteU5BU5D_t58506160* WebSocketCommandInfo_get_Data_m4129809812 (WebSocketCommandInfo_t3536916738 * __this, const MethodInfo* method)
{
	ByteU5BU5D_t58506160* V_0 = NULL;
	{
		ByteU5BU5D_t58506160* L_0 = __this->get_U3CDataU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		ByteU5BU5D_t58506160* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocketCommandInfo::set_Data(System.Byte[])
extern "C"  void WebSocketCommandInfo_set_Data_m2974951275 (WebSocketCommandInfo_t3536916738 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = ___value;
		__this->set_U3CDataU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String WebSocket4Net.WebSocketCommandInfo::get_Text()
extern "C"  String_t* WebSocketCommandInfo_get_Text_m101435856 (WebSocketCommandInfo_t3536916738 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CTextU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocketCommandInfo::set_Text(System.String)
extern "C"  void WebSocketCommandInfo_set_Text_m425876161 (WebSocketCommandInfo_t3536916738 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CTextU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Int32 WebSocket4Net.WebSocketCommandInfo::get_CloseStatusCode()
extern "C"  int32_t WebSocketCommandInfo_get_CloseStatusCode_m2849713255 (WebSocketCommandInfo_t3536916738 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CCloseStatusCodeU3Ek__BackingField_3();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void WebSocket4Net.WebSocketCommandInfo::set_CloseStatusCode(System.Int32)
extern "C"  void WebSocketCommandInfo_set_CloseStatusCode_m1865797974 (WebSocketCommandInfo_t3536916738 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CCloseStatusCodeU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 WebSocket4Net.WebSocketCommandInfo::<.ctor>b__0(WebSocket4Net.Protocol.WebSocketDataFrame)
extern "C"  int32_t WebSocketCommandInfo_U3C_ctorU3Eb__0_m1673743135 (Il2CppObject * __this /* static, unused */, WebSocketDataFrame_t222733806 * ___f, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		WebSocketDataFrame_t222733806 * L_0 = ___f;
		NullCheck(L_0);
		int64_t L_1 = WebSocketDataFrame_get_ActualPayloadLength_m2823861539(L_0, /*hidden argument*/NULL);
		V_0 = (((int32_t)((int32_t)L_1)));
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
