﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_DateTime339033936.h"

// System.TimeSpan SimpleJson.DateTimeUtils::GetUtcOffset(System.DateTime)
extern "C"  TimeSpan_t763862892  DateTimeUtils_GetUtcOffset_m816805979 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SimpleJson.DateTimeUtils::ToUniversalTicks(System.DateTime)
extern "C"  int64_t DateTimeUtils_ToUniversalTicks_m4026444844 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___dateTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SimpleJson.DateTimeUtils::ToUniversalTicks(System.DateTime,System.TimeSpan)
extern "C"  int64_t DateTimeUtils_ToUniversalTicks_m368464002 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___dateTime, TimeSpan_t763862892  ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SimpleJson.DateTimeUtils::ConvertDateTimeToJavaScriptTicks(System.DateTime)
extern "C"  int64_t DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m2628017042 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___dateTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SimpleJson.DateTimeUtils::ConvertDateTimeToJavaScriptTicks(System.DateTime,System.Boolean)
extern "C"  int64_t DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m2435522987 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___dateTime, bool ___convertToUtc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SimpleJson.DateTimeUtils::UniversialTicksToJavaScriptTicks(System.Int64)
extern "C"  int64_t DateTimeUtils_UniversialTicksToJavaScriptTicks_m2404214532 (Il2CppObject * __this /* static, unused */, int64_t ___universialTicks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime SimpleJson.DateTimeUtils::ConvertJavaScriptTicksToDateTime(System.Int64)
extern "C"  DateTime_t339033936  DateTimeUtils_ConvertJavaScriptTicksToDateTime_m4164648278 (Il2CppObject * __this /* static, unused */, int64_t ___javaScriptTicks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJson.DateTimeUtils::.cctor()
extern "C"  void DateTimeUtils__cctor_m2619914312 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
