﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.DraftHybi10DataReader
struct DraftHybi10DataReader_t1638928215;
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>
struct IClientCommandReader_1_t3175310917;
// SuperSocket.ClientEngine.Protocol.ArraySegmentList
struct ArraySegmentList_t1783467835;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;

#include "codegen/il2cpp-codegen.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Clie1783467835.h"

// System.Void WebSocket4Net.Protocol.DraftHybi10DataReader::.ctor()
extern "C"  void DraftHybi10DataReader__ctor_m1356719760 (DraftHybi10DataReader_t1638928215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo> WebSocket4Net.Protocol.DraftHybi10DataReader::get_NextCommandReader()
extern "C"  Il2CppObject* DraftHybi10DataReader_get_NextCommandReader_m1553728881 (DraftHybi10DataReader_t1638928215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi10DataReader::AddArraySegment(SuperSocket.ClientEngine.Protocol.ArraySegmentList,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern "C"  void DraftHybi10DataReader_AddArraySegment_m545309278 (DraftHybi10DataReader_t1638928215 * __this, ArraySegmentList_t1783467835 * ___segments, ByteU5BU5D_t58506160* ___buffer, int32_t ___offset, int32_t ___length, bool ___isReusableBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.WebSocketCommandInfo WebSocket4Net.Protocol.DraftHybi10DataReader::GetCommandInfo(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern "C"  WebSocketCommandInfo_t3536916738 * DraftHybi10DataReader_GetCommandInfo_m3786662742 (DraftHybi10DataReader_t1638928215 * __this, ByteU5BU5D_t58506160* ___readBuffer, int32_t ___offset, int32_t ___length, int32_t* ___left, const MethodInfo* method) IL2CPP_METHOD_ATTR;
