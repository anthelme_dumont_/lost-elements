﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.NavMeshAgent
struct NavMeshAgent_t3296505762;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MovementsController
struct  MovementsController_t463364160  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.NavMeshAgent MovementsController::agent
	NavMeshAgent_t3296505762 * ___agent_2;

public:
	inline static int32_t get_offset_of_agent_2() { return static_cast<int32_t>(offsetof(MovementsController_t463364160, ___agent_2)); }
	inline NavMeshAgent_t3296505762 * get_agent_2() const { return ___agent_2; }
	inline NavMeshAgent_t3296505762 ** get_address_of_agent_2() { return &___agent_2; }
	inline void set_agent_2(NavMeshAgent_t3296505762 * value)
	{
		___agent_2 = value;
		Il2CppCodeGenWriteBarrier(&___agent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
