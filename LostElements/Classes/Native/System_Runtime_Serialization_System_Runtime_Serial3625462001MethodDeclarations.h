﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.DataMemberAttribute
struct DataMemberAttribute_t3625462001;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.String System.Runtime.Serialization.DataMemberAttribute::get_Name()
extern "C"  String_t* DataMemberAttribute_get_Name_m2466648376 (DataMemberAttribute_t3625462001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
