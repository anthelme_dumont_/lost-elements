﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WebSocket4Net.Protocol.ICloseStatusCode
struct ICloseStatusCode_t28582816;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketVersion3981039553.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.ProtocolProcessorBase
struct  ProtocolProcessorBase_t894595261  : public Il2CppObject
{
public:
	// WebSocket4Net.Protocol.ICloseStatusCode WebSocket4Net.Protocol.ProtocolProcessorBase::<CloseStatusCode>k__BackingField
	Il2CppObject * ___U3CCloseStatusCodeU3Ek__BackingField_0;
	// WebSocket4Net.WebSocketVersion WebSocket4Net.Protocol.ProtocolProcessorBase::<Version>k__BackingField
	int32_t ___U3CVersionU3Ek__BackingField_1;
	// System.String WebSocket4Net.Protocol.ProtocolProcessorBase::<VersionTag>k__BackingField
	String_t* ___U3CVersionTagU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCloseStatusCodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProtocolProcessorBase_t894595261, ___U3CCloseStatusCodeU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CCloseStatusCodeU3Ek__BackingField_0() const { return ___U3CCloseStatusCodeU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CCloseStatusCodeU3Ek__BackingField_0() { return &___U3CCloseStatusCodeU3Ek__BackingField_0; }
	inline void set_U3CCloseStatusCodeU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CCloseStatusCodeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCloseStatusCodeU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProtocolProcessorBase_t894595261, ___U3CVersionU3Ek__BackingField_1)); }
	inline int32_t get_U3CVersionU3Ek__BackingField_1() const { return ___U3CVersionU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CVersionU3Ek__BackingField_1() { return &___U3CVersionU3Ek__BackingField_1; }
	inline void set_U3CVersionU3Ek__BackingField_1(int32_t value)
	{
		___U3CVersionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CVersionTagU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProtocolProcessorBase_t894595261, ___U3CVersionTagU3Ek__BackingField_2)); }
	inline String_t* get_U3CVersionTagU3Ek__BackingField_2() const { return ___U3CVersionTagU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CVersionTagU3Ek__BackingField_2() { return &___U3CVersionTagU3Ek__BackingField_2; }
	inline void set_U3CVersionTagU3Ek__BackingField_2(String_t* value)
	{
		___U3CVersionTagU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVersionTagU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
