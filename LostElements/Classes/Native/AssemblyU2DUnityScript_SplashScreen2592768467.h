﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Canvas
struct Canvas_t3534013893;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplashScreen
struct  SplashScreen_t2592768467  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Canvas SplashScreen::canva
	Canvas_t3534013893 * ___canva_2;

public:
	inline static int32_t get_offset_of_canva_2() { return static_cast<int32_t>(offsetof(SplashScreen_t2592768467, ___canva_2)); }
	inline Canvas_t3534013893 * get_canva_2() const { return ___canva_2; }
	inline Canvas_t3534013893 ** get_address_of_canva_2() { return &___canva_2; }
	inline void set_canva_2(Canvas_t3534013893 * value)
	{
		___canva_2 = value;
		Il2CppCodeGenWriteBarrier(&___canva_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
