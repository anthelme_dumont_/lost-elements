﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct List_1_t3660159136;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1745942128.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22863200167.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2584369076_gshared (Enumerator_t1745942128 * __this, List_1_t3660159136 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m2584369076(__this, ___l, method) ((  void (*) (Enumerator_t1745942128 *, List_1_t3660159136 *, const MethodInfo*))Enumerator__ctor_m2584369076_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2190588958_gshared (Enumerator_t1745942128 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2190588958(__this, method) ((  void (*) (Enumerator_t1745942128 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2190588958_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3007372554_gshared (Enumerator_t1745942128 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3007372554(__this, method) ((  Il2CppObject * (*) (Enumerator_t1745942128 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3007372554_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Dispose()
extern "C"  void Enumerator_Dispose_m1350636185_gshared (Enumerator_t1745942128 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1350636185(__this, method) ((  void (*) (Enumerator_t1745942128 *, const MethodInfo*))Enumerator_Dispose_m1350636185_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2538069330_gshared (Enumerator_t1745942128 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2538069330(__this, method) ((  void (*) (Enumerator_t1745942128 *, const MethodInfo*))Enumerator_VerifyState_m2538069330_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3587948426_gshared (Enumerator_t1745942128 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3587948426(__this, method) ((  bool (*) (Enumerator_t1745942128 *, const MethodInfo*))Enumerator_MoveNext_m3587948426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::get_Current()
extern "C"  KeyValuePair_2_t2863200167  Enumerator_get_Current_m1582713481_gshared (Enumerator_t1745942128 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1582713481(__this, method) ((  KeyValuePair_2_t2863200167  (*) (Enumerator_t1745942128 *, const MethodInfo*))Enumerator_get_Current_m1582713481_gshared)(__this, method)
