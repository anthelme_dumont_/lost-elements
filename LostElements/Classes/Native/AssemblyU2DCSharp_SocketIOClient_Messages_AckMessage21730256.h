﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.RegularExpressions.Regex
struct Regex_t3802381858;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t437523947;

#include "AssemblyU2DCSharp_SocketIOClient_Messages_Message957426777.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIOClient.Messages.AckMessage
struct  AckMessage_t21730256  : public Message_t957426777
{
public:
	// System.Action SocketIOClient.Messages.AckMessage::Callback
	Action_t437523947 * ___Callback_15;

public:
	inline static int32_t get_offset_of_Callback_15() { return static_cast<int32_t>(offsetof(AckMessage_t21730256, ___Callback_15)); }
	inline Action_t437523947 * get_Callback_15() const { return ___Callback_15; }
	inline Action_t437523947 ** get_address_of_Callback_15() { return &___Callback_15; }
	inline void set_Callback_15(Action_t437523947 * value)
	{
		___Callback_15 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_15, value);
	}
};

struct AckMessage_t21730256_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex SocketIOClient.Messages.AckMessage::reAckId
	Regex_t3802381858 * ___reAckId_10;
	// System.Text.RegularExpressions.Regex SocketIOClient.Messages.AckMessage::reAckPayload
	Regex_t3802381858 * ___reAckPayload_11;
	// System.Text.RegularExpressions.Regex SocketIOClient.Messages.AckMessage::reAckComplex
	Regex_t3802381858 * ___reAckComplex_12;
	// System.Object SocketIOClient.Messages.AckMessage::ackLock
	Il2CppObject * ___ackLock_13;
	// System.Int32 SocketIOClient.Messages.AckMessage::_akid
	int32_t ____akid_14;

public:
	inline static int32_t get_offset_of_reAckId_10() { return static_cast<int32_t>(offsetof(AckMessage_t21730256_StaticFields, ___reAckId_10)); }
	inline Regex_t3802381858 * get_reAckId_10() const { return ___reAckId_10; }
	inline Regex_t3802381858 ** get_address_of_reAckId_10() { return &___reAckId_10; }
	inline void set_reAckId_10(Regex_t3802381858 * value)
	{
		___reAckId_10 = value;
		Il2CppCodeGenWriteBarrier(&___reAckId_10, value);
	}

	inline static int32_t get_offset_of_reAckPayload_11() { return static_cast<int32_t>(offsetof(AckMessage_t21730256_StaticFields, ___reAckPayload_11)); }
	inline Regex_t3802381858 * get_reAckPayload_11() const { return ___reAckPayload_11; }
	inline Regex_t3802381858 ** get_address_of_reAckPayload_11() { return &___reAckPayload_11; }
	inline void set_reAckPayload_11(Regex_t3802381858 * value)
	{
		___reAckPayload_11 = value;
		Il2CppCodeGenWriteBarrier(&___reAckPayload_11, value);
	}

	inline static int32_t get_offset_of_reAckComplex_12() { return static_cast<int32_t>(offsetof(AckMessage_t21730256_StaticFields, ___reAckComplex_12)); }
	inline Regex_t3802381858 * get_reAckComplex_12() const { return ___reAckComplex_12; }
	inline Regex_t3802381858 ** get_address_of_reAckComplex_12() { return &___reAckComplex_12; }
	inline void set_reAckComplex_12(Regex_t3802381858 * value)
	{
		___reAckComplex_12 = value;
		Il2CppCodeGenWriteBarrier(&___reAckComplex_12, value);
	}

	inline static int32_t get_offset_of_ackLock_13() { return static_cast<int32_t>(offsetof(AckMessage_t21730256_StaticFields, ___ackLock_13)); }
	inline Il2CppObject * get_ackLock_13() const { return ___ackLock_13; }
	inline Il2CppObject ** get_address_of_ackLock_13() { return &___ackLock_13; }
	inline void set_ackLock_13(Il2CppObject * value)
	{
		___ackLock_13 = value;
		Il2CppCodeGenWriteBarrier(&___ackLock_13, value);
	}

	inline static int32_t get_offset_of__akid_14() { return static_cast<int32_t>(offsetof(AckMessage_t21730256_StaticFields, ____akid_14)); }
	inline int32_t get__akid_14() const { return ____akid_14; }
	inline int32_t* get_address_of__akid_14() { return &____akid_14; }
	inline void set__akid_14(int32_t value)
	{
		____akid_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
