﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Concurrent.ConcurrentQueue`1<System.ArraySegment`1<System.Byte>>
struct ConcurrentQueue_1_t2535351335;
// System.String
struct String_t;

#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn3100954378.h"
#include "mscorlib_System_Boolean211005341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.TcpClientSession
struct  TcpClientSession_t1301539049  : public ClientSession_t3100954378
{
public:
	// System.Boolean SuperSocket.ClientEngine.TcpClientSession::m_InConnecting
	bool ___m_InConnecting_11;
	// System.Collections.Concurrent.ConcurrentQueue`1<System.ArraySegment`1<System.Byte>> SuperSocket.ClientEngine.TcpClientSession::m_SendingQueue
	ConcurrentQueue_1_t2535351335 * ___m_SendingQueue_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) SuperSocket.ClientEngine.TcpClientSession::IsSending
	bool ___IsSending_13;
	// System.String SuperSocket.ClientEngine.TcpClientSession::<HostName>k__BackingField
	String_t* ___U3CHostNameU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_m_InConnecting_11() { return static_cast<int32_t>(offsetof(TcpClientSession_t1301539049, ___m_InConnecting_11)); }
	inline bool get_m_InConnecting_11() const { return ___m_InConnecting_11; }
	inline bool* get_address_of_m_InConnecting_11() { return &___m_InConnecting_11; }
	inline void set_m_InConnecting_11(bool value)
	{
		___m_InConnecting_11 = value;
	}

	inline static int32_t get_offset_of_m_SendingQueue_12() { return static_cast<int32_t>(offsetof(TcpClientSession_t1301539049, ___m_SendingQueue_12)); }
	inline ConcurrentQueue_1_t2535351335 * get_m_SendingQueue_12() const { return ___m_SendingQueue_12; }
	inline ConcurrentQueue_1_t2535351335 ** get_address_of_m_SendingQueue_12() { return &___m_SendingQueue_12; }
	inline void set_m_SendingQueue_12(ConcurrentQueue_1_t2535351335 * value)
	{
		___m_SendingQueue_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_SendingQueue_12, value);
	}

	inline static int32_t get_offset_of_IsSending_13() { return static_cast<int32_t>(offsetof(TcpClientSession_t1301539049, ___IsSending_13)); }
	inline bool get_IsSending_13() const { return ___IsSending_13; }
	inline bool* get_address_of_IsSending_13() { return &___IsSending_13; }
	inline void set_IsSending_13(bool value)
	{
		___IsSending_13 = value;
	}

	inline static int32_t get_offset_of_U3CHostNameU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(TcpClientSession_t1301539049, ___U3CHostNameU3Ek__BackingField_14)); }
	inline String_t* get_U3CHostNameU3Ek__BackingField_14() const { return ___U3CHostNameU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CHostNameU3Ek__BackingField_14() { return &___U3CHostNameU3Ek__BackingField_14; }
	inline void set_U3CHostNameU3Ek__BackingField_14(String_t* value)
	{
		___U3CHostNameU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHostNameU3Ek__BackingField_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
