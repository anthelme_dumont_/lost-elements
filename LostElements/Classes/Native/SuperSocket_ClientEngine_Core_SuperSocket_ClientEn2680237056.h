﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.Security.SslStream
struct SslStream_t2729491676;

#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1301539049.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.SslStreamTcpSession
struct  SslStreamTcpSession_t2680237056  : public TcpClientSession_t1301539049
{
public:
	// System.Net.Security.SslStream SuperSocket.ClientEngine.SslStreamTcpSession::m_SslStream
	SslStream_t2729491676 * ___m_SslStream_15;
	// System.Boolean SuperSocket.ClientEngine.SslStreamTcpSession::<AllowUnstrustedCertificate>k__BackingField
	bool ___U3CAllowUnstrustedCertificateU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_m_SslStream_15() { return static_cast<int32_t>(offsetof(SslStreamTcpSession_t2680237056, ___m_SslStream_15)); }
	inline SslStream_t2729491676 * get_m_SslStream_15() const { return ___m_SslStream_15; }
	inline SslStream_t2729491676 ** get_address_of_m_SslStream_15() { return &___m_SslStream_15; }
	inline void set_m_SslStream_15(SslStream_t2729491676 * value)
	{
		___m_SslStream_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_SslStream_15, value);
	}

	inline static int32_t get_offset_of_U3CAllowUnstrustedCertificateU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(SslStreamTcpSession_t2680237056, ___U3CAllowUnstrustedCertificateU3Ek__BackingField_16)); }
	inline bool get_U3CAllowUnstrustedCertificateU3Ek__BackingField_16() const { return ___U3CAllowUnstrustedCertificateU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CAllowUnstrustedCertificateU3Ek__BackingField_16() { return &___U3CAllowUnstrustedCertificateU3Ek__BackingField_16; }
	inline void set_U3CAllowUnstrustedCertificateU3Ek__BackingField_16(bool value)
	{
		___U3CAllowUnstrustedCertificateU3Ek__BackingField_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
