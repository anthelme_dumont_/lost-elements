﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2169094031.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22261603528.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.TimeZoneInfo/TimeType>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2087216049_gshared (InternalEnumerator_1_t2169094031 * __this, Il2CppArray * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2087216049(__this, ___array, method) ((  void (*) (InternalEnumerator_1_t2169094031 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2087216049_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.TimeZoneInfo/TimeType>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m62007887_gshared (InternalEnumerator_1_t2169094031 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m62007887(__this, method) ((  void (*) (InternalEnumerator_1_t2169094031 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m62007887_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.TimeZoneInfo/TimeType>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m297445819_gshared (InternalEnumerator_1_t2169094031 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m297445819(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2169094031 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m297445819_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.TimeZoneInfo/TimeType>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2251073224_gshared (InternalEnumerator_1_t2169094031 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2251073224(__this, method) ((  void (*) (InternalEnumerator_1_t2169094031 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2251073224_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.TimeZoneInfo/TimeType>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3002963387_gshared (InternalEnumerator_1_t2169094031 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3002963387(__this, method) ((  bool (*) (InternalEnumerator_1_t2169094031 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3002963387_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.TimeZoneInfo/TimeType>>::get_Current()
extern "C"  KeyValuePair_2_t2261603528  InternalEnumerator_1_get_Current_m1661516152_gshared (InternalEnumerator_1_t2169094031 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1661516152(__this, method) ((  KeyValuePair_2_t2261603528  (*) (InternalEnumerator_1_t2169094031 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1661516152_gshared)(__this, method)
