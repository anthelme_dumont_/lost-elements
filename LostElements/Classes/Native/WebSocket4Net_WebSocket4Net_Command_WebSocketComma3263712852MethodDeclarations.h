﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Command.WebSocketCommandBase
struct WebSocketCommandBase_t3263712852;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocket4Net.Command.WebSocketCommandBase::.ctor()
extern "C"  void WebSocketCommandBase__ctor_m3945612519 (WebSocketCommandBase_t3263712852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
