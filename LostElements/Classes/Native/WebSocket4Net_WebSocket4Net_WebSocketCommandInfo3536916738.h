﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Func`2<WebSocket4Net.Protocol.WebSocketDataFrame,System.Int32>
struct Func_2_t424480421;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.WebSocketCommandInfo
struct  WebSocketCommandInfo_t3536916738  : public Il2CppObject
{
public:
	// System.String WebSocket4Net.WebSocketCommandInfo::<Key>k__BackingField
	String_t* ___U3CKeyU3Ek__BackingField_0;
	// System.Byte[] WebSocket4Net.WebSocketCommandInfo::<Data>k__BackingField
	ByteU5BU5D_t58506160* ___U3CDataU3Ek__BackingField_1;
	// System.String WebSocket4Net.WebSocketCommandInfo::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_2;
	// System.Int32 WebSocket4Net.WebSocketCommandInfo::<CloseStatusCode>k__BackingField
	int32_t ___U3CCloseStatusCodeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CKeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WebSocketCommandInfo_t3536916738, ___U3CKeyU3Ek__BackingField_0)); }
	inline String_t* get_U3CKeyU3Ek__BackingField_0() const { return ___U3CKeyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CKeyU3Ek__BackingField_0() { return &___U3CKeyU3Ek__BackingField_0; }
	inline void set_U3CKeyU3Ek__BackingField_0(String_t* value)
	{
		___U3CKeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CKeyU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebSocketCommandInfo_t3536916738, ___U3CDataU3Ek__BackingField_1)); }
	inline ByteU5BU5D_t58506160* get_U3CDataU3Ek__BackingField_1() const { return ___U3CDataU3Ek__BackingField_1; }
	inline ByteU5BU5D_t58506160** get_address_of_U3CDataU3Ek__BackingField_1() { return &___U3CDataU3Ek__BackingField_1; }
	inline void set_U3CDataU3Ek__BackingField_1(ByteU5BU5D_t58506160* value)
	{
		___U3CDataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDataU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebSocketCommandInfo_t3536916738, ___U3CTextU3Ek__BackingField_2)); }
	inline String_t* get_U3CTextU3Ek__BackingField_2() const { return ___U3CTextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_2() { return &___U3CTextU3Ek__BackingField_2; }
	inline void set_U3CTextU3Ek__BackingField_2(String_t* value)
	{
		___U3CTextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTextU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CCloseStatusCodeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebSocketCommandInfo_t3536916738, ___U3CCloseStatusCodeU3Ek__BackingField_3)); }
	inline int32_t get_U3CCloseStatusCodeU3Ek__BackingField_3() const { return ___U3CCloseStatusCodeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CCloseStatusCodeU3Ek__BackingField_3() { return &___U3CCloseStatusCodeU3Ek__BackingField_3; }
	inline void set_U3CCloseStatusCodeU3Ek__BackingField_3(int32_t value)
	{
		___U3CCloseStatusCodeU3Ek__BackingField_3 = value;
	}
};

struct WebSocketCommandInfo_t3536916738_StaticFields
{
public:
	// System.Func`2<WebSocket4Net.Protocol.WebSocketDataFrame,System.Int32> WebSocket4Net.WebSocketCommandInfo::CS$<>9__CachedAnonymousMethodDelegate1
	Func_2_t424480421 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4() { return static_cast<int32_t>(offsetof(WebSocketCommandInfo_t3536916738_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4)); }
	inline Func_2_t424480421 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4; }
	inline Func_2_t424480421 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4(Func_2_t424480421 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
