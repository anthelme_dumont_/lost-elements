﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.DraftHybi00DataReader
struct DraftHybi00DataReader_t1509845496;
// WebSocket4Net.Protocol.ReaderBase
struct ReaderBase_t893022310;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_ReaderBase893022310.h"

// System.Void WebSocket4Net.Protocol.DraftHybi00DataReader::.ctor(WebSocket4Net.Protocol.ReaderBase)
extern "C"  void DraftHybi00DataReader__ctor_m2490528791 (DraftHybi00DataReader_t1509845496 * __this, ReaderBase_t893022310 * ___previousCommandReader, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.WebSocketCommandInfo WebSocket4Net.Protocol.DraftHybi00DataReader::GetCommandInfo(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern "C"  WebSocketCommandInfo_t3536916738 * DraftHybi00DataReader_GetCommandInfo_m976750997 (DraftHybi00DataReader_t1509845496 * __this, ByteU5BU5D_t58506160* ___readBuffer, int32_t ___offset, int32_t ___length, int32_t* ___left, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi00DataReader::Reset(System.Boolean)
extern "C"  void DraftHybi00DataReader_Reset_m222943283 (DraftHybi00DataReader_t1509845496 * __this, bool ___clearBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
