﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Queue`1<System.String>
struct Queue_1_t2676576442;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Concurrent.ConcurrentQueue`1<System.String>
struct  ConcurrentQueue_1_t702095371  : public Il2CppObject
{
public:
	// System.Collections.Generic.Queue`1<T> System.Collections.Concurrent.ConcurrentQueue`1::m_Queue
	Queue_1_t2676576442 * ___m_Queue_0;
	// System.Object System.Collections.Concurrent.ConcurrentQueue`1::m_SyncRoot
	Il2CppObject * ___m_SyncRoot_1;

public:
	inline static int32_t get_offset_of_m_Queue_0() { return static_cast<int32_t>(offsetof(ConcurrentQueue_1_t702095371, ___m_Queue_0)); }
	inline Queue_1_t2676576442 * get_m_Queue_0() const { return ___m_Queue_0; }
	inline Queue_1_t2676576442 ** get_address_of_m_Queue_0() { return &___m_Queue_0; }
	inline void set_m_Queue_0(Queue_1_t2676576442 * value)
	{
		___m_Queue_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Queue_0, value);
	}

	inline static int32_t get_offset_of_m_SyncRoot_1() { return static_cast<int32_t>(offsetof(ConcurrentQueue_1_t702095371, ___m_SyncRoot_1)); }
	inline Il2CppObject * get_m_SyncRoot_1() const { return ___m_SyncRoot_1; }
	inline Il2CppObject ** get_address_of_m_SyncRoot_1() { return &___m_SyncRoot_1; }
	inline void set_m_SyncRoot_1(Il2CppObject * value)
	{
		___m_SyncRoot_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_SyncRoot_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
