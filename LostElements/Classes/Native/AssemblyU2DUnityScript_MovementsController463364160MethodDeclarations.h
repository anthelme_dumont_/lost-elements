﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MovementsController
struct MovementsController_t463364160;

#include "codegen/il2cpp-codegen.h"

// System.Void MovementsController::.ctor()
extern "C"  void MovementsController__ctor_m1838486 (MovementsController_t463364160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovementsController::Start()
extern "C"  void MovementsController_Start_m3243943574 (MovementsController_t463364160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovementsController::FixedUpdate()
extern "C"  void MovementsController_FixedUpdate_m17752721 (MovementsController_t463364160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovementsController::Main()
extern "C"  void MovementsController_Main_m3379236967 (MovementsController_t463364160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
