﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.CloseStatusCodeHybi10
struct  CloseStatusCodeHybi10_t635449120  : public Il2CppObject
{
public:
	// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<NormalClosure>k__BackingField
	int32_t ___U3CNormalClosureU3Ek__BackingField_0;
	// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<GoingAway>k__BackingField
	int32_t ___U3CGoingAwayU3Ek__BackingField_1;
	// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<ProtocolError>k__BackingField
	int32_t ___U3CProtocolErrorU3Ek__BackingField_2;
	// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<NotAcceptableData>k__BackingField
	int32_t ___U3CNotAcceptableDataU3Ek__BackingField_3;
	// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<TooLargeFrame>k__BackingField
	int32_t ___U3CTooLargeFrameU3Ek__BackingField_4;
	// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<InvalidUTF8>k__BackingField
	int32_t ___U3CInvalidUTF8U3Ek__BackingField_5;
	// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<ViolatePolicy>k__BackingField
	int32_t ___U3CViolatePolicyU3Ek__BackingField_6;
	// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<ExtensionNotMatch>k__BackingField
	int32_t ___U3CExtensionNotMatchU3Ek__BackingField_7;
	// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<UnexpectedCondition>k__BackingField
	int32_t ___U3CUnexpectedConditionU3Ek__BackingField_8;
	// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<TLSHandshakeFailure>k__BackingField
	int32_t ___U3CTLSHandshakeFailureU3Ek__BackingField_9;
	// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::<NoStatusCode>k__BackingField
	int32_t ___U3CNoStatusCodeU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CNormalClosureU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_t635449120, ___U3CNormalClosureU3Ek__BackingField_0)); }
	inline int32_t get_U3CNormalClosureU3Ek__BackingField_0() const { return ___U3CNormalClosureU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CNormalClosureU3Ek__BackingField_0() { return &___U3CNormalClosureU3Ek__BackingField_0; }
	inline void set_U3CNormalClosureU3Ek__BackingField_0(int32_t value)
	{
		___U3CNormalClosureU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGoingAwayU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_t635449120, ___U3CGoingAwayU3Ek__BackingField_1)); }
	inline int32_t get_U3CGoingAwayU3Ek__BackingField_1() const { return ___U3CGoingAwayU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CGoingAwayU3Ek__BackingField_1() { return &___U3CGoingAwayU3Ek__BackingField_1; }
	inline void set_U3CGoingAwayU3Ek__BackingField_1(int32_t value)
	{
		___U3CGoingAwayU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CProtocolErrorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_t635449120, ___U3CProtocolErrorU3Ek__BackingField_2)); }
	inline int32_t get_U3CProtocolErrorU3Ek__BackingField_2() const { return ___U3CProtocolErrorU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CProtocolErrorU3Ek__BackingField_2() { return &___U3CProtocolErrorU3Ek__BackingField_2; }
	inline void set_U3CProtocolErrorU3Ek__BackingField_2(int32_t value)
	{
		___U3CProtocolErrorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CNotAcceptableDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_t635449120, ___U3CNotAcceptableDataU3Ek__BackingField_3)); }
	inline int32_t get_U3CNotAcceptableDataU3Ek__BackingField_3() const { return ___U3CNotAcceptableDataU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CNotAcceptableDataU3Ek__BackingField_3() { return &___U3CNotAcceptableDataU3Ek__BackingField_3; }
	inline void set_U3CNotAcceptableDataU3Ek__BackingField_3(int32_t value)
	{
		___U3CNotAcceptableDataU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CTooLargeFrameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_t635449120, ___U3CTooLargeFrameU3Ek__BackingField_4)); }
	inline int32_t get_U3CTooLargeFrameU3Ek__BackingField_4() const { return ___U3CTooLargeFrameU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CTooLargeFrameU3Ek__BackingField_4() { return &___U3CTooLargeFrameU3Ek__BackingField_4; }
	inline void set_U3CTooLargeFrameU3Ek__BackingField_4(int32_t value)
	{
		___U3CTooLargeFrameU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CInvalidUTF8U3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_t635449120, ___U3CInvalidUTF8U3Ek__BackingField_5)); }
	inline int32_t get_U3CInvalidUTF8U3Ek__BackingField_5() const { return ___U3CInvalidUTF8U3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CInvalidUTF8U3Ek__BackingField_5() { return &___U3CInvalidUTF8U3Ek__BackingField_5; }
	inline void set_U3CInvalidUTF8U3Ek__BackingField_5(int32_t value)
	{
		___U3CInvalidUTF8U3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CViolatePolicyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_t635449120, ___U3CViolatePolicyU3Ek__BackingField_6)); }
	inline int32_t get_U3CViolatePolicyU3Ek__BackingField_6() const { return ___U3CViolatePolicyU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CViolatePolicyU3Ek__BackingField_6() { return &___U3CViolatePolicyU3Ek__BackingField_6; }
	inline void set_U3CViolatePolicyU3Ek__BackingField_6(int32_t value)
	{
		___U3CViolatePolicyU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CExtensionNotMatchU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_t635449120, ___U3CExtensionNotMatchU3Ek__BackingField_7)); }
	inline int32_t get_U3CExtensionNotMatchU3Ek__BackingField_7() const { return ___U3CExtensionNotMatchU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CExtensionNotMatchU3Ek__BackingField_7() { return &___U3CExtensionNotMatchU3Ek__BackingField_7; }
	inline void set_U3CExtensionNotMatchU3Ek__BackingField_7(int32_t value)
	{
		___U3CExtensionNotMatchU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CUnexpectedConditionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_t635449120, ___U3CUnexpectedConditionU3Ek__BackingField_8)); }
	inline int32_t get_U3CUnexpectedConditionU3Ek__BackingField_8() const { return ___U3CUnexpectedConditionU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CUnexpectedConditionU3Ek__BackingField_8() { return &___U3CUnexpectedConditionU3Ek__BackingField_8; }
	inline void set_U3CUnexpectedConditionU3Ek__BackingField_8(int32_t value)
	{
		___U3CUnexpectedConditionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CTLSHandshakeFailureU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_t635449120, ___U3CTLSHandshakeFailureU3Ek__BackingField_9)); }
	inline int32_t get_U3CTLSHandshakeFailureU3Ek__BackingField_9() const { return ___U3CTLSHandshakeFailureU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CTLSHandshakeFailureU3Ek__BackingField_9() { return &___U3CTLSHandshakeFailureU3Ek__BackingField_9; }
	inline void set_U3CTLSHandshakeFailureU3Ek__BackingField_9(int32_t value)
	{
		___U3CTLSHandshakeFailureU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CNoStatusCodeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CloseStatusCodeHybi10_t635449120, ___U3CNoStatusCodeU3Ek__BackingField_10)); }
	inline int32_t get_U3CNoStatusCodeU3Ek__BackingField_10() const { return ___U3CNoStatusCodeU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CNoStatusCodeU3Ek__BackingField_10() { return &___U3CNoStatusCodeU3Ek__BackingField_10; }
	inline void set_U3CNoStatusCodeU3Ek__BackingField_10(int32_t value)
	{
		___U3CNoStatusCodeU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
