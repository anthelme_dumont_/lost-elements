﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct DefaultComparer_t2201153898;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22863200167.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::.ctor()
extern "C"  void DefaultComparer__ctor_m3521868299_gshared (DefaultComparer_t2201153898 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3521868299(__this, method) ((  void (*) (DefaultComparer_t2201153898 *, const MethodInfo*))DefaultComparer__ctor_m3521868299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m958060864_gshared (DefaultComparer_t2201153898 * __this, KeyValuePair_2_t2863200167  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m958060864(__this, ___obj, method) ((  int32_t (*) (DefaultComparer_t2201153898 *, KeyValuePair_2_t2863200167 , const MethodInfo*))DefaultComparer_GetHashCode_m958060864_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1578916444_gshared (DefaultComparer_t2201153898 * __this, KeyValuePair_2_t2863200167  ___x, KeyValuePair_2_t2863200167  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m1578916444(__this, ___x, ___y, method) ((  bool (*) (DefaultComparer_t2201153898 *, KeyValuePair_2_t2863200167 , KeyValuePair_2_t2863200167 , const MethodInfo*))DefaultComparer_Equals_m1578916444_gshared)(__this, ___x, ___y, method)
