﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4010202247(__this, ___l, method) ((  void (*) (Enumerator_t2553677508 *, List_1_t172927220 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m184108907(__this, method) ((  void (*) (Enumerator_t2553677508 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1108593559(__this, method) ((  Il2CppObject * (*) (Enumerator_t2553677508 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::Dispose()
#define Enumerator_Dispose_m569357356(__this, method) ((  void (*) (Enumerator_t2553677508 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::VerifyState()
#define Enumerator_VerifyState_m3868556645(__this, method) ((  void (*) (Enumerator_t2553677508 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::MoveNext()
#define Enumerator_MoveNext_m1746643543(__this, method) ((  bool (*) (Enumerator_t2553677508 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::get_Current()
#define Enumerator_get_Current_m1315895196(__this, method) ((  ArraySegmentEx_1_t3670935547 * (*) (Enumerator_t2553677508 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
