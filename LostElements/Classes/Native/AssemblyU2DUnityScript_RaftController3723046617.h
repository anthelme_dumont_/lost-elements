﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Renderer
struct Renderer_t1092684080;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RaftController
struct  RaftController_t3723046617  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject RaftController::water
	GameObject_t4012695102 * ___water_2;
	// System.Single RaftController::defaultYPosition
	float ___defaultYPosition_3;
	// UnityEngine.Renderer RaftController::waterRenderer
	Renderer_t1092684080 * ___waterRenderer_4;
	// System.Single RaftController::waterDefaultHeight
	float ___waterDefaultHeight_5;

public:
	inline static int32_t get_offset_of_water_2() { return static_cast<int32_t>(offsetof(RaftController_t3723046617, ___water_2)); }
	inline GameObject_t4012695102 * get_water_2() const { return ___water_2; }
	inline GameObject_t4012695102 ** get_address_of_water_2() { return &___water_2; }
	inline void set_water_2(GameObject_t4012695102 * value)
	{
		___water_2 = value;
		Il2CppCodeGenWriteBarrier(&___water_2, value);
	}

	inline static int32_t get_offset_of_defaultYPosition_3() { return static_cast<int32_t>(offsetof(RaftController_t3723046617, ___defaultYPosition_3)); }
	inline float get_defaultYPosition_3() const { return ___defaultYPosition_3; }
	inline float* get_address_of_defaultYPosition_3() { return &___defaultYPosition_3; }
	inline void set_defaultYPosition_3(float value)
	{
		___defaultYPosition_3 = value;
	}

	inline static int32_t get_offset_of_waterRenderer_4() { return static_cast<int32_t>(offsetof(RaftController_t3723046617, ___waterRenderer_4)); }
	inline Renderer_t1092684080 * get_waterRenderer_4() const { return ___waterRenderer_4; }
	inline Renderer_t1092684080 ** get_address_of_waterRenderer_4() { return &___waterRenderer_4; }
	inline void set_waterRenderer_4(Renderer_t1092684080 * value)
	{
		___waterRenderer_4 = value;
		Il2CppCodeGenWriteBarrier(&___waterRenderer_4, value);
	}

	inline static int32_t get_offset_of_waterDefaultHeight_5() { return static_cast<int32_t>(offsetof(RaftController_t3723046617, ___waterDefaultHeight_5)); }
	inline float get_waterDefaultHeight_5() const { return ___waterDefaultHeight_5; }
	inline float* get_address_of_waterDefaultHeight_5() { return &___waterDefaultHeight_5; }
	inline void set_waterDefaultHeight_5(float value)
	{
		___waterDefaultHeight_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
