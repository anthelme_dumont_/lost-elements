﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.SearchMarkState`1<System.Object>
struct  SearchMarkState_1_t2657612837  : public Il2CppObject
{
public:
	// T[] SuperSocket.ClientEngine.SearchMarkState`1::<Mark>k__BackingField
	ObjectU5BU5D_t11523773* ___U3CMarkU3Ek__BackingField_0;
	// System.Int32 SuperSocket.ClientEngine.SearchMarkState`1::<Matched>k__BackingField
	int32_t ___U3CMatchedU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMarkU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SearchMarkState_1_t2657612837, ___U3CMarkU3Ek__BackingField_0)); }
	inline ObjectU5BU5D_t11523773* get_U3CMarkU3Ek__BackingField_0() const { return ___U3CMarkU3Ek__BackingField_0; }
	inline ObjectU5BU5D_t11523773** get_address_of_U3CMarkU3Ek__BackingField_0() { return &___U3CMarkU3Ek__BackingField_0; }
	inline void set_U3CMarkU3Ek__BackingField_0(ObjectU5BU5D_t11523773* value)
	{
		___U3CMarkU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMarkU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CMatchedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SearchMarkState_1_t2657612837, ___U3CMatchedU3Ek__BackingField_1)); }
	inline int32_t get_U3CMatchedU3Ek__BackingField_1() const { return ___U3CMatchedU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CMatchedU3Ek__BackingField_1() { return &___U3CMatchedU3Ek__BackingField_1; }
	inline void set_U3CMatchedU3Ek__BackingField_1(int32_t value)
	{
		___U3CMatchedU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
