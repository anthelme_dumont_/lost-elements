﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.Protocol.ArraySegmentList
struct ArraySegmentList_t1783467835;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t180559927;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_Encoding180559927.h"

// System.String SuperSocket.ClientEngine.Protocol.ArraySegmentList::Decode(System.Text.Encoding)
extern "C"  String_t* ArraySegmentList_Decode_m4038946727 (ArraySegmentList_t1783467835 * __this, Encoding_t180559927 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SuperSocket.ClientEngine.Protocol.ArraySegmentList::Decode(System.Text.Encoding,System.Int32,System.Int32)
extern "C"  String_t* ArraySegmentList_Decode_m3168039495 (ArraySegmentList_t1783467835 * __this, Encoding_t180559927 * ___encoding, int32_t ___offset, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList::DecodeMask(System.Byte[],System.Int32,System.Int32)
extern "C"  void ArraySegmentList_DecodeMask_m3020502660 (ArraySegmentList_t1783467835 * __this, ByteU5BU5D_t58506160* ___mask, int32_t ___offset, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList::.ctor()
extern "C"  void ArraySegmentList__ctor_m3332344921 (ArraySegmentList_t1783467835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
