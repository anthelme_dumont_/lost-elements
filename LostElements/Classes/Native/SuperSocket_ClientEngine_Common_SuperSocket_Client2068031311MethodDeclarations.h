﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.ConnectAsyncExtension/ConnectToken
struct ConnectToken_t2068031311;
// System.Object
struct Il2CppObject;
// SuperSocket.ClientEngine.ConnectedCallback
struct ConnectedCallback_t1584521517;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client1584521517.h"

// System.Object SuperSocket.ClientEngine.ConnectAsyncExtension/ConnectToken::get_State()
extern "C"  Il2CppObject * ConnectToken_get_State_m3260382870 (ConnectToken_t2068031311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/ConnectToken::set_State(System.Object)
extern "C"  void ConnectToken_set_State_m3531158087 (ConnectToken_t2068031311 * __this, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SuperSocket.ClientEngine.ConnectedCallback SuperSocket.ClientEngine.ConnectAsyncExtension/ConnectToken::get_Callback()
extern "C"  ConnectedCallback_t1584521517 * ConnectToken_get_Callback_m655785259 (ConnectToken_t2068031311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/ConnectToken::set_Callback(SuperSocket.ClientEngine.ConnectedCallback)
extern "C"  void ConnectToken_set_Callback_m1292756542 (ConnectToken_t2068031311 * __this, ConnectedCallback_t1584521517 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/ConnectToken::.ctor()
extern "C"  void ConnectToken__ctor_m805112283 (ConnectToken_t2068031311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
