﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.DraftHybi00HandshakeReader
struct DraftHybi00HandshakeReader_t1765220979;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903.h"

// System.Void WebSocket4Net.Protocol.DraftHybi00HandshakeReader::.ctor(WebSocket4Net.WebSocket)
extern "C"  void DraftHybi00HandshakeReader__ctor_m2012279385 (DraftHybi00HandshakeReader_t1765220979 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi00HandshakeReader::SetDataReader()
extern "C"  void DraftHybi00HandshakeReader_SetDataReader_m522541629 (DraftHybi00HandshakeReader_t1765220979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.WebSocketCommandInfo WebSocket4Net.Protocol.DraftHybi00HandshakeReader::GetCommandInfo(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern "C"  WebSocketCommandInfo_t3536916738 * DraftHybi00HandshakeReader_GetCommandInfo_m2610356184 (DraftHybi00HandshakeReader_t1765220979 * __this, ByteU5BU5D_t58506160* ___readBuffer, int32_t ___offset, int32_t ___length, int32_t* ___left, const MethodInfo* method) IL2CPP_METHOD_ATTR;
