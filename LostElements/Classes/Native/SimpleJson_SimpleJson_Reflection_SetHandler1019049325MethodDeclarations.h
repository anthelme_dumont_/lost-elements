﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.Reflection.SetHandler
struct SetHandler_t1019049325;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SimpleJson.Reflection.SetHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SetHandler__ctor_m1978968818 (SetHandler_t1019049325 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJson.Reflection.SetHandler::Invoke(System.Object,System.Object)
extern "C"  void SetHandler_Invoke_m2910967830 (SetHandler_t1019049325 * __this, Il2CppObject * ___source, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SetHandler_t1019049325(Il2CppObject* delegate, Il2CppObject * ___source, Il2CppObject * ___value);
// System.IAsyncResult SimpleJson.Reflection.SetHandler::BeginInvoke(System.Object,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SetHandler_BeginInvoke_m362632603 (SetHandler_t1019049325 * __this, Il2CppObject * ___source, Il2CppObject * ___value, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJson.Reflection.SetHandler::EndInvoke(System.IAsyncResult)
extern "C"  void SetHandler_EndInvoke_m304571906 (SetHandler_t1019049325 * __this, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
