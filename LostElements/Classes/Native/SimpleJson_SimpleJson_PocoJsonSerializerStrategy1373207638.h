﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SimpleJson.Reflection.CacheResolver
struct CacheResolver_t2193954381;
// System.String[]
struct StringU5BU5D_t2956870243;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.PocoJsonSerializerStrategy
struct  PocoJsonSerializerStrategy_t1373207638  : public Il2CppObject
{
public:
	// SimpleJson.Reflection.CacheResolver SimpleJson.PocoJsonSerializerStrategy::CacheResolver
	CacheResolver_t2193954381 * ___CacheResolver_0;

public:
	inline static int32_t get_offset_of_CacheResolver_0() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t1373207638, ___CacheResolver_0)); }
	inline CacheResolver_t2193954381 * get_CacheResolver_0() const { return ___CacheResolver_0; }
	inline CacheResolver_t2193954381 ** get_address_of_CacheResolver_0() { return &___CacheResolver_0; }
	inline void set_CacheResolver_0(CacheResolver_t2193954381 * value)
	{
		___CacheResolver_0 = value;
		Il2CppCodeGenWriteBarrier(&___CacheResolver_0, value);
	}
};

struct PocoJsonSerializerStrategy_t1373207638_StaticFields
{
public:
	// System.String[] SimpleJson.PocoJsonSerializerStrategy::Iso8601Format
	StringU5BU5D_t2956870243* ___Iso8601Format_1;
	// System.DateTime SimpleJson.PocoJsonSerializerStrategy::_offsetDateTime
	DateTime_t339033936  ____offsetDateTime_2;

public:
	inline static int32_t get_offset_of_Iso8601Format_1() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t1373207638_StaticFields, ___Iso8601Format_1)); }
	inline StringU5BU5D_t2956870243* get_Iso8601Format_1() const { return ___Iso8601Format_1; }
	inline StringU5BU5D_t2956870243** get_address_of_Iso8601Format_1() { return &___Iso8601Format_1; }
	inline void set_Iso8601Format_1(StringU5BU5D_t2956870243* value)
	{
		___Iso8601Format_1 = value;
		Il2CppCodeGenWriteBarrier(&___Iso8601Format_1, value);
	}

	inline static int32_t get_offset_of__offsetDateTime_2() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t1373207638_StaticFields, ____offsetDateTime_2)); }
	inline DateTime_t339033936  get__offsetDateTime_2() const { return ____offsetDateTime_2; }
	inline DateTime_t339033936 * get_address_of__offsetDateTime_2() { return &____offsetDateTime_2; }
	inline void set__offsetDateTime_2(DateTime_t339033936  value)
	{
		____offsetDateTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
