﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.WebSocketDataFrame
struct WebSocketDataFrame_t222733806;
// SuperSocket.ClientEngine.Protocol.ArraySegmentList
struct ArraySegmentList_t1783467835;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Clie1783467835.h"

// SuperSocket.ClientEngine.Protocol.ArraySegmentList WebSocket4Net.Protocol.WebSocketDataFrame::get_InnerData()
extern "C"  ArraySegmentList_t1783467835 * WebSocketDataFrame_get_InnerData_m815354334 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.WebSocketDataFrame::.ctor(SuperSocket.ClientEngine.Protocol.ArraySegmentList)
extern "C"  void WebSocketDataFrame__ctor_m2101584072 (WebSocketDataFrame_t222733806 * __this, ArraySegmentList_t1783467835 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.Protocol.WebSocketDataFrame::get_FIN()
extern "C"  bool WebSocketDataFrame_get_FIN_m262651117 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte WebSocket4Net.Protocol.WebSocketDataFrame::get_OpCode()
extern "C"  int8_t WebSocketDataFrame_get_OpCode_m3260433441 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.Protocol.WebSocketDataFrame::get_HasMask()
extern "C"  bool WebSocketDataFrame_get_HasMask_m399471176 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte WebSocket4Net.Protocol.WebSocketDataFrame::get_PayloadLenght()
extern "C"  int8_t WebSocketDataFrame_get_PayloadLenght_m3291416379 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 WebSocket4Net.Protocol.WebSocketDataFrame::get_ActualPayloadLength()
extern "C"  int64_t WebSocketDataFrame_get_ActualPayloadLength_m2823861539 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocket4Net.Protocol.WebSocketDataFrame::get_MaskKey()
extern "C"  ByteU5BU5D_t58506160* WebSocketDataFrame_get_MaskKey_m2158284327 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.WebSocketDataFrame::set_MaskKey(System.Byte[])
extern "C"  void WebSocketDataFrame_set_MaskKey_m982527904 (WebSocketDataFrame_t222733806 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.WebSocketDataFrame::set_ExtensionData(System.Byte[])
extern "C"  void WebSocketDataFrame_set_ExtensionData_m3707031530 (WebSocketDataFrame_t222733806 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.WebSocketDataFrame::set_ApplicationData(System.Byte[])
extern "C"  void WebSocketDataFrame_set_ApplicationData_m1919509625 (WebSocketDataFrame_t222733806 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocket4Net.Protocol.WebSocketDataFrame::get_Length()
extern "C"  int32_t WebSocketDataFrame_get_Length_m3046518540 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.WebSocketDataFrame::Clear()
extern "C"  void WebSocketDataFrame_Clear_m2483472352 (WebSocketDataFrame_t222733806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
