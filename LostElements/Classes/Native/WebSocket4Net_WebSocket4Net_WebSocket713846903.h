﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>
struct Dictionary_2_t2262919787;
// WebSocket4Net.Protocol.ProtocolProcessorFactory
struct ProtocolProcessorFactory_t3499308290;
// System.Threading.Timer
struct Timer_t3546110984;
// System.EventHandler
struct EventHandler_t247020293;
// System.EventHandler`1<WebSocket4Net.MessageReceivedEventArgs>
struct EventHandler_1_t419508266;
// System.EventHandler`1<WebSocket4Net.DataReceivedEventArgs>
struct EventHandler_1_t2821670567;
// System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>
struct EventHandler_1_t2171844441;
// SuperSocket.ClientEngine.TcpClientSession
struct TcpClientSession_t1301539049;
// WebSocket4Net.Protocol.IProtocolProcessor
struct IProtocolProcessor_t4208156067;
// System.Uri
struct Uri_t2776692961;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t3650470111;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct List_1_t2891677073;
// SuperSocket.ClientEngine.IProxyConnector
struct IProxyConnector_t1374585095;
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>
struct IClientCommandReader_1_t3175310917;

#include "mscorlib_System_Object837106420.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketVersion3981039553.h"
#include "mscorlib_System_DateTime339033936.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketState1654176186.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.WebSocket
struct  WebSocket_t713846903  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>> WebSocket4Net.WebSocket::m_CommandDict
	Dictionary_2_t2262919787 * ___m_CommandDict_7;
	// System.Threading.Timer WebSocket4Net.WebSocket::m_PingTimer
	Timer_t3546110984 * ___m_PingTimer_9;
	// System.String WebSocket4Net.WebSocket::m_LastPingRequest
	String_t* ___m_LastPingRequest_10;
	// System.EventHandler WebSocket4Net.WebSocket::m_Opened
	EventHandler_t247020293 * ___m_Opened_11;
	// System.EventHandler`1<WebSocket4Net.MessageReceivedEventArgs> WebSocket4Net.WebSocket::m_MessageReceived
	EventHandler_1_t419508266 * ___m_MessageReceived_12;
	// System.EventHandler`1<WebSocket4Net.DataReceivedEventArgs> WebSocket4Net.WebSocket::m_DataReceived
	EventHandler_1_t2821670567 * ___m_DataReceived_13;
	// System.EventHandler WebSocket4Net.WebSocket::m_Closed
	EventHandler_t247020293 * ___m_Closed_14;
	// System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs> WebSocket4Net.WebSocket::m_Error
	EventHandler_1_t2171844441 * ___m_Error_15;
	// System.Boolean WebSocket4Net.WebSocket::m_AllowUnstrustedCertificate
	bool ___m_AllowUnstrustedCertificate_16;
	// SuperSocket.ClientEngine.TcpClientSession WebSocket4Net.WebSocket::<Client>k__BackingField
	TcpClientSession_t1301539049 * ___U3CClientU3Ek__BackingField_17;
	// WebSocket4Net.WebSocketVersion WebSocket4Net.WebSocket::<Version>k__BackingField
	int32_t ___U3CVersionU3Ek__BackingField_18;
	// System.DateTime WebSocket4Net.WebSocket::<LastActiveTime>k__BackingField
	DateTime_t339033936  ___U3CLastActiveTimeU3Ek__BackingField_19;
	// System.Boolean WebSocket4Net.WebSocket::<EnableAutoSendPing>k__BackingField
	bool ___U3CEnableAutoSendPingU3Ek__BackingField_20;
	// System.Int32 WebSocket4Net.WebSocket::<AutoSendPingInterval>k__BackingField
	int32_t ___U3CAutoSendPingIntervalU3Ek__BackingField_21;
	// WebSocket4Net.Protocol.IProtocolProcessor WebSocket4Net.WebSocket::<ProtocolProcessor>k__BackingField
	Il2CppObject * ___U3CProtocolProcessorU3Ek__BackingField_22;
	// System.Uri WebSocket4Net.WebSocket::<TargetUri>k__BackingField
	Uri_t2776692961 * ___U3CTargetUriU3Ek__BackingField_23;
	// System.String WebSocket4Net.WebSocket::<SubProtocol>k__BackingField
	String_t* ___U3CSubProtocolU3Ek__BackingField_24;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> WebSocket4Net.WebSocket::<Items>k__BackingField
	Il2CppObject* ___U3CItemsU3Ek__BackingField_25;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> WebSocket4Net.WebSocket::<Cookies>k__BackingField
	List_1_t2891677073 * ___U3CCookiesU3Ek__BackingField_26;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> WebSocket4Net.WebSocket::<CustomHeaderItems>k__BackingField
	List_1_t2891677073 * ___U3CCustomHeaderItemsU3Ek__BackingField_27;
	// WebSocket4Net.WebSocketState WebSocket4Net.WebSocket::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_28;
	// System.Boolean WebSocket4Net.WebSocket::<Handshaked>k__BackingField
	bool ___U3CHandshakedU3Ek__BackingField_29;
	// SuperSocket.ClientEngine.IProxyConnector WebSocket4Net.WebSocket::<Proxy>k__BackingField
	Il2CppObject * ___U3CProxyU3Ek__BackingField_30;
	// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo> WebSocket4Net.WebSocket::<CommandReader>k__BackingField
	Il2CppObject* ___U3CCommandReaderU3Ek__BackingField_31;
	// System.Boolean WebSocket4Net.WebSocket::<NotSpecifiedVersion>k__BackingField
	bool ___U3CNotSpecifiedVersionU3Ek__BackingField_32;
	// System.String WebSocket4Net.WebSocket::<LastPongResponse>k__BackingField
	String_t* ___U3CLastPongResponseU3Ek__BackingField_33;
	// System.String WebSocket4Net.WebSocket::<HandshakeHost>k__BackingField
	String_t* ___U3CHandshakeHostU3Ek__BackingField_34;
	// System.String WebSocket4Net.WebSocket::<Origin>k__BackingField
	String_t* ___U3COriginU3Ek__BackingField_35;

public:
	inline static int32_t get_offset_of_m_CommandDict_7() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___m_CommandDict_7)); }
	inline Dictionary_2_t2262919787 * get_m_CommandDict_7() const { return ___m_CommandDict_7; }
	inline Dictionary_2_t2262919787 ** get_address_of_m_CommandDict_7() { return &___m_CommandDict_7; }
	inline void set_m_CommandDict_7(Dictionary_2_t2262919787 * value)
	{
		___m_CommandDict_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_CommandDict_7, value);
	}

	inline static int32_t get_offset_of_m_PingTimer_9() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___m_PingTimer_9)); }
	inline Timer_t3546110984 * get_m_PingTimer_9() const { return ___m_PingTimer_9; }
	inline Timer_t3546110984 ** get_address_of_m_PingTimer_9() { return &___m_PingTimer_9; }
	inline void set_m_PingTimer_9(Timer_t3546110984 * value)
	{
		___m_PingTimer_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_PingTimer_9, value);
	}

	inline static int32_t get_offset_of_m_LastPingRequest_10() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___m_LastPingRequest_10)); }
	inline String_t* get_m_LastPingRequest_10() const { return ___m_LastPingRequest_10; }
	inline String_t** get_address_of_m_LastPingRequest_10() { return &___m_LastPingRequest_10; }
	inline void set_m_LastPingRequest_10(String_t* value)
	{
		___m_LastPingRequest_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_LastPingRequest_10, value);
	}

	inline static int32_t get_offset_of_m_Opened_11() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___m_Opened_11)); }
	inline EventHandler_t247020293 * get_m_Opened_11() const { return ___m_Opened_11; }
	inline EventHandler_t247020293 ** get_address_of_m_Opened_11() { return &___m_Opened_11; }
	inline void set_m_Opened_11(EventHandler_t247020293 * value)
	{
		___m_Opened_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_Opened_11, value);
	}

	inline static int32_t get_offset_of_m_MessageReceived_12() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___m_MessageReceived_12)); }
	inline EventHandler_1_t419508266 * get_m_MessageReceived_12() const { return ___m_MessageReceived_12; }
	inline EventHandler_1_t419508266 ** get_address_of_m_MessageReceived_12() { return &___m_MessageReceived_12; }
	inline void set_m_MessageReceived_12(EventHandler_1_t419508266 * value)
	{
		___m_MessageReceived_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_MessageReceived_12, value);
	}

	inline static int32_t get_offset_of_m_DataReceived_13() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___m_DataReceived_13)); }
	inline EventHandler_1_t2821670567 * get_m_DataReceived_13() const { return ___m_DataReceived_13; }
	inline EventHandler_1_t2821670567 ** get_address_of_m_DataReceived_13() { return &___m_DataReceived_13; }
	inline void set_m_DataReceived_13(EventHandler_1_t2821670567 * value)
	{
		___m_DataReceived_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_DataReceived_13, value);
	}

	inline static int32_t get_offset_of_m_Closed_14() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___m_Closed_14)); }
	inline EventHandler_t247020293 * get_m_Closed_14() const { return ___m_Closed_14; }
	inline EventHandler_t247020293 ** get_address_of_m_Closed_14() { return &___m_Closed_14; }
	inline void set_m_Closed_14(EventHandler_t247020293 * value)
	{
		___m_Closed_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_Closed_14, value);
	}

	inline static int32_t get_offset_of_m_Error_15() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___m_Error_15)); }
	inline EventHandler_1_t2171844441 * get_m_Error_15() const { return ___m_Error_15; }
	inline EventHandler_1_t2171844441 ** get_address_of_m_Error_15() { return &___m_Error_15; }
	inline void set_m_Error_15(EventHandler_1_t2171844441 * value)
	{
		___m_Error_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_Error_15, value);
	}

	inline static int32_t get_offset_of_m_AllowUnstrustedCertificate_16() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___m_AllowUnstrustedCertificate_16)); }
	inline bool get_m_AllowUnstrustedCertificate_16() const { return ___m_AllowUnstrustedCertificate_16; }
	inline bool* get_address_of_m_AllowUnstrustedCertificate_16() { return &___m_AllowUnstrustedCertificate_16; }
	inline void set_m_AllowUnstrustedCertificate_16(bool value)
	{
		___m_AllowUnstrustedCertificate_16 = value;
	}

	inline static int32_t get_offset_of_U3CClientU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CClientU3Ek__BackingField_17)); }
	inline TcpClientSession_t1301539049 * get_U3CClientU3Ek__BackingField_17() const { return ___U3CClientU3Ek__BackingField_17; }
	inline TcpClientSession_t1301539049 ** get_address_of_U3CClientU3Ek__BackingField_17() { return &___U3CClientU3Ek__BackingField_17; }
	inline void set_U3CClientU3Ek__BackingField_17(TcpClientSession_t1301539049 * value)
	{
		___U3CClientU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CClientU3Ek__BackingField_17, value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CVersionU3Ek__BackingField_18)); }
	inline int32_t get_U3CVersionU3Ek__BackingField_18() const { return ___U3CVersionU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CVersionU3Ek__BackingField_18() { return &___U3CVersionU3Ek__BackingField_18; }
	inline void set_U3CVersionU3Ek__BackingField_18(int32_t value)
	{
		___U3CVersionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CLastActiveTimeU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CLastActiveTimeU3Ek__BackingField_19)); }
	inline DateTime_t339033936  get_U3CLastActiveTimeU3Ek__BackingField_19() const { return ___U3CLastActiveTimeU3Ek__BackingField_19; }
	inline DateTime_t339033936 * get_address_of_U3CLastActiveTimeU3Ek__BackingField_19() { return &___U3CLastActiveTimeU3Ek__BackingField_19; }
	inline void set_U3CLastActiveTimeU3Ek__BackingField_19(DateTime_t339033936  value)
	{
		___U3CLastActiveTimeU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CEnableAutoSendPingU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CEnableAutoSendPingU3Ek__BackingField_20)); }
	inline bool get_U3CEnableAutoSendPingU3Ek__BackingField_20() const { return ___U3CEnableAutoSendPingU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CEnableAutoSendPingU3Ek__BackingField_20() { return &___U3CEnableAutoSendPingU3Ek__BackingField_20; }
	inline void set_U3CEnableAutoSendPingU3Ek__BackingField_20(bool value)
	{
		___U3CEnableAutoSendPingU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CAutoSendPingIntervalU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CAutoSendPingIntervalU3Ek__BackingField_21)); }
	inline int32_t get_U3CAutoSendPingIntervalU3Ek__BackingField_21() const { return ___U3CAutoSendPingIntervalU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CAutoSendPingIntervalU3Ek__BackingField_21() { return &___U3CAutoSendPingIntervalU3Ek__BackingField_21; }
	inline void set_U3CAutoSendPingIntervalU3Ek__BackingField_21(int32_t value)
	{
		___U3CAutoSendPingIntervalU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CProtocolProcessorU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CProtocolProcessorU3Ek__BackingField_22)); }
	inline Il2CppObject * get_U3CProtocolProcessorU3Ek__BackingField_22() const { return ___U3CProtocolProcessorU3Ek__BackingField_22; }
	inline Il2CppObject ** get_address_of_U3CProtocolProcessorU3Ek__BackingField_22() { return &___U3CProtocolProcessorU3Ek__BackingField_22; }
	inline void set_U3CProtocolProcessorU3Ek__BackingField_22(Il2CppObject * value)
	{
		___U3CProtocolProcessorU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProtocolProcessorU3Ek__BackingField_22, value);
	}

	inline static int32_t get_offset_of_U3CTargetUriU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CTargetUriU3Ek__BackingField_23)); }
	inline Uri_t2776692961 * get_U3CTargetUriU3Ek__BackingField_23() const { return ___U3CTargetUriU3Ek__BackingField_23; }
	inline Uri_t2776692961 ** get_address_of_U3CTargetUriU3Ek__BackingField_23() { return &___U3CTargetUriU3Ek__BackingField_23; }
	inline void set_U3CTargetUriU3Ek__BackingField_23(Uri_t2776692961 * value)
	{
		___U3CTargetUriU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTargetUriU3Ek__BackingField_23, value);
	}

	inline static int32_t get_offset_of_U3CSubProtocolU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CSubProtocolU3Ek__BackingField_24)); }
	inline String_t* get_U3CSubProtocolU3Ek__BackingField_24() const { return ___U3CSubProtocolU3Ek__BackingField_24; }
	inline String_t** get_address_of_U3CSubProtocolU3Ek__BackingField_24() { return &___U3CSubProtocolU3Ek__BackingField_24; }
	inline void set_U3CSubProtocolU3Ek__BackingField_24(String_t* value)
	{
		___U3CSubProtocolU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSubProtocolU3Ek__BackingField_24, value);
	}

	inline static int32_t get_offset_of_U3CItemsU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CItemsU3Ek__BackingField_25)); }
	inline Il2CppObject* get_U3CItemsU3Ek__BackingField_25() const { return ___U3CItemsU3Ek__BackingField_25; }
	inline Il2CppObject** get_address_of_U3CItemsU3Ek__BackingField_25() { return &___U3CItemsU3Ek__BackingField_25; }
	inline void set_U3CItemsU3Ek__BackingField_25(Il2CppObject* value)
	{
		___U3CItemsU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemsU3Ek__BackingField_25, value);
	}

	inline static int32_t get_offset_of_U3CCookiesU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CCookiesU3Ek__BackingField_26)); }
	inline List_1_t2891677073 * get_U3CCookiesU3Ek__BackingField_26() const { return ___U3CCookiesU3Ek__BackingField_26; }
	inline List_1_t2891677073 ** get_address_of_U3CCookiesU3Ek__BackingField_26() { return &___U3CCookiesU3Ek__BackingField_26; }
	inline void set_U3CCookiesU3Ek__BackingField_26(List_1_t2891677073 * value)
	{
		___U3CCookiesU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCookiesU3Ek__BackingField_26, value);
	}

	inline static int32_t get_offset_of_U3CCustomHeaderItemsU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CCustomHeaderItemsU3Ek__BackingField_27)); }
	inline List_1_t2891677073 * get_U3CCustomHeaderItemsU3Ek__BackingField_27() const { return ___U3CCustomHeaderItemsU3Ek__BackingField_27; }
	inline List_1_t2891677073 ** get_address_of_U3CCustomHeaderItemsU3Ek__BackingField_27() { return &___U3CCustomHeaderItemsU3Ek__BackingField_27; }
	inline void set_U3CCustomHeaderItemsU3Ek__BackingField_27(List_1_t2891677073 * value)
	{
		___U3CCustomHeaderItemsU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCustomHeaderItemsU3Ek__BackingField_27, value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CStateU3Ek__BackingField_28)); }
	inline int32_t get_U3CStateU3Ek__BackingField_28() const { return ___U3CStateU3Ek__BackingField_28; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_28() { return &___U3CStateU3Ek__BackingField_28; }
	inline void set_U3CStateU3Ek__BackingField_28(int32_t value)
	{
		___U3CStateU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CHandshakedU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CHandshakedU3Ek__BackingField_29)); }
	inline bool get_U3CHandshakedU3Ek__BackingField_29() const { return ___U3CHandshakedU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CHandshakedU3Ek__BackingField_29() { return &___U3CHandshakedU3Ek__BackingField_29; }
	inline void set_U3CHandshakedU3Ek__BackingField_29(bool value)
	{
		___U3CHandshakedU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CProxyU3Ek__BackingField_30)); }
	inline Il2CppObject * get_U3CProxyU3Ek__BackingField_30() const { return ___U3CProxyU3Ek__BackingField_30; }
	inline Il2CppObject ** get_address_of_U3CProxyU3Ek__BackingField_30() { return &___U3CProxyU3Ek__BackingField_30; }
	inline void set_U3CProxyU3Ek__BackingField_30(Il2CppObject * value)
	{
		___U3CProxyU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProxyU3Ek__BackingField_30, value);
	}

	inline static int32_t get_offset_of_U3CCommandReaderU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CCommandReaderU3Ek__BackingField_31)); }
	inline Il2CppObject* get_U3CCommandReaderU3Ek__BackingField_31() const { return ___U3CCommandReaderU3Ek__BackingField_31; }
	inline Il2CppObject** get_address_of_U3CCommandReaderU3Ek__BackingField_31() { return &___U3CCommandReaderU3Ek__BackingField_31; }
	inline void set_U3CCommandReaderU3Ek__BackingField_31(Il2CppObject* value)
	{
		___U3CCommandReaderU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCommandReaderU3Ek__BackingField_31, value);
	}

	inline static int32_t get_offset_of_U3CNotSpecifiedVersionU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CNotSpecifiedVersionU3Ek__BackingField_32)); }
	inline bool get_U3CNotSpecifiedVersionU3Ek__BackingField_32() const { return ___U3CNotSpecifiedVersionU3Ek__BackingField_32; }
	inline bool* get_address_of_U3CNotSpecifiedVersionU3Ek__BackingField_32() { return &___U3CNotSpecifiedVersionU3Ek__BackingField_32; }
	inline void set_U3CNotSpecifiedVersionU3Ek__BackingField_32(bool value)
	{
		___U3CNotSpecifiedVersionU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CLastPongResponseU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CLastPongResponseU3Ek__BackingField_33)); }
	inline String_t* get_U3CLastPongResponseU3Ek__BackingField_33() const { return ___U3CLastPongResponseU3Ek__BackingField_33; }
	inline String_t** get_address_of_U3CLastPongResponseU3Ek__BackingField_33() { return &___U3CLastPongResponseU3Ek__BackingField_33; }
	inline void set_U3CLastPongResponseU3Ek__BackingField_33(String_t* value)
	{
		___U3CLastPongResponseU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLastPongResponseU3Ek__BackingField_33, value);
	}

	inline static int32_t get_offset_of_U3CHandshakeHostU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3CHandshakeHostU3Ek__BackingField_34)); }
	inline String_t* get_U3CHandshakeHostU3Ek__BackingField_34() const { return ___U3CHandshakeHostU3Ek__BackingField_34; }
	inline String_t** get_address_of_U3CHandshakeHostU3Ek__BackingField_34() { return &___U3CHandshakeHostU3Ek__BackingField_34; }
	inline void set_U3CHandshakeHostU3Ek__BackingField_34(String_t* value)
	{
		___U3CHandshakeHostU3Ek__BackingField_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHandshakeHostU3Ek__BackingField_34, value);
	}

	inline static int32_t get_offset_of_U3COriginU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(WebSocket_t713846903, ___U3COriginU3Ek__BackingField_35)); }
	inline String_t* get_U3COriginU3Ek__BackingField_35() const { return ___U3COriginU3Ek__BackingField_35; }
	inline String_t** get_address_of_U3COriginU3Ek__BackingField_35() { return &___U3COriginU3Ek__BackingField_35; }
	inline void set_U3COriginU3Ek__BackingField_35(String_t* value)
	{
		___U3COriginU3Ek__BackingField_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3COriginU3Ek__BackingField_35, value);
	}
};

struct WebSocket_t713846903_StaticFields
{
public:
	// WebSocket4Net.Protocol.ProtocolProcessorFactory WebSocket4Net.WebSocket::m_ProtocolProcessorFactory
	ProtocolProcessorFactory_t3499308290 * ___m_ProtocolProcessorFactory_8;

public:
	inline static int32_t get_offset_of_m_ProtocolProcessorFactory_8() { return static_cast<int32_t>(offsetof(WebSocket_t713846903_StaticFields, ___m_ProtocolProcessorFactory_8)); }
	inline ProtocolProcessorFactory_t3499308290 * get_m_ProtocolProcessorFactory_8() const { return ___m_ProtocolProcessorFactory_8; }
	inline ProtocolProcessorFactory_t3499308290 ** get_address_of_m_ProtocolProcessorFactory_8() { return &___m_ProtocolProcessorFactory_8; }
	inline void set_m_ProtocolProcessorFactory_8(ProtocolProcessorFactory_t3499308290 * value)
	{
		___m_ProtocolProcessorFactory_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_ProtocolProcessorFactory_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
