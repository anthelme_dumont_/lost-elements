﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.ErrorEventArgs
struct ErrorEventArgs_t342512475;
// System.String
struct String_t;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void SocketIOClient.ErrorEventArgs::.ctor(System.String)
extern "C"  void ErrorEventArgs__ctor_m2633561680 (ErrorEventArgs_t342512475 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.ErrorEventArgs::.ctor(System.String,System.Exception)
extern "C"  void ErrorEventArgs__ctor_m2711369958 (ErrorEventArgs_t342512475 * __this, String_t* ___message, Exception_t1967233988 * ___exception, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.ErrorEventArgs::get_Message()
extern "C"  String_t* ErrorEventArgs_get_Message_m1223520785 (ErrorEventArgs_t342512475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.ErrorEventArgs::set_Message(System.String)
extern "C"  void ErrorEventArgs_set_Message_m1564870344 (ErrorEventArgs_t342512475 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception SocketIOClient.ErrorEventArgs::get_Exception()
extern "C"  Exception_t1967233988 * ErrorEventArgs_get_Exception_m4262431971 (ErrorEventArgs_t342512475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.ErrorEventArgs::set_Exception(System.Exception)
extern "C"  void ErrorEventArgs_set_Exception_m315626738 (ErrorEventArgs_t342512475 * __this, Exception_t1967233988 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
