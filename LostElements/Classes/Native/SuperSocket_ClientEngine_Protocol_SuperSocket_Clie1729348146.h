﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>
struct  ArraySegmentEx_1_t1729348146  : public Il2CppObject
{
public:
	// T[] SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1::<Array>k__BackingField
	ObjectU5BU5D_t11523773* ___U3CArrayU3Ek__BackingField_0;
	// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_1;
	// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1::<Offset>k__BackingField
	int32_t ___U3COffsetU3Ek__BackingField_2;
	// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1::<From>k__BackingField
	int32_t ___U3CFromU3Ek__BackingField_3;
	// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1::<To>k__BackingField
	int32_t ___U3CToU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CArrayU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArraySegmentEx_1_t1729348146, ___U3CArrayU3Ek__BackingField_0)); }
	inline ObjectU5BU5D_t11523773* get_U3CArrayU3Ek__BackingField_0() const { return ___U3CArrayU3Ek__BackingField_0; }
	inline ObjectU5BU5D_t11523773** get_address_of_U3CArrayU3Ek__BackingField_0() { return &___U3CArrayU3Ek__BackingField_0; }
	inline void set_U3CArrayU3Ek__BackingField_0(ObjectU5BU5D_t11523773* value)
	{
		___U3CArrayU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CArrayU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ArraySegmentEx_1_t1729348146, ___U3CCountU3Ek__BackingField_1)); }
	inline int32_t get_U3CCountU3Ek__BackingField_1() const { return ___U3CCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_1() { return &___U3CCountU3Ek__BackingField_1; }
	inline void set_U3CCountU3Ek__BackingField_1(int32_t value)
	{
		___U3CCountU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3COffsetU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ArraySegmentEx_1_t1729348146, ___U3COffsetU3Ek__BackingField_2)); }
	inline int32_t get_U3COffsetU3Ek__BackingField_2() const { return ___U3COffsetU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3COffsetU3Ek__BackingField_2() { return &___U3COffsetU3Ek__BackingField_2; }
	inline void set_U3COffsetU3Ek__BackingField_2(int32_t value)
	{
		___U3COffsetU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFromU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ArraySegmentEx_1_t1729348146, ___U3CFromU3Ek__BackingField_3)); }
	inline int32_t get_U3CFromU3Ek__BackingField_3() const { return ___U3CFromU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFromU3Ek__BackingField_3() { return &___U3CFromU3Ek__BackingField_3; }
	inline void set_U3CFromU3Ek__BackingField_3(int32_t value)
	{
		___U3CFromU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CToU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ArraySegmentEx_1_t1729348146, ___U3CToU3Ek__BackingField_4)); }
	inline int32_t get_U3CToU3Ek__BackingField_4() const { return ___U3CToU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CToU3Ek__BackingField_4() { return &___U3CToU3Ek__BackingField_4; }
	inline void set_U3CToU3Ek__BackingField_4(int32_t value)
	{
		___U3CToU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
