﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Runtime.Serialization.DataMemberAttribute
struct DataMemberAttribute_t3625462001;
// System.String
struct String_t;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Runtime_Serialization_U3CModuleU3E86524790.h"
#include "System_Runtime_Serialization_U3CModuleU3E86524790MethodDeclarations.h"
#include "System_Runtime_Serialization_System_Runtime_Serial2606959609.h"
#include "System_Runtime_Serialization_System_Runtime_Serial2606959609MethodDeclarations.h"
#include "System_Runtime_Serialization_System_Runtime_Serial3625462001.h"
#include "System_Runtime_Serialization_System_Runtime_Serial3625462001MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "System_Runtime_Serialization_System_Runtime_Seriali583091647.h"
#include "System_Runtime_Serialization_System_Runtime_Seriali583091647MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String System.Runtime.Serialization.DataMemberAttribute::get_Name()
extern "C"  String_t* DataMemberAttribute_get_Name_m2466648376 (DataMemberAttribute_t3625462001 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_0();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
