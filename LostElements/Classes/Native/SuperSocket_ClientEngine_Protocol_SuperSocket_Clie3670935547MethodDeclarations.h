﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>
struct ArraySegmentEx_1_t3670935547;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"

// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>::.ctor(T[],System.Int32,System.Int32)
extern "C"  void ArraySegmentEx_1__ctor_m3565895900_gshared (ArraySegmentEx_1_t3670935547 * __this, ByteU5BU5D_t58506160* ___array, int32_t ___offset, int32_t ___count, const MethodInfo* method);
#define ArraySegmentEx_1__ctor_m3565895900(__this, ___array, ___offset, ___count, method) ((  void (*) (ArraySegmentEx_1_t3670935547 *, ByteU5BU5D_t58506160*, int32_t, int32_t, const MethodInfo*))ArraySegmentEx_1__ctor_m3565895900_gshared)(__this, ___array, ___offset, ___count, method)
// T[] SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>::get_Array()
extern "C"  ByteU5BU5D_t58506160* ArraySegmentEx_1_get_Array_m3800972651_gshared (ArraySegmentEx_1_t3670935547 * __this, const MethodInfo* method);
#define ArraySegmentEx_1_get_Array_m3800972651(__this, method) ((  ByteU5BU5D_t58506160* (*) (ArraySegmentEx_1_t3670935547 *, const MethodInfo*))ArraySegmentEx_1_get_Array_m3800972651_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>::set_Array(T[])
extern "C"  void ArraySegmentEx_1_set_Array_m2713495650_gshared (ArraySegmentEx_1_t3670935547 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method);
#define ArraySegmentEx_1_set_Array_m2713495650(__this, ___value, method) ((  void (*) (ArraySegmentEx_1_t3670935547 *, ByteU5BU5D_t58506160*, const MethodInfo*))ArraySegmentEx_1_set_Array_m2713495650_gshared)(__this, ___value, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>::get_Count()
extern "C"  int32_t ArraySegmentEx_1_get_Count_m401629384_gshared (ArraySegmentEx_1_t3670935547 * __this, const MethodInfo* method);
#define ArraySegmentEx_1_get_Count_m401629384(__this, method) ((  int32_t (*) (ArraySegmentEx_1_t3670935547 *, const MethodInfo*))ArraySegmentEx_1_get_Count_m401629384_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>::set_Count(System.Int32)
extern "C"  void ArraySegmentEx_1_set_Count_m1315394785_gshared (ArraySegmentEx_1_t3670935547 * __this, int32_t ___value, const MethodInfo* method);
#define ArraySegmentEx_1_set_Count_m1315394785(__this, ___value, method) ((  void (*) (ArraySegmentEx_1_t3670935547 *, int32_t, const MethodInfo*))ArraySegmentEx_1_set_Count_m1315394785_gshared)(__this, ___value, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>::get_Offset()
extern "C"  int32_t ArraySegmentEx_1_get_Offset_m3476671836_gshared (ArraySegmentEx_1_t3670935547 * __this, const MethodInfo* method);
#define ArraySegmentEx_1_get_Offset_m3476671836(__this, method) ((  int32_t (*) (ArraySegmentEx_1_t3670935547 *, const MethodInfo*))ArraySegmentEx_1_get_Offset_m3476671836_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>::set_Offset(System.Int32)
extern "C"  void ArraySegmentEx_1_set_Offset_m1341884133_gshared (ArraySegmentEx_1_t3670935547 * __this, int32_t ___value, const MethodInfo* method);
#define ArraySegmentEx_1_set_Offset_m1341884133(__this, ___value, method) ((  void (*) (ArraySegmentEx_1_t3670935547 *, int32_t, const MethodInfo*))ArraySegmentEx_1_set_Offset_m1341884133_gshared)(__this, ___value, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>::get_From()
extern "C"  int32_t ArraySegmentEx_1_get_From_m4119304371_gshared (ArraySegmentEx_1_t3670935547 * __this, const MethodInfo* method);
#define ArraySegmentEx_1_get_From_m4119304371(__this, method) ((  int32_t (*) (ArraySegmentEx_1_t3670935547 *, const MethodInfo*))ArraySegmentEx_1_get_From_m4119304371_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>::set_From(System.Int32)
extern "C"  void ArraySegmentEx_1_set_From_m3588109756_gshared (ArraySegmentEx_1_t3670935547 * __this, int32_t ___value, const MethodInfo* method);
#define ArraySegmentEx_1_set_From_m3588109756(__this, ___value, method) ((  void (*) (ArraySegmentEx_1_t3670935547 *, int32_t, const MethodInfo*))ArraySegmentEx_1_set_From_m3588109756_gshared)(__this, ___value, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>::get_To()
extern "C"  int32_t ArraySegmentEx_1_get_To_m3647152452_gshared (ArraySegmentEx_1_t3670935547 * __this, const MethodInfo* method);
#define ArraySegmentEx_1_get_To_m3647152452(__this, method) ((  int32_t (*) (ArraySegmentEx_1_t3670935547 *, const MethodInfo*))ArraySegmentEx_1_get_To_m3647152452_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>::set_To(System.Int32)
extern "C"  void ArraySegmentEx_1_set_To_m318642893_gshared (ArraySegmentEx_1_t3670935547 * __this, int32_t ___value, const MethodInfo* method);
#define ArraySegmentEx_1_set_To_m318642893(__this, ___value, method) ((  void (*) (ArraySegmentEx_1_t3670935547 *, int32_t, const MethodInfo*))ArraySegmentEx_1_set_To_m318642893_gshared)(__this, ___value, method)
