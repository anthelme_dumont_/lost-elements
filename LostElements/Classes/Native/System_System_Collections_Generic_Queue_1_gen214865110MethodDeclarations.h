﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>
struct Queue_1_t214865110;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.ArraySegment`1<System.Byte>>
struct IEnumerator_1_t4284851314;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.ArraySegment`1<System.Byte>[]
struct ArraySegment_1U5BU5D_t2465283671;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_ArraySegment_1_gen2801744866.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat1684486827.h"

// System.Void System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::.ctor()
extern "C"  void Queue_1__ctor_m3967493680_gshared (Queue_1_t214865110 * __this, const MethodInfo* method);
#define Queue_1__ctor_m3967493680(__this, method) ((  void (*) (Queue_1_t214865110 *, const MethodInfo*))Queue_1__ctor_m3967493680_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Queue_1_System_Collections_ICollection_CopyTo_m420810004_gshared (Queue_1_t214865110 * __this, Il2CppArray * ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m420810004(__this, ___array, ___idx, method) ((  void (*) (Queue_1_t214865110 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m420810004_gshared)(__this, ___array, ___idx, method)
// System.Boolean System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m2858782142_gshared (Queue_1_t214865110 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m2858782142(__this, method) ((  bool (*) (Queue_1_t214865110 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m2858782142_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Queue_1_System_Collections_ICollection_get_SyncRoot_m1366899966_gshared (Queue_1_t214865110 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m1366899966(__this, method) ((  Il2CppObject * (*) (Queue_1_t214865110 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m1366899966_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3228911746_gshared (Queue_1_t214865110 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3228911746(__this, method) ((  Il2CppObject* (*) (Queue_1_t214865110 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3228911746_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Queue_1_System_Collections_IEnumerable_GetEnumerator_m1514563875_gshared (Queue_1_t214865110 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m1514563875(__this, method) ((  Il2CppObject * (*) (Queue_1_t214865110 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m1514563875_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::CopyTo(T[],System.Int32)
extern "C"  void Queue_1_CopyTo_m3453183615_gshared (Queue_1_t214865110 * __this, ArraySegment_1U5BU5D_t2465283671* ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_CopyTo_m3453183615(__this, ___array, ___idx, method) ((  void (*) (Queue_1_t214865110 *, ArraySegment_1U5BU5D_t2465283671*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3453183615_gshared)(__this, ___array, ___idx, method)
// T System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::Dequeue()
extern "C"  ArraySegment_1_t2801744866  Queue_1_Dequeue_m3615804639_gshared (Queue_1_t214865110 * __this, const MethodInfo* method);
#define Queue_1_Dequeue_m3615804639(__this, method) ((  ArraySegment_1_t2801744866  (*) (Queue_1_t214865110 *, const MethodInfo*))Queue_1_Dequeue_m3615804639_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::Peek()
extern "C"  ArraySegment_1_t2801744866  Queue_1_Peek_m1433803694_gshared (Queue_1_t214865110 * __this, const MethodInfo* method);
#define Queue_1_Peek_m1433803694(__this, method) ((  ArraySegment_1_t2801744866  (*) (Queue_1_t214865110 *, const MethodInfo*))Queue_1_Peek_m1433803694_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::Enqueue(T)
extern "C"  void Queue_1_Enqueue_m3564548104_gshared (Queue_1_t214865110 * __this, ArraySegment_1_t2801744866  ___item, const MethodInfo* method);
#define Queue_1_Enqueue_m3564548104(__this, ___item, method) ((  void (*) (Queue_1_t214865110 *, ArraySegment_1_t2801744866 , const MethodInfo*))Queue_1_Enqueue_m3564548104_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::SetCapacity(System.Int32)
extern "C"  void Queue_1_SetCapacity_m1541998619_gshared (Queue_1_t214865110 * __this, int32_t ___new_size, const MethodInfo* method);
#define Queue_1_SetCapacity_m1541998619(__this, ___new_size, method) ((  void (*) (Queue_1_t214865110 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m1541998619_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m4119769092_gshared (Queue_1_t214865110 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m4119769092(__this, method) ((  int32_t (*) (Queue_1_t214865110 *, const MethodInfo*))Queue_1_get_Count_m4119769092_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>::GetEnumerator()
extern "C"  Enumerator_t1684486827  Queue_1_GetEnumerator_m4232802095_gshared (Queue_1_t214865110 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m4232802095(__this, method) ((  Enumerator_t1684486827  (*) (Queue_1_t214865110 *, const MethodInfo*))Queue_1_GetEnumerator_m4232802095_gshared)(__this, method)
