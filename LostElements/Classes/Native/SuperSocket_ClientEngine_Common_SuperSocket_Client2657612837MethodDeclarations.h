﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.SearchMarkState`1<System.Object>
struct SearchMarkState_1_t2657612837;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"

// System.Void SuperSocket.ClientEngine.SearchMarkState`1<System.Object>::.ctor(T[])
extern "C"  void SearchMarkState_1__ctor_m2245802623_gshared (SearchMarkState_1_t2657612837 * __this, ObjectU5BU5D_t11523773* ___mark, const MethodInfo* method);
#define SearchMarkState_1__ctor_m2245802623(__this, ___mark, method) ((  void (*) (SearchMarkState_1_t2657612837 *, ObjectU5BU5D_t11523773*, const MethodInfo*))SearchMarkState_1__ctor_m2245802623_gshared)(__this, ___mark, method)
// T[] SuperSocket.ClientEngine.SearchMarkState`1<System.Object>::get_Mark()
extern "C"  ObjectU5BU5D_t11523773* SearchMarkState_1_get_Mark_m913728352_gshared (SearchMarkState_1_t2657612837 * __this, const MethodInfo* method);
#define SearchMarkState_1_get_Mark_m913728352(__this, method) ((  ObjectU5BU5D_t11523773* (*) (SearchMarkState_1_t2657612837 *, const MethodInfo*))SearchMarkState_1_get_Mark_m913728352_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.SearchMarkState`1<System.Object>::set_Mark(T[])
extern "C"  void SearchMarkState_1_set_Mark_m2315892747_gshared (SearchMarkState_1_t2657612837 * __this, ObjectU5BU5D_t11523773* ___value, const MethodInfo* method);
#define SearchMarkState_1_set_Mark_m2315892747(__this, ___value, method) ((  void (*) (SearchMarkState_1_t2657612837 *, ObjectU5BU5D_t11523773*, const MethodInfo*))SearchMarkState_1_set_Mark_m2315892747_gshared)(__this, ___value, method)
// System.Int32 SuperSocket.ClientEngine.SearchMarkState`1<System.Object>::get_Matched()
extern "C"  int32_t SearchMarkState_1_get_Matched_m3243511744_gshared (SearchMarkState_1_t2657612837 * __this, const MethodInfo* method);
#define SearchMarkState_1_get_Matched_m3243511744(__this, method) ((  int32_t (*) (SearchMarkState_1_t2657612837 *, const MethodInfo*))SearchMarkState_1_get_Matched_m3243511744_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.SearchMarkState`1<System.Object>::set_Matched(System.Int32)
extern "C"  void SearchMarkState_1_set_Matched_m1226678739_gshared (SearchMarkState_1_t2657612837 * __this, int32_t ___value, const MethodInfo* method);
#define SearchMarkState_1_set_Matched_m1226678739(__this, ___value, method) ((  void (*) (SearchMarkState_1_t2657612837 *, int32_t, const MethodInfo*))SearchMarkState_1_set_Matched_m1226678739_gshared)(__this, ___value, method)
