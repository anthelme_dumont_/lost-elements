﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SuperSocket.ClientEngine.Protocol.ArraySegmentList
struct ArraySegmentList_t1783467835;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>
struct IClientCommandReader_1_t3175310917;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.ReaderBase
struct  ReaderBase_t893022310  : public Il2CppObject
{
public:
	// SuperSocket.ClientEngine.Protocol.ArraySegmentList WebSocket4Net.Protocol.ReaderBase::m_BufferSegments
	ArraySegmentList_t1783467835 * ___m_BufferSegments_0;
	// WebSocket4Net.WebSocket WebSocket4Net.Protocol.ReaderBase::<WebSocket>k__BackingField
	WebSocket_t713846903 * ___U3CWebSocketU3Ek__BackingField_1;
	// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo> WebSocket4Net.Protocol.ReaderBase::<NextCommandReader>k__BackingField
	Il2CppObject* ___U3CNextCommandReaderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_m_BufferSegments_0() { return static_cast<int32_t>(offsetof(ReaderBase_t893022310, ___m_BufferSegments_0)); }
	inline ArraySegmentList_t1783467835 * get_m_BufferSegments_0() const { return ___m_BufferSegments_0; }
	inline ArraySegmentList_t1783467835 ** get_address_of_m_BufferSegments_0() { return &___m_BufferSegments_0; }
	inline void set_m_BufferSegments_0(ArraySegmentList_t1783467835 * value)
	{
		___m_BufferSegments_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_BufferSegments_0, value);
	}

	inline static int32_t get_offset_of_U3CWebSocketU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReaderBase_t893022310, ___U3CWebSocketU3Ek__BackingField_1)); }
	inline WebSocket_t713846903 * get_U3CWebSocketU3Ek__BackingField_1() const { return ___U3CWebSocketU3Ek__BackingField_1; }
	inline WebSocket_t713846903 ** get_address_of_U3CWebSocketU3Ek__BackingField_1() { return &___U3CWebSocketU3Ek__BackingField_1; }
	inline void set_U3CWebSocketU3Ek__BackingField_1(WebSocket_t713846903 * value)
	{
		___U3CWebSocketU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CWebSocketU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CNextCommandReaderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ReaderBase_t893022310, ___U3CNextCommandReaderU3Ek__BackingField_2)); }
	inline Il2CppObject* get_U3CNextCommandReaderU3Ek__BackingField_2() const { return ___U3CNextCommandReaderU3Ek__BackingField_2; }
	inline Il2CppObject** get_address_of_U3CNextCommandReaderU3Ek__BackingField_2() { return &___U3CNextCommandReaderU3Ek__BackingField_2; }
	inline void set_U3CNextCommandReaderU3Ek__BackingField_2(Il2CppObject* value)
	{
		___U3CNextCommandReaderU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNextCommandReaderU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
