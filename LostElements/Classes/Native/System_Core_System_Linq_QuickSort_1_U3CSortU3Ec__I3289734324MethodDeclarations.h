﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Int32>
struct U3CSortU3Ec__Iterator21_t3289734324;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t35553939;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Int32>::.ctor()
extern "C"  void U3CSortU3Ec__Iterator21__ctor_m2942573374_gshared (U3CSortU3Ec__Iterator21_t3289734324 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21__ctor_m2942573374(__this, method) ((  void (*) (U3CSortU3Ec__Iterator21_t3289734324 *, const MethodInfo*))U3CSortU3Ec__Iterator21__ctor_m2942573374_gshared)(__this, method)
// TElement System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Int32>::System.Collections.Generic.IEnumerator<TElement>.get_Current()
extern "C"  int32_t U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m1121046987_gshared (U3CSortU3Ec__Iterator21_t3289734324 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m1121046987(__this, method) ((  int32_t (*) (U3CSortU3Ec__Iterator21_t3289734324 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m1121046987_gshared)(__this, method)
// System.Object System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m2202915496_gshared (U3CSortU3Ec__Iterator21_t3289734324 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m2202915496(__this, method) ((  Il2CppObject * (*) (U3CSortU3Ec__Iterator21_t3289734324 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m2202915496_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m289677443_gshared (U3CSortU3Ec__Iterator21_t3289734324 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m289677443(__this, method) ((  Il2CppObject * (*) (U3CSortU3Ec__Iterator21_t3289734324 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m289677443_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Int32>::System.Collections.Generic.IEnumerable<TElement>.GetEnumerator()
extern "C"  Il2CppObject* U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m3680799834_gshared (U3CSortU3Ec__Iterator21_t3289734324 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m3680799834(__this, method) ((  Il2CppObject* (*) (U3CSortU3Ec__Iterator21_t3289734324 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m3680799834_gshared)(__this, method)
// System.Boolean System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Int32>::MoveNext()
extern "C"  bool U3CSortU3Ec__Iterator21_MoveNext_m166398742_gshared (U3CSortU3Ec__Iterator21_t3289734324 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_MoveNext_m166398742(__this, method) ((  bool (*) (U3CSortU3Ec__Iterator21_t3289734324 *, const MethodInfo*))U3CSortU3Ec__Iterator21_MoveNext_m166398742_gshared)(__this, method)
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Int32>::Dispose()
extern "C"  void U3CSortU3Ec__Iterator21_Dispose_m1622814715_gshared (U3CSortU3Ec__Iterator21_t3289734324 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_Dispose_m1622814715(__this, method) ((  void (*) (U3CSortU3Ec__Iterator21_t3289734324 *, const MethodInfo*))U3CSortU3Ec__Iterator21_Dispose_m1622814715_gshared)(__this, method)
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Int32>::Reset()
extern "C"  void U3CSortU3Ec__Iterator21_Reset_m589006315_gshared (U3CSortU3Ec__Iterator21_t3289734324 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_Reset_m589006315(__this, method) ((  void (*) (U3CSortU3Ec__Iterator21_t3289734324 *, const MethodInfo*))U3CSortU3Ec__Iterator21_Reset_m589006315_gshared)(__this, method)
