﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Messages.ConnectMessage
struct ConnectMessage_t3539960911;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SocketIOClient.Messages.ConnectMessage::.ctor()
extern "C"  void ConnectMessage__ctor_m4222249574 (ConnectMessage_t3539960911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.ConnectMessage::.ctor(System.String)
extern "C"  void ConnectMessage__ctor_m1926351452 (ConnectMessage_t3539960911 * __this, String_t* ___endPoint, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.ConnectMessage::get_Query()
extern "C"  String_t* ConnectMessage_get_Query_m2105175520 (ConnectMessage_t3539960911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.ConnectMessage::set_Query(System.String)
extern "C"  void ConnectMessage_set_Query_m2817543539 (ConnectMessage_t3539960911 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.ConnectMessage::get_Event()
extern "C"  String_t* ConnectMessage_get_Event_m73571122 (ConnectMessage_t3539960911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.ConnectMessage SocketIOClient.Messages.ConnectMessage::Deserialize(System.String)
extern "C"  ConnectMessage_t3539960911 * ConnectMessage_Deserialize_m3399944103 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.ConnectMessage::get_Encoded()
extern "C"  String_t* ConnectMessage_get_Encoded_m3460478694 (ConnectMessage_t3539960911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
