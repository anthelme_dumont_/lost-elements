﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "WebSocket4Net_WebSocket4Net_Protocol_ReaderBase893022310.h"
#include "mscorlib_System_Nullable_1_gen1369764433.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.DraftHybi00DataReader
struct  DraftHybi00DataReader_t1509845496  : public ReaderBase_t893022310
{
public:
	// System.Nullable`1<System.Byte> WebSocket4Net.Protocol.DraftHybi00DataReader::m_Type
	Nullable_1_t1369764433  ___m_Type_3;
	// System.Int32 WebSocket4Net.Protocol.DraftHybi00DataReader::m_TempLength
	int32_t ___m_TempLength_4;
	// System.Nullable`1<System.Int32> WebSocket4Net.Protocol.DraftHybi00DataReader::m_Length
	Nullable_1_t1438485399  ___m_Length_5;

public:
	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(DraftHybi00DataReader_t1509845496, ___m_Type_3)); }
	inline Nullable_1_t1369764433  get_m_Type_3() const { return ___m_Type_3; }
	inline Nullable_1_t1369764433 * get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(Nullable_1_t1369764433  value)
	{
		___m_Type_3 = value;
	}

	inline static int32_t get_offset_of_m_TempLength_4() { return static_cast<int32_t>(offsetof(DraftHybi00DataReader_t1509845496, ___m_TempLength_4)); }
	inline int32_t get_m_TempLength_4() const { return ___m_TempLength_4; }
	inline int32_t* get_address_of_m_TempLength_4() { return &___m_TempLength_4; }
	inline void set_m_TempLength_4(int32_t value)
	{
		___m_TempLength_4 = value;
	}

	inline static int32_t get_offset_of_m_Length_5() { return static_cast<int32_t>(offsetof(DraftHybi00DataReader_t1509845496, ___m_Length_5)); }
	inline Nullable_1_t1438485399  get_m_Length_5() const { return ___m_Length_5; }
	inline Nullable_1_t1438485399 * get_address_of_m_Length_5() { return &___m_Length_5; }
	inline void set_m_Length_5(Nullable_1_t1438485399  value)
	{
		___m_Length_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
