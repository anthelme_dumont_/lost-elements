﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.DraftHybi10HandshakeReader
struct DraftHybi10HandshakeReader_t3118530676;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903.h"

// System.Void WebSocket4Net.Protocol.DraftHybi10HandshakeReader::.ctor(WebSocket4Net.WebSocket)
extern "C"  void DraftHybi10HandshakeReader__ctor_m2004520026 (DraftHybi10HandshakeReader_t3118530676 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.WebSocketCommandInfo WebSocket4Net.Protocol.DraftHybi10HandshakeReader::GetCommandInfo(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern "C"  WebSocketCommandInfo_t3536916738 * DraftHybi10HandshakeReader_GetCommandInfo_m4260492983 (DraftHybi10HandshakeReader_t3118530676 * __this, ByteU5BU5D_t58506160* ___readBuffer, int32_t ___offset, int32_t ___length, int32_t* ___left, const MethodInfo* method) IL2CPP_METHOD_ATTR;
