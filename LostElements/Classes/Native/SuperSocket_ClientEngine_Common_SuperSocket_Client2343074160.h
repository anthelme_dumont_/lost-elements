﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.IPAddress[]
struct IPAddressU5BU5D_t3494006030;
// System.Net.Sockets.Socket
struct Socket_t150013987;
// System.Object
struct Il2CppObject;
// SuperSocket.ClientEngine.ConnectedCallback
struct ConnectedCallback_t1584521517;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState
struct  DnsConnectState_t2343074160  : public Il2CppObject
{
public:
	// System.Net.IPAddress[] SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::<Addresses>k__BackingField
	IPAddressU5BU5D_t3494006030* ___U3CAddressesU3Ek__BackingField_0;
	// System.Int32 SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::<NextAddressIndex>k__BackingField
	int32_t ___U3CNextAddressIndexU3Ek__BackingField_1;
	// System.Int32 SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_2;
	// System.Net.Sockets.Socket SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::<Socket4>k__BackingField
	Socket_t150013987 * ___U3CSocket4U3Ek__BackingField_3;
	// System.Net.Sockets.Socket SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::<Socket6>k__BackingField
	Socket_t150013987 * ___U3CSocket6U3Ek__BackingField_4;
	// System.Object SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::<State>k__BackingField
	Il2CppObject * ___U3CStateU3Ek__BackingField_5;
	// SuperSocket.ClientEngine.ConnectedCallback SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::<Callback>k__BackingField
	ConnectedCallback_t1584521517 * ___U3CCallbackU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CAddressesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DnsConnectState_t2343074160, ___U3CAddressesU3Ek__BackingField_0)); }
	inline IPAddressU5BU5D_t3494006030* get_U3CAddressesU3Ek__BackingField_0() const { return ___U3CAddressesU3Ek__BackingField_0; }
	inline IPAddressU5BU5D_t3494006030** get_address_of_U3CAddressesU3Ek__BackingField_0() { return &___U3CAddressesU3Ek__BackingField_0; }
	inline void set_U3CAddressesU3Ek__BackingField_0(IPAddressU5BU5D_t3494006030* value)
	{
		___U3CAddressesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAddressesU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CNextAddressIndexU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DnsConnectState_t2343074160, ___U3CNextAddressIndexU3Ek__BackingField_1)); }
	inline int32_t get_U3CNextAddressIndexU3Ek__BackingField_1() const { return ___U3CNextAddressIndexU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CNextAddressIndexU3Ek__BackingField_1() { return &___U3CNextAddressIndexU3Ek__BackingField_1; }
	inline void set_U3CNextAddressIndexU3Ek__BackingField_1(int32_t value)
	{
		___U3CNextAddressIndexU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DnsConnectState_t2343074160, ___U3CPortU3Ek__BackingField_2)); }
	inline int32_t get_U3CPortU3Ek__BackingField_2() const { return ___U3CPortU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_2() { return &___U3CPortU3Ek__BackingField_2; }
	inline void set_U3CPortU3Ek__BackingField_2(int32_t value)
	{
		___U3CPortU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CSocket4U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DnsConnectState_t2343074160, ___U3CSocket4U3Ek__BackingField_3)); }
	inline Socket_t150013987 * get_U3CSocket4U3Ek__BackingField_3() const { return ___U3CSocket4U3Ek__BackingField_3; }
	inline Socket_t150013987 ** get_address_of_U3CSocket4U3Ek__BackingField_3() { return &___U3CSocket4U3Ek__BackingField_3; }
	inline void set_U3CSocket4U3Ek__BackingField_3(Socket_t150013987 * value)
	{
		___U3CSocket4U3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSocket4U3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CSocket6U3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DnsConnectState_t2343074160, ___U3CSocket6U3Ek__BackingField_4)); }
	inline Socket_t150013987 * get_U3CSocket6U3Ek__BackingField_4() const { return ___U3CSocket6U3Ek__BackingField_4; }
	inline Socket_t150013987 ** get_address_of_U3CSocket6U3Ek__BackingField_4() { return &___U3CSocket6U3Ek__BackingField_4; }
	inline void set_U3CSocket6U3Ek__BackingField_4(Socket_t150013987 * value)
	{
		___U3CSocket6U3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSocket6U3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DnsConnectState_t2343074160, ___U3CStateU3Ek__BackingField_5)); }
	inline Il2CppObject * get_U3CStateU3Ek__BackingField_5() const { return ___U3CStateU3Ek__BackingField_5; }
	inline Il2CppObject ** get_address_of_U3CStateU3Ek__BackingField_5() { return &___U3CStateU3Ek__BackingField_5; }
	inline void set_U3CStateU3Ek__BackingField_5(Il2CppObject * value)
	{
		___U3CStateU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStateU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DnsConnectState_t2343074160, ___U3CCallbackU3Ek__BackingField_6)); }
	inline ConnectedCallback_t1584521517 * get_U3CCallbackU3Ek__BackingField_6() const { return ___U3CCallbackU3Ek__BackingField_6; }
	inline ConnectedCallback_t1584521517 ** get_address_of_U3CCallbackU3Ek__BackingField_6() { return &___U3CCallbackU3Ek__BackingField_6; }
	inline void set_U3CCallbackU3Ek__BackingField_6(ConnectedCallback_t1584521517 * value)
	{
		___U3CCallbackU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCallbackU3Ek__BackingField_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
