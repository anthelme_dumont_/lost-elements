﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.FramePartReader.FixPartReader
struct FixPartReader_t3993251252;
// WebSocket4Net.Protocol.WebSocketDataFrame
struct WebSocketDataFrame_t222733806;
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader
struct IDataFramePartReader_t2102555929;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_WebSocketDataF222733806.h"

// System.Int32 WebSocket4Net.Protocol.FramePartReader.FixPartReader::Process(System.Int32,WebSocket4Net.Protocol.WebSocketDataFrame,WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader&)
extern "C"  int32_t FixPartReader_Process_m94731578 (FixPartReader_t3993251252 * __this, int32_t ___lastLength, WebSocketDataFrame_t222733806 * ___frame, Il2CppObject ** ___nextPartReader, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.FramePartReader.FixPartReader::.ctor()
extern "C"  void FixPartReader__ctor_m3383315125 (FixPartReader_t3993251252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
