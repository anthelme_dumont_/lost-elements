﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "WebSocket4Net_WebSocket4Net_Protocol_HandshakeRead1078082956.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.DraftHybi00HandshakeReader
struct  DraftHybi00HandshakeReader_t1765220979  : public HandshakeReader_t1078082956
{
public:
	// System.Int32 WebSocket4Net.Protocol.DraftHybi00HandshakeReader::m_ReceivedChallengeLength
	int32_t ___m_ReceivedChallengeLength_6;
	// System.Int32 WebSocket4Net.Protocol.DraftHybi00HandshakeReader::m_ExpectedChallengeLength
	int32_t ___m_ExpectedChallengeLength_7;
	// WebSocket4Net.WebSocketCommandInfo WebSocket4Net.Protocol.DraftHybi00HandshakeReader::m_HandshakeCommand
	WebSocketCommandInfo_t3536916738 * ___m_HandshakeCommand_8;
	// System.Byte[] WebSocket4Net.Protocol.DraftHybi00HandshakeReader::m_Challenges
	ByteU5BU5D_t58506160* ___m_Challenges_9;

public:
	inline static int32_t get_offset_of_m_ReceivedChallengeLength_6() { return static_cast<int32_t>(offsetof(DraftHybi00HandshakeReader_t1765220979, ___m_ReceivedChallengeLength_6)); }
	inline int32_t get_m_ReceivedChallengeLength_6() const { return ___m_ReceivedChallengeLength_6; }
	inline int32_t* get_address_of_m_ReceivedChallengeLength_6() { return &___m_ReceivedChallengeLength_6; }
	inline void set_m_ReceivedChallengeLength_6(int32_t value)
	{
		___m_ReceivedChallengeLength_6 = value;
	}

	inline static int32_t get_offset_of_m_ExpectedChallengeLength_7() { return static_cast<int32_t>(offsetof(DraftHybi00HandshakeReader_t1765220979, ___m_ExpectedChallengeLength_7)); }
	inline int32_t get_m_ExpectedChallengeLength_7() const { return ___m_ExpectedChallengeLength_7; }
	inline int32_t* get_address_of_m_ExpectedChallengeLength_7() { return &___m_ExpectedChallengeLength_7; }
	inline void set_m_ExpectedChallengeLength_7(int32_t value)
	{
		___m_ExpectedChallengeLength_7 = value;
	}

	inline static int32_t get_offset_of_m_HandshakeCommand_8() { return static_cast<int32_t>(offsetof(DraftHybi00HandshakeReader_t1765220979, ___m_HandshakeCommand_8)); }
	inline WebSocketCommandInfo_t3536916738 * get_m_HandshakeCommand_8() const { return ___m_HandshakeCommand_8; }
	inline WebSocketCommandInfo_t3536916738 ** get_address_of_m_HandshakeCommand_8() { return &___m_HandshakeCommand_8; }
	inline void set_m_HandshakeCommand_8(WebSocketCommandInfo_t3536916738 * value)
	{
		___m_HandshakeCommand_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_HandshakeCommand_8, value);
	}

	inline static int32_t get_offset_of_m_Challenges_9() { return static_cast<int32_t>(offsetof(DraftHybi00HandshakeReader_t1765220979, ___m_Challenges_9)); }
	inline ByteU5BU5D_t58506160* get_m_Challenges_9() const { return ___m_Challenges_9; }
	inline ByteU5BU5D_t58506160** get_address_of_m_Challenges_9() { return &___m_Challenges_9; }
	inline void set_m_Challenges_9(ByteU5BU5D_t58506160* value)
	{
		___m_Challenges_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_Challenges_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
