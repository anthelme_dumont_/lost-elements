﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Void ElementsController::.cctor()
extern "C"  void ElementsController__cctor_m2377340613 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ElementsController::switchToEarth()
extern "C"  void ElementsController_switchToEarth_m2292208673 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ElementsController::switchToWind()
extern "C"  void ElementsController_switchToWind_m2813275891 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ElementsController::switchToWater()
extern "C"  void ElementsController_switchToWater_m1088815534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ElementsController::switchToSun()
extern "C"  void ElementsController_switchToSun_m225959715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ElementsController::changeIntesity(System.Single)
extern "C"  void ElementsController_changeIntesity_m1797890574 (Il2CppObject * __this /* static, unused */, float ___intensity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
