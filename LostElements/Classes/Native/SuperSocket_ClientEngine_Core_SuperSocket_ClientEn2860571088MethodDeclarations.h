﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.AsyncTcpSession
struct AsyncTcpSession_t2860571088;
// System.Net.EndPoint
struct EndPoint_t1294049535;
// System.Object
struct Il2CppObject;
// System.Net.Sockets.SocketAsyncEventArgs
struct SocketAsyncEventArgs_t970431102;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_EndPoint1294049535.h"
#include "mscorlib_System_Object837106420.h"
#include "System_System_Net_Sockets_SocketAsyncEventArgs970431102.h"
#include "mscorlib_System_ArraySegment_1_gen2801744866.h"

// System.Void SuperSocket.ClientEngine.AsyncTcpSession::.ctor(System.Net.EndPoint)
extern "C"  void AsyncTcpSession__ctor_m2893573072 (AsyncTcpSession_t2860571088 * __this, EndPoint_t1294049535 * ___remoteEndPoint, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::SocketEventArgsCompleted(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern "C"  void AsyncTcpSession_SocketEventArgsCompleted_m3117056998 (AsyncTcpSession_t2860571088 * __this, Il2CppObject * ___sender, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::OnGetSocket(System.Net.Sockets.SocketAsyncEventArgs)
extern "C"  void AsyncTcpSession_OnGetSocket_m4043555073 (AsyncTcpSession_t2860571088 * __this, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::ProcessReceive(System.Net.Sockets.SocketAsyncEventArgs)
extern "C"  void AsyncTcpSession_ProcessReceive_m139804939 (AsyncTcpSession_t2860571088 * __this, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::StartReceive()
extern "C"  void AsyncTcpSession_StartReceive_m3779991304 (AsyncTcpSession_t2860571088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::SendInternal(System.ArraySegment`1<System.Byte>)
extern "C"  void AsyncTcpSession_SendInternal_m822235139 (AsyncTcpSession_t2860571088 * __this, ArraySegment_1_t2801744866  ___segment, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::Sending_Completed(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern "C"  void AsyncTcpSession_Sending_Completed_m4078982099 (AsyncTcpSession_t2860571088 * __this, Il2CppObject * ___sender, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
