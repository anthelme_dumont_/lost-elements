﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t3416858730;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Extensions
struct  Extensions_t39620836  : public Il2CppObject
{
public:

public:
};

struct Extensions_t39620836_StaticFields
{
public:
	// System.Char[] WebSocket4Net.Extensions::m_CrCf
	CharU5BU5D_t3416858730* ___m_CrCf_0;

public:
	inline static int32_t get_offset_of_m_CrCf_0() { return static_cast<int32_t>(offsetof(Extensions_t39620836_StaticFields, ___m_CrCf_0)); }
	inline CharU5BU5D_t3416858730* get_m_CrCf_0() const { return ___m_CrCf_0; }
	inline CharU5BU5D_t3416858730** get_address_of_m_CrCf_0() { return &___m_CrCf_0; }
	inline void set_m_CrCf_0(CharU5BU5D_t3416858730* value)
	{
		___m_CrCf_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_CrCf_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
