﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.CloseStatusCodeRfc6455
struct CloseStatusCodeRfc6455_t1268471848;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::.ctor()
extern "C"  void CloseStatusCodeRfc6455__ctor_m917947323 (CloseStatusCodeRfc6455_t1268471848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::get_NormalClosure()
extern "C"  int32_t CloseStatusCodeRfc6455_get_NormalClosure_m1677157654 (CloseStatusCodeRfc6455_t1268471848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_NormalClosure(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_NormalClosure_m205733537 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_GoingAway(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_GoingAway_m1613287893 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::get_ProtocolError()
extern "C"  int32_t CloseStatusCodeRfc6455_get_ProtocolError_m2175001874 (CloseStatusCodeRfc6455_t1268471848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_ProtocolError(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_ProtocolError_m3508774045 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_NotAcceptableData(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_NotAcceptableData_m222792044 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_TooLargeFrame(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_TooLargeFrame_m2292392755 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_InvalidUTF8(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_InvalidUTF8_m3753982229 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_ViolatePolicy(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_ViolatePolicy_m1938886625 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_ExtensionNotMatch(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_ExtensionNotMatch_m2023093022 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_UnexpectedCondition(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_UnexpectedCondition_m335198295 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeRfc6455::get_NoStatusCode()
extern "C"  int32_t CloseStatusCodeRfc6455_get_NoStatusCode_m1528034496 (CloseStatusCodeRfc6455_t1268471848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeRfc6455::set_NoStatusCode(System.Int32)
extern "C"  void CloseStatusCodeRfc6455_set_NoStatusCode_m2855855511 (CloseStatusCodeRfc6455_t1268471848 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
