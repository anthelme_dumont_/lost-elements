﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<System.ArraySegment`1<System.Byte>>
struct Queue_1_t214865110;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat1684486827.h"
#include "mscorlib_System_ArraySegment_1_gen2801744866.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<System.ArraySegment`1<System.Byte>>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C"  void Enumerator__ctor_m781195182_gshared (Enumerator_t1684486827 * __this, Queue_1_t214865110 * ___q, const MethodInfo* method);
#define Enumerator__ctor_m781195182(__this, ___q, method) ((  void (*) (Enumerator_t1684486827 *, Queue_1_t214865110 *, const MethodInfo*))Enumerator__ctor_m781195182_gshared)(__this, ___q, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3486661609_gshared (Enumerator_t1684486827 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3486661609(__this, method) ((  void (*) (Enumerator_t1684486827 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3486661609_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1419203989_gshared (Enumerator_t1684486827 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1419203989(__this, method) ((  Il2CppObject * (*) (Enumerator_t1684486827 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1419203989_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.ArraySegment`1<System.Byte>>::Dispose()
extern "C"  void Enumerator_Dispose_m3819376366_gshared (Enumerator_t1684486827 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3819376366(__this, method) ((  void (*) (Enumerator_t1684486827 *, const MethodInfo*))Enumerator_Dispose_m3819376366_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.ArraySegment`1<System.Byte>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3671211969_gshared (Enumerator_t1684486827 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3671211969(__this, method) ((  bool (*) (Enumerator_t1684486827 *, const MethodInfo*))Enumerator_MoveNext_m3671211969_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<System.ArraySegment`1<System.Byte>>::get_Current()
extern "C"  ArraySegment_1_t2801744866  Enumerator_get_Current_m3179919710_gshared (Enumerator_t1684486827 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3179919710(__this, method) ((  ArraySegment_1_t2801744866  (*) (Enumerator_t1684486827 *, const MethodInfo*))Enumerator_get_Current_m3179919710_gshared)(__this, method)
