﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Messages.DisconnectMessage
struct DisconnectMessage_t1581018141;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SocketIOClient.Messages.DisconnectMessage::.ctor()
extern "C"  void DisconnectMessage__ctor_m1144767512 (DisconnectMessage_t1581018141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.DisconnectMessage::.ctor(System.String)
extern "C"  void DisconnectMessage__ctor_m2234466794 (DisconnectMessage_t1581018141 * __this, String_t* ___endPoint, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.DisconnectMessage::get_Event()
extern "C"  String_t* DisconnectMessage_get_Event_m2485685450 (DisconnectMessage_t1581018141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.DisconnectMessage SocketIOClient.Messages.DisconnectMessage::Deserialize(System.String)
extern "C"  DisconnectMessage_t1581018141 * DisconnectMessage_Deserialize_m2775835117 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.DisconnectMessage::get_Encoded()
extern "C"  String_t* DisconnectMessage_get_Encoded_m2220008062 (DisconnectMessage_t1581018141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
