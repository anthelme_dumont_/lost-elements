﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo4000251768MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m1947206736(__this, ___list, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m515477050(__this, ___item, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, ArraySegmentEx_1_t3670935547 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m806610384(__this, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m65867937(__this, ___index, ___item, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, int32_t, ArraySegmentEx_1_t3670935547 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2640369145(__this, ___item, method) ((  bool (*) (ReadOnlyCollection_1_t2539113599 *, ArraySegmentEx_1_t3670935547 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2234688103(__this, ___index, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2397735147(__this, ___index, method) ((  ArraySegmentEx_1_t3670935547 * (*) (ReadOnlyCollection_1_t2539113599 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m684174648(__this, ___index, ___value, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, int32_t, ArraySegmentEx_1_t3670935547 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1743908214(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2539113599 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1005883135(__this, ___array, ___index, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2014055098(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2539113599 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3613289335(__this, ___value, method) ((  int32_t (*) (ReadOnlyCollection_1_t2539113599 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2653248013(__this, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3192478645(__this, ___value, method) ((  bool (*) (ReadOnlyCollection_1_t2539113599 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2449346127(__this, ___value, method) ((  int32_t (*) (ReadOnlyCollection_1_t2539113599 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3851866618(__this, ___index, ___value, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2053974254(__this, ___value, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m834048138(__this, ___index, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3665453959(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2539113599 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2872640563(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2539113599 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m379139812(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2539113599 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3929922453(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2539113599 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m9221370(__this, ___index, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2539113599 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m4290930513(__this, ___index, ___value, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::Contains(T)
#define ReadOnlyCollection_1_Contains_m1795199038(__this, ___value, method) ((  bool (*) (ReadOnlyCollection_1_t2539113599 *, ArraySegmentEx_1_t3670935547 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1481863018(__this, ___array, ___index, method) ((  void (*) (ReadOnlyCollection_1_t2539113599 *, ArraySegmentEx_1U5BU5D_t3463162298*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m3672435873(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2539113599 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m2826342510(__this, ___value, method) ((  int32_t (*) (ReadOnlyCollection_1_t2539113599 *, ArraySegmentEx_1_t3670935547 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::get_Count()
#define ReadOnlyCollection_1_get_Count_m1534579405(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2539113599 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m3399009323(__this, ___index, method) ((  ArraySegmentEx_1_t3670935547 * (*) (ReadOnlyCollection_1_t2539113599 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index, method)
