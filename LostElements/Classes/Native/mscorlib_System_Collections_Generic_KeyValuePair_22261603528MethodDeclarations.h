﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22261603528.h"
#include "System_Core_System_TimeZoneInfo_TimeType2282261447.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.TimeZoneInfo/TimeType>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1984395650_gshared (KeyValuePair_2_t2261603528 * __this, int32_t ___key, TimeType_t2282261447  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1984395650(__this, ___key, ___value, method) ((  void (*) (KeyValuePair_2_t2261603528 *, int32_t, TimeType_t2282261447 , const MethodInfo*))KeyValuePair_2__ctor_m1984395650_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.TimeZoneInfo/TimeType>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1930846118_gshared (KeyValuePair_2_t2261603528 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1930846118(__this, method) ((  int32_t (*) (KeyValuePair_2_t2261603528 *, const MethodInfo*))KeyValuePair_2_get_Key_m1930846118_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.TimeZoneInfo/TimeType>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m490633063_gshared (KeyValuePair_2_t2261603528 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m490633063(__this, ___value, method) ((  void (*) (KeyValuePair_2_t2261603528 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m490633063_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.TimeZoneInfo/TimeType>::get_Value()
extern "C"  TimeType_t2282261447  KeyValuePair_2_get_Value_m1455001418_gshared (KeyValuePair_2_t2261603528 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1455001418(__this, method) ((  TimeType_t2282261447  (*) (KeyValuePair_2_t2261603528 *, const MethodInfo*))KeyValuePair_2_get_Value_m1455001418_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.TimeZoneInfo/TimeType>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3576807399_gshared (KeyValuePair_2_t2261603528 * __this, TimeType_t2282261447  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3576807399(__this, ___value, method) ((  void (*) (KeyValuePair_2_t2261603528 *, TimeType_t2282261447 , const MethodInfo*))KeyValuePair_2_set_Value_m3576807399_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.TimeZoneInfo/TimeType>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2540071873_gshared (KeyValuePair_2_t2261603528 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2540071873(__this, method) ((  String_t* (*) (KeyValuePair_2_t2261603528 *, const MethodInfo*))KeyValuePair_2_ToString_m2540071873_gshared)(__this, method)
