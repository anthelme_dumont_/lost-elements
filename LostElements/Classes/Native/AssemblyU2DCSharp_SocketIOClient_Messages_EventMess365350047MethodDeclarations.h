﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Messages.EventMessage
struct EventMessage_t365350047;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void SocketIOClient.Messages.EventMessage::.ctor()
extern "C"  void EventMessage__ctor_m1254573078 (EventMessage_t365350047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.EventMessage::.ctor(System.String,System.Object,System.String,System.Action`1<System.Object>)
extern "C"  void EventMessage__ctor_m820155226 (EventMessage_t365350047 * __this, String_t* ___eventName, Il2CppObject * ___jsonObject, String_t* ___endpoint, Action_1_t985559125 * ___callBack, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.EventMessage::.cctor()
extern "C"  void EventMessage__cctor_m4049930839 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SocketIOClient.Messages.EventMessage::get_NextAckID()
extern "C"  int32_t EventMessage_get_NextAckID_m800587118 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.EventMessage SocketIOClient.Messages.EventMessage::Deserialize(System.String)
extern "C"  EventMessage_t365350047 * EventMessage_Deserialize_m4156674791 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.EventMessage::get_Encoded()
extern "C"  String_t* EventMessage_get_Encoded_m3466975190 (EventMessage_t365350047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
