﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IList`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>
struct IList_1_t1542460565;
// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>
struct ArraySegmentEx_1_t3670935547;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>
struct  ArraySegmentList_1_t27518574  : public Il2CppObject
{
public:
	// System.Collections.Generic.IList`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<T>> SuperSocket.ClientEngine.Protocol.ArraySegmentList`1::m_Segments
	Il2CppObject* ___m_Segments_0;
	// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<T> SuperSocket.ClientEngine.Protocol.ArraySegmentList`1::m_PrevSegment
	ArraySegmentEx_1_t3670935547 * ___m_PrevSegment_1;
	// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1::m_PrevSegmentIndex
	int32_t ___m_PrevSegmentIndex_2;
	// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1::m_Count
	int32_t ___m_Count_3;

public:
	inline static int32_t get_offset_of_m_Segments_0() { return static_cast<int32_t>(offsetof(ArraySegmentList_1_t27518574, ___m_Segments_0)); }
	inline Il2CppObject* get_m_Segments_0() const { return ___m_Segments_0; }
	inline Il2CppObject** get_address_of_m_Segments_0() { return &___m_Segments_0; }
	inline void set_m_Segments_0(Il2CppObject* value)
	{
		___m_Segments_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Segments_0, value);
	}

	inline static int32_t get_offset_of_m_PrevSegment_1() { return static_cast<int32_t>(offsetof(ArraySegmentList_1_t27518574, ___m_PrevSegment_1)); }
	inline ArraySegmentEx_1_t3670935547 * get_m_PrevSegment_1() const { return ___m_PrevSegment_1; }
	inline ArraySegmentEx_1_t3670935547 ** get_address_of_m_PrevSegment_1() { return &___m_PrevSegment_1; }
	inline void set_m_PrevSegment_1(ArraySegmentEx_1_t3670935547 * value)
	{
		___m_PrevSegment_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_PrevSegment_1, value);
	}

	inline static int32_t get_offset_of_m_PrevSegmentIndex_2() { return static_cast<int32_t>(offsetof(ArraySegmentList_1_t27518574, ___m_PrevSegmentIndex_2)); }
	inline int32_t get_m_PrevSegmentIndex_2() const { return ___m_PrevSegmentIndex_2; }
	inline int32_t* get_address_of_m_PrevSegmentIndex_2() { return &___m_PrevSegmentIndex_2; }
	inline void set_m_PrevSegmentIndex_2(int32_t value)
	{
		___m_PrevSegmentIndex_2 = value;
	}

	inline static int32_t get_offset_of_m_Count_3() { return static_cast<int32_t>(offsetof(ArraySegmentList_1_t27518574, ___m_Count_3)); }
	inline int32_t get_m_Count_3() const { return ___m_Count_3; }
	inline int32_t* get_address_of_m_Count_3() { return &___m_Count_3; }
	inline void set_m_Count_3(int32_t value)
	{
		___m_Count_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
