﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.Reflection.MemberMapLoader
struct MemberMapLoader_t3073810362;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>
struct SafeDictionary_2_t2250466201;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SimpleJson.Reflection.MemberMapLoader::.ctor(System.Object,System.IntPtr)
extern "C"  void MemberMapLoader__ctor_m4182933641 (MemberMapLoader_t3073810362 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJson.Reflection.MemberMapLoader::Invoke(System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>)
extern "C"  void MemberMapLoader_Invoke_m1090529212 (MemberMapLoader_t3073810362 * __this, Type_t * ___type, SafeDictionary_2_t2250466201 * ___memberMaps, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult SimpleJson.Reflection.MemberMapLoader::BeginInvoke(System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MemberMapLoader_BeginInvoke_m2496604015 (MemberMapLoader_t3073810362 * __this, Type_t * ___type, SafeDictionary_2_t2250466201 * ___memberMaps, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJson.Reflection.MemberMapLoader::EndInvoke(System.IAsyncResult)
extern "C"  void MemberMapLoader_EndInvoke_m2416287513 (MemberMapLoader_t3073810362 * __this, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
