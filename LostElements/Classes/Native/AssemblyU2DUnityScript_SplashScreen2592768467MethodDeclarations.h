﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplashScreen
struct SplashScreen_t2592768467;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void SplashScreen::.ctor()
extern "C"  void SplashScreen__ctor_m1751502365 (SplashScreen_t2592768467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SplashScreen::Start()
extern "C"  Il2CppObject * SplashScreen_Start_m2977755083 (SplashScreen_t2592768467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplashScreen::Main()
extern "C"  void SplashScreen_Main_m2050204416 (SplashScreen_t2592768467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
