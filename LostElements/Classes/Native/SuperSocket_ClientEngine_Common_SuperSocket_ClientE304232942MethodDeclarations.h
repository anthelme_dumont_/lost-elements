﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.SearchMarkState`1<System.Byte>
struct SearchMarkState_1_t304232942;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"

// System.Void SuperSocket.ClientEngine.SearchMarkState`1<System.Byte>::.ctor(T[])
extern "C"  void SearchMarkState_1__ctor_m2012179761_gshared (SearchMarkState_1_t304232942 * __this, ByteU5BU5D_t58506160* ___mark, const MethodInfo* method);
#define SearchMarkState_1__ctor_m2012179761(__this, ___mark, method) ((  void (*) (SearchMarkState_1_t304232942 *, ByteU5BU5D_t58506160*, const MethodInfo*))SearchMarkState_1__ctor_m2012179761_gshared)(__this, ___mark, method)
// T[] SuperSocket.ClientEngine.SearchMarkState`1<System.Byte>::get_Mark()
extern "C"  ByteU5BU5D_t58506160* SearchMarkState_1_get_Mark_m289705655_gshared (SearchMarkState_1_t304232942 * __this, const MethodInfo* method);
#define SearchMarkState_1_get_Mark_m289705655(__this, method) ((  ByteU5BU5D_t58506160* (*) (SearchMarkState_1_t304232942 *, const MethodInfo*))SearchMarkState_1_get_Mark_m289705655_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.SearchMarkState`1<System.Byte>::set_Mark(T[])
extern "C"  void SearchMarkState_1_set_Mark_m2478896660_gshared (SearchMarkState_1_t304232942 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method);
#define SearchMarkState_1_set_Mark_m2478896660(__this, ___value, method) ((  void (*) (SearchMarkState_1_t304232942 *, ByteU5BU5D_t58506160*, const MethodInfo*))SearchMarkState_1_set_Mark_m2478896660_gshared)(__this, ___value, method)
// System.Int32 SuperSocket.ClientEngine.SearchMarkState`1<System.Byte>::get_Matched()
extern "C"  int32_t SearchMarkState_1_get_Matched_m3815954317_gshared (SearchMarkState_1_t304232942 * __this, const MethodInfo* method);
#define SearchMarkState_1_get_Matched_m3815954317(__this, method) ((  int32_t (*) (SearchMarkState_1_t304232942 *, const MethodInfo*))SearchMarkState_1_get_Matched_m3815954317_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.SearchMarkState`1<System.Byte>::set_Matched(System.Int32)
extern "C"  void SearchMarkState_1_set_Matched_m2525174364_gshared (SearchMarkState_1_t304232942 * __this, int32_t ___value, const MethodInfo* method);
#define SearchMarkState_1_set_Matched_m2525174364(__this, ___value, method) ((  void (*) (SearchMarkState_1_t304232942 *, int32_t, const MethodInfo*))SearchMarkState_1_set_Matched_m2525174364_gshared)(__this, ___value, method)
