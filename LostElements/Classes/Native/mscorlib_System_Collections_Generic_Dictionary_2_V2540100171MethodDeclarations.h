﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.TimeZoneInfo/TimeType>
struct Dictionary_2_t2773072230;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2540100171.h"
#include "System_Core_System_TimeZoneInfo_TimeType2282261447.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.TimeZoneInfo/TimeType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2014010174_gshared (Enumerator_t2540100173 * __this, Dictionary_2_t2773072230 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m2014010174(__this, ___host, method) ((  void (*) (Enumerator_t2540100173 *, Dictionary_2_t2773072230 *, const MethodInfo*))Enumerator__ctor_m2014010174_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.TimeZoneInfo/TimeType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2275132899_gshared (Enumerator_t2540100173 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2275132899(__this, method) ((  Il2CppObject * (*) (Enumerator_t2540100173 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2275132899_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.TimeZoneInfo/TimeType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m728623799_gshared (Enumerator_t2540100173 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m728623799(__this, method) ((  void (*) (Enumerator_t2540100173 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m728623799_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.TimeZoneInfo/TimeType>::Dispose()
extern "C"  void Enumerator_Dispose_m4178087264_gshared (Enumerator_t2540100173 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4178087264(__this, method) ((  void (*) (Enumerator_t2540100173 *, const MethodInfo*))Enumerator_Dispose_m4178087264_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.TimeZoneInfo/TimeType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1948120995_gshared (Enumerator_t2540100173 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1948120995(__this, method) ((  bool (*) (Enumerator_t2540100173 *, const MethodInfo*))Enumerator_MoveNext_m1948120995_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.TimeZoneInfo/TimeType>::get_Current()
extern "C"  TimeType_t2282261447  Enumerator_get_Current_m2254405375_gshared (Enumerator_t2540100173 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2254405375(__this, method) ((  TimeType_t2282261447  (*) (Enumerator_t2540100173 *, const MethodInfo*))Enumerator_get_Current_m2254405375_gshared)(__this, method)
