﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WaterController
struct WaterController_t632785907;

#include "codegen/il2cpp-codegen.h"

// System.Void WaterController::.ctor()
extern "C"  void WaterController__ctor_m3966958808 (WaterController_t632785907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterController::Start()
extern "C"  void WaterController_Start_m2914096600 (WaterController_t632785907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterController::Update()
extern "C"  void WaterController_Update_m148533557 (WaterController_t632785907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterController::changeWaterLevel()
extern "C"  void WaterController_changeWaterLevel_m2315523593 (WaterController_t632785907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
