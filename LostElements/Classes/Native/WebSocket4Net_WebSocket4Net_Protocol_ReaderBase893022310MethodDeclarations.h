﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.ReaderBase
struct ReaderBase_t893022310;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// SuperSocket.ClientEngine.Protocol.ArraySegmentList
struct ArraySegmentList_t1783467835;
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>
struct IClientCommandReader_1_t3175310917;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_ReaderBase893022310.h"

// System.Void WebSocket4Net.Protocol.ReaderBase::set_WebSocket(WebSocket4Net.WebSocket)
extern "C"  void ReaderBase_set_WebSocket_m2856695396 (ReaderBase_t893022310 * __this, WebSocket_t713846903 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.ReaderBase::.ctor(WebSocket4Net.WebSocket)
extern "C"  void ReaderBase__ctor_m3143587916 (ReaderBase_t893022310 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SuperSocket.ClientEngine.Protocol.ArraySegmentList WebSocket4Net.Protocol.ReaderBase::get_BufferSegments()
extern "C"  ArraySegmentList_t1783467835 * ReaderBase_get_BufferSegments_m2174645212 (ReaderBase_t893022310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.ReaderBase::.ctor(WebSocket4Net.Protocol.ReaderBase)
extern "C"  void ReaderBase__ctor_m849562793 (ReaderBase_t893022310 * __this, ReaderBase_t893022310 * ___previousCommandReader, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo> WebSocket4Net.Protocol.ReaderBase::get_NextCommandReader()
extern "C"  Il2CppObject* ReaderBase_get_NextCommandReader_m3473317212 (ReaderBase_t893022310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.ReaderBase::set_NextCommandReader(SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>)
extern "C"  void ReaderBase_set_NextCommandReader_m1719152039 (ReaderBase_t893022310 * __this, Il2CppObject* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.ReaderBase::AddArraySegment(System.Byte[],System.Int32,System.Int32)
extern "C"  void ReaderBase_AddArraySegment_m3437881651 (ReaderBase_t893022310 * __this, ByteU5BU5D_t58506160* ___buffer, int32_t ___offset, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
