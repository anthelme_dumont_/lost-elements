﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.EventHandler
struct EventHandler_t247020293;
// System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>
struct EventHandler_1_t2171844441;
// System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>
struct EventHandler_1_t4058788791;
// SuperSocket.ClientEngine.DataEventArgs
struct DataEventArgs_t3216211148;
// System.Net.Sockets.Socket
struct Socket_t150013987;
// System.Net.EndPoint
struct EndPoint_t1294049535;
// SuperSocket.ClientEngine.IProxyConnector
struct IProxyConnector_t1374585095;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_ArraySegment_1_gen2801744866.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.ClientSession
struct  ClientSession_t3100954378  : public Il2CppObject
{
public:
	// System.EventHandler SuperSocket.ClientEngine.ClientSession::m_Closed
	EventHandler_t247020293 * ___m_Closed_0;
	// System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs> SuperSocket.ClientEngine.ClientSession::m_Error
	EventHandler_1_t2171844441 * ___m_Error_1;
	// System.EventHandler SuperSocket.ClientEngine.ClientSession::m_Connected
	EventHandler_t247020293 * ___m_Connected_2;
	// System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs> SuperSocket.ClientEngine.ClientSession::m_DataReceived
	EventHandler_1_t4058788791 * ___m_DataReceived_3;
	// SuperSocket.ClientEngine.DataEventArgs SuperSocket.ClientEngine.ClientSession::m_DataArgs
	DataEventArgs_t3216211148 * ___m_DataArgs_4;
	// System.Net.Sockets.Socket SuperSocket.ClientEngine.ClientSession::<Client>k__BackingField
	Socket_t150013987 * ___U3CClientU3Ek__BackingField_5;
	// System.Net.EndPoint SuperSocket.ClientEngine.ClientSession::<RemoteEndPoint>k__BackingField
	EndPoint_t1294049535 * ___U3CRemoteEndPointU3Ek__BackingField_6;
	// System.Boolean SuperSocket.ClientEngine.ClientSession::<IsConnected>k__BackingField
	bool ___U3CIsConnectedU3Ek__BackingField_7;
	// System.Int32 SuperSocket.ClientEngine.ClientSession::<ReceiveBufferSize>k__BackingField
	int32_t ___U3CReceiveBufferSizeU3Ek__BackingField_8;
	// SuperSocket.ClientEngine.IProxyConnector SuperSocket.ClientEngine.ClientSession::<Proxy>k__BackingField
	Il2CppObject * ___U3CProxyU3Ek__BackingField_9;
	// System.ArraySegment`1<System.Byte> SuperSocket.ClientEngine.ClientSession::<Buffer>k__BackingField
	ArraySegment_1_t2801744866  ___U3CBufferU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_m_Closed_0() { return static_cast<int32_t>(offsetof(ClientSession_t3100954378, ___m_Closed_0)); }
	inline EventHandler_t247020293 * get_m_Closed_0() const { return ___m_Closed_0; }
	inline EventHandler_t247020293 ** get_address_of_m_Closed_0() { return &___m_Closed_0; }
	inline void set_m_Closed_0(EventHandler_t247020293 * value)
	{
		___m_Closed_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Closed_0, value);
	}

	inline static int32_t get_offset_of_m_Error_1() { return static_cast<int32_t>(offsetof(ClientSession_t3100954378, ___m_Error_1)); }
	inline EventHandler_1_t2171844441 * get_m_Error_1() const { return ___m_Error_1; }
	inline EventHandler_1_t2171844441 ** get_address_of_m_Error_1() { return &___m_Error_1; }
	inline void set_m_Error_1(EventHandler_1_t2171844441 * value)
	{
		___m_Error_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Error_1, value);
	}

	inline static int32_t get_offset_of_m_Connected_2() { return static_cast<int32_t>(offsetof(ClientSession_t3100954378, ___m_Connected_2)); }
	inline EventHandler_t247020293 * get_m_Connected_2() const { return ___m_Connected_2; }
	inline EventHandler_t247020293 ** get_address_of_m_Connected_2() { return &___m_Connected_2; }
	inline void set_m_Connected_2(EventHandler_t247020293 * value)
	{
		___m_Connected_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Connected_2, value);
	}

	inline static int32_t get_offset_of_m_DataReceived_3() { return static_cast<int32_t>(offsetof(ClientSession_t3100954378, ___m_DataReceived_3)); }
	inline EventHandler_1_t4058788791 * get_m_DataReceived_3() const { return ___m_DataReceived_3; }
	inline EventHandler_1_t4058788791 ** get_address_of_m_DataReceived_3() { return &___m_DataReceived_3; }
	inline void set_m_DataReceived_3(EventHandler_1_t4058788791 * value)
	{
		___m_DataReceived_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_DataReceived_3, value);
	}

	inline static int32_t get_offset_of_m_DataArgs_4() { return static_cast<int32_t>(offsetof(ClientSession_t3100954378, ___m_DataArgs_4)); }
	inline DataEventArgs_t3216211148 * get_m_DataArgs_4() const { return ___m_DataArgs_4; }
	inline DataEventArgs_t3216211148 ** get_address_of_m_DataArgs_4() { return &___m_DataArgs_4; }
	inline void set_m_DataArgs_4(DataEventArgs_t3216211148 * value)
	{
		___m_DataArgs_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_DataArgs_4, value);
	}

	inline static int32_t get_offset_of_U3CClientU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ClientSession_t3100954378, ___U3CClientU3Ek__BackingField_5)); }
	inline Socket_t150013987 * get_U3CClientU3Ek__BackingField_5() const { return ___U3CClientU3Ek__BackingField_5; }
	inline Socket_t150013987 ** get_address_of_U3CClientU3Ek__BackingField_5() { return &___U3CClientU3Ek__BackingField_5; }
	inline void set_U3CClientU3Ek__BackingField_5(Socket_t150013987 * value)
	{
		___U3CClientU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CClientU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CRemoteEndPointU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ClientSession_t3100954378, ___U3CRemoteEndPointU3Ek__BackingField_6)); }
	inline EndPoint_t1294049535 * get_U3CRemoteEndPointU3Ek__BackingField_6() const { return ___U3CRemoteEndPointU3Ek__BackingField_6; }
	inline EndPoint_t1294049535 ** get_address_of_U3CRemoteEndPointU3Ek__BackingField_6() { return &___U3CRemoteEndPointU3Ek__BackingField_6; }
	inline void set_U3CRemoteEndPointU3Ek__BackingField_6(EndPoint_t1294049535 * value)
	{
		___U3CRemoteEndPointU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRemoteEndPointU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CIsConnectedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ClientSession_t3100954378, ___U3CIsConnectedU3Ek__BackingField_7)); }
	inline bool get_U3CIsConnectedU3Ek__BackingField_7() const { return ___U3CIsConnectedU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsConnectedU3Ek__BackingField_7() { return &___U3CIsConnectedU3Ek__BackingField_7; }
	inline void set_U3CIsConnectedU3Ek__BackingField_7(bool value)
	{
		___U3CIsConnectedU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CReceiveBufferSizeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ClientSession_t3100954378, ___U3CReceiveBufferSizeU3Ek__BackingField_8)); }
	inline int32_t get_U3CReceiveBufferSizeU3Ek__BackingField_8() const { return ___U3CReceiveBufferSizeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CReceiveBufferSizeU3Ek__BackingField_8() { return &___U3CReceiveBufferSizeU3Ek__BackingField_8; }
	inline void set_U3CReceiveBufferSizeU3Ek__BackingField_8(int32_t value)
	{
		___U3CReceiveBufferSizeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ClientSession_t3100954378, ___U3CProxyU3Ek__BackingField_9)); }
	inline Il2CppObject * get_U3CProxyU3Ek__BackingField_9() const { return ___U3CProxyU3Ek__BackingField_9; }
	inline Il2CppObject ** get_address_of_U3CProxyU3Ek__BackingField_9() { return &___U3CProxyU3Ek__BackingField_9; }
	inline void set_U3CProxyU3Ek__BackingField_9(Il2CppObject * value)
	{
		___U3CProxyU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProxyU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CBufferU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ClientSession_t3100954378, ___U3CBufferU3Ek__BackingField_10)); }
	inline ArraySegment_1_t2801744866  get_U3CBufferU3Ek__BackingField_10() const { return ___U3CBufferU3Ek__BackingField_10; }
	inline ArraySegment_1_t2801744866 * get_address_of_U3CBufferU3Ek__BackingField_10() { return &___U3CBufferU3Ek__BackingField_10; }
	inline void set_U3CBufferU3Ek__BackingField_10(ArraySegment_1_t2801744866  value)
	{
		___U3CBufferU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
