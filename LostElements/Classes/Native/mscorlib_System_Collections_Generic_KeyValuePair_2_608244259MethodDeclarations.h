﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1873802457(__this, ___key, ___value, method) ((  void (*) (KeyValuePair_2_t608244259 *, String_t*, Action_1_t3776982353 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::get_Key()
#define KeyValuePair_2_get_Key_m1105995375(__this, method) ((  String_t* (*) (KeyValuePair_2_t608244259 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2525888944(__this, ___value, method) ((  void (*) (KeyValuePair_2_t608244259 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::get_Value()
#define KeyValuePair_2_get_Value_m3623661139(__this, method) ((  Action_1_t3776982353 * (*) (KeyValuePair_2_t608244259 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2880503216(__this, ___value, method) ((  void (*) (KeyValuePair_2_t608244259 *, Action_1_t3776982353 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::ToString()
#define KeyValuePair_2_ToString_m2301546968(__this, method) ((  String_t* (*) (KeyValuePair_2_t608244259 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
