﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.PocoJsonSerializerStrategy
struct PocoJsonSerializerStrategy_t1373207638;
// System.Type
struct Type_t;
// SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>
struct SafeDictionary_2_t2250466201;
// System.Object
struct Il2CppObject;
// System.Enum
struct Enum_t2778772662;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Enum2778772662.h"

// System.Void SimpleJson.PocoJsonSerializerStrategy::.ctor()
extern "C"  void PocoJsonSerializerStrategy__ctor_m1303007027 (PocoJsonSerializerStrategy_t1373207638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJson.PocoJsonSerializerStrategy::BuildMap(System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>)
extern "C"  void PocoJsonSerializerStrategy_BuildMap_m2906231328 (PocoJsonSerializerStrategy_t1373207638 * __this, Type_t * ___type, SafeDictionary_2_t2250466201 * ___memberMaps, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::SerializeNonPrimitiveObject(System.Object,System.Object&)
extern "C"  bool PocoJsonSerializerStrategy_SerializeNonPrimitiveObject_m2970911232 (PocoJsonSerializerStrategy_t1373207638 * __this, Il2CppObject * ___input, Il2CppObject ** ___output, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJson.PocoJsonSerializerStrategy::DeserializeObject(System.Object,System.Type)
extern "C"  Il2CppObject * PocoJsonSerializerStrategy_DeserializeObject_m59513699 (PocoJsonSerializerStrategy_t1373207638 * __this, Il2CppObject * ___value, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJson.PocoJsonSerializerStrategy::SerializeEnum(System.Enum)
extern "C"  Il2CppObject * PocoJsonSerializerStrategy_SerializeEnum_m3979086379 (PocoJsonSerializerStrategy_t1373207638 * __this, Enum_t2778772662 * ___p, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeKnownTypes(System.Object,System.Object&)
extern "C"  bool PocoJsonSerializerStrategy_TrySerializeKnownTypes_m484211082 (PocoJsonSerializerStrategy_t1373207638 * __this, Il2CppObject * ___input, Il2CppObject ** ___output, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeUnknownTypes(System.Object,System.Object&)
extern "C"  bool PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m3846546659 (PocoJsonSerializerStrategy_t1373207638 * __this, Il2CppObject * ___input, Il2CppObject ** ___output, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJson.PocoJsonSerializerStrategy::.cctor()
extern "C"  void PocoJsonSerializerStrategy__cctor_m1256415962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
