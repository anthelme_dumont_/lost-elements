﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.SortContext`1<System.Int32>
struct SortContext_1_t2080786391;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_SortDirection2805156518.h"

// System.Void System.Linq.SortContext`1<System.Int32>::.ctor(System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortContext_1__ctor_m1775961834_gshared (SortContext_1_t2080786391 * __this, int32_t ___direction, SortContext_1_t2080786391 * ___child_context, const MethodInfo* method);
#define SortContext_1__ctor_m1775961834(__this, ___direction, ___child_context, method) ((  void (*) (SortContext_1_t2080786391 *, int32_t, SortContext_1_t2080786391 *, const MethodInfo*))SortContext_1__ctor_m1775961834_gshared)(__this, ___direction, ___child_context, method)
