﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.DnsEndPoint
struct DnsEndPoint_t1814926600;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.String System.Net.DnsEndPoint::get_Host()
extern "C"  String_t* DnsEndPoint_get_Host_m1042524665 (DnsEndPoint_t1814926600 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.DnsEndPoint::set_Host(System.String)
extern "C"  void DnsEndPoint_set_Host_m3137319876 (DnsEndPoint_t1814926600 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.DnsEndPoint::get_Port()
extern "C"  int32_t DnsEndPoint_get_Port_m2270576877 (DnsEndPoint_t1814926600 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.DnsEndPoint::set_Port(System.Int32)
extern "C"  void DnsEndPoint_set_Port_m240530280 (DnsEndPoint_t1814926600 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.DnsEndPoint::.ctor(System.String,System.Int32)
extern "C"  void DnsEndPoint__ctor_m1299091296 (DnsEndPoint_t1814926600 * __this, String_t* ___host, int32_t ___port, const MethodInfo* method) IL2CPP_METHOD_ATTR;
