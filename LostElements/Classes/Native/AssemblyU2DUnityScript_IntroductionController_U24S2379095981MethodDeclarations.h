﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IntroductionController/$Start$3
struct U24StartU243_t2379095981;
// IntroductionController
struct IntroductionController_t4287196022;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t2774239688;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_IntroductionController4287196022.h"

// System.Void IntroductionController/$Start$3::.ctor(IntroductionController)
extern "C"  void U24StartU243__ctor_m3555563002 (U24StartU243_t2379095981 * __this, IntroductionController_t4287196022 * ___self_, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> IntroductionController/$Start$3::GetEnumerator()
extern "C"  Il2CppObject* U24StartU243_GetEnumerator_m3172959872 (U24StartU243_t2379095981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
