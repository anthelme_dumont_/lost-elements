﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_SocketIOClient_Messages_Message957426777.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIOClient.Messages.TextMessage
struct  TextMessage_t120705644  : public Message_t957426777
{
public:
	// System.String SocketIOClient.Messages.TextMessage::eventName
	String_t* ___eventName_10;

public:
	inline static int32_t get_offset_of_eventName_10() { return static_cast<int32_t>(offsetof(TextMessage_t120705644, ___eventName_10)); }
	inline String_t* get_eventName_10() const { return ___eventName_10; }
	inline String_t** get_address_of_eventName_10() { return &___eventName_10; }
	inline void set_eventName_10(String_t* value)
	{
		___eventName_10 = value;
		Il2CppCodeGenWriteBarrier(&___eventName_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
