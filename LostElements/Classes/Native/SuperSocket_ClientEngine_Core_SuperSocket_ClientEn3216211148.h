﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "mscorlib_System_EventArgs516466188.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.DataEventArgs
struct  DataEventArgs_t3216211148  : public EventArgs_t516466188
{
public:
	// System.Byte[] SuperSocket.ClientEngine.DataEventArgs::<Data>k__BackingField
	ByteU5BU5D_t58506160* ___U3CDataU3Ek__BackingField_1;
	// System.Int32 SuperSocket.ClientEngine.DataEventArgs::<Offset>k__BackingField
	int32_t ___U3COffsetU3Ek__BackingField_2;
	// System.Int32 SuperSocket.ClientEngine.DataEventArgs::<Length>k__BackingField
	int32_t ___U3CLengthU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DataEventArgs_t3216211148, ___U3CDataU3Ek__BackingField_1)); }
	inline ByteU5BU5D_t58506160* get_U3CDataU3Ek__BackingField_1() const { return ___U3CDataU3Ek__BackingField_1; }
	inline ByteU5BU5D_t58506160** get_address_of_U3CDataU3Ek__BackingField_1() { return &___U3CDataU3Ek__BackingField_1; }
	inline void set_U3CDataU3Ek__BackingField_1(ByteU5BU5D_t58506160* value)
	{
		___U3CDataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDataU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3COffsetU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DataEventArgs_t3216211148, ___U3COffsetU3Ek__BackingField_2)); }
	inline int32_t get_U3COffsetU3Ek__BackingField_2() const { return ___U3COffsetU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3COffsetU3Ek__BackingField_2() { return &___U3COffsetU3Ek__BackingField_2; }
	inline void set_U3COffsetU3Ek__BackingField_2(int32_t value)
	{
		___U3COffsetU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DataEventArgs_t3216211148, ___U3CLengthU3Ek__BackingField_3)); }
	inline int32_t get_U3CLengthU3Ek__BackingField_3() const { return ___U3CLengthU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CLengthU3Ek__BackingField_3() { return &___U3CLengthU3Ek__BackingField_3; }
	inline void set_U3CLengthU3Ek__BackingField_3(int32_t value)
	{
		___U3CLengthU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
