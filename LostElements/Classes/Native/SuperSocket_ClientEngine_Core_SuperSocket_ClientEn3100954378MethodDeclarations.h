﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.ClientSession
struct ClientSession_t3100954378;
// System.Net.Sockets.Socket
struct Socket_t150013987;
// System.Net.EndPoint
struct EndPoint_t1294049535;
// System.EventHandler
struct EventHandler_t247020293;
// System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>
struct EventHandler_1_t2171844441;
// System.Exception
struct Exception_t1967233988;
// System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>
struct EventHandler_1_t4058788791;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// SuperSocket.ClientEngine.IProxyConnector
struct IProxyConnector_t1374585095;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Sockets_Socket150013987.h"
#include "System_System_Net_EndPoint1294049535.h"
#include "mscorlib_System_EventHandler247020293.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_ArraySegment_1_gen2801744866.h"

// System.Net.Sockets.Socket SuperSocket.ClientEngine.ClientSession::get_Client()
extern "C"  Socket_t150013987 * ClientSession_get_Client_m275537367 (ClientSession_t3100954378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::set_Client(System.Net.Sockets.Socket)
extern "C"  void ClientSession_set_Client_m911781880 (ClientSession_t3100954378 * __this, Socket_t150013987 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.EndPoint SuperSocket.ClientEngine.ClientSession::get_RemoteEndPoint()
extern "C"  EndPoint_t1294049535 * ClientSession_get_RemoteEndPoint_m2153238163 (ClientSession_t3100954378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::set_RemoteEndPoint(System.Net.EndPoint)
extern "C"  void ClientSession_set_RemoteEndPoint_m4259102164 (ClientSession_t3100954378 * __this, EndPoint_t1294049535 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::set_IsConnected(System.Boolean)
extern "C"  void ClientSession_set_IsConnected_m477764794 (ClientSession_t3100954378 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::.ctor(System.Net.EndPoint)
extern "C"  void ClientSession__ctor_m2648651594 (ClientSession_t3100954378 * __this, EndPoint_t1294049535 * ___remoteEndPoint, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::add_Closed(System.EventHandler)
extern "C"  void ClientSession_add_Closed_m3100078168 (ClientSession_t3100954378 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::remove_Closed(System.EventHandler)
extern "C"  void ClientSession_remove_Closed_m1111740091 (ClientSession_t3100954378 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::OnClosed()
extern "C"  void ClientSession_OnClosed_m4144825100 (ClientSession_t3100954378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::add_Error(System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>)
extern "C"  void ClientSession_add_Error_m1151993633 (ClientSession_t3100954378 * __this, EventHandler_1_t2171844441 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::remove_Error(System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>)
extern "C"  void ClientSession_remove_Error_m3793664452 (ClientSession_t3100954378 * __this, EventHandler_1_t2171844441 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception)
extern "C"  void ClientSession_OnError_m1645804090 (ClientSession_t3100954378 * __this, Exception_t1967233988 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::add_Connected(System.EventHandler)
extern "C"  void ClientSession_add_Connected_m2440294935 (ClientSession_t3100954378 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::remove_Connected(System.EventHandler)
extern "C"  void ClientSession_remove_Connected_m4049589460 (ClientSession_t3100954378 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::OnConnected()
extern "C"  void ClientSession_OnConnected_m3607001547 (ClientSession_t3100954378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::add_DataReceived(System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>)
extern "C"  void ClientSession_add_DataReceived_m489756946 (ClientSession_t3100954378 * __this, EventHandler_1_t4058788791 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::remove_DataReceived(System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>)
extern "C"  void ClientSession_remove_DataReceived_m2540701045 (ClientSession_t3100954378 * __this, EventHandler_1_t4058788791 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::OnDataReceived(System.Byte[],System.Int32,System.Int32)
extern "C"  void ClientSession_OnDataReceived_m1273102142 (ClientSession_t3100954378 * __this, ByteU5BU5D_t58506160* ___data, int32_t ___offset, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SuperSocket.ClientEngine.ClientSession::get_ReceiveBufferSize()
extern "C"  int32_t ClientSession_get_ReceiveBufferSize_m487623082 (ClientSession_t3100954378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::set_ReceiveBufferSize(System.Int32)
extern "C"  void ClientSession_set_ReceiveBufferSize_m2308395257 (ClientSession_t3100954378 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SuperSocket.ClientEngine.IProxyConnector SuperSocket.ClientEngine.ClientSession::get_Proxy()
extern "C"  Il2CppObject * ClientSession_get_Proxy_m2237735788 (ClientSession_t3100954378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::set_Proxy(SuperSocket.ClientEngine.IProxyConnector)
extern "C"  void ClientSession_set_Proxy_m3800103115 (ClientSession_t3100954378 * __this, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ArraySegment`1<System.Byte> SuperSocket.ClientEngine.ClientSession::get_Buffer()
extern "C"  ArraySegment_1_t2801744866  ClientSession_get_Buffer_m4110712514 (ClientSession_t3100954378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ClientSession::set_Buffer(System.ArraySegment`1<System.Byte>)
extern "C"  void ClientSession_set_Buffer_m634249301 (ClientSession_t3100954378 * __this, ArraySegment_1_t2801744866  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
