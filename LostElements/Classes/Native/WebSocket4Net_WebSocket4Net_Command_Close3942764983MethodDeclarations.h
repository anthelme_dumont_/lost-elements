﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Command.Close
struct Close_t3942764983;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketCommandInfo3536916738.h"

// System.Void WebSocket4Net.Command.Close::ExecuteCommand(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo)
extern "C"  void Close_ExecuteCommand_m1102373333 (Close_t3942764983 * __this, WebSocket_t713846903 * ___session, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.Command.Close::get_Name()
extern "C"  String_t* Close_get_Name_m3509318595 (Close_t3942764983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Command.Close::.ctor()
extern "C"  void Close__ctor_m2113310610 (Close_t3942764983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
