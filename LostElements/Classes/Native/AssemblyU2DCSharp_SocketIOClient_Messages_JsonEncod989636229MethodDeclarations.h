﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Messages.JsonEncodedEventMessage
struct JsonEncodedEventMessage_t989636229;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void SocketIOClient.Messages.JsonEncodedEventMessage::.ctor()
extern "C"  void JsonEncodedEventMessage__ctor_m614799024 (JsonEncodedEventMessage_t989636229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.JsonEncodedEventMessage::.ctor(System.String,System.Object)
extern "C"  void JsonEncodedEventMessage__ctor_m1402911136 (JsonEncodedEventMessage_t989636229 * __this, String_t* ___name, Il2CppObject * ___payload, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.JsonEncodedEventMessage::.ctor(System.String,System.Object[])
extern "C"  void JsonEncodedEventMessage__ctor_m3872889022 (JsonEncodedEventMessage_t989636229 * __this, String_t* ___name, ObjectU5BU5D_t11523773* ___payloads, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.JsonEncodedEventMessage::get_name()
extern "C"  String_t* JsonEncodedEventMessage_get_name_m2613943237 (JsonEncodedEventMessage_t989636229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.JsonEncodedEventMessage::set_name(System.String)
extern "C"  void JsonEncodedEventMessage_set_name_m3782929222 (JsonEncodedEventMessage_t989636229 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] SocketIOClient.Messages.JsonEncodedEventMessage::get_args()
extern "C"  ObjectU5BU5D_t11523773* JsonEncodedEventMessage_get_args_m908799847 (JsonEncodedEventMessage_t989636229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.JsonEncodedEventMessage::set_args(System.Object[])
extern "C"  void JsonEncodedEventMessage_set_args_m3437467364 (JsonEncodedEventMessage_t989636229 * __this, ObjectU5BU5D_t11523773* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.JsonEncodedEventMessage::ToJsonString()
extern "C"  String_t* JsonEncodedEventMessage_ToJsonString_m4146288133 (JsonEncodedEventMessage_t989636229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.JsonEncodedEventMessage SocketIOClient.Messages.JsonEncodedEventMessage::Deserialize(System.String)
extern "C"  JsonEncodedEventMessage_t989636229 * JsonEncodedEventMessage_Deserialize_m1080785149 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
