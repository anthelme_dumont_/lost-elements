﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.TimeZoneNotFoundException
struct TimeZoneNotFoundException_t3613219068;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.TimeZoneNotFoundException::.ctor()
extern "C"  void TimeZoneNotFoundException__ctor_m636423683 (TimeZoneNotFoundException_t3613219068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.TimeZoneNotFoundException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TimeZoneNotFoundException__ctor_m4152878276 (TimeZoneNotFoundException_t3613219068 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___sc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
