﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "mscorlib_System_EventArgs516466188.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.DataReceivedEventArgs
struct  DataReceivedEventArgs_t1979092924  : public EventArgs_t516466188
{
public:
	// System.Byte[] WebSocket4Net.DataReceivedEventArgs::<Data>k__BackingField
	ByteU5BU5D_t58506160* ___U3CDataU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DataReceivedEventArgs_t1979092924, ___U3CDataU3Ek__BackingField_1)); }
	inline ByteU5BU5D_t58506160* get_U3CDataU3Ek__BackingField_1() const { return ___U3CDataU3Ek__BackingField_1; }
	inline ByteU5BU5D_t58506160** get_address_of_U3CDataU3Ek__BackingField_1() { return &___U3CDataU3Ek__BackingField_1; }
	inline void set_U3CDataU3Ek__BackingField_1(ByteU5BU5D_t58506160* value)
	{
		___U3CDataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDataU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
