﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// WebSocket4Net.Protocol.WebSocketDataFrame
struct WebSocketDataFrame_t222733806;
// WebSocket4Net.Protocol.IProtocolProcessor
struct IProtocolProcessor_t4208156067;

#include "mscorlib_System_Array2840145358.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_WebSocketDataF222733806.h"

#pragma once
// WebSocket4Net.Protocol.WebSocketDataFrame[]
struct WebSocketDataFrameU5BU5D_t584879771  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) WebSocketDataFrame_t222733806 * m_Items[1];

public:
	inline WebSocketDataFrame_t222733806 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WebSocketDataFrame_t222733806 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WebSocketDataFrame_t222733806 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WebSocket4Net.Protocol.IProtocolProcessor[]
struct IProtocolProcessorU5BU5D_t2571124850  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
