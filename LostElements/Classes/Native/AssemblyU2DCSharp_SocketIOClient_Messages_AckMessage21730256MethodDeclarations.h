﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Messages.AckMessage
struct AckMessage_t21730256;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SocketIOClient.Messages.AckMessage::.ctor()
extern "C"  void AckMessage__ctor_m760400901 (AckMessage_t21730256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.AckMessage::.cctor()
extern "C"  void AckMessage__cctor_m1615495240 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SocketIOClient.Messages.AckMessage::get_NextAckID()
extern "C"  int32_t AckMessage_get_NextAckID_m1593256669 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.AckMessage SocketIOClient.Messages.AckMessage::Deserialize(System.String)
extern "C"  AckMessage_t21730256 * AckMessage_Deserialize_m3207619207 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.AckMessage::get_Encoded()
extern "C"  String_t* AckMessage_get_Encoded_m1476365253 (AckMessage_t21730256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
