﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.SocketIOHandshake
struct SocketIOHandshake_t3315670474;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void SocketIOClient.SocketIOHandshake::.ctor()
extern "C"  void SocketIOHandshake__ctor_m2023930727 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.SocketIOHandshake::get_SID()
extern "C"  String_t* SocketIOHandshake_get_SID_m3604612263 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.SocketIOHandshake::set_SID(System.String)
extern "C"  void SocketIOHandshake_set_SID_m776236044 (SocketIOHandshake_t3315670474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SocketIOClient.SocketIOHandshake::get_HeartbeatTimeout()
extern "C"  int32_t SocketIOHandshake_get_HeartbeatTimeout_m1020887065 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.SocketIOHandshake::set_HeartbeatTimeout(System.Int32)
extern "C"  void SocketIOHandshake_set_HeartbeatTimeout_m1801981520 (SocketIOHandshake_t3315670474 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.SocketIOHandshake::get_ErrorMessage()
extern "C"  String_t* SocketIOHandshake_get_ErrorMessage_m282465992 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.SocketIOHandshake::set_ErrorMessage(System.String)
extern "C"  void SocketIOHandshake_set_ErrorMessage_m2459766281 (SocketIOHandshake_t3315670474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SocketIOClient.SocketIOHandshake::get_HadError()
extern "C"  bool SocketIOHandshake_get_HadError_m2556394295 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan SocketIOClient.SocketIOHandshake::get_HeartbeatInterval()
extern "C"  TimeSpan_t763862892  SocketIOHandshake_get_HeartbeatInterval_m4089311776 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SocketIOClient.SocketIOHandshake::get_ConnectionTimeout()
extern "C"  int32_t SocketIOHandshake_get_ConnectionTimeout_m586544049 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.SocketIOHandshake::set_ConnectionTimeout(System.Int32)
extern "C"  void SocketIOHandshake_set_ConnectionTimeout_m3774365148 (SocketIOHandshake_t3315670474 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.SocketIOHandshake SocketIOClient.SocketIOHandshake::LoadFromString(System.String)
extern "C"  SocketIOHandshake_t3315670474 * SocketIOHandshake_LoadFromString_m766272635 (Il2CppObject * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
