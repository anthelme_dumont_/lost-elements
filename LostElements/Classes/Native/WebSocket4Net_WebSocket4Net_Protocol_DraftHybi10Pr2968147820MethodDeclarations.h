﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.DraftHybi10Processor
struct DraftHybi10Processor_t2968147820;
// WebSocket4Net.Protocol.ICloseStatusCode
struct ICloseStatusCode_t28582816;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// WebSocket4Net.Protocol.ReaderBase
struct ReaderBase_t893022310;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketVersion3981039553.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903.h"
#include "mscorlib_System_String968488902.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketCommandInfo3536916738.h"

// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::.ctor()
extern "C"  void DraftHybi10Processor__ctor_m2859170935 (DraftHybi10Processor_t2968147820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::.ctor(WebSocket4Net.WebSocketVersion,WebSocket4Net.Protocol.ICloseStatusCode)
extern "C"  void DraftHybi10Processor__ctor_m3986497774 (DraftHybi10Processor_t2968147820 * __this, int32_t ___version, Il2CppObject * ___closeStatusCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendHandshake(WebSocket4Net.WebSocket)
extern "C"  void DraftHybi10Processor_SendHandshake_m1468137349 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.Protocol.ReaderBase WebSocket4Net.Protocol.DraftHybi10Processor::CreateHandshakeReader(WebSocket4Net.WebSocket)
extern "C"  ReaderBase_t893022310 * DraftHybi10Processor_CreateHandshakeReader_m2909912431 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendMessage(WebSocket4Net.WebSocket,System.Int32,System.String)
extern "C"  void DraftHybi10Processor_SendMessage_m712760638 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, int32_t ___opCode, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocket4Net.Protocol.DraftHybi10Processor::EncodeDataFrame(System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t58506160* DraftHybi10Processor_EncodeDataFrame_m854661280 (DraftHybi10Processor_t2968147820 * __this, int32_t ___opCode, ByteU5BU5D_t58506160* ___playloadData, int32_t ___offset, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendDataFragment(WebSocket4Net.WebSocket,System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  void DraftHybi10Processor_SendDataFragment_m1842464672 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, int32_t ___opCode, ByteU5BU5D_t58506160* ___playloadData, int32_t ___offset, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendMessage(WebSocket4Net.WebSocket,System.String)
extern "C"  void DraftHybi10Processor_SendMessage_m3031038449 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendCloseHandshake(WebSocket4Net.WebSocket,System.Int32,System.String)
extern "C"  void DraftHybi10Processor_SendCloseHandshake_m193522878 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, int32_t ___statusCode, String_t* ___closeReason, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendPing(WebSocket4Net.WebSocket,System.String)
extern "C"  void DraftHybi10Processor_SendPing_m916911486 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, String_t* ___ping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::SendPong(WebSocket4Net.WebSocket,System.String)
extern "C"  void DraftHybi10Processor_SendPong_m1661350968 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, String_t* ___pong, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.Protocol.DraftHybi10Processor::VerifyHandshake(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo,System.String&)
extern "C"  bool DraftHybi10Processor_VerifyHandshake_m3185403576 (DraftHybi10Processor_t2968147820 * __this, WebSocket_t713846903 * ___websocket, WebSocketCommandInfo_t3536916738 * ___handshakeInfo, String_t** ___description, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.Protocol.DraftHybi10Processor::get_SupportPingPong()
extern "C"  bool DraftHybi10Processor_get_SupportPingPong_m3340585117 (DraftHybi10Processor_t2968147820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::GenerateMask(System.Byte[],System.Int32)
extern "C"  void DraftHybi10Processor_GenerateMask_m2250987996 (DraftHybi10Processor_t2968147820 * __this, ByteU5BU5D_t58506160* ___mask, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::MaskData(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32,System.Byte[],System.Int32)
extern "C"  void DraftHybi10Processor_MaskData_m3859186278 (DraftHybi10Processor_t2968147820 * __this, ByteU5BU5D_t58506160* ___rawData, int32_t ___offset, int32_t ___length, ByteU5BU5D_t58506160* ___outputData, int32_t ___outputOffset, ByteU5BU5D_t58506160* ___mask, int32_t ___maskOffset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi10Processor::.cctor()
extern "C"  void DraftHybi10Processor__cctor_m2252856854 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
