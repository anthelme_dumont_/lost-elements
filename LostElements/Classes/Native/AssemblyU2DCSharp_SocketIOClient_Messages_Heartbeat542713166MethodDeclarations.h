﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Messages.Heartbeat
struct Heartbeat_t542713166;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void SocketIOClient.Messages.Heartbeat::.ctor()
extern "C"  void Heartbeat__ctor_m2318316039 (Heartbeat_t542713166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.Heartbeat::.cctor()
extern "C"  void Heartbeat__cctor_m2666224262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.Heartbeat::get_Encoded()
extern "C"  String_t* Heartbeat_get_Encoded_m1904079917 (Heartbeat_t542713166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
