﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_SocketIOClient_Messages_Message957426777.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIOClient.Messages.Heartbeat
struct  Heartbeat_t542713166  : public Message_t957426777
{
public:

public:
};

struct Heartbeat_t542713166_StaticFields
{
public:
	// System.String SocketIOClient.Messages.Heartbeat::HEARTBEAT
	String_t* ___HEARTBEAT_10;

public:
	inline static int32_t get_offset_of_HEARTBEAT_10() { return static_cast<int32_t>(offsetof(Heartbeat_t542713166_StaticFields, ___HEARTBEAT_10)); }
	inline String_t* get_HEARTBEAT_10() const { return ___HEARTBEAT_10; }
	inline String_t** get_address_of_HEARTBEAT_10() { return &___HEARTBEAT_10; }
	inline void set_HEARTBEAT_10(String_t* value)
	{
		___HEARTBEAT_10 = value;
		Il2CppCodeGenWriteBarrier(&___HEARTBEAT_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
