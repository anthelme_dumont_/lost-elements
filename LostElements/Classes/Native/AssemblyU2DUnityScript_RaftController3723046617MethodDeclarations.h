﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RaftController
struct RaftController_t3723046617;

#include "codegen/il2cpp-codegen.h"

// System.Void RaftController::.ctor()
extern "C"  void RaftController__ctor_m1820334231 (RaftController_t3723046617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaftController::Start()
extern "C"  void RaftController_Start_m767472023 (RaftController_t3723046617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaftController::Update()
extern "C"  void RaftController_Update_m2322648406 (RaftController_t3723046617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaftController::Main()
extern "C"  void RaftController_Main_m1221140806 (RaftController_t3723046617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
