﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Single ExtensionMethods::Map(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  float ExtensionMethods_Map_m3708687785 (Il2CppObject * __this /* static, unused */, float ___value, float ___fromSource, float ___toSource, float ___fromTarget, float ___toTarget, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExtensionMethods::Round(System.Single)
extern "C"  float ExtensionMethods_Round_m1332053347 (Il2CppObject * __this /* static, unused */, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
