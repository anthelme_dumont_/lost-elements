﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Command.Binary
struct Binary_t1572461568;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketCommandInfo3536916738.h"

// System.Void WebSocket4Net.Command.Binary::ExecuteCommand(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo)
extern "C"  void Binary_ExecuteCommand_m979543116 (Binary_t1572461568 * __this, WebSocket_t713846903 * ___session, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.Command.Binary::get_Name()
extern "C"  String_t* Binary_get_Name_m2304013792 (Binary_t1572461568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Command.Binary::.ctor()
extern "C"  void Binary__ctor_m1144881275 (Binary_t1572461568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
