﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Concurrent.ConcurrentQueue`1<System.Object>
struct ConcurrentQueue_1_t570712889;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Concurrent.ConcurrentQueue`1<System.Object>::.ctor()
extern "C"  void ConcurrentQueue_1__ctor_m3561420925_gshared (ConcurrentQueue_1_t570712889 * __this, const MethodInfo* method);
#define ConcurrentQueue_1__ctor_m3561420925(__this, method) ((  void (*) (ConcurrentQueue_1_t570712889 *, const MethodInfo*))ConcurrentQueue_1__ctor_m3561420925_gshared)(__this, method)
// System.Void System.Collections.Concurrent.ConcurrentQueue`1<System.Object>::Enqueue(T)
extern "C"  void ConcurrentQueue_1_Enqueue_m879009435_gshared (ConcurrentQueue_1_t570712889 * __this, Il2CppObject * ___item, const MethodInfo* method);
#define ConcurrentQueue_1_Enqueue_m879009435(__this, ___item, method) ((  void (*) (ConcurrentQueue_1_t570712889 *, Il2CppObject *, const MethodInfo*))ConcurrentQueue_1_Enqueue_m879009435_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Concurrent.ConcurrentQueue`1<System.Object>::TryDequeue(T&)
extern "C"  bool ConcurrentQueue_1_TryDequeue_m1762985058_gshared (ConcurrentQueue_1_t570712889 * __this, Il2CppObject ** ___item, const MethodInfo* method);
#define ConcurrentQueue_1_TryDequeue_m1762985058(__this, ___item, method) ((  bool (*) (ConcurrentQueue_1_t570712889 *, Il2CppObject **, const MethodInfo*))ConcurrentQueue_1_TryDequeue_m1762985058_gshared)(__this, ___item, method)
