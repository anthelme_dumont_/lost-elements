﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "AssemblyU2DCSharp_SocketIOClient_Messages_Message957426777.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIOClient.Messages.EventMessage
struct  EventMessage_t365350047  : public Message_t957426777
{
public:
	// System.Action`1<System.Object> SocketIOClient.Messages.EventMessage::Callback
	Action_1_t985559125 * ___Callback_12;

public:
	inline static int32_t get_offset_of_Callback_12() { return static_cast<int32_t>(offsetof(EventMessage_t365350047, ___Callback_12)); }
	inline Action_1_t985559125 * get_Callback_12() const { return ___Callback_12; }
	inline Action_1_t985559125 ** get_address_of_Callback_12() { return &___Callback_12; }
	inline void set_Callback_12(Action_1_t985559125 * value)
	{
		___Callback_12 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_12, value);
	}
};

struct EventMessage_t365350047_StaticFields
{
public:
	// System.Object SocketIOClient.Messages.EventMessage::ackLock
	Il2CppObject * ___ackLock_10;
	// System.Int32 SocketIOClient.Messages.EventMessage::_akid
	int32_t ____akid_11;

public:
	inline static int32_t get_offset_of_ackLock_10() { return static_cast<int32_t>(offsetof(EventMessage_t365350047_StaticFields, ___ackLock_10)); }
	inline Il2CppObject * get_ackLock_10() const { return ___ackLock_10; }
	inline Il2CppObject ** get_address_of_ackLock_10() { return &___ackLock_10; }
	inline void set_ackLock_10(Il2CppObject * value)
	{
		___ackLock_10 = value;
		Il2CppCodeGenWriteBarrier(&___ackLock_10, value);
	}

	inline static int32_t get_offset_of__akid_11() { return static_cast<int32_t>(offsetof(EventMessage_t365350047_StaticFields, ____akid_11)); }
	inline int32_t get__akid_11() const { return ____akid_11; }
	inline int32_t* get_address_of__akid_11() { return &____akid_11; }
	inline void set__akid_11(int32_t value)
	{
		____akid_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
