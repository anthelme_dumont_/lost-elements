﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t3650470111;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void WebSocket4Net.Extensions::AppendFormatWithCrCf(System.Text.StringBuilder,System.String,System.Object)
extern "C"  void Extensions_AppendFormatWithCrCf_m759365938 (Il2CppObject * __this /* static, unused */, StringBuilder_t3822575854 * ___builder, String_t* ___format, Il2CppObject * ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Extensions::AppendFormatWithCrCf(System.Text.StringBuilder,System.String,System.Object[])
extern "C"  void Extensions_AppendFormatWithCrCf_m3901244368 (Il2CppObject * __this /* static, unused */, StringBuilder_t3822575854 * ___builder, String_t* ___format, ObjectU5BU5D_t11523773* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Extensions::AppendWithCrCf(System.Text.StringBuilder,System.String)
extern "C"  void Extensions_AppendWithCrCf_m3522543387 (Il2CppObject * __this /* static, unused */, StringBuilder_t3822575854 * ___builder, String_t* ___content, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Extensions::AppendWithCrCf(System.Text.StringBuilder)
extern "C"  void Extensions_AppendWithCrCf_m3929176351 (Il2CppObject * __this /* static, unused */, StringBuilder_t3822575854 * ___builder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.Extensions::ParseMimeHeader(System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C"  bool Extensions_ParseMimeHeader_m1946375443 (Il2CppObject * __this /* static, unused */, String_t* ___source, Il2CppObject* ___valueContainer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Extensions::.cctor()
extern "C"  void Extensions__cctor_m3284150402 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
