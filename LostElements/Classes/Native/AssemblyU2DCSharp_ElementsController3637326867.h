﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ElementsController
struct  ElementsController_t3637326867  : public Il2CppObject
{
public:

public:
};

struct ElementsController_t3637326867_StaticFields
{
public:
	// System.String ElementsController::activeElement
	String_t* ___activeElement_0;
	// System.Single ElementsController::intensityOfElement
	float ___intensityOfElement_1;
	// System.Single ElementsController::coefficientRealIntensity
	float ___coefficientRealIntensity_2;

public:
	inline static int32_t get_offset_of_activeElement_0() { return static_cast<int32_t>(offsetof(ElementsController_t3637326867_StaticFields, ___activeElement_0)); }
	inline String_t* get_activeElement_0() const { return ___activeElement_0; }
	inline String_t** get_address_of_activeElement_0() { return &___activeElement_0; }
	inline void set_activeElement_0(String_t* value)
	{
		___activeElement_0 = value;
		Il2CppCodeGenWriteBarrier(&___activeElement_0, value);
	}

	inline static int32_t get_offset_of_intensityOfElement_1() { return static_cast<int32_t>(offsetof(ElementsController_t3637326867_StaticFields, ___intensityOfElement_1)); }
	inline float get_intensityOfElement_1() const { return ___intensityOfElement_1; }
	inline float* get_address_of_intensityOfElement_1() { return &___intensityOfElement_1; }
	inline void set_intensityOfElement_1(float value)
	{
		___intensityOfElement_1 = value;
	}

	inline static int32_t get_offset_of_coefficientRealIntensity_2() { return static_cast<int32_t>(offsetof(ElementsController_t3637326867_StaticFields, ___coefficientRealIntensity_2)); }
	inline float get_coefficientRealIntensity_2() const { return ___coefficientRealIntensity_2; }
	inline float* get_address_of_coefficientRealIntensity_2() { return &___coefficientRealIntensity_2; }
	inline void set_coefficientRealIntensity_2(float value)
	{
		___coefficientRealIntensity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
