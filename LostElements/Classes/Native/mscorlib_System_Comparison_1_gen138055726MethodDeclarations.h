﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3540781296MethodDeclarations.h"

// System.Void System.Comparison`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m803507112(__this, ___object, ___method, method) ((  void (*) (Comparison_1_t138055726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>::Invoke(T,T)
#define Comparison_1_Invoke_m2458736792(__this, ___x, ___y, method) ((  int32_t (*) (Comparison_1_t138055726 *, ArraySegmentEx_1_t1729348146 *, ArraySegmentEx_1_t1729348146 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m3404611089(__this, ___x, ___y, ___callback, ___object, method) ((  Il2CppObject * (*) (Comparison_1_t138055726 *, ArraySegmentEx_1_t1729348146 *, ArraySegmentEx_1_t1729348146 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m3661766868(__this, ___result, method) ((  int32_t (*) (Comparison_1_t138055726 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result, method)
