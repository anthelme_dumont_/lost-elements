﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.TcpClientSession
struct TcpClientSession_t1301539049;
// System.String
struct String_t;
// System.Net.EndPoint
struct EndPoint_t1294049535;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;
// SuperSocket.ClientEngine.ProxyEventArgs
struct ProxyEventArgs_t3602520776;
// System.Net.Sockets.Socket
struct Socket_t150013987;
// System.Net.Sockets.SocketAsyncEventArgs
struct SocketAsyncEventArgs_t970431102;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Net_EndPoint1294049535.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client3602520776.h"
#include "System_System_Net_Sockets_Socket150013987.h"
#include "System_System_Net_Sockets_SocketAsyncEventArgs970431102.h"

// System.String SuperSocket.ClientEngine.TcpClientSession::get_HostName()
extern "C"  String_t* TcpClientSession_get_HostName_m3978347023 (TcpClientSession_t1301539049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.TcpClientSession::set_HostName(System.String)
extern "C"  void TcpClientSession_set_HostName_m311133692 (TcpClientSession_t1301539049 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.TcpClientSession::.ctor(System.Net.EndPoint)
extern "C"  void TcpClientSession__ctor_m2728456543 (TcpClientSession_t1301539049 * __this, EndPoint_t1294049535 * ___remoteEndPoint, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.TcpClientSession::.ctor(System.Net.EndPoint,System.Int32)
extern "C"  void TcpClientSession__ctor_m748658136 (TcpClientSession_t1301539049 * __this, EndPoint_t1294049535 * ___remoteEndPoint, int32_t ___receiveBufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SuperSocket.ClientEngine.TcpClientSession::get_ReceiveBufferSize()
extern "C"  int32_t TcpClientSession_get_ReceiveBufferSize_m1207241433 (TcpClientSession_t1301539049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.TcpClientSession::set_ReceiveBufferSize(System.Int32)
extern "C"  void TcpClientSession_set_ReceiveBufferSize_m3022855812 (TcpClientSession_t1301539049 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SuperSocket.ClientEngine.TcpClientSession::IsIgnorableException(System.Exception)
extern "C"  bool TcpClientSession_IsIgnorableException_m3961902810 (TcpClientSession_t1301539049 * __this, Exception_t1967233988 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SuperSocket.ClientEngine.TcpClientSession::IsIgnorableSocketError(System.Int32)
extern "C"  bool TcpClientSession_IsIgnorableSocketError_m1357922689 (TcpClientSession_t1301539049 * __this, int32_t ___errorCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.TcpClientSession::Connect()
extern "C"  void TcpClientSession_Connect_m1891457046 (TcpClientSession_t1301539049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.TcpClientSession::Proxy_Completed(System.Object,SuperSocket.ClientEngine.ProxyEventArgs)
extern "C"  void TcpClientSession_Proxy_Completed_m4143037264 (TcpClientSession_t1301539049 * __this, Il2CppObject * ___sender, ProxyEventArgs_t3602520776 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.TcpClientSession::ProcessConnect(System.Net.Sockets.Socket,System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern "C"  void TcpClientSession_ProcessConnect_m1201190206 (TcpClientSession_t1301539049 * __this, Socket_t150013987 * ___socket, Il2CppObject * ___state, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SuperSocket.ClientEngine.TcpClientSession::EnsureSocketClosed()
extern "C"  bool TcpClientSession_EnsureSocketClosed_m2842965627 (TcpClientSession_t1301539049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SuperSocket.ClientEngine.TcpClientSession::EnsureSocketClosed(System.Net.Sockets.Socket)
extern "C"  bool TcpClientSession_EnsureSocketClosed_m4142523302 (TcpClientSession_t1301539049 * __this, Socket_t150013987 * ___prevClient, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.TcpClientSession::DetectConnected()
extern "C"  void TcpClientSession_DetectConnected_m1627322578 (TcpClientSession_t1301539049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.TcpClientSession::Send(System.Byte[],System.Int32,System.Int32)
extern "C"  void TcpClientSession_Send_m1729004651 (TcpClientSession_t1301539049 * __this, ByteU5BU5D_t58506160* ___data, int32_t ___offset, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SuperSocket.ClientEngine.TcpClientSession::DequeueSend()
extern "C"  bool TcpClientSession_DequeueSend_m881139676 (TcpClientSession_t1301539049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.TcpClientSession::Close()
extern "C"  void TcpClientSession_Close_m3550666852 (TcpClientSession_t1301539049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
