﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState
struct SslAsyncState_t3174738561;
// System.Net.Security.SslStream
struct SslStream_t2729491676;
// System.Net.Sockets.Socket
struct Socket_t150013987;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Security_SslStream2729491676.h"
#include "System_System_Net_Sockets_Socket150013987.h"

// System.Net.Security.SslStream SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState::get_SslStream()
extern "C"  SslStream_t2729491676 * SslAsyncState_get_SslStream_m3690350103 (SslAsyncState_t3174738561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState::set_SslStream(System.Net.Security.SslStream)
extern "C"  void SslAsyncState_set_SslStream_m2061956892 (SslAsyncState_t3174738561 * __this, SslStream_t2729491676 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.Socket SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState::get_Client()
extern "C"  Socket_t150013987 * SslAsyncState_get_Client_m1672307775 (SslAsyncState_t3174738561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState::set_Client(System.Net.Sockets.Socket)
extern "C"  void SslAsyncState_set_Client_m2235287952 (SslAsyncState_t3174738561 * __this, Socket_t150013987 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState::.ctor()
extern "C"  void SslAsyncState__ctor_m1197978331 (SslAsyncState_t3174738561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
