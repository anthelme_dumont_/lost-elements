﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IntroductionController
struct IntroductionController_t4287196022;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen2359605680.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IntroductionController/$Start$3
struct  U24StartU243_t2379095981  : public GenericGenerator_1_t2359605680
{
public:
	// IntroductionController IntroductionController/$Start$3::$self_$6
	IntroductionController_t4287196022 * ___U24self_U246_0;

public:
	inline static int32_t get_offset_of_U24self_U246_0() { return static_cast<int32_t>(offsetof(U24StartU243_t2379095981, ___U24self_U246_0)); }
	inline IntroductionController_t4287196022 * get_U24self_U246_0() const { return ___U24self_U246_0; }
	inline IntroductionController_t4287196022 ** get_address_of_U24self_U246_0() { return &___U24self_U246_0; }
	inline void set_U24self_U246_0(IntroductionController_t4287196022 * value)
	{
		___U24self_U246_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U246_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
