﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.ProtocolProcessorBase
struct ProtocolProcessorBase_t894595261;
// WebSocket4Net.Protocol.ICloseStatusCode
struct ICloseStatusCode_t28582816;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketVersion3981039553.h"
#include "mscorlib_System_String968488902.h"

// System.Void WebSocket4Net.Protocol.ProtocolProcessorBase::.ctor(WebSocket4Net.WebSocketVersion,WebSocket4Net.Protocol.ICloseStatusCode)
extern "C"  void ProtocolProcessorBase__ctor_m448700961 (ProtocolProcessorBase_t894595261 * __this, int32_t ___version, Il2CppObject * ___closeStatusCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.Protocol.ICloseStatusCode WebSocket4Net.Protocol.ProtocolProcessorBase::get_CloseStatusCode()
extern "C"  Il2CppObject * ProtocolProcessorBase_get_CloseStatusCode_m779297321 (ProtocolProcessorBase_t894595261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.ProtocolProcessorBase::set_CloseStatusCode(WebSocket4Net.Protocol.ICloseStatusCode)
extern "C"  void ProtocolProcessorBase_set_CloseStatusCode_m3560475338 (ProtocolProcessorBase_t894595261 * __this, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.WebSocketVersion WebSocket4Net.Protocol.ProtocolProcessorBase::get_Version()
extern "C"  int32_t ProtocolProcessorBase_get_Version_m961545797 (ProtocolProcessorBase_t894595261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.ProtocolProcessorBase::set_Version(WebSocket4Net.WebSocketVersion)
extern "C"  void ProtocolProcessorBase_set_Version_m182824756 (ProtocolProcessorBase_t894595261 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.Protocol.ProtocolProcessorBase::get_VersionTag()
extern "C"  String_t* ProtocolProcessorBase_get_VersionTag_m3245039016 (ProtocolProcessorBase_t894595261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.ProtocolProcessorBase::set_VersionTag(System.String)
extern "C"  void ProtocolProcessorBase_set_VersionTag_m2556159721 (ProtocolProcessorBase_t894595261 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
