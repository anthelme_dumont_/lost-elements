﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.Reflection.CacheResolver/MemberMap
struct MemberMap_t3346170946;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782.h"

// System.Void SimpleJson.Reflection.CacheResolver/MemberMap::.ctor(System.Reflection.PropertyInfo)
extern "C"  void MemberMap__ctor_m3239711532 (MemberMap_t3346170946 * __this, PropertyInfo_t * ___propertyInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJson.Reflection.CacheResolver/MemberMap::.ctor(System.Reflection.FieldInfo)
extern "C"  void MemberMap__ctor_m3335798619 (MemberMap_t3346170946 * __this, FieldInfo_t * ___fieldInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
