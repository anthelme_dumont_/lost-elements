﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// SuperSocket.ClientEngine.AsyncTcpSession
struct AsyncTcpSession_t2860571088;
// System.Net.EndPoint
struct EndPoint_t1294049535;
// System.Object
struct Il2CppObject;
// System.Net.Sockets.SocketAsyncEventArgs
struct SocketAsyncEventArgs_t970431102;
// SuperSocket.ClientEngine.ClientSession
struct ClientSession_t3100954378;
// System.Net.Sockets.Socket
struct Socket_t150013987;
// System.EventHandler
struct EventHandler_t247020293;
// System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>
struct EventHandler_1_t2171844441;
// System.Exception
struct Exception_t1967233988;
// System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>
struct EventHandler_1_t4058788791;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// SuperSocket.ClientEngine.IProxyConnector
struct IProxyConnector_t1374585095;
// SuperSocket.ClientEngine.DataEventArgs
struct DataEventArgs_t3216211148;
// SuperSocket.ClientEngine.ErrorEventArgs
struct ErrorEventArgs_t1329266798;
// SuperSocket.ClientEngine.SslStreamTcpSession
struct SslStreamTcpSession_t2680237056;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t3432067208;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t2831591730;
// SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState
struct SslAsyncState_t3174738561;
// System.Net.Security.SslStream
struct SslStream_t2729491676;
// SuperSocket.ClientEngine.TcpClientSession
struct TcpClientSession_t1301539049;
// System.String
struct String_t;
// SuperSocket.ClientEngine.ProxyEventArgs
struct ProxyEventArgs_t3602520776;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "SuperSocket_ClientEngine_Core_U3CModuleU3E86524790.h"
#include "SuperSocket_ClientEngine_Core_U3CModuleU3E86524790MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn2860571088.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn2860571088MethodDeclarations.h"
#include "System_System_Net_EndPoint1294049535.h"
#include "mscorlib_System_Void2779279689.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1301539049MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "System_System_Net_Sockets_SocketAsyncEventArgs970431102.h"
#include "System_System_Net_Sockets_SocketAsyncEventArgs970431102MethodDeclarations.h"
#include "System_System_Net_Sockets_SocketAsyncOperation2602276174.h"
#include "System_System_Net_Sockets_Socket150013987.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn3100954378MethodDeclarations.h"
#include "mscorlib_System_ArraySegment_1_gen2801744866MethodDeclarations.h"
#include "mscorlib_System_ArraySegment_1_gen2801744866.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2778693821.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn3100954378.h"
#include "mscorlib_System_Int322847414787.h"
#include "System_System_Net_Sockets_SocketException964769420MethodDeclarations.h"
#include "System_System_Net_Sockets_SocketError291509957.h"
#include "mscorlib_System_Boolean211005341.h"
#include "System_System_Net_Sockets_SocketException964769420.h"
#include "mscorlib_System_Exception1967233988.h"
#include "System_System_Net_Sockets_Socket150013987MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ExternalEx1945928326MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ExternalEx1945928326.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1301539049.h"
#include "mscorlib_System_EventHandler_1_gen1813008745MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen1813008745.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn3216211148MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn3216211148.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "mscorlib_System_EventHandler247020293.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "mscorlib_System_EventHandler247020293MethodDeclarations.h"
#include "mscorlib_System_EventArgs516466188.h"
#include "mscorlib_System_EventArgs516466188MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen2171844441.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1329266798MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen2171844441MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1329266798.h"
#include "mscorlib_System_EventHandler_1_gen4058788791.h"
#include "mscorlib_System_EventHandler_1_gen4058788791MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn2680237056.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn2680237056MethodDeclarations.h"
#include "System_System_Net_Sockets_NetworkStream1815209918MethodDeclarations.h"
#include "System_System_Net_Security_RemoteCertificateValida4087051103MethodDeclarations.h"
#include "System_System_Net_Security_SslStream2729491676MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1363551830MethodDeclarations.h"
#include "System_System_Net_Security_SslStream2729491676.h"
#include "System_System_Net_Sockets_NetworkStream1815209918.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3432067208.h"
#include "System_System_Security_Cryptography_X509Certificat2831591730.h"
#include "System_System_Net_Security_SslPolicyErrors3085256601.h"
#include "System_System_Net_Security_RemoteCertificateValida4087051103.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn3174738561MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn3174738561.h"
#include "mscorlib_System_IO_Stream219029575MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat2831591730MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3432067208MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat1122151684MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat1122151684.h"
#include "System_ArrayTypes.h"
#include "System_System_Security_Cryptography_X509Certificat1387926129.h"
#include "mscorlib_System_IO_IOException544650864.h"
#include "mscorlib_System_ObjectDisposedException973246880.h"
#include "SuperSocket_ClientEngine_Core_System_Collections_C2535351335MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_System_Net_DnsEndP1814926600MethodDeclarations.h"
#include "System_System_Net_IPEndPoint1265996582MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_System_Net_DnsEndP1814926600.h"
#include "System_System_Net_IPEndPoint1265996582.h"
#include "SuperSocket_ClientEngine_Core_System_Collections_C2535351335.h"
#include "System_System_Net_IPAddress3220500535.h"
#include "mscorlib_System_NullReferenceException3216235232.h"
#include "mscorlib_System_EventHandler_1_gen150131123MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client1584521517MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_ClientE931740524MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client3602520776.h"
#include "mscorlib_System_EventHandler_1_gen150131123.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client1584521517.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client3602520776MethodDeclarations.h"
#include "System_System_Net_Sockets_SocketOptionLevel1963840268.h"
#include "System_System_Net_Sockets_SocketOptionName3764387107.h"
#include "System_System_Net_Sockets_SocketShutdown3653513113.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::.ctor(System.Net.EndPoint)
extern "C"  void AsyncTcpSession__ctor_m2893573072 (AsyncTcpSession_t2860571088 * __this, EndPoint_t1294049535 * ___remoteEndPoint, const MethodInfo* method)
{
	{
		EndPoint_t1294049535 * L_0 = ___remoteEndPoint;
		TcpClientSession__ctor_m2728456543(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::SocketEventArgsCompleted(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern TypeInfo* Socket_t150013987_il2cpp_TypeInfo_var;
extern const uint32_t AsyncTcpSession_SocketEventArgsCompleted_m3117056998_MetadataUsageId;
extern "C"  void AsyncTcpSession_SocketEventArgsCompleted_m3117056998 (AsyncTcpSession_t2860571088 * __this, Il2CppObject * ___sender, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncTcpSession_SocketEventArgsCompleted_m3117056998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SocketAsyncEventArgs_t970431102 * L_0 = ___e;
		NullCheck(L_0);
		int32_t L_1 = SocketAsyncEventArgs_get_LastOperation_m227446427(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_2 = ___sender;
		SocketAsyncEventArgs_t970431102 * L_3 = ___e;
		TcpClientSession_ProcessConnect_m1201190206(__this, ((Socket_t150013987 *)IsInstClass(L_2, Socket_t150013987_il2cpp_TypeInfo_var)), NULL, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		SocketAsyncEventArgs_t970431102 * L_4 = ___e;
		AsyncTcpSession_ProcessReceive_m139804939(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::OnGetSocket(System.Net.Sockets.SocketAsyncEventArgs)
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegment_1_get_Array_m2310099123_MethodInfo_var;
extern const MethodInfo* ArraySegment_1__ctor_m3685935155_MethodInfo_var;
extern const MethodInfo* ArraySegment_1_get_Offset_m901008349_MethodInfo_var;
extern const MethodInfo* ArraySegment_1_get_Count_m2396753447_MethodInfo_var;
extern const uint32_t AsyncTcpSession_OnGetSocket_m4043555073_MetadataUsageId;
extern "C"  void AsyncTcpSession_OnGetSocket_m4043555073 (AsyncTcpSession_t2860571088 * __this, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncTcpSession_OnGetSocket_m4043555073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArraySegment_1_t2801744866  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ArraySegment_1_t2801744866  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ArraySegment_1_t2801744866  V_2;
	memset(&V_2, 0, sizeof(V_2));
	ArraySegment_1_t2801744866  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		ArraySegment_1_t2801744866  L_0 = ClientSession_get_Buffer_m4110712514(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		ByteU5BU5D_t58506160* L_1 = ArraySegment_1_get_Array_m2310099123((&V_0), /*hidden argument*/ArraySegment_1_get_Array_m2310099123_MethodInfo_var);
		if (L_1)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 SuperSocket.ClientEngine.ClientSession::get_ReceiveBufferSize() */, __this);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 SuperSocket.ClientEngine.ClientSession::get_ReceiveBufferSize() */, __this);
		ArraySegment_1_t2801744866  L_4;
		memset(&L_4, 0, sizeof(L_4));
		ArraySegment_1__ctor_m3685935155(&L_4, ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_2)), 0, L_3, /*hidden argument*/ArraySegment_1__ctor_m3685935155_MethodInfo_var);
		ClientSession_set_Buffer_m634249301(__this, L_4, /*hidden argument*/NULL);
	}

IL_002d:
	{
		SocketAsyncEventArgs_t970431102 * L_5 = ___e;
		ArraySegment_1_t2801744866  L_6 = ClientSession_get_Buffer_m4110712514(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		ByteU5BU5D_t58506160* L_7 = ArraySegment_1_get_Array_m2310099123((&V_1), /*hidden argument*/ArraySegment_1_get_Array_m2310099123_MethodInfo_var);
		ArraySegment_1_t2801744866  L_8 = ClientSession_get_Buffer_m4110712514(__this, /*hidden argument*/NULL);
		V_2 = L_8;
		int32_t L_9 = ArraySegment_1_get_Offset_m901008349((&V_2), /*hidden argument*/ArraySegment_1_get_Offset_m901008349_MethodInfo_var);
		ArraySegment_1_t2801744866  L_10 = ClientSession_get_Buffer_m4110712514(__this, /*hidden argument*/NULL);
		V_3 = L_10;
		int32_t L_11 = ArraySegment_1_get_Count_m2396753447((&V_3), /*hidden argument*/ArraySegment_1_get_Count_m2396753447_MethodInfo_var);
		NullCheck(L_5);
		SocketAsyncEventArgs_SetBuffer_m2648613995(L_5, L_7, L_9, L_11, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_12 = ___e;
		__this->set_m_SocketEventArgs_15(L_12);
		VirtActionInvoker0::Invoke(15 /* System.Void SuperSocket.ClientEngine.ClientSession::OnConnected() */, __this);
		AsyncTcpSession_StartReceive_m3779991304(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::ProcessReceive(System.Net.Sockets.SocketAsyncEventArgs)
extern TypeInfo* SocketException_t964769420_il2cpp_TypeInfo_var;
extern const uint32_t AsyncTcpSession_ProcessReceive_m139804939_MetadataUsageId;
extern "C"  void AsyncTcpSession_ProcessReceive_m139804939 (AsyncTcpSession_t2860571088 * __this, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncTcpSession_ProcessReceive_m139804939_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SocketAsyncEventArgs_t970431102 * L_0 = ___e;
		NullCheck(L_0);
		int32_t L_1 = SocketAsyncEventArgs_get_SocketError_m3030139504(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		bool L_2 = TcpClientSession_EnsureSocketClosed_m2842965627(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		VirtActionInvoker0::Invoke(9 /* System.Void SuperSocket.ClientEngine.ClientSession::OnClosed() */, __this);
	}

IL_0016:
	{
		SocketAsyncEventArgs_t970431102 * L_3 = ___e;
		NullCheck(L_3);
		int32_t L_4 = SocketAsyncEventArgs_get_SocketError_m3030139504(L_3, /*hidden argument*/NULL);
		bool L_5 = TcpClientSession_IsIgnorableSocketError_m1357922689(__this, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		SocketAsyncEventArgs_t970431102 * L_6 = ___e;
		NullCheck(L_6);
		int32_t L_7 = SocketAsyncEventArgs_get_SocketError_m3030139504(L_6, /*hidden argument*/NULL);
		SocketException_t964769420 * L_8 = (SocketException_t964769420 *)il2cpp_codegen_object_new(SocketException_t964769420_il2cpp_TypeInfo_var);
		SocketException__ctor_m2020928431(L_8, L_7, /*hidden argument*/NULL);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_8);
	}

IL_0035:
	{
		return;
	}

IL_0036:
	{
		SocketAsyncEventArgs_t970431102 * L_9 = ___e;
		NullCheck(L_9);
		int32_t L_10 = SocketAsyncEventArgs_get_BytesTransferred_m3649262106(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_004d;
		}
	}
	{
		bool L_11 = TcpClientSession_EnsureSocketClosed_m2842965627(__this, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_004c;
		}
	}
	{
		VirtActionInvoker0::Invoke(9 /* System.Void SuperSocket.ClientEngine.ClientSession::OnClosed() */, __this);
	}

IL_004c:
	{
		return;
	}

IL_004d:
	{
		SocketAsyncEventArgs_t970431102 * L_12 = ___e;
		NullCheck(L_12);
		ByteU5BU5D_t58506160* L_13 = SocketAsyncEventArgs_get_Buffer_m3362688133(L_12, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_14 = ___e;
		NullCheck(L_14);
		int32_t L_15 = SocketAsyncEventArgs_get_Offset_m1199092434(L_14, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_16 = ___e;
		NullCheck(L_16);
		int32_t L_17 = SocketAsyncEventArgs_get_BytesTransferred_m3649262106(L_16, /*hidden argument*/NULL);
		VirtActionInvoker3< ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(18 /* System.Void SuperSocket.ClientEngine.ClientSession::OnDataReceived(System.Byte[],System.Int32,System.Int32) */, __this, L_13, L_15, L_17);
		AsyncTcpSession_StartReceive_m3779991304(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::StartReceive()
extern TypeInfo* SocketException_t964769420_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t AsyncTcpSession_StartReceive_m3779991304_MetadataUsageId;
extern "C"  void AsyncTcpSession_StartReceive_m3779991304 (AsyncTcpSession_t2860571088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncTcpSession_StartReceive_m3779991304_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Socket_t150013987 * V_1 = NULL;
	SocketException_t964769420 * V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Socket_t150013987 * L_0 = ClientSession_get_Client_m275537367(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		Socket_t150013987 * L_1 = V_1;
		if (L_1)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		Socket_t150013987 * L_2 = V_1;
		SocketAsyncEventArgs_t970431102 * L_3 = __this->get_m_SocketEventArgs_15();
		NullCheck(L_2);
		bool L_4 = Socket_ReceiveAsync_m2408835960(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0063;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (SocketException_t964769420_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001a;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0041;
		throw e;
	}

CATCH_001a:
	{ // begin catch(System.Net.Sockets.SocketException)
		{
			V_2 = ((SocketException_t964769420 *)__exception_local);
			SocketException_t964769420 * L_5 = V_2;
			NullCheck(L_5);
			int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(11 /* System.Int32 System.Runtime.InteropServices.ExternalException::get_ErrorCode() */, L_5);
			bool L_7 = TcpClientSession_IsIgnorableSocketError_m1357922689(__this, L_6, /*hidden argument*/NULL);
			if (L_7)
			{
				goto IL_0030;
			}
		}

IL_0029:
		{
			SocketException_t964769420 * L_8 = V_2;
			VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_8);
		}

IL_0030:
		{
			Socket_t150013987 * L_9 = V_1;
			bool L_10 = TcpClientSession_EnsureSocketClosed_m4142523302(__this, L_9, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			VirtActionInvoker0::Invoke(9 /* System.Void SuperSocket.ClientEngine.ClientSession::OnClosed() */, __this);
		}

IL_003f:
		{
			goto IL_0072;
		}
	} // end catch (depth: 1)

CATCH_0041:
	{ // begin catch(System.Exception)
		{
			V_3 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_11 = V_3;
			bool L_12 = VirtFuncInvoker1< bool, Exception_t1967233988 * >::Invoke(23 /* System.Boolean SuperSocket.ClientEngine.TcpClientSession::IsIgnorableException(System.Exception) */, __this, L_11);
			if (L_12)
			{
				goto IL_0052;
			}
		}

IL_004b:
		{
			Exception_t1967233988 * L_13 = V_3;
			VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_13);
		}

IL_0052:
		{
			Socket_t150013987 * L_14 = V_1;
			bool L_15 = TcpClientSession_EnsureSocketClosed_m4142523302(__this, L_14, /*hidden argument*/NULL);
			if (!L_15)
			{
				goto IL_0061;
			}
		}

IL_005b:
		{
			VirtActionInvoker0::Invoke(9 /* System.Void SuperSocket.ClientEngine.ClientSession::OnClosed() */, __this);
		}

IL_0061:
		{
			goto IL_0072;
		}
	} // end catch (depth: 1)

IL_0063:
	{
		bool L_16 = V_0;
		if (L_16)
		{
			goto IL_0072;
		}
	}
	{
		SocketAsyncEventArgs_t970431102 * L_17 = __this->get_m_SocketEventArgs_15();
		AsyncTcpSession_ProcessReceive_m139804939(__this, L_17, /*hidden argument*/NULL);
	}

IL_0072:
	{
		return;
	}
}
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::SendInternal(System.ArraySegment`1<System.Byte>)
extern TypeInfo* SocketAsyncEventArgs_t970431102_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_1_t1813008745_il2cpp_TypeInfo_var;
extern TypeInfo* SocketException_t964769420_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* AsyncTcpSession_Sending_Completed_m4078982099_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3542837379_MethodInfo_var;
extern const MethodInfo* ArraySegment_1_get_Array_m2310099123_MethodInfo_var;
extern const MethodInfo* ArraySegment_1_get_Offset_m901008349_MethodInfo_var;
extern const MethodInfo* ArraySegment_1_get_Count_m2396753447_MethodInfo_var;
extern const uint32_t AsyncTcpSession_SendInternal_m822235139_MetadataUsageId;
extern "C"  void AsyncTcpSession_SendInternal_m822235139 (AsyncTcpSession_t2860571088 * __this, ArraySegment_1_t2801744866  ___segment, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncTcpSession_SendInternal_m822235139_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	SocketException_t964769420 * V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SocketAsyncEventArgs_t970431102 * L_0 = __this->get_m_SocketEventArgsSend_16();
		if (L_0)
		{
			goto IL_002a;
		}
	}
	{
		SocketAsyncEventArgs_t970431102 * L_1 = (SocketAsyncEventArgs_t970431102 *)il2cpp_codegen_object_new(SocketAsyncEventArgs_t970431102_il2cpp_TypeInfo_var);
		SocketAsyncEventArgs__ctor_m2409327454(L_1, /*hidden argument*/NULL);
		__this->set_m_SocketEventArgsSend_16(L_1);
		SocketAsyncEventArgs_t970431102 * L_2 = __this->get_m_SocketEventArgsSend_16();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)AsyncTcpSession_Sending_Completed_m4078982099_MethodInfo_var);
		EventHandler_1_t1813008745 * L_4 = (EventHandler_1_t1813008745 *)il2cpp_codegen_object_new(EventHandler_1_t1813008745_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m3542837379(L_4, __this, L_3, /*hidden argument*/EventHandler_1__ctor_m3542837379_MethodInfo_var);
		NullCheck(L_2);
		SocketAsyncEventArgs_add_Completed_m504170843(L_2, L_4, /*hidden argument*/NULL);
	}

IL_002a:
	{
		SocketAsyncEventArgs_t970431102 * L_5 = __this->get_m_SocketEventArgsSend_16();
		ByteU5BU5D_t58506160* L_6 = ArraySegment_1_get_Array_m2310099123((&___segment), /*hidden argument*/ArraySegment_1_get_Array_m2310099123_MethodInfo_var);
		int32_t L_7 = ArraySegment_1_get_Offset_m901008349((&___segment), /*hidden argument*/ArraySegment_1_get_Offset_m901008349_MethodInfo_var);
		int32_t L_8 = ArraySegment_1_get_Count_m2396753447((&___segment), /*hidden argument*/ArraySegment_1_get_Count_m2396753447_MethodInfo_var);
		NullCheck(L_5);
		SocketAsyncEventArgs_SetBuffer_m2648613995(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_004a:
	try
	{ // begin try (depth: 1)
		Socket_t150013987 * L_9 = ClientSession_get_Client_m275537367(__this, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_10 = __this->get_m_SocketEventArgsSend_16();
		NullCheck(L_9);
		bool L_11 = Socket_SendAsync_m3069255749(L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_0099;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (SocketException_t964769420_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_007e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Net.Sockets.SocketException)
		{
			V_1 = ((SocketException_t964769420 *)__exception_local);
			bool L_12 = TcpClientSession_EnsureSocketClosed_m2842965627(__this, /*hidden argument*/NULL);
			if (!L_12)
			{
				goto IL_007c;
			}
		}

IL_0067:
		{
			SocketException_t964769420 * L_13 = V_1;
			NullCheck(L_13);
			int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(11 /* System.Int32 System.Runtime.InteropServices.ExternalException::get_ErrorCode() */, L_13);
			bool L_15 = TcpClientSession_IsIgnorableSocketError_m1357922689(__this, L_14, /*hidden argument*/NULL);
			if (L_15)
			{
				goto IL_007c;
			}
		}

IL_0075:
		{
			SocketException_t964769420 * L_16 = V_1;
			VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_16);
		}

IL_007c:
		{
			goto IL_00ae;
		}
	} // end catch (depth: 1)

CATCH_007e:
	{ // begin catch(System.Exception)
		{
			V_2 = ((Exception_t1967233988 *)__exception_local);
			bool L_17 = TcpClientSession_EnsureSocketClosed_m2842965627(__this, /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_0097;
			}
		}

IL_0087:
		{
			Exception_t1967233988 * L_18 = V_2;
			bool L_19 = VirtFuncInvoker1< bool, Exception_t1967233988 * >::Invoke(23 /* System.Boolean SuperSocket.ClientEngine.TcpClientSession::IsIgnorableException(System.Exception) */, __this, L_18);
			if (!L_19)
			{
				goto IL_0097;
			}
		}

IL_0090:
		{
			Exception_t1967233988 * L_20 = V_2;
			VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_20);
		}

IL_0097:
		{
			goto IL_00ae;
		}
	} // end catch (depth: 1)

IL_0099:
	{
		bool L_21 = V_0;
		if (L_21)
		{
			goto IL_00ae;
		}
	}
	{
		Socket_t150013987 * L_22 = ClientSession_get_Client_m275537367(__this, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_23 = __this->get_m_SocketEventArgsSend_16();
		AsyncTcpSession_Sending_Completed_m4078982099(__this, L_22, L_23, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		return;
	}
}
// System.Void SuperSocket.ClientEngine.AsyncTcpSession::Sending_Completed(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern TypeInfo* SocketException_t964769420_il2cpp_TypeInfo_var;
extern const uint32_t AsyncTcpSession_Sending_Completed_m4078982099_MetadataUsageId;
extern "C"  void AsyncTcpSession_Sending_Completed_m4078982099 (AsyncTcpSession_t2860571088 * __this, Il2CppObject * ___sender, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncTcpSession_Sending_Completed_m4078982099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SocketAsyncEventArgs_t970431102 * L_0 = ___e;
		NullCheck(L_0);
		int32_t L_1 = SocketAsyncEventArgs_get_LastOperation_m227446427(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)7)))
		{
			goto IL_0013;
		}
	}
	{
		il2cpp_codegen_memory_barrier();
		((TcpClientSession_t1301539049 *)__this)->set_IsSending_13(0);
		return;
	}

IL_0013:
	{
		SocketAsyncEventArgs_t970431102 * L_2 = ___e;
		NullCheck(L_2);
		int32_t L_3 = SocketAsyncEventArgs_get_SocketError_m3030139504(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		SocketAsyncEventArgs_t970431102 * L_4 = ___e;
		NullCheck(L_4);
		int32_t L_5 = SocketAsyncEventArgs_get_BytesTransferred_m3649262106(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0062;
		}
	}

IL_0023:
	{
		il2cpp_codegen_memory_barrier();
		((TcpClientSession_t1301539049 *)__this)->set_IsSending_13(0);
		bool L_6 = TcpClientSession_EnsureSocketClosed_m2842965627(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003a;
		}
	}
	{
		VirtActionInvoker0::Invoke(9 /* System.Void SuperSocket.ClientEngine.ClientSession::OnClosed() */, __this);
	}

IL_003a:
	{
		SocketAsyncEventArgs_t970431102 * L_7 = ___e;
		NullCheck(L_7);
		int32_t L_8 = SocketAsyncEventArgs_get_SocketError_m3030139504(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		SocketAsyncEventArgs_t970431102 * L_9 = ___e;
		NullCheck(L_9);
		int32_t L_10 = SocketAsyncEventArgs_get_SocketError_m3030139504(L_9, /*hidden argument*/NULL);
		bool L_11 = TcpClientSession_IsIgnorableSocketError_m1357922689(__this, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0061;
		}
	}
	{
		SocketAsyncEventArgs_t970431102 * L_12 = ___e;
		NullCheck(L_12);
		int32_t L_13 = SocketAsyncEventArgs_get_SocketError_m3030139504(L_12, /*hidden argument*/NULL);
		SocketException_t964769420 * L_14 = (SocketException_t964769420 *)il2cpp_codegen_object_new(SocketException_t964769420_il2cpp_TypeInfo_var);
		SocketException__ctor_m2020928431(L_14, L_13, /*hidden argument*/NULL);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_14);
	}

IL_0061:
	{
		return;
	}

IL_0062:
	{
		TcpClientSession_DequeueSend_m881139676(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Net.Sockets.Socket SuperSocket.ClientEngine.ClientSession::get_Client()
extern "C"  Socket_t150013987 * ClientSession_get_Client_m275537367 (ClientSession_t3100954378 * __this, const MethodInfo* method)
{
	{
		Socket_t150013987 * L_0 = __this->get_U3CClientU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::set_Client(System.Net.Sockets.Socket)
extern "C"  void ClientSession_set_Client_m911781880 (ClientSession_t3100954378 * __this, Socket_t150013987 * ___value, const MethodInfo* method)
{
	{
		Socket_t150013987 * L_0 = ___value;
		__this->set_U3CClientU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Net.EndPoint SuperSocket.ClientEngine.ClientSession::get_RemoteEndPoint()
extern "C"  EndPoint_t1294049535 * ClientSession_get_RemoteEndPoint_m2153238163 (ClientSession_t3100954378 * __this, const MethodInfo* method)
{
	{
		EndPoint_t1294049535 * L_0 = __this->get_U3CRemoteEndPointU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::set_RemoteEndPoint(System.Net.EndPoint)
extern "C"  void ClientSession_set_RemoteEndPoint_m4259102164 (ClientSession_t3100954378 * __this, EndPoint_t1294049535 * ___value, const MethodInfo* method)
{
	{
		EndPoint_t1294049535 * L_0 = ___value;
		__this->set_U3CRemoteEndPointU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::set_IsConnected(System.Boolean)
extern "C"  void ClientSession_set_IsConnected_m477764794 (ClientSession_t3100954378 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->set_U3CIsConnectedU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::.ctor(System.Net.EndPoint)
extern TypeInfo* DataEventArgs_t3216211148_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral510125019;
extern const uint32_t ClientSession__ctor_m2648651594_MetadataUsageId;
extern "C"  void ClientSession__ctor_m2648651594 (ClientSession_t3100954378 * __this, EndPoint_t1294049535 * ___remoteEndPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession__ctor_m2648651594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DataEventArgs_t3216211148 * L_0 = (DataEventArgs_t3216211148 *)il2cpp_codegen_object_new(DataEventArgs_t3216211148_il2cpp_TypeInfo_var);
		DataEventArgs__ctor_m3194743745(L_0, /*hidden argument*/NULL);
		__this->set_m_DataArgs_4(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		EndPoint_t1294049535 * L_1 = ___remoteEndPoint;
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_2 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, _stringLiteral510125019, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001f:
	{
		EndPoint_t1294049535 * L_3 = ___remoteEndPoint;
		ClientSession_set_RemoteEndPoint_m4259102164(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::add_Closed(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t ClientSession_add_Closed_m3100078168_MetadataUsageId;
extern "C"  void ClientSession_add_Closed_m3100078168 (ClientSession_t3100954378 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession_add_Closed_m3100078168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_m_Closed_0();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_Closed_0(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::remove_Closed(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t ClientSession_remove_Closed_m1111740091_MetadataUsageId;
extern "C"  void ClientSession_remove_Closed_m1111740091 (ClientSession_t3100954378 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession_remove_Closed_m1111740091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_m_Closed_0();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_Closed_0(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::OnClosed()
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t ClientSession_OnClosed_m4144825100_MetadataUsageId;
extern "C"  void ClientSession_OnClosed_m4144825100 (ClientSession_t3100954378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession_OnClosed_m4144825100_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EventHandler_t247020293 * V_0 = NULL;
	{
		ClientSession_set_IsConnected_m477764794(__this, (bool)0, /*hidden argument*/NULL);
		EventHandler_t247020293 * L_0 = __this->get_m_Closed_0();
		V_0 = L_0;
		EventHandler_t247020293 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		EventHandler_t247020293 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs_t516466188 * L_3 = ((EventArgs_t516466188_StaticFields*)EventArgs_t516466188_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_2);
		EventHandler_Invoke_m1659461265(L_2, __this, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::add_Error(System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>)
extern TypeInfo* EventHandler_1_t2171844441_il2cpp_TypeInfo_var;
extern const uint32_t ClientSession_add_Error_m1151993633_MetadataUsageId;
extern "C"  void ClientSession_add_Error_m1151993633 (ClientSession_t3100954378 * __this, EventHandler_1_t2171844441 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession_add_Error_m1151993633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t2171844441 * L_0 = __this->get_m_Error_1();
		EventHandler_1_t2171844441 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_Error_1(((EventHandler_1_t2171844441 *)CastclassSealed(L_2, EventHandler_1_t2171844441_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::remove_Error(System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>)
extern TypeInfo* EventHandler_1_t2171844441_il2cpp_TypeInfo_var;
extern const uint32_t ClientSession_remove_Error_m3793664452_MetadataUsageId;
extern "C"  void ClientSession_remove_Error_m3793664452 (ClientSession_t3100954378 * __this, EventHandler_1_t2171844441 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession_remove_Error_m3793664452_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t2171844441 * L_0 = __this->get_m_Error_1();
		EventHandler_1_t2171844441 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_Error_1(((EventHandler_1_t2171844441 *)CastclassSealed(L_2, EventHandler_1_t2171844441_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception)
extern TypeInfo* ErrorEventArgs_t1329266798_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m2407594268_MethodInfo_var;
extern const uint32_t ClientSession_OnError_m1645804090_MetadataUsageId;
extern "C"  void ClientSession_OnError_m1645804090 (ClientSession_t3100954378 * __this, Exception_t1967233988 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession_OnError_m1645804090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EventHandler_1_t2171844441 * V_0 = NULL;
	{
		EventHandler_1_t2171844441 * L_0 = __this->get_m_Error_1();
		V_0 = L_0;
		EventHandler_1_t2171844441 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		EventHandler_1_t2171844441 * L_2 = V_0;
		Exception_t1967233988 * L_3 = ___e;
		ErrorEventArgs_t1329266798 * L_4 = (ErrorEventArgs_t1329266798 *)il2cpp_codegen_object_new(ErrorEventArgs_t1329266798_il2cpp_TypeInfo_var);
		ErrorEventArgs__ctor_m1471498585(L_4, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		EventHandler_1_Invoke_m2407594268(L_2, __this, L_4, /*hidden argument*/EventHandler_1_Invoke_m2407594268_MethodInfo_var);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::add_Connected(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t ClientSession_add_Connected_m2440294935_MetadataUsageId;
extern "C"  void ClientSession_add_Connected_m2440294935 (ClientSession_t3100954378 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession_add_Connected_m2440294935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_m_Connected_2();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_Connected_2(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::remove_Connected(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t ClientSession_remove_Connected_m4049589460_MetadataUsageId;
extern "C"  void ClientSession_remove_Connected_m4049589460 (ClientSession_t3100954378 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession_remove_Connected_m4049589460_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_m_Connected_2();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_Connected_2(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::OnConnected()
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t ClientSession_OnConnected_m3607001547_MetadataUsageId;
extern "C"  void ClientSession_OnConnected_m3607001547 (ClientSession_t3100954378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession_OnConnected_m3607001547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EventHandler_t247020293 * V_0 = NULL;
	{
		ClientSession_set_IsConnected_m477764794(__this, (bool)1, /*hidden argument*/NULL);
		EventHandler_t247020293 * L_0 = __this->get_m_Connected_2();
		V_0 = L_0;
		EventHandler_t247020293 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		EventHandler_t247020293 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs_t516466188 * L_3 = ((EventArgs_t516466188_StaticFields*)EventArgs_t516466188_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_2);
		EventHandler_Invoke_m1659461265(L_2, __this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::add_DataReceived(System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>)
extern TypeInfo* EventHandler_1_t4058788791_il2cpp_TypeInfo_var;
extern const uint32_t ClientSession_add_DataReceived_m489756946_MetadataUsageId;
extern "C"  void ClientSession_add_DataReceived_m489756946 (ClientSession_t3100954378 * __this, EventHandler_1_t4058788791 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession_add_DataReceived_m489756946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t4058788791 * L_0 = __this->get_m_DataReceived_3();
		EventHandler_1_t4058788791 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_DataReceived_3(((EventHandler_1_t4058788791 *)CastclassSealed(L_2, EventHandler_1_t4058788791_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::remove_DataReceived(System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>)
extern TypeInfo* EventHandler_1_t4058788791_il2cpp_TypeInfo_var;
extern const uint32_t ClientSession_remove_DataReceived_m2540701045_MetadataUsageId;
extern "C"  void ClientSession_remove_DataReceived_m2540701045 (ClientSession_t3100954378 * __this, EventHandler_1_t4058788791 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession_remove_DataReceived_m2540701045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t4058788791 * L_0 = __this->get_m_DataReceived_3();
		EventHandler_1_t4058788791 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_DataReceived_3(((EventHandler_1_t4058788791 *)CastclassSealed(L_2, EventHandler_1_t4058788791_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::OnDataReceived(System.Byte[],System.Int32,System.Int32)
extern const MethodInfo* EventHandler_1_Invoke_m1625816252_MethodInfo_var;
extern const uint32_t ClientSession_OnDataReceived_m1273102142_MetadataUsageId;
extern "C"  void ClientSession_OnDataReceived_m1273102142 (ClientSession_t3100954378 * __this, ByteU5BU5D_t58506160* ___data, int32_t ___offset, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClientSession_OnDataReceived_m1273102142_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EventHandler_1_t4058788791 * V_0 = NULL;
	{
		EventHandler_1_t4058788791 * L_0 = __this->get_m_DataReceived_3();
		V_0 = L_0;
		EventHandler_1_t4058788791 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		DataEventArgs_t3216211148 * L_2 = __this->get_m_DataArgs_4();
		ByteU5BU5D_t58506160* L_3 = ___data;
		NullCheck(L_2);
		DataEventArgs_set_Data_m1442934783(L_2, L_3, /*hidden argument*/NULL);
		DataEventArgs_t3216211148 * L_4 = __this->get_m_DataArgs_4();
		int32_t L_5 = ___offset;
		NullCheck(L_4);
		DataEventArgs_set_Offset_m3659609028(L_4, L_5, /*hidden argument*/NULL);
		DataEventArgs_t3216211148 * L_6 = __this->get_m_DataArgs_4();
		int32_t L_7 = ___length;
		NullCheck(L_6);
		DataEventArgs_set_Length_m3010264279(L_6, L_7, /*hidden argument*/NULL);
		EventHandler_1_t4058788791 * L_8 = V_0;
		DataEventArgs_t3216211148 * L_9 = __this->get_m_DataArgs_4();
		NullCheck(L_8);
		EventHandler_1_Invoke_m1625816252(L_8, __this, L_9, /*hidden argument*/EventHandler_1_Invoke_m1625816252_MethodInfo_var);
		return;
	}
}
// System.Int32 SuperSocket.ClientEngine.ClientSession::get_ReceiveBufferSize()
extern "C"  int32_t ClientSession_get_ReceiveBufferSize_m487623082 (ClientSession_t3100954378 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CReceiveBufferSizeU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::set_ReceiveBufferSize(System.Int32)
extern "C"  void ClientSession_set_ReceiveBufferSize_m2308395257 (ClientSession_t3100954378 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CReceiveBufferSizeU3Ek__BackingField_8(L_0);
		return;
	}
}
// SuperSocket.ClientEngine.IProxyConnector SuperSocket.ClientEngine.ClientSession::get_Proxy()
extern "C"  Il2CppObject * ClientSession_get_Proxy_m2237735788 (ClientSession_t3100954378 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CProxyU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::set_Proxy(SuperSocket.ClientEngine.IProxyConnector)
extern "C"  void ClientSession_set_Proxy_m3800103115 (ClientSession_t3100954378 * __this, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value;
		__this->set_U3CProxyU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.ArraySegment`1<System.Byte> SuperSocket.ClientEngine.ClientSession::get_Buffer()
extern "C"  ArraySegment_1_t2801744866  ClientSession_get_Buffer_m4110712514 (ClientSession_t3100954378 * __this, const MethodInfo* method)
{
	{
		ArraySegment_1_t2801744866  L_0 = __this->get_U3CBufferU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ClientSession::set_Buffer(System.ArraySegment`1<System.Byte>)
extern "C"  void ClientSession_set_Buffer_m634249301 (ClientSession_t3100954378 * __this, ArraySegment_1_t2801744866  ___value, const MethodInfo* method)
{
	{
		ArraySegment_1_t2801744866  L_0 = ___value;
		__this->set_U3CBufferU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Byte[] SuperSocket.ClientEngine.DataEventArgs::get_Data()
extern "C"  ByteU5BU5D_t58506160* DataEventArgs_get_Data_m3597378432 (DataEventArgs_t3216211148 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = __this->get_U3CDataU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.DataEventArgs::set_Data(System.Byte[])
extern "C"  void DataEventArgs_set_Data_m1442934783 (DataEventArgs_t3216211148 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = ___value;
		__this->set_U3CDataU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 SuperSocket.ClientEngine.DataEventArgs::get_Offset()
extern "C"  int32_t DataEventArgs_get_Offset_m635555377 (DataEventArgs_t3216211148 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3COffsetU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.DataEventArgs::set_Offset(System.Int32)
extern "C"  void DataEventArgs_set_Offset_m3659609028 (DataEventArgs_t3216211148 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3COffsetU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Int32 SuperSocket.ClientEngine.DataEventArgs::get_Length()
extern "C"  int32_t DataEventArgs_get_Length_m3327941572 (DataEventArgs_t3216211148 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CLengthU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.DataEventArgs::set_Length(System.Int32)
extern "C"  void DataEventArgs_set_Length_m3010264279 (DataEventArgs_t3216211148 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CLengthU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.DataEventArgs::.ctor()
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t DataEventArgs__ctor_m3194743745_MetadataUsageId;
extern "C"  void DataEventArgs__ctor_m3194743745 (DataEventArgs_t3216211148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataEventArgs__ctor_m3194743745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1904770202(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Exception SuperSocket.ClientEngine.ErrorEventArgs::get_Exception()
extern "C"  Exception_t1967233988 * ErrorEventArgs_get_Exception_m2582201342 (ErrorEventArgs_t1329266798 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = __this->get_U3CExceptionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.ErrorEventArgs::set_Exception(System.Exception)
extern "C"  void ErrorEventArgs_set_Exception_m3394529449 (ErrorEventArgs_t1329266798 * __this, Exception_t1967233988 * ___value, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___value;
		__this->set_U3CExceptionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.ErrorEventArgs::.ctor(System.Exception)
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t ErrorEventArgs__ctor_m1471498585_MetadataUsageId;
extern "C"  void ErrorEventArgs__ctor_m1471498585 (ErrorEventArgs_t1329266798 * __this, Exception_t1967233988 * ___exception, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ErrorEventArgs__ctor_m1471498585_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1904770202(__this, /*hidden argument*/NULL);
		Exception_t1967233988 * L_0 = ___exception;
		ErrorEventArgs_set_Exception_m3394529449(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SuperSocket.ClientEngine.SslStreamTcpSession::get_AllowUnstrustedCertificate()
extern "C"  bool SslStreamTcpSession_get_AllowUnstrustedCertificate_m4133364489 (SslStreamTcpSession_t2680237056 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CAllowUnstrustedCertificateU3Ek__BackingField_16();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::.ctor(System.Net.EndPoint)
extern "C"  void SslStreamTcpSession__ctor_m1591305088 (SslStreamTcpSession_t2680237056 * __this, EndPoint_t1294049535 * ___remoteEndPoint, const MethodInfo* method)
{
	{
		EndPoint_t1294049535 * L_0 = ___remoteEndPoint;
		TcpClientSession__ctor_m2728456543(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::SocketEventArgsCompleted(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern TypeInfo* Socket_t150013987_il2cpp_TypeInfo_var;
extern const uint32_t SslStreamTcpSession_SocketEventArgsCompleted_m1031600694_MetadataUsageId;
extern "C"  void SslStreamTcpSession_SocketEventArgsCompleted_m1031600694 (SslStreamTcpSession_t2680237056 * __this, Il2CppObject * ___sender, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SslStreamTcpSession_SocketEventArgsCompleted_m1031600694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___sender;
		SocketAsyncEventArgs_t970431102 * L_1 = ___e;
		TcpClientSession_ProcessConnect_m1201190206(__this, ((Socket_t150013987 *)IsInstClass(L_0, Socket_t150013987_il2cpp_TypeInfo_var)), NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::OnGetSocket(System.Net.Sockets.SocketAsyncEventArgs)
extern TypeInfo* NetworkStream_t1815209918_il2cpp_TypeInfo_var;
extern TypeInfo* RemoteCertificateValidationCallback_t4087051103_il2cpp_TypeInfo_var;
extern TypeInfo* SslStream_t2729491676_il2cpp_TypeInfo_var;
extern TypeInfo* AsyncCallback_t1363551830_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* SslStreamTcpSession_ValidateRemoteCertificate_m495241029_MethodInfo_var;
extern const MethodInfo* SslStreamTcpSession_OnAuthenticated_m2132154888_MethodInfo_var;
extern const uint32_t SslStreamTcpSession_OnGetSocket_m172588209_MetadataUsageId;
extern "C"  void SslStreamTcpSession_OnGetSocket_m172588209 (SslStreamTcpSession_t2680237056 * __this, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SslStreamTcpSession_OnGetSocket_m172588209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SslStream_t2729491676 * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Socket_t150013987 * L_0 = ClientSession_get_Client_m275537367(__this, /*hidden argument*/NULL);
		NetworkStream_t1815209918 * L_1 = (NetworkStream_t1815209918 *)il2cpp_codegen_object_new(NetworkStream_t1815209918_il2cpp_TypeInfo_var);
		NetworkStream__ctor_m2753228565(L_1, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)SslStreamTcpSession_ValidateRemoteCertificate_m495241029_MethodInfo_var);
		RemoteCertificateValidationCallback_t4087051103 * L_3 = (RemoteCertificateValidationCallback_t4087051103 *)il2cpp_codegen_object_new(RemoteCertificateValidationCallback_t4087051103_il2cpp_TypeInfo_var);
		RemoteCertificateValidationCallback__ctor_m1684204841(L_3, __this, L_2, /*hidden argument*/NULL);
		SslStream_t2729491676 * L_4 = (SslStream_t2729491676 *)il2cpp_codegen_object_new(SslStream_t2729491676_il2cpp_TypeInfo_var);
		SslStream__ctor_m2104712965(L_4, L_1, (bool)0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		SslStream_t2729491676 * L_5 = V_0;
		String_t* L_6 = TcpClientSession_get_HostName_m3978347023(__this, /*hidden argument*/NULL);
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)SslStreamTcpSession_OnAuthenticated_m2132154888_MethodInfo_var);
		AsyncCallback_t1363551830 * L_8 = (AsyncCallback_t1363551830 *)il2cpp_codegen_object_new(AsyncCallback_t1363551830_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_8, __this, L_7, /*hidden argument*/NULL);
		SslStream_t2729491676 * L_9 = V_0;
		NullCheck(L_5);
		VirtFuncInvoker3< Il2CppObject *, String_t*, AsyncCallback_t1363551830 *, Il2CppObject * >::Invoke(27 /* System.IAsyncResult System.Net.Security.SslStream::BeginAuthenticateAsClient(System.String,System.AsyncCallback,System.Object) */, L_5, L_6, L_8, L_9);
		goto IL_004d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003a;
		throw e;
	}

CATCH_003a:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_10 = V_1;
			bool L_11 = VirtFuncInvoker1< bool, Exception_t1967233988 * >::Invoke(23 /* System.Boolean SuperSocket.ClientEngine.TcpClientSession::IsIgnorableException(System.Exception) */, __this, L_10);
			if (L_11)
			{
				goto IL_004b;
			}
		}

IL_0044:
		{
			Exception_t1967233988 * L_12 = V_1;
			VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_12);
		}

IL_004b:
		{
			goto IL_004d;
		}
	} // end catch (depth: 1)

IL_004d:
	{
		return;
	}
}
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::OnAuthenticated(System.IAsyncResult)
extern TypeInfo* IAsyncResult_t537683269_il2cpp_TypeInfo_var;
extern TypeInfo* SslStream_t2729491676_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegment_1_get_Array_m2310099123_MethodInfo_var;
extern const MethodInfo* ArraySegment_1__ctor_m3685935155_MethodInfo_var;
extern const uint32_t SslStreamTcpSession_OnAuthenticated_m2132154888_MetadataUsageId;
extern "C"  void SslStreamTcpSession_OnAuthenticated_m2132154888 (SslStreamTcpSession_t2680237056 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SslStreamTcpSession_OnAuthenticated_m2132154888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SslStream_t2729491676 * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	ArraySegment_1_t2801744866  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___result;
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.IAsyncResult::get_AsyncState() */, IAsyncResult_t537683269_il2cpp_TypeInfo_var, L_0);
		V_0 = ((SslStream_t2729491676 *)IsInstClass(L_1, SslStream_t2729491676_il2cpp_TypeInfo_var));
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		SslStream_t2729491676 * L_2 = V_0;
		Il2CppObject * L_3 = ___result;
		NullCheck(L_2);
		VirtActionInvoker1< Il2CppObject * >::Invoke(30 /* System.Void System.Net.Security.SslStream::EndAuthenticateAsClient(System.IAsyncResult) */, L_2, L_3);
		goto IL_001f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0015;
		throw e;
	}

CATCH_0015:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t1967233988 *)__exception_local);
		Exception_t1967233988 * L_4 = V_1;
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_4);
		goto IL_005f;
	} // end catch (depth: 1)

IL_001f:
	{
		SslStream_t2729491676 * L_5 = V_0;
		__this->set_m_SslStream_15(L_5);
		VirtActionInvoker0::Invoke(15 /* System.Void SuperSocket.ClientEngine.ClientSession::OnConnected() */, __this);
		ArraySegment_1_t2801744866  L_6 = ClientSession_get_Buffer_m4110712514(__this, /*hidden argument*/NULL);
		V_2 = L_6;
		ByteU5BU5D_t58506160* L_7 = ArraySegment_1_get_Array_m2310099123((&V_2), /*hidden argument*/ArraySegment_1_get_Array_m2310099123_MethodInfo_var);
		if (L_7)
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 SuperSocket.ClientEngine.ClientSession::get_ReceiveBufferSize() */, __this);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 SuperSocket.ClientEngine.ClientSession::get_ReceiveBufferSize() */, __this);
		ArraySegment_1_t2801744866  L_10;
		memset(&L_10, 0, sizeof(L_10));
		ArraySegment_1__ctor_m3685935155(&L_10, ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_8)), 0, L_9, /*hidden argument*/ArraySegment_1__ctor_m3685935155_MethodInfo_var);
		ClientSession_set_Buffer_m634249301(__this, L_10, /*hidden argument*/NULL);
	}

IL_0059:
	{
		SslStreamTcpSession_BeginRead_m2969334026(__this, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::OnDataRead(System.IAsyncResult)
extern TypeInfo* IAsyncResult_t537683269_il2cpp_TypeInfo_var;
extern TypeInfo* SslAsyncState_t3174738561_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegment_1_get_Array_m2310099123_MethodInfo_var;
extern const MethodInfo* ArraySegment_1_get_Offset_m901008349_MethodInfo_var;
extern const uint32_t SslStreamTcpSession_OnDataRead_m1314874925_MetadataUsageId;
extern "C"  void SslStreamTcpSession_OnDataRead_m1314874925 (SslStreamTcpSession_t2680237056 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SslStreamTcpSession_OnDataRead_m1314874925_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SslAsyncState_t3174738561 * V_0 = NULL;
	SslStream_t2729491676 * V_1 = NULL;
	int32_t V_2 = 0;
	Exception_t1967233988 * V_3 = NULL;
	ArraySegment_1_t2801744866  V_4;
	memset(&V_4, 0, sizeof(V_4));
	ArraySegment_1_t2801744866  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___result;
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.IAsyncResult::get_AsyncState() */, IAsyncResult_t537683269_il2cpp_TypeInfo_var, L_0);
		V_0 = ((SslAsyncState_t3174738561 *)IsInstClass(L_1, SslAsyncState_t3174738561_il2cpp_TypeInfo_var));
		SslAsyncState_t3174738561 * L_2 = V_0;
		NullCheck(L_2);
		SslStream_t2729491676 * L_3 = SslAsyncState_get_SslStream_m3690350103(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		V_2 = 0;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		SslStream_t2729491676 * L_4 = V_1;
		Il2CppObject * L_5 = ___result;
		NullCheck(L_4);
		int32_t L_6 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(24 /* System.Int32 System.IO.Stream::EndRead(System.IAsyncResult) */, L_4, L_5);
		V_2 = L_6;
		goto IL_0046;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.Exception)
		{
			V_3 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_7 = V_3;
			bool L_8 = VirtFuncInvoker1< bool, Exception_t1967233988 * >::Invoke(23 /* System.Boolean SuperSocket.ClientEngine.TcpClientSession::IsIgnorableException(System.Exception) */, __this, L_7);
			if (L_8)
			{
				goto IL_0030;
			}
		}

IL_0029:
		{
			Exception_t1967233988 * L_9 = V_3;
			VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_9);
		}

IL_0030:
		{
			SslAsyncState_t3174738561 * L_10 = V_0;
			NullCheck(L_10);
			Socket_t150013987 * L_11 = SslAsyncState_get_Client_m1672307775(L_10, /*hidden argument*/NULL);
			bool L_12 = TcpClientSession_EnsureSocketClosed_m4142523302(__this, L_11, /*hidden argument*/NULL);
			if (!L_12)
			{
				goto IL_0044;
			}
		}

IL_003e:
		{
			VirtActionInvoker0::Invoke(9 /* System.Void SuperSocket.ClientEngine.ClientSession::OnClosed() */, __this);
		}

IL_0044:
		{
			goto IL_005d;
		}
	} // end catch (depth: 1)

IL_0046:
	{
		int32_t L_13 = V_2;
		if (L_13)
		{
			goto IL_005e;
		}
	}
	{
		SslAsyncState_t3174738561 * L_14 = V_0;
		NullCheck(L_14);
		Socket_t150013987 * L_15 = SslAsyncState_get_Client_m1672307775(L_14, /*hidden argument*/NULL);
		bool L_16 = TcpClientSession_EnsureSocketClosed_m4142523302(__this, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_005d;
		}
	}
	{
		VirtActionInvoker0::Invoke(9 /* System.Void SuperSocket.ClientEngine.ClientSession::OnClosed() */, __this);
	}

IL_005d:
	{
		return;
	}

IL_005e:
	{
		ArraySegment_1_t2801744866  L_17 = ClientSession_get_Buffer_m4110712514(__this, /*hidden argument*/NULL);
		V_4 = L_17;
		ByteU5BU5D_t58506160* L_18 = ArraySegment_1_get_Array_m2310099123((&V_4), /*hidden argument*/ArraySegment_1_get_Array_m2310099123_MethodInfo_var);
		ArraySegment_1_t2801744866  L_19 = ClientSession_get_Buffer_m4110712514(__this, /*hidden argument*/NULL);
		V_5 = L_19;
		int32_t L_20 = ArraySegment_1_get_Offset_m901008349((&V_5), /*hidden argument*/ArraySegment_1_get_Offset_m901008349_MethodInfo_var);
		int32_t L_21 = V_2;
		VirtActionInvoker3< ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(18 /* System.Void SuperSocket.ClientEngine.ClientSession::OnDataReceived(System.Byte[],System.Int32,System.Int32) */, __this, L_18, L_20, L_21);
		SslStreamTcpSession_BeginRead_m2969334026(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::BeginRead()
extern TypeInfo* AsyncCallback_t1363551830_il2cpp_TypeInfo_var;
extern TypeInfo* SslAsyncState_t3174738561_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegment_1_get_Array_m2310099123_MethodInfo_var;
extern const MethodInfo* ArraySegment_1_get_Offset_m901008349_MethodInfo_var;
extern const MethodInfo* ArraySegment_1_get_Count_m2396753447_MethodInfo_var;
extern const MethodInfo* SslStreamTcpSession_OnDataRead_m1314874925_MethodInfo_var;
extern const uint32_t SslStreamTcpSession_BeginRead_m2969334026_MetadataUsageId;
extern "C"  void SslStreamTcpSession_BeginRead_m2969334026 (SslStreamTcpSession_t2680237056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SslStreamTcpSession_BeginRead_m2969334026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Socket_t150013987 * V_0 = NULL;
	SslAsyncState_t3174738561 * V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	ArraySegment_1_t2801744866  V_3;
	memset(&V_3, 0, sizeof(V_3));
	ArraySegment_1_t2801744866  V_4;
	memset(&V_4, 0, sizeof(V_4));
	ArraySegment_1_t2801744866  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Socket_t150013987 * L_0 = ClientSession_get_Client_m275537367(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Socket_t150013987 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		SslStream_t2729491676 * L_2 = __this->get_m_SslStream_15();
		ArraySegment_1_t2801744866  L_3 = ClientSession_get_Buffer_m4110712514(__this, /*hidden argument*/NULL);
		V_3 = L_3;
		ByteU5BU5D_t58506160* L_4 = ArraySegment_1_get_Array_m2310099123((&V_3), /*hidden argument*/ArraySegment_1_get_Array_m2310099123_MethodInfo_var);
		ArraySegment_1_t2801744866  L_5 = ClientSession_get_Buffer_m4110712514(__this, /*hidden argument*/NULL);
		V_4 = L_5;
		int32_t L_6 = ArraySegment_1_get_Offset_m901008349((&V_4), /*hidden argument*/ArraySegment_1_get_Offset_m901008349_MethodInfo_var);
		ArraySegment_1_t2801744866  L_7 = ClientSession_get_Buffer_m4110712514(__this, /*hidden argument*/NULL);
		V_5 = L_7;
		int32_t L_8 = ArraySegment_1_get_Count_m2396753447((&V_5), /*hidden argument*/ArraySegment_1_get_Count_m2396753447_MethodInfo_var);
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)SslStreamTcpSession_OnDataRead_m1314874925_MethodInfo_var);
		AsyncCallback_t1363551830 * L_10 = (AsyncCallback_t1363551830 *)il2cpp_codegen_object_new(AsyncCallback_t1363551830_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_10, __this, L_9, /*hidden argument*/NULL);
		SslAsyncState_t3174738561 * L_11 = (SslAsyncState_t3174738561 *)il2cpp_codegen_object_new(SslAsyncState_t3174738561_il2cpp_TypeInfo_var);
		SslAsyncState__ctor_m1197978331(L_11, /*hidden argument*/NULL);
		V_1 = L_11;
		SslAsyncState_t3174738561 * L_12 = V_1;
		SslStream_t2729491676 * L_13 = __this->get_m_SslStream_15();
		NullCheck(L_12);
		SslAsyncState_set_SslStream_m2061956892(L_12, L_13, /*hidden argument*/NULL);
		SslAsyncState_t3174738561 * L_14 = V_1;
		Socket_t150013987 * L_15 = V_0;
		NullCheck(L_14);
		SslAsyncState_set_Client_m2235287952(L_14, L_15, /*hidden argument*/NULL);
		SslAsyncState_t3174738561 * L_16 = V_1;
		NullCheck(L_2);
		VirtFuncInvoker5< Il2CppObject *, ByteU5BU5D_t58506160*, int32_t, int32_t, AsyncCallback_t1363551830 *, Il2CppObject * >::Invoke(22 /* System.IAsyncResult System.IO.Stream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object) */, L_2, L_4, L_6, L_8, L_10, L_16);
		goto IL_008d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_006b;
		throw e;
	}

CATCH_006b:
	{ // begin catch(System.Exception)
		{
			V_2 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_17 = V_2;
			bool L_18 = VirtFuncInvoker1< bool, Exception_t1967233988 * >::Invoke(23 /* System.Boolean SuperSocket.ClientEngine.TcpClientSession::IsIgnorableException(System.Exception) */, __this, L_17);
			if (L_18)
			{
				goto IL_007c;
			}
		}

IL_0075:
		{
			Exception_t1967233988 * L_19 = V_2;
			VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_19);
		}

IL_007c:
		{
			Socket_t150013987 * L_20 = V_0;
			bool L_21 = TcpClientSession_EnsureSocketClosed_m4142523302(__this, L_20, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_008b;
			}
		}

IL_0085:
		{
			VirtActionInvoker0::Invoke(9 /* System.Void SuperSocket.ClientEngine.ClientSession::OnClosed() */, __this);
		}

IL_008b:
		{
			goto IL_008d;
		}
	} // end catch (depth: 1)

IL_008d:
	{
		return;
	}
}
// System.Boolean SuperSocket.ClientEngine.SslStreamTcpSession::ValidateRemoteCertificate(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern TypeInfo* SslPolicyErrors_t3085256601_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SslStreamTcpSession_ValidateRemoteCertificate_m495241029_MetadataUsageId;
extern "C"  bool SslStreamTcpSession_ValidateRemoteCertificate_m495241029 (SslStreamTcpSession_t2680237056 * __this, Il2CppObject * ___sender, X509Certificate_t3432067208 * ___certificate, X509Chain_t2831591730 * ___chain, int32_t ___sslPolicyErrors, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SslStreamTcpSession_ValidateRemoteCertificate_m495241029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	X509ChainStatus_t1122151684  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	X509ChainStatusU5BU5D_t256805485* V_2 = NULL;
	int32_t V_3 = 0;
	{
		int32_t L_0 = ___sslPolicyErrors;
		if (L_0)
		{
			goto IL_0006;
		}
	}
	{
		return (bool)1;
	}

IL_0006:
	{
		bool L_1 = SslStreamTcpSession_get_AllowUnstrustedCertificate_m4133364489(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_2 = ___sslPolicyErrors;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(SslPolicyErrors_t3085256601_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		Exception_t1967233988 * L_6 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_6, L_5, /*hidden argument*/NULL);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_6);
		return (bool)0;
	}

IL_0027:
	{
		int32_t L_7 = ___sslPolicyErrors;
		if (((int32_t)((int32_t)L_7&(int32_t)4)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_8 = ___sslPolicyErrors;
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(SslPolicyErrors_t3085256601_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		Exception_t1967233988 * L_12 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_12, L_11, /*hidden argument*/NULL);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_12);
		return (bool)0;
	}

IL_0046:
	{
		X509Chain_t2831591730 * L_13 = ___chain;
		if (!L_13)
		{
			goto IL_00b5;
		}
	}
	{
		X509Chain_t2831591730 * L_14 = ___chain;
		NullCheck(L_14);
		X509ChainStatusU5BU5D_t256805485* L_15 = X509Chain_get_ChainStatus_m3128438893(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00b5;
		}
	}
	{
		X509Chain_t2831591730 * L_16 = ___chain;
		NullCheck(L_16);
		X509ChainStatusU5BU5D_t256805485* L_17 = X509Chain_get_ChainStatus_m3128438893(L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		V_3 = 0;
		goto IL_00af;
	}

IL_005c:
	{
		X509ChainStatusU5BU5D_t256805485* L_18 = V_2;
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		V_0 = (*(X509ChainStatus_t1122151684 *)((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19))));
		X509Certificate_t3432067208 * L_20 = ___certificate;
		NullCheck(L_20);
		String_t* L_21 = X509Certificate_get_Subject_m4012979648(L_20, /*hidden argument*/NULL);
		X509Certificate_t3432067208 * L_22 = ___certificate;
		NullCheck(L_22);
		String_t* L_23 = X509Certificate_get_Issuer_m681285767(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_25 = X509ChainStatus_get_Status_m805216413((&V_0), /*hidden argument*/NULL);
		if ((((int32_t)L_25) == ((int32_t)((int32_t)32))))
		{
			goto IL_00ab;
		}
	}

IL_0087:
	{
		int32_t L_26 = X509ChainStatus_get_Status_m805216413((&V_0), /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00ab;
		}
	}
	{
		int32_t L_27 = ___sslPolicyErrors;
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = Box(SslPolicyErrors_t3085256601_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_29);
		Exception_t1967233988 * L_31 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_31, L_30, /*hidden argument*/NULL);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_31);
		V_1 = (bool)0;
		goto IL_00b7;
	}

IL_00ab:
	{
		int32_t L_32 = V_3;
		V_3 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00af:
	{
		int32_t L_33 = V_3;
		X509ChainStatusU5BU5D_t256805485* L_34 = V_2;
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length)))))))
		{
			goto IL_005c;
		}
	}

IL_00b5:
	{
		return (bool)1;
	}

IL_00b7:
	{
		bool L_35 = V_1;
		return L_35;
	}
}
// System.Boolean SuperSocket.ClientEngine.SslStreamTcpSession::IsIgnorableException(System.Exception)
extern TypeInfo* IOException_t544650864_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t SslStreamTcpSession_IsIgnorableException_m1155508779_MetadataUsageId;
extern "C"  bool SslStreamTcpSession_IsIgnorableException_m1155508779 (SslStreamTcpSession_t2680237056 * __this, Exception_t1967233988 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SslStreamTcpSession_IsIgnorableException_m1155508779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = ___e;
		bool L_1 = TcpClientSession_IsIgnorableException_m3961902810(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000b;
		}
	}
	{
		return (bool)1;
	}

IL_000b:
	{
		Exception_t1967233988 * L_2 = ___e;
		if (!((IOException_t544650864 *)IsInstClass(L_2, IOException_t544650864_il2cpp_TypeInfo_var)))
		{
			goto IL_0043;
		}
	}
	{
		Exception_t1967233988 * L_3 = ___e;
		NullCheck(L_3);
		Exception_t1967233988 * L_4 = VirtFuncInvoker0< Exception_t1967233988 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_3);
		if (!((ObjectDisposedException_t973246880 *)IsInstClass(L_4, ObjectDisposedException_t973246880_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		return (bool)1;
	}

IL_0022:
	{
		Exception_t1967233988 * L_5 = ___e;
		NullCheck(L_5);
		Exception_t1967233988 * L_6 = VirtFuncInvoker0< Exception_t1967233988 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_5);
		if (!((IOException_t544650864 *)IsInstClass(L_6, IOException_t544650864_il2cpp_TypeInfo_var)))
		{
			goto IL_0043;
		}
	}
	{
		Exception_t1967233988 * L_7 = ___e;
		NullCheck(L_7);
		Exception_t1967233988 * L_8 = VirtFuncInvoker0< Exception_t1967233988 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_7);
		NullCheck(L_8);
		Exception_t1967233988 * L_9 = VirtFuncInvoker0< Exception_t1967233988 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_8);
		if (!((ObjectDisposedException_t973246880 *)IsInstClass(L_9, ObjectDisposedException_t973246880_il2cpp_TypeInfo_var)))
		{
			goto IL_0043;
		}
	}
	{
		return (bool)1;
	}

IL_0043:
	{
		return (bool)0;
	}
}
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::SendInternal(System.ArraySegment`1<System.Byte>)
extern TypeInfo* AsyncCallback_t1363551830_il2cpp_TypeInfo_var;
extern TypeInfo* SslAsyncState_t3174738561_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegment_1_get_Array_m2310099123_MethodInfo_var;
extern const MethodInfo* ArraySegment_1_get_Offset_m901008349_MethodInfo_var;
extern const MethodInfo* ArraySegment_1_get_Count_m2396753447_MethodInfo_var;
extern const MethodInfo* SslStreamTcpSession_OnWriteComplete_m2304180127_MethodInfo_var;
extern const uint32_t SslStreamTcpSession_SendInternal_m3092865971_MetadataUsageId;
extern "C"  void SslStreamTcpSession_SendInternal_m3092865971 (SslStreamTcpSession_t2680237056 * __this, ArraySegment_1_t2801744866  ___segment, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SslStreamTcpSession_SendInternal_m3092865971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Socket_t150013987 * V_0 = NULL;
	SslAsyncState_t3174738561 * V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Socket_t150013987 * L_0 = ClientSession_get_Client_m275537367(__this, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		SslStream_t2729491676 * L_1 = __this->get_m_SslStream_15();
		ByteU5BU5D_t58506160* L_2 = ArraySegment_1_get_Array_m2310099123((&___segment), /*hidden argument*/ArraySegment_1_get_Array_m2310099123_MethodInfo_var);
		int32_t L_3 = ArraySegment_1_get_Offset_m901008349((&___segment), /*hidden argument*/ArraySegment_1_get_Offset_m901008349_MethodInfo_var);
		int32_t L_4 = ArraySegment_1_get_Count_m2396753447((&___segment), /*hidden argument*/ArraySegment_1_get_Count_m2396753447_MethodInfo_var);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)SslStreamTcpSession_OnWriteComplete_m2304180127_MethodInfo_var);
		AsyncCallback_t1363551830 * L_6 = (AsyncCallback_t1363551830 *)il2cpp_codegen_object_new(AsyncCallback_t1363551830_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_6, __this, L_5, /*hidden argument*/NULL);
		SslAsyncState_t3174738561 * L_7 = (SslAsyncState_t3174738561 *)il2cpp_codegen_object_new(SslAsyncState_t3174738561_il2cpp_TypeInfo_var);
		SslAsyncState__ctor_m1197978331(L_7, /*hidden argument*/NULL);
		V_1 = L_7;
		SslAsyncState_t3174738561 * L_8 = V_1;
		SslStream_t2729491676 * L_9 = __this->get_m_SslStream_15();
		NullCheck(L_8);
		SslAsyncState_set_SslStream_m2061956892(L_8, L_9, /*hidden argument*/NULL);
		SslAsyncState_t3174738561 * L_10 = V_1;
		Socket_t150013987 * L_11 = V_0;
		NullCheck(L_10);
		SslAsyncState_set_Client_m2235287952(L_10, L_11, /*hidden argument*/NULL);
		SslAsyncState_t3174738561 * L_12 = V_1;
		NullCheck(L_1);
		VirtFuncInvoker5< Il2CppObject *, ByteU5BU5D_t58506160*, int32_t, int32_t, AsyncCallback_t1363551830 *, Il2CppObject * >::Invoke(23 /* System.IAsyncResult System.IO.Stream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object) */, L_1, L_2, L_3, L_4, L_6, L_12);
		goto IL_0072;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0050;
		throw e;
	}

CATCH_0050:
	{ // begin catch(System.Exception)
		{
			V_2 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_13 = V_2;
			bool L_14 = VirtFuncInvoker1< bool, Exception_t1967233988 * >::Invoke(23 /* System.Boolean SuperSocket.ClientEngine.TcpClientSession::IsIgnorableException(System.Exception) */, __this, L_13);
			if (L_14)
			{
				goto IL_0061;
			}
		}

IL_005a:
		{
			Exception_t1967233988 * L_15 = V_2;
			VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_15);
		}

IL_0061:
		{
			Socket_t150013987 * L_16 = V_0;
			bool L_17 = TcpClientSession_EnsureSocketClosed_m4142523302(__this, L_16, /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_0070;
			}
		}

IL_006a:
		{
			VirtActionInvoker0::Invoke(9 /* System.Void SuperSocket.ClientEngine.ClientSession::OnClosed() */, __this);
		}

IL_0070:
		{
			goto IL_0072;
		}
	} // end catch (depth: 1)

IL_0072:
	{
		return;
	}
}
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::OnWriteComplete(System.IAsyncResult)
extern TypeInfo* IAsyncResult_t537683269_il2cpp_TypeInfo_var;
extern TypeInfo* SslAsyncState_t3174738561_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t SslStreamTcpSession_OnWriteComplete_m2304180127_MetadataUsageId;
extern "C"  void SslStreamTcpSession_OnWriteComplete_m2304180127 (SslStreamTcpSession_t2680237056 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SslStreamTcpSession_OnWriteComplete_m2304180127_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SslAsyncState_t3174738561 * V_0 = NULL;
	SslStream_t2729491676 * V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___result;
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.IAsyncResult::get_AsyncState() */, IAsyncResult_t537683269_il2cpp_TypeInfo_var, L_0);
		V_0 = ((SslAsyncState_t3174738561 *)IsInstClass(L_1, SslAsyncState_t3174738561_il2cpp_TypeInfo_var));
		SslAsyncState_t3174738561 * L_2 = V_0;
		NullCheck(L_2);
		SslStream_t2729491676 * L_3 = SslAsyncState_get_SslStream_m3690350103(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		SslStream_t2729491676 * L_4 = V_1;
		Il2CppObject * L_5 = ___result;
		NullCheck(L_4);
		VirtActionInvoker1< Il2CppObject * >::Invoke(25 /* System.Void System.IO.Stream::EndWrite(System.IAsyncResult) */, L_4, L_5);
		goto IL_004c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001c;
		throw e;
	}

CATCH_001c:
	{ // begin catch(System.Exception)
		{
			V_2 = ((Exception_t1967233988 *)__exception_local);
			il2cpp_codegen_memory_barrier();
			((TcpClientSession_t1301539049 *)__this)->set_IsSending_13(0);
			Exception_t1967233988 * L_6 = V_2;
			bool L_7 = VirtFuncInvoker1< bool, Exception_t1967233988 * >::Invoke(23 /* System.Boolean SuperSocket.ClientEngine.TcpClientSession::IsIgnorableException(System.Exception) */, __this, L_6);
			if (L_7)
			{
				goto IL_0036;
			}
		}

IL_002f:
		{
			Exception_t1967233988 * L_8 = V_2;
			VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_8);
		}

IL_0036:
		{
			SslAsyncState_t3174738561 * L_9 = V_0;
			NullCheck(L_9);
			Socket_t150013987 * L_10 = SslAsyncState_get_Client_m1672307775(L_9, /*hidden argument*/NULL);
			bool L_11 = TcpClientSession_EnsureSocketClosed_m4142523302(__this, L_10, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_004a;
			}
		}

IL_0044:
		{
			VirtActionInvoker0::Invoke(9 /* System.Void SuperSocket.ClientEngine.ClientSession::OnClosed() */, __this);
		}

IL_004a:
		{
			goto IL_0088;
		}
	} // end catch (depth: 1)

IL_004c:
	{
		bool L_12 = TcpClientSession_DequeueSend_m881139676(__this, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0088;
		}
	}

IL_0054:
	try
	{ // begin try (depth: 1)
		SslStream_t2729491676 * L_13 = __this->get_m_SslStream_15();
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(15 /* System.Void System.IO.Stream::Flush() */, L_13);
		goto IL_0088;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0061;
		throw e;
	}

CATCH_0061:
	{ // begin catch(System.Exception)
		{
			V_3 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_14 = V_3;
			bool L_15 = VirtFuncInvoker1< bool, Exception_t1967233988 * >::Invoke(23 /* System.Boolean SuperSocket.ClientEngine.TcpClientSession::IsIgnorableException(System.Exception) */, __this, L_14);
			if (L_15)
			{
				goto IL_0072;
			}
		}

IL_006b:
		{
			Exception_t1967233988 * L_16 = V_3;
			VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_16);
		}

IL_0072:
		{
			SslAsyncState_t3174738561 * L_17 = V_0;
			NullCheck(L_17);
			Socket_t150013987 * L_18 = SslAsyncState_get_Client_m1672307775(L_17, /*hidden argument*/NULL);
			bool L_19 = TcpClientSession_EnsureSocketClosed_m4142523302(__this, L_18, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_0086;
			}
		}

IL_0080:
		{
			VirtActionInvoker0::Invoke(9 /* System.Void SuperSocket.ClientEngine.ClientSession::OnClosed() */, __this);
		}

IL_0086:
		{
			goto IL_0088;
		}
	} // end catch (depth: 1)

IL_0088:
	{
		return;
	}
}
// System.Net.Security.SslStream SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState::get_SslStream()
extern "C"  SslStream_t2729491676 * SslAsyncState_get_SslStream_m3690350103 (SslAsyncState_t3174738561 * __this, const MethodInfo* method)
{
	{
		SslStream_t2729491676 * L_0 = __this->get_U3CSslStreamU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState::set_SslStream(System.Net.Security.SslStream)
extern "C"  void SslAsyncState_set_SslStream_m2061956892 (SslAsyncState_t3174738561 * __this, SslStream_t2729491676 * ___value, const MethodInfo* method)
{
	{
		SslStream_t2729491676 * L_0 = ___value;
		__this->set_U3CSslStreamU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Net.Sockets.Socket SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState::get_Client()
extern "C"  Socket_t150013987 * SslAsyncState_get_Client_m1672307775 (SslAsyncState_t3174738561 * __this, const MethodInfo* method)
{
	{
		Socket_t150013987 * L_0 = __this->get_U3CClientU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState::set_Client(System.Net.Sockets.Socket)
extern "C"  void SslAsyncState_set_Client_m2235287952 (SslAsyncState_t3174738561 * __this, Socket_t150013987 * ___value, const MethodInfo* method)
{
	{
		Socket_t150013987 * L_0 = ___value;
		__this->set_U3CClientU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState::.ctor()
extern "C"  void SslAsyncState__ctor_m1197978331 (SslAsyncState_t3174738561 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SuperSocket.ClientEngine.TcpClientSession::get_HostName()
extern "C"  String_t* TcpClientSession_get_HostName_m3978347023 (TcpClientSession_t1301539049 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CHostNameU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.TcpClientSession::set_HostName(System.String)
extern "C"  void TcpClientSession_set_HostName_m311133692 (TcpClientSession_t1301539049 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CHostNameU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.TcpClientSession::.ctor(System.Net.EndPoint)
extern "C"  void TcpClientSession__ctor_m2728456543 (TcpClientSession_t1301539049 * __this, EndPoint_t1294049535 * ___remoteEndPoint, const MethodInfo* method)
{
	{
		EndPoint_t1294049535 * L_0 = ___remoteEndPoint;
		TcpClientSession__ctor_m748658136(__this, L_0, ((int32_t)1024), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.TcpClientSession::.ctor(System.Net.EndPoint,System.Int32)
extern TypeInfo* ConcurrentQueue_1_t2535351335_il2cpp_TypeInfo_var;
extern TypeInfo* DnsEndPoint_t1814926600_il2cpp_TypeInfo_var;
extern TypeInfo* IPEndPoint_t1265996582_il2cpp_TypeInfo_var;
extern const MethodInfo* ConcurrentQueue_1__ctor_m3093470100_MethodInfo_var;
extern const uint32_t TcpClientSession__ctor_m748658136_MetadataUsageId;
extern "C"  void TcpClientSession__ctor_m748658136 (TcpClientSession_t1301539049 * __this, EndPoint_t1294049535 * ___remoteEndPoint, int32_t ___receiveBufferSize, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TcpClientSession__ctor_m748658136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DnsEndPoint_t1814926600 * V_0 = NULL;
	IPEndPoint_t1265996582 * V_1 = NULL;
	{
		ConcurrentQueue_1_t2535351335 * L_0 = (ConcurrentQueue_1_t2535351335 *)il2cpp_codegen_object_new(ConcurrentQueue_1_t2535351335_il2cpp_TypeInfo_var);
		ConcurrentQueue_1__ctor_m3093470100(L_0, /*hidden argument*/ConcurrentQueue_1__ctor_m3093470100_MethodInfo_var);
		__this->set_m_SendingQueue_12(L_0);
		EndPoint_t1294049535 * L_1 = ___remoteEndPoint;
		ClientSession__ctor_m2648651594(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___receiveBufferSize;
		VirtActionInvoker1< int32_t >::Invoke(20 /* System.Void SuperSocket.ClientEngine.ClientSession::set_ReceiveBufferSize(System.Int32) */, __this, L_2);
		EndPoint_t1294049535 * L_3 = ___remoteEndPoint;
		V_0 = ((DnsEndPoint_t1814926600 *)IsInstClass(L_3, DnsEndPoint_t1814926600_il2cpp_TypeInfo_var));
		DnsEndPoint_t1814926600 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		DnsEndPoint_t1814926600 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = DnsEndPoint_get_Host_m1042524665(L_5, /*hidden argument*/NULL);
		TcpClientSession_set_HostName_m311133692(__this, L_6, /*hidden argument*/NULL);
		return;
	}

IL_0030:
	{
		EndPoint_t1294049535 * L_7 = ___remoteEndPoint;
		V_1 = ((IPEndPoint_t1265996582 *)IsInstClass(L_7, IPEndPoint_t1265996582_il2cpp_TypeInfo_var));
		IPEndPoint_t1265996582 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		IPEndPoint_t1265996582 * L_9 = V_1;
		NullCheck(L_9);
		IPAddress_t3220500535 * L_10 = IPEndPoint_get_Address_m1282959913(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		TcpClientSession_set_HostName_m311133692(__this, L_11, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
// System.Int32 SuperSocket.ClientEngine.TcpClientSession::get_ReceiveBufferSize()
extern "C"  int32_t TcpClientSession_get_ReceiveBufferSize_m1207241433 (TcpClientSession_t1301539049 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ClientSession_get_ReceiveBufferSize_m487623082(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void SuperSocket.ClientEngine.TcpClientSession::set_ReceiveBufferSize(System.Int32)
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegment_1_get_Array_m2310099123_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3500595078;
extern const uint32_t TcpClientSession_set_ReceiveBufferSize_m3022855812_MetadataUsageId;
extern "C"  void TcpClientSession_set_ReceiveBufferSize_m3022855812 (TcpClientSession_t1301539049 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TcpClientSession_set_ReceiveBufferSize_m3022855812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArraySegment_1_t2801744866  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ArraySegment_1_t2801744866  L_0 = ClientSession_get_Buffer_m4110712514(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		ByteU5BU5D_t58506160* L_1 = ArraySegment_1_get_Array_m2310099123((&V_0), /*hidden argument*/ArraySegment_1_get_Array_m2310099123_MethodInfo_var);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Exception_t1967233988 * L_2 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_2, _stringLiteral3500595078, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001b:
	{
		int32_t L_3 = ___value;
		ClientSession_set_ReceiveBufferSize_m2308395257(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SuperSocket.ClientEngine.TcpClientSession::IsIgnorableException(System.Exception)
extern TypeInfo* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern TypeInfo* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern const uint32_t TcpClientSession_IsIgnorableException_m3961902810_MetadataUsageId;
extern "C"  bool TcpClientSession_IsIgnorableException_m3961902810 (TcpClientSession_t1301539049 * __this, Exception_t1967233988 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TcpClientSession_IsIgnorableException_m3961902810_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = ___e;
		if (!((ObjectDisposedException_t973246880 *)IsInstClass(L_0, ObjectDisposedException_t973246880_il2cpp_TypeInfo_var)))
		{
			goto IL_000a;
		}
	}
	{
		return (bool)1;
	}

IL_000a:
	{
		Exception_t1967233988 * L_1 = ___e;
		if (!((NullReferenceException_t3216235232 *)IsInstClass(L_1, NullReferenceException_t3216235232_il2cpp_TypeInfo_var)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)1;
	}

IL_0014:
	{
		return (bool)0;
	}
}
// System.Boolean SuperSocket.ClientEngine.TcpClientSession::IsIgnorableSocketError(System.Int32)
extern "C"  bool TcpClientSession_IsIgnorableSocketError_m1357922689 (TcpClientSession_t1301539049 * __this, int32_t ___errorCode, const MethodInfo* method)
{
	{
		int32_t L_0 = ___errorCode;
		if ((((int32_t)L_0) == ((int32_t)((int32_t)10058))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___errorCode;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)10053))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = ___errorCode;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)10053)))))
		{
			goto IL_001a;
		}
	}

IL_0018:
	{
		return (bool)1;
	}

IL_001a:
	{
		return (bool)0;
	}
}
// System.Void SuperSocket.ClientEngine.TcpClientSession::Connect()
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_1_t150131123_il2cpp_TypeInfo_var;
extern TypeInfo* IProxyConnector_t1374585095_il2cpp_TypeInfo_var;
extern TypeInfo* ConnectedCallback_t1584521517_il2cpp_TypeInfo_var;
extern const MethodInfo* TcpClientSession_Proxy_Completed_m4143037264_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3142821637_MethodInfo_var;
extern const MethodInfo* TcpClientSession_ProcessConnect_m1201190206_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2351858832;
extern Il2CppCodeGenString* _stringLiteral2067822994;
extern const uint32_t TcpClientSession_Connect_m1891457046_MetadataUsageId;
extern "C"  void TcpClientSession_Connect_m1891457046 (TcpClientSession_t1301539049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TcpClientSession_Connect_m1891457046_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_InConnecting_11();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Exception_t1967233988 * L_1 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_1, _stringLiteral2351858832, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		Socket_t150013987 * L_2 = ClientSession_get_Client_m275537367(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		Exception_t1967233988 * L_3 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_3, _stringLiteral2067822994, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0026:
	{
		Il2CppObject * L_4 = ClientSession_get_Proxy_m2237735788(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005e;
		}
	}
	{
		Il2CppObject * L_5 = ClientSession_get_Proxy_m2237735788(__this, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)TcpClientSession_Proxy_Completed_m4143037264_MethodInfo_var);
		EventHandler_1_t150131123 * L_7 = (EventHandler_1_t150131123 *)il2cpp_codegen_object_new(EventHandler_1_t150131123_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m3142821637(L_7, __this, L_6, /*hidden argument*/EventHandler_1__ctor_m3142821637_MethodInfo_var);
		NullCheck(L_5);
		InterfaceActionInvoker1< EventHandler_1_t150131123 * >::Invoke(1 /* System.Void SuperSocket.ClientEngine.IProxyConnector::add_Completed(System.EventHandler`1<SuperSocket.ClientEngine.ProxyEventArgs>) */, IProxyConnector_t1374585095_il2cpp_TypeInfo_var, L_5, L_7);
		Il2CppObject * L_8 = ClientSession_get_Proxy_m2237735788(__this, /*hidden argument*/NULL);
		EndPoint_t1294049535 * L_9 = ClientSession_get_RemoteEndPoint_m2153238163(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		InterfaceActionInvoker1< EndPoint_t1294049535 * >::Invoke(0 /* System.Void SuperSocket.ClientEngine.IProxyConnector::Connect(System.Net.EndPoint) */, IProxyConnector_t1374585095_il2cpp_TypeInfo_var, L_8, L_9);
		__this->set_m_InConnecting_11((bool)1);
		return;
	}

IL_005e:
	{
		__this->set_m_InConnecting_11((bool)1);
		EndPoint_t1294049535 * L_10 = ClientSession_get_RemoteEndPoint_m2153238163(__this, /*hidden argument*/NULL);
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)TcpClientSession_ProcessConnect_m1201190206_MethodInfo_var);
		ConnectedCallback_t1584521517 * L_12 = (ConnectedCallback_t1584521517 *)il2cpp_codegen_object_new(ConnectedCallback_t1584521517_il2cpp_TypeInfo_var);
		ConnectedCallback__ctor_m3119060258(L_12, __this, L_11, /*hidden argument*/NULL);
		ConnectAsyncExtension_ConnectAsync_m2701800175(NULL /*static, unused*/, L_10, L_12, NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.TcpClientSession::Proxy_Completed(System.Object,SuperSocket.ClientEngine.ProxyEventArgs)
extern TypeInfo* EventHandler_1_t150131123_il2cpp_TypeInfo_var;
extern TypeInfo* IProxyConnector_t1374585095_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* TcpClientSession_Proxy_Completed_m4143037264_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3142821637_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral623748566;
extern const uint32_t TcpClientSession_Proxy_Completed_m4143037264_MetadataUsageId;
extern "C"  void TcpClientSession_Proxy_Completed_m4143037264 (TcpClientSession_t1301539049 * __this, Il2CppObject * ___sender, ProxyEventArgs_t3602520776 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TcpClientSession_Proxy_Completed_m4143037264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ClientSession_get_Proxy_m2237735788(__this, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)TcpClientSession_Proxy_Completed_m4143037264_MethodInfo_var);
		EventHandler_1_t150131123 * L_2 = (EventHandler_1_t150131123 *)il2cpp_codegen_object_new(EventHandler_1_t150131123_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m3142821637(L_2, __this, L_1, /*hidden argument*/EventHandler_1__ctor_m3142821637_MethodInfo_var);
		NullCheck(L_0);
		InterfaceActionInvoker1< EventHandler_1_t150131123 * >::Invoke(2 /* System.Void SuperSocket.ClientEngine.IProxyConnector::remove_Completed(System.EventHandler`1<SuperSocket.ClientEngine.ProxyEventArgs>) */, IProxyConnector_t1374585095_il2cpp_TypeInfo_var, L_0, L_2);
		ProxyEventArgs_t3602520776 * L_3 = ___e;
		NullCheck(L_3);
		bool L_4 = ProxyEventArgs_get_Connected_m3880444133(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		ProxyEventArgs_t3602520776 * L_5 = ___e;
		NullCheck(L_5);
		Socket_t150013987 * L_6 = ProxyEventArgs_get_Socket_m476433455(L_5, /*hidden argument*/NULL);
		TcpClientSession_ProcessConnect_m1201190206(__this, L_6, NULL, (SocketAsyncEventArgs_t970431102 *)NULL, /*hidden argument*/NULL);
		return;
	}

IL_002e:
	{
		ProxyEventArgs_t3602520776 * L_7 = ___e;
		NullCheck(L_7);
		Exception_t1967233988 * L_8 = ProxyEventArgs_get_Exception_m210430180(L_7, /*hidden argument*/NULL);
		Exception_t1967233988 * L_9 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m1328171222(L_9, _stringLiteral623748566, L_8, /*hidden argument*/NULL);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_9);
		__this->set_m_InConnecting_11((bool)0);
		return;
	}
}
// System.Void SuperSocket.ClientEngine.TcpClientSession::ProcessConnect(System.Net.Sockets.Socket,System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern TypeInfo* SocketException_t964769420_il2cpp_TypeInfo_var;
extern TypeInfo* SocketAsyncEventArgs_t970431102_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_1_t1813008745_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3542837379_MethodInfo_var;
extern const uint32_t TcpClientSession_ProcessConnect_m1201190206_MetadataUsageId;
extern "C"  void TcpClientSession_ProcessConnect_m1201190206 (TcpClientSession_t1301539049 * __this, Socket_t150013987 * ___socket, Il2CppObject * ___state, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TcpClientSession_ProcessConnect_m1201190206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SocketAsyncEventArgs_t970431102 * L_0 = ___e;
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		SocketAsyncEventArgs_t970431102 * L_1 = ___e;
		NullCheck(L_1);
		int32_t L_2 = SocketAsyncEventArgs_get_SocketError_m3030139504(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		__this->set_m_InConnecting_11((bool)0);
		SocketAsyncEventArgs_t970431102 * L_3 = ___e;
		NullCheck(L_3);
		int32_t L_4 = SocketAsyncEventArgs_get_SocketError_m3030139504(L_3, /*hidden argument*/NULL);
		SocketException_t964769420 * L_5 = (SocketException_t964769420 *)il2cpp_codegen_object_new(SocketException_t964769420_il2cpp_TypeInfo_var);
		SocketException__ctor_m2020928431(L_5, L_4, /*hidden argument*/NULL);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_5);
		return;
	}

IL_0024:
	{
		Socket_t150013987 * L_6 = ___socket;
		if (L_6)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_m_InConnecting_11((bool)0);
		SocketException_t964769420 * L_7 = (SocketException_t964769420 *)il2cpp_codegen_object_new(SocketException_t964769420_il2cpp_TypeInfo_var);
		SocketException__ctor_m2020928431(L_7, ((int32_t)10053), /*hidden argument*/NULL);
		VirtActionInvoker1< Exception_t1967233988 * >::Invoke(12 /* System.Void SuperSocket.ClientEngine.ClientSession::OnError(System.Exception) */, __this, L_7);
		return;
	}

IL_003f:
	{
		SocketAsyncEventArgs_t970431102 * L_8 = ___e;
		if (L_8)
		{
			goto IL_0049;
		}
	}
	{
		SocketAsyncEventArgs_t970431102 * L_9 = (SocketAsyncEventArgs_t970431102 *)il2cpp_codegen_object_new(SocketAsyncEventArgs_t970431102_il2cpp_TypeInfo_var);
		SocketAsyncEventArgs__ctor_m2409327454(L_9, /*hidden argument*/NULL);
		___e = L_9;
	}

IL_0049:
	{
		SocketAsyncEventArgs_t970431102 * L_10 = ___e;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)GetVirtualMethodInfo(__this, 24));
		EventHandler_1_t1813008745 * L_12 = (EventHandler_1_t1813008745 *)il2cpp_codegen_object_new(EventHandler_1_t1813008745_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m3542837379(L_12, __this, L_11, /*hidden argument*/EventHandler_1__ctor_m3542837379_MethodInfo_var);
		NullCheck(L_10);
		SocketAsyncEventArgs_add_Completed_m504170843(L_10, L_12, /*hidden argument*/NULL);
		Socket_t150013987 * L_13 = ___socket;
		ClientSession_set_Client_m911781880(__this, L_13, /*hidden argument*/NULL);
		__this->set_m_InConnecting_11((bool)0);
		Socket_t150013987 * L_14 = ClientSession_get_Client_m275537367(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Socket_SetSocketOption_m3110410751(L_14, ((int32_t)65535), 8, (bool)1, /*hidden argument*/NULL);
		SocketAsyncEventArgs_t970431102 * L_15 = ___e;
		VirtActionInvoker1< SocketAsyncEventArgs_t970431102 * >::Invoke(25 /* System.Void SuperSocket.ClientEngine.TcpClientSession::OnGetSocket(System.Net.Sockets.SocketAsyncEventArgs) */, __this, L_15);
		return;
	}
}
// System.Boolean SuperSocket.ClientEngine.TcpClientSession::EnsureSocketClosed()
extern "C"  bool TcpClientSession_EnsureSocketClosed_m2842965627 (TcpClientSession_t1301539049 * __this, const MethodInfo* method)
{
	{
		bool L_0 = TcpClientSession_EnsureSocketClosed_m4142523302(__this, (Socket_t150013987 *)NULL, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean SuperSocket.ClientEngine.TcpClientSession::EnsureSocketClosed(System.Net.Sockets.Socket)
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TcpClientSession_EnsureSocketClosed_m4142523302_MetadataUsageId;
extern "C"  bool TcpClientSession_EnsureSocketClosed_m4142523302 (TcpClientSession_t1301539049 * __this, Socket_t150013987 * ___prevClient, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TcpClientSession_EnsureSocketClosed_m4142523302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Socket_t150013987 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Socket_t150013987 * L_0 = ClientSession_get_Client_m275537367(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Socket_t150013987 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (bool)0;
	}

IL_000c:
	{
		V_1 = (bool)1;
		Socket_t150013987 * L_2 = ___prevClient;
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		Socket_t150013987 * L_3 = ___prevClient;
		Socket_t150013987 * L_4 = V_0;
		if ((((Il2CppObject*)(Socket_t150013987 *)L_3) == ((Il2CppObject*)(Socket_t150013987 *)L_4)))
		{
			goto IL_001b;
		}
	}
	{
		Socket_t150013987 * L_5 = ___prevClient;
		V_0 = L_5;
		V_1 = (bool)0;
		goto IL_002b;
	}

IL_001b:
	{
		ClientSession_set_Client_m911781880(__this, (Socket_t150013987 *)NULL, /*hidden argument*/NULL);
		il2cpp_codegen_memory_barrier();
		__this->set_IsSending_13(0);
	}

IL_002b:
	{
		Socket_t150013987 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = Socket_get_Connected_m32348333(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004d;
		}
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Socket_t150013987 * L_8 = V_0;
			NullCheck(L_8);
			Socket_Shutdown_m202135370(L_8, 2, /*hidden argument*/NULL);
			goto IL_003f;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_003c;
			throw e;
		}

CATCH_003c:
		{ // begin catch(System.Object)
			goto IL_003f;
		} // end catch (depth: 2)

IL_003f:
		{
			IL2CPP_LEAVE(0x4D, FINALLY_0041);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		try
		{ // begin try (depth: 2)
			Socket_t150013987 * L_9 = V_0;
			NullCheck(L_9);
			Socket_Close_m183746607(L_9, /*hidden argument*/NULL);
			goto IL_004c;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0049;
			throw e;
		}

CATCH_0049:
		{ // begin catch(System.Object)
			goto IL_004c;
		} // end catch (depth: 2)

IL_004c:
		{
			IL2CPP_END_FINALLY(65)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x4D, IL_004d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004d:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void SuperSocket.ClientEngine.TcpClientSession::DetectConnected()
extern TypeInfo* SocketException_t964769420_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1728535773;
extern const uint32_t TcpClientSession_DetectConnected_m1627322578_MetadataUsageId;
extern "C"  void TcpClientSession_DetectConnected_m1627322578 (TcpClientSession_t1301539049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TcpClientSession_DetectConnected_m1627322578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Socket_t150013987 * L_0 = ClientSession_get_Client_m275537367(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		SocketException_t964769420 * L_1 = (SocketException_t964769420 *)il2cpp_codegen_object_new(SocketException_t964769420_il2cpp_TypeInfo_var);
		SocketException__ctor_m2020928431(L_1, ((int32_t)10057), /*hidden argument*/NULL);
		Exception_t1967233988 * L_2 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m1328171222(L_2, _stringLiteral1728535773, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// System.Void SuperSocket.ClientEngine.TcpClientSession::Send(System.Byte[],System.Int32,System.Int32)
extern const MethodInfo* ArraySegment_1__ctor_m3685935155_MethodInfo_var;
extern const MethodInfo* ConcurrentQueue_1_Enqueue_m1619824676_MethodInfo_var;
extern const uint32_t TcpClientSession_Send_m1729004651_MetadataUsageId;
extern "C"  void TcpClientSession_Send_m1729004651 (TcpClientSession_t1301539049 * __this, ByteU5BU5D_t58506160* ___data, int32_t ___offset, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TcpClientSession_Send_m1729004651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TcpClientSession_DetectConnected_m1627322578(__this, /*hidden argument*/NULL);
		ConcurrentQueue_1_t2535351335 * L_0 = __this->get_m_SendingQueue_12();
		ByteU5BU5D_t58506160* L_1 = ___data;
		int32_t L_2 = ___offset;
		int32_t L_3 = ___length;
		ArraySegment_1_t2801744866  L_4;
		memset(&L_4, 0, sizeof(L_4));
		ArraySegment_1__ctor_m3685935155(&L_4, L_1, L_2, L_3, /*hidden argument*/ArraySegment_1__ctor_m3685935155_MethodInfo_var);
		NullCheck(L_0);
		ConcurrentQueue_1_Enqueue_m1619824676(L_0, L_4, /*hidden argument*/ConcurrentQueue_1_Enqueue_m1619824676_MethodInfo_var);
		bool L_5 = __this->get_IsSending_13();
		il2cpp_codegen_memory_barrier();
		if (L_5)
		{
			goto IL_002a;
		}
	}
	{
		TcpClientSession_DequeueSend_m881139676(__this, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Boolean SuperSocket.ClientEngine.TcpClientSession::DequeueSend()
extern const MethodInfo* ConcurrentQueue_1_TryDequeue_m1933623995_MethodInfo_var;
extern const uint32_t TcpClientSession_DequeueSend_m881139676_MetadataUsageId;
extern "C"  bool TcpClientSession_DequeueSend_m881139676 (TcpClientSession_t1301539049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TcpClientSession_DequeueSend_m881139676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArraySegment_1_t2801744866  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		il2cpp_codegen_memory_barrier();
		__this->set_IsSending_13(1);
		ConcurrentQueue_1_t2535351335 * L_0 = __this->get_m_SendingQueue_12();
		NullCheck(L_0);
		bool L_1 = ConcurrentQueue_1_TryDequeue_m1933623995(L_0, (&V_0), /*hidden argument*/ConcurrentQueue_1_TryDequeue_m1933623995_MethodInfo_var);
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		il2cpp_codegen_memory_barrier();
		__this->set_IsSending_13(0);
		return (bool)0;
	}

IL_0023:
	{
		ArraySegment_1_t2801744866  L_2 = V_0;
		VirtActionInvoker1< ArraySegment_1_t2801744866  >::Invoke(26 /* System.Void SuperSocket.ClientEngine.TcpClientSession::SendInternal(System.ArraySegment`1<System.Byte>) */, __this, L_2);
		return (bool)1;
	}
}
// System.Void SuperSocket.ClientEngine.TcpClientSession::Close()
extern "C"  void TcpClientSession_Close_m3550666852 (TcpClientSession_t1301539049 * __this, const MethodInfo* method)
{
	{
		bool L_0 = TcpClientSession_EnsureSocketClosed_m2842965627(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		VirtActionInvoker0::Invoke(9 /* System.Void SuperSocket.ClientEngine.ClientSession::OnClosed() */, __this);
	}

IL_000e:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
