﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.TimeZoneInfo/TimeType>
struct ShimEnumerator_t4203796918;
// System.Collections.Generic.Dictionary`2<System.Int32,System.TimeZoneInfo/TimeType>
struct Dictionary_2_t2773072230;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.TimeZoneInfo/TimeType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2407026583_gshared (ShimEnumerator_t4203796918 * __this, Dictionary_2_t2773072230 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m2407026583(__this, ___host, method) ((  void (*) (ShimEnumerator_t4203796918 *, Dictionary_2_t2773072230 *, const MethodInfo*))ShimEnumerator__ctor_m2407026583_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.TimeZoneInfo/TimeType>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2344536874_gshared (ShimEnumerator_t4203796918 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2344536874(__this, method) ((  bool (*) (ShimEnumerator_t4203796918 *, const MethodInfo*))ShimEnumerator_MoveNext_m2344536874_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.TimeZoneInfo/TimeType>::get_Entry()
extern "C"  DictionaryEntry_t130027246  ShimEnumerator_get_Entry_m1484220714_gshared (ShimEnumerator_t4203796918 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1484220714(__this, method) ((  DictionaryEntry_t130027246  (*) (ShimEnumerator_t4203796918 *, const MethodInfo*))ShimEnumerator_get_Entry_m1484220714_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.TimeZoneInfo/TimeType>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3395323653_gshared (ShimEnumerator_t4203796918 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3395323653(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4203796918 *, const MethodInfo*))ShimEnumerator_get_Key_m3395323653_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.TimeZoneInfo/TimeType>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m4074289239_gshared (ShimEnumerator_t4203796918 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m4074289239(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4203796918 *, const MethodInfo*))ShimEnumerator_get_Value_m4074289239_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.TimeZoneInfo/TimeType>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m4101442847_gshared (ShimEnumerator_t4203796918 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m4101442847(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4203796918 *, const MethodInfo*))ShimEnumerator_get_Current_m4101442847_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.TimeZoneInfo/TimeType>::Reset()
extern "C"  void ShimEnumerator_Reset_m3633018729_gshared (ShimEnumerator_t4203796918 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3633018729(__this, method) ((  void (*) (ShimEnumerator_t4203796918 *, const MethodInfo*))ShimEnumerator_Reset_m3633018729_gshared)(__this, method)
