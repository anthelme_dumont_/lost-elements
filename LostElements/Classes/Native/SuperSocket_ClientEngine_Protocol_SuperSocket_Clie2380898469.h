﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IList`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>
struct IList_1_t3895840460;
// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>
struct ArraySegmentEx_1_t1729348146;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>
struct  ArraySegmentList_1_t2380898469  : public Il2CppObject
{
public:
	// System.Collections.Generic.IList`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<T>> SuperSocket.ClientEngine.Protocol.ArraySegmentList`1::m_Segments
	Il2CppObject* ___m_Segments_0;
	// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<T> SuperSocket.ClientEngine.Protocol.ArraySegmentList`1::m_PrevSegment
	ArraySegmentEx_1_t1729348146 * ___m_PrevSegment_1;
	// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1::m_PrevSegmentIndex
	int32_t ___m_PrevSegmentIndex_2;
	// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1::m_Count
	int32_t ___m_Count_3;

public:
	inline static int32_t get_offset_of_m_Segments_0() { return static_cast<int32_t>(offsetof(ArraySegmentList_1_t2380898469, ___m_Segments_0)); }
	inline Il2CppObject* get_m_Segments_0() const { return ___m_Segments_0; }
	inline Il2CppObject** get_address_of_m_Segments_0() { return &___m_Segments_0; }
	inline void set_m_Segments_0(Il2CppObject* value)
	{
		___m_Segments_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Segments_0, value);
	}

	inline static int32_t get_offset_of_m_PrevSegment_1() { return static_cast<int32_t>(offsetof(ArraySegmentList_1_t2380898469, ___m_PrevSegment_1)); }
	inline ArraySegmentEx_1_t1729348146 * get_m_PrevSegment_1() const { return ___m_PrevSegment_1; }
	inline ArraySegmentEx_1_t1729348146 ** get_address_of_m_PrevSegment_1() { return &___m_PrevSegment_1; }
	inline void set_m_PrevSegment_1(ArraySegmentEx_1_t1729348146 * value)
	{
		___m_PrevSegment_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_PrevSegment_1, value);
	}

	inline static int32_t get_offset_of_m_PrevSegmentIndex_2() { return static_cast<int32_t>(offsetof(ArraySegmentList_1_t2380898469, ___m_PrevSegmentIndex_2)); }
	inline int32_t get_m_PrevSegmentIndex_2() const { return ___m_PrevSegmentIndex_2; }
	inline int32_t* get_address_of_m_PrevSegmentIndex_2() { return &___m_PrevSegmentIndex_2; }
	inline void set_m_PrevSegmentIndex_2(int32_t value)
	{
		___m_PrevSegmentIndex_2 = value;
	}

	inline static int32_t get_offset_of_m_Count_3() { return static_cast<int32_t>(offsetof(ArraySegmentList_1_t2380898469, ___m_Count_3)); }
	inline int32_t get_m_Count_3() const { return ___m_Count_3; }
	inline int32_t* get_address_of_m_Count_3() { return &___m_Count_3; }
	inline void set_m_Count_3(int32_t value)
	{
		___m_Count_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
