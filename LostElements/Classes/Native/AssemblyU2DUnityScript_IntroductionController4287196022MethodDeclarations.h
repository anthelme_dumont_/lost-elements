﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IntroductionController
struct IntroductionController_t4287196022;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void IntroductionController::.ctor()
extern "C"  void IntroductionController__ctor_m3934298010 (IntroductionController_t4287196022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntroductionController::.cctor()
extern "C"  void IntroductionController__cctor_m1222057811 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IntroductionController::Start()
extern "C"  Il2CppObject * IntroductionController_Start_m4160700360 (IntroductionController_t4287196022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntroductionController::Main()
extern "C"  void IntroductionController_Main_m2397711843 (IntroductionController_t4287196022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
