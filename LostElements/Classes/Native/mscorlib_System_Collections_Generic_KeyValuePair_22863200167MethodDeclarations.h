﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22863200167.h"
#include "mscorlib_System_DateTime339033936.h"
#include "System_Core_System_TimeZoneInfo_TimeType2282261447.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m435210039_gshared (KeyValuePair_2_t2863200167 * __this, DateTime_t339033936  ___key, TimeType_t2282261447  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m435210039(__this, ___key, ___value, method) ((  void (*) (KeyValuePair_2_t2863200167 *, DateTime_t339033936 , TimeType_t2282261447 , const MethodInfo*))KeyValuePair_2__ctor_m435210039_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>::get_Key()
extern "C"  DateTime_t339033936  KeyValuePair_2_get_Key_m1163845744_gshared (KeyValuePair_2_t2863200167 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1163845744(__this, method) ((  DateTime_t339033936  (*) (KeyValuePair_2_t2863200167 *, const MethodInfo*))KeyValuePair_2_get_Key_m1163845744_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2699759388_gshared (KeyValuePair_2_t2863200167 * __this, DateTime_t339033936  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2699759388(__this, ___value, method) ((  void (*) (KeyValuePair_2_t2863200167 *, DateTime_t339033936 , const MethodInfo*))KeyValuePair_2_set_Key_m2699759388_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>::get_Value()
extern "C"  TimeType_t2282261447  KeyValuePair_2_get_Value_m1879345057_gshared (KeyValuePair_2_t2863200167 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1879345057(__this, method) ((  TimeType_t2282261447  (*) (KeyValuePair_2_t2863200167 *, const MethodInfo*))KeyValuePair_2_get_Value_m1879345057_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4239488284_gshared (KeyValuePair_2_t2863200167 * __this, TimeType_t2282261447  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m4239488284(__this, ___value, method) ((  void (*) (KeyValuePair_2_t2863200167 *, TimeType_t2282261447 , const MethodInfo*))KeyValuePair_2_set_Value_m4239488284_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2550307206_gshared (KeyValuePair_2_t2863200167 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2550307206(__this, method) ((  String_t* (*) (KeyValuePair_2_t2863200167 *, const MethodInfo*))KeyValuePair_2_ToString_m2550307206_gshared)(__this, method)
