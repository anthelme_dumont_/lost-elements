﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.MessageReceivedEventArgs
struct MessageReceivedEventArgs_t3871897919;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void WebSocket4Net.MessageReceivedEventArgs::.ctor(System.String)
extern "C"  void MessageReceivedEventArgs__ctor_m2681790322 (MessageReceivedEventArgs_t3871897919 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.MessageReceivedEventArgs::get_Message()
extern "C"  String_t* MessageReceivedEventArgs_get_Message_m1329338057 (MessageReceivedEventArgs_t3871897919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.MessageReceivedEventArgs::set_Message(System.String)
extern "C"  void MessageReceivedEventArgs_set_Message_m258412138 (MessageReceivedEventArgs_t3871897919 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
