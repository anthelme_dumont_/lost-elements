﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Messages.TextMessage
struct TextMessage_t120705644;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SocketIOClient.Messages.TextMessage::.ctor()
extern "C"  void TextMessage__ctor_m374255273 (TextMessage_t120705644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.TextMessage::.ctor(System.String)
extern "C"  void TextMessage__ctor_m3376925433 (TextMessage_t120705644 * __this, String_t* ___textMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.TextMessage::get_Event()
extern "C"  String_t* TextMessage_get_Event_m3886918939 (TextMessage_t120705644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.TextMessage SocketIOClient.Messages.TextMessage::Deserialize(System.String)
extern "C"  TextMessage_t120705644 * TextMessage_Deserialize_m2513453003 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
