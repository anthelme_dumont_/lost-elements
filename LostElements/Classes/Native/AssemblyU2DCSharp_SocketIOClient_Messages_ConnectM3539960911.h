﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_SocketIOClient_Messages_Message957426777.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIOClient.Messages.ConnectMessage
struct  ConnectMessage_t3539960911  : public Message_t957426777
{
public:
	// System.String SocketIOClient.Messages.ConnectMessage::<Query>k__BackingField
	String_t* ___U3CQueryU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CQueryU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ConnectMessage_t3539960911, ___U3CQueryU3Ek__BackingField_10)); }
	inline String_t* get_U3CQueryU3Ek__BackingField_10() const { return ___U3CQueryU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CQueryU3Ek__BackingField_10() { return &___U3CQueryU3Ek__BackingField_10; }
	inline void set_U3CQueryU3Ek__BackingField_10(String_t* value)
	{
		___U3CQueryU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CQueryU3Ek__BackingField_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
