﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>
struct Dictionary_2_t1476369908;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>
struct Dictionary_2_t1119712961;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIOClient.Eventing.RegistrationManager
struct  RegistrationManager_t998967514  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>> SocketIOClient.Eventing.RegistrationManager::callBackRegistry
	Dictionary_2_t1476369908 * ___callBackRegistry_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<SocketIOClient.Messages.IMessage>> SocketIOClient.Eventing.RegistrationManager::eventNameRegistry
	Dictionary_2_t1119712961 * ___eventNameRegistry_1;

public:
	inline static int32_t get_offset_of_callBackRegistry_0() { return static_cast<int32_t>(offsetof(RegistrationManager_t998967514, ___callBackRegistry_0)); }
	inline Dictionary_2_t1476369908 * get_callBackRegistry_0() const { return ___callBackRegistry_0; }
	inline Dictionary_2_t1476369908 ** get_address_of_callBackRegistry_0() { return &___callBackRegistry_0; }
	inline void set_callBackRegistry_0(Dictionary_2_t1476369908 * value)
	{
		___callBackRegistry_0 = value;
		Il2CppCodeGenWriteBarrier(&___callBackRegistry_0, value);
	}

	inline static int32_t get_offset_of_eventNameRegistry_1() { return static_cast<int32_t>(offsetof(RegistrationManager_t998967514, ___eventNameRegistry_1)); }
	inline Dictionary_2_t1119712961 * get_eventNameRegistry_1() const { return ___eventNameRegistry_1; }
	inline Dictionary_2_t1119712961 ** get_address_of_eventNameRegistry_1() { return &___eventNameRegistry_1; }
	inline void set_eventNameRegistry_1(Dictionary_2_t1119712961 * value)
	{
		___eventNameRegistry_1 = value;
		Il2CppCodeGenWriteBarrier(&___eventNameRegistry_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
