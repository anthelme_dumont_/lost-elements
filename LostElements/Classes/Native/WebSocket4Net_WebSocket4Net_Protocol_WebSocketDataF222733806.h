﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SuperSocket.ClientEngine.Protocol.ArraySegmentList
struct ArraySegmentList_t1783467835;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.WebSocketDataFrame
struct  WebSocketDataFrame_t222733806  : public Il2CppObject
{
public:
	// SuperSocket.ClientEngine.Protocol.ArraySegmentList WebSocket4Net.Protocol.WebSocketDataFrame::m_InnerData
	ArraySegmentList_t1783467835 * ___m_InnerData_0;
	// System.Int64 WebSocket4Net.Protocol.WebSocketDataFrame::m_ActualPayloadLength
	int64_t ___m_ActualPayloadLength_1;
	// System.Byte[] WebSocket4Net.Protocol.WebSocketDataFrame::<MaskKey>k__BackingField
	ByteU5BU5D_t58506160* ___U3CMaskKeyU3Ek__BackingField_2;
	// System.Byte[] WebSocket4Net.Protocol.WebSocketDataFrame::<ExtensionData>k__BackingField
	ByteU5BU5D_t58506160* ___U3CExtensionDataU3Ek__BackingField_3;
	// System.Byte[] WebSocket4Net.Protocol.WebSocketDataFrame::<ApplicationData>k__BackingField
	ByteU5BU5D_t58506160* ___U3CApplicationDataU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_m_InnerData_0() { return static_cast<int32_t>(offsetof(WebSocketDataFrame_t222733806, ___m_InnerData_0)); }
	inline ArraySegmentList_t1783467835 * get_m_InnerData_0() const { return ___m_InnerData_0; }
	inline ArraySegmentList_t1783467835 ** get_address_of_m_InnerData_0() { return &___m_InnerData_0; }
	inline void set_m_InnerData_0(ArraySegmentList_t1783467835 * value)
	{
		___m_InnerData_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_InnerData_0, value);
	}

	inline static int32_t get_offset_of_m_ActualPayloadLength_1() { return static_cast<int32_t>(offsetof(WebSocketDataFrame_t222733806, ___m_ActualPayloadLength_1)); }
	inline int64_t get_m_ActualPayloadLength_1() const { return ___m_ActualPayloadLength_1; }
	inline int64_t* get_address_of_m_ActualPayloadLength_1() { return &___m_ActualPayloadLength_1; }
	inline void set_m_ActualPayloadLength_1(int64_t value)
	{
		___m_ActualPayloadLength_1 = value;
	}

	inline static int32_t get_offset_of_U3CMaskKeyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebSocketDataFrame_t222733806, ___U3CMaskKeyU3Ek__BackingField_2)); }
	inline ByteU5BU5D_t58506160* get_U3CMaskKeyU3Ek__BackingField_2() const { return ___U3CMaskKeyU3Ek__BackingField_2; }
	inline ByteU5BU5D_t58506160** get_address_of_U3CMaskKeyU3Ek__BackingField_2() { return &___U3CMaskKeyU3Ek__BackingField_2; }
	inline void set_U3CMaskKeyU3Ek__BackingField_2(ByteU5BU5D_t58506160* value)
	{
		___U3CMaskKeyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMaskKeyU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CExtensionDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebSocketDataFrame_t222733806, ___U3CExtensionDataU3Ek__BackingField_3)); }
	inline ByteU5BU5D_t58506160* get_U3CExtensionDataU3Ek__BackingField_3() const { return ___U3CExtensionDataU3Ek__BackingField_3; }
	inline ByteU5BU5D_t58506160** get_address_of_U3CExtensionDataU3Ek__BackingField_3() { return &___U3CExtensionDataU3Ek__BackingField_3; }
	inline void set_U3CExtensionDataU3Ek__BackingField_3(ByteU5BU5D_t58506160* value)
	{
		___U3CExtensionDataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CExtensionDataU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CApplicationDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WebSocketDataFrame_t222733806, ___U3CApplicationDataU3Ek__BackingField_4)); }
	inline ByteU5BU5D_t58506160* get_U3CApplicationDataU3Ek__BackingField_4() const { return ___U3CApplicationDataU3Ek__BackingField_4; }
	inline ByteU5BU5D_t58506160** get_address_of_U3CApplicationDataU3Ek__BackingField_4() { return &___U3CApplicationDataU3Ek__BackingField_4; }
	inline void set_U3CApplicationDataU3Ek__BackingField_4(ByteU5BU5D_t58506160* value)
	{
		___U3CApplicationDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CApplicationDataU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
