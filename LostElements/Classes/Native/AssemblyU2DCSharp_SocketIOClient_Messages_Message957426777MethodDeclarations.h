﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Messages.Message
struct Message_t957426777;
// System.String
struct String_t;
// SocketIOClient.Messages.JsonEncodedEventMessage
struct JsonEncodedEventMessage_t989636229;
// SocketIOClient.Messages.IMessage
struct IMessage_t3628529648;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_SocketIOClient_SocketIOMessageTyp905678103.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_JsonEncod989636229.h"

// System.Void SocketIOClient.Messages.Message::.ctor()
extern "C"  void Message__ctor_m3082395484 (Message_t957426777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.Message::.ctor(System.String)
extern "C"  void Message__ctor_m523687206 (Message_t957426777 * __this, String_t* ___rawMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.Message::.cctor()
extern "C"  void Message__cctor_m582883281 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.Message::get_RawMessage()
extern "C"  String_t* Message_get_RawMessage_m2306002733 (Message_t957426777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.Message::set_RawMessage(System.String)
extern "C"  void Message_set_RawMessage_m2720466974 (Message_t957426777 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.SocketIOMessageTypes SocketIOClient.Messages.Message::get_MessageType()
extern "C"  int32_t Message_get_MessageType_m2815837072 (Message_t957426777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.Message::set_MessageType(SocketIOClient.SocketIOMessageTypes)
extern "C"  void Message_set_MessageType_m2319013183 (Message_t957426777 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> SocketIOClient.Messages.Message::get_AckId()
extern "C"  Nullable_1_t1438485399  Message_get_AckId_m1933451524 (Message_t957426777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.Message::set_AckId(System.Nullable`1<System.Int32>)
extern "C"  void Message_set_AckId_m1799485069 (Message_t957426777 * __this, Nullable_1_t1438485399  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.Message::get_Endpoint()
extern "C"  String_t* Message_get_Endpoint_m3485176771 (Message_t957426777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String)
extern "C"  void Message_set_Endpoint_m2483199368 (Message_t957426777 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.Message::get_MessageText()
extern "C"  String_t* Message_get_MessageText_m3757705288 (Message_t957426777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.Message::set_MessageText(System.String)
extern "C"  void Message_set_MessageText_m231655857 (Message_t957426777 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.JsonEncodedEventMessage SocketIOClient.Messages.Message::get_JsonEncodedMessage()
extern "C"  JsonEncodedEventMessage_t989636229 * Message_get_JsonEncodedMessage_m3705134714 (Message_t957426777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.Message::set_JsonEncodedMessage(SocketIOClient.Messages.JsonEncodedEventMessage)
extern "C"  void Message_set_JsonEncodedMessage_m1452199601 (Message_t957426777 * __this, JsonEncodedEventMessage_t989636229 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.JsonEncodedEventMessage SocketIOClient.Messages.Message::get_Json()
extern "C"  JsonEncodedEventMessage_t989636229 * Message_get_Json_m1852634497 (Message_t957426777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.Message::set_Json(SocketIOClient.Messages.JsonEncodedEventMessage)
extern "C"  void Message_set_Json_m2290864074 (Message_t957426777 * __this, JsonEncodedEventMessage_t989636229 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.Message::get_Event()
extern "C"  String_t* Message_get_Event_m1041405390 (Message_t957426777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.Message::set_Event(System.String)
extern "C"  void Message_set_Event_m2037541675 (Message_t957426777 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.Message::get_Encoded()
extern "C"  String_t* Message_get_Encoded_m1541307010 (Message_t957426777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.IMessage SocketIOClient.Messages.Message::Factory(System.String)
extern "C"  Il2CppObject * Message_Factory_m1793067991 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
