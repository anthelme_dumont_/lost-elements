﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.Rfc6455Processor
struct Rfc6455Processor_t2702576183;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocket4Net.Protocol.Rfc6455Processor::.ctor()
extern "C"  void Rfc6455Processor__ctor_m2534995212 (Rfc6455Processor_t2702576183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
