﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SimpleJson.Reflection.MemberMapLoader
struct MemberMapLoader_t3073810362;
// SimpleJson.Reflection.SafeDictionary`2<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>
struct SafeDictionary_2_t1629611267;
// SimpleJson.Reflection.SafeDictionary`2<System.Type,SimpleJson.Reflection.CacheResolver/CtorDelegate>
struct SafeDictionary_2_t2099217731;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.Reflection.CacheResolver
struct  CacheResolver_t2193954381  : public Il2CppObject
{
public:
	// SimpleJson.Reflection.MemberMapLoader SimpleJson.Reflection.CacheResolver::_memberMapLoader
	MemberMapLoader_t3073810362 * ____memberMapLoader_0;
	// SimpleJson.Reflection.SafeDictionary`2<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>> SimpleJson.Reflection.CacheResolver::_memberMapsCache
	SafeDictionary_2_t1629611267 * ____memberMapsCache_1;

public:
	inline static int32_t get_offset_of__memberMapLoader_0() { return static_cast<int32_t>(offsetof(CacheResolver_t2193954381, ____memberMapLoader_0)); }
	inline MemberMapLoader_t3073810362 * get__memberMapLoader_0() const { return ____memberMapLoader_0; }
	inline MemberMapLoader_t3073810362 ** get_address_of__memberMapLoader_0() { return &____memberMapLoader_0; }
	inline void set__memberMapLoader_0(MemberMapLoader_t3073810362 * value)
	{
		____memberMapLoader_0 = value;
		Il2CppCodeGenWriteBarrier(&____memberMapLoader_0, value);
	}

	inline static int32_t get_offset_of__memberMapsCache_1() { return static_cast<int32_t>(offsetof(CacheResolver_t2193954381, ____memberMapsCache_1)); }
	inline SafeDictionary_2_t1629611267 * get__memberMapsCache_1() const { return ____memberMapsCache_1; }
	inline SafeDictionary_2_t1629611267 ** get_address_of__memberMapsCache_1() { return &____memberMapsCache_1; }
	inline void set__memberMapsCache_1(SafeDictionary_2_t1629611267 * value)
	{
		____memberMapsCache_1 = value;
		Il2CppCodeGenWriteBarrier(&____memberMapsCache_1, value);
	}
};

struct CacheResolver_t2193954381_StaticFields
{
public:
	// SimpleJson.Reflection.SafeDictionary`2<System.Type,SimpleJson.Reflection.CacheResolver/CtorDelegate> SimpleJson.Reflection.CacheResolver::ConstructorCache
	SafeDictionary_2_t2099217731 * ___ConstructorCache_2;

public:
	inline static int32_t get_offset_of_ConstructorCache_2() { return static_cast<int32_t>(offsetof(CacheResolver_t2193954381_StaticFields, ___ConstructorCache_2)); }
	inline SafeDictionary_2_t2099217731 * get_ConstructorCache_2() const { return ___ConstructorCache_2; }
	inline SafeDictionary_2_t2099217731 ** get_address_of_ConstructorCache_2() { return &___ConstructorCache_2; }
	inline void set_ConstructorCache_2(SafeDictionary_2_t2099217731 * value)
	{
		___ConstructorCache_2 = value;
		Il2CppCodeGenWriteBarrier(&___ConstructorCache_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
