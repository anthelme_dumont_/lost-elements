﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.ErrorEventArgs
struct ErrorEventArgs_t1329266798;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Exception SuperSocket.ClientEngine.ErrorEventArgs::get_Exception()
extern "C"  Exception_t1967233988 * ErrorEventArgs_get_Exception_m2582201342 (ErrorEventArgs_t1329266798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ErrorEventArgs::set_Exception(System.Exception)
extern "C"  void ErrorEventArgs_set_Exception_m3394529449 (ErrorEventArgs_t1329266798 * __this, Exception_t1967233988 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ErrorEventArgs::.ctor(System.Exception)
extern "C"  void ErrorEventArgs__ctor_m1471498585 (ErrorEventArgs_t1329266798 * __this, Exception_t1967233988 * ___exception, const MethodInfo* method) IL2CPP_METHOD_ATTR;
