﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<System.TimeZoneInfo/TimeType>
struct EqualityComparer_1_t1716652097;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeZoneInfo/TimeType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1131510171_gshared (EqualityComparer_1_t1716652097 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m1131510171(__this, method) ((  void (*) (EqualityComparer_1_t1716652097 *, const MethodInfo*))EqualityComparer_1__ctor_m1131510171_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeZoneInfo/TimeType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m234980722_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m234980722(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m234980722_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeZoneInfo/TimeType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2609821228_gshared (EqualityComparer_1_t1716652097 * __this, Il2CppObject * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2609821228(__this, ___obj, method) ((  int32_t (*) (EqualityComparer_1_t1716652097 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2609821228_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeZoneInfo/TimeType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m61301458_gshared (EqualityComparer_1_t1716652097 * __this, Il2CppObject * ___x, Il2CppObject * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m61301458(__this, ___x, ___y, method) ((  bool (*) (EqualityComparer_1_t1716652097 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m61301458_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.TimeZoneInfo/TimeType>::get_Default()
extern "C"  EqualityComparer_1_t1716652097 * EqualityComparer_1_get_Default_m3260236189_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m3260236189(__this /* static, unused */, method) ((  EqualityComparer_1_t1716652097 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m3260236189_gshared)(__this /* static, unused */, method)
