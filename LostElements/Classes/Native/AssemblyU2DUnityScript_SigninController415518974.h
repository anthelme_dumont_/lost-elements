﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SigninController
struct  SigninController_t415518974  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct SigninController_t415518974_StaticFields
{
public:
	// System.String SigninController::playerName
	String_t* ___playerName_2;

public:
	inline static int32_t get_offset_of_playerName_2() { return static_cast<int32_t>(offsetof(SigninController_t415518974_StaticFields, ___playerName_2)); }
	inline String_t* get_playerName_2() const { return ___playerName_2; }
	inline String_t** get_address_of_playerName_2() { return &___playerName_2; }
	inline void set_playerName_2(String_t* value)
	{
		___playerName_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerName_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
