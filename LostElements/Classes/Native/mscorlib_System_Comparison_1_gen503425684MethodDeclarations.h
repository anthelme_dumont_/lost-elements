﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen1721664028MethodDeclarations.h"

// System.Void System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m3058476368(__this, ___object, ___method, method) ((  void (*) (Comparison_1_t503425684 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m449166892_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::Invoke(T,T)
#define Comparison_1_Invoke_m1695186344(__this, ___x, ___y, method) ((  int32_t (*) (Comparison_1_t503425684 *, KeyValuePair_2_t2094718104 , KeyValuePair_2_t2094718104 , const MethodInfo*))Comparison_1_Invoke_m2456628300_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m2656340209(__this, ___x, ___y, ___callback, ___object, method) ((  Il2CppObject * (*) (Comparison_1_t503425684 *, KeyValuePair_2_t2094718104 , KeyValuePair_2_t2094718104 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m898417557_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m1388200388(__this, ___result, method) ((  int32_t (*) (Comparison_1_t503425684 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m2038311072_gshared)(__this, ___result, method)
