﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.DraftHybi00Processor
struct DraftHybi00Processor_t470131883;
// WebSocket4Net.Protocol.ReaderBase
struct ReaderBase_t893022310;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketCommandInfo3536916738.h"
#include "mscorlib_System_String968488902.h"

// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::.ctor()
extern "C"  void DraftHybi00Processor__ctor_m3267995160 (DraftHybi00Processor_t470131883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::.cctor()
extern "C"  void DraftHybi00Processor__cctor_m2041505941 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.Protocol.ReaderBase WebSocket4Net.Protocol.DraftHybi00Processor::CreateHandshakeReader(WebSocket4Net.WebSocket)
extern "C"  ReaderBase_t893022310 * DraftHybi00Processor_CreateHandshakeReader_m2629562542 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.Protocol.DraftHybi00Processor::VerifyHandshake(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo,System.String&)
extern "C"  bool DraftHybi00Processor_VerifyHandshake_m2054616951 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, WebSocketCommandInfo_t3536916738 * ___handshakeInfo, String_t** ___description, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::SendMessage(WebSocket4Net.WebSocket,System.String)
extern "C"  void DraftHybi00Processor_SendMessage_m3239736752 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::SendCloseHandshake(WebSocket4Net.WebSocket,System.Int32,System.String)
extern "C"  void DraftHybi00Processor_SendCloseHandshake_m1678578429 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, int32_t ___statusCode, String_t* ___closeReason, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::SendPing(WebSocket4Net.WebSocket,System.String)
extern "C"  void DraftHybi00Processor_SendPing_m815999519 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, String_t* ___ping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::SendPong(WebSocket4Net.WebSocket,System.String)
extern "C"  void DraftHybi00Processor_SendPong_m1560439001 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, String_t* ___pong, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.DraftHybi00Processor::SendHandshake(WebSocket4Net.WebSocket)
extern "C"  void DraftHybi00Processor_SendHandshake_m334946756 (DraftHybi00Processor_t470131883 * __this, WebSocket_t713846903 * ___websocket, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocket4Net.Protocol.DraftHybi00Processor::GetResponseSecurityKey(System.String,System.String,System.Byte[])
extern "C"  ByteU5BU5D_t58506160* DraftHybi00Processor_GetResponseSecurityKey_m3262218871 (DraftHybi00Processor_t470131883 * __this, String_t* ___secKey1, String_t* ___secKey2, ByteU5BU5D_t58506160* ___secKey3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocket4Net.Protocol.DraftHybi00Processor::GenerateSecKey()
extern "C"  ByteU5BU5D_t58506160* DraftHybi00Processor_GenerateSecKey_m1117508773 (DraftHybi00Processor_t470131883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocket4Net.Protocol.DraftHybi00Processor::GenerateSecKey(System.Int32)
extern "C"  ByteU5BU5D_t58506160* DraftHybi00Processor_GenerateSecKey_m2222897782 (DraftHybi00Processor_t470131883 * __this, int32_t ___totalLen, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.Protocol.DraftHybi00Processor::get_SupportPingPong()
extern "C"  bool DraftHybi00Processor_get_SupportPingPong_m2315093118 (DraftHybi00Processor_t470131883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.Protocol.DraftHybi00Processor::<GetResponseSecurityKey>b__0(System.Char)
extern "C"  bool DraftHybi00Processor_U3CGetResponseSecurityKeyU3Eb__0_m754971777 (Il2CppObject * __this /* static, unused */, uint16_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.Protocol.DraftHybi00Processor::<GetResponseSecurityKey>b__1(System.Char)
extern "C"  bool DraftHybi00Processor_U3CGetResponseSecurityKeyU3Eb__1_m249413152 (Il2CppObject * __this /* static, unused */, uint16_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
