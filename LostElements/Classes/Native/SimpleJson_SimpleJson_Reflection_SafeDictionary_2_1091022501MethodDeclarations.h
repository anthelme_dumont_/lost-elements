﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.Reflection.SafeDictionary`2<System.Object,System.Object>
struct SafeDictionary_2_t1091022501;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t501095600;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Boolean SimpleJson.Reflection.SafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryGetValue_m2241969233_gshared (SafeDictionary_2_t1091022501 * __this, Il2CppObject * ___key, Il2CppObject ** ___value, const MethodInfo* method);
#define SafeDictionary_2_TryGetValue_m2241969233(__this, ___key, ___value, method) ((  bool (*) (SafeDictionary_2_t1091022501 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))SafeDictionary_2_TryGetValue_m2241969233_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.SafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* SafeDictionary_2_GetEnumerator_m3507230980_gshared (SafeDictionary_2_t1091022501 * __this, const MethodInfo* method);
#define SafeDictionary_2_GetEnumerator_m3507230980(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t1091022501 *, const MethodInfo*))SafeDictionary_2_GetEnumerator_m3507230980_gshared)(__this, method)
// System.Void SimpleJson.Reflection.SafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void SafeDictionary_2_Add_m3862242812_gshared (SafeDictionary_2_t1091022501 * __this, Il2CppObject * ___key, Il2CppObject * ___value, const MethodInfo* method);
#define SafeDictionary_2_Add_m3862242812(__this, ___key, ___value, method) ((  void (*) (SafeDictionary_2_t1091022501 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SafeDictionary_2_Add_m3862242812_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.SafeDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void SafeDictionary_2__ctor_m3700110041_gshared (SafeDictionary_2_t1091022501 * __this, const MethodInfo* method);
#define SafeDictionary_2__ctor_m3700110041(__this, method) ((  void (*) (SafeDictionary_2_t1091022501 *, const MethodInfo*))SafeDictionary_2__ctor_m3700110041_gshared)(__this, method)
