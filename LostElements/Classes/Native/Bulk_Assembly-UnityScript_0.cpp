﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ButtonController
struct ButtonController_t2447168654;
// IntroductionController
struct IntroductionController_t4287196022;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// IntroductionController/$Start$3
struct U24StartU243_t2379095981;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t2774239688;
// IntroductionController/$Start$3/$
struct U24_t36;
// MovementsController
struct MovementsController_t463364160;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t3296505762;
// System.Object
struct Il2CppObject;
// RaftController
struct RaftController_t3723046617;
// UnityEngine.Renderer
struct Renderer_t1092684080;
// SigninController
struct SigninController_t415518974;
// System.String
struct String_t;
// SplashScreen
struct SplashScreen_t2592768467;
// SplashScreen/$Start$7
struct U24StartU247_t2379095985;
// SplashScreen/$Start$7/$
struct U24_t37;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E86524790.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DUnityScript_ButtonController2447168654.h"
#include "AssemblyU2DUnityScript_ButtonController2447168654MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag1523288937MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DUnityScript_IntroductionController4287196022.h"
#include "AssemblyU2DUnityScript_IntroductionController4287196022MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "AssemblyU2DUnityScript_IntroductionController_U24S2379095981MethodDeclarations.h"
#include "AssemblyU2DUnityScript_IntroductionController_U24S2379095981.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen2359605680MethodDeclarations.h"
#include "AssemblyU2DUnityScript_IntroductionController_U24StartU24336MethodDeclarations.h"
#include "AssemblyU2DUnityScript_IntroductionController_U24StartU24336.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen1056199668MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3733964924MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices2273795202MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen1056199668.h"
#include "mscorlib_System_Int322847414787.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240.h"
#include "mscorlib_System_Single958209021.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198MethodDeclarations.h"
#include "AssemblyU2DUnityScript_MovementsController463364160.h"
#include "AssemblyU2DUnityScript_MovementsController463364160MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NavMeshAgent3296505762.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch1603883884MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera3533968274MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2601443956MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit46221527MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NavMeshAgent3296505762MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit46221527.h"
#include "UnityEngine_UnityEngine_Ray1522967639.h"
#include "UnityEngine_UnityEngine_Touch1603883884.h"
#include "UnityEngine_UnityEngine_TouchPhase1905076713.h"
#include "UnityEngine_UnityEngine_Camera3533968274.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DUnityScript_RaftController3723046617.h"
#include "AssemblyU2DUnityScript_RaftController3723046617MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_Renderer1092684080.h"
#include "UnityEngine_UnityEngine_Renderer1092684080MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"
#include "AssemblyU2DUnityScript_SigninController415518974.h"
#include "AssemblyU2DUnityScript_SigninController415518974MethodDeclarations.h"
#include "AssemblyU2DUnityScript_SplashScreen2592768467.h"
#include "AssemblyU2DUnityScript_SplashScreen2592768467MethodDeclarations.h"
#include "AssemblyU2DUnityScript_SplashScreen_U24StartU2472379095985MethodDeclarations.h"
#include "AssemblyU2DUnityScript_SplashScreen_U24StartU2472379095985.h"
#include "AssemblyU2DUnityScript_SplashScreen_U24StartU247_U2436MethodDeclarations.h"
#include "AssemblyU2DUnityScript_SplashScreen_U24StartU247_U2436.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.NavMeshAgent>()
#define Component_GetComponent_TisNavMeshAgent_t3296505762_m513972174(__this, method) ((  NavMeshAgent_t3296505762 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t1092684080_m1227825166(__this, method) ((  Renderer_t1092684080 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ButtonController::.ctor()
extern "C"  void ButtonController__ctor_m497520450 (ButtonController_t2447168654 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonController::newGame()
extern Il2CppCodeGenString* _stringLiteral3392499618;
extern const uint32_t ButtonController_newGame_m2357226994_MetadataUsageId;
extern "C"  void ButtonController_newGame_m2357226994 (ButtonController_t2447168654 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ButtonController_newGame_m2357226994_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral3392499618, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonController::loadGame()
extern "C"  void ButtonController_loadGame_m3378449690 (ButtonController_t2447168654 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ButtonController::settings()
extern "C"  void ButtonController_settings_m3951577221 (ButtonController_t2447168654 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ButtonController::learnMore()
extern "C"  void ButtonController_learnMore_m1930016377 (ButtonController_t2447168654 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ButtonController::credits()
extern "C"  void ButtonController_credits_m3988745946 (ButtonController_t2447168654 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ButtonController::Main()
extern "C"  void ButtonController_Main_m2563942715 (ButtonController_t2447168654 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void IntroductionController::.ctor()
extern Il2CppCodeGenString* _stringLiteral1333642419;
extern const uint32_t IntroductionController__ctor_m3934298010_MetadataUsageId;
extern "C"  void IntroductionController__ctor_m3934298010 (IntroductionController_t4287196022 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IntroductionController__ctor_m3934298010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_introductionText_4(_stringLiteral1333642419);
		return;
	}
}
// System.Void IntroductionController::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IntroductionController_t4287196022_il2cpp_TypeInfo_var;
extern const uint32_t IntroductionController__cctor_m1222057811_MetadataUsageId;
extern "C"  void IntroductionController__cctor_m1222057811 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IntroductionController__cctor_m1222057811_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((IntroductionController_t4287196022_StaticFields*)IntroductionController_t4287196022_il2cpp_TypeInfo_var->static_fields)->set_playerName_2(L_0);
		return;
	}
}
// System.Collections.IEnumerator IntroductionController::Start()
extern TypeInfo* U24StartU243_t2379095981_il2cpp_TypeInfo_var;
extern const uint32_t IntroductionController_Start_m4160700360_MetadataUsageId;
extern "C"  Il2CppObject * IntroductionController_Start_m4160700360 (IntroductionController_t4287196022 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IntroductionController_Start_m4160700360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24StartU243_t2379095981 * L_0 = (U24StartU243_t2379095981 *)il2cpp_codegen_object_new(U24StartU243_t2379095981_il2cpp_TypeInfo_var);
		U24StartU243__ctor_m3555563002(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24StartU243_GetEnumerator_m3172959872(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void IntroductionController::Main()
extern "C"  void IntroductionController_Main_m2397711843 (IntroductionController_t4287196022 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void IntroductionController/$Start$3::.ctor(IntroductionController)
extern const MethodInfo* GenericGenerator_1__ctor_m1086344373_MethodInfo_var;
extern const uint32_t U24StartU243__ctor_m3555563002_MetadataUsageId;
extern "C"  void U24StartU243__ctor_m3555563002 (U24StartU243_t2379095981 * __this, IntroductionController_t4287196022 * ___self_, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24StartU243__ctor_m3555563002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1086344373(__this, /*hidden argument*/GenericGenerator_1__ctor_m1086344373_MethodInfo_var);
		IntroductionController_t4287196022 * L_0 = ___self_;
		__this->set_U24self_U246_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> IntroductionController/$Start$3::GetEnumerator()
extern TypeInfo* U24_t36_il2cpp_TypeInfo_var;
extern const uint32_t U24StartU243_GetEnumerator_m3172959872_MetadataUsageId;
extern "C"  Il2CppObject* U24StartU243_GetEnumerator_m3172959872 (U24StartU243_t2379095981 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24StartU243_GetEnumerator_m3172959872_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntroductionController_t4287196022 * L_0 = __this->get_U24self_U246_0();
		U24_t36 * L_1 = (U24_t36 *)il2cpp_codegen_object_new(U24_t36_il2cpp_TypeInfo_var);
		U24__ctor_m3366870597(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void IntroductionController/$Start$3/$::.ctor(IntroductionController)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var;
extern const uint32_t U24__ctor_m3366870597_MetadataUsageId;
extern "C"  void U24__ctor_m3366870597 (U24_t36 * __this, IntroductionController_t4287196022 * ___self_, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m3366870597_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3634585745(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var);
		IntroductionController_t4287196022 * L_0 = ___self_;
		__this->set_U24self_U245_3(L_0);
		return;
	}
}
// System.Boolean IntroductionController/$Start$3/$::MoveNext()
extern TypeInfo* IntroductionController_t4287196022_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t1291133240_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeServices_t2273795202_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2948511130;
extern Il2CppCodeGenString* _stringLiteral2157899182;
extern Il2CppCodeGenString* _stringLiteral1458;
extern Il2CppCodeGenString* _stringLiteral35051977;
extern const uint32_t U24_MoveNext_m3900849807_MetadataUsageId;
extern "C"  bool U24_MoveNext_m3900849807 (U24_t36 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m3900849807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t1056199668 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_001b;
		}
		if (L_0 == 1)
		{
			goto IL_00b2;
		}
		if (L_0 == 2)
		{
			goto IL_0051;
		}
		if (L_0 == 3)
		{
			goto IL_00a0;
		}
	}

IL_001b:
	{
		String_t* L_1 = PlayerPrefs_GetString_m378864272(NULL /*static, unused*/, _stringLiteral2948511130, /*hidden argument*/NULL);
		__this->set_U24currentPlayerU244_2(L_1);
		String_t* L_2 = __this->get_U24currentPlayerU244_2();
		String_t* L_3 = PlayerPrefs_GetString_m378864272(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IntroductionController_t4287196022_il2cpp_TypeInfo_var);
		((IntroductionController_t4287196022_StaticFields*)IntroductionController_t4287196022_il2cpp_TypeInfo_var->static_fields)->set_playerName_2(L_3);
		WaitForSeconds_t1291133240 * L_4 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_4, (2.0f), /*hidden argument*/NULL);
		bool L_5 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 2, L_4, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B5_0 = ((int32_t)(L_5));
		goto IL_00b3;
	}

IL_0051:
	{
		IntroductionController_t4287196022 * L_6 = __this->get_U24self_U245_3();
		NullCheck(L_6);
		Text_t3286458198 * L_7 = L_6->get_inputText_3();
		IL2CPP_RUNTIME_CLASS_INIT(IntroductionController_t4287196022_il2cpp_TypeInfo_var);
		String_t* L_8 = ((IntroductionController_t4287196022_StaticFields*)IntroductionController_t4287196022_il2cpp_TypeInfo_var->static_fields)->get_playerName_2();
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t2273795202_il2cpp_TypeInfo_var);
		String_t* L_9 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, _stringLiteral2157899182, L_8, /*hidden argument*/NULL);
		String_t* L_10 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, L_9, _stringLiteral1458, /*hidden argument*/NULL);
		IntroductionController_t4287196022 * L_11 = __this->get_U24self_U245_3();
		NullCheck(L_11);
		String_t* L_12 = L_11->get_introductionText_4();
		String_t* L_13 = RuntimeServices_op_Addition_m3391097550(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_13);
		WaitForSeconds_t1291133240 * L_14 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_14, (3.0f), /*hidden argument*/NULL);
		bool L_15 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 3, L_14, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B5_0 = ((int32_t)(L_15));
		goto IL_00b3;
	}

IL_00a0:
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral35051977, /*hidden argument*/NULL);
		GenericGeneratorEnumerator_1_YieldDefault_m289716250(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var);
	}

IL_00b2:
	{
		G_B5_0 = 0;
	}

IL_00b3:
	{
		return (bool)G_B5_0;
	}
}
// System.Void MovementsController::.ctor()
extern "C"  void MovementsController__ctor_m1838486 (MovementsController_t463364160 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MovementsController::Start()
extern const MethodInfo* Component_GetComponent_TisNavMeshAgent_t3296505762_m513972174_MethodInfo_var;
extern const uint32_t MovementsController_Start_m3243943574_MetadataUsageId;
extern "C"  void MovementsController_Start_m3243943574 (MovementsController_t463364160 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MovementsController_Start_m3243943574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NavMeshAgent_t3296505762 * L_0 = Component_GetComponent_TisNavMeshAgent_t3296505762_m513972174(__this, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_t3296505762_m513972174_MethodInfo_var);
		__this->set_agent_2(L_0);
		return;
	}
}
// System.Void MovementsController::FixedUpdate()
extern TypeInfo* RaycastHit_t46221527_il2cpp_TypeInfo_var;
extern TypeInfo* Ray_t1522967639_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t MovementsController_FixedUpdate_m17752721_MetadataUsageId;
extern "C"  void MovementsController_FixedUpdate_m17752721 (MovementsController_t463364160 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MovementsController_FixedUpdate_m17752721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t46221527  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Ray_t1522967639  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t1603883884  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Touch_t1603883884  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Initobj (RaycastHit_t46221527_il2cpp_TypeInfo_var, (&V_0));
		Initobj (Ray_t1522967639_il2cpp_TypeInfo_var, (&V_1));
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		int32_t L_1 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0078;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_2 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = Touch_get_phase_m3314549414((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)0))))
		{
			goto IL_0078;
		}
	}

IL_003a:
	{
		Camera_t3533968274 * L_4 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_5 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_3 = L_5;
		Vector2_t3525329788  L_6 = Touch_get_position_m1943849441((&V_3), /*hidden argument*/NULL);
		Vector3_t3525329789  L_7 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Ray_t1522967639  L_8 = Camera_ScreenPointToRay_m1733083890(L_4, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		Ray_t1522967639  L_9 = V_1;
		bool L_10 = Physics_Raycast_m1343340263(NULL /*static, unused*/, L_9, (&V_0), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0078;
		}
	}
	{
		NavMeshAgent_t3296505762 * L_11 = __this->get_agent_2();
		Vector3_t3525329789  L_12 = RaycastHit_get_point_m4165497838((&V_0), /*hidden argument*/NULL);
		NullCheck(L_11);
		NavMeshAgent_SetDestination_m1934812347(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0078:
	{
		return;
	}
}
// System.Void MovementsController::Main()
extern "C"  void MovementsController_Main_m3379236967 (MovementsController_t463364160 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void RaftController::.ctor()
extern "C"  void RaftController__ctor_m1820334231 (RaftController_t3723046617 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RaftController::Start()
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t1092684080_m1227825166_MethodInfo_var;
extern const uint32_t RaftController_Start_m767472023_MetadataUsageId;
extern "C"  void RaftController_Start_m767472023 (RaftController_t3723046617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RaftController_Start_m767472023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		__this->set_defaultYPosition_3(L_2);
		GameObject_t4012695102 * L_3 = __this->get_water_2();
		NullCheck(L_3);
		Renderer_t1092684080 * L_4 = GameObject_GetComponent_TisRenderer_t1092684080_m1227825166(L_3, /*hidden argument*/GameObject_GetComponent_TisRenderer_t1092684080_m1227825166_MethodInfo_var);
		__this->set_waterRenderer_4(L_4);
		return;
	}
}
// System.Void RaftController::Update()
extern "C"  void RaftController_Update_m2322648406 (RaftController_t3723046617 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Bounds_t3518514978  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3525329789  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector3_t3525329789  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		GameObject_t4012695102 * L_0 = __this->get_water_2();
		NullCheck(L_0);
		Transform_t284553113 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = (&V_2)->get_y_2();
		Renderer_t1092684080 * L_4 = __this->get_waterRenderer_4();
		NullCheck(L_4);
		Bounds_t3518514978  L_5 = Renderer_get_bounds_m1533373851(L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		Vector3_t3525329789  L_6 = Bounds_get_size_m3666348432((&V_3), /*hidden argument*/NULL);
		V_4 = L_6;
		float L_7 = (&V_4)->get_y_2();
		float L_8 = ((float)((float)L_3+(float)L_7));
		V_0 = L_8;
		Transform_t284553113 * L_9 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3525329789  L_10 = Transform_get_position_m2211398607(L_9, /*hidden argument*/NULL);
		Vector3_t3525329789  L_11 = L_10;
		V_1 = L_11;
		float L_12 = V_0;
		float L_13 = L_12;
		V_5 = L_13;
		(&V_1)->set_y_2(L_13);
		float L_14 = V_5;
		Transform_t284553113 * L_15 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_16 = V_1;
		Vector3_t3525329789  L_17 = L_16;
		V_6 = L_17;
		NullCheck(L_15);
		Transform_set_position_m3111394108(L_15, L_17, /*hidden argument*/NULL);
		Vector3_t3525329789  L_18 = V_6;
		return;
	}
}
// System.Void RaftController::Main()
extern "C"  void RaftController_Main_m1221140806 (RaftController_t3723046617 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SigninController::.ctor()
extern "C"  void SigninController__ctor_m3717009618 (SigninController_t415518974 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SigninController::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* SigninController_t415518974_il2cpp_TypeInfo_var;
extern const uint32_t SigninController__cctor_m3076052251_MetadataUsageId;
extern "C"  void SigninController__cctor_m3076052251 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SigninController__cctor_m3076052251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((SigninController_t415518974_StaticFields*)SigninController_t415518974_il2cpp_TypeInfo_var->static_fields)->set_playerName_2(L_0);
		return;
	}
}
// System.Void SigninController::start()
extern TypeInfo* SigninController_t415518974_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1171085648;
extern Il2CppCodeGenString* _stringLiteral3196363796;
extern Il2CppCodeGenString* _stringLiteral2948511130;
extern Il2CppCodeGenString* _stringLiteral1171085649;
extern Il2CppCodeGenString* _stringLiteral3224992947;
extern Il2CppCodeGenString* _stringLiteral1171085650;
extern Il2CppCodeGenString* _stringLiteral3253622098;
extern Il2CppCodeGenString* _stringLiteral1171085651;
extern Il2CppCodeGenString* _stringLiteral3282251249;
extern Il2CppCodeGenString* _stringLiteral1539594266;
extern const uint32_t SigninController_start_m999494130_MetadataUsageId;
extern "C"  void SigninController_start_m999494130 (SigninController_t415518974 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SigninController_start_m999494130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayerPrefs_HasKey_m2032560073(NULL /*static, unused*/, _stringLiteral1171085648, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SigninController_t415518974_il2cpp_TypeInfo_var);
		String_t* L_1 = ((SigninController_t415518974_StaticFields*)SigninController_t415518974_il2cpp_TypeInfo_var->static_fields)->get_playerName_2();
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, _stringLiteral1171085648, L_1, /*hidden argument*/NULL);
		PlayerPrefs_SetFloat_m1687591347(NULL /*static, unused*/, _stringLiteral3196363796, (((float)((float)0))), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, _stringLiteral2948511130, _stringLiteral1171085648, /*hidden argument*/NULL);
		goto IL_00f3;
	}

IL_003e:
	{
		bool L_2 = PlayerPrefs_HasKey_m2032560073(NULL /*static, unused*/, _stringLiteral1171085649, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_007c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SigninController_t415518974_il2cpp_TypeInfo_var);
		String_t* L_3 = ((SigninController_t415518974_StaticFields*)SigninController_t415518974_il2cpp_TypeInfo_var->static_fields)->get_playerName_2();
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, _stringLiteral1171085649, L_3, /*hidden argument*/NULL);
		PlayerPrefs_SetFloat_m1687591347(NULL /*static, unused*/, _stringLiteral3224992947, (((float)((float)0))), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, _stringLiteral2948511130, _stringLiteral1171085649, /*hidden argument*/NULL);
		goto IL_00f3;
	}

IL_007c:
	{
		bool L_4 = PlayerPrefs_HasKey_m2032560073(NULL /*static, unused*/, _stringLiteral1171085650, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_00ba;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SigninController_t415518974_il2cpp_TypeInfo_var);
		String_t* L_5 = ((SigninController_t415518974_StaticFields*)SigninController_t415518974_il2cpp_TypeInfo_var->static_fields)->get_playerName_2();
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, _stringLiteral1171085650, L_5, /*hidden argument*/NULL);
		PlayerPrefs_SetFloat_m1687591347(NULL /*static, unused*/, _stringLiteral3253622098, (((float)((float)0))), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, _stringLiteral2948511130, _stringLiteral1171085650, /*hidden argument*/NULL);
		goto IL_00f3;
	}

IL_00ba:
	{
		bool L_6 = PlayerPrefs_HasKey_m2032560073(NULL /*static, unused*/, _stringLiteral1171085651, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_00f3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SigninController_t415518974_il2cpp_TypeInfo_var);
		String_t* L_7 = ((SigninController_t415518974_StaticFields*)SigninController_t415518974_il2cpp_TypeInfo_var->static_fields)->get_playerName_2();
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, _stringLiteral1171085651, L_7, /*hidden argument*/NULL);
		PlayerPrefs_SetFloat_m1687591347(NULL /*static, unused*/, _stringLiteral3282251249, (((float)((float)0))), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, _stringLiteral2948511130, _stringLiteral1171085651, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral1539594266, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SigninController::setPlayerName(System.String)
extern TypeInfo* SigninController_t415518974_il2cpp_TypeInfo_var;
extern const uint32_t SigninController_setPlayerName_m1770407204_MetadataUsageId;
extern "C"  void SigninController_setPlayerName_m1770407204 (SigninController_t415518974 * __this, String_t* ____playerName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SigninController_setPlayerName_m1770407204_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____playerName;
		IL2CPP_RUNTIME_CLASS_INIT(SigninController_t415518974_il2cpp_TypeInfo_var);
		((SigninController_t415518974_StaticFields*)SigninController_t415518974_il2cpp_TypeInfo_var->static_fields)->set_playerName_2(L_0);
		return;
	}
}
// System.Void SigninController::Main()
extern "C"  void SigninController_Main_m1282323883 (SigninController_t415518974 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SplashScreen::.ctor()
extern "C"  void SplashScreen__ctor_m1751502365 (SplashScreen_t2592768467 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator SplashScreen::Start()
extern TypeInfo* U24StartU247_t2379095985_il2cpp_TypeInfo_var;
extern const uint32_t SplashScreen_Start_m2977755083_MetadataUsageId;
extern "C"  Il2CppObject * SplashScreen_Start_m2977755083 (SplashScreen_t2592768467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SplashScreen_Start_m2977755083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24StartU247_t2379095985 * L_0 = (U24StartU247_t2379095985 *)il2cpp_codegen_object_new(U24StartU247_t2379095985_il2cpp_TypeInfo_var);
		U24StartU247__ctor_m2695850825(L_0, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24StartU247_GetEnumerator_m154303705(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void SplashScreen::Main()
extern "C"  void SplashScreen_Main_m2050204416 (SplashScreen_t2592768467 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SplashScreen/$Start$7::.ctor()
extern const MethodInfo* GenericGenerator_1__ctor_m1086344373_MethodInfo_var;
extern const uint32_t U24StartU247__ctor_m2695850825_MetadataUsageId;
extern "C"  void U24StartU247__ctor_m2695850825 (U24StartU247_t2379095985 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24StartU247__ctor_m2695850825_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1086344373(__this, /*hidden argument*/GenericGenerator_1__ctor_m1086344373_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> SplashScreen/$Start$7::GetEnumerator()
extern TypeInfo* U24_t37_il2cpp_TypeInfo_var;
extern const uint32_t U24StartU247_GetEnumerator_m154303705_MetadataUsageId;
extern "C"  Il2CppObject* U24StartU247_GetEnumerator_m154303705 (U24StartU247_t2379095985 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24StartU247_GetEnumerator_m154303705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24_t37 * L_0 = (U24_t37 *)il2cpp_codegen_object_new(U24_t37_il2cpp_TypeInfo_var);
		U24__ctor_m2471660436(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void SplashScreen/$Start$7/$::.ctor()
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var;
extern const uint32_t U24__ctor_m2471660436_MetadataUsageId;
extern "C"  void U24__ctor_m2471660436 (U24_t37 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m2471660436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3634585745(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var);
		return;
	}
}
// System.Boolean SplashScreen/$Start$7/$::MoveNext()
extern TypeInfo* WaitForSeconds_t1291133240_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4286319288;
extern const uint32_t U24_MoveNext_m2900844374_MetadataUsageId;
extern "C"  bool U24_MoveNext_m2900844374 (U24_t37 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m2900844374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t1056199668 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_0044;
		}
		if (L_0 == 2)
		{
			goto IL_0032;
		}
	}

IL_0017:
	{
		PlayerPrefs_DeleteAll_m2619453438(NULL /*static, unused*/, /*hidden argument*/NULL);
		WaitForSeconds_t1291133240 * L_1 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_1, (1.0f), /*hidden argument*/NULL);
		bool L_2 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 2, L_1, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B4_0 = ((int32_t)(L_2));
		goto IL_0045;
	}

IL_0032:
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral4286319288, /*hidden argument*/NULL);
		GenericGeneratorEnumerator_1_YieldDefault_m289716250(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var);
	}

IL_0044:
	{
		G_B4_0 = 0;
	}

IL_0045:
	{
		return (bool)G_B4_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
