﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SimpleJson.IJsonSerializerStrategy
struct IJsonSerializerStrategy_t4110884692;
// SimpleJson.DataContractJsonSerializerStrategy
struct DataContractJsonSerializerStrategy_t3752539591;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.SimpleJson
struct  SimpleJson_t2437938008  : public Il2CppObject
{
public:

public:
};

struct SimpleJson_t2437938008_StaticFields
{
public:
	// SimpleJson.IJsonSerializerStrategy SimpleJson.SimpleJson::currentJsonSerializerStrategy
	Il2CppObject * ___currentJsonSerializerStrategy_0;
	// SimpleJson.DataContractJsonSerializerStrategy SimpleJson.SimpleJson::dataContractJsonSerializerStrategy
	DataContractJsonSerializerStrategy_t3752539591 * ___dataContractJsonSerializerStrategy_1;

public:
	inline static int32_t get_offset_of_currentJsonSerializerStrategy_0() { return static_cast<int32_t>(offsetof(SimpleJson_t2437938008_StaticFields, ___currentJsonSerializerStrategy_0)); }
	inline Il2CppObject * get_currentJsonSerializerStrategy_0() const { return ___currentJsonSerializerStrategy_0; }
	inline Il2CppObject ** get_address_of_currentJsonSerializerStrategy_0() { return &___currentJsonSerializerStrategy_0; }
	inline void set_currentJsonSerializerStrategy_0(Il2CppObject * value)
	{
		___currentJsonSerializerStrategy_0 = value;
		Il2CppCodeGenWriteBarrier(&___currentJsonSerializerStrategy_0, value);
	}

	inline static int32_t get_offset_of_dataContractJsonSerializerStrategy_1() { return static_cast<int32_t>(offsetof(SimpleJson_t2437938008_StaticFields, ___dataContractJsonSerializerStrategy_1)); }
	inline DataContractJsonSerializerStrategy_t3752539591 * get_dataContractJsonSerializerStrategy_1() const { return ___dataContractJsonSerializerStrategy_1; }
	inline DataContractJsonSerializerStrategy_t3752539591 ** get_address_of_dataContractJsonSerializerStrategy_1() { return &___dataContractJsonSerializerStrategy_1; }
	inline void set_dataContractJsonSerializerStrategy_1(DataContractJsonSerializerStrategy_t3752539591 * value)
	{
		___dataContractJsonSerializerStrategy_1 = value;
		Il2CppCodeGenWriteBarrier(&___dataContractJsonSerializerStrategy_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
