﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>
struct ValueCollection_t400242028;
// System.Collections.Generic.Dictionary`2<System.Int32,System.TimeZoneInfo/TimeType>
struct Dictionary_2_t2773072230;
// System.Collections.Generic.IEnumerator`1<System.TimeZoneInfo/TimeType>
struct IEnumerator_1_t3765367895;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.TimeZoneInfo/TimeType[]
struct TimeTypeU5BU5D_t202998078;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_TimeZoneInfo_TimeType2282261447.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2540100171.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3280423927_gshared (ValueCollection_t400242028 * __this, Dictionary_2_t2773072230 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m3280423927(__this, ___dictionary, method) ((  void (*) (ValueCollection_t400242028 *, Dictionary_2_t2773072230 *, const MethodInfo*))ValueCollection__ctor_m3280423927_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2884769115_gshared (ValueCollection_t400242028 * __this, TimeType_t2282261447  ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2884769115(__this, ___item, method) ((  void (*) (ValueCollection_t400242028 *, TimeType_t2282261447 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2884769115_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1902569380_gshared (ValueCollection_t400242028 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1902569380(__this, method) ((  void (*) (ValueCollection_t400242028 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1902569380_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1909510955_gshared (ValueCollection_t400242028 * __this, TimeType_t2282261447  ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1909510955(__this, ___item, method) ((  bool (*) (ValueCollection_t400242028 *, TimeType_t2282261447 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1909510955_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m579517072_gshared (ValueCollection_t400242028 * __this, TimeType_t2282261447  ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m579517072(__this, ___item, method) ((  bool (*) (ValueCollection_t400242028 *, TimeType_t2282261447 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m579517072_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1445680242_gshared (ValueCollection_t400242028 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1445680242(__this, method) ((  Il2CppObject* (*) (ValueCollection_t400242028 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1445680242_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1978354600_gshared (ValueCollection_t400242028 * __this, Il2CppArray * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1978354600(__this, ___array, ___index, method) ((  void (*) (ValueCollection_t400242028 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1978354600_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m911747363_gshared (ValueCollection_t400242028 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m911747363(__this, method) ((  Il2CppObject * (*) (ValueCollection_t400242028 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m911747363_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m184686814_gshared (ValueCollection_t400242028 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m184686814(__this, method) ((  bool (*) (ValueCollection_t400242028 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m184686814_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2385525694_gshared (ValueCollection_t400242028 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2385525694(__this, method) ((  bool (*) (ValueCollection_t400242028 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2385525694_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2493223658_gshared (ValueCollection_t400242028 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2493223658(__this, method) ((  Il2CppObject * (*) (ValueCollection_t400242028 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2493223658_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3571550846_gshared (ValueCollection_t400242028 * __this, TimeTypeU5BU5D_t202998078* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m3571550846(__this, ___array, ___index, method) ((  void (*) (ValueCollection_t400242028 *, TimeTypeU5BU5D_t202998078*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3571550846_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::GetEnumerator()
extern "C"  Enumerator_t2540100173  ValueCollection_GetEnumerator_m193870113_gshared (ValueCollection_t400242028 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m193870113(__this, method) ((  Enumerator_t2540100173  (*) (ValueCollection_t400242028 *, const MethodInfo*))ValueCollection_GetEnumerator_m193870113_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.TimeZoneInfo/TimeType>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m2297613188_gshared (ValueCollection_t400242028 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m2297613188(__this, method) ((  int32_t (*) (ValueCollection_t400242028 *, const MethodInfo*))ValueCollection_get_Count_m2297613188_gshared)(__this, method)
