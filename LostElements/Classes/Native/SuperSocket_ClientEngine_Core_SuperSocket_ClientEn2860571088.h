﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.Sockets.SocketAsyncEventArgs
struct SocketAsyncEventArgs_t970431102;

#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1301539049.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.AsyncTcpSession
struct  AsyncTcpSession_t2860571088  : public TcpClientSession_t1301539049
{
public:
	// System.Net.Sockets.SocketAsyncEventArgs SuperSocket.ClientEngine.AsyncTcpSession::m_SocketEventArgs
	SocketAsyncEventArgs_t970431102 * ___m_SocketEventArgs_15;
	// System.Net.Sockets.SocketAsyncEventArgs SuperSocket.ClientEngine.AsyncTcpSession::m_SocketEventArgsSend
	SocketAsyncEventArgs_t970431102 * ___m_SocketEventArgsSend_16;

public:
	inline static int32_t get_offset_of_m_SocketEventArgs_15() { return static_cast<int32_t>(offsetof(AsyncTcpSession_t2860571088, ___m_SocketEventArgs_15)); }
	inline SocketAsyncEventArgs_t970431102 * get_m_SocketEventArgs_15() const { return ___m_SocketEventArgs_15; }
	inline SocketAsyncEventArgs_t970431102 ** get_address_of_m_SocketEventArgs_15() { return &___m_SocketEventArgs_15; }
	inline void set_m_SocketEventArgs_15(SocketAsyncEventArgs_t970431102 * value)
	{
		___m_SocketEventArgs_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_SocketEventArgs_15, value);
	}

	inline static int32_t get_offset_of_m_SocketEventArgsSend_16() { return static_cast<int32_t>(offsetof(AsyncTcpSession_t2860571088, ___m_SocketEventArgsSend_16)); }
	inline SocketAsyncEventArgs_t970431102 * get_m_SocketEventArgsSend_16() const { return ___m_SocketEventArgsSend_16; }
	inline SocketAsyncEventArgs_t970431102 ** get_address_of_m_SocketEventArgsSend_16() { return &___m_SocketEventArgsSend_16; }
	inline void set_m_SocketEventArgsSend_16(SocketAsyncEventArgs_t970431102 * value)
	{
		___m_SocketEventArgsSend_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_SocketEventArgsSend_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
