﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Command.BadRequest
struct BadRequest_t1215901385;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketCommandInfo3536916738.h"

// System.Void WebSocket4Net.Command.BadRequest::ExecuteCommand(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo)
extern "C"  void BadRequest_ExecuteCommand_m1364508181 (BadRequest_t1215901385 * __this, WebSocket_t713846903 * ___session, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.Command.BadRequest::get_Name()
extern "C"  String_t* BadRequest_get_Name_m2834877993 (BadRequest_t1215901385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Command.BadRequest::.ctor()
extern "C"  void BadRequest__ctor_m2931563346 (BadRequest_t1215901385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Command.BadRequest::.cctor()
extern "C"  void BadRequest__cctor_m202054299 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
