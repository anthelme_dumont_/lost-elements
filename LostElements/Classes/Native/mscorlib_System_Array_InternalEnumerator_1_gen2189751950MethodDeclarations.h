﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2189751950.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Core_System_TimeZoneInfo_TimeType2282261447.h"

// System.Void System.Array/InternalEnumerator`1<System.TimeZoneInfo/TimeType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3726960129_gshared (InternalEnumerator_1_t2189751950 * __this, Il2CppArray * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3726960129(__this, ___array, method) ((  void (*) (InternalEnumerator_1_t2189751950 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3726960129_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.TimeZoneInfo/TimeType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m31811071_gshared (InternalEnumerator_1_t2189751950 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m31811071(__this, method) ((  void (*) (InternalEnumerator_1_t2189751950 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m31811071_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.TimeZoneInfo/TimeType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4062273131_gshared (InternalEnumerator_1_t2189751950 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4062273131(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2189751950 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4062273131_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.TimeZoneInfo/TimeType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2833829144_gshared (InternalEnumerator_1_t2189751950 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2833829144(__this, method) ((  void (*) (InternalEnumerator_1_t2189751950 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2833829144_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.TimeZoneInfo/TimeType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m41192299_gshared (InternalEnumerator_1_t2189751950 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m41192299(__this, method) ((  bool (*) (InternalEnumerator_1_t2189751950 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m41192299_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.TimeZoneInfo/TimeType>::get_Current()
extern "C"  TimeType_t2282261447  InternalEnumerator_1_get_Current_m1838727368_gshared (InternalEnumerator_1_t2189751950 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1838727368(__this, method) ((  TimeType_t2282261447  (*) (InternalEnumerator_1_t2189751950 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1838727368_gshared)(__this, method)
