﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.ProtocolProcessorFactory
struct ProtocolProcessorFactory_t3499308290;
// WebSocket4Net.Protocol.IProtocolProcessor[]
struct IProtocolProcessorU5BU5D_t2571124850;
// WebSocket4Net.Protocol.IProtocolProcessor
struct IProtocolProcessor_t4208156067;
// System.Int32[]
struct Int32U5BU5D_t1809983122;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketVersion3981039553.h"

// System.Void WebSocket4Net.Protocol.ProtocolProcessorFactory::.ctor(WebSocket4Net.Protocol.IProtocolProcessor[])
extern "C"  void ProtocolProcessorFactory__ctor_m557102662 (ProtocolProcessorFactory_t3499308290 * __this, IProtocolProcessorU5BU5D_t2571124850* ___processors, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.Protocol.IProtocolProcessor WebSocket4Net.Protocol.ProtocolProcessorFactory::GetProcessorByVersion(WebSocket4Net.WebSocketVersion)
extern "C"  Il2CppObject * ProtocolProcessorFactory_GetProcessorByVersion_m1724826559 (ProtocolProcessorFactory_t3499308290 * __this, int32_t ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.Protocol.IProtocolProcessor WebSocket4Net.Protocol.ProtocolProcessorFactory::GetPreferedProcessorFromAvialable(System.Int32[])
extern "C"  Il2CppObject * ProtocolProcessorFactory_GetPreferedProcessorFromAvialable_m2607568602 (ProtocolProcessorFactory_t3499308290 * __this, Int32U5BU5D_t1809983122* ___versions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocket4Net.Protocol.ProtocolProcessorFactory::<.ctor>b__0(WebSocket4Net.Protocol.IProtocolProcessor)
extern "C"  int32_t ProtocolProcessorFactory_U3C_ctorU3Eb__0_m1829162418 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___p, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocket4Net.Protocol.ProtocolProcessorFactory::<GetPreferedProcessorFromAvialable>b__5(System.Int32)
extern "C"  int32_t ProtocolProcessorFactory_U3CGetPreferedProcessorFromAvialableU3Eb__5_m2783378925 (Il2CppObject * __this /* static, unused */, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
