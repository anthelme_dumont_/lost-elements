﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>
struct ArraySegmentList_1_t2380898469;
// System.Collections.Generic.IList`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>
struct IList_1_t3895840460;
// System.Object
struct Il2CppObject;
// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>
struct ArraySegmentEx_1_t1729348146;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Collections.Generic.IList`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<T>> SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::get_Segments()
extern "C"  Il2CppObject* ArraySegmentList_1_get_Segments_m1707833398_gshared (ArraySegmentList_1_t2380898469 * __this, const MethodInfo* method);
#define ArraySegmentList_1_get_Segments_m1707833398(__this, method) ((  Il2CppObject* (*) (ArraySegmentList_1_t2380898469 *, const MethodInfo*))ArraySegmentList_1_get_Segments_m1707833398_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::.ctor()
extern "C"  void ArraySegmentList_1__ctor_m912386146_gshared (ArraySegmentList_1_t2380898469 * __this, const MethodInfo* method);
#define ArraySegmentList_1__ctor_m912386146(__this, method) ((  void (*) (ArraySegmentList_1_t2380898469 *, const MethodInfo*))ArraySegmentList_1__ctor_m912386146_gshared)(__this, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArraySegmentList_1_IndexOf_m3824786417_gshared (ArraySegmentList_1_t2380898469 * __this, Il2CppObject * ___item, const MethodInfo* method);
#define ArraySegmentList_1_IndexOf_m3824786417(__this, ___item, method) ((  int32_t (*) (ArraySegmentList_1_t2380898469 *, Il2CppObject *, const MethodInfo*))ArraySegmentList_1_IndexOf_m3824786417_gshared)(__this, ___item, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArraySegmentList_1_Insert_m2411816004_gshared (ArraySegmentList_1_t2380898469 * __this, int32_t ___index, Il2CppObject * ___item, const MethodInfo* method);
#define ArraySegmentList_1_Insert_m2411816004(__this, ___index, ___item, method) ((  void (*) (ArraySegmentList_1_t2380898469 *, int32_t, Il2CppObject *, const MethodInfo*))ArraySegmentList_1_Insert_m2411816004_gshared)(__this, ___index, ___item, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArraySegmentList_1_RemoveAt_m285668874_gshared (ArraySegmentList_1_t2380898469 * __this, int32_t ___index, const MethodInfo* method);
#define ArraySegmentList_1_RemoveAt_m285668874(__this, ___index, method) ((  void (*) (ArraySegmentList_1_t2380898469 *, int32_t, const MethodInfo*))ArraySegmentList_1_RemoveAt_m285668874_gshared)(__this, ___index, method)
// T SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ArraySegmentList_1_get_Item_m3837915824_gshared (ArraySegmentList_1_t2380898469 * __this, int32_t ___index, const MethodInfo* method);
#define ArraySegmentList_1_get_Item_m3837915824(__this, ___index, method) ((  Il2CppObject * (*) (ArraySegmentList_1_t2380898469 *, int32_t, const MethodInfo*))ArraySegmentList_1_get_Item_m3837915824_gshared)(__this, ___index, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArraySegmentList_1_set_Item_m282436635_gshared (ArraySegmentList_1_t2380898469 * __this, int32_t ___index, Il2CppObject * ___value, const MethodInfo* method);
#define ArraySegmentList_1_set_Item_m282436635(__this, ___index, ___value, method) ((  void (*) (ArraySegmentList_1_t2380898469 *, int32_t, Il2CppObject *, const MethodInfo*))ArraySegmentList_1_set_Item_m282436635_gshared)(__this, ___index, ___value, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::GetElementInternalIndex(System.Int32,SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<T>&)
extern "C"  int32_t ArraySegmentList_1_GetElementInternalIndex_m3919961391_gshared (ArraySegmentList_1_t2380898469 * __this, int32_t ___index, ArraySegmentEx_1_t1729348146 ** ___segment, const MethodInfo* method);
#define ArraySegmentList_1_GetElementInternalIndex_m3919961391(__this, ___index, ___segment, method) ((  int32_t (*) (ArraySegmentList_1_t2380898469 *, int32_t, ArraySegmentEx_1_t1729348146 **, const MethodInfo*))ArraySegmentList_1_GetElementInternalIndex_m3919961391_gshared)(__this, ___index, ___segment, method)
// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<T> SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::QuickSearchSegment(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C"  ArraySegmentEx_1_t1729348146 * ArraySegmentList_1_QuickSearchSegment_m1369326214_gshared (ArraySegmentList_1_t2380898469 * __this, int32_t ___from, int32_t ___to, int32_t ___index, int32_t* ___segmentIndex, const MethodInfo* method);
#define ArraySegmentList_1_QuickSearchSegment_m1369326214(__this, ___from, ___to, ___index, ___segmentIndex, method) ((  ArraySegmentEx_1_t1729348146 * (*) (ArraySegmentList_1_t2380898469 *, int32_t, int32_t, int32_t, int32_t*, const MethodInfo*))ArraySegmentList_1_QuickSearchSegment_m1369326214_gshared)(__this, ___from, ___to, ___index, ___segmentIndex, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::Add(T)
extern "C"  void ArraySegmentList_1_Add_m1820689373_gshared (ArraySegmentList_1_t2380898469 * __this, Il2CppObject * ___item, const MethodInfo* method);
#define ArraySegmentList_1_Add_m1820689373(__this, ___item, method) ((  void (*) (ArraySegmentList_1_t2380898469 *, Il2CppObject *, const MethodInfo*))ArraySegmentList_1_Add_m1820689373_gshared)(__this, ___item, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::Clear()
extern "C"  void ArraySegmentList_1_Clear_m2613486733_gshared (ArraySegmentList_1_t2380898469 * __this, const MethodInfo* method);
#define ArraySegmentList_1_Clear_m2613486733(__this, method) ((  void (*) (ArraySegmentList_1_t2380898469 *, const MethodInfo*))ArraySegmentList_1_Clear_m2613486733_gshared)(__this, method)
// System.Boolean SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::Contains(T)
extern "C"  bool ArraySegmentList_1_Contains_m486350119_gshared (ArraySegmentList_1_t2380898469 * __this, Il2CppObject * ___item, const MethodInfo* method);
#define ArraySegmentList_1_Contains_m486350119(__this, ___item, method) ((  bool (*) (ArraySegmentList_1_t2380898469 *, Il2CppObject *, const MethodInfo*))ArraySegmentList_1_Contains_m486350119_gshared)(__this, ___item, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArraySegmentList_1_CopyTo_m3196522125_gshared (ArraySegmentList_1_t2380898469 * __this, ObjectU5BU5D_t11523773* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ArraySegmentList_1_CopyTo_m3196522125(__this, ___array, ___arrayIndex, method) ((  void (*) (ArraySegmentList_1_t2380898469 *, ObjectU5BU5D_t11523773*, int32_t, const MethodInfo*))ArraySegmentList_1_CopyTo_m3196522125_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::get_Count()
extern "C"  int32_t ArraySegmentList_1_get_Count_m2421569450_gshared (ArraySegmentList_1_t2380898469 * __this, const MethodInfo* method);
#define ArraySegmentList_1_get_Count_m2421569450(__this, method) ((  int32_t (*) (ArraySegmentList_1_t2380898469 *, const MethodInfo*))ArraySegmentList_1_get_Count_m2421569450_gshared)(__this, method)
// System.Boolean SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArraySegmentList_1_get_IsReadOnly_m599728237_gshared (ArraySegmentList_1_t2380898469 * __this, const MethodInfo* method);
#define ArraySegmentList_1_get_IsReadOnly_m599728237(__this, method) ((  bool (*) (ArraySegmentList_1_t2380898469 *, const MethodInfo*))ArraySegmentList_1_get_IsReadOnly_m599728237_gshared)(__this, method)
// System.Boolean SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::Remove(T)
extern "C"  bool ArraySegmentList_1_Remove_m4250204898_gshared (ArraySegmentList_1_t2380898469 * __this, Il2CppObject * ___item, const MethodInfo* method);
#define ArraySegmentList_1_Remove_m4250204898(__this, ___item, method) ((  bool (*) (ArraySegmentList_1_t2380898469 *, Il2CppObject *, const MethodInfo*))ArraySegmentList_1_Remove_m4250204898_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArraySegmentList_1_GetEnumerator_m1761951958_gshared (ArraySegmentList_1_t2380898469 * __this, const MethodInfo* method);
#define ArraySegmentList_1_GetEnumerator_m1761951958(__this, method) ((  Il2CppObject* (*) (ArraySegmentList_1_t2380898469 *, const MethodInfo*))ArraySegmentList_1_GetEnumerator_m1761951958_gshared)(__this, method)
// System.Collections.IEnumerator SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArraySegmentList_1_System_Collections_IEnumerable_GetEnumerator_m673655689_gshared (ArraySegmentList_1_t2380898469 * __this, const MethodInfo* method);
#define ArraySegmentList_1_System_Collections_IEnumerable_GetEnumerator_m673655689(__this, method) ((  Il2CppObject * (*) (ArraySegmentList_1_t2380898469 *, const MethodInfo*))ArraySegmentList_1_System_Collections_IEnumerable_GetEnumerator_m673655689_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::RemoveSegmentAt(System.Int32)
extern "C"  void ArraySegmentList_1_RemoveSegmentAt_m136802099_gshared (ArraySegmentList_1_t2380898469 * __this, int32_t ___index, const MethodInfo* method);
#define ArraySegmentList_1_RemoveSegmentAt_m136802099(__this, ___index, method) ((  void (*) (ArraySegmentList_1_t2380898469 *, int32_t, const MethodInfo*))ArraySegmentList_1_RemoveSegmentAt_m136802099_gshared)(__this, ___index, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::AddSegment(T[],System.Int32,System.Int32,System.Boolean)
extern "C"  void ArraySegmentList_1_AddSegment_m3559280981_gshared (ArraySegmentList_1_t2380898469 * __this, ObjectU5BU5D_t11523773* ___array, int32_t ___offset, int32_t ___length, bool ___toBeCopied, const MethodInfo* method);
#define ArraySegmentList_1_AddSegment_m3559280981(__this, ___array, ___offset, ___length, ___toBeCopied, method) ((  void (*) (ArraySegmentList_1_t2380898469 *, ObjectU5BU5D_t11523773*, int32_t, int32_t, bool, const MethodInfo*))ArraySegmentList_1_AddSegment_m3559280981_gshared)(__this, ___array, ___offset, ___length, ___toBeCopied, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::get_SegmentCount()
extern "C"  int32_t ArraySegmentList_1_get_SegmentCount_m4041807107_gshared (ArraySegmentList_1_t2380898469 * __this, const MethodInfo* method);
#define ArraySegmentList_1_get_SegmentCount_m4041807107(__this, method) ((  int32_t (*) (ArraySegmentList_1_t2380898469 *, const MethodInfo*))ArraySegmentList_1_get_SegmentCount_m4041807107_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::ClearSegements()
extern "C"  void ArraySegmentList_1_ClearSegements_m4220144570_gshared (ArraySegmentList_1_t2380898469 * __this, const MethodInfo* method);
#define ArraySegmentList_1_ClearSegements_m4220144570(__this, method) ((  void (*) (ArraySegmentList_1_t2380898469 *, const MethodInfo*))ArraySegmentList_1_ClearSegements_m4220144570_gshared)(__this, method)
// T[] SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::ToArrayData(System.Int32,System.Int32)
extern "C"  ObjectU5BU5D_t11523773* ArraySegmentList_1_ToArrayData_m411657921_gshared (ArraySegmentList_1_t2380898469 * __this, int32_t ___startIndex, int32_t ___length, const MethodInfo* method);
#define ArraySegmentList_1_ToArrayData_m411657921(__this, ___startIndex, ___length, method) ((  ObjectU5BU5D_t11523773* (*) (ArraySegmentList_1_t2380898469 *, int32_t, int32_t, const MethodInfo*))ArraySegmentList_1_ToArrayData_m411657921_gshared)(__this, ___startIndex, ___length, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::TrimEnd(System.Int32)
extern "C"  void ArraySegmentList_1_TrimEnd_m309199018_gshared (ArraySegmentList_1_t2380898469 * __this, int32_t ___trimSize, const MethodInfo* method);
#define ArraySegmentList_1_TrimEnd_m309199018(__this, ___trimSize, method) ((  void (*) (ArraySegmentList_1_t2380898469 *, int32_t, const MethodInfo*))ArraySegmentList_1_TrimEnd_m309199018_gshared)(__this, ___trimSize, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Object>::CopyTo(T[],System.Int32,System.Int32,System.Int32)
extern "C"  int32_t ArraySegmentList_1_CopyTo_m3561165033_gshared (ArraySegmentList_1_t2380898469 * __this, ObjectU5BU5D_t11523773* ___to, int32_t ___srcIndex, int32_t ___toIndex, int32_t ___length, const MethodInfo* method);
#define ArraySegmentList_1_CopyTo_m3561165033(__this, ___to, ___srcIndex, ___toIndex, ___length, method) ((  int32_t (*) (ArraySegmentList_1_t2380898469 *, ObjectU5BU5D_t11523773*, int32_t, int32_t, int32_t, const MethodInfo*))ArraySegmentList_1_CopyTo_m3561165033_gshared)(__this, ___to, ___srcIndex, ___toIndex, ___length, method)
