﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// UnityEngine.Component
struct Component_t2126946602;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3547103726;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Predicate`1<System.Object>
struct Predicate_1_t1408070318;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3019176036;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t3123668047;
// UnityEngine.Object[]
struct ObjectU5BU5D_t3051965477;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t113996300;
// System.Collections.Generic.IEnumerable`1<System.Char>
struct IEnumerable_1_t1355893759;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1424601847;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t3650470111;
// System.String
struct String_t;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t3885370180;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_SByte2855346064.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991.h"
#include "mscorlib_System_Void2779279689.h"
#include "System_System_Security_Cryptography_X509Certificat1122151684.h"
#include "mscorlib_System_Single958209021.h"
#include "System_System_Text_RegularExpressions_Mark3725932776.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "System_Core_System_TimeZoneInfo_TimeType2282261447.h"
#include "mscorlib_System_UInt16985925268.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_UInt64985925421.h"
#include "System_System_Uri_UriScheme3266528785.h"
#include "UnityEngine_UnityEngine_Color324137084207.h"
#include "UnityEngine_UnityEngine_ContactPoint2951122365.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3963746319.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResu959898689.h"
#include "UnityEngine_UnityEngine_Keyframe2095052507.h"
#include "UnityEngine_UnityEngine_RaycastHit46221527.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4082783401.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo2591228609.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1317012096.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2223678307.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy1278737203.h"
#include "UnityEngine_UnityEngine_UICharInfo403820581.h"
#include "UnityEngine_UnityEngine_UILineInfo156921283.h"
#include "UnityEngine_UnityEngine_UIVertex2260061605.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen4244616972.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve4196265289.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3547103726.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject184905905.h"
#include "UnityEngine_UnityEngine_ScriptableObject184905905MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown2916437562.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_ClientEn80775955.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2778693821.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_ClientEn80775955MethodDeclarations.h"
#include "mscorlib_System_Random922188920.h"
#include "mscorlib_System_Random922188920MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1408070318.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeData2584644259.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgu318735129.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgu560415562.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Resources1543782994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources1543782994.h"
#include "mscorlib_System_Converter_2_gen113996300.h"
#include "mscorlib_System_Converter_2_gen113996300MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "mscorlib_System_Char2778706699.h"
#include "System_Core_System_Linq_Check3277941805MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "System_Core_System_Linq_Enumerable_Fallback825887490.h"
#include "System_Core_System_Func_2_gen1509682273.h"
#include "System_Core_System_Func_2_gen1509682273MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_PredicateOf_1_ge606765376.h"
#include "System_Core_System_Linq_Enumerable_PredicateOf_1_ge606765376MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_Extensions39620836.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve3885370180.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve4196265289MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1081512082.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1081512082MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"

// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230(__this, ___index, method) ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.SByte>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSByte_t2855346064_m1632182611_gshared (Il2CppArray * __this, int32_t p0, int8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSByte_t2855346064_m1632182611(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisSByte_t2855346064_m1632182611_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern "C"  X509ChainStatus_t1122151684  Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218(__this, ___index, method) ((  X509ChainStatus_t1122151684  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisX509ChainStatus_t1122151684_m1175622045_gshared (Il2CppArray * __this, int32_t p0, X509ChainStatus_t1122151684 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisX509ChainStatus_t1122151684_m1175622045(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, X509ChainStatus_t1122151684 *, const MethodInfo*))Array_GetGenericValueImpl_TisX509ChainStatus_t1122151684_m1175622045_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775(__this, ___index, method) ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Single>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSingle_t958209021_m1387272912_gshared (Il2CppArray * __this, int32_t p0, float* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSingle_t958209021_m1387272912(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, float*, const MethodInfo*))Array_GetGenericValueImpl_TisSingle_t958209021_m1387272912_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t3725932776  Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMark_t3725932776_m160824484(__this, ___index, method) ((  Mark_t3725932776  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Text.RegularExpressions.Mark>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisMark_t3725932776_m2494808835_gshared (Il2CppArray * __this, int32_t p0, Mark_t3725932776 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisMark_t3725932776_m2494808835(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Mark_t3725932776 *, const MethodInfo*))Array_GetGenericValueImpl_TisMark_t3725932776_m2494808835_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t763862892  Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760(__this, ___index, method) ((  TimeSpan_t763862892  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTimeSpan_t763862892_m2245336767_gshared (Il2CppArray * __this, int32_t p0, TimeSpan_t763862892 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTimeSpan_t763862892_m2245336767(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TimeSpan_t763862892 *, const MethodInfo*))Array_GetGenericValueImpl_TisTimeSpan_t763862892_m2245336767_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.TimeZoneInfo/TimeType>(System.Int32)
extern "C"  TimeType_t2282261447  Array_InternalArray__get_Item_TisTimeType_t2282261447_m1298446258_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeType_t2282261447_m1298446258(__this, ___index, method) ((  TimeType_t2282261447  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeType_t2282261447_m1298446258_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.TimeZoneInfo/TimeType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTimeType_t2282261447_m3355356071_gshared (Il2CppArray * __this, int32_t p0, TimeType_t2282261447 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTimeType_t2282261447_m3355356071(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TimeType_t2282261447 *, const MethodInfo*))Array_GetGenericValueImpl_TisTimeType_t2282261447_m3355356071_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256(__this, ___index, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt16>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt16_t985925268_m1140249575_gshared (Il2CppArray * __this, int32_t p0, uint16_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt16_t985925268_m1140249575(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint16_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt16_t985925268_m1140249575_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062(__this, ___index, method) ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt32>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt32_t985925326_m513700641_gshared (Il2CppArray * __this, int32_t p0, uint32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt32_t985925326_m513700641(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt32_t985925326_m513700641_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503(__this, ___index, method) ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt64>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt64_t985925421_m3412168192_gshared (Il2CppArray * __this, int32_t p0, uint64_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt64_t985925421_m3412168192(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint64_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt64_t985925421_m3412168192_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t3266528785  Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123(__this, ___index, method) ((  UriScheme_t3266528785  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<System.Uri/UriScheme>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUriScheme_t3266528785_m4227749606_gshared (Il2CppArray * __this, int32_t p0, UriScheme_t3266528785 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUriScheme_t3266528785_m4227749606(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UriScheme_t3266528785 *, const MethodInfo*))Array_GetGenericValueImpl_TisUriScheme_t3266528785_m4227749606_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern "C"  Color32_t4137084207  Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403(__this, ___index, method) ((  Color32_t4137084207  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Color32>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisColor32_t4137084207_m3930958100_gshared (Il2CppArray * __this, int32_t p0, Color32_t4137084207 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisColor32_t4137084207_m3930958100(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Color32_t4137084207 *, const MethodInfo*))Array_GetGenericValueImpl_TisColor32_t4137084207_m3930958100_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern "C"  ContactPoint_t2951122365  Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859(__this, ___index, method) ((  ContactPoint_t2951122365  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.ContactPoint>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContactPoint_t2951122365_m2683850686_gshared (Il2CppArray * __this, int32_t p0, ContactPoint_t2951122365 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContactPoint_t2951122365_m2683850686(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ContactPoint_t2951122365 *, const MethodInfo*))Array_GetGenericValueImpl_TisContactPoint_t2951122365_m2683850686_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern "C"  ContactPoint2D_t3963746319  Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017(__this, ___index, method) ((  ContactPoint2D_t3963746319  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.ContactPoint2D>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContactPoint2D_t3963746319_m1672251792_gshared (Il2CppArray * __this, int32_t p0, ContactPoint2D_t3963746319 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContactPoint2D_t3963746319_m1672251792(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ContactPoint2D_t3963746319 *, const MethodInfo*))Array_GetGenericValueImpl_TisContactPoint2D_t3963746319_m1672251792_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern "C"  RaycastResult_t959898689  Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905(__this, ___index, method) ((  RaycastResult_t959898689  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.EventSystems.RaycastResult>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRaycastResult_t959898689_m1415359984_gshared (Il2CppArray * __this, int32_t p0, RaycastResult_t959898689 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRaycastResult_t959898689_m1415359984(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RaycastResult_t959898689 *, const MethodInfo*))Array_GetGenericValueImpl_TisRaycastResult_t959898689_m1415359984_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern "C"  Keyframe_t2095052507  Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013(__this, ___index, method) ((  Keyframe_t2095052507  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Keyframe>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyframe_t2095052507_m3275637980_gshared (Il2CppArray * __this, int32_t p0, Keyframe_t2095052507 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyframe_t2095052507_m3275637980(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Keyframe_t2095052507 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyframe_t2095052507_m3275637980_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern "C"  RaycastHit_t46221527  Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793(__this, ___index, method) ((  RaycastHit_t46221527  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.RaycastHit>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRaycastHit_t46221527_m1822174552_gshared (Il2CppArray * __this, int32_t p0, RaycastHit_t46221527 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRaycastHit_t46221527_m1822174552(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RaycastHit_t46221527 *, const MethodInfo*))Array_GetGenericValueImpl_TisRaycastHit_t46221527_m1822174552_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern "C"  RaycastHit2D_t4082783401  Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143(__this, ___index, method) ((  RaycastHit2D_t4082783401  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.RaycastHit2D>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRaycastHit2D_t4082783401_m2530175146_gshared (Il2CppArray * __this, int32_t p0, RaycastHit2D_t4082783401 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRaycastHit2D_t4082783401_m2530175146(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RaycastHit2D_t4082783401 *, const MethodInfo*))Array_GetGenericValueImpl_TisRaycastHit2D_t4082783401_m2530175146_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern "C"  HitInfo_t2591228609  Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997(__this, ___index, method) ((  HitInfo_t2591228609  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SendMouseEvents/HitInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisHitInfo_t2591228609_m3860792378_gshared (Il2CppArray * __this, int32_t p0, HitInfo_t2591228609 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisHitInfo_t2591228609_m3860792378(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, HitInfo_t2591228609 *, const MethodInfo*))Array_GetGenericValueImpl_TisHitInfo_t2591228609_m3860792378_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern "C"  GcAchievementData_t1317012096  Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226(__this, ___index, method) ((  GcAchievementData_t1317012096  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisGcAchievementData_t1317012096_m989090367_gshared (Il2CppArray * __this, int32_t p0, GcAchievementData_t1317012096 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisGcAchievementData_t1317012096_m989090367(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, GcAchievementData_t1317012096 *, const MethodInfo*))Array_GetGenericValueImpl_TisGcAchievementData_t1317012096_m989090367_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern "C"  GcScoreData_t2223678307  Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207(__this, ___index, method) ((  GcScoreData_t2223678307  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisGcScoreData_t2223678307_m4057734434_gshared (Il2CppArray * __this, int32_t p0, GcScoreData_t2223678307 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisGcScoreData_t2223678307_m4057734434(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, GcScoreData_t2223678307 *, const MethodInfo*))Array_GetGenericValueImpl_TisGcScoreData_t2223678307_m4057734434_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015(__this, ___index, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UI.InputField/ContentType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContentType_t1278737203_m2904454496_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContentType_t1278737203_m2904454496(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisContentType_t1278737203_m2904454496_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
extern "C"  UICharInfo_t403820581  Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395(__this, ___index, method) ((  UICharInfo_t403820581  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UICharInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUICharInfo_t403820581_m972947878_gshared (Il2CppArray * __this, int32_t p0, UICharInfo_t403820581 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUICharInfo_t403820581_m972947878(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UICharInfo_t403820581 *, const MethodInfo*))Array_GetGenericValueImpl_TisUICharInfo_t403820581_m972947878_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
extern "C"  UILineInfo_t156921283  Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045(__this, ___index, method) ((  UILineInfo_t156921283  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UILineInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUILineInfo_t156921283_m1279108164_gshared (Il2CppArray * __this, int32_t p0, UILineInfo_t156921283 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUILineInfo_t156921283_m1279108164(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UILineInfo_t156921283 *, const MethodInfo*))Array_GetGenericValueImpl_TisUILineInfo_t156921283_m1279108164_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
extern "C"  UIVertex_t2260061605  Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059(__this, ___index, method) ((  UIVertex_t2260061605  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UIVertex>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUIVertex_t2260061605_m1585369766_gshared (Il2CppArray * __this, int32_t p0, UIVertex_t2260061605 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUIVertex_t2260061605_m1585369766(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UIVertex_t2260061605 *, const MethodInfo*))Array_GetGenericValueImpl_TisUIVertex_t2260061605_m1585369766_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern "C"  Vector2_t3525329788  Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166(__this, ___index, method) ((  Vector2_t3525329788  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector2>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector2_t3525329788_m2608717537_gshared (Il2CppArray * __this, int32_t p0, Vector2_t3525329788 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector2_t3525329788_m2608717537(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector2_t3525329788 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector2_t3525329788_m2608717537_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern "C"  Vector3_t3525329789  Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989(__this, ___index, method) ((  Vector3_t3525329789  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector3>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector3_t3525329789_m2820068450_gshared (Il2CppArray * __this, int32_t p0, Vector3_t3525329789 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector3_t3525329789_m2820068450(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector3_t3525329789 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector3_t3525329789_m2820068450_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
extern "C"  Vector4_t3525329790  Array_InternalArray__get_Item_TisVector4_t3525329790_m823375812_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector4_t3525329790_m823375812(__this, ___index, method) ((  Vector4_t3525329790  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector4_t3525329790_m823375812_gshared)(__this, ___index, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector4>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector4_t3525329790_m3031419363_gshared (Il2CppArray * __this, int32_t p0, Vector4_t3525329790 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector4_t3525329790_m3031419363(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector4_t3525329790 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector4_t3525329790_m3031419363_gshared)(__this, p0, p1, method)
// T UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m900797242(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m833851745_gshared (Component_t2126946602 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m833851745(__this, p0, method) ((  Il2CppObject * (*) (Component_t2126946602 *, bool, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m833851745_gshared)(__this, p0, method)
// T UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentInParent_TisIl2CppObject_m1297875695(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared)(__this, method)
// T UnityEngine.EventSystems.ExecuteEvents::ValidateEventData<System.Object>(UnityEngine.EventSystems.BaseEventData)
extern "C"  Il2CppObject * ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_gshared (Il2CppObject * __this /* static, unused */, BaseEventData_t3547103726 * ___data, const MethodInfo* method);
#define ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176(__this /* static, unused */, ___data, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, BaseEventData_t3547103726 *, const MethodInfo*))ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_gshared)(__this /* static, unused */, ___data, method)
// T UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m337943659_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m337943659(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// T UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m782999868(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_gshared (GameObject_t4012695102 * __this, bool p0, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411(__this, p0, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, bool, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_gshared)(__this, p0, method)
// T UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m829214560_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m829214560(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m829214560_gshared)(__this /* static, unused */, method)
// T UnityEngine.Object::Instantiate<System.Object>(T)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, ___original, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, ___original, method)
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ScriptableObject_CreateInstance_TisIl2CppObject_m512360883(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_gshared)(__this /* static, unused */, method)
// T UnityEngine.UI.Dropdown::GetOrAddComponent<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * Dropdown_GetOrAddComponent_TisIl2CppObject_m4155558758_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___go, const MethodInfo* method);
#define Dropdown_GetOrAddComponent_TisIl2CppObject_m4155558758(__this /* static, unused */, ___go, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))Dropdown_GetOrAddComponent_TisIl2CppObject_m4155558758_gshared)(__this /* static, unused */, ___go, method)
// T[] SuperSocket.ClientEngine.Extensions::CloneRange<System.Byte>(T[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t58506160* Extensions_CloneRange_TisByte_t2778693821_m1443443318_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___source, int32_t ___offset, int32_t ___length, const MethodInfo* method);
#define Extensions_CloneRange_TisByte_t2778693821_m1443443318(__this /* static, unused */, ___source, ___offset, ___length, method) ((  ByteU5BU5D_t58506160* (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t58506160*, int32_t, int32_t, const MethodInfo*))Extensions_CloneRange_TisByte_t2778693821_m1443443318_gshared)(__this /* static, unused */, ___source, ___offset, ___length, method)
// T[] SuperSocket.ClientEngine.Extensions::CloneRange<System.Object>(T[],System.Int32,System.Int32)
extern "C"  ObjectU5BU5D_t11523773* Extensions_CloneRange_TisIl2CppObject_m3906846829_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___source, int32_t ___offset, int32_t ___length, const MethodInfo* method);
#define Extensions_CloneRange_TisIl2CppObject_m3906846829(__this /* static, unused */, ___source, ___offset, ___length, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, int32_t, int32_t, const MethodInfo*))Extensions_CloneRange_TisIl2CppObject_m3906846829_gshared)(__this /* static, unused */, ___source, ___offset, ___length, method)
// T[] SuperSocket.ClientEngine.Extensions::RandomOrder<System.Byte>(T[])
extern "C"  ByteU5BU5D_t58506160* Extensions_RandomOrder_TisByte_t2778693821_m2917735209_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___source, const MethodInfo* method);
#define Extensions_RandomOrder_TisByte_t2778693821_m2917735209(__this /* static, unused */, ___source, method) ((  ByteU5BU5D_t58506160* (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t58506160*, const MethodInfo*))Extensions_RandomOrder_TisByte_t2778693821_m2917735209_gshared)(__this /* static, unused */, ___source, method)
// T[] SuperSocket.ClientEngine.Extensions::RandomOrder<System.Object>(T[])
extern "C"  ObjectU5BU5D_t11523773* Extensions_RandomOrder_TisIl2CppObject_m2089857052_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___source, const MethodInfo* method);
#define Extensions_RandomOrder_TisIl2CppObject_m2089857052(__this /* static, unused */, ___source, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, const MethodInfo*))Extensions_RandomOrder_TisIl2CppObject_m2089857052_gshared)(__this /* static, unused */, ___source, method)
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern "C"  ObjectU5BU5D_t11523773* Array_FindAll_TisIl2CppObject_m3670613038_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___array, Predicate_1_t1408070318 * ___match, const MethodInfo* method);
#define Array_FindAll_TisIl2CppObject_m3670613038(__this /* static, unused */, ___array, ___match, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Predicate_1_t1408070318 *, const MethodInfo*))Array_FindAll_TisIl2CppObject_m3670613038_gshared)(__this /* static, unused */, ___array, ___match, method)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisIl2CppObject_m4097160425_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisIl2CppObject_m4097160425(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773**, int32_t, const MethodInfo*))Array_Resize_TisIl2CppObject_m4097160425_gshared)(__this /* static, unused */, p0, p1, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C"  ObjectU5BU5D_t11523773* CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542(__this /* static, unused */, ___values, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, const MethodInfo*))CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542_gshared)(__this /* static, unused */, ___values, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C"  CustomAttributeNamedArgumentU5BU5D_t3019176036* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t318735129_m2964992489_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t318735129_m2964992489(__this /* static, unused */, ___values, method) ((  CustomAttributeNamedArgumentU5BU5D_t3019176036* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t318735129_m2964992489_gshared)(__this /* static, unused */, ___values, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C"  CustomAttributeTypedArgumentU5BU5D_t3123668047* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t560415562_m3125716954_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t560415562_m3125716954(__this /* static, unused */, ___values, method) ((  CustomAttributeTypedArgumentU5BU5D_t3123668047* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t560415562_m3125716954_gshared)(__this /* static, unused */, ___values, method)
// T[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponents_TisIl2CppObject_m1562339739_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponents_TisIl2CppObject_m1562339739(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponents_TisIl2CppObject_m1562339739_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponents_TisIl2CppObject_m2453515573_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponents_TisIl2CppObject_m2453515573(__this, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m2453515573_gshared)(__this, method)
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m3277197879_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m3277197879(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3277197879_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m4074314606_gshared (Component_t2126946602 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m4074314606(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m4074314606_gshared)(__this, p0, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInChildren_TisIl2CppObject_m1462546696_gshared (GameObject_t4012695102 * __this, bool p0, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m1462546696(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m1462546696_gshared)(__this, p0, method)
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInParent_TisIl2CppObject_m1228840236_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentsInParent_TisIl2CppObject_m1228840236(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInParent_TisIl2CppObject_m1228840236_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInParent_TisIl2CppObject_m1225053603_gshared (Component_t2126946602 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInParent_TisIl2CppObject_m1225053603(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))Component_GetComponentsInParent_TisIl2CppObject_m1225053603_gshared)(__this, p0, method)
// !!0[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_gshared (GameObject_t4012695102 * __this, bool p0, const MethodInfo* method);
#define GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_gshared)(__this, p0, method)
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Object_FindObjectsOfType_TisIl2CppObject_m3888714627_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectsOfType_TisIl2CppObject_m3888714627(__this /* static, unused */, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m3888714627_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t11523773* Resources_ConvertObjects_TisIl2CppObject_m1537961554_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3051965477* p0, const MethodInfo* method);
#define Resources_ConvertObjects_TisIl2CppObject_m1537961554(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3051965477*, const MethodInfo*))Resources_ConvertObjects_TisIl2CppObject_m1537961554_gshared)(__this /* static, unused */, p0, method)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern "C"  ObjectU5BU5D_t11523773* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___array, Converter_2_t113996300 * ___converter, const MethodInfo* method);
#define Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410(__this /* static, unused */, ___array, ___converter, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Converter_2_t113996300 *, const MethodInfo*))Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_gshared)(__this /* static, unused */, ___array, ___converter, method)
// TSource System.Linq.Enumerable::First<System.Char>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  uint16_t Enumerable_First_TisChar_t2778706699_m2365985763_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, const MethodInfo* method);
#define Enumerable_First_TisChar_t2778706699_m2365985763(__this /* static, unused */, ___source, method) ((  uint16_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisChar_t2778706699_m2365985763_gshared)(__this /* static, unused */, ___source, method)
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m2819401402_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, const MethodInfo* method);
#define Enumerable_First_TisIl2CppObject_m2819401402(__this /* static, unused */, ___source, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisIl2CppObject_m2819401402_gshared)(__this /* static, unused */, ___source, method)
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m3431128615_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, Func_2_t1509682273 * ___predicate, int32_t ___fallback, const MethodInfo* method);
#define Enumerable_First_TisIl2CppObject_m3431128615(__this /* static, unused */, ___source, ___predicate, ___fallback, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, int32_t, const MethodInfo*))Enumerable_First_TisIl2CppObject_m3431128615_gshared)(__this /* static, unused */, ___source, ___predicate, ___fallback, method)
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m4033907310_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisIl2CppObject_m4033907310(__this /* static, unused */, ___source, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_FirstOrDefault_TisIl2CppObject_m4033907310_gshared)(__this /* static, unused */, ___source, method)
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m1453134342_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, Func_2_t1509682273 * ___predicate, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisIl2CppObject_m1453134342(__this /* static, unused */, ___source, ___predicate, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_FirstOrDefault_TisIl2CppObject_m1453134342_gshared)(__this /* static, unused */, ___source, ___predicate, method)
// TSource[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Int32U5BU5D_t1809983122* Enumerable_ToArray_TisInt32_t2847414787_m4144341433_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, const MethodInfo* method);
#define Enumerable_ToArray_TisInt32_t2847414787_m4144341433(__this /* static, unused */, ___source, method) ((  Int32U5BU5D_t1809983122* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisInt32_t2847414787_m4144341433_gshared)(__this /* static, unused */, ___source, method)
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  ObjectU5BU5D_t11523773* Enumerable_ToArray_TisIl2CppObject_m235635230_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m235635230(__this /* static, unused */, ___source, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m235635230_gshared)(__this /* static, unused */, ___source, method)
// TValue WebSocket4Net.Extensions::GetValue<System.Object>(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,TValue)
extern "C"  Il2CppObject * Extensions_GetValue_TisIl2CppObject_m46373847_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___valueContainer, String_t* ___name, Il2CppObject * ___defaultValue, const MethodInfo* method);
#define Extensions_GetValue_TisIl2CppObject_m46373847(__this /* static, unused */, ___valueContainer, ___name, ___defaultValue, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, String_t*, Il2CppObject *, const MethodInfo*))Extensions_GetValue_TisIl2CppObject_m46373847_gshared)(__this /* static, unused */, ___valueContainer, ___name, ___defaultValue, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern "C"  GameObject_t4012695102 * ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___root, BaseEventData_t3547103726 * ___eventData, EventFunction_1_t3885370180 * ___callbackFunction, const MethodInfo* method);
#define ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293(__this /* static, unused */, ___root, ___eventData, ___callbackFunction, method) ((  GameObject_t4012695102 * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, BaseEventData_t3547103726 *, EventFunction_1_t3885370180 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_gshared)(__this /* static, unused */, ___root, ___eventData, ___callbackFunction, method)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
extern "C"  bool ExecuteEvents_Execute_TisIl2CppObject_m1533897725_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * p0, BaseEventData_t3547103726 * p1, EventFunction_1_t3885370180 * p2, const MethodInfo* method);
#define ExecuteEvents_Execute_TisIl2CppObject_m1533897725(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, BaseEventData_t3547103726 *, EventFunction_1_t3885370180 *, const MethodInfo*))ExecuteEvents_Execute_TisIl2CppObject_m1533897725_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern "C"  GameObject_t4012695102 * ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___root, const MethodInfo* method);
#define ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506(__this /* static, unused */, ___root, method) ((  GameObject_t4012695102 * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_gshared)(__this /* static, unused */, ___root, method)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::CanHandleEvent<System.Object>(UnityEngine.GameObject)
extern "C"  bool ExecuteEvents_CanHandleEvent_TisIl2CppObject_m2627025177_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * p0, const MethodInfo* method);
#define ExecuteEvents_CanHandleEvent_TisIl2CppObject_m2627025177(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))ExecuteEvents_CanHandleEvent_TisIl2CppObject_m2627025177_gshared)(__this /* static, unused */, p0, method)
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_MetadataUsageId;
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int8_t V_0 = 0x0;
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int8_t*)(&V_0));
		int8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_MetadataUsageId;
extern "C"  X509ChainStatus_t1122151684  Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	X509ChainStatus_t1122151684  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (X509ChainStatus_t1122151684 *)(&V_0));
		X509ChainStatus_t1122151684  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_MetadataUsageId;
extern "C"  float Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (float*)(&V_0));
		float L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_MetadataUsageId;
extern "C"  Mark_t3725932776  Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Mark_t3725932776  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Mark_t3725932776 *)(&V_0));
		Mark_t3725932776  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_MetadataUsageId;
extern "C"  TimeSpan_t763862892  Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t763862892  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TimeSpan_t763862892 *)(&V_0));
		TimeSpan_t763862892  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.TimeZoneInfo/TimeType>(System.Int32)
// T System.Array::InternalArray__get_Item<System.TimeZoneInfo/TimeType>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTimeType_t2282261447_m1298446258_MetadataUsageId;
extern "C"  TimeType_t2282261447  Array_InternalArray__get_Item_TisTimeType_t2282261447_m1298446258_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTimeType_t2282261447_m1298446258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeType_t2282261447  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TimeType_t2282261447 *)(&V_0));
		TimeType_t2282261447  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_MetadataUsageId;
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint16_t*)(&V_0));
		uint16_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_MetadataUsageId;
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint32_t*)(&V_0));
		uint32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_MetadataUsageId;
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint64_t*)(&V_0));
		uint64_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123_MetadataUsageId;
extern "C"  UriScheme_t3266528785  Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UriScheme_t3266528785  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UriScheme_t3266528785 *)(&V_0));
		UriScheme_t3266528785  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_MetadataUsageId;
extern "C"  Color32_t4137084207  Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color32_t4137084207  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Color32_t4137084207 *)(&V_0));
		Color32_t4137084207  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_MetadataUsageId;
extern "C"  ContactPoint_t2951122365  Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ContactPoint_t2951122365  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ContactPoint_t2951122365 *)(&V_0));
		ContactPoint_t2951122365  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017_MetadataUsageId;
extern "C"  ContactPoint2D_t3963746319  Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ContactPoint2D_t3963746319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ContactPoint2D_t3963746319 *)(&V_0));
		ContactPoint2D_t3963746319  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905_MetadataUsageId;
extern "C"  RaycastResult_t959898689  Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastResult_t959898689  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastResult_t959898689 *)(&V_0));
		RaycastResult_t959898689  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_MetadataUsageId;
extern "C"  Keyframe_t2095052507  Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Keyframe_t2095052507  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Keyframe_t2095052507 *)(&V_0));
		Keyframe_t2095052507  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793_MetadataUsageId;
extern "C"  RaycastHit_t46221527  Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t46221527  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastHit_t46221527 *)(&V_0));
		RaycastHit_t46221527  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143_MetadataUsageId;
extern "C"  RaycastHit2D_t4082783401  Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t4082783401  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastHit2D_t4082783401 *)(&V_0));
		RaycastHit2D_t4082783401  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_MetadataUsageId;
extern "C"  HitInfo_t2591228609  Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HitInfo_t2591228609  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (HitInfo_t2591228609 *)(&V_0));
		HitInfo_t2591228609  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_MetadataUsageId;
extern "C"  GcAchievementData_t1317012096  Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcAchievementData_t1317012096  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (GcAchievementData_t1317012096 *)(&V_0));
		GcAchievementData_t1317012096  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_MetadataUsageId;
extern "C"  GcScoreData_t2223678307  Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcScoreData_t2223678307  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (GcScoreData_t2223678307 *)(&V_0));
		GcScoreData_t2223678307  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395_MetadataUsageId;
extern "C"  UICharInfo_t403820581  Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UICharInfo_t403820581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UICharInfo_t403820581 *)(&V_0));
		UICharInfo_t403820581  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045_MetadataUsageId;
extern "C"  UILineInfo_t156921283  Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UILineInfo_t156921283  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UILineInfo_t156921283 *)(&V_0));
		UILineInfo_t156921283  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059_MetadataUsageId;
extern "C"  UIVertex_t2260061605  Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t2260061605  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UIVertex_t2260061605 *)(&V_0));
		UIVertex_t2260061605  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_MetadataUsageId;
extern "C"  Vector2_t3525329788  Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector2_t3525329788 *)(&V_0));
		Vector2_t3525329788  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_MetadataUsageId;
extern "C"  Vector3_t3525329789  Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector3_t3525329789 *)(&V_0));
		Vector3_t3525329789  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVector4_t3525329790_m823375812_MetadataUsageId;
extern "C"  Vector4_t3525329790  Array_InternalArray__get_Item_TisVector4_t3525329790_m823375812_gshared (Il2CppArray * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector4_t3525329790_m823375812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t3525329790  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector4_t3525329790 *)(&V_0));
		Vector4_t3525329790  L_4 = V_0;
		return L_4;
	}
}
// T UnityEngine.Component::GetComponent<System.Object>()
// T UnityEngine.Component::GetComponent<System.Object>()
extern TypeInfo* CastHelper_1_t4244616972_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponent_TisIl2CppObject_m267839954_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponent_TisIl2CppObject_m267839954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CastHelper_1_t4244616972  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CastHelper_1_t4244616972_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IntPtr_t* L_1 = (IntPtr_t*)(&V_0)->get_address_of_onePointerFurtherThanT_1();
		IntPtr_t L_2;
		memset(&L_2, 0, sizeof(L_2));
		IntPtr__ctor_m2509422495(&L_2, (void*)(void*)L_1, /*hidden argument*/NULL);
		NullCheck((Component_t2126946602 *)__this);
		Component_GetComponentFastPath_m1455568887((Component_t2126946602 *)__this, (Type_t *)L_0, (IntPtr_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = (Il2CppObject *)(&V_0)->get_t_0();
		return L_3;
	}
}
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		NullCheck((Component_t2126946602 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Component_t2126946602 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Component_t2126946602 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
// T UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponentInChildren_TisIl2CppObject_m833851745_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m833851745_gshared (Component_t2126946602 * __this, bool ___includeInactive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponentInChildren_TisIl2CppObject_m833851745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive;
		NullCheck((Component_t2126946602 *)__this);
		Component_t2126946602 * L_2 = Component_GetComponentInChildren_m1899663946((Component_t2126946602 *)__this, (Type_t *)L_0, (bool)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.Component::GetComponentInParent<System.Object>()
// T UnityEngine.Component::GetComponentInParent<System.Object>()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponentInParent_TisIl2CppObject_m1297875695_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponentInParent_TisIl2CppObject_m1297875695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((Component_t2126946602 *)__this);
		Component_t2126946602 * L_1 = Component_GetComponentInParent_m1953645192((Component_t2126946602 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.EventSystems.ExecuteEvents::ValidateEventData<System.Object>(UnityEngine.EventSystems.BaseEventData)
// T UnityEngine.EventSystems.ExecuteEvents::ValidateEventData<System.Object>(UnityEngine.EventSystems.BaseEventData)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1477400244;
extern const uint32_t ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_MetadataUsageId;
extern "C"  Il2CppObject * ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_gshared (Il2CppObject * __this /* static, unused */, BaseEventData_t3547103726 * ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseEventData_t3547103726 * L_0 = ___data;
		if (((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
		{
			goto IL_003a;
		}
	}
	{
		BaseEventData_t3547103726 * L_1 = ___data;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m2022236990((Il2CppObject *)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2398979370(NULL /*static, unused*/, (String_t*)_stringLiteral1477400244, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		ArgumentException_t124305799 * L_5 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_003a:
	{
		BaseEventData_t3547103726 * L_6 = ___data;
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}
}
// T UnityEngine.GameObject::AddComponent<System.Object>()
// T UnityEngine.GameObject::AddComponent<System.Object>()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_AddComponent_TisIl2CppObject_m337943659_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m337943659_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_AddComponent_TisIl2CppObject_m337943659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)__this);
		Component_t2126946602 * L_1 = GameObject_AddComponent_m2208780168((GameObject_t4012695102 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.GameObject::GetComponent<System.Object>()
// T UnityEngine.GameObject::GetComponent<System.Object>()
extern TypeInfo* CastHelper_1_t4244616972_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponent_TisIl2CppObject_m2447772384_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponent_TisIl2CppObject_m2447772384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CastHelper_1_t4244616972  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CastHelper_1_t4244616972_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IntPtr_t* L_1 = (IntPtr_t*)(&V_0)->get_address_of_onePointerFurtherThanT_1();
		IntPtr_t L_2;
		memset(&L_2, 0, sizeof(L_2));
		IntPtr__ctor_m2509422495(&L_2, (void*)(void*)L_1, /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)__this);
		GameObject_GetComponentFastPath_m2905716663((GameObject_t4012695102 *)__this, (Type_t *)L_0, (IntPtr_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = (Il2CppObject *)(&V_0)->get_t_0();
		return L_3;
	}
}
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		NullCheck((GameObject_t4012695102 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t4012695102 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_gshared (GameObject_t4012695102 * __this, bool ___includeInactive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive;
		NullCheck((GameObject_t4012695102 *)__this);
		Component_t2126946602 * L_2 = GameObject_GetComponentInChildren_m1490154500((GameObject_t4012695102 *)__this, (Type_t *)L_0, (bool)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.Object::FindObjectOfType<System.Object>()
// T UnityEngine.Object::FindObjectOfType<System.Object>()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectOfType_TisIl2CppObject_m829214560_MetadataUsageId;
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m829214560_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_TisIl2CppObject_m829214560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Object_FindObjectOfType_m3820159265(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T)
// T UnityEngine.Object::Instantiate<System.Object>(T)
extern Il2CppCodeGenString* _stringLiteral3473406;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m3133387403_MetadataUsageId;
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m3133387403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___original;
		Object_CheckNullArgument_m264735768(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral3473406, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___original;
		Object_t3878351788 * L_2 = Object_Internal_CloneSingle_m3129073756(NULL /*static, unused*/, (Object_t3878351788 *)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}
}
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_MetadataUsageId;
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		ScriptableObject_t184905905 * L_1 = ScriptableObject_CreateInstance_m3255479417(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.UI.Dropdown::GetOrAddComponent<System.Object>(UnityEngine.GameObject)
// T UnityEngine.UI.Dropdown::GetOrAddComponent<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * Dropdown_GetOrAddComponent_TisIl2CppObject_m4155558758_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___go, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		GameObject_t4012695102 * L_0 = ___go;
		NullCheck((GameObject_t4012695102 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, (Object_t3878351788 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		GameObject_t4012695102 * L_4 = ___go;
		NullCheck((GameObject_t4012695102 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)((GameObject_t4012695102 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_5;
	}

IL_001e:
	{
		Il2CppObject * L_6 = V_0;
		return L_6;
	}
}
// T[] SuperSocket.ClientEngine.Extensions::CloneRange<System.Byte>(T[],System.Int32,System.Int32)
// T[] SuperSocket.ClientEngine.Extensions::CloneRange<System.Byte>(T[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t58506160* Extensions_CloneRange_TisByte_t2778693821_m1443443318_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___source, int32_t ___offset, int32_t ___length, const MethodInfo* method)
{
	ByteU5BU5D_t58506160* V_0 = NULL;
	{
		int32_t L_0 = ___length;
		V_0 = (ByteU5BU5D_t58506160*)((ByteU5BU5D_t58506160*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)L_0));
		ByteU5BU5D_t58506160* L_1 = ___source;
		int32_t L_2 = ___offset;
		ByteU5BU5D_t58506160* L_3 = V_0;
		int32_t L_4 = ___length;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, (int32_t)L_4, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_5 = V_0;
		return L_5;
	}
}
// T[] SuperSocket.ClientEngine.Extensions::CloneRange<System.Object>(T[],System.Int32,System.Int32)
// T[] SuperSocket.ClientEngine.Extensions::CloneRange<System.Object>(T[],System.Int32,System.Int32)
extern "C"  ObjectU5BU5D_t11523773* Extensions_CloneRange_TisIl2CppObject_m3906846829_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___source, int32_t ___offset, int32_t ___length, const MethodInfo* method)
{
	ObjectU5BU5D_t11523773* V_0 = NULL;
	{
		int32_t L_0 = ___length;
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)L_0));
		ObjectU5BU5D_t11523773* L_1 = ___source;
		int32_t L_2 = ___offset;
		ObjectU5BU5D_t11523773* L_3 = V_0;
		int32_t L_4 = ___length;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, (int32_t)L_4, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_5 = V_0;
		return L_5;
	}
}
// T[] SuperSocket.ClientEngine.Extensions::RandomOrder<System.Byte>(T[])
// T[] SuperSocket.ClientEngine.Extensions::RandomOrder<System.Byte>(T[])
extern TypeInfo* Extensions_t80775955_il2cpp_TypeInfo_var;
extern const uint32_t Extensions_RandomOrder_TisByte_t2778693821_m2917735209_MetadataUsageId;
extern "C"  ByteU5BU5D_t58506160* Extensions_RandomOrder_TisByte_t2778693821_m2917735209_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___source, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Extensions_RandomOrder_TisByte_t2778693821_m2917735209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	uint8_t V_4 = 0x0;
	{
		ByteU5BU5D_t58506160* L_0 = ___source;
		NullCheck(L_0);
		V_0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))/(int32_t)2));
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t80775955_il2cpp_TypeInfo_var);
		Random_t922188920 * L_1 = ((Extensions_t80775955_StaticFields*)Extensions_t80775955_il2cpp_TypeInfo_var->static_fields)->get_m_Random_0();
		ByteU5BU5D_t58506160* L_2 = ___source;
		NullCheck(L_2);
		NullCheck((Random_t922188920 *)L_1);
		int32_t L_3 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, (Random_t922188920 *)L_1, (int32_t)0, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))-(int32_t)1)));
		V_2 = (int32_t)L_3;
		Random_t922188920 * L_4 = ((Extensions_t80775955_StaticFields*)Extensions_t80775955_il2cpp_TypeInfo_var->static_fields)->get_m_Random_0();
		ByteU5BU5D_t58506160* L_5 = ___source;
		NullCheck(L_5);
		NullCheck((Random_t922188920 *)L_4);
		int32_t L_6 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, (Random_t922188920 *)L_4, (int32_t)0, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))-(int32_t)1)));
		V_3 = (int32_t)L_6;
		int32_t L_7 = V_2;
		int32_t L_8 = V_3;
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_0050;
		}
	}
	{
		ByteU5BU5D_t58506160* L_9 = ___source;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		V_4 = (uint8_t)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11)));
		ByteU5BU5D_t58506160* L_12 = ___source;
		int32_t L_13 = V_3;
		ByteU5BU5D_t58506160* L_14 = ___source;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (uint8_t)((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16))));
		ByteU5BU5D_t58506160* L_17 = ___source;
		int32_t L_18 = V_2;
		uint8_t L_19 = V_4;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (uint8_t)L_19);
	}

IL_0050:
	{
		int32_t L_20 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_21 = V_1;
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_000a;
		}
	}
	{
		ByteU5BU5D_t58506160* L_23 = ___source;
		return L_23;
	}
}
// T[] SuperSocket.ClientEngine.Extensions::RandomOrder<System.Object>(T[])
// T[] SuperSocket.ClientEngine.Extensions::RandomOrder<System.Object>(T[])
extern TypeInfo* Extensions_t80775955_il2cpp_TypeInfo_var;
extern const uint32_t Extensions_RandomOrder_TisIl2CppObject_m2089857052_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* Extensions_RandomOrder_TisIl2CppObject_m2089857052_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___source, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Extensions_RandomOrder_TisIl2CppObject_m2089857052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Il2CppObject * V_4 = NULL;
	{
		ObjectU5BU5D_t11523773* L_0 = ___source;
		NullCheck(L_0);
		V_0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))/(int32_t)2));
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t80775955_il2cpp_TypeInfo_var);
		Random_t922188920 * L_1 = ((Extensions_t80775955_StaticFields*)Extensions_t80775955_il2cpp_TypeInfo_var->static_fields)->get_m_Random_0();
		ObjectU5BU5D_t11523773* L_2 = ___source;
		NullCheck(L_2);
		NullCheck((Random_t922188920 *)L_1);
		int32_t L_3 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, (Random_t922188920 *)L_1, (int32_t)0, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))-(int32_t)1)));
		V_2 = (int32_t)L_3;
		Random_t922188920 * L_4 = ((Extensions_t80775955_StaticFields*)Extensions_t80775955_il2cpp_TypeInfo_var->static_fields)->get_m_Random_0();
		ObjectU5BU5D_t11523773* L_5 = ___source;
		NullCheck(L_5);
		NullCheck((Random_t922188920 *)L_4);
		int32_t L_6 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, (Random_t922188920 *)L_4, (int32_t)0, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))-(int32_t)1)));
		V_3 = (int32_t)L_6;
		int32_t L_7 = V_2;
		int32_t L_8 = V_3;
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_0050;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = ___source;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		V_4 = (Il2CppObject *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11)));
		ObjectU5BU5D_t11523773* L_12 = ___source;
		int32_t L_13 = V_3;
		ObjectU5BU5D_t11523773* L_14 = ___source;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (Il2CppObject *)((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16))));
		ObjectU5BU5D_t11523773* L_17 = ___source;
		int32_t L_18 = V_2;
		Il2CppObject * L_19 = V_4;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_19);
	}

IL_0050:
	{
		int32_t L_20 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_21 = V_1;
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_000a;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_23 = ___source;
		return L_23;
	}
}
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral103668165;
extern const uint32_t Array_FindAll_TisIl2CppObject_m3670613038_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* Array_FindAll_TisIl2CppObject_m3670613038_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___array, Predicate_1_t1408070318 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_FindAll_TisIl2CppObject_m3670613038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ObjectU5BU5D_t11523773* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	ObjectU5BU5D_t11523773* V_3 = NULL;
	int32_t V_4 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Predicate_1_t1408070318 * L_2 = ___match;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, (String_t*)_stringLiteral103668165, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		V_0 = (int32_t)0;
		ObjectU5BU5D_t11523773* L_4 = ___array;
		NullCheck(L_4);
		V_1 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		ObjectU5BU5D_t11523773* L_5 = ___array;
		V_3 = (ObjectU5BU5D_t11523773*)L_5;
		V_4 = (int32_t)0;
		goto IL_005e;
	}

IL_0037:
	{
		ObjectU5BU5D_t11523773* L_6 = V_3;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_2 = (Il2CppObject *)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8)));
		Predicate_1_t1408070318 * L_9 = ___match;
		Il2CppObject * L_10 = V_2;
		NullCheck((Predicate_1_t1408070318 *)L_9);
		bool L_11 = ((  bool (*) (Predicate_1_t1408070318 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Predicate_1_t1408070318 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		if (!L_11)
		{
			goto IL_0058;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_12 = V_1;
		int32_t L_13 = V_0;
		int32_t L_14 = (int32_t)L_13;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		Il2CppObject * L_15 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Il2CppObject *)L_15);
	}

IL_0058:
	{
		int32_t L_16 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_17 = V_4;
		ObjectU5BU5D_t11523773* L_18 = V_3;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_19 = V_0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773**)(&V_1), (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		ObjectU5BU5D_t11523773* L_20 = V_1;
		return L_20;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C"  ObjectU5BU5D_t11523773* CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values, const MethodInfo* method)
{
	ObjectU5BU5D_t11523773* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___values;
		NullCheck(L_0);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		ObjectU5BU5D_t11523773* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t11523773* L_3 = ___values;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)((Il2CppObject *)Castclass(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t11523773* L_8 = ___values;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = V_0;
		return L_9;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C"  CustomAttributeNamedArgumentU5BU5D_t3019176036* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t318735129_m2964992489_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t3019176036* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___values;
		NullCheck(L_0);
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)((CustomAttributeNamedArgumentU5BU5D_t3019176036*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t11523773* L_3 = ___values;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t11523773* L_8 = ___values;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_9 = V_0;
		return L_9;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C"  CustomAttributeTypedArgumentU5BU5D_t3123668047* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t560415562_m3125716954_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t3123668047* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___values;
		NullCheck(L_0);
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)((CustomAttributeTypedArgumentU5BU5D_t3123668047*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t11523773* L_3 = ___values;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t11523773* L_8 = ___values;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_9 = V_0;
		return L_9;
	}
}
// T[] UnityEngine.Component::GetComponents<System.Object>()
// T[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponents_TisIl2CppObject_m1562339739_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899((Component_t2126946602 *)__this, /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)L_0);
		ObjectU5BU5D_t11523773* L_1 = ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m3277197879_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		ObjectU5BU5D_t11523773* L_0 = ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Component_t2126946602 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m4074314606_gshared (Component_t2126946602 * __this, bool ___includeInactive, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899((Component_t2126946602 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive;
		NullCheck((GameObject_t4012695102 *)L_0);
		ObjectU5BU5D_t11523773* L_2 = ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInParent_TisIl2CppObject_m1228840236_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		ObjectU5BU5D_t11523773* L_0 = ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Component_t2126946602 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInParent_TisIl2CppObject_m1225053603_gshared (Component_t2126946602 * __this, bool ___includeInactive, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899((Component_t2126946602 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive;
		NullCheck((GameObject_t4012695102 *)L_0);
		ObjectU5BU5D_t11523773* L_2 = ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// T[] UnityEngine.GameObject::GetComponents<System.Object>()
// T[] UnityEngine.GameObject::GetComponents<System.Object>()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponents_TisIl2CppObject_m2453515573_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponents_TisIl2CppObject_m2453515573_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponents_TisIl2CppObject_m2453515573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)__this);
		Il2CppArray * L_1 = GameObject_GetComponentsInternal_m181453881((GameObject_t4012695102 *)__this, (Type_t *)L_0, (bool)1, (bool)0, (bool)1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t11523773*)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInChildren_TisIl2CppObject_m1462546696_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInChildren_TisIl2CppObject_m1462546696_gshared (GameObject_t4012695102 * __this, bool ___includeInactive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInChildren_TisIl2CppObject_m1462546696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive;
		NullCheck((GameObject_t4012695102 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m181453881((GameObject_t4012695102 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t11523773*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_gshared (GameObject_t4012695102 * __this, bool ___includeInactive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive;
		NullCheck((GameObject_t4012695102 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m181453881((GameObject_t4012695102 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)1, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t11523773*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectsOfType_TisIl2CppObject_m3888714627_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* Object_FindObjectsOfType_TisIl2CppObject_m3888714627_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectsOfType_TisIl2CppObject_m3888714627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		ObjectU5BU5D_t3051965477* L_1 = Object_FindObjectsOfType_m975740280(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3051965477*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t3051965477*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// T[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
// T[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t11523773* Resources_ConvertObjects_TisIl2CppObject_m1537961554_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3051965477* ___rawObjects, const MethodInfo* method)
{
	ObjectU5BU5D_t11523773* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t3051965477* L_0 = ___rawObjects;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (ObjectU5BU5D_t11523773*)NULL;
	}

IL_0008:
	{
		ObjectU5BU5D_t3051965477* L_1 = ___rawObjects;
		NullCheck(L_1);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_002b;
	}

IL_0018:
	{
		ObjectU5BU5D_t11523773* L_2 = V_0;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t3051965477* L_4 = ___rawObjects;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Il2CppObject *)((Il2CppObject *)Castclass(((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_8 = V_1;
		ObjectU5BU5D_t11523773* L_9 = V_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_10 = V_0;
		return L_10;
	}
}
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral3945236896;
extern const uint32_t Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___array, Converter_2_t113996300 * ___converter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t11523773* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Converter_2_t113996300 * L_2 = ___converter;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, (String_t*)_stringLiteral3945236896, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t11523773* L_4 = ___array;
		NullCheck(L_4);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_004a;
	}

IL_0032:
	{
		ObjectU5BU5D_t11523773* L_5 = V_0;
		int32_t L_6 = V_1;
		Converter_2_t113996300 * L_7 = ___converter;
		ObjectU5BU5D_t11523773* L_8 = ___array;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck((Converter_2_t113996300 *)L_7);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (Converter_2_t113996300 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Converter_2_t113996300 *)L_7, (Il2CppObject *)((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Il2CppObject *)L_11);
		int32_t L_12 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_13 = V_1;
		ObjectU5BU5D_t11523773* L_14 = ___array;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0032;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_15 = V_0;
		return L_15;
	}
}
// TSource System.Linq.Enumerable::First<System.Char>(System.Collections.Generic.IEnumerable`1<TSource>)
// TSource System.Linq.Enumerable::First<System.Char>(System.Collections.Generic.IEnumerable`1<TSource>)
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_First_TisChar_t2778706699_m2365985763_MetadataUsageId;
extern "C"  uint16_t Enumerable_First_TisChar_t2778706699_m2365985763_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_First_TisChar_t2778706699_m2365985763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	uint16_t V_2 = 0x0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Char>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_3);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		uint16_t L_6 = InterfaceFuncInvoker1< uint16_t, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Char>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (int32_t)0);
		return L_6;
	}

IL_0026:
	{
		InvalidOperationException_t2420574324 * L_7 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002c:
	{
		Il2CppObject* L_8 = ___source;
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject* L_9 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Char>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Il2CppObject*)L_8);
		V_1 = (Il2CppObject*)L_9;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (!L_11)
			{
				goto IL_004a;
			}
		}

IL_003e:
		{
			Il2CppObject* L_12 = V_1;
			NullCheck((Il2CppObject*)L_12);
			uint16_t L_13 = InterfaceFuncInvoker0< uint16_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Char>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 3), (Il2CppObject*)L_12);
			V_2 = (uint16_t)L_13;
			IL2CPP_LEAVE(0x62, FINALLY_004f);
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_1;
			if (!L_14)
			{
				goto IL_005b;
			}
		}

IL_0055:
		{
			Il2CppObject* L_15 = V_1;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
		}

IL_005b:
		{
			IL2CPP_END_FINALLY(79)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005c:
	{
		InvalidOperationException_t2420574324 * L_16 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0062:
	{
		uint16_t L_17 = V_2;
		return L_17;
	}
}
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_First_TisIl2CppObject_m2819401402_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m2819401402_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_First_TisIl2CppObject_m2819401402_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_3);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (int32_t)0);
		return L_6;
	}

IL_0026:
	{
		InvalidOperationException_t2420574324 * L_7 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002c:
	{
		Il2CppObject* L_8 = ___source;
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject* L_9 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Il2CppObject*)L_8);
		V_1 = (Il2CppObject*)L_9;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (!L_11)
			{
				goto IL_004a;
			}
		}

IL_003e:
		{
			Il2CppObject* L_12 = V_1;
			NullCheck((Il2CppObject*)L_12);
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 3), (Il2CppObject*)L_12);
			V_2 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x62, FINALLY_004f);
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_1;
			if (!L_14)
			{
				goto IL_005b;
			}
		}

IL_0055:
		{
			Il2CppObject* L_15 = V_1;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
		}

IL_005b:
		{
			IL2CPP_END_FINALLY(79)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005c:
	{
		InvalidOperationException_t2420574324 * L_16 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0062:
	{
		Il2CppObject * L_17 = V_2;
		return L_17;
	}
}
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_First_TisIl2CppObject_m3431128615_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m3431128615_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, Func_2_t1509682273 * ___predicate, int32_t ___fallback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_First_TisIl2CppObject_m3431128615_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Il2CppObject *)L_3;
			Func_2_t1509682273 * L_4 = ___predicate;
			Il2CppObject * L_5 = V_0;
			NullCheck((Func_2_t1509682273 *)L_4);
			bool L_6 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)((Func_2_t1509682273 *)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			if (!L_6)
			{
				goto IL_0026;
			}
		}

IL_001f:
		{
			Il2CppObject * L_7 = V_0;
			V_2 = (Il2CppObject *)L_7;
			IL2CPP_LEAVE(0x58, FINALLY_0036);
		}

IL_0026:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_000c;
			}
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			if (L_10)
			{
				goto IL_003a;
			}
		}

IL_0039:
		{
			IL2CPP_END_FINALLY(54)
		}

IL_003a:
		{
			Il2CppObject* L_11 = V_1;
			NullCheck((Il2CppObject *)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			IL2CPP_END_FINALLY(54)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x58, IL_0058)
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		int32_t L_12 = ___fallback;
		if ((!(((uint32_t)L_12) == ((uint32_t)1))))
		{
			goto IL_004e;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_13 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_004e:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_14 = V_3;
		return L_14;
	}

IL_0058:
	{
		Il2CppObject * L_15 = V_2;
		return L_15;
	}
}
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m4033907310_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Func_2_t1509682273 * L_2 = ((PredicateOf_1_t606765376_StaticFields*)IL2CPP_RGCTX_DATA(method->rgctx_data, 0)->static_fields)->get_Always_0();
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_1, (Func_2_t1509682273 *)L_2, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m1453134342_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, Func_2_t1509682273 * ___predicate, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source;
		Func_2_t1509682273 * L_1 = ___predicate;
		Check_SourceAndPredicate_m2252398949(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		Il2CppObject* L_2 = ___source;
		Func_2_t1509682273 * L_3 = ___predicate;
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, (Func_2_t1509682273 *)L_3, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_4;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
// TSource[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Int32U5BU5D_t1809983122* Enumerable_ToArray_TisInt32_t2847414787_m4144341433_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Int32U5BU5D_t1809983122* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (Int32U5BU5D_t1809983122*)((Int32U5BU5D_t1809983122*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		Int32U5BU5D_t1809983122* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< Int32U5BU5D_t1809983122*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Int32>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (Int32U5BU5D_t1809983122*)L_6, (int32_t)0);
		Int32U5BU5D_t1809983122* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source;
		List_1_t3644373756 * L_9 = (List_1_t3644373756 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t3644373756 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t3644373756 *)L_9);
		Int32U5BU5D_t1809983122* L_10 = ((  Int32U5BU5D_t1809983122* (*) (List_1_t3644373756 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->method)((List_1_t3644373756 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  ObjectU5BU5D_t11523773* Enumerable_ToArray_TisIl2CppObject_m235635230_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	ObjectU5BU5D_t11523773* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		ObjectU5BU5D_t11523773* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< ObjectU5BU5D_t11523773*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (ObjectU5BU5D_t11523773*)L_6, (int32_t)0);
		ObjectU5BU5D_t11523773* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source;
		List_1_t1634065389 * L_9 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t1634065389 *)L_9);
		ObjectU5BU5D_t11523773* L_10 = ((  ObjectU5BU5D_t11523773* (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->method)((List_1_t1634065389 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// TValue WebSocket4Net.Extensions::GetValue<System.Object>(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,TValue)
// TValue WebSocket4Net.Extensions::GetValue<System.Object>(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,TValue)
extern TypeInfo* IDictionary_2_t3650470111_il2cpp_TypeInfo_var;
extern const uint32_t Extensions_GetValue_TisIl2CppObject_m46373847_MetadataUsageId;
extern "C"  Il2CppObject * Extensions_GetValue_TisIl2CppObject_m46373847_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___valueContainer, String_t* ___name, Il2CppObject * ___defaultValue, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Extensions_GetValue_TisIl2CppObject_m46373847_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	{
		Il2CppObject* L_0 = ___valueContainer;
		String_t* L_1 = ___name;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker2< bool, String_t*, Il2CppObject ** >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, (Il2CppObject*)L_0, (String_t*)L_1, (Il2CppObject **)(&V_0));
		V_2 = (bool)L_2;
		bool L_3 = V_2;
		if (L_3)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_4 = ___defaultValue;
		V_1 = (Il2CppObject *)L_4;
		goto IL_001b;
	}

IL_0012:
	{
		Il2CppObject * L_5 = V_0;
		V_1 = (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern TypeInfo* ExecuteEvents_t4196265289_il2cpp_TypeInfo_var;
extern const uint32_t ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_MetadataUsageId;
extern "C"  GameObject_t4012695102 * ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___root, BaseEventData_t3547103726 * ___eventData, EventFunction_1_t3885370180 * ___callbackFunction, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Transform_t284553113 * V_1 = NULL;
	{
		GameObject_t4012695102 * L_0 = ___root;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t4196265289_il2cpp_TypeInfo_var);
		List_1_t1081512082 * L_1 = ((ExecuteEvents_t4196265289_StaticFields*)ExecuteEvents_t4196265289_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		ExecuteEvents_GetEventChain_m1321751930(NULL /*static, unused*/, (GameObject_t4012695102 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/NULL);
		V_0 = (int32_t)0;
		goto IL_003b;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t4196265289_il2cpp_TypeInfo_var);
		List_1_t1081512082 * L_2 = ((ExecuteEvents_t4196265289_StaticFields*)ExecuteEvents_t4196265289_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		int32_t L_3 = V_0;
		NullCheck((List_1_t1081512082 *)L_2);
		Transform_t284553113 * L_4 = VirtFuncInvoker1< Transform_t284553113 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32) */, (List_1_t1081512082 *)L_2, (int32_t)L_3);
		V_1 = (Transform_t284553113 *)L_4;
		Transform_t284553113 * L_5 = V_1;
		NullCheck((Component_t2126946602 *)L_5);
		GameObject_t4012695102 * L_6 = Component_get_gameObject_m1170635899((Component_t2126946602 *)L_5, /*hidden argument*/NULL);
		BaseEventData_t3547103726 * L_7 = ___eventData;
		EventFunction_1_t3885370180 * L_8 = ___callbackFunction;
		bool L_9 = ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, BaseEventData_t3547103726 *, EventFunction_1_t3885370180 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (GameObject_t4012695102 *)L_6, (BaseEventData_t3547103726 *)L_7, (EventFunction_1_t3885370180 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_9)
		{
			goto IL_0037;
		}
	}
	{
		Transform_t284553113 * L_10 = V_1;
		NullCheck((Component_t2126946602 *)L_10);
		GameObject_t4012695102 * L_11 = Component_get_gameObject_m1170635899((Component_t2126946602 *)L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0037:
	{
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t4196265289_il2cpp_TypeInfo_var);
		List_1_t1081512082 * L_14 = ((ExecuteEvents_t4196265289_StaticFields*)ExecuteEvents_t4196265289_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		NullCheck((List_1_t1081512082 *)L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, (List_1_t1081512082 *)L_14);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0012;
		}
	}
	{
		return (GameObject_t4012695102 *)NULL;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern TypeInfo* ExecuteEvents_t4196265289_il2cpp_TypeInfo_var;
extern const uint32_t ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_MetadataUsageId;
extern "C"  GameObject_t4012695102 * ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___root, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t284553113 * V_0 = NULL;
	{
		GameObject_t4012695102 * L_0 = ___root;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (GameObject_t4012695102 *)NULL;
	}

IL_000e:
	{
		GameObject_t4012695102 * L_2 = ___root;
		NullCheck((GameObject_t4012695102 *)L_2);
		Transform_t284553113 * L_3 = GameObject_get_transform_m1278640159((GameObject_t4012695102 *)L_2, /*hidden argument*/NULL);
		V_0 = (Transform_t284553113 *)L_3;
		goto IL_0038;
	}

IL_001a:
	{
		Transform_t284553113 * L_4 = V_0;
		NullCheck((Component_t2126946602 *)L_4);
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899((Component_t2126946602 *)L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t4196265289_il2cpp_TypeInfo_var);
		bool L_6 = ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (GameObject_t4012695102 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_6)
		{
			goto IL_0031;
		}
	}
	{
		Transform_t284553113 * L_7 = V_0;
		NullCheck((Component_t2126946602 *)L_7);
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899((Component_t2126946602 *)L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0031:
	{
		Transform_t284553113 * L_9 = V_0;
		NullCheck((Transform_t284553113 *)L_9);
		Transform_t284553113 * L_10 = Transform_get_parent_m2236876972((Transform_t284553113 *)L_9, /*hidden argument*/NULL);
		V_0 = (Transform_t284553113 *)L_10;
	}

IL_0038:
	{
		Transform_t284553113 * L_11 = V_0;
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, (Object_t3878351788 *)L_11, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_001a;
		}
	}
	{
		return (GameObject_t4012695102 *)NULL;
	}
}
