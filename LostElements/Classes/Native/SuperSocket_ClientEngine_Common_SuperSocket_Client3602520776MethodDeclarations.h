﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.ProxyEventArgs
struct ProxyEventArgs_t3602520776;
// System.Net.Sockets.Socket
struct Socket_t150013987;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"

// System.Boolean SuperSocket.ClientEngine.ProxyEventArgs::get_Connected()
extern "C"  bool ProxyEventArgs_get_Connected_m3880444133 (ProxyEventArgs_t3602520776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.Socket SuperSocket.ClientEngine.ProxyEventArgs::get_Socket()
extern "C"  Socket_t150013987 * ProxyEventArgs_get_Socket_m476433455 (ProxyEventArgs_t3602520776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception SuperSocket.ClientEngine.ProxyEventArgs::get_Exception()
extern "C"  Exception_t1967233988 * ProxyEventArgs_get_Exception_m210430180 (ProxyEventArgs_t3602520776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
