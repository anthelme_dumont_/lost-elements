﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.CloseStatusCodeHybi10
struct CloseStatusCodeHybi10_t635449120;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::.ctor()
extern "C"  void CloseStatusCodeHybi10__ctor_m4111104231 (CloseStatusCodeHybi10_t635449120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::get_NormalClosure()
extern "C"  int32_t CloseStatusCodeHybi10_get_NormalClosure_m707748958 (CloseStatusCodeHybi10_t635449120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_NormalClosure(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_NormalClosure_m2689937357 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_GoingAway(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_GoingAway_m114964737 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::get_ProtocolError()
extern "C"  int32_t CloseStatusCodeHybi10_get_ProtocolError_m1205593178 (CloseStatusCodeHybi10_t635449120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_ProtocolError(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_ProtocolError_m1698010569 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_NotAcceptableData(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_NotAcceptableData_m2003109016 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_TooLargeFrame(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_TooLargeFrame_m481629279 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_InvalidUTF8(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_InvalidUTF8_m2679473473 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_ViolatePolicy(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_ViolatePolicy_m128123149 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_ExtensionNotMatch(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_ExtensionNotMatch_m3803409994 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_UnexpectedCondition(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_UnexpectedCondition_m1822824579 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_TLSHandshakeFailure(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_TLSHandshakeFailure_m3799478887 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocket4Net.Protocol.CloseStatusCodeHybi10::get_NoStatusCode()
extern "C"  int32_t CloseStatusCodeHybi10_get_NoStatusCode_m1773857912 (CloseStatusCodeHybi10_t635449120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.CloseStatusCodeHybi10::set_NoStatusCode(System.Int32)
extern "C"  void CloseStatusCodeHybi10_set_NoStatusCode_m3905822443 (CloseStatusCodeHybi10_t635449120 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
