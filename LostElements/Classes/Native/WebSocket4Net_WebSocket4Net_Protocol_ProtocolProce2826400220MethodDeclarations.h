﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.ProtocolProcessorFactory/<>c__DisplayClass3
struct U3CU3Ec__DisplayClass3_t2826400220;
// WebSocket4Net.Protocol.IProtocolProcessor
struct IProtocolProcessor_t4208156067;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocket4Net.Protocol.ProtocolProcessorFactory/<>c__DisplayClass3::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3__ctor_m3256459194 (U3CU3Ec__DisplayClass3_t2826400220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.Protocol.ProtocolProcessorFactory/<>c__DisplayClass3::<GetProcessorByVersion>b__2(WebSocket4Net.Protocol.IProtocolProcessor)
extern "C"  bool U3CU3Ec__DisplayClass3_U3CGetProcessorByVersionU3Eb__2_m229891044 (U3CU3Ec__DisplayClass3_t2826400220 * __this, Il2CppObject * ___p, const MethodInfo* method) IL2CPP_METHOD_ATTR;
