﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Attribute
struct Attribute_t498693649;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Object837106420.h"

// System.Attribute SimpleJson.Reflection.ReflectionUtils::GetAttribute(System.Reflection.MemberInfo,System.Type)
extern "C"  Attribute_t498693649 * ReflectionUtils_GetAttribute_m2472672625 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___info, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute SimpleJson.Reflection.ReflectionUtils::GetAttribute(System.Type,System.Type)
extern "C"  Attribute_t498693649 * ReflectionUtils_GetAttribute_m3007953666 (Il2CppObject * __this /* static, unused */, Type_t * ___objectType, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleJson.Reflection.ReflectionUtils::IsTypeGenericeCollectionInterface(System.Type)
extern "C"  bool ReflectionUtils_IsTypeGenericeCollectionInterface_m1283721406 (Il2CppObject * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleJson.Reflection.ReflectionUtils::IsTypeDictionary(System.Type)
extern "C"  bool ReflectionUtils_IsTypeDictionary_m3763562223 (Il2CppObject * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleJson.Reflection.ReflectionUtils::IsNullableType(System.Type)
extern "C"  bool ReflectionUtils_IsNullableType_m2201186244 (Il2CppObject * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJson.Reflection.ReflectionUtils::ToNullableType(System.Object,System.Type)
extern "C"  Il2CppObject * ReflectionUtils_ToNullableType_m4235305546 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj, Type_t * ___nullableType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
