﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Command.Ping
struct Ping_t3880049009;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketCommandInfo3536916738.h"

// System.Void WebSocket4Net.Command.Ping::ExecuteCommand(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo)
extern "C"  void Ping_ExecuteCommand_m3149555197 (Ping_t3880049009 * __this, WebSocket_t713846903 * ___session, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.Command.Ping::get_Name()
extern "C"  String_t* Ping_get_Name_m3923037265 (Ping_t3880049009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Command.Ping::.ctor()
extern "C"  void Ping__ctor_m2399566954 (Ping_t3880049009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
