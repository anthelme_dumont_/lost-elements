﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplashScreen/$Start$7
struct U24StartU247_t2379095985;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t2774239688;

#include "codegen/il2cpp-codegen.h"

// System.Void SplashScreen/$Start$7::.ctor()
extern "C"  void U24StartU247__ctor_m2695850825 (U24StartU247_t2379095985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> SplashScreen/$Start$7::GetEnumerator()
extern "C"  Il2CppObject* U24StartU247_GetEnumerator_m154303705 (U24StartU247_t2379095985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
