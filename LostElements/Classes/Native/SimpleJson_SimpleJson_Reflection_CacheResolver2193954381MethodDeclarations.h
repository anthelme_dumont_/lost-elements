﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.Reflection.CacheResolver
struct CacheResolver_t2193954381;
// SimpleJson.Reflection.MemberMapLoader
struct MemberMapLoader_t3073810362;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>
struct SafeDictionary_2_t2250466201;
// System.Reflection.Emit.DynamicMethod
struct DynamicMethod_t2778460557;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// SimpleJson.Reflection.GetHandler
struct GetHandler_t3377211385;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// SimpleJson.Reflection.SetHandler
struct SetHandler_t1019049325;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "SimpleJson_SimpleJson_Reflection_MemberMapLoader3073810362.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369.h"

// System.Void SimpleJson.Reflection.CacheResolver::.ctor(SimpleJson.Reflection.MemberMapLoader)
extern "C"  void CacheResolver__ctor_m3244435910 (CacheResolver_t2193954381 * __this, MemberMapLoader_t3073810362 * ___memberMapLoader, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJson.Reflection.CacheResolver::GetNewInstance(System.Type)
extern "C"  Il2CppObject * CacheResolver_GetNewInstance_m3764201201 (Il2CppObject * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap> SimpleJson.Reflection.CacheResolver::LoadMaps(System.Type)
extern "C"  SafeDictionary_2_t2250466201 * CacheResolver_LoadMaps_m2248807851 (CacheResolver_t2193954381 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.DynamicMethod SimpleJson.Reflection.CacheResolver::CreateDynamicMethod(System.String,System.Type,System.Type[],System.Type)
extern "C"  DynamicMethod_t2778460557 * CacheResolver_CreateDynamicMethod_m3348281029 (Il2CppObject * __this /* static, unused */, String_t* ___name, Type_t * ___returnType, TypeU5BU5D_t3431720054* ___parameterTypes, Type_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SimpleJson.Reflection.GetHandler SimpleJson.Reflection.CacheResolver::CreateGetHandler(System.Reflection.FieldInfo)
extern "C"  GetHandler_t3377211385 * CacheResolver_CreateGetHandler_m3079080210 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SimpleJson.Reflection.SetHandler SimpleJson.Reflection.CacheResolver::CreateSetHandler(System.Reflection.FieldInfo)
extern "C"  SetHandler_t1019049325 * CacheResolver_CreateSetHandler_m3885692458 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SimpleJson.Reflection.GetHandler SimpleJson.Reflection.CacheResolver::CreateGetHandler(System.Reflection.PropertyInfo)
extern "C"  GetHandler_t3377211385 * CacheResolver_CreateGetHandler_m383375893 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SimpleJson.Reflection.SetHandler SimpleJson.Reflection.CacheResolver::CreateSetHandler(System.Reflection.PropertyInfo)
extern "C"  SetHandler_t1019049325 * CacheResolver_CreateSetHandler_m4121802237 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJson.Reflection.CacheResolver::.cctor()
extern "C"  void CacheResolver__cctor_m4243496679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
