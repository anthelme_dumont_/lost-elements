﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.Reflection.GetHandler
struct GetHandler_t3377211385;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SimpleJson.Reflection.GetHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void GetHandler__ctor_m3294234494 (GetHandler_t3377211385 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJson.Reflection.GetHandler::Invoke(System.Object)
extern "C"  Il2CppObject * GetHandler_Invoke_m865915879 (GetHandler_t3377211385 * __this, Il2CppObject * ___source, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" Il2CppObject * pinvoke_delegate_wrapper_GetHandler_t3377211385(Il2CppObject* delegate, Il2CppObject * ___source);
// System.IAsyncResult SimpleJson.Reflection.GetHandler::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetHandler_BeginInvoke_m2440018781 (GetHandler_t3377211385 * __this, Il2CppObject * ___source, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJson.Reflection.GetHandler::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * GetHandler_EndInvoke_m1813626947 (GetHandler_t3377211385 * __this, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
