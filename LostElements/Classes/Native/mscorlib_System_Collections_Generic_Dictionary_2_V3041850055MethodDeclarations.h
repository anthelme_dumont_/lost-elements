﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1451594948MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2514533120(__this, ___dictionary, method) ((  void (*) (ValueCollection_t3041850055 *, Dictionary_2_t1119712961 *, const MethodInfo*))ValueCollection__ctor_m4177258586_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m781852978(__this, ___item, method) ((  void (*) (ValueCollection_t3041850055 *, Action_1_t3776982353 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1528225944_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4137973499(__this, method) ((  void (*) (ValueCollection_t3041850055 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2827278689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1762038580(__this, ___item, method) ((  bool (*) (ValueCollection_t3041850055 *, Action_1_t3776982353 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2015709838_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m454224089(__this, ___item, method) ((  bool (*) (ValueCollection_t3041850055 *, Action_1_t3776982353 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3578506931_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3689224585(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3041850055 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4041606511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2893732415(__this, ___array, ___index, method) ((  void (*) (ValueCollection_t3041850055 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1709700389_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2823678714(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3041850055 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2843387488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m37214439(__this, method) ((  bool (*) (ValueCollection_t3041850055 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m290885697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2118582599(__this, method) ((  bool (*) (ValueCollection_t3041850055 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1930063777_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3386594163(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3041850055 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1874108365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m839434055(__this, ___array, ___index, method) ((  void (*) (ValueCollection_t3041850055 *, Action_1U5BU5D_t273548876*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1735386657_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2748883434(__this, method) ((  Enumerator_t886740904  (*) (ValueCollection_t3041850055 *, const MethodInfo*))ValueCollection_GetEnumerator_m1204216004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::get_Count()
#define ValueCollection_get_Count_m1940499853(__this, method) ((  int32_t (*) (ValueCollection_t3041850055 *, const MethodInfo*))ValueCollection_get_Count_m2709231847_gshared)(__this, method)
