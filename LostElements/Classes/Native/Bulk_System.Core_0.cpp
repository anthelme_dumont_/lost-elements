﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2174318432;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// Mono.Security.Cryptography.SymmetricTransform
struct SymmetricTransform_t3854241867;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t839208017;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.InvalidTimeZoneException
struct InvalidTimeZoneException_t3180604084;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t795512515;
// System.Security.Cryptography.Aes
struct Aes_t1557549544;
// System.Security.Cryptography.AesManaged
struct AesManaged_t564372777;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t4226691419;
// System.Security.Cryptography.AesTransform
struct AesTransform_t2186883254;
// System.UInt32[]
struct UInt32U5BU5D_t2133601851;
// System.TimeZoneInfo
struct TimeZoneInfo_t4131446812;
// System.TimeZoneInfo/AdjustmentRule[]
struct AdjustmentRuleU5BU5D_t1713960724;
// System.TimeZoneInfo/AdjustmentRule
struct AdjustmentRule_t2290566697;
// System.Collections.Generic.List`1<System.TimeZoneInfo/AdjustmentRule>
struct List_1_t3087525666;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t1459299685;
// System.Collections.Generic.Dictionary`2<System.Int32,System.TimeZoneInfo/TimeType>
struct Dictionary_2_t2773072230;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct List_1_t3660159136;
// System.TimeZoneNotFoundException
struct TimeZoneNotFoundException_t3613219068;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Core_U3CModuleU3E86524790.h"
#include "System_Core_U3CModuleU3E86524790MethodDeclarations.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E3053238933MethodDeclarations.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24A335950518.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24A335950518MethodDeclarations.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242366141818.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242366141818MethodDeclarations.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242366142878.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242366142878MethodDeclarations.h"
#include "System_Core_Locale2281372282.h"
#include "System_Core_Locale2281372282MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder2049706641.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder2049706641MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_RandomNumber2174318432.h"
#include "mscorlib_System_Security_Cryptography_RandomNumber2174318432MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Void2779279689.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTr3854241866.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTr3854241866MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SymmetricAlgo839208017.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_Cryptographi3718270561MethodDeclarations.h"
#include "mscorlib_System_Math2778998461MethodDeclarations.h"
#include "mscorlib_System_Buffer482356213MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SymmetricAlgo839208017MethodDeclarations.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_Cryptographi3718270561.h"
#include "mscorlib_System_GC2776609905MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1091014741MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_CipherMode3203384231.h"
#include "mscorlib_System_Enum2778772662MethodDeclarations.h"
#include "mscorlib_System_Enum2778772662.h"
#include "mscorlib_System_NotImplementedException1091014741.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "mscorlib_System_ObjectDisposedException973246880MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException973246880.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode1724215917.h"
#include "System_Core_System_Action437523947.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "System_Core_System_InvalidTimeZoneException3180604084.h"
#include "System_Core_System_InvalidTimeZoneException3180604084MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "System_Core_System_Linq_Check3277941805.h"
#include "System_Core_System_Linq_Check3277941805MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_Fallback825887490.h"
#include "System_Core_System_Linq_Enumerable_Fallback825887490MethodDeclarations.h"
#include "System_Core_System_Linq_SortDirection2805156518.h"
#include "System_Core_System_Linq_SortDirection2805156518MethodDeclarations.h"
#include "System_Core_System_Runtime_CompilerServices_Extensi795512515.h"
#include "System_Core_System_Runtime_CompilerServices_Extensi795512515MethodDeclarations.h"
#include "mscorlib_System_Attribute498693649MethodDeclarations.h"
#include "System_Core_System_Security_Cryptography_Aes1557549544.h"
#include "System_Core_System_Security_Cryptography_Aes1557549544MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_KeySizes2111859404MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_KeySizes2111859404.h"
#include "System_Core_System_Security_Cryptography_AesManaged564372777.h"
#include "System_Core_System_Security_Cryptography_AesManaged564372777MethodDeclarations.h"
#include "System_Core_System_Security_Cryptography_AesTransf2186883254MethodDeclarations.h"
#include "System_Core_System_Security_Cryptography_AesTransf2186883254.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHe1695827251MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle3184214143.h"
#include "System_Core_System_TimeZoneInfo4131446812.h"
#include "System_Core_System_TimeZoneInfo4131446812MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "System.Core_ArrayTypes.h"
#include "System_Core_System_TimeZoneInfo_AdjustmentRule2290566697.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "System_Core_System_TimeZoneInfo_AdjustmentRule2290566697MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_IO_Path2029632748MethodDeclarations.h"
#include "System_Core_System_TimeZoneNotFoundException3613219068MethodDeclarations.h"
#include "System_Core_System_TimeZoneNotFoundException3613219068.h"
#include "mscorlib_System_IO_File2029342275MethodDeclarations.h"
#include "mscorlib_System_IO_FileStream1527309539.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_IO_FileStream1527309539MethodDeclarations.h"
#include "System_Core_System_TimeZoneInfo_TransitionTime1289309666MethodDeclarations.h"
#include "System_Core_System_TimeZoneInfo_TransitionTime1289309666.h"
#include "mscorlib_System_DateTimeKind3550648708.h"
#include "mscorlib_System_DayOfWeek4050023580.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_BitConverter3338308296MethodDeclarations.h"
#include "mscorlib_System_BitConverter3338308296.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3087525666MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22863200167MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1459299685.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2773072230.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3660159136.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3087525666.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22863200167.h"
#include "System_Core_System_TimeZoneInfo_TimeType2282261447.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2773072230MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3660159136MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1459299685MethodDeclarations.h"
#include "System_Core_System_TimeZoneInfo_TimeType2282261447MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$1024
extern "C" void U24ArrayTypeU241024_t335950519_marshal(const U24ArrayTypeU241024_t335950519& unmarshaled, U24ArrayTypeU241024_t335950519_marshaled& marshaled)
{
}
extern "C" void U24ArrayTypeU241024_t335950519_marshal_back(const U24ArrayTypeU241024_t335950519_marshaled& marshaled, U24ArrayTypeU241024_t335950519& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$1024
extern "C" void U24ArrayTypeU241024_t335950519_marshal_cleanup(U24ArrayTypeU241024_t335950519_marshaled& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$120
extern "C" void U24ArrayTypeU24120_t2366141819_marshal(const U24ArrayTypeU24120_t2366141819& unmarshaled, U24ArrayTypeU24120_t2366141819_marshaled& marshaled)
{
}
extern "C" void U24ArrayTypeU24120_t2366141819_marshal_back(const U24ArrayTypeU24120_t2366141819_marshaled& marshaled, U24ArrayTypeU24120_t2366141819& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$120
extern "C" void U24ArrayTypeU24120_t2366141819_marshal_cleanup(U24ArrayTypeU24120_t2366141819_marshaled& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$256
extern "C" void U24ArrayTypeU24256_t2366142879_marshal(const U24ArrayTypeU24256_t2366142879& unmarshaled, U24ArrayTypeU24256_t2366142879_marshaled& marshaled)
{
}
extern "C" void U24ArrayTypeU24256_t2366142879_marshal_back(const U24ArrayTypeU24256_t2366142879_marshaled& marshaled, U24ArrayTypeU24256_t2366142879& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$256
extern "C" void U24ArrayTypeU24256_t2366142879_marshal_cleanup(U24ArrayTypeU24256_t2366142879_marshaled& marshaled)
{
}
// System.String Locale::GetText(System.String)
extern "C"  String_t* Locale_GetText_m2031928403 (Il2CppObject * __this /* static, unused */, String_t* ___msg, const MethodInfo* method)
{
	{
		String_t* L_0 = ___msg;
		return L_0;
	}
}
// System.String Locale::GetText(System.String,System.Object[])
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Locale_GetText_m2138526911_MetadataUsageId;
extern "C"  String_t* Locale_GetText_m2138526911 (Il2CppObject * __this /* static, unused */, String_t* ___fmt, ObjectU5BU5D_t11523773* ___args, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Locale_GetText_m2138526911_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___fmt;
		ObjectU5BU5D_t11523773* L_1 = ___args;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m4050103162(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.KeyBuilder::get_Rng()
extern TypeInfo* KeyBuilder_t2049706642_il2cpp_TypeInfo_var;
extern const uint32_t KeyBuilder_get_Rng_m1224245225_MetadataUsageId;
extern "C"  RandomNumberGenerator_t2174318432 * KeyBuilder_get_Rng_m1224245225 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyBuilder_get_Rng_m1224245225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RandomNumberGenerator_t2174318432 * L_0 = ((KeyBuilder_t2049706642_StaticFields*)KeyBuilder_t2049706642_il2cpp_TypeInfo_var->static_fields)->get_rng_0();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		RandomNumberGenerator_t2174318432 * L_1 = RandomNumberGenerator_Create_m2029084057(NULL /*static, unused*/, /*hidden argument*/NULL);
		((KeyBuilder_t2049706642_StaticFields*)KeyBuilder_t2049706642_il2cpp_TypeInfo_var->static_fields)->set_rng_0(L_1);
	}

IL_0014:
	{
		RandomNumberGenerator_t2174318432 * L_2 = ((KeyBuilder_t2049706642_StaticFields*)KeyBuilder_t2049706642_il2cpp_TypeInfo_var->static_fields)->get_rng_0();
		return L_2;
	}
}
// System.Byte[] Mono.Security.Cryptography.KeyBuilder::Key(System.Int32)
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern const uint32_t KeyBuilder_Key_m489708312_MetadataUsageId;
extern "C"  ByteU5BU5D_t58506160* KeyBuilder_Key_m489708312 (Il2CppObject * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyBuilder_Key_m489708312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t58506160* V_0 = NULL;
	{
		int32_t L_0 = ___size;
		V_0 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_0));
		RandomNumberGenerator_t2174318432 * L_1 = KeyBuilder_get_Rng_m1224245225(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_2 = V_0;
		NullCheck(L_1);
		VirtActionInvoker1< ByteU5BU5D_t58506160* >::Invoke(4 /* System.Void System.Security.Cryptography.RandomNumberGenerator::GetBytes(System.Byte[]) */, L_1, L_2);
		ByteU5BU5D_t58506160* L_3 = V_0;
		return L_3;
	}
}
// System.Byte[] Mono.Security.Cryptography.KeyBuilder::IV(System.Int32)
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern const uint32_t KeyBuilder_IV_m3901719576_MetadataUsageId;
extern "C"  ByteU5BU5D_t58506160* KeyBuilder_IV_m3901719576 (Il2CppObject * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyBuilder_IV_m3901719576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t58506160* V_0 = NULL;
	{
		int32_t L_0 = ___size;
		V_0 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_0));
		RandomNumberGenerator_t2174318432 * L_1 = KeyBuilder_get_Rng_m1224245225(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_2 = V_0;
		NullCheck(L_1);
		VirtActionInvoker1< ByteU5BU5D_t58506160* >::Invoke(4 /* System.Void System.Security.Cryptography.RandomNumberGenerator::GetBytes(System.Byte[]) */, L_1, L_2);
		ByteU5BU5D_t58506160* L_3 = V_0;
		return L_3;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::.ctor(System.Security.Cryptography.SymmetricAlgorithm,System.Boolean,System.Byte[])
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicException_t3718270561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral717243593;
extern const uint32_t SymmetricTransform__ctor_m1205754880_MetadataUsageId;
extern "C"  void SymmetricTransform__ctor_m1205754880 (SymmetricTransform_t3854241867 * __this, SymmetricAlgorithm_t839208017 * ___symmAlgo, bool ___encryption, ByteU5BU5D_t58506160* ___rgbIV, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform__ctor_m1205754880_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		SymmetricAlgorithm_t839208017 * L_0 = ___symmAlgo;
		__this->set_algo_0(L_0);
		bool L_1 = ___encryption;
		__this->set_encrypt_1(L_1);
		SymmetricAlgorithm_t839208017 * L_2 = __this->get_algo_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Security.Cryptography.SymmetricAlgorithm::get_BlockSize() */, L_2);
		__this->set_BlockSizeByte_2(((int32_t)((int32_t)L_3>>(int32_t)3)));
		ByteU5BU5D_t58506160* L_4 = ___rgbIV;
		if (L_4)
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_5 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t58506160* L_6 = KeyBuilder_IV_m3901719576(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		___rgbIV = L_6;
		goto IL_004c;
	}

IL_003f:
	{
		ByteU5BU5D_t58506160* L_7 = ___rgbIV;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_7);
		Il2CppObject * L_8 = VirtFuncInvoker0< Il2CppObject * >::Invoke(5 /* System.Object System.Array::Clone() */, (Il2CppArray *)(Il2CppArray *)L_7);
		___rgbIV = ((ByteU5BU5D_t58506160*)Castclass(L_8, ByteU5BU5D_t58506160_il2cpp_TypeInfo_var));
	}

IL_004c:
	{
		ByteU5BU5D_t58506160* L_9 = ___rgbIV;
		NullCheck(L_9);
		int32_t L_10 = __this->get_BlockSizeByte_2();
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))) >= ((int32_t)L_10)))
		{
			goto IL_008b;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_11 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		ByteU5BU5D_t58506160* L_12 = ___rgbIV;
		NullCheck(L_12);
		int32_t L_13 = (((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))));
		Il2CppObject * L_14 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = L_11;
		int32_t L_16 = __this->get_BlockSizeByte_2();
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_18);
		String_t* L_19 = Locale_GetText_m2138526911(NULL /*static, unused*/, _stringLiteral717243593, L_15, /*hidden argument*/NULL);
		V_0 = L_19;
		String_t* L_20 = V_0;
		CryptographicException_t3718270561 * L_21 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m2400722889(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_008b:
	{
		int32_t L_22 = __this->get_BlockSizeByte_2();
		__this->set_temp_3(((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_22)));
		ByteU5BU5D_t58506160* L_23 = ___rgbIV;
		ByteU5BU5D_t58506160* L_24 = __this->get_temp_3();
		int32_t L_25 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t58506160* L_26 = ___rgbIV;
		NullCheck(L_26);
		int32_t L_27 = Math_Min_m811624909(NULL /*static, unused*/, L_25, (((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length)))), /*hidden argument*/NULL);
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_23, 0, (Il2CppArray *)(Il2CppArray *)L_24, 0, L_27, /*hidden argument*/NULL);
		int32_t L_28 = __this->get_BlockSizeByte_2();
		__this->set_temp2_4(((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_28)));
		SymmetricAlgorithm_t839208017 * L_29 = __this->get_algo_0();
		NullCheck(L_29);
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Security.Cryptography.SymmetricAlgorithm::get_FeedbackSize() */, L_29);
		__this->set_FeedBackByte_7(((int32_t)((int32_t)L_30>>(int32_t)3)));
		int32_t L_31 = __this->get_FeedBackByte_7();
		if (!L_31)
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_32 = __this->get_BlockSizeByte_2();
		int32_t L_33 = __this->get_FeedBackByte_7();
		__this->set_FeedBackIter_8(((int32_t)((int32_t)L_32/(int32_t)L_33)));
	}

IL_00fa:
	{
		int32_t L_34 = __this->get_BlockSizeByte_2();
		__this->set_workBuff_5(((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_34)));
		int32_t L_35 = __this->get_BlockSizeByte_2();
		__this->set_workout_6(((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_35)));
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::System.IDisposable.Dispose()
extern "C"  void SymmetricTransform_System_IDisposable_Dispose_m455424824 (SymmetricTransform_t3854241867 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(8 /* System.Void Mono.Security.Cryptography.SymmetricTransform::Dispose(System.Boolean) */, __this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::Finalize()
extern "C"  void SymmetricTransform_Finalize_m1432383547 (SymmetricTransform_t3854241867 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(8 /* System.Void Mono.Security.Cryptography.SymmetricTransform::Dispose(System.Boolean) */, __this, (bool)0);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::Dispose(System.Boolean)
extern "C"  void SymmetricTransform_Dispose_m2730879931 (SymmetricTransform_t3854241867 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_disposed_9();
		if (L_0)
		{
			goto IL_004a;
		}
	}
	{
		bool L_1 = ___disposing;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		ByteU5BU5D_t58506160* L_2 = __this->get_temp_3();
		int32_t L_3 = __this->get_BlockSizeByte_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, 0, L_3, /*hidden argument*/NULL);
		__this->set_temp_3((ByteU5BU5D_t58506160*)NULL);
		ByteU5BU5D_t58506160* L_4 = __this->get_temp2_4();
		int32_t L_5 = __this->get_BlockSizeByte_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, 0, L_5, /*hidden argument*/NULL);
		__this->set_temp2_4((ByteU5BU5D_t58506160*)NULL);
	}

IL_0043:
	{
		__this->set_m_disposed_9((bool)1);
	}

IL_004a:
	{
		return;
	}
}
// System.Boolean Mono.Security.Cryptography.SymmetricTransform::get_CanReuseTransform()
extern "C"  bool SymmetricTransform_get_CanReuseTransform_m725989976 (SymmetricTransform_t3854241867 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::Transform(System.Byte[],System.Byte[])
extern TypeInfo* CipherMode_t3203384231_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1591853210;
extern const uint32_t SymmetricTransform_Transform_m3903574043_MetadataUsageId;
extern "C"  void SymmetricTransform_Transform_m3903574043 (SymmetricTransform_t3854241867 * __this, ByteU5BU5D_t58506160* ___input, ByteU5BU5D_t58506160* ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_Transform_m3903574043_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		SymmetricAlgorithm_t839208017 * L_0 = __this->get_algo_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(16 /* System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::get_Mode() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 0)
		{
			goto IL_003a;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 1)
		{
			goto IL_002d;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 2)
		{
			goto IL_0054;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 3)
		{
			goto IL_0047;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 4)
		{
			goto IL_0061;
		}
	}
	{
		goto IL_006e;
	}

IL_002d:
	{
		ByteU5BU5D_t58506160* L_3 = ___input;
		ByteU5BU5D_t58506160* L_4 = ___output;
		VirtActionInvoker2< ByteU5BU5D_t58506160*, ByteU5BU5D_t58506160* >::Invoke(11 /* System.Void Mono.Security.Cryptography.SymmetricTransform::ECB(System.Byte[],System.Byte[]) */, __this, L_3, L_4);
		goto IL_0093;
	}

IL_003a:
	{
		ByteU5BU5D_t58506160* L_5 = ___input;
		ByteU5BU5D_t58506160* L_6 = ___output;
		VirtActionInvoker2< ByteU5BU5D_t58506160*, ByteU5BU5D_t58506160* >::Invoke(12 /* System.Void Mono.Security.Cryptography.SymmetricTransform::CBC(System.Byte[],System.Byte[]) */, __this, L_5, L_6);
		goto IL_0093;
	}

IL_0047:
	{
		ByteU5BU5D_t58506160* L_7 = ___input;
		ByteU5BU5D_t58506160* L_8 = ___output;
		VirtActionInvoker2< ByteU5BU5D_t58506160*, ByteU5BU5D_t58506160* >::Invoke(13 /* System.Void Mono.Security.Cryptography.SymmetricTransform::CFB(System.Byte[],System.Byte[]) */, __this, L_7, L_8);
		goto IL_0093;
	}

IL_0054:
	{
		ByteU5BU5D_t58506160* L_9 = ___input;
		ByteU5BU5D_t58506160* L_10 = ___output;
		VirtActionInvoker2< ByteU5BU5D_t58506160*, ByteU5BU5D_t58506160* >::Invoke(14 /* System.Void Mono.Security.Cryptography.SymmetricTransform::OFB(System.Byte[],System.Byte[]) */, __this, L_9, L_10);
		goto IL_0093;
	}

IL_0061:
	{
		ByteU5BU5D_t58506160* L_11 = ___input;
		ByteU5BU5D_t58506160* L_12 = ___output;
		VirtActionInvoker2< ByteU5BU5D_t58506160*, ByteU5BU5D_t58506160* >::Invoke(15 /* System.Void Mono.Security.Cryptography.SymmetricTransform::CTS(System.Byte[],System.Byte[]) */, __this, L_11, L_12);
		goto IL_0093;
	}

IL_006e:
	{
		SymmetricAlgorithm_t839208017 * L_13 = __this->get_algo_0();
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(16 /* System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::get_Mode() */, L_13);
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(CipherMode_t3203384231_il2cpp_TypeInfo_var, &L_15);
		NullCheck((Enum_t2778772662 *)L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2778772662 *)L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1591853210, L_17, /*hidden argument*/NULL);
		NotImplementedException_t1091014741 * L_19 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_19, L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_0093:
	{
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::CBC(System.Byte[],System.Byte[])
extern "C"  void SymmetricTransform_CBC_m2217465763 (SymmetricTransform_t3854241867 * __this, ByteU5BU5D_t58506160* ___input, ByteU5BU5D_t58506160* ___output, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = __this->get_encrypt_1();
		if (!L_0)
		{
			goto IL_005c;
		}
	}
	{
		V_0 = 0;
		goto IL_002a;
	}

IL_0012:
	{
		ByteU5BU5D_t58506160* L_1 = __this->get_temp_3();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		uint8_t* L_3 = ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)));
		ByteU5BU5D_t58506160* L_4 = ___input;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		*((int8_t*)(L_3)) = (int8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)(*((uint8_t*)L_3))^(int32_t)((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6))))))));
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0012;
		}
	}
	{
		ByteU5BU5D_t58506160* L_10 = __this->get_temp_3();
		ByteU5BU5D_t58506160* L_11 = ___output;
		VirtActionInvoker2< ByteU5BU5D_t58506160*, ByteU5BU5D_t58506160* >::Invoke(11 /* System.Void Mono.Security.Cryptography.SymmetricTransform::ECB(System.Byte[],System.Byte[]) */, __this, L_10, L_11);
		ByteU5BU5D_t58506160* L_12 = ___output;
		ByteU5BU5D_t58506160* L_13 = __this->get_temp_3();
		int32_t L_14 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_12, 0, (Il2CppArray *)(Il2CppArray *)L_13, 0, L_14, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_005c:
	{
		ByteU5BU5D_t58506160* L_15 = ___input;
		ByteU5BU5D_t58506160* L_16 = __this->get_temp2_4();
		int32_t L_17 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, 0, (Il2CppArray *)(Il2CppArray *)L_16, 0, L_17, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_18 = ___input;
		ByteU5BU5D_t58506160* L_19 = ___output;
		VirtActionInvoker2< ByteU5BU5D_t58506160*, ByteU5BU5D_t58506160* >::Invoke(11 /* System.Void Mono.Security.Cryptography.SymmetricTransform::ECB(System.Byte[],System.Byte[]) */, __this, L_18, L_19);
		V_1 = 0;
		goto IL_0097;
	}

IL_007f:
	{
		ByteU5BU5D_t58506160* L_20 = ___output;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		uint8_t* L_22 = ((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21)));
		ByteU5BU5D_t58506160* L_23 = __this->get_temp_3();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		*((int8_t*)(L_22)) = (int8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)(*((uint8_t*)L_22))^(int32_t)((L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25))))))));
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_0097:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_007f;
		}
	}
	{
		ByteU5BU5D_t58506160* L_29 = __this->get_temp2_4();
		ByteU5BU5D_t58506160* L_30 = __this->get_temp_3();
		int32_t L_31 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_29, 0, (Il2CppArray *)(Il2CppArray *)L_30, 0, L_31, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::CFB(System.Byte[],System.Byte[])
extern "C"  void SymmetricTransform_CFB_m3609349384 (SymmetricTransform_t3854241867 * __this, ByteU5BU5D_t58506160* ___input, ByteU5BU5D_t58506160* ___output, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get_encrypt_1();
		if (!L_0)
		{
			goto IL_00a9;
		}
	}
	{
		V_0 = 0;
		goto IL_0098;
	}

IL_0012:
	{
		ByteU5BU5D_t58506160* L_1 = __this->get_temp_3();
		ByteU5BU5D_t58506160* L_2 = __this->get_temp2_4();
		VirtActionInvoker2< ByteU5BU5D_t58506160*, ByteU5BU5D_t58506160* >::Invoke(11 /* System.Void Mono.Security.Cryptography.SymmetricTransform::ECB(System.Byte[],System.Byte[]) */, __this, L_1, L_2);
		V_1 = 0;
		goto IL_0043;
	}

IL_002b:
	{
		ByteU5BU5D_t58506160* L_3 = ___output;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		ByteU5BU5D_t58506160* L_6 = __this->get_temp2_4();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		ByteU5BU5D_t58506160* L_9 = ___input;
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10+(int32_t)L_11)));
		int32_t L_12 = ((int32_t)((int32_t)L_10+(int32_t)L_11));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, ((int32_t)((int32_t)L_4+(int32_t)L_5)));
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_4+(int32_t)L_5))), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8)))^(int32_t)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_12)))))))));
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_14 = V_1;
		int32_t L_15 = __this->get_FeedBackByte_7();
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_002b;
		}
	}
	{
		ByteU5BU5D_t58506160* L_16 = __this->get_temp_3();
		int32_t L_17 = __this->get_FeedBackByte_7();
		ByteU5BU5D_t58506160* L_18 = __this->get_temp_3();
		int32_t L_19 = __this->get_BlockSizeByte_2();
		int32_t L_20 = __this->get_FeedBackByte_7();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_16, L_17, (Il2CppArray *)(Il2CppArray *)L_18, 0, ((int32_t)((int32_t)L_19-(int32_t)L_20)), /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_21 = ___output;
		int32_t L_22 = V_0;
		ByteU5BU5D_t58506160* L_23 = __this->get_temp_3();
		int32_t L_24 = __this->get_BlockSizeByte_2();
		int32_t L_25 = __this->get_FeedBackByte_7();
		int32_t L_26 = __this->get_FeedBackByte_7();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_21, L_22, (Il2CppArray *)(Il2CppArray *)L_23, ((int32_t)((int32_t)L_24-(int32_t)L_25)), L_26, /*hidden argument*/NULL);
		int32_t L_27 = V_0;
		V_0 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0098:
	{
		int32_t L_28 = V_0;
		int32_t L_29 = __this->get_FeedBackIter_8();
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0012;
		}
	}
	{
		goto IL_0150;
	}

IL_00a9:
	{
		V_2 = 0;
		goto IL_0144;
	}

IL_00b0:
	{
		__this->set_encrypt_1((bool)1);
		ByteU5BU5D_t58506160* L_30 = __this->get_temp_3();
		ByteU5BU5D_t58506160* L_31 = __this->get_temp2_4();
		VirtActionInvoker2< ByteU5BU5D_t58506160*, ByteU5BU5D_t58506160* >::Invoke(11 /* System.Void Mono.Security.Cryptography.SymmetricTransform::ECB(System.Byte[],System.Byte[]) */, __this, L_30, L_31);
		__this->set_encrypt_1((bool)0);
		ByteU5BU5D_t58506160* L_32 = __this->get_temp_3();
		int32_t L_33 = __this->get_FeedBackByte_7();
		ByteU5BU5D_t58506160* L_34 = __this->get_temp_3();
		int32_t L_35 = __this->get_BlockSizeByte_2();
		int32_t L_36 = __this->get_FeedBackByte_7();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_32, L_33, (Il2CppArray *)(Il2CppArray *)L_34, 0, ((int32_t)((int32_t)L_35-(int32_t)L_36)), /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_37 = ___input;
		int32_t L_38 = V_2;
		ByteU5BU5D_t58506160* L_39 = __this->get_temp_3();
		int32_t L_40 = __this->get_BlockSizeByte_2();
		int32_t L_41 = __this->get_FeedBackByte_7();
		int32_t L_42 = __this->get_FeedBackByte_7();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_37, L_38, (Il2CppArray *)(Il2CppArray *)L_39, ((int32_t)((int32_t)L_40-(int32_t)L_41)), L_42, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_0134;
	}

IL_011c:
	{
		ByteU5BU5D_t58506160* L_43 = ___output;
		int32_t L_44 = V_3;
		int32_t L_45 = V_2;
		ByteU5BU5D_t58506160* L_46 = __this->get_temp2_4();
		int32_t L_47 = V_3;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		int32_t L_48 = L_47;
		ByteU5BU5D_t58506160* L_49 = ___input;
		int32_t L_50 = V_3;
		int32_t L_51 = V_2;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, ((int32_t)((int32_t)L_50+(int32_t)L_51)));
		int32_t L_52 = ((int32_t)((int32_t)L_50+(int32_t)L_51));
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)((int32_t)L_44+(int32_t)L_45)));
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_44+(int32_t)L_45))), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48)))^(int32_t)((L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_52)))))))));
		int32_t L_53 = V_3;
		V_3 = ((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_0134:
	{
		int32_t L_54 = V_3;
		int32_t L_55 = __this->get_FeedBackByte_7();
		if ((((int32_t)L_54) < ((int32_t)L_55)))
		{
			goto IL_011c;
		}
	}
	{
		int32_t L_56 = V_2;
		V_2 = ((int32_t)((int32_t)L_56+(int32_t)1));
	}

IL_0144:
	{
		int32_t L_57 = V_2;
		int32_t L_58 = __this->get_FeedBackIter_8();
		if ((((int32_t)L_57) < ((int32_t)L_58)))
		{
			goto IL_00b0;
		}
	}

IL_0150:
	{
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::OFB(System.Byte[],System.Byte[])
extern TypeInfo* CryptographicException_t3718270561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral557596476;
extern const uint32_t SymmetricTransform_OFB_m962939772_MetadataUsageId;
extern "C"  void SymmetricTransform_OFB_m962939772 (SymmetricTransform_t3854241867 * __this, ByteU5BU5D_t58506160* ___input, ByteU5BU5D_t58506160* ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_OFB_m962939772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CryptographicException_t3718270561 * L_0 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m2400722889(L_0, _stringLiteral557596476, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::CTS(System.Byte[],System.Byte[])
extern TypeInfo* CryptographicException_t3718270561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1564220773;
extern const uint32_t SymmetricTransform_CTS_m122988069_MetadataUsageId;
extern "C"  void SymmetricTransform_CTS_m122988069 (SymmetricTransform_t3854241867 * __this, ByteU5BU5D_t58506160* ___input, ByteU5BU5D_t58506160* ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_CTS_m122988069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CryptographicException_t3718270561 * L_0 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m2400722889(L_0, _stringLiteral1564220773, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::CheckInput(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3502856362;
extern Il2CppCodeGenString* _stringLiteral3861195005;
extern Il2CppCodeGenString* _stringLiteral58700;
extern Il2CppCodeGenString* _stringLiteral1360680805;
extern Il2CppCodeGenString* _stringLiteral594286626;
extern const uint32_t SymmetricTransform_CheckInput_m3506478922_MetadataUsageId;
extern "C"  void SymmetricTransform_CheckInput_m3506478922 (SymmetricTransform_t3854241867 * __this, ByteU5BU5D_t58506160* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_CheckInput_m3506478922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t58506160* L_0 = ___inputBuffer;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3502856362, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___inputOffset;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_3, _stringLiteral3861195005, _stringLiteral58700, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		int32_t L_4 = ___inputCount;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_5 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_5, _stringLiteral1360680805, _stringLiteral58700, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_003f:
	{
		int32_t L_6 = ___inputOffset;
		ByteU5BU5D_t58506160* L_7 = ___inputBuffer;
		NullCheck(L_7);
		int32_t L_8 = ___inputCount;
		if ((((int32_t)L_6) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))-(int32_t)L_8)))))
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_9 = Locale_GetText_m2031928403(NULL /*static, unused*/, _stringLiteral594286626, /*hidden argument*/NULL);
		ArgumentException_t124305799 * L_10 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_10, _stringLiteral3502856362, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_005f:
	{
		return;
	}
}
// System.Int32 Mono.Security.Cryptography.SymmetricTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern TypeInfo* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicException_t3718270561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral783533338;
extern Il2CppCodeGenString* _stringLiteral3678475425;
extern Il2CppCodeGenString* _stringLiteral4036814068;
extern Il2CppCodeGenString* _stringLiteral58700;
extern Il2CppCodeGenString* _stringLiteral594286626;
extern const uint32_t SymmetricTransform_TransformBlock_m1944043099_MetadataUsageId;
extern "C"  int32_t SymmetricTransform_TransformBlock_m1944043099 (SymmetricTransform_t3854241867 * __this, ByteU5BU5D_t58506160* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, ByteU5BU5D_t58506160* ___outputBuffer, int32_t ___outputOffset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_TransformBlock_m1944043099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_disposed_9();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, _stringLiteral783533338, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		ByteU5BU5D_t58506160* L_2 = ___inputBuffer;
		int32_t L_3 = ___inputOffset;
		int32_t L_4 = ___inputCount;
		SymmetricTransform_CheckInput_m3506478922(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_5 = ___outputBuffer;
		if (L_5)
		{
			goto IL_0031;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_6 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_6, _stringLiteral3678475425, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0031:
	{
		int32_t L_7 = ___outputOffset;
		if ((((int32_t)L_7) >= ((int32_t)0)))
		{
			goto IL_0049;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_8 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_8, _stringLiteral4036814068, _stringLiteral58700, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0049:
	{
		ByteU5BU5D_t58506160* L_9 = ___outputBuffer;
		NullCheck(L_9);
		int32_t L_10 = ___inputCount;
		int32_t L_11 = ___outputOffset;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))-(int32_t)L_11));
		bool L_12 = __this->get_encrypt_1();
		if (L_12)
		{
			goto IL_009c;
		}
	}
	{
		int32_t L_13 = V_0;
		if ((((int32_t)0) <= ((int32_t)L_13)))
		{
			goto IL_009c;
		}
	}
	{
		SymmetricAlgorithm_t839208017 * L_14 = __this->get_algo_0();
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_14);
		if ((((int32_t)L_15) == ((int32_t)1)))
		{
			goto IL_0087;
		}
	}
	{
		SymmetricAlgorithm_t839208017 * L_16 = __this->get_algo_0();
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_16);
		if ((!(((uint32_t)L_17) == ((uint32_t)3))))
		{
			goto IL_009c;
		}
	}

IL_0087:
	{
		String_t* L_18 = Locale_GetText_m2031928403(NULL /*static, unused*/, _stringLiteral594286626, /*hidden argument*/NULL);
		CryptographicException_t3718270561 * L_19 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1958048837(L_19, _stringLiteral3678475425, L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_009c:
	{
		bool L_20 = SymmetricTransform_get_KeepLastBlock_m2173858210(__this, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00cf;
		}
	}
	{
		int32_t L_21 = V_0;
		int32_t L_22 = __this->get_BlockSizeByte_2();
		if ((((int32_t)0) <= ((int32_t)((int32_t)((int32_t)L_21+(int32_t)L_22)))))
		{
			goto IL_00ca;
		}
	}
	{
		String_t* L_23 = Locale_GetText_m2031928403(NULL /*static, unused*/, _stringLiteral594286626, /*hidden argument*/NULL);
		CryptographicException_t3718270561 * L_24 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1958048837(L_24, _stringLiteral3678475425, L_23, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_00ca:
	{
		goto IL_010e;
	}

IL_00cf:
	{
		int32_t L_25 = V_0;
		if ((((int32_t)0) <= ((int32_t)L_25)))
		{
			goto IL_010e;
		}
	}
	{
		ByteU5BU5D_t58506160* L_26 = ___inputBuffer;
		NullCheck(L_26);
		int32_t L_27 = ___inputOffset;
		ByteU5BU5D_t58506160* L_28 = ___outputBuffer;
		NullCheck(L_28);
		int32_t L_29 = __this->get_BlockSizeByte_2();
		if ((!(((uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))))-(int32_t)L_27))-(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length))))))) == ((uint32_t)L_29))))
		{
			goto IL_00f9;
		}
	}
	{
		ByteU5BU5D_t58506160* L_30 = ___outputBuffer;
		NullCheck(L_30);
		int32_t L_31 = ___outputOffset;
		___inputCount = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))-(int32_t)L_31));
		goto IL_010e;
	}

IL_00f9:
	{
		String_t* L_32 = Locale_GetText_m2031928403(NULL /*static, unused*/, _stringLiteral594286626, /*hidden argument*/NULL);
		CryptographicException_t3718270561 * L_33 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1958048837(L_33, _stringLiteral3678475425, L_32, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_010e:
	{
		ByteU5BU5D_t58506160* L_34 = ___inputBuffer;
		int32_t L_35 = ___inputOffset;
		int32_t L_36 = ___inputCount;
		ByteU5BU5D_t58506160* L_37 = ___outputBuffer;
		int32_t L_38 = ___outputOffset;
		int32_t L_39 = SymmetricTransform_InternalTransformBlock_m3380491768(__this, L_34, L_35, L_36, L_37, L_38, /*hidden argument*/NULL);
		return L_39;
	}
}
// System.Boolean Mono.Security.Cryptography.SymmetricTransform::get_KeepLastBlock()
extern "C"  bool SymmetricTransform_get_KeepLastBlock_m2173858210 (SymmetricTransform_t3854241867 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		bool L_0 = __this->get_encrypt_1();
		if (L_0)
		{
			goto IL_002f;
		}
	}
	{
		SymmetricAlgorithm_t839208017 * L_1 = __this->get_algo_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_1);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_002f;
		}
	}
	{
		SymmetricAlgorithm_t839208017 * L_3 = __this->get_algo_0();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_3);
		G_B4_0 = ((((int32_t)((((int32_t)L_4) == ((int32_t)3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0030;
	}

IL_002f:
	{
		G_B4_0 = 0;
	}

IL_0030:
	{
		return (bool)G_B4_0;
	}
}
// System.Int32 Mono.Security.Cryptography.SymmetricTransform::InternalTransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern TypeInfo* CryptographicException_t3718270561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3566855579;
extern const uint32_t SymmetricTransform_InternalTransformBlock_m3380491768_MetadataUsageId;
extern "C"  int32_t SymmetricTransform_InternalTransformBlock_m3380491768 (SymmetricTransform_t3854241867 * __this, ByteU5BU5D_t58506160* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, ByteU5BU5D_t58506160* ___outputBuffer, int32_t ___outputOffset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_InternalTransformBlock_m3380491768_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = ___inputOffset;
		V_0 = L_0;
		int32_t L_1 = ___inputCount;
		int32_t L_2 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_3 = ___inputCount;
		int32_t L_4 = __this->get_BlockSizeByte_2();
		if (!((int32_t)((int32_t)L_3%(int32_t)L_4)))
		{
			goto IL_0026;
		}
	}
	{
		CryptographicException_t3718270561 * L_5 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m2400722889(L_5, _stringLiteral3566855579, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = ___inputCount;
		int32_t L_7 = __this->get_BlockSizeByte_2();
		V_1 = ((int32_t)((int32_t)L_6/(int32_t)L_7));
		goto IL_0036;
	}

IL_0034:
	{
		V_1 = 1;
	}

IL_0036:
	{
		bool L_8 = SymmetricTransform_get_KeepLastBlock_m2173858210(__this, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_0045:
	{
		V_2 = 0;
		bool L_10 = __this->get_lastBlock_10();
		if (!L_10)
		{
			goto IL_0095;
		}
	}
	{
		ByteU5BU5D_t58506160* L_11 = __this->get_workBuff_5();
		ByteU5BU5D_t58506160* L_12 = __this->get_workout_6();
		VirtActionInvoker2< ByteU5BU5D_t58506160*, ByteU5BU5D_t58506160* >::Invoke(10 /* System.Void Mono.Security.Cryptography.SymmetricTransform::Transform(System.Byte[],System.Byte[]) */, __this, L_11, L_12);
		ByteU5BU5D_t58506160* L_13 = __this->get_workout_6();
		ByteU5BU5D_t58506160* L_14 = ___outputBuffer;
		int32_t L_15 = ___outputOffset;
		int32_t L_16 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_13, 0, (Il2CppArray *)(Il2CppArray *)L_14, L_15, L_16, /*hidden argument*/NULL);
		int32_t L_17 = ___outputOffset;
		int32_t L_18 = __this->get_BlockSizeByte_2();
		___outputOffset = ((int32_t)((int32_t)L_17+(int32_t)L_18));
		int32_t L_19 = V_2;
		int32_t L_20 = __this->get_BlockSizeByte_2();
		V_2 = ((int32_t)((int32_t)L_19+(int32_t)L_20));
		__this->set_lastBlock_10((bool)0);
	}

IL_0095:
	{
		V_3 = 0;
		goto IL_00f9;
	}

IL_009c:
	{
		ByteU5BU5D_t58506160* L_21 = ___inputBuffer;
		int32_t L_22 = V_0;
		ByteU5BU5D_t58506160* L_23 = __this->get_workBuff_5();
		int32_t L_24 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_21, L_22, (Il2CppArray *)(Il2CppArray *)L_23, 0, L_24, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_25 = __this->get_workBuff_5();
		ByteU5BU5D_t58506160* L_26 = __this->get_workout_6();
		VirtActionInvoker2< ByteU5BU5D_t58506160*, ByteU5BU5D_t58506160* >::Invoke(10 /* System.Void Mono.Security.Cryptography.SymmetricTransform::Transform(System.Byte[],System.Byte[]) */, __this, L_25, L_26);
		ByteU5BU5D_t58506160* L_27 = __this->get_workout_6();
		ByteU5BU5D_t58506160* L_28 = ___outputBuffer;
		int32_t L_29 = ___outputOffset;
		int32_t L_30 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_27, 0, (Il2CppArray *)(Il2CppArray *)L_28, L_29, L_30, /*hidden argument*/NULL);
		int32_t L_31 = V_0;
		int32_t L_32 = __this->get_BlockSizeByte_2();
		V_0 = ((int32_t)((int32_t)L_31+(int32_t)L_32));
		int32_t L_33 = ___outputOffset;
		int32_t L_34 = __this->get_BlockSizeByte_2();
		___outputOffset = ((int32_t)((int32_t)L_33+(int32_t)L_34));
		int32_t L_35 = V_2;
		int32_t L_36 = __this->get_BlockSizeByte_2();
		V_2 = ((int32_t)((int32_t)L_35+(int32_t)L_36));
		int32_t L_37 = V_3;
		V_3 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00f9:
	{
		int32_t L_38 = V_3;
		int32_t L_39 = V_1;
		if ((((int32_t)L_38) < ((int32_t)L_39)))
		{
			goto IL_009c;
		}
	}
	{
		bool L_40 = SymmetricTransform_get_KeepLastBlock_m2173858210(__this, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0126;
		}
	}
	{
		ByteU5BU5D_t58506160* L_41 = ___inputBuffer;
		int32_t L_42 = V_0;
		ByteU5BU5D_t58506160* L_43 = __this->get_workBuff_5();
		int32_t L_44 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_41, L_42, (Il2CppArray *)(Il2CppArray *)L_43, 0, L_44, /*hidden argument*/NULL);
		__this->set_lastBlock_10((bool)1);
	}

IL_0126:
	{
		int32_t L_45 = V_2;
		return L_45;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::Random(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern const uint32_t SymmetricTransform_Random_m3415780681_MetadataUsageId;
extern "C"  void SymmetricTransform_Random_m3415780681 (SymmetricTransform_t3854241867 * __this, ByteU5BU5D_t58506160* ___buffer, int32_t ___start, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_Random_m3415780681_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t58506160* V_0 = NULL;
	{
		RandomNumberGenerator_t2174318432 * L_0 = __this->get__rng_11();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		RandomNumberGenerator_t2174318432 * L_1 = RandomNumberGenerator_Create_m2029084057(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__rng_11(L_1);
	}

IL_0016:
	{
		int32_t L_2 = ___length;
		V_0 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_2));
		RandomNumberGenerator_t2174318432 * L_3 = __this->get__rng_11();
		ByteU5BU5D_t58506160* L_4 = V_0;
		NullCheck(L_3);
		VirtActionInvoker1< ByteU5BU5D_t58506160* >::Invoke(4 /* System.Void System.Security.Cryptography.RandomNumberGenerator::GetBytes(System.Byte[]) */, L_3, L_4);
		ByteU5BU5D_t58506160* L_5 = V_0;
		ByteU5BU5D_t58506160* L_6 = ___buffer;
		int32_t L_7 = ___start;
		int32_t L_8 = ___length;
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, 0, (Il2CppArray *)(Il2CppArray *)L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Security.Cryptography.SymmetricTransform::ThrowBadPaddingException(System.Security.Cryptography.PaddingMode,System.Int32,System.Int32)
extern TypeInfo* PaddingMode_t1724215917_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicException_t3718270561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3665781680;
extern Il2CppCodeGenString* _stringLiteral3275307319;
extern Il2CppCodeGenString* _stringLiteral210489030;
extern const uint32_t SymmetricTransform_ThrowBadPaddingException_m2083216889_MetadataUsageId;
extern "C"  void SymmetricTransform_ThrowBadPaddingException_m2083216889 (SymmetricTransform_t3854241867 * __this, int32_t ___padding, int32_t ___length, int32_t ___position, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_ThrowBadPaddingException_m2083216889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = Locale_GetText_m2031928403(NULL /*static, unused*/, _stringLiteral3665781680, /*hidden argument*/NULL);
		int32_t L_1 = ___padding;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(PaddingMode_t1724215917_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2471250780(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = ___length;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		String_t* L_6 = V_0;
		String_t* L_7 = Locale_GetText_m2031928403(NULL /*static, unused*/, _stringLiteral3275307319, /*hidden argument*/NULL);
		int32_t L_8 = ___length;
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2471250780(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Concat_m138640077(NULL /*static, unused*/, L_6, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0039:
	{
		int32_t L_13 = ___position;
		if ((((int32_t)L_13) < ((int32_t)0)))
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_14 = V_0;
		String_t* L_15 = Locale_GetText_m2031928403(NULL /*static, unused*/, _stringLiteral210489030, /*hidden argument*/NULL);
		int32_t L_16 = ___position;
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m2471250780(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/NULL);
		String_t* L_20 = String_Concat_m138640077(NULL /*static, unused*/, L_14, L_19, /*hidden argument*/NULL);
		V_0 = L_20;
	}

IL_005c:
	{
		String_t* L_21 = V_0;
		CryptographicException_t3718270561 * L_22 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m2400722889(L_22, L_21, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}
}
// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::FinalEncrypt(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicException_t3718270561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1719475554;
extern const uint32_t SymmetricTransform_FinalEncrypt_m975601255_MetadataUsageId;
extern "C"  ByteU5BU5D_t58506160* SymmetricTransform_FinalEncrypt_m975601255 (SymmetricTransform_t3854241867 * __this, ByteU5BU5D_t58506160* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_FinalEncrypt_m975601255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ByteU5BU5D_t58506160* V_3 = NULL;
	ByteU5BU5D_t58506160* V_4 = NULL;
	int32_t V_5 = 0;
	uint8_t V_6 = 0x0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		int32_t L_0 = ___inputCount;
		int32_t L_1 = __this->get_BlockSizeByte_2();
		int32_t L_2 = __this->get_BlockSizeByte_2();
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)L_1))*(int32_t)L_2));
		int32_t L_3 = ___inputCount;
		int32_t L_4 = V_0;
		V_1 = ((int32_t)((int32_t)L_3-(int32_t)L_4));
		int32_t L_5 = V_0;
		V_2 = L_5;
		SymmetricAlgorithm_t839208017 * L_6 = __this->get_algo_0();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_6);
		V_8 = L_7;
		int32_t L_8 = V_8;
		if (((int32_t)((int32_t)L_8-(int32_t)2)) == 0)
		{
			goto IL_0041;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)2)) == 1)
		{
			goto IL_004f;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)2)) == 2)
		{
			goto IL_0041;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)2)) == 3)
		{
			goto IL_0041;
		}
	}
	{
		goto IL_004f;
	}

IL_0041:
	{
		int32_t L_9 = V_2;
		int32_t L_10 = __this->get_BlockSizeByte_2();
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)L_10));
		goto IL_00a8;
	}

IL_004f:
	{
		int32_t L_11 = ___inputCount;
		if (L_11)
		{
			goto IL_005c;
		}
	}
	{
		return ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_005c:
	{
		int32_t L_12 = V_1;
		if (!L_12)
		{
			goto IL_00a3;
		}
	}
	{
		SymmetricAlgorithm_t839208017 * L_13 = __this->get_algo_0();
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_13);
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_007e;
		}
	}
	{
		CryptographicException_t3718270561 * L_15 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m2400722889(L_15, _stringLiteral1719475554, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_007e:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = __this->get_BlockSizeByte_2();
		V_3 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_16+(int32_t)L_17))));
		ByteU5BU5D_t58506160* L_18 = ___inputBuffer;
		int32_t L_19 = ___inputOffset;
		ByteU5BU5D_t58506160* L_20 = V_3;
		int32_t L_21 = ___inputCount;
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_18, L_19, (Il2CppArray *)(Il2CppArray *)L_20, 0, L_21, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_22 = V_3;
		___inputBuffer = L_22;
		___inputOffset = 0;
		ByteU5BU5D_t58506160* L_23 = V_3;
		NullCheck(L_23);
		___inputCount = (((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length))));
		int32_t L_24 = ___inputCount;
		V_2 = L_24;
	}

IL_00a3:
	{
		goto IL_00a8;
	}

IL_00a8:
	{
		int32_t L_25 = V_2;
		V_4 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_25));
		V_5 = 0;
		goto IL_00e9;
	}

IL_00b8:
	{
		ByteU5BU5D_t58506160* L_26 = ___inputBuffer;
		int32_t L_27 = ___inputOffset;
		int32_t L_28 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t58506160* L_29 = V_4;
		int32_t L_30 = V_5;
		SymmetricTransform_InternalTransformBlock_m3380491768(__this, L_26, L_27, L_28, L_29, L_30, /*hidden argument*/NULL);
		int32_t L_31 = ___inputOffset;
		int32_t L_32 = __this->get_BlockSizeByte_2();
		___inputOffset = ((int32_t)((int32_t)L_31+(int32_t)L_32));
		int32_t L_33 = V_5;
		int32_t L_34 = __this->get_BlockSizeByte_2();
		V_5 = ((int32_t)((int32_t)L_33+(int32_t)L_34));
		int32_t L_35 = V_2;
		int32_t L_36 = __this->get_BlockSizeByte_2();
		V_2 = ((int32_t)((int32_t)L_35-(int32_t)L_36));
	}

IL_00e9:
	{
		int32_t L_37 = V_2;
		int32_t L_38 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_37) > ((int32_t)L_38)))
		{
			goto IL_00b8;
		}
	}
	{
		int32_t L_39 = __this->get_BlockSizeByte_2();
		int32_t L_40 = V_1;
		V_6 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_39-(int32_t)L_40)))));
		SymmetricAlgorithm_t839208017 * L_41 = __this->get_algo_0();
		NullCheck(L_41);
		int32_t L_42 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_41);
		V_8 = L_42;
		int32_t L_43 = V_8;
		if (((int32_t)((int32_t)L_43-(int32_t)2)) == 0)
		{
			goto IL_019a;
		}
		if (((int32_t)((int32_t)L_43-(int32_t)2)) == 1)
		{
			goto IL_01e2;
		}
		if (((int32_t)((int32_t)L_43-(int32_t)2)) == 2)
		{
			goto IL_012b;
		}
		if (((int32_t)((int32_t)L_43-(int32_t)2)) == 3)
		{
			goto IL_0159;
		}
	}
	{
		goto IL_01e2;
	}

IL_012b:
	{
		ByteU5BU5D_t58506160* L_44 = V_4;
		ByteU5BU5D_t58506160* L_45 = V_4;
		NullCheck(L_45);
		uint8_t L_46 = V_6;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))))-(int32_t)1)));
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))))-(int32_t)1))), (uint8_t)L_46);
		ByteU5BU5D_t58506160* L_47 = ___inputBuffer;
		int32_t L_48 = ___inputOffset;
		ByteU5BU5D_t58506160* L_49 = V_4;
		int32_t L_50 = V_0;
		int32_t L_51 = V_1;
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_47, L_48, (Il2CppArray *)(Il2CppArray *)L_49, L_50, L_51, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_52 = V_4;
		int32_t L_53 = V_0;
		int32_t L_54 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t58506160* L_55 = V_4;
		int32_t L_56 = V_0;
		SymmetricTransform_InternalTransformBlock_m3380491768(__this, L_52, L_53, L_54, L_55, L_56, /*hidden argument*/NULL);
		goto IL_01fa;
	}

IL_0159:
	{
		ByteU5BU5D_t58506160* L_57 = V_4;
		ByteU5BU5D_t58506160* L_58 = V_4;
		NullCheck(L_58);
		uint8_t L_59 = V_6;
		uint8_t L_60 = V_6;
		SymmetricTransform_Random_m3415780681(__this, L_57, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_58)->max_length))))-(int32_t)L_59)), ((int32_t)((int32_t)L_60-(int32_t)1)), /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_61 = V_4;
		ByteU5BU5D_t58506160* L_62 = V_4;
		NullCheck(L_62);
		uint8_t L_63 = V_6;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_62)->max_length))))-(int32_t)1)));
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_62)->max_length))))-(int32_t)1))), (uint8_t)L_63);
		ByteU5BU5D_t58506160* L_64 = ___inputBuffer;
		int32_t L_65 = ___inputOffset;
		ByteU5BU5D_t58506160* L_66 = V_4;
		int32_t L_67 = V_0;
		int32_t L_68 = V_1;
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_64, L_65, (Il2CppArray *)(Il2CppArray *)L_66, L_67, L_68, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_69 = V_4;
		int32_t L_70 = V_0;
		int32_t L_71 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t58506160* L_72 = V_4;
		int32_t L_73 = V_0;
		SymmetricTransform_InternalTransformBlock_m3380491768(__this, L_69, L_70, L_71, L_72, L_73, /*hidden argument*/NULL);
		goto IL_01fa;
	}

IL_019a:
	{
		ByteU5BU5D_t58506160* L_74 = V_4;
		NullCheck(L_74);
		V_7 = (((int32_t)((int32_t)(((Il2CppArray *)L_74)->max_length))));
		goto IL_01ac;
	}

IL_01a5:
	{
		ByteU5BU5D_t58506160* L_75 = V_4;
		int32_t L_76 = V_7;
		uint8_t L_77 = V_6;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, L_76);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(L_76), (uint8_t)L_77);
	}

IL_01ac:
	{
		int32_t L_78 = V_7;
		int32_t L_79 = ((int32_t)((int32_t)L_78-(int32_t)1));
		V_7 = L_79;
		ByteU5BU5D_t58506160* L_80 = V_4;
		NullCheck(L_80);
		uint8_t L_81 = V_6;
		if ((((int32_t)L_79) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_80)->max_length))))-(int32_t)L_81)))))
		{
			goto IL_01a5;
		}
	}
	{
		ByteU5BU5D_t58506160* L_82 = ___inputBuffer;
		int32_t L_83 = ___inputOffset;
		ByteU5BU5D_t58506160* L_84 = V_4;
		int32_t L_85 = V_0;
		int32_t L_86 = V_1;
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_82, L_83, (Il2CppArray *)(Il2CppArray *)L_84, L_85, L_86, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_87 = V_4;
		int32_t L_88 = V_0;
		int32_t L_89 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t58506160* L_90 = V_4;
		int32_t L_91 = V_0;
		SymmetricTransform_InternalTransformBlock_m3380491768(__this, L_87, L_88, L_89, L_90, L_91, /*hidden argument*/NULL);
		goto IL_01fa;
	}

IL_01e2:
	{
		ByteU5BU5D_t58506160* L_92 = ___inputBuffer;
		int32_t L_93 = ___inputOffset;
		int32_t L_94 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t58506160* L_95 = V_4;
		int32_t L_96 = V_5;
		SymmetricTransform_InternalTransformBlock_m3380491768(__this, L_92, L_93, L_94, L_95, L_96, /*hidden argument*/NULL);
		goto IL_01fa;
	}

IL_01fa:
	{
		ByteU5BU5D_t58506160* L_97 = V_4;
		return L_97;
	}
}
// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::FinalDecrypt(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* CryptographicException_t3718270561_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3566855579;
extern const uint32_t SymmetricTransform_FinalDecrypt_m3742401599_MetadataUsageId;
extern "C"  ByteU5BU5D_t58506160* SymmetricTransform_FinalDecrypt_m3742401599 (SymmetricTransform_t3854241867 * __this, ByteU5BU5D_t58506160* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_FinalDecrypt_m3742401599_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t58506160* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	uint8_t V_4 = 0x0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ByteU5BU5D_t58506160* V_7 = NULL;
	int32_t V_8 = 0;
	int32_t G_B12_0 = 0;
	{
		int32_t L_0 = ___inputCount;
		int32_t L_1 = __this->get_BlockSizeByte_2();
		if ((((int32_t)((int32_t)((int32_t)L_0%(int32_t)L_1))) <= ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		CryptographicException_t3718270561 * L_2 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m2400722889(L_2, _stringLiteral3566855579, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		int32_t L_3 = ___inputCount;
		V_0 = L_3;
		bool L_4 = __this->get_lastBlock_10();
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_5 = V_0;
		int32_t L_6 = __this->get_BlockSizeByte_2();
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)L_6));
	}

IL_002f:
	{
		int32_t L_7 = V_0;
		V_1 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_7));
		V_2 = 0;
		goto IL_0066;
	}

IL_003d:
	{
		ByteU5BU5D_t58506160* L_8 = ___inputBuffer;
		int32_t L_9 = ___inputOffset;
		int32_t L_10 = __this->get_BlockSizeByte_2();
		ByteU5BU5D_t58506160* L_11 = V_1;
		int32_t L_12 = V_2;
		int32_t L_13 = SymmetricTransform_InternalTransformBlock_m3380491768(__this, L_8, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		int32_t L_14 = ___inputOffset;
		int32_t L_15 = __this->get_BlockSizeByte_2();
		___inputOffset = ((int32_t)((int32_t)L_14+(int32_t)L_15));
		int32_t L_16 = V_2;
		int32_t L_17 = V_3;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)L_17));
		int32_t L_18 = ___inputCount;
		int32_t L_19 = __this->get_BlockSizeByte_2();
		___inputCount = ((int32_t)((int32_t)L_18-(int32_t)L_19));
	}

IL_0066:
	{
		int32_t L_20 = ___inputCount;
		if ((((int32_t)L_20) > ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		bool L_21 = __this->get_lastBlock_10();
		if (!L_21)
		{
			goto IL_00ae;
		}
	}
	{
		ByteU5BU5D_t58506160* L_22 = __this->get_workBuff_5();
		ByteU5BU5D_t58506160* L_23 = __this->get_workout_6();
		VirtActionInvoker2< ByteU5BU5D_t58506160*, ByteU5BU5D_t58506160* >::Invoke(10 /* System.Void Mono.Security.Cryptography.SymmetricTransform::Transform(System.Byte[],System.Byte[]) */, __this, L_22, L_23);
		ByteU5BU5D_t58506160* L_24 = __this->get_workout_6();
		ByteU5BU5D_t58506160* L_25 = V_1;
		int32_t L_26 = V_2;
		int32_t L_27 = __this->get_BlockSizeByte_2();
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_24, 0, (Il2CppArray *)(Il2CppArray *)L_25, L_26, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_2;
		int32_t L_29 = __this->get_BlockSizeByte_2();
		V_2 = ((int32_t)((int32_t)L_28+(int32_t)L_29));
		__this->set_lastBlock_10((bool)0);
	}

IL_00ae:
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) <= ((int32_t)0)))
		{
			goto IL_00bf;
		}
	}
	{
		ByteU5BU5D_t58506160* L_31 = V_1;
		int32_t L_32 = V_0;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)((int32_t)L_32-(int32_t)1)));
		int32_t L_33 = ((int32_t)((int32_t)L_32-(int32_t)1));
		G_B12_0 = ((int32_t)(((L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33)))));
		goto IL_00c0;
	}

IL_00bf:
	{
		G_B12_0 = 0;
	}

IL_00c0:
	{
		V_4 = G_B12_0;
		SymmetricAlgorithm_t839208017 * L_34 = __this->get_algo_0();
		NullCheck(L_34);
		int32_t L_35 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_34);
		V_8 = L_35;
		int32_t L_36 = V_8;
		if (((int32_t)((int32_t)L_36-(int32_t)1)) == 0)
		{
			goto IL_01fd;
		}
		if (((int32_t)((int32_t)L_36-(int32_t)1)) == 1)
		{
			goto IL_018f;
		}
		if (((int32_t)((int32_t)L_36-(int32_t)1)) == 2)
		{
			goto IL_01fd;
		}
		if (((int32_t)((int32_t)L_36-(int32_t)1)) == 3)
		{
			goto IL_00f1;
		}
		if (((int32_t)((int32_t)L_36-(int32_t)1)) == 4)
		{
			goto IL_015d;
		}
	}
	{
		goto IL_0202;
	}

IL_00f1:
	{
		uint8_t L_37 = V_4;
		if (!L_37)
		{
			goto IL_0105;
		}
	}
	{
		uint8_t L_38 = V_4;
		int32_t L_39 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_38) <= ((int32_t)L_39)))
		{
			goto IL_0119;
		}
	}

IL_0105:
	{
		SymmetricAlgorithm_t839208017 * L_40 = __this->get_algo_0();
		NullCheck(L_40);
		int32_t L_41 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_40);
		uint8_t L_42 = V_4;
		SymmetricTransform_ThrowBadPaddingException_m2083216889(__this, L_41, L_42, (-1), /*hidden argument*/NULL);
	}

IL_0119:
	{
		uint8_t L_43 = V_4;
		V_5 = ((int32_t)((int32_t)L_43-(int32_t)1));
		goto IL_014b;
	}

IL_0124:
	{
		ByteU5BU5D_t58506160* L_44 = V_1;
		int32_t L_45 = V_0;
		int32_t L_46 = V_5;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)((int32_t)((int32_t)((int32_t)L_45-(int32_t)1))-(int32_t)L_46)));
		int32_t L_47 = ((int32_t)((int32_t)((int32_t)((int32_t)L_45-(int32_t)1))-(int32_t)L_46));
		if (!((L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_47))))
		{
			goto IL_0145;
		}
	}
	{
		SymmetricAlgorithm_t839208017 * L_48 = __this->get_algo_0();
		NullCheck(L_48);
		int32_t L_49 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_48);
		int32_t L_50 = V_5;
		SymmetricTransform_ThrowBadPaddingException_m2083216889(__this, L_49, (-1), L_50, /*hidden argument*/NULL);
	}

IL_0145:
	{
		int32_t L_51 = V_5;
		V_5 = ((int32_t)((int32_t)L_51-(int32_t)1));
	}

IL_014b:
	{
		int32_t L_52 = V_5;
		if ((((int32_t)L_52) > ((int32_t)0)))
		{
			goto IL_0124;
		}
	}
	{
		int32_t L_53 = V_0;
		uint8_t L_54 = V_4;
		V_0 = ((int32_t)((int32_t)L_53-(int32_t)L_54));
		goto IL_0202;
	}

IL_015d:
	{
		uint8_t L_55 = V_4;
		if (!L_55)
		{
			goto IL_0171;
		}
	}
	{
		uint8_t L_56 = V_4;
		int32_t L_57 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_56) <= ((int32_t)L_57)))
		{
			goto IL_0185;
		}
	}

IL_0171:
	{
		SymmetricAlgorithm_t839208017 * L_58 = __this->get_algo_0();
		NullCheck(L_58);
		int32_t L_59 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_58);
		uint8_t L_60 = V_4;
		SymmetricTransform_ThrowBadPaddingException_m2083216889(__this, L_59, L_60, (-1), /*hidden argument*/NULL);
	}

IL_0185:
	{
		int32_t L_61 = V_0;
		uint8_t L_62 = V_4;
		V_0 = ((int32_t)((int32_t)L_61-(int32_t)L_62));
		goto IL_0202;
	}

IL_018f:
	{
		uint8_t L_63 = V_4;
		if (!L_63)
		{
			goto IL_01a3;
		}
	}
	{
		uint8_t L_64 = V_4;
		int32_t L_65 = __this->get_BlockSizeByte_2();
		if ((((int32_t)L_64) <= ((int32_t)L_65)))
		{
			goto IL_01b7;
		}
	}

IL_01a3:
	{
		SymmetricAlgorithm_t839208017 * L_66 = __this->get_algo_0();
		NullCheck(L_66);
		int32_t L_67 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_66);
		uint8_t L_68 = V_4;
		SymmetricTransform_ThrowBadPaddingException_m2083216889(__this, L_67, L_68, (-1), /*hidden argument*/NULL);
	}

IL_01b7:
	{
		uint8_t L_69 = V_4;
		V_6 = ((int32_t)((int32_t)L_69-(int32_t)1));
		goto IL_01eb;
	}

IL_01c2:
	{
		ByteU5BU5D_t58506160* L_70 = V_1;
		int32_t L_71 = V_0;
		int32_t L_72 = V_6;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, ((int32_t)((int32_t)((int32_t)((int32_t)L_71-(int32_t)1))-(int32_t)L_72)));
		int32_t L_73 = ((int32_t)((int32_t)((int32_t)((int32_t)L_71-(int32_t)1))-(int32_t)L_72));
		uint8_t L_74 = V_4;
		if ((((int32_t)((L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_73)))) == ((int32_t)L_74)))
		{
			goto IL_01e5;
		}
	}
	{
		SymmetricAlgorithm_t839208017 * L_75 = __this->get_algo_0();
		NullCheck(L_75);
		int32_t L_76 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::get_Padding() */, L_75);
		int32_t L_77 = V_6;
		SymmetricTransform_ThrowBadPaddingException_m2083216889(__this, L_76, (-1), L_77, /*hidden argument*/NULL);
	}

IL_01e5:
	{
		int32_t L_78 = V_6;
		V_6 = ((int32_t)((int32_t)L_78-(int32_t)1));
	}

IL_01eb:
	{
		int32_t L_79 = V_6;
		if ((((int32_t)L_79) > ((int32_t)0)))
		{
			goto IL_01c2;
		}
	}
	{
		int32_t L_80 = V_0;
		uint8_t L_81 = V_4;
		V_0 = ((int32_t)((int32_t)L_80-(int32_t)L_81));
		goto IL_0202;
	}

IL_01fd:
	{
		goto IL_0202;
	}

IL_0202:
	{
		int32_t L_82 = V_0;
		if ((((int32_t)L_82) <= ((int32_t)0)))
		{
			goto IL_0229;
		}
	}
	{
		int32_t L_83 = V_0;
		V_7 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_83));
		ByteU5BU5D_t58506160* L_84 = V_1;
		ByteU5BU5D_t58506160* L_85 = V_7;
		int32_t L_86 = V_0;
		Buffer_BlockCopy_m1580643184(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_84, 0, (Il2CppArray *)(Il2CppArray *)L_85, 0, L_86, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_87 = V_1;
		ByteU5BU5D_t58506160* L_88 = V_1;
		NullCheck(L_88);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_87, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_88)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_89 = V_7;
		return L_89;
	}

IL_0229:
	{
		return ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)0));
	}
}
// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral783533338;
extern const uint32_t SymmetricTransform_TransformFinalBlock_m2524388439_MetadataUsageId;
extern "C"  ByteU5BU5D_t58506160* SymmetricTransform_TransformFinalBlock_m2524388439 (SymmetricTransform_t3854241867 * __this, ByteU5BU5D_t58506160* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SymmetricTransform_TransformFinalBlock_m2524388439_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_disposed_9();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, _stringLiteral783533338, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		ByteU5BU5D_t58506160* L_2 = ___inputBuffer;
		int32_t L_3 = ___inputOffset;
		int32_t L_4 = ___inputCount;
		SymmetricTransform_CheckInput_m3506478922(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
		bool L_5 = __this->get_encrypt_1();
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		ByteU5BU5D_t58506160* L_6 = ___inputBuffer;
		int32_t L_7 = ___inputOffset;
		int32_t L_8 = ___inputCount;
		ByteU5BU5D_t58506160* L_9 = SymmetricTransform_FinalEncrypt_m975601255(__this, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0034:
	{
		ByteU5BU5D_t58506160* L_10 = ___inputBuffer;
		int32_t L_11 = ___inputOffset;
		int32_t L_12 = ___inputCount;
		ByteU5BU5D_t58506160* L_13 = SymmetricTransform_FinalDecrypt_m3742401599(__this, L_10, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C"  void Action__ctor_m2957240604 (Action_t437523947 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Void System.Action::Invoke()
extern "C"  void Action_Invoke_m1445970038 (Action_t437523947 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_Invoke_m1445970038((Action_t437523947 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_Action_t437523947(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult System.Action::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_BeginInvoke_m1056371541 (Action_t437523947 * __this, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Action::EndInvoke(System.IAsyncResult)
extern "C"  void Action_EndInvoke_m2672838956 (Action_t437523947 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.InvalidTimeZoneException::.ctor()
extern "C"  void InvalidTimeZoneException__ctor_m377819229 (InvalidTimeZoneException_t3180604084 * __this, const MethodInfo* method)
{
	{
		Exception__ctor_m3223090658(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.InvalidTimeZoneException::.ctor(System.String)
extern "C"  void InvalidTimeZoneException__ctor_m707317189 (InvalidTimeZoneException_t3180604084 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception__ctor_m3870771296(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.InvalidTimeZoneException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void InvalidTimeZoneException__ctor_m806710302 (InvalidTimeZoneException_t3180604084 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___sc, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		StreamingContext_t986364934  L_1 = ___sc;
		Exception__ctor_m3602014243(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Linq.Check::Source(System.Object)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3398461467;
extern const uint32_t Check_Source_m228347543_MetadataUsageId;
extern "C"  void Check_Source_m228347543 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___source, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Check_Source_m228347543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___source;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3398461467, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Linq.Check::SourceAndSelector(System.Object,System.Object)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3398461467;
extern Il2CppCodeGenString* _stringLiteral1191572447;
extern const uint32_t Check_SourceAndSelector_m3164339431_MetadataUsageId;
extern "C"  void Check_SourceAndSelector_m3164339431 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___source, Il2CppObject * ___selector, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Check_SourceAndSelector_m3164339431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___source;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3398461467, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___selector;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, _stringLiteral1191572447, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		return;
	}
}
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3398461467;
extern Il2CppCodeGenString* _stringLiteral2946935223;
extern const uint32_t Check_SourceAndPredicate_m2252398949_MetadataUsageId;
extern "C"  void Check_SourceAndPredicate_m2252398949 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___source, Il2CppObject * ___predicate, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Check_SourceAndPredicate_m2252398949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___source;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3398461467, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___predicate;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, _stringLiteral2946935223, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		return;
	}
}
// System.Void System.Linq.Check::SourceAndKeySelector(System.Object,System.Object)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3398461467;
extern Il2CppCodeGenString* _stringLiteral4224264030;
extern const uint32_t Check_SourceAndKeySelector_m1357342302_MetadataUsageId;
extern "C"  void Check_SourceAndKeySelector_m1357342302 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___source, Il2CppObject * ___keySelector, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Check_SourceAndKeySelector_m1357342302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___source;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3398461467, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___keySelector;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, _stringLiteral4224264030, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		return;
	}
}
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern "C"  void ExtensionAttribute__ctor_m1242622322 (ExtensionAttribute_t795512515 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.Aes::.ctor()
extern TypeInfo* KeySizesU5BU5D_t1304982661_il2cpp_TypeInfo_var;
extern TypeInfo* KeySizes_t2111859404_il2cpp_TypeInfo_var;
extern const uint32_t Aes__ctor_m380511311_MetadataUsageId;
extern "C"  void Aes__ctor_m380511311 (Aes_t1557549544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Aes__ctor_m380511311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SymmetricAlgorithm__ctor_m3015042793(__this, /*hidden argument*/NULL);
		((SymmetricAlgorithm_t839208017 *)__this)->set_KeySizeValue_2(((int32_t)256));
		((SymmetricAlgorithm_t839208017 *)__this)->set_BlockSizeValue_0(((int32_t)128));
		((SymmetricAlgorithm_t839208017 *)__this)->set_LegalKeySizesValue_5(((KeySizesU5BU5D_t1304982661*)SZArrayNew(KeySizesU5BU5D_t1304982661_il2cpp_TypeInfo_var, (uint32_t)1)));
		KeySizesU5BU5D_t1304982661* L_0 = ((SymmetricAlgorithm_t839208017 *)__this)->get_LegalKeySizesValue_5();
		KeySizes_t2111859404 * L_1 = (KeySizes_t2111859404 *)il2cpp_codegen_object_new(KeySizes_t2111859404_il2cpp_TypeInfo_var);
		KeySizes__ctor_m428291391(L_1, ((int32_t)128), ((int32_t)256), ((int32_t)64), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (KeySizes_t2111859404 *)L_1);
		((SymmetricAlgorithm_t839208017 *)__this)->set_LegalBlockSizesValue_4(((KeySizesU5BU5D_t1304982661*)SZArrayNew(KeySizesU5BU5D_t1304982661_il2cpp_TypeInfo_var, (uint32_t)1)));
		KeySizesU5BU5D_t1304982661* L_2 = ((SymmetricAlgorithm_t839208017 *)__this)->get_LegalBlockSizesValue_4();
		KeySizes_t2111859404 * L_3 = (KeySizes_t2111859404 *)il2cpp_codegen_object_new(KeySizes_t2111859404_il2cpp_TypeInfo_var);
		KeySizes__ctor_m428291391(L_3, ((int32_t)128), ((int32_t)128), 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (KeySizes_t2111859404 *)L_3);
		return;
	}
}
// System.Void System.Security.Cryptography.AesManaged::.ctor()
extern "C"  void AesManaged__ctor_m1572578968 (AesManaged_t564372777 * __this, const MethodInfo* method)
{
	{
		Aes__ctor_m380511311(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern "C"  void AesManaged_GenerateIV_m4188712014 (AesManaged_t564372777 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((SymmetricAlgorithm_t839208017 *)__this)->get_BlockSizeValue_0();
		ByteU5BU5D_t58506160* L_1 = KeyBuilder_IV_m3901719576(NULL /*static, unused*/, ((int32_t)((int32_t)L_0>>(int32_t)3)), /*hidden argument*/NULL);
		((SymmetricAlgorithm_t839208017 *)__this)->set_IVValue_1(L_1);
		return;
	}
}
// System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern "C"  void AesManaged_GenerateKey_m1003425312 (AesManaged_t564372777 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((SymmetricAlgorithm_t839208017 *)__this)->get_KeySizeValue_2();
		ByteU5BU5D_t58506160* L_1 = KeyBuilder_Key_m489708312(NULL /*static, unused*/, ((int32_t)((int32_t)L_0>>(int32_t)3)), /*hidden argument*/NULL);
		((SymmetricAlgorithm_t839208017 *)__this)->set_KeyValue_3(L_1);
		return;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern TypeInfo* AesTransform_t2186883254_il2cpp_TypeInfo_var;
extern const uint32_t AesManaged_CreateDecryptor_m1952358980_MetadataUsageId;
extern "C"  Il2CppObject * AesManaged_CreateDecryptor_m1952358980 (AesManaged_t564372777 * __this, ByteU5BU5D_t58506160* ___rgbKey, ByteU5BU5D_t58506160* ___rgbIV, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AesManaged_CreateDecryptor_m1952358980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t58506160* L_0 = ___rgbKey;
		ByteU5BU5D_t58506160* L_1 = ___rgbIV;
		AesTransform_t2186883254 * L_2 = (AesTransform_t2186883254 *)il2cpp_codegen_object_new(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		AesTransform__ctor_m2781154868(L_2, __this, (bool)0, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern TypeInfo* AesTransform_t2186883254_il2cpp_TypeInfo_var;
extern const uint32_t AesManaged_CreateEncryptor_m2258087532_MetadataUsageId;
extern "C"  Il2CppObject * AesManaged_CreateEncryptor_m2258087532 (AesManaged_t564372777 * __this, ByteU5BU5D_t58506160* ___rgbKey, ByteU5BU5D_t58506160* ___rgbIV, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AesManaged_CreateEncryptor_m2258087532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t58506160* L_0 = ___rgbKey;
		ByteU5BU5D_t58506160* L_1 = ___rgbIV;
		AesTransform_t2186883254 * L_2 = (AesTransform_t2186883254 *)il2cpp_codegen_object_new(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		AesTransform__ctor_m2781154868(L_2, __this, (bool)1, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern "C"  ByteU5BU5D_t58506160* AesManaged_get_IV_m3771642968 (AesManaged_t564372777 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = SymmetricAlgorithm_get_IV_m2399566439(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern "C"  void AesManaged_set_IV_m2456625939 (AesManaged_t564372777 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = ___value;
		SymmetricAlgorithm_set_IV_m870580644(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern "C"  ByteU5BU5D_t58506160* AesManaged_get_Key_m959186774 (AesManaged_t564372777 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = SymmetricAlgorithm_get_Key_m1374487335(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern "C"  void AesManaged_set_Key_m1884561361 (AesManaged_t564372777 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = ___value;
		SymmetricAlgorithm_set_Key_m4256764768(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern "C"  int32_t AesManaged_get_KeySize_m1856785595 (AesManaged_t564372777 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = SymmetricAlgorithm_get_KeySize_m3649844218(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern "C"  void AesManaged_set_KeySize_m838068010 (AesManaged_t564372777 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		SymmetricAlgorithm_set_KeySize_m1757877563(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern "C"  Il2CppObject * AesManaged_CreateDecryptor_m2733487560 (AesManaged_t564372777 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = AesManaged_get_Key_m959186774(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_1 = AesManaged_get_IV_m3771642968(__this, /*hidden argument*/NULL);
		Il2CppObject * L_2 = AesManaged_CreateDecryptor_m1952358980(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern "C"  Il2CppObject * AesManaged_CreateEncryptor_m3462881952 (AesManaged_t564372777 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = AesManaged_get_Key_m959186774(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_1 = AesManaged_get_IV_m3771642968(__this, /*hidden argument*/NULL);
		Il2CppObject * L_2 = AesManaged_CreateEncryptor_m2258087532(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern "C"  void AesManaged_Dispose_m2950523276 (AesManaged_t564372777 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		bool L_0 = ___disposing;
		SymmetricAlgorithm_Dispose_m3474384861(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern TypeInfo* CryptographicException_t3718270561_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32U5BU5D_t2133601851_il2cpp_TypeInfo_var;
extern TypeInfo* AesTransform_t2186883254_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral632339964;
extern Il2CppCodeGenString* _stringLiteral1688174393;
extern Il2CppCodeGenString* _stringLiteral3873498541;
extern const uint32_t AesTransform__ctor_m2781154868_MetadataUsageId;
extern "C"  void AesTransform__ctor_m2781154868 (AesTransform_t2186883254 * __this, Aes_t1557549544 * ___algo, bool ___encryption, ByteU5BU5D_t58506160* ___key, ByteU5BU5D_t58506160* ___iv, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AesTransform__ctor_m2781154868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	UInt32U5BU5D_t2133601851* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	uint32_t V_7 = 0;
	int32_t V_8 = 0;
	uint32_t V_9 = 0;
	uint32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	uint32_t V_14 = 0;
	int32_t V_15 = 0;
	{
		Aes_t1557549544 * L_0 = ___algo;
		bool L_1 = ___encryption;
		ByteU5BU5D_t58506160* L_2 = ___iv;
		SymmetricTransform__ctor_m1205754880(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_3 = ___key;
		if (L_3)
		{
			goto IL_001b;
		}
	}
	{
		CryptographicException_t3718270561 * L_4 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m2400722889(L_4, _stringLiteral632339964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_001b:
	{
		ByteU5BU5D_t58506160* L_5 = ___iv;
		if (!L_5)
		{
			goto IL_0067;
		}
	}
	{
		ByteU5BU5D_t58506160* L_6 = ___iv;
		NullCheck(L_6);
		Aes_t1557549544 * L_7 = ___algo;
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Security.Cryptography.SymmetricAlgorithm::get_BlockSize() */, L_7);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((int32_t)((int32_t)((int32_t)L_8>>(int32_t)3)))))
		{
			goto IL_0067;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		ByteU5BU5D_t58506160* L_10 = ___iv;
		NullCheck(L_10);
		int32_t L_11 = (((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))));
		Il2CppObject * L_12 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = L_9;
		Aes_t1557549544 * L_14 = ___algo;
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Security.Cryptography.SymmetricAlgorithm::get_BlockSize() */, L_14);
		int32_t L_16 = ((int32_t)((int32_t)L_15>>(int32_t)3));
		Il2CppObject * L_17 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		ArrayElementTypeCheck (L_13, L_17);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_17);
		String_t* L_18 = Locale_GetText_m2138526911(NULL /*static, unused*/, _stringLiteral1688174393, L_13, /*hidden argument*/NULL);
		V_0 = L_18;
		String_t* L_19 = V_0;
		CryptographicException_t3718270561 * L_20 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m2400722889(L_20, L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}

IL_0067:
	{
		ByteU5BU5D_t58506160* L_21 = ___key;
		NullCheck(L_21);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length))));
		int32_t L_22 = V_1;
		if ((((int32_t)L_22) == ((int32_t)((int32_t)16))))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_23 = V_1;
		if ((((int32_t)L_23) == ((int32_t)((int32_t)24))))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) == ((int32_t)((int32_t)32))))
		{
			goto IL_00c2;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_25 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_26 = V_1;
		int32_t L_27 = L_26;
		Il2CppObject * L_28 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		ArrayElementTypeCheck (L_25, L_28);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_28);
		ObjectU5BU5D_t11523773* L_29 = L_25;
		int32_t L_30 = ((int32_t)16);
		Il2CppObject * L_31 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 1);
		ArrayElementTypeCheck (L_29, L_31);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_31);
		ObjectU5BU5D_t11523773* L_32 = L_29;
		int32_t L_33 = ((int32_t)24);
		Il2CppObject * L_34 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 2);
		ArrayElementTypeCheck (L_32, L_34);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_34);
		ObjectU5BU5D_t11523773* L_35 = L_32;
		int32_t L_36 = ((int32_t)32);
		Il2CppObject * L_37 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 3);
		ArrayElementTypeCheck (L_35, L_37);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_37);
		String_t* L_38 = Locale_GetText_m2138526911(NULL /*static, unused*/, _stringLiteral3873498541, L_35, /*hidden argument*/NULL);
		V_2 = L_38;
		String_t* L_39 = V_2;
		CryptographicException_t3718270561 * L_40 = (CryptographicException_t3718270561 *)il2cpp_codegen_object_new(CryptographicException_t3718270561_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m2400722889(L_40, L_39, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_40);
	}

IL_00c2:
	{
		int32_t L_41 = V_1;
		V_1 = ((int32_t)((int32_t)L_41<<(int32_t)3));
		int32_t L_42 = V_1;
		__this->set_Nk_13(((int32_t)((int32_t)L_42>>(int32_t)5)));
		int32_t L_43 = __this->get_Nk_13();
		if ((!(((uint32_t)L_43) == ((uint32_t)8))))
		{
			goto IL_00e8;
		}
	}
	{
		__this->set_Nr_14(((int32_t)14));
		goto IL_0109;
	}

IL_00e8:
	{
		int32_t L_44 = __this->get_Nk_13();
		if ((!(((uint32_t)L_44) == ((uint32_t)6))))
		{
			goto IL_0101;
		}
	}
	{
		__this->set_Nr_14(((int32_t)12));
		goto IL_0109;
	}

IL_0101:
	{
		__this->set_Nr_14(((int32_t)10));
	}

IL_0109:
	{
		int32_t L_45 = __this->get_Nr_14();
		V_3 = ((int32_t)((int32_t)4*(int32_t)((int32_t)((int32_t)L_45+(int32_t)1))));
		int32_t L_46 = V_3;
		V_4 = ((UInt32U5BU5D_t2133601851*)SZArrayNew(UInt32U5BU5D_t2133601851_il2cpp_TypeInfo_var, (uint32_t)L_46));
		V_5 = 0;
		V_6 = 0;
		goto IL_0171;
	}

IL_0127:
	{
		ByteU5BU5D_t58506160* L_47 = ___key;
		int32_t L_48 = V_5;
		int32_t L_49 = L_48;
		V_5 = ((int32_t)((int32_t)L_49+(int32_t)1));
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_49);
		int32_t L_50 = L_49;
		V_7 = ((int32_t)((int32_t)((L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_50)))<<(int32_t)((int32_t)24)));
		uint32_t L_51 = V_7;
		ByteU5BU5D_t58506160* L_52 = ___key;
		int32_t L_53 = V_5;
		int32_t L_54 = L_53;
		V_5 = ((int32_t)((int32_t)L_54+(int32_t)1));
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_54);
		int32_t L_55 = L_54;
		V_7 = ((int32_t)((int32_t)L_51|(int32_t)((int32_t)((int32_t)((L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_55)))<<(int32_t)((int32_t)16)))));
		uint32_t L_56 = V_7;
		ByteU5BU5D_t58506160* L_57 = ___key;
		int32_t L_58 = V_5;
		int32_t L_59 = L_58;
		V_5 = ((int32_t)((int32_t)L_59+(int32_t)1));
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, L_59);
		int32_t L_60 = L_59;
		V_7 = ((int32_t)((int32_t)L_56|(int32_t)((int32_t)((int32_t)((L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_60)))<<(int32_t)8))));
		uint32_t L_61 = V_7;
		ByteU5BU5D_t58506160* L_62 = ___key;
		int32_t L_63 = V_5;
		int32_t L_64 = L_63;
		V_5 = ((int32_t)((int32_t)L_64+(int32_t)1));
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, L_64);
		int32_t L_65 = L_64;
		V_7 = ((int32_t)((int32_t)L_61|(int32_t)((L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_65)))));
		UInt32U5BU5D_t2133601851* L_66 = V_4;
		int32_t L_67 = V_6;
		uint32_t L_68 = V_7;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(L_67), (uint32_t)L_68);
		int32_t L_69 = V_6;
		V_6 = ((int32_t)((int32_t)L_69+(int32_t)1));
	}

IL_0171:
	{
		int32_t L_70 = V_6;
		int32_t L_71 = __this->get_Nk_13();
		if ((((int32_t)L_70) < ((int32_t)L_71)))
		{
			goto IL_0127;
		}
	}
	{
		int32_t L_72 = __this->get_Nk_13();
		V_8 = L_72;
		goto IL_0212;
	}

IL_018b:
	{
		UInt32U5BU5D_t2133601851* L_73 = V_4;
		int32_t L_74 = V_8;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)((int32_t)L_74-(int32_t)1)));
		int32_t L_75 = ((int32_t)((int32_t)L_74-(int32_t)1));
		V_9 = ((L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_75)));
		int32_t L_76 = V_8;
		int32_t L_77 = __this->get_Nk_13();
		if (((int32_t)((int32_t)L_76%(int32_t)L_77)))
		{
			goto IL_01d3;
		}
	}
	{
		uint32_t L_78 = V_9;
		uint32_t L_79 = V_9;
		V_10 = ((int32_t)((int32_t)((int32_t)((int32_t)L_78<<(int32_t)8))|(int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_79>>((int32_t)24)))&(int32_t)((int32_t)255)))));
		uint32_t L_80 = V_10;
		uint32_t L_81 = AesTransform_SubByte_m1214814958(__this, L_80, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2133601851* L_82 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_Rcon_15();
		int32_t L_83 = V_8;
		int32_t L_84 = __this->get_Nk_13();
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, ((int32_t)((int32_t)L_83/(int32_t)L_84)));
		int32_t L_85 = ((int32_t)((int32_t)L_83/(int32_t)L_84));
		V_9 = ((int32_t)((int32_t)L_81^(int32_t)((L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_85)))));
		goto IL_01f8;
	}

IL_01d3:
	{
		int32_t L_86 = __this->get_Nk_13();
		if ((((int32_t)L_86) <= ((int32_t)6)))
		{
			goto IL_01f8;
		}
	}
	{
		int32_t L_87 = V_8;
		int32_t L_88 = __this->get_Nk_13();
		if ((!(((uint32_t)((int32_t)((int32_t)L_87%(int32_t)L_88))) == ((uint32_t)4))))
		{
			goto IL_01f8;
		}
	}
	{
		uint32_t L_89 = V_9;
		uint32_t L_90 = AesTransform_SubByte_m1214814958(__this, L_89, /*hidden argument*/NULL);
		V_9 = L_90;
	}

IL_01f8:
	{
		UInt32U5BU5D_t2133601851* L_91 = V_4;
		int32_t L_92 = V_8;
		UInt32U5BU5D_t2133601851* L_93 = V_4;
		int32_t L_94 = V_8;
		int32_t L_95 = __this->get_Nk_13();
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, ((int32_t)((int32_t)L_94-(int32_t)L_95)));
		int32_t L_96 = ((int32_t)((int32_t)L_94-(int32_t)L_95));
		uint32_t L_97 = V_9;
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, L_92);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(L_92), (uint32_t)((int32_t)((int32_t)((L_93)->GetAt(static_cast<il2cpp_array_size_t>(L_96)))^(int32_t)L_97)));
		int32_t L_98 = V_8;
		V_8 = ((int32_t)((int32_t)L_98+(int32_t)1));
	}

IL_0212:
	{
		int32_t L_99 = V_8;
		int32_t L_100 = V_3;
		if ((((int32_t)L_99) < ((int32_t)L_100)))
		{
			goto IL_018b;
		}
	}
	{
		bool L_101 = ___encryption;
		if (L_101)
		{
			goto IL_02ef;
		}
	}
	{
		V_11 = 0;
		int32_t L_102 = V_3;
		V_12 = ((int32_t)((int32_t)L_102-(int32_t)4));
		goto IL_0273;
	}

IL_022d:
	{
		V_13 = 0;
		goto IL_025f;
	}

IL_0235:
	{
		UInt32U5BU5D_t2133601851* L_103 = V_4;
		int32_t L_104 = V_11;
		int32_t L_105 = V_13;
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, ((int32_t)((int32_t)L_104+(int32_t)L_105)));
		int32_t L_106 = ((int32_t)((int32_t)L_104+(int32_t)L_105));
		V_14 = ((L_103)->GetAt(static_cast<il2cpp_array_size_t>(L_106)));
		UInt32U5BU5D_t2133601851* L_107 = V_4;
		int32_t L_108 = V_11;
		int32_t L_109 = V_13;
		UInt32U5BU5D_t2133601851* L_110 = V_4;
		int32_t L_111 = V_12;
		int32_t L_112 = V_13;
		NullCheck(L_110);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_110, ((int32_t)((int32_t)L_111+(int32_t)L_112)));
		int32_t L_113 = ((int32_t)((int32_t)L_111+(int32_t)L_112));
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, ((int32_t)((int32_t)L_108+(int32_t)L_109)));
		(L_107)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_108+(int32_t)L_109))), (uint32_t)((L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_113))));
		UInt32U5BU5D_t2133601851* L_114 = V_4;
		int32_t L_115 = V_12;
		int32_t L_116 = V_13;
		uint32_t L_117 = V_14;
		NullCheck(L_114);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_114, ((int32_t)((int32_t)L_115+(int32_t)L_116)));
		(L_114)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_115+(int32_t)L_116))), (uint32_t)L_117);
		int32_t L_118 = V_13;
		V_13 = ((int32_t)((int32_t)L_118+(int32_t)1));
	}

IL_025f:
	{
		int32_t L_119 = V_13;
		if ((((int32_t)L_119) < ((int32_t)4)))
		{
			goto IL_0235;
		}
	}
	{
		int32_t L_120 = V_11;
		V_11 = ((int32_t)((int32_t)L_120+(int32_t)4));
		int32_t L_121 = V_12;
		V_12 = ((int32_t)((int32_t)L_121-(int32_t)4));
	}

IL_0273:
	{
		int32_t L_122 = V_11;
		int32_t L_123 = V_12;
		if ((((int32_t)L_122) < ((int32_t)L_123)))
		{
			goto IL_022d;
		}
	}
	{
		V_15 = 4;
		goto IL_02e2;
	}

IL_0284:
	{
		UInt32U5BU5D_t2133601851* L_124 = V_4;
		int32_t L_125 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2133601851* L_126 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		ByteU5BU5D_t58506160* L_127 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		UInt32U5BU5D_t2133601851* L_128 = V_4;
		int32_t L_129 = V_15;
		NullCheck(L_128);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_128, L_129);
		int32_t L_130 = L_129;
		NullCheck(L_127);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_127, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)((L_128)->GetAt(static_cast<il2cpp_array_size_t>(L_130)))>>((int32_t)24))))));
		uintptr_t L_131 = (((uintptr_t)((int32_t)((uint32_t)((L_128)->GetAt(static_cast<il2cpp_array_size_t>(L_130)))>>((int32_t)24)))));
		NullCheck(L_126);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_126, ((L_127)->GetAt(static_cast<il2cpp_array_size_t>(L_131))));
		uint8_t L_132 = ((L_127)->GetAt(static_cast<il2cpp_array_size_t>(L_131)));
		UInt32U5BU5D_t2133601851* L_133 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		ByteU5BU5D_t58506160* L_134 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		UInt32U5BU5D_t2133601851* L_135 = V_4;
		int32_t L_136 = V_15;
		NullCheck(L_135);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_135, L_136);
		int32_t L_137 = L_136;
		NullCheck(L_134);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_134, (((int32_t)((uint8_t)((int32_t)((uint32_t)((L_135)->GetAt(static_cast<il2cpp_array_size_t>(L_137)))>>((int32_t)16)))))));
		int32_t L_138 = (((int32_t)((uint8_t)((int32_t)((uint32_t)((L_135)->GetAt(static_cast<il2cpp_array_size_t>(L_137)))>>((int32_t)16))))));
		NullCheck(L_133);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_133, ((L_134)->GetAt(static_cast<il2cpp_array_size_t>(L_138))));
		uint8_t L_139 = ((L_134)->GetAt(static_cast<il2cpp_array_size_t>(L_138)));
		UInt32U5BU5D_t2133601851* L_140 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		ByteU5BU5D_t58506160* L_141 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		UInt32U5BU5D_t2133601851* L_142 = V_4;
		int32_t L_143 = V_15;
		NullCheck(L_142);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_142, L_143);
		int32_t L_144 = L_143;
		NullCheck(L_141);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_141, (((int32_t)((uint8_t)((int32_t)((uint32_t)((L_142)->GetAt(static_cast<il2cpp_array_size_t>(L_144)))>>8))))));
		int32_t L_145 = (((int32_t)((uint8_t)((int32_t)((uint32_t)((L_142)->GetAt(static_cast<il2cpp_array_size_t>(L_144)))>>8)))));
		NullCheck(L_140);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_140, ((L_141)->GetAt(static_cast<il2cpp_array_size_t>(L_145))));
		uint8_t L_146 = ((L_141)->GetAt(static_cast<il2cpp_array_size_t>(L_145)));
		UInt32U5BU5D_t2133601851* L_147 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		ByteU5BU5D_t58506160* L_148 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		UInt32U5BU5D_t2133601851* L_149 = V_4;
		int32_t L_150 = V_15;
		NullCheck(L_149);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_149, L_150);
		int32_t L_151 = L_150;
		NullCheck(L_148);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_148, (((int32_t)((uint8_t)((L_149)->GetAt(static_cast<il2cpp_array_size_t>(L_151)))))));
		int32_t L_152 = (((int32_t)((uint8_t)((L_149)->GetAt(static_cast<il2cpp_array_size_t>(L_151))))));
		NullCheck(L_147);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_147, ((L_148)->GetAt(static_cast<il2cpp_array_size_t>(L_152))));
		uint8_t L_153 = ((L_148)->GetAt(static_cast<il2cpp_array_size_t>(L_152)));
		NullCheck(L_124);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_124, L_125);
		(L_124)->SetAt(static_cast<il2cpp_array_size_t>(L_125), (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_126)->GetAt(static_cast<il2cpp_array_size_t>(L_132)))^(int32_t)((L_133)->GetAt(static_cast<il2cpp_array_size_t>(L_139)))))^(int32_t)((L_140)->GetAt(static_cast<il2cpp_array_size_t>(L_146)))))^(int32_t)((L_147)->GetAt(static_cast<il2cpp_array_size_t>(L_153))))));
		int32_t L_154 = V_15;
		V_15 = ((int32_t)((int32_t)L_154+(int32_t)1));
	}

IL_02e2:
	{
		int32_t L_155 = V_15;
		UInt32U5BU5D_t2133601851* L_156 = V_4;
		NullCheck(L_156);
		if ((((int32_t)L_155) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_156)->max_length))))-(int32_t)4)))))
		{
			goto IL_0284;
		}
	}

IL_02ef:
	{
		UInt32U5BU5D_t2133601851* L_157 = V_4;
		__this->set_expandedKey_12(L_157);
		return;
	}
}
// System.Void System.Security.Cryptography.AesTransform::.cctor()
extern TypeInfo* UInt32U5BU5D_t2133601851_il2cpp_TypeInfo_var;
extern TypeInfo* AesTransform_t2186883254_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D1_0_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D2_1_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D3_2_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D4_3_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D5_4_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D6_5_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D7_6_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D8_7_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D9_8_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D10_9_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D11_10_FieldInfo_var;
extern const uint32_t AesTransform__cctor_m1179286690_MetadataUsageId;
extern "C"  void AesTransform__cctor_m1179286690 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AesTransform__cctor_m1179286690_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UInt32U5BU5D_t2133601851* L_0 = ((UInt32U5BU5D_t2133601851*)SZArrayNew(UInt32U5BU5D_t2133601851_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D1_0_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->set_Rcon_15(L_0);
		ByteU5BU5D_t58506160* L_1 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D2_1_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->set_SBox_16(L_1);
		ByteU5BU5D_t58506160* L_2 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D3_2_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->set_iSBox_17(L_2);
		UInt32U5BU5D_t2133601851* L_3 = ((UInt32U5BU5D_t2133601851*)SZArrayNew(UInt32U5BU5D_t2133601851_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D4_3_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->set_T0_18(L_3);
		UInt32U5BU5D_t2133601851* L_4 = ((UInt32U5BU5D_t2133601851*)SZArrayNew(UInt32U5BU5D_t2133601851_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D5_4_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->set_T1_19(L_4);
		UInt32U5BU5D_t2133601851* L_5 = ((UInt32U5BU5D_t2133601851*)SZArrayNew(UInt32U5BU5D_t2133601851_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D6_5_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->set_T2_20(L_5);
		UInt32U5BU5D_t2133601851* L_6 = ((UInt32U5BU5D_t2133601851*)SZArrayNew(UInt32U5BU5D_t2133601851_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D7_6_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->set_T3_21(L_6);
		UInt32U5BU5D_t2133601851* L_7 = ((UInt32U5BU5D_t2133601851*)SZArrayNew(UInt32U5BU5D_t2133601851_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_7, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D8_7_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->set_iT0_22(L_7);
		UInt32U5BU5D_t2133601851* L_8 = ((UInt32U5BU5D_t2133601851*)SZArrayNew(UInt32U5BU5D_t2133601851_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_8, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D9_8_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->set_iT1_23(L_8);
		UInt32U5BU5D_t2133601851* L_9 = ((UInt32U5BU5D_t2133601851*)SZArrayNew(UInt32U5BU5D_t2133601851_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_9, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D10_9_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->set_iT2_24(L_9);
		UInt32U5BU5D_t2133601851* L_10 = ((UInt32U5BU5D_t2133601851*)SZArrayNew(UInt32U5BU5D_t2133601851_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_10, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238935____U24U24fieldU2D11_10_FieldInfo_var), /*hidden argument*/NULL);
		((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->set_iT3_25(L_10);
		return;
	}
}
// System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern "C"  void AesTransform_ECB_m2589908703 (AesTransform_t2186883254 * __this, ByteU5BU5D_t58506160* ___input, ByteU5BU5D_t58506160* ___output, const MethodInfo* method)
{
	{
		bool L_0 = ((SymmetricTransform_t3854241867 *)__this)->get_encrypt_1();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		ByteU5BU5D_t58506160* L_1 = ___input;
		ByteU5BU5D_t58506160* L_2 = ___output;
		UInt32U5BU5D_t2133601851* L_3 = __this->get_expandedKey_12();
		AesTransform_Encrypt128_m3651261907(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		goto IL_002c;
	}

IL_001e:
	{
		ByteU5BU5D_t58506160* L_4 = ___input;
		ByteU5BU5D_t58506160* L_5 = ___output;
		UInt32U5BU5D_t2133601851* L_6 = __this->get_expandedKey_12();
		AesTransform_Decrypt128_m3310750971(__this, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern TypeInfo* AesTransform_t2186883254_il2cpp_TypeInfo_var;
extern const uint32_t AesTransform_SubByte_m1214814958_MetadataUsageId;
extern "C"  uint32_t AesTransform_SubByte_m1214814958 (AesTransform_t2186883254 * __this, uint32_t ___a, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AesTransform_SubByte_m1214814958_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	{
		uint32_t L_0 = ___a;
		V_0 = ((int32_t)((int32_t)((int32_t)255)&(int32_t)L_0));
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		ByteU5BU5D_t58506160* L_1 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)L_2)));
		uintptr_t L_3 = (((uintptr_t)L_2));
		V_1 = ((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3)));
		uint32_t L_4 = ___a;
		V_0 = ((int32_t)((int32_t)((int32_t)255)&(int32_t)((int32_t)((uint32_t)L_4>>8))));
		uint32_t L_5 = V_1;
		ByteU5BU5D_t58506160* L_6 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)L_7)));
		uintptr_t L_8 = (((uintptr_t)L_7));
		V_1 = ((int32_t)((int32_t)L_5|(int32_t)((int32_t)((int32_t)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8)))<<(int32_t)8))));
		uint32_t L_9 = ___a;
		V_0 = ((int32_t)((int32_t)((int32_t)255)&(int32_t)((int32_t)((uint32_t)L_9>>((int32_t)16)))));
		uint32_t L_10 = V_1;
		ByteU5BU5D_t58506160* L_11 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)L_12)));
		uintptr_t L_13 = (((uintptr_t)L_12));
		V_1 = ((int32_t)((int32_t)L_10|(int32_t)((int32_t)((int32_t)((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13)))<<(int32_t)((int32_t)16)))));
		uint32_t L_14 = ___a;
		V_0 = ((int32_t)((int32_t)((int32_t)255)&(int32_t)((int32_t)((uint32_t)L_14>>((int32_t)24)))));
		uint32_t L_15 = V_1;
		ByteU5BU5D_t58506160* L_16 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_17 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)L_17)));
		uintptr_t L_18 = (((uintptr_t)L_17));
		return ((int32_t)((int32_t)L_15|(int32_t)((int32_t)((int32_t)((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18)))<<(int32_t)((int32_t)24)))));
	}
}
// System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern TypeInfo* AesTransform_t2186883254_il2cpp_TypeInfo_var;
extern const uint32_t AesTransform_Encrypt128_m3651261907_MetadataUsageId;
extern "C"  void AesTransform_Encrypt128_m3651261907 (AesTransform_t2186883254 * __this, ByteU5BU5D_t58506160* ___indata, ByteU5BU5D_t58506160* ___outdata, UInt32U5BU5D_t2133601851* ___ekey, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AesTransform_Encrypt128_m3651261907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	uint32_t V_4 = 0;
	uint32_t V_5 = 0;
	uint32_t V_6 = 0;
	uint32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		V_8 = ((int32_t)40);
		ByteU5BU5D_t58506160* L_0 = ___indata;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		ByteU5BU5D_t58506160* L_2 = ___indata;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		int32_t L_3 = 1;
		ByteU5BU5D_t58506160* L_4 = ___indata;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		int32_t L_5 = 2;
		ByteU5BU5D_t58506160* L_6 = ___indata;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		int32_t L_7 = 3;
		UInt32U5BU5D_t2133601851* L_8 = ___ekey;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1)))<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5)))<<(int32_t)8))))|(int32_t)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7)))))^(int32_t)((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9)))));
		ByteU5BU5D_t58506160* L_10 = ___indata;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		int32_t L_11 = 4;
		ByteU5BU5D_t58506160* L_12 = ___indata;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		int32_t L_13 = 5;
		ByteU5BU5D_t58506160* L_14 = ___indata;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 6);
		int32_t L_15 = 6;
		ByteU5BU5D_t58506160* L_16 = ___indata;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 7);
		int32_t L_17 = 7;
		UInt32U5BU5D_t2133601851* L_18 = ___ekey;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		int32_t L_19 = 1;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11)))<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)((L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15)))<<(int32_t)8))))|(int32_t)((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17)))))^(int32_t)((L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19)))));
		ByteU5BU5D_t58506160* L_20 = ___indata;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 8);
		int32_t L_21 = 8;
		ByteU5BU5D_t58506160* L_22 = ___indata;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)9));
		int32_t L_23 = ((int32_t)9);
		ByteU5BU5D_t58506160* L_24 = ___indata;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)10));
		int32_t L_25 = ((int32_t)10);
		ByteU5BU5D_t58506160* L_26 = ___indata;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)11));
		int32_t L_27 = ((int32_t)11);
		UInt32U5BU5D_t2133601851* L_28 = ___ekey;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 2);
		int32_t L_29 = 2;
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21)))<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)((L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25)))<<(int32_t)8))))|(int32_t)((L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27)))))^(int32_t)((L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29)))));
		ByteU5BU5D_t58506160* L_30 = ___indata;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)12));
		int32_t L_31 = ((int32_t)12);
		ByteU5BU5D_t58506160* L_32 = ___indata;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, ((int32_t)13));
		int32_t L_33 = ((int32_t)13);
		ByteU5BU5D_t58506160* L_34 = ___indata;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)14));
		int32_t L_35 = ((int32_t)14);
		ByteU5BU5D_t58506160* L_36 = ___indata;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)15));
		int32_t L_37 = ((int32_t)15);
		UInt32U5BU5D_t2133601851* L_38 = ___ekey;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 3);
		int32_t L_39 = 3;
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31)))<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)((L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_33)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)((L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_35)))<<(int32_t)8))))|(int32_t)((L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37)))))^(int32_t)((L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_39)))));
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2133601851* L_40 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_41 = V_0;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_41>>((int32_t)24))))));
		uintptr_t L_42 = (((uintptr_t)((int32_t)((uint32_t)L_41>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_43 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_44 = V_1;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_44>>((int32_t)16)))))));
		int32_t L_45 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_44>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_46 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_47 = V_2;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_47>>8))))));
		int32_t L_48 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_47>>8)))));
		UInt32U5BU5D_t2133601851* L_49 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_50 = V_3;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, (((int32_t)((uint8_t)L_50))));
		int32_t L_51 = (((int32_t)((uint8_t)L_50)));
		UInt32U5BU5D_t2133601851* L_52 = ___ekey;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 4);
		int32_t L_53 = 4;
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42)))^(int32_t)((L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45)))))^(int32_t)((L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48)))))^(int32_t)((L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51)))))^(int32_t)((L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_53)))));
		UInt32U5BU5D_t2133601851* L_54 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_55 = V_1;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_55>>((int32_t)24))))));
		uintptr_t L_56 = (((uintptr_t)((int32_t)((uint32_t)L_55>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_57 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_58 = V_2;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_58>>((int32_t)16)))))));
		int32_t L_59 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_58>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_60 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_61 = V_3;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_61>>8))))));
		int32_t L_62 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_61>>8)))));
		UInt32U5BU5D_t2133601851* L_63 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_64 = V_0;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, (((int32_t)((uint8_t)L_64))));
		int32_t L_65 = (((int32_t)((uint8_t)L_64)));
		UInt32U5BU5D_t2133601851* L_66 = ___ekey;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, 5);
		int32_t L_67 = 5;
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56)))^(int32_t)((L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59)))))^(int32_t)((L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62)))))^(int32_t)((L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_65)))))^(int32_t)((L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_67)))));
		UInt32U5BU5D_t2133601851* L_68 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_69 = V_2;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_69>>((int32_t)24))))));
		uintptr_t L_70 = (((uintptr_t)((int32_t)((uint32_t)L_69>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_71 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_72 = V_3;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_72>>((int32_t)16)))))));
		int32_t L_73 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_72>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_74 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_75 = V_0;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_75>>8))))));
		int32_t L_76 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_75>>8)))));
		UInt32U5BU5D_t2133601851* L_77 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_78 = V_1;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, (((int32_t)((uint8_t)L_78))));
		int32_t L_79 = (((int32_t)((uint8_t)L_78)));
		UInt32U5BU5D_t2133601851* L_80 = ___ekey;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, 6);
		int32_t L_81 = 6;
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70)))^(int32_t)((L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73)))))^(int32_t)((L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76)))))^(int32_t)((L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_79)))))^(int32_t)((L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_81)))));
		UInt32U5BU5D_t2133601851* L_82 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_83 = V_3;
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_83>>((int32_t)24))))));
		uintptr_t L_84 = (((uintptr_t)((int32_t)((uint32_t)L_83>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_85 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_86 = V_0;
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_86>>((int32_t)16)))))));
		int32_t L_87 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_86>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_88 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_89 = V_1;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_89>>8))))));
		int32_t L_90 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_89>>8)))));
		UInt32U5BU5D_t2133601851* L_91 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_92 = V_2;
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, (((int32_t)((uint8_t)L_92))));
		int32_t L_93 = (((int32_t)((uint8_t)L_92)));
		UInt32U5BU5D_t2133601851* L_94 = ___ekey;
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, 7);
		int32_t L_95 = 7;
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84)))^(int32_t)((L_85)->GetAt(static_cast<il2cpp_array_size_t>(L_87)))))^(int32_t)((L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_90)))))^(int32_t)((L_91)->GetAt(static_cast<il2cpp_array_size_t>(L_93)))))^(int32_t)((L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_95)))));
		UInt32U5BU5D_t2133601851* L_96 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_97 = V_4;
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_97>>((int32_t)24))))));
		uintptr_t L_98 = (((uintptr_t)((int32_t)((uint32_t)L_97>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_99 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_100 = V_5;
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_100>>((int32_t)16)))))));
		int32_t L_101 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_100>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_102 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_103 = V_6;
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_103>>8))))));
		int32_t L_104 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_103>>8)))));
		UInt32U5BU5D_t2133601851* L_105 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_106 = V_7;
		NullCheck(L_105);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_105, (((int32_t)((uint8_t)L_106))));
		int32_t L_107 = (((int32_t)((uint8_t)L_106)));
		UInt32U5BU5D_t2133601851* L_108 = ___ekey;
		NullCheck(L_108);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_108, 8);
		int32_t L_109 = 8;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_98)))^(int32_t)((L_99)->GetAt(static_cast<il2cpp_array_size_t>(L_101)))))^(int32_t)((L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104)))))^(int32_t)((L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_107)))))^(int32_t)((L_108)->GetAt(static_cast<il2cpp_array_size_t>(L_109)))));
		UInt32U5BU5D_t2133601851* L_110 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_111 = V_5;
		NullCheck(L_110);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_110, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_111>>((int32_t)24))))));
		uintptr_t L_112 = (((uintptr_t)((int32_t)((uint32_t)L_111>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_113 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_114 = V_6;
		NullCheck(L_113);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_113, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_114>>((int32_t)16)))))));
		int32_t L_115 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_114>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_116 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_117 = V_7;
		NullCheck(L_116);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_116, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_117>>8))))));
		int32_t L_118 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_117>>8)))));
		UInt32U5BU5D_t2133601851* L_119 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_120 = V_4;
		NullCheck(L_119);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_119, (((int32_t)((uint8_t)L_120))));
		int32_t L_121 = (((int32_t)((uint8_t)L_120)));
		UInt32U5BU5D_t2133601851* L_122 = ___ekey;
		NullCheck(L_122);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_122, ((int32_t)9));
		int32_t L_123 = ((int32_t)9);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_112)))^(int32_t)((L_113)->GetAt(static_cast<il2cpp_array_size_t>(L_115)))))^(int32_t)((L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_118)))))^(int32_t)((L_119)->GetAt(static_cast<il2cpp_array_size_t>(L_121)))))^(int32_t)((L_122)->GetAt(static_cast<il2cpp_array_size_t>(L_123)))));
		UInt32U5BU5D_t2133601851* L_124 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_125 = V_6;
		NullCheck(L_124);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_124, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_125>>((int32_t)24))))));
		uintptr_t L_126 = (((uintptr_t)((int32_t)((uint32_t)L_125>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_127 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_128 = V_7;
		NullCheck(L_127);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_127, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_128>>((int32_t)16)))))));
		int32_t L_129 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_128>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_130 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_131 = V_4;
		NullCheck(L_130);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_130, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_131>>8))))));
		int32_t L_132 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_131>>8)))));
		UInt32U5BU5D_t2133601851* L_133 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_134 = V_5;
		NullCheck(L_133);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_133, (((int32_t)((uint8_t)L_134))));
		int32_t L_135 = (((int32_t)((uint8_t)L_134)));
		UInt32U5BU5D_t2133601851* L_136 = ___ekey;
		NullCheck(L_136);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_136, ((int32_t)10));
		int32_t L_137 = ((int32_t)10);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_124)->GetAt(static_cast<il2cpp_array_size_t>(L_126)))^(int32_t)((L_127)->GetAt(static_cast<il2cpp_array_size_t>(L_129)))))^(int32_t)((L_130)->GetAt(static_cast<il2cpp_array_size_t>(L_132)))))^(int32_t)((L_133)->GetAt(static_cast<il2cpp_array_size_t>(L_135)))))^(int32_t)((L_136)->GetAt(static_cast<il2cpp_array_size_t>(L_137)))));
		UInt32U5BU5D_t2133601851* L_138 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_139 = V_7;
		NullCheck(L_138);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_138, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_139>>((int32_t)24))))));
		uintptr_t L_140 = (((uintptr_t)((int32_t)((uint32_t)L_139>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_141 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_142 = V_4;
		NullCheck(L_141);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_141, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_142>>((int32_t)16)))))));
		int32_t L_143 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_142>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_144 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_145 = V_5;
		NullCheck(L_144);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_144, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_145>>8))))));
		int32_t L_146 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_145>>8)))));
		UInt32U5BU5D_t2133601851* L_147 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_148 = V_6;
		NullCheck(L_147);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_147, (((int32_t)((uint8_t)L_148))));
		int32_t L_149 = (((int32_t)((uint8_t)L_148)));
		UInt32U5BU5D_t2133601851* L_150 = ___ekey;
		NullCheck(L_150);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_150, ((int32_t)11));
		int32_t L_151 = ((int32_t)11);
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_138)->GetAt(static_cast<il2cpp_array_size_t>(L_140)))^(int32_t)((L_141)->GetAt(static_cast<il2cpp_array_size_t>(L_143)))))^(int32_t)((L_144)->GetAt(static_cast<il2cpp_array_size_t>(L_146)))))^(int32_t)((L_147)->GetAt(static_cast<il2cpp_array_size_t>(L_149)))))^(int32_t)((L_150)->GetAt(static_cast<il2cpp_array_size_t>(L_151)))));
		UInt32U5BU5D_t2133601851* L_152 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_153 = V_0;
		NullCheck(L_152);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_152, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_153>>((int32_t)24))))));
		uintptr_t L_154 = (((uintptr_t)((int32_t)((uint32_t)L_153>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_155 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_156 = V_1;
		NullCheck(L_155);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_155, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_156>>((int32_t)16)))))));
		int32_t L_157 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_156>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_158 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_159 = V_2;
		NullCheck(L_158);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_158, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_159>>8))))));
		int32_t L_160 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_159>>8)))));
		UInt32U5BU5D_t2133601851* L_161 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_162 = V_3;
		NullCheck(L_161);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_161, (((int32_t)((uint8_t)L_162))));
		int32_t L_163 = (((int32_t)((uint8_t)L_162)));
		UInt32U5BU5D_t2133601851* L_164 = ___ekey;
		NullCheck(L_164);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_164, ((int32_t)12));
		int32_t L_165 = ((int32_t)12);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_154)))^(int32_t)((L_155)->GetAt(static_cast<il2cpp_array_size_t>(L_157)))))^(int32_t)((L_158)->GetAt(static_cast<il2cpp_array_size_t>(L_160)))))^(int32_t)((L_161)->GetAt(static_cast<il2cpp_array_size_t>(L_163)))))^(int32_t)((L_164)->GetAt(static_cast<il2cpp_array_size_t>(L_165)))));
		UInt32U5BU5D_t2133601851* L_166 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_167 = V_1;
		NullCheck(L_166);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_166, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_167>>((int32_t)24))))));
		uintptr_t L_168 = (((uintptr_t)((int32_t)((uint32_t)L_167>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_169 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_170 = V_2;
		NullCheck(L_169);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_169, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_170>>((int32_t)16)))))));
		int32_t L_171 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_170>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_172 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_173 = V_3;
		NullCheck(L_172);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_172, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_173>>8))))));
		int32_t L_174 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_173>>8)))));
		UInt32U5BU5D_t2133601851* L_175 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_176 = V_0;
		NullCheck(L_175);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_175, (((int32_t)((uint8_t)L_176))));
		int32_t L_177 = (((int32_t)((uint8_t)L_176)));
		UInt32U5BU5D_t2133601851* L_178 = ___ekey;
		NullCheck(L_178);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_178, ((int32_t)13));
		int32_t L_179 = ((int32_t)13);
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_166)->GetAt(static_cast<il2cpp_array_size_t>(L_168)))^(int32_t)((L_169)->GetAt(static_cast<il2cpp_array_size_t>(L_171)))))^(int32_t)((L_172)->GetAt(static_cast<il2cpp_array_size_t>(L_174)))))^(int32_t)((L_175)->GetAt(static_cast<il2cpp_array_size_t>(L_177)))))^(int32_t)((L_178)->GetAt(static_cast<il2cpp_array_size_t>(L_179)))));
		UInt32U5BU5D_t2133601851* L_180 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_181 = V_2;
		NullCheck(L_180);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_180, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_181>>((int32_t)24))))));
		uintptr_t L_182 = (((uintptr_t)((int32_t)((uint32_t)L_181>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_183 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_184 = V_3;
		NullCheck(L_183);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_183, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_184>>((int32_t)16)))))));
		int32_t L_185 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_184>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_186 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_187 = V_0;
		NullCheck(L_186);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_186, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_187>>8))))));
		int32_t L_188 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_187>>8)))));
		UInt32U5BU5D_t2133601851* L_189 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_190 = V_1;
		NullCheck(L_189);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_189, (((int32_t)((uint8_t)L_190))));
		int32_t L_191 = (((int32_t)((uint8_t)L_190)));
		UInt32U5BU5D_t2133601851* L_192 = ___ekey;
		NullCheck(L_192);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_192, ((int32_t)14));
		int32_t L_193 = ((int32_t)14);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_180)->GetAt(static_cast<il2cpp_array_size_t>(L_182)))^(int32_t)((L_183)->GetAt(static_cast<il2cpp_array_size_t>(L_185)))))^(int32_t)((L_186)->GetAt(static_cast<il2cpp_array_size_t>(L_188)))))^(int32_t)((L_189)->GetAt(static_cast<il2cpp_array_size_t>(L_191)))))^(int32_t)((L_192)->GetAt(static_cast<il2cpp_array_size_t>(L_193)))));
		UInt32U5BU5D_t2133601851* L_194 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_195 = V_3;
		NullCheck(L_194);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_194, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_195>>((int32_t)24))))));
		uintptr_t L_196 = (((uintptr_t)((int32_t)((uint32_t)L_195>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_197 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_198 = V_0;
		NullCheck(L_197);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_197, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_198>>((int32_t)16)))))));
		int32_t L_199 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_198>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_200 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_201 = V_1;
		NullCheck(L_200);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_200, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_201>>8))))));
		int32_t L_202 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_201>>8)))));
		UInt32U5BU5D_t2133601851* L_203 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_204 = V_2;
		NullCheck(L_203);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_203, (((int32_t)((uint8_t)L_204))));
		int32_t L_205 = (((int32_t)((uint8_t)L_204)));
		UInt32U5BU5D_t2133601851* L_206 = ___ekey;
		NullCheck(L_206);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_206, ((int32_t)15));
		int32_t L_207 = ((int32_t)15);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_194)->GetAt(static_cast<il2cpp_array_size_t>(L_196)))^(int32_t)((L_197)->GetAt(static_cast<il2cpp_array_size_t>(L_199)))))^(int32_t)((L_200)->GetAt(static_cast<il2cpp_array_size_t>(L_202)))))^(int32_t)((L_203)->GetAt(static_cast<il2cpp_array_size_t>(L_205)))))^(int32_t)((L_206)->GetAt(static_cast<il2cpp_array_size_t>(L_207)))));
		UInt32U5BU5D_t2133601851* L_208 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_209 = V_4;
		NullCheck(L_208);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_208, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_209>>((int32_t)24))))));
		uintptr_t L_210 = (((uintptr_t)((int32_t)((uint32_t)L_209>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_211 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_212 = V_5;
		NullCheck(L_211);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_211, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_212>>((int32_t)16)))))));
		int32_t L_213 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_212>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_214 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_215 = V_6;
		NullCheck(L_214);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_214, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_215>>8))))));
		int32_t L_216 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_215>>8)))));
		UInt32U5BU5D_t2133601851* L_217 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_218 = V_7;
		NullCheck(L_217);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_217, (((int32_t)((uint8_t)L_218))));
		int32_t L_219 = (((int32_t)((uint8_t)L_218)));
		UInt32U5BU5D_t2133601851* L_220 = ___ekey;
		NullCheck(L_220);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_220, ((int32_t)16));
		int32_t L_221 = ((int32_t)16);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_208)->GetAt(static_cast<il2cpp_array_size_t>(L_210)))^(int32_t)((L_211)->GetAt(static_cast<il2cpp_array_size_t>(L_213)))))^(int32_t)((L_214)->GetAt(static_cast<il2cpp_array_size_t>(L_216)))))^(int32_t)((L_217)->GetAt(static_cast<il2cpp_array_size_t>(L_219)))))^(int32_t)((L_220)->GetAt(static_cast<il2cpp_array_size_t>(L_221)))));
		UInt32U5BU5D_t2133601851* L_222 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_223 = V_5;
		NullCheck(L_222);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_222, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_223>>((int32_t)24))))));
		uintptr_t L_224 = (((uintptr_t)((int32_t)((uint32_t)L_223>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_225 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_226 = V_6;
		NullCheck(L_225);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_225, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_226>>((int32_t)16)))))));
		int32_t L_227 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_226>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_228 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_229 = V_7;
		NullCheck(L_228);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_228, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_229>>8))))));
		int32_t L_230 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_229>>8)))));
		UInt32U5BU5D_t2133601851* L_231 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_232 = V_4;
		NullCheck(L_231);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_231, (((int32_t)((uint8_t)L_232))));
		int32_t L_233 = (((int32_t)((uint8_t)L_232)));
		UInt32U5BU5D_t2133601851* L_234 = ___ekey;
		NullCheck(L_234);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_234, ((int32_t)17));
		int32_t L_235 = ((int32_t)17);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_222)->GetAt(static_cast<il2cpp_array_size_t>(L_224)))^(int32_t)((L_225)->GetAt(static_cast<il2cpp_array_size_t>(L_227)))))^(int32_t)((L_228)->GetAt(static_cast<il2cpp_array_size_t>(L_230)))))^(int32_t)((L_231)->GetAt(static_cast<il2cpp_array_size_t>(L_233)))))^(int32_t)((L_234)->GetAt(static_cast<il2cpp_array_size_t>(L_235)))));
		UInt32U5BU5D_t2133601851* L_236 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_237 = V_6;
		NullCheck(L_236);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_236, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_237>>((int32_t)24))))));
		uintptr_t L_238 = (((uintptr_t)((int32_t)((uint32_t)L_237>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_239 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_240 = V_7;
		NullCheck(L_239);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_239, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_240>>((int32_t)16)))))));
		int32_t L_241 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_240>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_242 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_243 = V_4;
		NullCheck(L_242);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_242, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_243>>8))))));
		int32_t L_244 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_243>>8)))));
		UInt32U5BU5D_t2133601851* L_245 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_246 = V_5;
		NullCheck(L_245);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_245, (((int32_t)((uint8_t)L_246))));
		int32_t L_247 = (((int32_t)((uint8_t)L_246)));
		UInt32U5BU5D_t2133601851* L_248 = ___ekey;
		NullCheck(L_248);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_248, ((int32_t)18));
		int32_t L_249 = ((int32_t)18);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_236)->GetAt(static_cast<il2cpp_array_size_t>(L_238)))^(int32_t)((L_239)->GetAt(static_cast<il2cpp_array_size_t>(L_241)))))^(int32_t)((L_242)->GetAt(static_cast<il2cpp_array_size_t>(L_244)))))^(int32_t)((L_245)->GetAt(static_cast<il2cpp_array_size_t>(L_247)))))^(int32_t)((L_248)->GetAt(static_cast<il2cpp_array_size_t>(L_249)))));
		UInt32U5BU5D_t2133601851* L_250 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_251 = V_7;
		NullCheck(L_250);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_250, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_251>>((int32_t)24))))));
		uintptr_t L_252 = (((uintptr_t)((int32_t)((uint32_t)L_251>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_253 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_254 = V_4;
		NullCheck(L_253);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_253, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_254>>((int32_t)16)))))));
		int32_t L_255 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_254>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_256 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_257 = V_5;
		NullCheck(L_256);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_256, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_257>>8))))));
		int32_t L_258 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_257>>8)))));
		UInt32U5BU5D_t2133601851* L_259 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_260 = V_6;
		NullCheck(L_259);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_259, (((int32_t)((uint8_t)L_260))));
		int32_t L_261 = (((int32_t)((uint8_t)L_260)));
		UInt32U5BU5D_t2133601851* L_262 = ___ekey;
		NullCheck(L_262);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_262, ((int32_t)19));
		int32_t L_263 = ((int32_t)19);
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_250)->GetAt(static_cast<il2cpp_array_size_t>(L_252)))^(int32_t)((L_253)->GetAt(static_cast<il2cpp_array_size_t>(L_255)))))^(int32_t)((L_256)->GetAt(static_cast<il2cpp_array_size_t>(L_258)))))^(int32_t)((L_259)->GetAt(static_cast<il2cpp_array_size_t>(L_261)))))^(int32_t)((L_262)->GetAt(static_cast<il2cpp_array_size_t>(L_263)))));
		UInt32U5BU5D_t2133601851* L_264 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_265 = V_0;
		NullCheck(L_264);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_264, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_265>>((int32_t)24))))));
		uintptr_t L_266 = (((uintptr_t)((int32_t)((uint32_t)L_265>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_267 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_268 = V_1;
		NullCheck(L_267);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_267, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_268>>((int32_t)16)))))));
		int32_t L_269 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_268>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_270 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_271 = V_2;
		NullCheck(L_270);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_270, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_271>>8))))));
		int32_t L_272 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_271>>8)))));
		UInt32U5BU5D_t2133601851* L_273 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_274 = V_3;
		NullCheck(L_273);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_273, (((int32_t)((uint8_t)L_274))));
		int32_t L_275 = (((int32_t)((uint8_t)L_274)));
		UInt32U5BU5D_t2133601851* L_276 = ___ekey;
		NullCheck(L_276);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_276, ((int32_t)20));
		int32_t L_277 = ((int32_t)20);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_264)->GetAt(static_cast<il2cpp_array_size_t>(L_266)))^(int32_t)((L_267)->GetAt(static_cast<il2cpp_array_size_t>(L_269)))))^(int32_t)((L_270)->GetAt(static_cast<il2cpp_array_size_t>(L_272)))))^(int32_t)((L_273)->GetAt(static_cast<il2cpp_array_size_t>(L_275)))))^(int32_t)((L_276)->GetAt(static_cast<il2cpp_array_size_t>(L_277)))));
		UInt32U5BU5D_t2133601851* L_278 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_279 = V_1;
		NullCheck(L_278);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_278, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_279>>((int32_t)24))))));
		uintptr_t L_280 = (((uintptr_t)((int32_t)((uint32_t)L_279>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_281 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_282 = V_2;
		NullCheck(L_281);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_281, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_282>>((int32_t)16)))))));
		int32_t L_283 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_282>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_284 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_285 = V_3;
		NullCheck(L_284);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_284, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_285>>8))))));
		int32_t L_286 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_285>>8)))));
		UInt32U5BU5D_t2133601851* L_287 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_288 = V_0;
		NullCheck(L_287);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_287, (((int32_t)((uint8_t)L_288))));
		int32_t L_289 = (((int32_t)((uint8_t)L_288)));
		UInt32U5BU5D_t2133601851* L_290 = ___ekey;
		NullCheck(L_290);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_290, ((int32_t)21));
		int32_t L_291 = ((int32_t)21);
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_278)->GetAt(static_cast<il2cpp_array_size_t>(L_280)))^(int32_t)((L_281)->GetAt(static_cast<il2cpp_array_size_t>(L_283)))))^(int32_t)((L_284)->GetAt(static_cast<il2cpp_array_size_t>(L_286)))))^(int32_t)((L_287)->GetAt(static_cast<il2cpp_array_size_t>(L_289)))))^(int32_t)((L_290)->GetAt(static_cast<il2cpp_array_size_t>(L_291)))));
		UInt32U5BU5D_t2133601851* L_292 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_293 = V_2;
		NullCheck(L_292);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_292, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_293>>((int32_t)24))))));
		uintptr_t L_294 = (((uintptr_t)((int32_t)((uint32_t)L_293>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_295 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_296 = V_3;
		NullCheck(L_295);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_295, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_296>>((int32_t)16)))))));
		int32_t L_297 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_296>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_298 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_299 = V_0;
		NullCheck(L_298);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_298, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_299>>8))))));
		int32_t L_300 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_299>>8)))));
		UInt32U5BU5D_t2133601851* L_301 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_302 = V_1;
		NullCheck(L_301);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_301, (((int32_t)((uint8_t)L_302))));
		int32_t L_303 = (((int32_t)((uint8_t)L_302)));
		UInt32U5BU5D_t2133601851* L_304 = ___ekey;
		NullCheck(L_304);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_304, ((int32_t)22));
		int32_t L_305 = ((int32_t)22);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_292)->GetAt(static_cast<il2cpp_array_size_t>(L_294)))^(int32_t)((L_295)->GetAt(static_cast<il2cpp_array_size_t>(L_297)))))^(int32_t)((L_298)->GetAt(static_cast<il2cpp_array_size_t>(L_300)))))^(int32_t)((L_301)->GetAt(static_cast<il2cpp_array_size_t>(L_303)))))^(int32_t)((L_304)->GetAt(static_cast<il2cpp_array_size_t>(L_305)))));
		UInt32U5BU5D_t2133601851* L_306 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_307 = V_3;
		NullCheck(L_306);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_306, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_307>>((int32_t)24))))));
		uintptr_t L_308 = (((uintptr_t)((int32_t)((uint32_t)L_307>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_309 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_310 = V_0;
		NullCheck(L_309);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_309, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_310>>((int32_t)16)))))));
		int32_t L_311 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_310>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_312 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_313 = V_1;
		NullCheck(L_312);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_312, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_313>>8))))));
		int32_t L_314 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_313>>8)))));
		UInt32U5BU5D_t2133601851* L_315 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_316 = V_2;
		NullCheck(L_315);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_315, (((int32_t)((uint8_t)L_316))));
		int32_t L_317 = (((int32_t)((uint8_t)L_316)));
		UInt32U5BU5D_t2133601851* L_318 = ___ekey;
		NullCheck(L_318);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_318, ((int32_t)23));
		int32_t L_319 = ((int32_t)23);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_306)->GetAt(static_cast<il2cpp_array_size_t>(L_308)))^(int32_t)((L_309)->GetAt(static_cast<il2cpp_array_size_t>(L_311)))))^(int32_t)((L_312)->GetAt(static_cast<il2cpp_array_size_t>(L_314)))))^(int32_t)((L_315)->GetAt(static_cast<il2cpp_array_size_t>(L_317)))))^(int32_t)((L_318)->GetAt(static_cast<il2cpp_array_size_t>(L_319)))));
		UInt32U5BU5D_t2133601851* L_320 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_321 = V_4;
		NullCheck(L_320);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_320, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_321>>((int32_t)24))))));
		uintptr_t L_322 = (((uintptr_t)((int32_t)((uint32_t)L_321>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_323 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_324 = V_5;
		NullCheck(L_323);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_323, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_324>>((int32_t)16)))))));
		int32_t L_325 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_324>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_326 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_327 = V_6;
		NullCheck(L_326);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_326, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_327>>8))))));
		int32_t L_328 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_327>>8)))));
		UInt32U5BU5D_t2133601851* L_329 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_330 = V_7;
		NullCheck(L_329);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_329, (((int32_t)((uint8_t)L_330))));
		int32_t L_331 = (((int32_t)((uint8_t)L_330)));
		UInt32U5BU5D_t2133601851* L_332 = ___ekey;
		NullCheck(L_332);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_332, ((int32_t)24));
		int32_t L_333 = ((int32_t)24);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_320)->GetAt(static_cast<il2cpp_array_size_t>(L_322)))^(int32_t)((L_323)->GetAt(static_cast<il2cpp_array_size_t>(L_325)))))^(int32_t)((L_326)->GetAt(static_cast<il2cpp_array_size_t>(L_328)))))^(int32_t)((L_329)->GetAt(static_cast<il2cpp_array_size_t>(L_331)))))^(int32_t)((L_332)->GetAt(static_cast<il2cpp_array_size_t>(L_333)))));
		UInt32U5BU5D_t2133601851* L_334 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_335 = V_5;
		NullCheck(L_334);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_334, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_335>>((int32_t)24))))));
		uintptr_t L_336 = (((uintptr_t)((int32_t)((uint32_t)L_335>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_337 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_338 = V_6;
		NullCheck(L_337);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_337, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_338>>((int32_t)16)))))));
		int32_t L_339 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_338>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_340 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_341 = V_7;
		NullCheck(L_340);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_340, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_341>>8))))));
		int32_t L_342 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_341>>8)))));
		UInt32U5BU5D_t2133601851* L_343 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_344 = V_4;
		NullCheck(L_343);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_343, (((int32_t)((uint8_t)L_344))));
		int32_t L_345 = (((int32_t)((uint8_t)L_344)));
		UInt32U5BU5D_t2133601851* L_346 = ___ekey;
		NullCheck(L_346);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_346, ((int32_t)25));
		int32_t L_347 = ((int32_t)25);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_334)->GetAt(static_cast<il2cpp_array_size_t>(L_336)))^(int32_t)((L_337)->GetAt(static_cast<il2cpp_array_size_t>(L_339)))))^(int32_t)((L_340)->GetAt(static_cast<il2cpp_array_size_t>(L_342)))))^(int32_t)((L_343)->GetAt(static_cast<il2cpp_array_size_t>(L_345)))))^(int32_t)((L_346)->GetAt(static_cast<il2cpp_array_size_t>(L_347)))));
		UInt32U5BU5D_t2133601851* L_348 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_349 = V_6;
		NullCheck(L_348);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_348, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_349>>((int32_t)24))))));
		uintptr_t L_350 = (((uintptr_t)((int32_t)((uint32_t)L_349>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_351 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_352 = V_7;
		NullCheck(L_351);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_351, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_352>>((int32_t)16)))))));
		int32_t L_353 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_352>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_354 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_355 = V_4;
		NullCheck(L_354);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_354, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_355>>8))))));
		int32_t L_356 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_355>>8)))));
		UInt32U5BU5D_t2133601851* L_357 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_358 = V_5;
		NullCheck(L_357);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_357, (((int32_t)((uint8_t)L_358))));
		int32_t L_359 = (((int32_t)((uint8_t)L_358)));
		UInt32U5BU5D_t2133601851* L_360 = ___ekey;
		NullCheck(L_360);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_360, ((int32_t)26));
		int32_t L_361 = ((int32_t)26);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_348)->GetAt(static_cast<il2cpp_array_size_t>(L_350)))^(int32_t)((L_351)->GetAt(static_cast<il2cpp_array_size_t>(L_353)))))^(int32_t)((L_354)->GetAt(static_cast<il2cpp_array_size_t>(L_356)))))^(int32_t)((L_357)->GetAt(static_cast<il2cpp_array_size_t>(L_359)))))^(int32_t)((L_360)->GetAt(static_cast<il2cpp_array_size_t>(L_361)))));
		UInt32U5BU5D_t2133601851* L_362 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_363 = V_7;
		NullCheck(L_362);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_362, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_363>>((int32_t)24))))));
		uintptr_t L_364 = (((uintptr_t)((int32_t)((uint32_t)L_363>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_365 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_366 = V_4;
		NullCheck(L_365);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_365, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_366>>((int32_t)16)))))));
		int32_t L_367 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_366>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_368 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_369 = V_5;
		NullCheck(L_368);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_368, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_369>>8))))));
		int32_t L_370 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_369>>8)))));
		UInt32U5BU5D_t2133601851* L_371 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_372 = V_6;
		NullCheck(L_371);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_371, (((int32_t)((uint8_t)L_372))));
		int32_t L_373 = (((int32_t)((uint8_t)L_372)));
		UInt32U5BU5D_t2133601851* L_374 = ___ekey;
		NullCheck(L_374);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_374, ((int32_t)27));
		int32_t L_375 = ((int32_t)27);
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_362)->GetAt(static_cast<il2cpp_array_size_t>(L_364)))^(int32_t)((L_365)->GetAt(static_cast<il2cpp_array_size_t>(L_367)))))^(int32_t)((L_368)->GetAt(static_cast<il2cpp_array_size_t>(L_370)))))^(int32_t)((L_371)->GetAt(static_cast<il2cpp_array_size_t>(L_373)))))^(int32_t)((L_374)->GetAt(static_cast<il2cpp_array_size_t>(L_375)))));
		UInt32U5BU5D_t2133601851* L_376 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_377 = V_0;
		NullCheck(L_376);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_376, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_377>>((int32_t)24))))));
		uintptr_t L_378 = (((uintptr_t)((int32_t)((uint32_t)L_377>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_379 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_380 = V_1;
		NullCheck(L_379);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_379, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_380>>((int32_t)16)))))));
		int32_t L_381 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_380>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_382 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_383 = V_2;
		NullCheck(L_382);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_382, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_383>>8))))));
		int32_t L_384 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_383>>8)))));
		UInt32U5BU5D_t2133601851* L_385 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_386 = V_3;
		NullCheck(L_385);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_385, (((int32_t)((uint8_t)L_386))));
		int32_t L_387 = (((int32_t)((uint8_t)L_386)));
		UInt32U5BU5D_t2133601851* L_388 = ___ekey;
		NullCheck(L_388);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_388, ((int32_t)28));
		int32_t L_389 = ((int32_t)28);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_376)->GetAt(static_cast<il2cpp_array_size_t>(L_378)))^(int32_t)((L_379)->GetAt(static_cast<il2cpp_array_size_t>(L_381)))))^(int32_t)((L_382)->GetAt(static_cast<il2cpp_array_size_t>(L_384)))))^(int32_t)((L_385)->GetAt(static_cast<il2cpp_array_size_t>(L_387)))))^(int32_t)((L_388)->GetAt(static_cast<il2cpp_array_size_t>(L_389)))));
		UInt32U5BU5D_t2133601851* L_390 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_391 = V_1;
		NullCheck(L_390);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_390, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_391>>((int32_t)24))))));
		uintptr_t L_392 = (((uintptr_t)((int32_t)((uint32_t)L_391>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_393 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_394 = V_2;
		NullCheck(L_393);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_393, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_394>>((int32_t)16)))))));
		int32_t L_395 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_394>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_396 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_397 = V_3;
		NullCheck(L_396);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_396, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_397>>8))))));
		int32_t L_398 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_397>>8)))));
		UInt32U5BU5D_t2133601851* L_399 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_400 = V_0;
		NullCheck(L_399);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_399, (((int32_t)((uint8_t)L_400))));
		int32_t L_401 = (((int32_t)((uint8_t)L_400)));
		UInt32U5BU5D_t2133601851* L_402 = ___ekey;
		NullCheck(L_402);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_402, ((int32_t)29));
		int32_t L_403 = ((int32_t)29);
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_390)->GetAt(static_cast<il2cpp_array_size_t>(L_392)))^(int32_t)((L_393)->GetAt(static_cast<il2cpp_array_size_t>(L_395)))))^(int32_t)((L_396)->GetAt(static_cast<il2cpp_array_size_t>(L_398)))))^(int32_t)((L_399)->GetAt(static_cast<il2cpp_array_size_t>(L_401)))))^(int32_t)((L_402)->GetAt(static_cast<il2cpp_array_size_t>(L_403)))));
		UInt32U5BU5D_t2133601851* L_404 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_405 = V_2;
		NullCheck(L_404);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_404, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_405>>((int32_t)24))))));
		uintptr_t L_406 = (((uintptr_t)((int32_t)((uint32_t)L_405>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_407 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_408 = V_3;
		NullCheck(L_407);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_407, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_408>>((int32_t)16)))))));
		int32_t L_409 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_408>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_410 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_411 = V_0;
		NullCheck(L_410);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_410, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_411>>8))))));
		int32_t L_412 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_411>>8)))));
		UInt32U5BU5D_t2133601851* L_413 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_414 = V_1;
		NullCheck(L_413);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_413, (((int32_t)((uint8_t)L_414))));
		int32_t L_415 = (((int32_t)((uint8_t)L_414)));
		UInt32U5BU5D_t2133601851* L_416 = ___ekey;
		NullCheck(L_416);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_416, ((int32_t)30));
		int32_t L_417 = ((int32_t)30);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_404)->GetAt(static_cast<il2cpp_array_size_t>(L_406)))^(int32_t)((L_407)->GetAt(static_cast<il2cpp_array_size_t>(L_409)))))^(int32_t)((L_410)->GetAt(static_cast<il2cpp_array_size_t>(L_412)))))^(int32_t)((L_413)->GetAt(static_cast<il2cpp_array_size_t>(L_415)))))^(int32_t)((L_416)->GetAt(static_cast<il2cpp_array_size_t>(L_417)))));
		UInt32U5BU5D_t2133601851* L_418 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_419 = V_3;
		NullCheck(L_418);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_418, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_419>>((int32_t)24))))));
		uintptr_t L_420 = (((uintptr_t)((int32_t)((uint32_t)L_419>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_421 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_422 = V_0;
		NullCheck(L_421);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_421, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_422>>((int32_t)16)))))));
		int32_t L_423 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_422>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_424 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_425 = V_1;
		NullCheck(L_424);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_424, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_425>>8))))));
		int32_t L_426 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_425>>8)))));
		UInt32U5BU5D_t2133601851* L_427 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_428 = V_2;
		NullCheck(L_427);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_427, (((int32_t)((uint8_t)L_428))));
		int32_t L_429 = (((int32_t)((uint8_t)L_428)));
		UInt32U5BU5D_t2133601851* L_430 = ___ekey;
		NullCheck(L_430);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_430, ((int32_t)31));
		int32_t L_431 = ((int32_t)31);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_418)->GetAt(static_cast<il2cpp_array_size_t>(L_420)))^(int32_t)((L_421)->GetAt(static_cast<il2cpp_array_size_t>(L_423)))))^(int32_t)((L_424)->GetAt(static_cast<il2cpp_array_size_t>(L_426)))))^(int32_t)((L_427)->GetAt(static_cast<il2cpp_array_size_t>(L_429)))))^(int32_t)((L_430)->GetAt(static_cast<il2cpp_array_size_t>(L_431)))));
		UInt32U5BU5D_t2133601851* L_432 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_433 = V_4;
		NullCheck(L_432);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_432, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_433>>((int32_t)24))))));
		uintptr_t L_434 = (((uintptr_t)((int32_t)((uint32_t)L_433>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_435 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_436 = V_5;
		NullCheck(L_435);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_435, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_436>>((int32_t)16)))))));
		int32_t L_437 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_436>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_438 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_439 = V_6;
		NullCheck(L_438);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_438, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_439>>8))))));
		int32_t L_440 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_439>>8)))));
		UInt32U5BU5D_t2133601851* L_441 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_442 = V_7;
		NullCheck(L_441);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_441, (((int32_t)((uint8_t)L_442))));
		int32_t L_443 = (((int32_t)((uint8_t)L_442)));
		UInt32U5BU5D_t2133601851* L_444 = ___ekey;
		NullCheck(L_444);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_444, ((int32_t)32));
		int32_t L_445 = ((int32_t)32);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_432)->GetAt(static_cast<il2cpp_array_size_t>(L_434)))^(int32_t)((L_435)->GetAt(static_cast<il2cpp_array_size_t>(L_437)))))^(int32_t)((L_438)->GetAt(static_cast<il2cpp_array_size_t>(L_440)))))^(int32_t)((L_441)->GetAt(static_cast<il2cpp_array_size_t>(L_443)))))^(int32_t)((L_444)->GetAt(static_cast<il2cpp_array_size_t>(L_445)))));
		UInt32U5BU5D_t2133601851* L_446 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_447 = V_5;
		NullCheck(L_446);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_446, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_447>>((int32_t)24))))));
		uintptr_t L_448 = (((uintptr_t)((int32_t)((uint32_t)L_447>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_449 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_450 = V_6;
		NullCheck(L_449);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_449, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_450>>((int32_t)16)))))));
		int32_t L_451 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_450>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_452 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_453 = V_7;
		NullCheck(L_452);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_452, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_453>>8))))));
		int32_t L_454 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_453>>8)))));
		UInt32U5BU5D_t2133601851* L_455 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_456 = V_4;
		NullCheck(L_455);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_455, (((int32_t)((uint8_t)L_456))));
		int32_t L_457 = (((int32_t)((uint8_t)L_456)));
		UInt32U5BU5D_t2133601851* L_458 = ___ekey;
		NullCheck(L_458);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_458, ((int32_t)33));
		int32_t L_459 = ((int32_t)33);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_446)->GetAt(static_cast<il2cpp_array_size_t>(L_448)))^(int32_t)((L_449)->GetAt(static_cast<il2cpp_array_size_t>(L_451)))))^(int32_t)((L_452)->GetAt(static_cast<il2cpp_array_size_t>(L_454)))))^(int32_t)((L_455)->GetAt(static_cast<il2cpp_array_size_t>(L_457)))))^(int32_t)((L_458)->GetAt(static_cast<il2cpp_array_size_t>(L_459)))));
		UInt32U5BU5D_t2133601851* L_460 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_461 = V_6;
		NullCheck(L_460);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_460, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_461>>((int32_t)24))))));
		uintptr_t L_462 = (((uintptr_t)((int32_t)((uint32_t)L_461>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_463 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_464 = V_7;
		NullCheck(L_463);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_463, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_464>>((int32_t)16)))))));
		int32_t L_465 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_464>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_466 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_467 = V_4;
		NullCheck(L_466);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_466, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_467>>8))))));
		int32_t L_468 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_467>>8)))));
		UInt32U5BU5D_t2133601851* L_469 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_470 = V_5;
		NullCheck(L_469);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_469, (((int32_t)((uint8_t)L_470))));
		int32_t L_471 = (((int32_t)((uint8_t)L_470)));
		UInt32U5BU5D_t2133601851* L_472 = ___ekey;
		NullCheck(L_472);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_472, ((int32_t)34));
		int32_t L_473 = ((int32_t)34);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_460)->GetAt(static_cast<il2cpp_array_size_t>(L_462)))^(int32_t)((L_463)->GetAt(static_cast<il2cpp_array_size_t>(L_465)))))^(int32_t)((L_466)->GetAt(static_cast<il2cpp_array_size_t>(L_468)))))^(int32_t)((L_469)->GetAt(static_cast<il2cpp_array_size_t>(L_471)))))^(int32_t)((L_472)->GetAt(static_cast<il2cpp_array_size_t>(L_473)))));
		UInt32U5BU5D_t2133601851* L_474 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_475 = V_7;
		NullCheck(L_474);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_474, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_475>>((int32_t)24))))));
		uintptr_t L_476 = (((uintptr_t)((int32_t)((uint32_t)L_475>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_477 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_478 = V_4;
		NullCheck(L_477);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_477, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_478>>((int32_t)16)))))));
		int32_t L_479 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_478>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_480 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_481 = V_5;
		NullCheck(L_480);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_480, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_481>>8))))));
		int32_t L_482 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_481>>8)))));
		UInt32U5BU5D_t2133601851* L_483 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_484 = V_6;
		NullCheck(L_483);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_483, (((int32_t)((uint8_t)L_484))));
		int32_t L_485 = (((int32_t)((uint8_t)L_484)));
		UInt32U5BU5D_t2133601851* L_486 = ___ekey;
		NullCheck(L_486);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_486, ((int32_t)35));
		int32_t L_487 = ((int32_t)35);
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_474)->GetAt(static_cast<il2cpp_array_size_t>(L_476)))^(int32_t)((L_477)->GetAt(static_cast<il2cpp_array_size_t>(L_479)))))^(int32_t)((L_480)->GetAt(static_cast<il2cpp_array_size_t>(L_482)))))^(int32_t)((L_483)->GetAt(static_cast<il2cpp_array_size_t>(L_485)))))^(int32_t)((L_486)->GetAt(static_cast<il2cpp_array_size_t>(L_487)))));
		UInt32U5BU5D_t2133601851* L_488 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_489 = V_0;
		NullCheck(L_488);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_488, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_489>>((int32_t)24))))));
		uintptr_t L_490 = (((uintptr_t)((int32_t)((uint32_t)L_489>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_491 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_492 = V_1;
		NullCheck(L_491);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_491, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_492>>((int32_t)16)))))));
		int32_t L_493 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_492>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_494 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_495 = V_2;
		NullCheck(L_494);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_494, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_495>>8))))));
		int32_t L_496 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_495>>8)))));
		UInt32U5BU5D_t2133601851* L_497 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_498 = V_3;
		NullCheck(L_497);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_497, (((int32_t)((uint8_t)L_498))));
		int32_t L_499 = (((int32_t)((uint8_t)L_498)));
		UInt32U5BU5D_t2133601851* L_500 = ___ekey;
		NullCheck(L_500);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_500, ((int32_t)36));
		int32_t L_501 = ((int32_t)36);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_488)->GetAt(static_cast<il2cpp_array_size_t>(L_490)))^(int32_t)((L_491)->GetAt(static_cast<il2cpp_array_size_t>(L_493)))))^(int32_t)((L_494)->GetAt(static_cast<il2cpp_array_size_t>(L_496)))))^(int32_t)((L_497)->GetAt(static_cast<il2cpp_array_size_t>(L_499)))))^(int32_t)((L_500)->GetAt(static_cast<il2cpp_array_size_t>(L_501)))));
		UInt32U5BU5D_t2133601851* L_502 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_503 = V_1;
		NullCheck(L_502);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_502, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_503>>((int32_t)24))))));
		uintptr_t L_504 = (((uintptr_t)((int32_t)((uint32_t)L_503>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_505 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_506 = V_2;
		NullCheck(L_505);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_505, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_506>>((int32_t)16)))))));
		int32_t L_507 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_506>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_508 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_509 = V_3;
		NullCheck(L_508);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_508, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_509>>8))))));
		int32_t L_510 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_509>>8)))));
		UInt32U5BU5D_t2133601851* L_511 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_512 = V_0;
		NullCheck(L_511);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_511, (((int32_t)((uint8_t)L_512))));
		int32_t L_513 = (((int32_t)((uint8_t)L_512)));
		UInt32U5BU5D_t2133601851* L_514 = ___ekey;
		NullCheck(L_514);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_514, ((int32_t)37));
		int32_t L_515 = ((int32_t)37);
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_502)->GetAt(static_cast<il2cpp_array_size_t>(L_504)))^(int32_t)((L_505)->GetAt(static_cast<il2cpp_array_size_t>(L_507)))))^(int32_t)((L_508)->GetAt(static_cast<il2cpp_array_size_t>(L_510)))))^(int32_t)((L_511)->GetAt(static_cast<il2cpp_array_size_t>(L_513)))))^(int32_t)((L_514)->GetAt(static_cast<il2cpp_array_size_t>(L_515)))));
		UInt32U5BU5D_t2133601851* L_516 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_517 = V_2;
		NullCheck(L_516);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_516, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_517>>((int32_t)24))))));
		uintptr_t L_518 = (((uintptr_t)((int32_t)((uint32_t)L_517>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_519 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_520 = V_3;
		NullCheck(L_519);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_519, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_520>>((int32_t)16)))))));
		int32_t L_521 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_520>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_522 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_523 = V_0;
		NullCheck(L_522);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_522, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_523>>8))))));
		int32_t L_524 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_523>>8)))));
		UInt32U5BU5D_t2133601851* L_525 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_526 = V_1;
		NullCheck(L_525);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_525, (((int32_t)((uint8_t)L_526))));
		int32_t L_527 = (((int32_t)((uint8_t)L_526)));
		UInt32U5BU5D_t2133601851* L_528 = ___ekey;
		NullCheck(L_528);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_528, ((int32_t)38));
		int32_t L_529 = ((int32_t)38);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_516)->GetAt(static_cast<il2cpp_array_size_t>(L_518)))^(int32_t)((L_519)->GetAt(static_cast<il2cpp_array_size_t>(L_521)))))^(int32_t)((L_522)->GetAt(static_cast<il2cpp_array_size_t>(L_524)))))^(int32_t)((L_525)->GetAt(static_cast<il2cpp_array_size_t>(L_527)))))^(int32_t)((L_528)->GetAt(static_cast<il2cpp_array_size_t>(L_529)))));
		UInt32U5BU5D_t2133601851* L_530 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_531 = V_3;
		NullCheck(L_530);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_530, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_531>>((int32_t)24))))));
		uintptr_t L_532 = (((uintptr_t)((int32_t)((uint32_t)L_531>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_533 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_534 = V_0;
		NullCheck(L_533);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_533, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_534>>((int32_t)16)))))));
		int32_t L_535 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_534>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_536 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_537 = V_1;
		NullCheck(L_536);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_536, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_537>>8))))));
		int32_t L_538 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_537>>8)))));
		UInt32U5BU5D_t2133601851* L_539 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_540 = V_2;
		NullCheck(L_539);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_539, (((int32_t)((uint8_t)L_540))));
		int32_t L_541 = (((int32_t)((uint8_t)L_540)));
		UInt32U5BU5D_t2133601851* L_542 = ___ekey;
		NullCheck(L_542);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_542, ((int32_t)39));
		int32_t L_543 = ((int32_t)39);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_530)->GetAt(static_cast<il2cpp_array_size_t>(L_532)))^(int32_t)((L_533)->GetAt(static_cast<il2cpp_array_size_t>(L_535)))))^(int32_t)((L_536)->GetAt(static_cast<il2cpp_array_size_t>(L_538)))))^(int32_t)((L_539)->GetAt(static_cast<il2cpp_array_size_t>(L_541)))))^(int32_t)((L_542)->GetAt(static_cast<il2cpp_array_size_t>(L_543)))));
		int32_t L_544 = __this->get_Nr_14();
		if ((((int32_t)L_544) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0b08;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2133601851* L_545 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_546 = V_4;
		NullCheck(L_545);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_545, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_546>>((int32_t)24))))));
		uintptr_t L_547 = (((uintptr_t)((int32_t)((uint32_t)L_546>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_548 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_549 = V_5;
		NullCheck(L_548);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_548, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_549>>((int32_t)16)))))));
		int32_t L_550 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_549>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_551 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_552 = V_6;
		NullCheck(L_551);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_551, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_552>>8))))));
		int32_t L_553 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_552>>8)))));
		UInt32U5BU5D_t2133601851* L_554 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_555 = V_7;
		NullCheck(L_554);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_554, (((int32_t)((uint8_t)L_555))));
		int32_t L_556 = (((int32_t)((uint8_t)L_555)));
		UInt32U5BU5D_t2133601851* L_557 = ___ekey;
		NullCheck(L_557);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_557, ((int32_t)40));
		int32_t L_558 = ((int32_t)40);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_545)->GetAt(static_cast<il2cpp_array_size_t>(L_547)))^(int32_t)((L_548)->GetAt(static_cast<il2cpp_array_size_t>(L_550)))))^(int32_t)((L_551)->GetAt(static_cast<il2cpp_array_size_t>(L_553)))))^(int32_t)((L_554)->GetAt(static_cast<il2cpp_array_size_t>(L_556)))))^(int32_t)((L_557)->GetAt(static_cast<il2cpp_array_size_t>(L_558)))));
		UInt32U5BU5D_t2133601851* L_559 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_560 = V_5;
		NullCheck(L_559);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_559, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_560>>((int32_t)24))))));
		uintptr_t L_561 = (((uintptr_t)((int32_t)((uint32_t)L_560>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_562 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_563 = V_6;
		NullCheck(L_562);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_562, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_563>>((int32_t)16)))))));
		int32_t L_564 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_563>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_565 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_566 = V_7;
		NullCheck(L_565);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_565, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_566>>8))))));
		int32_t L_567 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_566>>8)))));
		UInt32U5BU5D_t2133601851* L_568 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_569 = V_4;
		NullCheck(L_568);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_568, (((int32_t)((uint8_t)L_569))));
		int32_t L_570 = (((int32_t)((uint8_t)L_569)));
		UInt32U5BU5D_t2133601851* L_571 = ___ekey;
		NullCheck(L_571);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_571, ((int32_t)41));
		int32_t L_572 = ((int32_t)41);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_559)->GetAt(static_cast<il2cpp_array_size_t>(L_561)))^(int32_t)((L_562)->GetAt(static_cast<il2cpp_array_size_t>(L_564)))))^(int32_t)((L_565)->GetAt(static_cast<il2cpp_array_size_t>(L_567)))))^(int32_t)((L_568)->GetAt(static_cast<il2cpp_array_size_t>(L_570)))))^(int32_t)((L_571)->GetAt(static_cast<il2cpp_array_size_t>(L_572)))));
		UInt32U5BU5D_t2133601851* L_573 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_574 = V_6;
		NullCheck(L_573);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_573, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_574>>((int32_t)24))))));
		uintptr_t L_575 = (((uintptr_t)((int32_t)((uint32_t)L_574>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_576 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_577 = V_7;
		NullCheck(L_576);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_576, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_577>>((int32_t)16)))))));
		int32_t L_578 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_577>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_579 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_580 = V_4;
		NullCheck(L_579);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_579, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_580>>8))))));
		int32_t L_581 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_580>>8)))));
		UInt32U5BU5D_t2133601851* L_582 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_583 = V_5;
		NullCheck(L_582);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_582, (((int32_t)((uint8_t)L_583))));
		int32_t L_584 = (((int32_t)((uint8_t)L_583)));
		UInt32U5BU5D_t2133601851* L_585 = ___ekey;
		NullCheck(L_585);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_585, ((int32_t)42));
		int32_t L_586 = ((int32_t)42);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_573)->GetAt(static_cast<il2cpp_array_size_t>(L_575)))^(int32_t)((L_576)->GetAt(static_cast<il2cpp_array_size_t>(L_578)))))^(int32_t)((L_579)->GetAt(static_cast<il2cpp_array_size_t>(L_581)))))^(int32_t)((L_582)->GetAt(static_cast<il2cpp_array_size_t>(L_584)))))^(int32_t)((L_585)->GetAt(static_cast<il2cpp_array_size_t>(L_586)))));
		UInt32U5BU5D_t2133601851* L_587 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_588 = V_7;
		NullCheck(L_587);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_587, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_588>>((int32_t)24))))));
		uintptr_t L_589 = (((uintptr_t)((int32_t)((uint32_t)L_588>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_590 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_591 = V_4;
		NullCheck(L_590);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_590, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_591>>((int32_t)16)))))));
		int32_t L_592 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_591>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_593 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_594 = V_5;
		NullCheck(L_593);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_593, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_594>>8))))));
		int32_t L_595 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_594>>8)))));
		UInt32U5BU5D_t2133601851* L_596 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_597 = V_6;
		NullCheck(L_596);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_596, (((int32_t)((uint8_t)L_597))));
		int32_t L_598 = (((int32_t)((uint8_t)L_597)));
		UInt32U5BU5D_t2133601851* L_599 = ___ekey;
		NullCheck(L_599);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_599, ((int32_t)43));
		int32_t L_600 = ((int32_t)43);
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_587)->GetAt(static_cast<il2cpp_array_size_t>(L_589)))^(int32_t)((L_590)->GetAt(static_cast<il2cpp_array_size_t>(L_592)))))^(int32_t)((L_593)->GetAt(static_cast<il2cpp_array_size_t>(L_595)))))^(int32_t)((L_596)->GetAt(static_cast<il2cpp_array_size_t>(L_598)))))^(int32_t)((L_599)->GetAt(static_cast<il2cpp_array_size_t>(L_600)))));
		UInt32U5BU5D_t2133601851* L_601 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_602 = V_0;
		NullCheck(L_601);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_601, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_602>>((int32_t)24))))));
		uintptr_t L_603 = (((uintptr_t)((int32_t)((uint32_t)L_602>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_604 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_605 = V_1;
		NullCheck(L_604);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_604, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_605>>((int32_t)16)))))));
		int32_t L_606 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_605>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_607 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_608 = V_2;
		NullCheck(L_607);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_607, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_608>>8))))));
		int32_t L_609 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_608>>8)))));
		UInt32U5BU5D_t2133601851* L_610 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_611 = V_3;
		NullCheck(L_610);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_610, (((int32_t)((uint8_t)L_611))));
		int32_t L_612 = (((int32_t)((uint8_t)L_611)));
		UInt32U5BU5D_t2133601851* L_613 = ___ekey;
		NullCheck(L_613);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_613, ((int32_t)44));
		int32_t L_614 = ((int32_t)44);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_601)->GetAt(static_cast<il2cpp_array_size_t>(L_603)))^(int32_t)((L_604)->GetAt(static_cast<il2cpp_array_size_t>(L_606)))))^(int32_t)((L_607)->GetAt(static_cast<il2cpp_array_size_t>(L_609)))))^(int32_t)((L_610)->GetAt(static_cast<il2cpp_array_size_t>(L_612)))))^(int32_t)((L_613)->GetAt(static_cast<il2cpp_array_size_t>(L_614)))));
		UInt32U5BU5D_t2133601851* L_615 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_616 = V_1;
		NullCheck(L_615);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_615, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_616>>((int32_t)24))))));
		uintptr_t L_617 = (((uintptr_t)((int32_t)((uint32_t)L_616>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_618 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_619 = V_2;
		NullCheck(L_618);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_618, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_619>>((int32_t)16)))))));
		int32_t L_620 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_619>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_621 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_622 = V_3;
		NullCheck(L_621);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_621, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_622>>8))))));
		int32_t L_623 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_622>>8)))));
		UInt32U5BU5D_t2133601851* L_624 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_625 = V_0;
		NullCheck(L_624);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_624, (((int32_t)((uint8_t)L_625))));
		int32_t L_626 = (((int32_t)((uint8_t)L_625)));
		UInt32U5BU5D_t2133601851* L_627 = ___ekey;
		NullCheck(L_627);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_627, ((int32_t)45));
		int32_t L_628 = ((int32_t)45);
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_615)->GetAt(static_cast<il2cpp_array_size_t>(L_617)))^(int32_t)((L_618)->GetAt(static_cast<il2cpp_array_size_t>(L_620)))))^(int32_t)((L_621)->GetAt(static_cast<il2cpp_array_size_t>(L_623)))))^(int32_t)((L_624)->GetAt(static_cast<il2cpp_array_size_t>(L_626)))))^(int32_t)((L_627)->GetAt(static_cast<il2cpp_array_size_t>(L_628)))));
		UInt32U5BU5D_t2133601851* L_629 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_630 = V_2;
		NullCheck(L_629);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_629, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_630>>((int32_t)24))))));
		uintptr_t L_631 = (((uintptr_t)((int32_t)((uint32_t)L_630>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_632 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_633 = V_3;
		NullCheck(L_632);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_632, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_633>>((int32_t)16)))))));
		int32_t L_634 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_633>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_635 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_636 = V_0;
		NullCheck(L_635);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_635, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_636>>8))))));
		int32_t L_637 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_636>>8)))));
		UInt32U5BU5D_t2133601851* L_638 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_639 = V_1;
		NullCheck(L_638);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_638, (((int32_t)((uint8_t)L_639))));
		int32_t L_640 = (((int32_t)((uint8_t)L_639)));
		UInt32U5BU5D_t2133601851* L_641 = ___ekey;
		NullCheck(L_641);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_641, ((int32_t)46));
		int32_t L_642 = ((int32_t)46);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_629)->GetAt(static_cast<il2cpp_array_size_t>(L_631)))^(int32_t)((L_632)->GetAt(static_cast<il2cpp_array_size_t>(L_634)))))^(int32_t)((L_635)->GetAt(static_cast<il2cpp_array_size_t>(L_637)))))^(int32_t)((L_638)->GetAt(static_cast<il2cpp_array_size_t>(L_640)))))^(int32_t)((L_641)->GetAt(static_cast<il2cpp_array_size_t>(L_642)))));
		UInt32U5BU5D_t2133601851* L_643 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_644 = V_3;
		NullCheck(L_643);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_643, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_644>>((int32_t)24))))));
		uintptr_t L_645 = (((uintptr_t)((int32_t)((uint32_t)L_644>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_646 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_647 = V_0;
		NullCheck(L_646);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_646, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_647>>((int32_t)16)))))));
		int32_t L_648 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_647>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_649 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_650 = V_1;
		NullCheck(L_649);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_649, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_650>>8))))));
		int32_t L_651 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_650>>8)))));
		UInt32U5BU5D_t2133601851* L_652 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_653 = V_2;
		NullCheck(L_652);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_652, (((int32_t)((uint8_t)L_653))));
		int32_t L_654 = (((int32_t)((uint8_t)L_653)));
		UInt32U5BU5D_t2133601851* L_655 = ___ekey;
		NullCheck(L_655);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_655, ((int32_t)47));
		int32_t L_656 = ((int32_t)47);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_643)->GetAt(static_cast<il2cpp_array_size_t>(L_645)))^(int32_t)((L_646)->GetAt(static_cast<il2cpp_array_size_t>(L_648)))))^(int32_t)((L_649)->GetAt(static_cast<il2cpp_array_size_t>(L_651)))))^(int32_t)((L_652)->GetAt(static_cast<il2cpp_array_size_t>(L_654)))))^(int32_t)((L_655)->GetAt(static_cast<il2cpp_array_size_t>(L_656)))));
		V_8 = ((int32_t)48);
		int32_t L_657 = __this->get_Nr_14();
		if ((((int32_t)L_657) <= ((int32_t)((int32_t)12))))
		{
			goto IL_0b08;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2133601851* L_658 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_659 = V_4;
		NullCheck(L_658);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_658, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_659>>((int32_t)24))))));
		uintptr_t L_660 = (((uintptr_t)((int32_t)((uint32_t)L_659>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_661 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_662 = V_5;
		NullCheck(L_661);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_661, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_662>>((int32_t)16)))))));
		int32_t L_663 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_662>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_664 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_665 = V_6;
		NullCheck(L_664);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_664, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_665>>8))))));
		int32_t L_666 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_665>>8)))));
		UInt32U5BU5D_t2133601851* L_667 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_668 = V_7;
		NullCheck(L_667);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_667, (((int32_t)((uint8_t)L_668))));
		int32_t L_669 = (((int32_t)((uint8_t)L_668)));
		UInt32U5BU5D_t2133601851* L_670 = ___ekey;
		NullCheck(L_670);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_670, ((int32_t)48));
		int32_t L_671 = ((int32_t)48);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_658)->GetAt(static_cast<il2cpp_array_size_t>(L_660)))^(int32_t)((L_661)->GetAt(static_cast<il2cpp_array_size_t>(L_663)))))^(int32_t)((L_664)->GetAt(static_cast<il2cpp_array_size_t>(L_666)))))^(int32_t)((L_667)->GetAt(static_cast<il2cpp_array_size_t>(L_669)))))^(int32_t)((L_670)->GetAt(static_cast<il2cpp_array_size_t>(L_671)))));
		UInt32U5BU5D_t2133601851* L_672 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_673 = V_5;
		NullCheck(L_672);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_672, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_673>>((int32_t)24))))));
		uintptr_t L_674 = (((uintptr_t)((int32_t)((uint32_t)L_673>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_675 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_676 = V_6;
		NullCheck(L_675);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_675, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_676>>((int32_t)16)))))));
		int32_t L_677 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_676>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_678 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_679 = V_7;
		NullCheck(L_678);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_678, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_679>>8))))));
		int32_t L_680 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_679>>8)))));
		UInt32U5BU5D_t2133601851* L_681 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_682 = V_4;
		NullCheck(L_681);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_681, (((int32_t)((uint8_t)L_682))));
		int32_t L_683 = (((int32_t)((uint8_t)L_682)));
		UInt32U5BU5D_t2133601851* L_684 = ___ekey;
		NullCheck(L_684);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_684, ((int32_t)49));
		int32_t L_685 = ((int32_t)49);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_672)->GetAt(static_cast<il2cpp_array_size_t>(L_674)))^(int32_t)((L_675)->GetAt(static_cast<il2cpp_array_size_t>(L_677)))))^(int32_t)((L_678)->GetAt(static_cast<il2cpp_array_size_t>(L_680)))))^(int32_t)((L_681)->GetAt(static_cast<il2cpp_array_size_t>(L_683)))))^(int32_t)((L_684)->GetAt(static_cast<il2cpp_array_size_t>(L_685)))));
		UInt32U5BU5D_t2133601851* L_686 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_687 = V_6;
		NullCheck(L_686);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_686, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_687>>((int32_t)24))))));
		uintptr_t L_688 = (((uintptr_t)((int32_t)((uint32_t)L_687>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_689 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_690 = V_7;
		NullCheck(L_689);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_689, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_690>>((int32_t)16)))))));
		int32_t L_691 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_690>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_692 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_693 = V_4;
		NullCheck(L_692);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_692, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_693>>8))))));
		int32_t L_694 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_693>>8)))));
		UInt32U5BU5D_t2133601851* L_695 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_696 = V_5;
		NullCheck(L_695);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_695, (((int32_t)((uint8_t)L_696))));
		int32_t L_697 = (((int32_t)((uint8_t)L_696)));
		UInt32U5BU5D_t2133601851* L_698 = ___ekey;
		NullCheck(L_698);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_698, ((int32_t)50));
		int32_t L_699 = ((int32_t)50);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_686)->GetAt(static_cast<il2cpp_array_size_t>(L_688)))^(int32_t)((L_689)->GetAt(static_cast<il2cpp_array_size_t>(L_691)))))^(int32_t)((L_692)->GetAt(static_cast<il2cpp_array_size_t>(L_694)))))^(int32_t)((L_695)->GetAt(static_cast<il2cpp_array_size_t>(L_697)))))^(int32_t)((L_698)->GetAt(static_cast<il2cpp_array_size_t>(L_699)))));
		UInt32U5BU5D_t2133601851* L_700 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_701 = V_7;
		NullCheck(L_700);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_700, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_701>>((int32_t)24))))));
		uintptr_t L_702 = (((uintptr_t)((int32_t)((uint32_t)L_701>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_703 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_704 = V_4;
		NullCheck(L_703);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_703, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_704>>((int32_t)16)))))));
		int32_t L_705 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_704>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_706 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_707 = V_5;
		NullCheck(L_706);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_706, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_707>>8))))));
		int32_t L_708 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_707>>8)))));
		UInt32U5BU5D_t2133601851* L_709 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_710 = V_6;
		NullCheck(L_709);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_709, (((int32_t)((uint8_t)L_710))));
		int32_t L_711 = (((int32_t)((uint8_t)L_710)));
		UInt32U5BU5D_t2133601851* L_712 = ___ekey;
		NullCheck(L_712);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_712, ((int32_t)51));
		int32_t L_713 = ((int32_t)51);
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_700)->GetAt(static_cast<il2cpp_array_size_t>(L_702)))^(int32_t)((L_703)->GetAt(static_cast<il2cpp_array_size_t>(L_705)))))^(int32_t)((L_706)->GetAt(static_cast<il2cpp_array_size_t>(L_708)))))^(int32_t)((L_709)->GetAt(static_cast<il2cpp_array_size_t>(L_711)))))^(int32_t)((L_712)->GetAt(static_cast<il2cpp_array_size_t>(L_713)))));
		UInt32U5BU5D_t2133601851* L_714 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_715 = V_0;
		NullCheck(L_714);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_714, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_715>>((int32_t)24))))));
		uintptr_t L_716 = (((uintptr_t)((int32_t)((uint32_t)L_715>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_717 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_718 = V_1;
		NullCheck(L_717);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_717, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_718>>((int32_t)16)))))));
		int32_t L_719 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_718>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_720 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_721 = V_2;
		NullCheck(L_720);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_720, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_721>>8))))));
		int32_t L_722 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_721>>8)))));
		UInt32U5BU5D_t2133601851* L_723 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_724 = V_3;
		NullCheck(L_723);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_723, (((int32_t)((uint8_t)L_724))));
		int32_t L_725 = (((int32_t)((uint8_t)L_724)));
		UInt32U5BU5D_t2133601851* L_726 = ___ekey;
		NullCheck(L_726);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_726, ((int32_t)52));
		int32_t L_727 = ((int32_t)52);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_714)->GetAt(static_cast<il2cpp_array_size_t>(L_716)))^(int32_t)((L_717)->GetAt(static_cast<il2cpp_array_size_t>(L_719)))))^(int32_t)((L_720)->GetAt(static_cast<il2cpp_array_size_t>(L_722)))))^(int32_t)((L_723)->GetAt(static_cast<il2cpp_array_size_t>(L_725)))))^(int32_t)((L_726)->GetAt(static_cast<il2cpp_array_size_t>(L_727)))));
		UInt32U5BU5D_t2133601851* L_728 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_729 = V_1;
		NullCheck(L_728);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_728, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_729>>((int32_t)24))))));
		uintptr_t L_730 = (((uintptr_t)((int32_t)((uint32_t)L_729>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_731 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_732 = V_2;
		NullCheck(L_731);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_731, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_732>>((int32_t)16)))))));
		int32_t L_733 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_732>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_734 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_735 = V_3;
		NullCheck(L_734);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_734, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_735>>8))))));
		int32_t L_736 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_735>>8)))));
		UInt32U5BU5D_t2133601851* L_737 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_738 = V_0;
		NullCheck(L_737);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_737, (((int32_t)((uint8_t)L_738))));
		int32_t L_739 = (((int32_t)((uint8_t)L_738)));
		UInt32U5BU5D_t2133601851* L_740 = ___ekey;
		NullCheck(L_740);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_740, ((int32_t)53));
		int32_t L_741 = ((int32_t)53);
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_728)->GetAt(static_cast<il2cpp_array_size_t>(L_730)))^(int32_t)((L_731)->GetAt(static_cast<il2cpp_array_size_t>(L_733)))))^(int32_t)((L_734)->GetAt(static_cast<il2cpp_array_size_t>(L_736)))))^(int32_t)((L_737)->GetAt(static_cast<il2cpp_array_size_t>(L_739)))))^(int32_t)((L_740)->GetAt(static_cast<il2cpp_array_size_t>(L_741)))));
		UInt32U5BU5D_t2133601851* L_742 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_743 = V_2;
		NullCheck(L_742);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_742, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_743>>((int32_t)24))))));
		uintptr_t L_744 = (((uintptr_t)((int32_t)((uint32_t)L_743>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_745 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_746 = V_3;
		NullCheck(L_745);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_745, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_746>>((int32_t)16)))))));
		int32_t L_747 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_746>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_748 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_749 = V_0;
		NullCheck(L_748);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_748, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_749>>8))))));
		int32_t L_750 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_749>>8)))));
		UInt32U5BU5D_t2133601851* L_751 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_752 = V_1;
		NullCheck(L_751);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_751, (((int32_t)((uint8_t)L_752))));
		int32_t L_753 = (((int32_t)((uint8_t)L_752)));
		UInt32U5BU5D_t2133601851* L_754 = ___ekey;
		NullCheck(L_754);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_754, ((int32_t)54));
		int32_t L_755 = ((int32_t)54);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_742)->GetAt(static_cast<il2cpp_array_size_t>(L_744)))^(int32_t)((L_745)->GetAt(static_cast<il2cpp_array_size_t>(L_747)))))^(int32_t)((L_748)->GetAt(static_cast<il2cpp_array_size_t>(L_750)))))^(int32_t)((L_751)->GetAt(static_cast<il2cpp_array_size_t>(L_753)))))^(int32_t)((L_754)->GetAt(static_cast<il2cpp_array_size_t>(L_755)))));
		UInt32U5BU5D_t2133601851* L_756 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T0_18();
		uint32_t L_757 = V_3;
		NullCheck(L_756);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_756, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_757>>((int32_t)24))))));
		uintptr_t L_758 = (((uintptr_t)((int32_t)((uint32_t)L_757>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_759 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T1_19();
		uint32_t L_760 = V_0;
		NullCheck(L_759);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_759, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_760>>((int32_t)16)))))));
		int32_t L_761 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_760>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_762 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T2_20();
		uint32_t L_763 = V_1;
		NullCheck(L_762);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_762, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_763>>8))))));
		int32_t L_764 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_763>>8)))));
		UInt32U5BU5D_t2133601851* L_765 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_T3_21();
		uint32_t L_766 = V_2;
		NullCheck(L_765);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_765, (((int32_t)((uint8_t)L_766))));
		int32_t L_767 = (((int32_t)((uint8_t)L_766)));
		UInt32U5BU5D_t2133601851* L_768 = ___ekey;
		NullCheck(L_768);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_768, ((int32_t)55));
		int32_t L_769 = ((int32_t)55);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_756)->GetAt(static_cast<il2cpp_array_size_t>(L_758)))^(int32_t)((L_759)->GetAt(static_cast<il2cpp_array_size_t>(L_761)))))^(int32_t)((L_762)->GetAt(static_cast<il2cpp_array_size_t>(L_764)))))^(int32_t)((L_765)->GetAt(static_cast<il2cpp_array_size_t>(L_767)))))^(int32_t)((L_768)->GetAt(static_cast<il2cpp_array_size_t>(L_769)))));
		V_8 = ((int32_t)56);
	}

IL_0b08:
	{
		ByteU5BU5D_t58506160* L_770 = ___outdata;
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		ByteU5BU5D_t58506160* L_771 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_772 = V_4;
		NullCheck(L_771);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_771, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_772>>((int32_t)24))))));
		uintptr_t L_773 = (((uintptr_t)((int32_t)((uint32_t)L_772>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_774 = ___ekey;
		int32_t L_775 = V_8;
		NullCheck(L_774);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_774, L_775);
		int32_t L_776 = L_775;
		NullCheck(L_770);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_770, 0);
		(L_770)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_771)->GetAt(static_cast<il2cpp_array_size_t>(L_773)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_774)->GetAt(static_cast<il2cpp_array_size_t>(L_776)))>>((int32_t)24))))))))))));
		ByteU5BU5D_t58506160* L_777 = ___outdata;
		ByteU5BU5D_t58506160* L_778 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_779 = V_5;
		NullCheck(L_778);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_778, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_779>>((int32_t)16)))))));
		int32_t L_780 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_779>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_781 = ___ekey;
		int32_t L_782 = V_8;
		NullCheck(L_781);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_781, L_782);
		int32_t L_783 = L_782;
		NullCheck(L_777);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_777, 1);
		(L_777)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_778)->GetAt(static_cast<il2cpp_array_size_t>(L_780)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_781)->GetAt(static_cast<il2cpp_array_size_t>(L_783)))>>((int32_t)16))))))))))));
		ByteU5BU5D_t58506160* L_784 = ___outdata;
		ByteU5BU5D_t58506160* L_785 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_786 = V_6;
		NullCheck(L_785);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_785, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_786>>8))))));
		int32_t L_787 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_786>>8)))));
		UInt32U5BU5D_t2133601851* L_788 = ___ekey;
		int32_t L_789 = V_8;
		NullCheck(L_788);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_788, L_789);
		int32_t L_790 = L_789;
		NullCheck(L_784);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_784, 2);
		(L_784)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_785)->GetAt(static_cast<il2cpp_array_size_t>(L_787)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_788)->GetAt(static_cast<il2cpp_array_size_t>(L_790)))>>8)))))))))));
		ByteU5BU5D_t58506160* L_791 = ___outdata;
		ByteU5BU5D_t58506160* L_792 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_793 = V_7;
		NullCheck(L_792);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_792, (((int32_t)((uint8_t)L_793))));
		int32_t L_794 = (((int32_t)((uint8_t)L_793)));
		UInt32U5BU5D_t2133601851* L_795 = ___ekey;
		int32_t L_796 = V_8;
		int32_t L_797 = L_796;
		V_8 = ((int32_t)((int32_t)L_797+(int32_t)1));
		NullCheck(L_795);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_795, L_797);
		int32_t L_798 = L_797;
		NullCheck(L_791);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_791, 3);
		(L_791)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_792)->GetAt(static_cast<il2cpp_array_size_t>(L_794)))^(int32_t)(((int32_t)((uint8_t)((L_795)->GetAt(static_cast<il2cpp_array_size_t>(L_798))))))))))));
		ByteU5BU5D_t58506160* L_799 = ___outdata;
		ByteU5BU5D_t58506160* L_800 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_801 = V_5;
		NullCheck(L_800);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_800, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_801>>((int32_t)24))))));
		uintptr_t L_802 = (((uintptr_t)((int32_t)((uint32_t)L_801>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_803 = ___ekey;
		int32_t L_804 = V_8;
		NullCheck(L_803);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_803, L_804);
		int32_t L_805 = L_804;
		NullCheck(L_799);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_799, 4);
		(L_799)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_800)->GetAt(static_cast<il2cpp_array_size_t>(L_802)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_803)->GetAt(static_cast<il2cpp_array_size_t>(L_805)))>>((int32_t)24))))))))))));
		ByteU5BU5D_t58506160* L_806 = ___outdata;
		ByteU5BU5D_t58506160* L_807 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_808 = V_6;
		NullCheck(L_807);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_807, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_808>>((int32_t)16)))))));
		int32_t L_809 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_808>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_810 = ___ekey;
		int32_t L_811 = V_8;
		NullCheck(L_810);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_810, L_811);
		int32_t L_812 = L_811;
		NullCheck(L_806);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_806, 5);
		(L_806)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_807)->GetAt(static_cast<il2cpp_array_size_t>(L_809)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_810)->GetAt(static_cast<il2cpp_array_size_t>(L_812)))>>((int32_t)16))))))))))));
		ByteU5BU5D_t58506160* L_813 = ___outdata;
		ByteU5BU5D_t58506160* L_814 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_815 = V_7;
		NullCheck(L_814);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_814, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_815>>8))))));
		int32_t L_816 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_815>>8)))));
		UInt32U5BU5D_t2133601851* L_817 = ___ekey;
		int32_t L_818 = V_8;
		NullCheck(L_817);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_817, L_818);
		int32_t L_819 = L_818;
		NullCheck(L_813);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_813, 6);
		(L_813)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_814)->GetAt(static_cast<il2cpp_array_size_t>(L_816)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_817)->GetAt(static_cast<il2cpp_array_size_t>(L_819)))>>8)))))))))));
		ByteU5BU5D_t58506160* L_820 = ___outdata;
		ByteU5BU5D_t58506160* L_821 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_822 = V_4;
		NullCheck(L_821);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_821, (((int32_t)((uint8_t)L_822))));
		int32_t L_823 = (((int32_t)((uint8_t)L_822)));
		UInt32U5BU5D_t2133601851* L_824 = ___ekey;
		int32_t L_825 = V_8;
		int32_t L_826 = L_825;
		V_8 = ((int32_t)((int32_t)L_826+(int32_t)1));
		NullCheck(L_824);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_824, L_826);
		int32_t L_827 = L_826;
		NullCheck(L_820);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_820, 7);
		(L_820)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_821)->GetAt(static_cast<il2cpp_array_size_t>(L_823)))^(int32_t)(((int32_t)((uint8_t)((L_824)->GetAt(static_cast<il2cpp_array_size_t>(L_827))))))))))));
		ByteU5BU5D_t58506160* L_828 = ___outdata;
		ByteU5BU5D_t58506160* L_829 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_830 = V_6;
		NullCheck(L_829);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_829, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_830>>((int32_t)24))))));
		uintptr_t L_831 = (((uintptr_t)((int32_t)((uint32_t)L_830>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_832 = ___ekey;
		int32_t L_833 = V_8;
		NullCheck(L_832);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_832, L_833);
		int32_t L_834 = L_833;
		NullCheck(L_828);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_828, 8);
		(L_828)->SetAt(static_cast<il2cpp_array_size_t>(8), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_829)->GetAt(static_cast<il2cpp_array_size_t>(L_831)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_832)->GetAt(static_cast<il2cpp_array_size_t>(L_834)))>>((int32_t)24))))))))))));
		ByteU5BU5D_t58506160* L_835 = ___outdata;
		ByteU5BU5D_t58506160* L_836 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_837 = V_7;
		NullCheck(L_836);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_836, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_837>>((int32_t)16)))))));
		int32_t L_838 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_837>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_839 = ___ekey;
		int32_t L_840 = V_8;
		NullCheck(L_839);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_839, L_840);
		int32_t L_841 = L_840;
		NullCheck(L_835);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_835, ((int32_t)9));
		(L_835)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_836)->GetAt(static_cast<il2cpp_array_size_t>(L_838)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_839)->GetAt(static_cast<il2cpp_array_size_t>(L_841)))>>((int32_t)16))))))))))));
		ByteU5BU5D_t58506160* L_842 = ___outdata;
		ByteU5BU5D_t58506160* L_843 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_844 = V_4;
		NullCheck(L_843);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_843, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_844>>8))))));
		int32_t L_845 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_844>>8)))));
		UInt32U5BU5D_t2133601851* L_846 = ___ekey;
		int32_t L_847 = V_8;
		NullCheck(L_846);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_846, L_847);
		int32_t L_848 = L_847;
		NullCheck(L_842);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_842, ((int32_t)10));
		(L_842)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_843)->GetAt(static_cast<il2cpp_array_size_t>(L_845)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_846)->GetAt(static_cast<il2cpp_array_size_t>(L_848)))>>8)))))))))));
		ByteU5BU5D_t58506160* L_849 = ___outdata;
		ByteU5BU5D_t58506160* L_850 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_851 = V_5;
		NullCheck(L_850);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_850, (((int32_t)((uint8_t)L_851))));
		int32_t L_852 = (((int32_t)((uint8_t)L_851)));
		UInt32U5BU5D_t2133601851* L_853 = ___ekey;
		int32_t L_854 = V_8;
		int32_t L_855 = L_854;
		V_8 = ((int32_t)((int32_t)L_855+(int32_t)1));
		NullCheck(L_853);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_853, L_855);
		int32_t L_856 = L_855;
		NullCheck(L_849);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_849, ((int32_t)11));
		(L_849)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_850)->GetAt(static_cast<il2cpp_array_size_t>(L_852)))^(int32_t)(((int32_t)((uint8_t)((L_853)->GetAt(static_cast<il2cpp_array_size_t>(L_856))))))))))));
		ByteU5BU5D_t58506160* L_857 = ___outdata;
		ByteU5BU5D_t58506160* L_858 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_859 = V_7;
		NullCheck(L_858);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_858, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_859>>((int32_t)24))))));
		uintptr_t L_860 = (((uintptr_t)((int32_t)((uint32_t)L_859>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_861 = ___ekey;
		int32_t L_862 = V_8;
		NullCheck(L_861);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_861, L_862);
		int32_t L_863 = L_862;
		NullCheck(L_857);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_857, ((int32_t)12));
		(L_857)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_858)->GetAt(static_cast<il2cpp_array_size_t>(L_860)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_861)->GetAt(static_cast<il2cpp_array_size_t>(L_863)))>>((int32_t)24))))))))))));
		ByteU5BU5D_t58506160* L_864 = ___outdata;
		ByteU5BU5D_t58506160* L_865 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_866 = V_4;
		NullCheck(L_865);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_865, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_866>>((int32_t)16)))))));
		int32_t L_867 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_866>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_868 = ___ekey;
		int32_t L_869 = V_8;
		NullCheck(L_868);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_868, L_869);
		int32_t L_870 = L_869;
		NullCheck(L_864);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_864, ((int32_t)13));
		(L_864)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_865)->GetAt(static_cast<il2cpp_array_size_t>(L_867)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_868)->GetAt(static_cast<il2cpp_array_size_t>(L_870)))>>((int32_t)16))))))))))));
		ByteU5BU5D_t58506160* L_871 = ___outdata;
		ByteU5BU5D_t58506160* L_872 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_873 = V_5;
		NullCheck(L_872);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_872, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_873>>8))))));
		int32_t L_874 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_873>>8)))));
		UInt32U5BU5D_t2133601851* L_875 = ___ekey;
		int32_t L_876 = V_8;
		NullCheck(L_875);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_875, L_876);
		int32_t L_877 = L_876;
		NullCheck(L_871);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_871, ((int32_t)14));
		(L_871)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_872)->GetAt(static_cast<il2cpp_array_size_t>(L_874)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_875)->GetAt(static_cast<il2cpp_array_size_t>(L_877)))>>8)))))))))));
		ByteU5BU5D_t58506160* L_878 = ___outdata;
		ByteU5BU5D_t58506160* L_879 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_SBox_16();
		uint32_t L_880 = V_6;
		NullCheck(L_879);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_879, (((int32_t)((uint8_t)L_880))));
		int32_t L_881 = (((int32_t)((uint8_t)L_880)));
		UInt32U5BU5D_t2133601851* L_882 = ___ekey;
		int32_t L_883 = V_8;
		int32_t L_884 = L_883;
		V_8 = ((int32_t)((int32_t)L_884+(int32_t)1));
		NullCheck(L_882);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_882, L_884);
		int32_t L_885 = L_884;
		NullCheck(L_878);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_878, ((int32_t)15));
		(L_878)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_879)->GetAt(static_cast<il2cpp_array_size_t>(L_881)))^(int32_t)(((int32_t)((uint8_t)((L_882)->GetAt(static_cast<il2cpp_array_size_t>(L_885))))))))))));
		return;
	}
}
// System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern TypeInfo* AesTransform_t2186883254_il2cpp_TypeInfo_var;
extern const uint32_t AesTransform_Decrypt128_m3310750971_MetadataUsageId;
extern "C"  void AesTransform_Decrypt128_m3310750971 (AesTransform_t2186883254 * __this, ByteU5BU5D_t58506160* ___indata, ByteU5BU5D_t58506160* ___outdata, UInt32U5BU5D_t2133601851* ___ekey, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AesTransform_Decrypt128_m3310750971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	uint32_t V_4 = 0;
	uint32_t V_5 = 0;
	uint32_t V_6 = 0;
	uint32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		V_8 = ((int32_t)40);
		ByteU5BU5D_t58506160* L_0 = ___indata;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		ByteU5BU5D_t58506160* L_2 = ___indata;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		int32_t L_3 = 1;
		ByteU5BU5D_t58506160* L_4 = ___indata;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		int32_t L_5 = 2;
		ByteU5BU5D_t58506160* L_6 = ___indata;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		int32_t L_7 = 3;
		UInt32U5BU5D_t2133601851* L_8 = ___ekey;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1)))<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5)))<<(int32_t)8))))|(int32_t)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7)))))^(int32_t)((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9)))));
		ByteU5BU5D_t58506160* L_10 = ___indata;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		int32_t L_11 = 4;
		ByteU5BU5D_t58506160* L_12 = ___indata;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		int32_t L_13 = 5;
		ByteU5BU5D_t58506160* L_14 = ___indata;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 6);
		int32_t L_15 = 6;
		ByteU5BU5D_t58506160* L_16 = ___indata;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 7);
		int32_t L_17 = 7;
		UInt32U5BU5D_t2133601851* L_18 = ___ekey;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		int32_t L_19 = 1;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11)))<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)((L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15)))<<(int32_t)8))))|(int32_t)((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17)))))^(int32_t)((L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19)))));
		ByteU5BU5D_t58506160* L_20 = ___indata;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 8);
		int32_t L_21 = 8;
		ByteU5BU5D_t58506160* L_22 = ___indata;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)9));
		int32_t L_23 = ((int32_t)9);
		ByteU5BU5D_t58506160* L_24 = ___indata;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)10));
		int32_t L_25 = ((int32_t)10);
		ByteU5BU5D_t58506160* L_26 = ___indata;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)11));
		int32_t L_27 = ((int32_t)11);
		UInt32U5BU5D_t2133601851* L_28 = ___ekey;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 2);
		int32_t L_29 = 2;
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21)))<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)((L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25)))<<(int32_t)8))))|(int32_t)((L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27)))))^(int32_t)((L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29)))));
		ByteU5BU5D_t58506160* L_30 = ___indata;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)12));
		int32_t L_31 = ((int32_t)12);
		ByteU5BU5D_t58506160* L_32 = ___indata;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, ((int32_t)13));
		int32_t L_33 = ((int32_t)13);
		ByteU5BU5D_t58506160* L_34 = ___indata;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)14));
		int32_t L_35 = ((int32_t)14);
		ByteU5BU5D_t58506160* L_36 = ___indata;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)15));
		int32_t L_37 = ((int32_t)15);
		UInt32U5BU5D_t2133601851* L_38 = ___ekey;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 3);
		int32_t L_39 = 3;
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31)))<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)((L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_33)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)((L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_35)))<<(int32_t)8))))|(int32_t)((L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37)))))^(int32_t)((L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_39)))));
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2133601851* L_40 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_41 = V_0;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_41>>((int32_t)24))))));
		uintptr_t L_42 = (((uintptr_t)((int32_t)((uint32_t)L_41>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_43 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_44 = V_3;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_44>>((int32_t)16)))))));
		int32_t L_45 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_44>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_46 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_47 = V_2;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_47>>8))))));
		int32_t L_48 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_47>>8)))));
		UInt32U5BU5D_t2133601851* L_49 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_50 = V_1;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, (((int32_t)((uint8_t)L_50))));
		int32_t L_51 = (((int32_t)((uint8_t)L_50)));
		UInt32U5BU5D_t2133601851* L_52 = ___ekey;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 4);
		int32_t L_53 = 4;
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42)))^(int32_t)((L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45)))))^(int32_t)((L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48)))))^(int32_t)((L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51)))))^(int32_t)((L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_53)))));
		UInt32U5BU5D_t2133601851* L_54 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_55 = V_1;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_55>>((int32_t)24))))));
		uintptr_t L_56 = (((uintptr_t)((int32_t)((uint32_t)L_55>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_57 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_58 = V_0;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_58>>((int32_t)16)))))));
		int32_t L_59 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_58>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_60 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_61 = V_3;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_61>>8))))));
		int32_t L_62 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_61>>8)))));
		UInt32U5BU5D_t2133601851* L_63 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_64 = V_2;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, (((int32_t)((uint8_t)L_64))));
		int32_t L_65 = (((int32_t)((uint8_t)L_64)));
		UInt32U5BU5D_t2133601851* L_66 = ___ekey;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, 5);
		int32_t L_67 = 5;
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56)))^(int32_t)((L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59)))))^(int32_t)((L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62)))))^(int32_t)((L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_65)))))^(int32_t)((L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_67)))));
		UInt32U5BU5D_t2133601851* L_68 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_69 = V_2;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_69>>((int32_t)24))))));
		uintptr_t L_70 = (((uintptr_t)((int32_t)((uint32_t)L_69>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_71 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_72 = V_1;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_72>>((int32_t)16)))))));
		int32_t L_73 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_72>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_74 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_75 = V_0;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_75>>8))))));
		int32_t L_76 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_75>>8)))));
		UInt32U5BU5D_t2133601851* L_77 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_78 = V_3;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, (((int32_t)((uint8_t)L_78))));
		int32_t L_79 = (((int32_t)((uint8_t)L_78)));
		UInt32U5BU5D_t2133601851* L_80 = ___ekey;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, 6);
		int32_t L_81 = 6;
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70)))^(int32_t)((L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73)))))^(int32_t)((L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76)))))^(int32_t)((L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_79)))))^(int32_t)((L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_81)))));
		UInt32U5BU5D_t2133601851* L_82 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_83 = V_3;
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_83>>((int32_t)24))))));
		uintptr_t L_84 = (((uintptr_t)((int32_t)((uint32_t)L_83>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_85 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_86 = V_2;
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_86>>((int32_t)16)))))));
		int32_t L_87 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_86>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_88 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_89 = V_1;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_89>>8))))));
		int32_t L_90 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_89>>8)))));
		UInt32U5BU5D_t2133601851* L_91 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_92 = V_0;
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, (((int32_t)((uint8_t)L_92))));
		int32_t L_93 = (((int32_t)((uint8_t)L_92)));
		UInt32U5BU5D_t2133601851* L_94 = ___ekey;
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, 7);
		int32_t L_95 = 7;
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84)))^(int32_t)((L_85)->GetAt(static_cast<il2cpp_array_size_t>(L_87)))))^(int32_t)((L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_90)))))^(int32_t)((L_91)->GetAt(static_cast<il2cpp_array_size_t>(L_93)))))^(int32_t)((L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_95)))));
		UInt32U5BU5D_t2133601851* L_96 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_97 = V_4;
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_97>>((int32_t)24))))));
		uintptr_t L_98 = (((uintptr_t)((int32_t)((uint32_t)L_97>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_99 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_100 = V_7;
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_100>>((int32_t)16)))))));
		int32_t L_101 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_100>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_102 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_103 = V_6;
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_103>>8))))));
		int32_t L_104 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_103>>8)))));
		UInt32U5BU5D_t2133601851* L_105 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_106 = V_5;
		NullCheck(L_105);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_105, (((int32_t)((uint8_t)L_106))));
		int32_t L_107 = (((int32_t)((uint8_t)L_106)));
		UInt32U5BU5D_t2133601851* L_108 = ___ekey;
		NullCheck(L_108);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_108, 8);
		int32_t L_109 = 8;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_98)))^(int32_t)((L_99)->GetAt(static_cast<il2cpp_array_size_t>(L_101)))))^(int32_t)((L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104)))))^(int32_t)((L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_107)))))^(int32_t)((L_108)->GetAt(static_cast<il2cpp_array_size_t>(L_109)))));
		UInt32U5BU5D_t2133601851* L_110 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_111 = V_5;
		NullCheck(L_110);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_110, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_111>>((int32_t)24))))));
		uintptr_t L_112 = (((uintptr_t)((int32_t)((uint32_t)L_111>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_113 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_114 = V_4;
		NullCheck(L_113);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_113, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_114>>((int32_t)16)))))));
		int32_t L_115 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_114>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_116 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_117 = V_7;
		NullCheck(L_116);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_116, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_117>>8))))));
		int32_t L_118 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_117>>8)))));
		UInt32U5BU5D_t2133601851* L_119 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_120 = V_6;
		NullCheck(L_119);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_119, (((int32_t)((uint8_t)L_120))));
		int32_t L_121 = (((int32_t)((uint8_t)L_120)));
		UInt32U5BU5D_t2133601851* L_122 = ___ekey;
		NullCheck(L_122);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_122, ((int32_t)9));
		int32_t L_123 = ((int32_t)9);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_112)))^(int32_t)((L_113)->GetAt(static_cast<il2cpp_array_size_t>(L_115)))))^(int32_t)((L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_118)))))^(int32_t)((L_119)->GetAt(static_cast<il2cpp_array_size_t>(L_121)))))^(int32_t)((L_122)->GetAt(static_cast<il2cpp_array_size_t>(L_123)))));
		UInt32U5BU5D_t2133601851* L_124 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_125 = V_6;
		NullCheck(L_124);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_124, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_125>>((int32_t)24))))));
		uintptr_t L_126 = (((uintptr_t)((int32_t)((uint32_t)L_125>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_127 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_128 = V_5;
		NullCheck(L_127);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_127, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_128>>((int32_t)16)))))));
		int32_t L_129 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_128>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_130 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_131 = V_4;
		NullCheck(L_130);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_130, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_131>>8))))));
		int32_t L_132 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_131>>8)))));
		UInt32U5BU5D_t2133601851* L_133 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_134 = V_7;
		NullCheck(L_133);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_133, (((int32_t)((uint8_t)L_134))));
		int32_t L_135 = (((int32_t)((uint8_t)L_134)));
		UInt32U5BU5D_t2133601851* L_136 = ___ekey;
		NullCheck(L_136);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_136, ((int32_t)10));
		int32_t L_137 = ((int32_t)10);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_124)->GetAt(static_cast<il2cpp_array_size_t>(L_126)))^(int32_t)((L_127)->GetAt(static_cast<il2cpp_array_size_t>(L_129)))))^(int32_t)((L_130)->GetAt(static_cast<il2cpp_array_size_t>(L_132)))))^(int32_t)((L_133)->GetAt(static_cast<il2cpp_array_size_t>(L_135)))))^(int32_t)((L_136)->GetAt(static_cast<il2cpp_array_size_t>(L_137)))));
		UInt32U5BU5D_t2133601851* L_138 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_139 = V_7;
		NullCheck(L_138);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_138, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_139>>((int32_t)24))))));
		uintptr_t L_140 = (((uintptr_t)((int32_t)((uint32_t)L_139>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_141 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_142 = V_6;
		NullCheck(L_141);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_141, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_142>>((int32_t)16)))))));
		int32_t L_143 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_142>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_144 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_145 = V_5;
		NullCheck(L_144);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_144, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_145>>8))))));
		int32_t L_146 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_145>>8)))));
		UInt32U5BU5D_t2133601851* L_147 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_148 = V_4;
		NullCheck(L_147);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_147, (((int32_t)((uint8_t)L_148))));
		int32_t L_149 = (((int32_t)((uint8_t)L_148)));
		UInt32U5BU5D_t2133601851* L_150 = ___ekey;
		NullCheck(L_150);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_150, ((int32_t)11));
		int32_t L_151 = ((int32_t)11);
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_138)->GetAt(static_cast<il2cpp_array_size_t>(L_140)))^(int32_t)((L_141)->GetAt(static_cast<il2cpp_array_size_t>(L_143)))))^(int32_t)((L_144)->GetAt(static_cast<il2cpp_array_size_t>(L_146)))))^(int32_t)((L_147)->GetAt(static_cast<il2cpp_array_size_t>(L_149)))))^(int32_t)((L_150)->GetAt(static_cast<il2cpp_array_size_t>(L_151)))));
		UInt32U5BU5D_t2133601851* L_152 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_153 = V_0;
		NullCheck(L_152);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_152, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_153>>((int32_t)24))))));
		uintptr_t L_154 = (((uintptr_t)((int32_t)((uint32_t)L_153>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_155 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_156 = V_3;
		NullCheck(L_155);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_155, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_156>>((int32_t)16)))))));
		int32_t L_157 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_156>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_158 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_159 = V_2;
		NullCheck(L_158);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_158, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_159>>8))))));
		int32_t L_160 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_159>>8)))));
		UInt32U5BU5D_t2133601851* L_161 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_162 = V_1;
		NullCheck(L_161);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_161, (((int32_t)((uint8_t)L_162))));
		int32_t L_163 = (((int32_t)((uint8_t)L_162)));
		UInt32U5BU5D_t2133601851* L_164 = ___ekey;
		NullCheck(L_164);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_164, ((int32_t)12));
		int32_t L_165 = ((int32_t)12);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_154)))^(int32_t)((L_155)->GetAt(static_cast<il2cpp_array_size_t>(L_157)))))^(int32_t)((L_158)->GetAt(static_cast<il2cpp_array_size_t>(L_160)))))^(int32_t)((L_161)->GetAt(static_cast<il2cpp_array_size_t>(L_163)))))^(int32_t)((L_164)->GetAt(static_cast<il2cpp_array_size_t>(L_165)))));
		UInt32U5BU5D_t2133601851* L_166 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_167 = V_1;
		NullCheck(L_166);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_166, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_167>>((int32_t)24))))));
		uintptr_t L_168 = (((uintptr_t)((int32_t)((uint32_t)L_167>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_169 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_170 = V_0;
		NullCheck(L_169);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_169, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_170>>((int32_t)16)))))));
		int32_t L_171 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_170>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_172 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_173 = V_3;
		NullCheck(L_172);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_172, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_173>>8))))));
		int32_t L_174 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_173>>8)))));
		UInt32U5BU5D_t2133601851* L_175 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_176 = V_2;
		NullCheck(L_175);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_175, (((int32_t)((uint8_t)L_176))));
		int32_t L_177 = (((int32_t)((uint8_t)L_176)));
		UInt32U5BU5D_t2133601851* L_178 = ___ekey;
		NullCheck(L_178);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_178, ((int32_t)13));
		int32_t L_179 = ((int32_t)13);
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_166)->GetAt(static_cast<il2cpp_array_size_t>(L_168)))^(int32_t)((L_169)->GetAt(static_cast<il2cpp_array_size_t>(L_171)))))^(int32_t)((L_172)->GetAt(static_cast<il2cpp_array_size_t>(L_174)))))^(int32_t)((L_175)->GetAt(static_cast<il2cpp_array_size_t>(L_177)))))^(int32_t)((L_178)->GetAt(static_cast<il2cpp_array_size_t>(L_179)))));
		UInt32U5BU5D_t2133601851* L_180 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_181 = V_2;
		NullCheck(L_180);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_180, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_181>>((int32_t)24))))));
		uintptr_t L_182 = (((uintptr_t)((int32_t)((uint32_t)L_181>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_183 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_184 = V_1;
		NullCheck(L_183);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_183, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_184>>((int32_t)16)))))));
		int32_t L_185 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_184>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_186 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_187 = V_0;
		NullCheck(L_186);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_186, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_187>>8))))));
		int32_t L_188 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_187>>8)))));
		UInt32U5BU5D_t2133601851* L_189 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_190 = V_3;
		NullCheck(L_189);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_189, (((int32_t)((uint8_t)L_190))));
		int32_t L_191 = (((int32_t)((uint8_t)L_190)));
		UInt32U5BU5D_t2133601851* L_192 = ___ekey;
		NullCheck(L_192);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_192, ((int32_t)14));
		int32_t L_193 = ((int32_t)14);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_180)->GetAt(static_cast<il2cpp_array_size_t>(L_182)))^(int32_t)((L_183)->GetAt(static_cast<il2cpp_array_size_t>(L_185)))))^(int32_t)((L_186)->GetAt(static_cast<il2cpp_array_size_t>(L_188)))))^(int32_t)((L_189)->GetAt(static_cast<il2cpp_array_size_t>(L_191)))))^(int32_t)((L_192)->GetAt(static_cast<il2cpp_array_size_t>(L_193)))));
		UInt32U5BU5D_t2133601851* L_194 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_195 = V_3;
		NullCheck(L_194);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_194, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_195>>((int32_t)24))))));
		uintptr_t L_196 = (((uintptr_t)((int32_t)((uint32_t)L_195>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_197 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_198 = V_2;
		NullCheck(L_197);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_197, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_198>>((int32_t)16)))))));
		int32_t L_199 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_198>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_200 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_201 = V_1;
		NullCheck(L_200);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_200, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_201>>8))))));
		int32_t L_202 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_201>>8)))));
		UInt32U5BU5D_t2133601851* L_203 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_204 = V_0;
		NullCheck(L_203);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_203, (((int32_t)((uint8_t)L_204))));
		int32_t L_205 = (((int32_t)((uint8_t)L_204)));
		UInt32U5BU5D_t2133601851* L_206 = ___ekey;
		NullCheck(L_206);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_206, ((int32_t)15));
		int32_t L_207 = ((int32_t)15);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_194)->GetAt(static_cast<il2cpp_array_size_t>(L_196)))^(int32_t)((L_197)->GetAt(static_cast<il2cpp_array_size_t>(L_199)))))^(int32_t)((L_200)->GetAt(static_cast<il2cpp_array_size_t>(L_202)))))^(int32_t)((L_203)->GetAt(static_cast<il2cpp_array_size_t>(L_205)))))^(int32_t)((L_206)->GetAt(static_cast<il2cpp_array_size_t>(L_207)))));
		UInt32U5BU5D_t2133601851* L_208 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_209 = V_4;
		NullCheck(L_208);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_208, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_209>>((int32_t)24))))));
		uintptr_t L_210 = (((uintptr_t)((int32_t)((uint32_t)L_209>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_211 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_212 = V_7;
		NullCheck(L_211);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_211, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_212>>((int32_t)16)))))));
		int32_t L_213 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_212>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_214 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_215 = V_6;
		NullCheck(L_214);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_214, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_215>>8))))));
		int32_t L_216 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_215>>8)))));
		UInt32U5BU5D_t2133601851* L_217 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_218 = V_5;
		NullCheck(L_217);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_217, (((int32_t)((uint8_t)L_218))));
		int32_t L_219 = (((int32_t)((uint8_t)L_218)));
		UInt32U5BU5D_t2133601851* L_220 = ___ekey;
		NullCheck(L_220);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_220, ((int32_t)16));
		int32_t L_221 = ((int32_t)16);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_208)->GetAt(static_cast<il2cpp_array_size_t>(L_210)))^(int32_t)((L_211)->GetAt(static_cast<il2cpp_array_size_t>(L_213)))))^(int32_t)((L_214)->GetAt(static_cast<il2cpp_array_size_t>(L_216)))))^(int32_t)((L_217)->GetAt(static_cast<il2cpp_array_size_t>(L_219)))))^(int32_t)((L_220)->GetAt(static_cast<il2cpp_array_size_t>(L_221)))));
		UInt32U5BU5D_t2133601851* L_222 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_223 = V_5;
		NullCheck(L_222);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_222, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_223>>((int32_t)24))))));
		uintptr_t L_224 = (((uintptr_t)((int32_t)((uint32_t)L_223>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_225 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_226 = V_4;
		NullCheck(L_225);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_225, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_226>>((int32_t)16)))))));
		int32_t L_227 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_226>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_228 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_229 = V_7;
		NullCheck(L_228);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_228, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_229>>8))))));
		int32_t L_230 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_229>>8)))));
		UInt32U5BU5D_t2133601851* L_231 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_232 = V_6;
		NullCheck(L_231);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_231, (((int32_t)((uint8_t)L_232))));
		int32_t L_233 = (((int32_t)((uint8_t)L_232)));
		UInt32U5BU5D_t2133601851* L_234 = ___ekey;
		NullCheck(L_234);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_234, ((int32_t)17));
		int32_t L_235 = ((int32_t)17);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_222)->GetAt(static_cast<il2cpp_array_size_t>(L_224)))^(int32_t)((L_225)->GetAt(static_cast<il2cpp_array_size_t>(L_227)))))^(int32_t)((L_228)->GetAt(static_cast<il2cpp_array_size_t>(L_230)))))^(int32_t)((L_231)->GetAt(static_cast<il2cpp_array_size_t>(L_233)))))^(int32_t)((L_234)->GetAt(static_cast<il2cpp_array_size_t>(L_235)))));
		UInt32U5BU5D_t2133601851* L_236 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_237 = V_6;
		NullCheck(L_236);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_236, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_237>>((int32_t)24))))));
		uintptr_t L_238 = (((uintptr_t)((int32_t)((uint32_t)L_237>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_239 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_240 = V_5;
		NullCheck(L_239);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_239, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_240>>((int32_t)16)))))));
		int32_t L_241 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_240>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_242 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_243 = V_4;
		NullCheck(L_242);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_242, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_243>>8))))));
		int32_t L_244 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_243>>8)))));
		UInt32U5BU5D_t2133601851* L_245 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_246 = V_7;
		NullCheck(L_245);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_245, (((int32_t)((uint8_t)L_246))));
		int32_t L_247 = (((int32_t)((uint8_t)L_246)));
		UInt32U5BU5D_t2133601851* L_248 = ___ekey;
		NullCheck(L_248);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_248, ((int32_t)18));
		int32_t L_249 = ((int32_t)18);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_236)->GetAt(static_cast<il2cpp_array_size_t>(L_238)))^(int32_t)((L_239)->GetAt(static_cast<il2cpp_array_size_t>(L_241)))))^(int32_t)((L_242)->GetAt(static_cast<il2cpp_array_size_t>(L_244)))))^(int32_t)((L_245)->GetAt(static_cast<il2cpp_array_size_t>(L_247)))))^(int32_t)((L_248)->GetAt(static_cast<il2cpp_array_size_t>(L_249)))));
		UInt32U5BU5D_t2133601851* L_250 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_251 = V_7;
		NullCheck(L_250);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_250, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_251>>((int32_t)24))))));
		uintptr_t L_252 = (((uintptr_t)((int32_t)((uint32_t)L_251>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_253 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_254 = V_6;
		NullCheck(L_253);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_253, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_254>>((int32_t)16)))))));
		int32_t L_255 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_254>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_256 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_257 = V_5;
		NullCheck(L_256);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_256, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_257>>8))))));
		int32_t L_258 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_257>>8)))));
		UInt32U5BU5D_t2133601851* L_259 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_260 = V_4;
		NullCheck(L_259);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_259, (((int32_t)((uint8_t)L_260))));
		int32_t L_261 = (((int32_t)((uint8_t)L_260)));
		UInt32U5BU5D_t2133601851* L_262 = ___ekey;
		NullCheck(L_262);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_262, ((int32_t)19));
		int32_t L_263 = ((int32_t)19);
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_250)->GetAt(static_cast<il2cpp_array_size_t>(L_252)))^(int32_t)((L_253)->GetAt(static_cast<il2cpp_array_size_t>(L_255)))))^(int32_t)((L_256)->GetAt(static_cast<il2cpp_array_size_t>(L_258)))))^(int32_t)((L_259)->GetAt(static_cast<il2cpp_array_size_t>(L_261)))))^(int32_t)((L_262)->GetAt(static_cast<il2cpp_array_size_t>(L_263)))));
		UInt32U5BU5D_t2133601851* L_264 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_265 = V_0;
		NullCheck(L_264);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_264, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_265>>((int32_t)24))))));
		uintptr_t L_266 = (((uintptr_t)((int32_t)((uint32_t)L_265>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_267 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_268 = V_3;
		NullCheck(L_267);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_267, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_268>>((int32_t)16)))))));
		int32_t L_269 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_268>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_270 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_271 = V_2;
		NullCheck(L_270);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_270, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_271>>8))))));
		int32_t L_272 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_271>>8)))));
		UInt32U5BU5D_t2133601851* L_273 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_274 = V_1;
		NullCheck(L_273);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_273, (((int32_t)((uint8_t)L_274))));
		int32_t L_275 = (((int32_t)((uint8_t)L_274)));
		UInt32U5BU5D_t2133601851* L_276 = ___ekey;
		NullCheck(L_276);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_276, ((int32_t)20));
		int32_t L_277 = ((int32_t)20);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_264)->GetAt(static_cast<il2cpp_array_size_t>(L_266)))^(int32_t)((L_267)->GetAt(static_cast<il2cpp_array_size_t>(L_269)))))^(int32_t)((L_270)->GetAt(static_cast<il2cpp_array_size_t>(L_272)))))^(int32_t)((L_273)->GetAt(static_cast<il2cpp_array_size_t>(L_275)))))^(int32_t)((L_276)->GetAt(static_cast<il2cpp_array_size_t>(L_277)))));
		UInt32U5BU5D_t2133601851* L_278 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_279 = V_1;
		NullCheck(L_278);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_278, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_279>>((int32_t)24))))));
		uintptr_t L_280 = (((uintptr_t)((int32_t)((uint32_t)L_279>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_281 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_282 = V_0;
		NullCheck(L_281);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_281, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_282>>((int32_t)16)))))));
		int32_t L_283 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_282>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_284 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_285 = V_3;
		NullCheck(L_284);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_284, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_285>>8))))));
		int32_t L_286 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_285>>8)))));
		UInt32U5BU5D_t2133601851* L_287 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_288 = V_2;
		NullCheck(L_287);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_287, (((int32_t)((uint8_t)L_288))));
		int32_t L_289 = (((int32_t)((uint8_t)L_288)));
		UInt32U5BU5D_t2133601851* L_290 = ___ekey;
		NullCheck(L_290);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_290, ((int32_t)21));
		int32_t L_291 = ((int32_t)21);
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_278)->GetAt(static_cast<il2cpp_array_size_t>(L_280)))^(int32_t)((L_281)->GetAt(static_cast<il2cpp_array_size_t>(L_283)))))^(int32_t)((L_284)->GetAt(static_cast<il2cpp_array_size_t>(L_286)))))^(int32_t)((L_287)->GetAt(static_cast<il2cpp_array_size_t>(L_289)))))^(int32_t)((L_290)->GetAt(static_cast<il2cpp_array_size_t>(L_291)))));
		UInt32U5BU5D_t2133601851* L_292 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_293 = V_2;
		NullCheck(L_292);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_292, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_293>>((int32_t)24))))));
		uintptr_t L_294 = (((uintptr_t)((int32_t)((uint32_t)L_293>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_295 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_296 = V_1;
		NullCheck(L_295);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_295, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_296>>((int32_t)16)))))));
		int32_t L_297 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_296>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_298 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_299 = V_0;
		NullCheck(L_298);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_298, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_299>>8))))));
		int32_t L_300 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_299>>8)))));
		UInt32U5BU5D_t2133601851* L_301 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_302 = V_3;
		NullCheck(L_301);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_301, (((int32_t)((uint8_t)L_302))));
		int32_t L_303 = (((int32_t)((uint8_t)L_302)));
		UInt32U5BU5D_t2133601851* L_304 = ___ekey;
		NullCheck(L_304);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_304, ((int32_t)22));
		int32_t L_305 = ((int32_t)22);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_292)->GetAt(static_cast<il2cpp_array_size_t>(L_294)))^(int32_t)((L_295)->GetAt(static_cast<il2cpp_array_size_t>(L_297)))))^(int32_t)((L_298)->GetAt(static_cast<il2cpp_array_size_t>(L_300)))))^(int32_t)((L_301)->GetAt(static_cast<il2cpp_array_size_t>(L_303)))))^(int32_t)((L_304)->GetAt(static_cast<il2cpp_array_size_t>(L_305)))));
		UInt32U5BU5D_t2133601851* L_306 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_307 = V_3;
		NullCheck(L_306);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_306, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_307>>((int32_t)24))))));
		uintptr_t L_308 = (((uintptr_t)((int32_t)((uint32_t)L_307>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_309 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_310 = V_2;
		NullCheck(L_309);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_309, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_310>>((int32_t)16)))))));
		int32_t L_311 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_310>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_312 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_313 = V_1;
		NullCheck(L_312);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_312, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_313>>8))))));
		int32_t L_314 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_313>>8)))));
		UInt32U5BU5D_t2133601851* L_315 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_316 = V_0;
		NullCheck(L_315);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_315, (((int32_t)((uint8_t)L_316))));
		int32_t L_317 = (((int32_t)((uint8_t)L_316)));
		UInt32U5BU5D_t2133601851* L_318 = ___ekey;
		NullCheck(L_318);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_318, ((int32_t)23));
		int32_t L_319 = ((int32_t)23);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_306)->GetAt(static_cast<il2cpp_array_size_t>(L_308)))^(int32_t)((L_309)->GetAt(static_cast<il2cpp_array_size_t>(L_311)))))^(int32_t)((L_312)->GetAt(static_cast<il2cpp_array_size_t>(L_314)))))^(int32_t)((L_315)->GetAt(static_cast<il2cpp_array_size_t>(L_317)))))^(int32_t)((L_318)->GetAt(static_cast<il2cpp_array_size_t>(L_319)))));
		UInt32U5BU5D_t2133601851* L_320 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_321 = V_4;
		NullCheck(L_320);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_320, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_321>>((int32_t)24))))));
		uintptr_t L_322 = (((uintptr_t)((int32_t)((uint32_t)L_321>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_323 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_324 = V_7;
		NullCheck(L_323);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_323, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_324>>((int32_t)16)))))));
		int32_t L_325 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_324>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_326 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_327 = V_6;
		NullCheck(L_326);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_326, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_327>>8))))));
		int32_t L_328 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_327>>8)))));
		UInt32U5BU5D_t2133601851* L_329 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_330 = V_5;
		NullCheck(L_329);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_329, (((int32_t)((uint8_t)L_330))));
		int32_t L_331 = (((int32_t)((uint8_t)L_330)));
		UInt32U5BU5D_t2133601851* L_332 = ___ekey;
		NullCheck(L_332);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_332, ((int32_t)24));
		int32_t L_333 = ((int32_t)24);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_320)->GetAt(static_cast<il2cpp_array_size_t>(L_322)))^(int32_t)((L_323)->GetAt(static_cast<il2cpp_array_size_t>(L_325)))))^(int32_t)((L_326)->GetAt(static_cast<il2cpp_array_size_t>(L_328)))))^(int32_t)((L_329)->GetAt(static_cast<il2cpp_array_size_t>(L_331)))))^(int32_t)((L_332)->GetAt(static_cast<il2cpp_array_size_t>(L_333)))));
		UInt32U5BU5D_t2133601851* L_334 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_335 = V_5;
		NullCheck(L_334);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_334, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_335>>((int32_t)24))))));
		uintptr_t L_336 = (((uintptr_t)((int32_t)((uint32_t)L_335>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_337 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_338 = V_4;
		NullCheck(L_337);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_337, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_338>>((int32_t)16)))))));
		int32_t L_339 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_338>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_340 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_341 = V_7;
		NullCheck(L_340);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_340, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_341>>8))))));
		int32_t L_342 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_341>>8)))));
		UInt32U5BU5D_t2133601851* L_343 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_344 = V_6;
		NullCheck(L_343);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_343, (((int32_t)((uint8_t)L_344))));
		int32_t L_345 = (((int32_t)((uint8_t)L_344)));
		UInt32U5BU5D_t2133601851* L_346 = ___ekey;
		NullCheck(L_346);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_346, ((int32_t)25));
		int32_t L_347 = ((int32_t)25);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_334)->GetAt(static_cast<il2cpp_array_size_t>(L_336)))^(int32_t)((L_337)->GetAt(static_cast<il2cpp_array_size_t>(L_339)))))^(int32_t)((L_340)->GetAt(static_cast<il2cpp_array_size_t>(L_342)))))^(int32_t)((L_343)->GetAt(static_cast<il2cpp_array_size_t>(L_345)))))^(int32_t)((L_346)->GetAt(static_cast<il2cpp_array_size_t>(L_347)))));
		UInt32U5BU5D_t2133601851* L_348 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_349 = V_6;
		NullCheck(L_348);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_348, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_349>>((int32_t)24))))));
		uintptr_t L_350 = (((uintptr_t)((int32_t)((uint32_t)L_349>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_351 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_352 = V_5;
		NullCheck(L_351);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_351, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_352>>((int32_t)16)))))));
		int32_t L_353 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_352>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_354 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_355 = V_4;
		NullCheck(L_354);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_354, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_355>>8))))));
		int32_t L_356 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_355>>8)))));
		UInt32U5BU5D_t2133601851* L_357 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_358 = V_7;
		NullCheck(L_357);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_357, (((int32_t)((uint8_t)L_358))));
		int32_t L_359 = (((int32_t)((uint8_t)L_358)));
		UInt32U5BU5D_t2133601851* L_360 = ___ekey;
		NullCheck(L_360);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_360, ((int32_t)26));
		int32_t L_361 = ((int32_t)26);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_348)->GetAt(static_cast<il2cpp_array_size_t>(L_350)))^(int32_t)((L_351)->GetAt(static_cast<il2cpp_array_size_t>(L_353)))))^(int32_t)((L_354)->GetAt(static_cast<il2cpp_array_size_t>(L_356)))))^(int32_t)((L_357)->GetAt(static_cast<il2cpp_array_size_t>(L_359)))))^(int32_t)((L_360)->GetAt(static_cast<il2cpp_array_size_t>(L_361)))));
		UInt32U5BU5D_t2133601851* L_362 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_363 = V_7;
		NullCheck(L_362);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_362, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_363>>((int32_t)24))))));
		uintptr_t L_364 = (((uintptr_t)((int32_t)((uint32_t)L_363>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_365 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_366 = V_6;
		NullCheck(L_365);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_365, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_366>>((int32_t)16)))))));
		int32_t L_367 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_366>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_368 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_369 = V_5;
		NullCheck(L_368);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_368, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_369>>8))))));
		int32_t L_370 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_369>>8)))));
		UInt32U5BU5D_t2133601851* L_371 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_372 = V_4;
		NullCheck(L_371);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_371, (((int32_t)((uint8_t)L_372))));
		int32_t L_373 = (((int32_t)((uint8_t)L_372)));
		UInt32U5BU5D_t2133601851* L_374 = ___ekey;
		NullCheck(L_374);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_374, ((int32_t)27));
		int32_t L_375 = ((int32_t)27);
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_362)->GetAt(static_cast<il2cpp_array_size_t>(L_364)))^(int32_t)((L_365)->GetAt(static_cast<il2cpp_array_size_t>(L_367)))))^(int32_t)((L_368)->GetAt(static_cast<il2cpp_array_size_t>(L_370)))))^(int32_t)((L_371)->GetAt(static_cast<il2cpp_array_size_t>(L_373)))))^(int32_t)((L_374)->GetAt(static_cast<il2cpp_array_size_t>(L_375)))));
		UInt32U5BU5D_t2133601851* L_376 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_377 = V_0;
		NullCheck(L_376);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_376, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_377>>((int32_t)24))))));
		uintptr_t L_378 = (((uintptr_t)((int32_t)((uint32_t)L_377>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_379 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_380 = V_3;
		NullCheck(L_379);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_379, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_380>>((int32_t)16)))))));
		int32_t L_381 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_380>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_382 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_383 = V_2;
		NullCheck(L_382);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_382, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_383>>8))))));
		int32_t L_384 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_383>>8)))));
		UInt32U5BU5D_t2133601851* L_385 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_386 = V_1;
		NullCheck(L_385);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_385, (((int32_t)((uint8_t)L_386))));
		int32_t L_387 = (((int32_t)((uint8_t)L_386)));
		UInt32U5BU5D_t2133601851* L_388 = ___ekey;
		NullCheck(L_388);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_388, ((int32_t)28));
		int32_t L_389 = ((int32_t)28);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_376)->GetAt(static_cast<il2cpp_array_size_t>(L_378)))^(int32_t)((L_379)->GetAt(static_cast<il2cpp_array_size_t>(L_381)))))^(int32_t)((L_382)->GetAt(static_cast<il2cpp_array_size_t>(L_384)))))^(int32_t)((L_385)->GetAt(static_cast<il2cpp_array_size_t>(L_387)))))^(int32_t)((L_388)->GetAt(static_cast<il2cpp_array_size_t>(L_389)))));
		UInt32U5BU5D_t2133601851* L_390 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_391 = V_1;
		NullCheck(L_390);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_390, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_391>>((int32_t)24))))));
		uintptr_t L_392 = (((uintptr_t)((int32_t)((uint32_t)L_391>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_393 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_394 = V_0;
		NullCheck(L_393);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_393, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_394>>((int32_t)16)))))));
		int32_t L_395 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_394>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_396 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_397 = V_3;
		NullCheck(L_396);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_396, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_397>>8))))));
		int32_t L_398 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_397>>8)))));
		UInt32U5BU5D_t2133601851* L_399 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_400 = V_2;
		NullCheck(L_399);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_399, (((int32_t)((uint8_t)L_400))));
		int32_t L_401 = (((int32_t)((uint8_t)L_400)));
		UInt32U5BU5D_t2133601851* L_402 = ___ekey;
		NullCheck(L_402);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_402, ((int32_t)29));
		int32_t L_403 = ((int32_t)29);
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_390)->GetAt(static_cast<il2cpp_array_size_t>(L_392)))^(int32_t)((L_393)->GetAt(static_cast<il2cpp_array_size_t>(L_395)))))^(int32_t)((L_396)->GetAt(static_cast<il2cpp_array_size_t>(L_398)))))^(int32_t)((L_399)->GetAt(static_cast<il2cpp_array_size_t>(L_401)))))^(int32_t)((L_402)->GetAt(static_cast<il2cpp_array_size_t>(L_403)))));
		UInt32U5BU5D_t2133601851* L_404 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_405 = V_2;
		NullCheck(L_404);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_404, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_405>>((int32_t)24))))));
		uintptr_t L_406 = (((uintptr_t)((int32_t)((uint32_t)L_405>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_407 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_408 = V_1;
		NullCheck(L_407);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_407, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_408>>((int32_t)16)))))));
		int32_t L_409 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_408>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_410 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_411 = V_0;
		NullCheck(L_410);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_410, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_411>>8))))));
		int32_t L_412 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_411>>8)))));
		UInt32U5BU5D_t2133601851* L_413 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_414 = V_3;
		NullCheck(L_413);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_413, (((int32_t)((uint8_t)L_414))));
		int32_t L_415 = (((int32_t)((uint8_t)L_414)));
		UInt32U5BU5D_t2133601851* L_416 = ___ekey;
		NullCheck(L_416);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_416, ((int32_t)30));
		int32_t L_417 = ((int32_t)30);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_404)->GetAt(static_cast<il2cpp_array_size_t>(L_406)))^(int32_t)((L_407)->GetAt(static_cast<il2cpp_array_size_t>(L_409)))))^(int32_t)((L_410)->GetAt(static_cast<il2cpp_array_size_t>(L_412)))))^(int32_t)((L_413)->GetAt(static_cast<il2cpp_array_size_t>(L_415)))))^(int32_t)((L_416)->GetAt(static_cast<il2cpp_array_size_t>(L_417)))));
		UInt32U5BU5D_t2133601851* L_418 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_419 = V_3;
		NullCheck(L_418);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_418, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_419>>((int32_t)24))))));
		uintptr_t L_420 = (((uintptr_t)((int32_t)((uint32_t)L_419>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_421 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_422 = V_2;
		NullCheck(L_421);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_421, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_422>>((int32_t)16)))))));
		int32_t L_423 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_422>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_424 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_425 = V_1;
		NullCheck(L_424);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_424, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_425>>8))))));
		int32_t L_426 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_425>>8)))));
		UInt32U5BU5D_t2133601851* L_427 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_428 = V_0;
		NullCheck(L_427);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_427, (((int32_t)((uint8_t)L_428))));
		int32_t L_429 = (((int32_t)((uint8_t)L_428)));
		UInt32U5BU5D_t2133601851* L_430 = ___ekey;
		NullCheck(L_430);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_430, ((int32_t)31));
		int32_t L_431 = ((int32_t)31);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_418)->GetAt(static_cast<il2cpp_array_size_t>(L_420)))^(int32_t)((L_421)->GetAt(static_cast<il2cpp_array_size_t>(L_423)))))^(int32_t)((L_424)->GetAt(static_cast<il2cpp_array_size_t>(L_426)))))^(int32_t)((L_427)->GetAt(static_cast<il2cpp_array_size_t>(L_429)))))^(int32_t)((L_430)->GetAt(static_cast<il2cpp_array_size_t>(L_431)))));
		UInt32U5BU5D_t2133601851* L_432 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_433 = V_4;
		NullCheck(L_432);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_432, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_433>>((int32_t)24))))));
		uintptr_t L_434 = (((uintptr_t)((int32_t)((uint32_t)L_433>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_435 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_436 = V_7;
		NullCheck(L_435);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_435, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_436>>((int32_t)16)))))));
		int32_t L_437 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_436>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_438 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_439 = V_6;
		NullCheck(L_438);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_438, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_439>>8))))));
		int32_t L_440 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_439>>8)))));
		UInt32U5BU5D_t2133601851* L_441 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_442 = V_5;
		NullCheck(L_441);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_441, (((int32_t)((uint8_t)L_442))));
		int32_t L_443 = (((int32_t)((uint8_t)L_442)));
		UInt32U5BU5D_t2133601851* L_444 = ___ekey;
		NullCheck(L_444);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_444, ((int32_t)32));
		int32_t L_445 = ((int32_t)32);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_432)->GetAt(static_cast<il2cpp_array_size_t>(L_434)))^(int32_t)((L_435)->GetAt(static_cast<il2cpp_array_size_t>(L_437)))))^(int32_t)((L_438)->GetAt(static_cast<il2cpp_array_size_t>(L_440)))))^(int32_t)((L_441)->GetAt(static_cast<il2cpp_array_size_t>(L_443)))))^(int32_t)((L_444)->GetAt(static_cast<il2cpp_array_size_t>(L_445)))));
		UInt32U5BU5D_t2133601851* L_446 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_447 = V_5;
		NullCheck(L_446);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_446, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_447>>((int32_t)24))))));
		uintptr_t L_448 = (((uintptr_t)((int32_t)((uint32_t)L_447>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_449 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_450 = V_4;
		NullCheck(L_449);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_449, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_450>>((int32_t)16)))))));
		int32_t L_451 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_450>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_452 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_453 = V_7;
		NullCheck(L_452);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_452, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_453>>8))))));
		int32_t L_454 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_453>>8)))));
		UInt32U5BU5D_t2133601851* L_455 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_456 = V_6;
		NullCheck(L_455);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_455, (((int32_t)((uint8_t)L_456))));
		int32_t L_457 = (((int32_t)((uint8_t)L_456)));
		UInt32U5BU5D_t2133601851* L_458 = ___ekey;
		NullCheck(L_458);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_458, ((int32_t)33));
		int32_t L_459 = ((int32_t)33);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_446)->GetAt(static_cast<il2cpp_array_size_t>(L_448)))^(int32_t)((L_449)->GetAt(static_cast<il2cpp_array_size_t>(L_451)))))^(int32_t)((L_452)->GetAt(static_cast<il2cpp_array_size_t>(L_454)))))^(int32_t)((L_455)->GetAt(static_cast<il2cpp_array_size_t>(L_457)))))^(int32_t)((L_458)->GetAt(static_cast<il2cpp_array_size_t>(L_459)))));
		UInt32U5BU5D_t2133601851* L_460 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_461 = V_6;
		NullCheck(L_460);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_460, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_461>>((int32_t)24))))));
		uintptr_t L_462 = (((uintptr_t)((int32_t)((uint32_t)L_461>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_463 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_464 = V_5;
		NullCheck(L_463);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_463, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_464>>((int32_t)16)))))));
		int32_t L_465 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_464>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_466 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_467 = V_4;
		NullCheck(L_466);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_466, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_467>>8))))));
		int32_t L_468 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_467>>8)))));
		UInt32U5BU5D_t2133601851* L_469 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_470 = V_7;
		NullCheck(L_469);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_469, (((int32_t)((uint8_t)L_470))));
		int32_t L_471 = (((int32_t)((uint8_t)L_470)));
		UInt32U5BU5D_t2133601851* L_472 = ___ekey;
		NullCheck(L_472);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_472, ((int32_t)34));
		int32_t L_473 = ((int32_t)34);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_460)->GetAt(static_cast<il2cpp_array_size_t>(L_462)))^(int32_t)((L_463)->GetAt(static_cast<il2cpp_array_size_t>(L_465)))))^(int32_t)((L_466)->GetAt(static_cast<il2cpp_array_size_t>(L_468)))))^(int32_t)((L_469)->GetAt(static_cast<il2cpp_array_size_t>(L_471)))))^(int32_t)((L_472)->GetAt(static_cast<il2cpp_array_size_t>(L_473)))));
		UInt32U5BU5D_t2133601851* L_474 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_475 = V_7;
		NullCheck(L_474);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_474, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_475>>((int32_t)24))))));
		uintptr_t L_476 = (((uintptr_t)((int32_t)((uint32_t)L_475>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_477 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_478 = V_6;
		NullCheck(L_477);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_477, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_478>>((int32_t)16)))))));
		int32_t L_479 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_478>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_480 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_481 = V_5;
		NullCheck(L_480);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_480, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_481>>8))))));
		int32_t L_482 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_481>>8)))));
		UInt32U5BU5D_t2133601851* L_483 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_484 = V_4;
		NullCheck(L_483);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_483, (((int32_t)((uint8_t)L_484))));
		int32_t L_485 = (((int32_t)((uint8_t)L_484)));
		UInt32U5BU5D_t2133601851* L_486 = ___ekey;
		NullCheck(L_486);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_486, ((int32_t)35));
		int32_t L_487 = ((int32_t)35);
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_474)->GetAt(static_cast<il2cpp_array_size_t>(L_476)))^(int32_t)((L_477)->GetAt(static_cast<il2cpp_array_size_t>(L_479)))))^(int32_t)((L_480)->GetAt(static_cast<il2cpp_array_size_t>(L_482)))))^(int32_t)((L_483)->GetAt(static_cast<il2cpp_array_size_t>(L_485)))))^(int32_t)((L_486)->GetAt(static_cast<il2cpp_array_size_t>(L_487)))));
		UInt32U5BU5D_t2133601851* L_488 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_489 = V_0;
		NullCheck(L_488);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_488, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_489>>((int32_t)24))))));
		uintptr_t L_490 = (((uintptr_t)((int32_t)((uint32_t)L_489>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_491 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_492 = V_3;
		NullCheck(L_491);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_491, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_492>>((int32_t)16)))))));
		int32_t L_493 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_492>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_494 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_495 = V_2;
		NullCheck(L_494);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_494, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_495>>8))))));
		int32_t L_496 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_495>>8)))));
		UInt32U5BU5D_t2133601851* L_497 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_498 = V_1;
		NullCheck(L_497);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_497, (((int32_t)((uint8_t)L_498))));
		int32_t L_499 = (((int32_t)((uint8_t)L_498)));
		UInt32U5BU5D_t2133601851* L_500 = ___ekey;
		NullCheck(L_500);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_500, ((int32_t)36));
		int32_t L_501 = ((int32_t)36);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_488)->GetAt(static_cast<il2cpp_array_size_t>(L_490)))^(int32_t)((L_491)->GetAt(static_cast<il2cpp_array_size_t>(L_493)))))^(int32_t)((L_494)->GetAt(static_cast<il2cpp_array_size_t>(L_496)))))^(int32_t)((L_497)->GetAt(static_cast<il2cpp_array_size_t>(L_499)))))^(int32_t)((L_500)->GetAt(static_cast<il2cpp_array_size_t>(L_501)))));
		UInt32U5BU5D_t2133601851* L_502 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_503 = V_1;
		NullCheck(L_502);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_502, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_503>>((int32_t)24))))));
		uintptr_t L_504 = (((uintptr_t)((int32_t)((uint32_t)L_503>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_505 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_506 = V_0;
		NullCheck(L_505);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_505, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_506>>((int32_t)16)))))));
		int32_t L_507 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_506>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_508 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_509 = V_3;
		NullCheck(L_508);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_508, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_509>>8))))));
		int32_t L_510 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_509>>8)))));
		UInt32U5BU5D_t2133601851* L_511 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_512 = V_2;
		NullCheck(L_511);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_511, (((int32_t)((uint8_t)L_512))));
		int32_t L_513 = (((int32_t)((uint8_t)L_512)));
		UInt32U5BU5D_t2133601851* L_514 = ___ekey;
		NullCheck(L_514);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_514, ((int32_t)37));
		int32_t L_515 = ((int32_t)37);
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_502)->GetAt(static_cast<il2cpp_array_size_t>(L_504)))^(int32_t)((L_505)->GetAt(static_cast<il2cpp_array_size_t>(L_507)))))^(int32_t)((L_508)->GetAt(static_cast<il2cpp_array_size_t>(L_510)))))^(int32_t)((L_511)->GetAt(static_cast<il2cpp_array_size_t>(L_513)))))^(int32_t)((L_514)->GetAt(static_cast<il2cpp_array_size_t>(L_515)))));
		UInt32U5BU5D_t2133601851* L_516 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_517 = V_2;
		NullCheck(L_516);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_516, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_517>>((int32_t)24))))));
		uintptr_t L_518 = (((uintptr_t)((int32_t)((uint32_t)L_517>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_519 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_520 = V_1;
		NullCheck(L_519);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_519, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_520>>((int32_t)16)))))));
		int32_t L_521 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_520>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_522 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_523 = V_0;
		NullCheck(L_522);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_522, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_523>>8))))));
		int32_t L_524 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_523>>8)))));
		UInt32U5BU5D_t2133601851* L_525 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_526 = V_3;
		NullCheck(L_525);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_525, (((int32_t)((uint8_t)L_526))));
		int32_t L_527 = (((int32_t)((uint8_t)L_526)));
		UInt32U5BU5D_t2133601851* L_528 = ___ekey;
		NullCheck(L_528);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_528, ((int32_t)38));
		int32_t L_529 = ((int32_t)38);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_516)->GetAt(static_cast<il2cpp_array_size_t>(L_518)))^(int32_t)((L_519)->GetAt(static_cast<il2cpp_array_size_t>(L_521)))))^(int32_t)((L_522)->GetAt(static_cast<il2cpp_array_size_t>(L_524)))))^(int32_t)((L_525)->GetAt(static_cast<il2cpp_array_size_t>(L_527)))))^(int32_t)((L_528)->GetAt(static_cast<il2cpp_array_size_t>(L_529)))));
		UInt32U5BU5D_t2133601851* L_530 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_531 = V_3;
		NullCheck(L_530);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_530, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_531>>((int32_t)24))))));
		uintptr_t L_532 = (((uintptr_t)((int32_t)((uint32_t)L_531>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_533 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_534 = V_2;
		NullCheck(L_533);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_533, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_534>>((int32_t)16)))))));
		int32_t L_535 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_534>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_536 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_537 = V_1;
		NullCheck(L_536);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_536, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_537>>8))))));
		int32_t L_538 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_537>>8)))));
		UInt32U5BU5D_t2133601851* L_539 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_540 = V_0;
		NullCheck(L_539);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_539, (((int32_t)((uint8_t)L_540))));
		int32_t L_541 = (((int32_t)((uint8_t)L_540)));
		UInt32U5BU5D_t2133601851* L_542 = ___ekey;
		NullCheck(L_542);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_542, ((int32_t)39));
		int32_t L_543 = ((int32_t)39);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_530)->GetAt(static_cast<il2cpp_array_size_t>(L_532)))^(int32_t)((L_533)->GetAt(static_cast<il2cpp_array_size_t>(L_535)))))^(int32_t)((L_536)->GetAt(static_cast<il2cpp_array_size_t>(L_538)))))^(int32_t)((L_539)->GetAt(static_cast<il2cpp_array_size_t>(L_541)))))^(int32_t)((L_542)->GetAt(static_cast<il2cpp_array_size_t>(L_543)))));
		int32_t L_544 = __this->get_Nr_14();
		if ((((int32_t)L_544) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0b08;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2133601851* L_545 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_546 = V_4;
		NullCheck(L_545);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_545, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_546>>((int32_t)24))))));
		uintptr_t L_547 = (((uintptr_t)((int32_t)((uint32_t)L_546>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_548 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_549 = V_7;
		NullCheck(L_548);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_548, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_549>>((int32_t)16)))))));
		int32_t L_550 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_549>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_551 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_552 = V_6;
		NullCheck(L_551);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_551, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_552>>8))))));
		int32_t L_553 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_552>>8)))));
		UInt32U5BU5D_t2133601851* L_554 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_555 = V_5;
		NullCheck(L_554);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_554, (((int32_t)((uint8_t)L_555))));
		int32_t L_556 = (((int32_t)((uint8_t)L_555)));
		UInt32U5BU5D_t2133601851* L_557 = ___ekey;
		NullCheck(L_557);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_557, ((int32_t)40));
		int32_t L_558 = ((int32_t)40);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_545)->GetAt(static_cast<il2cpp_array_size_t>(L_547)))^(int32_t)((L_548)->GetAt(static_cast<il2cpp_array_size_t>(L_550)))))^(int32_t)((L_551)->GetAt(static_cast<il2cpp_array_size_t>(L_553)))))^(int32_t)((L_554)->GetAt(static_cast<il2cpp_array_size_t>(L_556)))))^(int32_t)((L_557)->GetAt(static_cast<il2cpp_array_size_t>(L_558)))));
		UInt32U5BU5D_t2133601851* L_559 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_560 = V_5;
		NullCheck(L_559);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_559, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_560>>((int32_t)24))))));
		uintptr_t L_561 = (((uintptr_t)((int32_t)((uint32_t)L_560>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_562 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_563 = V_4;
		NullCheck(L_562);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_562, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_563>>((int32_t)16)))))));
		int32_t L_564 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_563>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_565 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_566 = V_7;
		NullCheck(L_565);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_565, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_566>>8))))));
		int32_t L_567 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_566>>8)))));
		UInt32U5BU5D_t2133601851* L_568 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_569 = V_6;
		NullCheck(L_568);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_568, (((int32_t)((uint8_t)L_569))));
		int32_t L_570 = (((int32_t)((uint8_t)L_569)));
		UInt32U5BU5D_t2133601851* L_571 = ___ekey;
		NullCheck(L_571);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_571, ((int32_t)41));
		int32_t L_572 = ((int32_t)41);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_559)->GetAt(static_cast<il2cpp_array_size_t>(L_561)))^(int32_t)((L_562)->GetAt(static_cast<il2cpp_array_size_t>(L_564)))))^(int32_t)((L_565)->GetAt(static_cast<il2cpp_array_size_t>(L_567)))))^(int32_t)((L_568)->GetAt(static_cast<il2cpp_array_size_t>(L_570)))))^(int32_t)((L_571)->GetAt(static_cast<il2cpp_array_size_t>(L_572)))));
		UInt32U5BU5D_t2133601851* L_573 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_574 = V_6;
		NullCheck(L_573);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_573, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_574>>((int32_t)24))))));
		uintptr_t L_575 = (((uintptr_t)((int32_t)((uint32_t)L_574>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_576 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_577 = V_5;
		NullCheck(L_576);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_576, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_577>>((int32_t)16)))))));
		int32_t L_578 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_577>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_579 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_580 = V_4;
		NullCheck(L_579);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_579, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_580>>8))))));
		int32_t L_581 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_580>>8)))));
		UInt32U5BU5D_t2133601851* L_582 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_583 = V_7;
		NullCheck(L_582);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_582, (((int32_t)((uint8_t)L_583))));
		int32_t L_584 = (((int32_t)((uint8_t)L_583)));
		UInt32U5BU5D_t2133601851* L_585 = ___ekey;
		NullCheck(L_585);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_585, ((int32_t)42));
		int32_t L_586 = ((int32_t)42);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_573)->GetAt(static_cast<il2cpp_array_size_t>(L_575)))^(int32_t)((L_576)->GetAt(static_cast<il2cpp_array_size_t>(L_578)))))^(int32_t)((L_579)->GetAt(static_cast<il2cpp_array_size_t>(L_581)))))^(int32_t)((L_582)->GetAt(static_cast<il2cpp_array_size_t>(L_584)))))^(int32_t)((L_585)->GetAt(static_cast<il2cpp_array_size_t>(L_586)))));
		UInt32U5BU5D_t2133601851* L_587 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_588 = V_7;
		NullCheck(L_587);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_587, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_588>>((int32_t)24))))));
		uintptr_t L_589 = (((uintptr_t)((int32_t)((uint32_t)L_588>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_590 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_591 = V_6;
		NullCheck(L_590);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_590, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_591>>((int32_t)16)))))));
		int32_t L_592 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_591>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_593 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_594 = V_5;
		NullCheck(L_593);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_593, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_594>>8))))));
		int32_t L_595 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_594>>8)))));
		UInt32U5BU5D_t2133601851* L_596 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_597 = V_4;
		NullCheck(L_596);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_596, (((int32_t)((uint8_t)L_597))));
		int32_t L_598 = (((int32_t)((uint8_t)L_597)));
		UInt32U5BU5D_t2133601851* L_599 = ___ekey;
		NullCheck(L_599);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_599, ((int32_t)43));
		int32_t L_600 = ((int32_t)43);
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_587)->GetAt(static_cast<il2cpp_array_size_t>(L_589)))^(int32_t)((L_590)->GetAt(static_cast<il2cpp_array_size_t>(L_592)))))^(int32_t)((L_593)->GetAt(static_cast<il2cpp_array_size_t>(L_595)))))^(int32_t)((L_596)->GetAt(static_cast<il2cpp_array_size_t>(L_598)))))^(int32_t)((L_599)->GetAt(static_cast<il2cpp_array_size_t>(L_600)))));
		UInt32U5BU5D_t2133601851* L_601 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_602 = V_0;
		NullCheck(L_601);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_601, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_602>>((int32_t)24))))));
		uintptr_t L_603 = (((uintptr_t)((int32_t)((uint32_t)L_602>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_604 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_605 = V_3;
		NullCheck(L_604);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_604, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_605>>((int32_t)16)))))));
		int32_t L_606 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_605>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_607 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_608 = V_2;
		NullCheck(L_607);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_607, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_608>>8))))));
		int32_t L_609 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_608>>8)))));
		UInt32U5BU5D_t2133601851* L_610 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_611 = V_1;
		NullCheck(L_610);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_610, (((int32_t)((uint8_t)L_611))));
		int32_t L_612 = (((int32_t)((uint8_t)L_611)));
		UInt32U5BU5D_t2133601851* L_613 = ___ekey;
		NullCheck(L_613);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_613, ((int32_t)44));
		int32_t L_614 = ((int32_t)44);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_601)->GetAt(static_cast<il2cpp_array_size_t>(L_603)))^(int32_t)((L_604)->GetAt(static_cast<il2cpp_array_size_t>(L_606)))))^(int32_t)((L_607)->GetAt(static_cast<il2cpp_array_size_t>(L_609)))))^(int32_t)((L_610)->GetAt(static_cast<il2cpp_array_size_t>(L_612)))))^(int32_t)((L_613)->GetAt(static_cast<il2cpp_array_size_t>(L_614)))));
		UInt32U5BU5D_t2133601851* L_615 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_616 = V_1;
		NullCheck(L_615);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_615, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_616>>((int32_t)24))))));
		uintptr_t L_617 = (((uintptr_t)((int32_t)((uint32_t)L_616>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_618 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_619 = V_0;
		NullCheck(L_618);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_618, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_619>>((int32_t)16)))))));
		int32_t L_620 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_619>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_621 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_622 = V_3;
		NullCheck(L_621);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_621, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_622>>8))))));
		int32_t L_623 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_622>>8)))));
		UInt32U5BU5D_t2133601851* L_624 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_625 = V_2;
		NullCheck(L_624);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_624, (((int32_t)((uint8_t)L_625))));
		int32_t L_626 = (((int32_t)((uint8_t)L_625)));
		UInt32U5BU5D_t2133601851* L_627 = ___ekey;
		NullCheck(L_627);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_627, ((int32_t)45));
		int32_t L_628 = ((int32_t)45);
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_615)->GetAt(static_cast<il2cpp_array_size_t>(L_617)))^(int32_t)((L_618)->GetAt(static_cast<il2cpp_array_size_t>(L_620)))))^(int32_t)((L_621)->GetAt(static_cast<il2cpp_array_size_t>(L_623)))))^(int32_t)((L_624)->GetAt(static_cast<il2cpp_array_size_t>(L_626)))))^(int32_t)((L_627)->GetAt(static_cast<il2cpp_array_size_t>(L_628)))));
		UInt32U5BU5D_t2133601851* L_629 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_630 = V_2;
		NullCheck(L_629);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_629, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_630>>((int32_t)24))))));
		uintptr_t L_631 = (((uintptr_t)((int32_t)((uint32_t)L_630>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_632 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_633 = V_1;
		NullCheck(L_632);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_632, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_633>>((int32_t)16)))))));
		int32_t L_634 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_633>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_635 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_636 = V_0;
		NullCheck(L_635);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_635, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_636>>8))))));
		int32_t L_637 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_636>>8)))));
		UInt32U5BU5D_t2133601851* L_638 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_639 = V_3;
		NullCheck(L_638);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_638, (((int32_t)((uint8_t)L_639))));
		int32_t L_640 = (((int32_t)((uint8_t)L_639)));
		UInt32U5BU5D_t2133601851* L_641 = ___ekey;
		NullCheck(L_641);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_641, ((int32_t)46));
		int32_t L_642 = ((int32_t)46);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_629)->GetAt(static_cast<il2cpp_array_size_t>(L_631)))^(int32_t)((L_632)->GetAt(static_cast<il2cpp_array_size_t>(L_634)))))^(int32_t)((L_635)->GetAt(static_cast<il2cpp_array_size_t>(L_637)))))^(int32_t)((L_638)->GetAt(static_cast<il2cpp_array_size_t>(L_640)))))^(int32_t)((L_641)->GetAt(static_cast<il2cpp_array_size_t>(L_642)))));
		UInt32U5BU5D_t2133601851* L_643 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_644 = V_3;
		NullCheck(L_643);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_643, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_644>>((int32_t)24))))));
		uintptr_t L_645 = (((uintptr_t)((int32_t)((uint32_t)L_644>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_646 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_647 = V_2;
		NullCheck(L_646);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_646, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_647>>((int32_t)16)))))));
		int32_t L_648 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_647>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_649 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_650 = V_1;
		NullCheck(L_649);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_649, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_650>>8))))));
		int32_t L_651 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_650>>8)))));
		UInt32U5BU5D_t2133601851* L_652 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_653 = V_0;
		NullCheck(L_652);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_652, (((int32_t)((uint8_t)L_653))));
		int32_t L_654 = (((int32_t)((uint8_t)L_653)));
		UInt32U5BU5D_t2133601851* L_655 = ___ekey;
		NullCheck(L_655);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_655, ((int32_t)47));
		int32_t L_656 = ((int32_t)47);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_643)->GetAt(static_cast<il2cpp_array_size_t>(L_645)))^(int32_t)((L_646)->GetAt(static_cast<il2cpp_array_size_t>(L_648)))))^(int32_t)((L_649)->GetAt(static_cast<il2cpp_array_size_t>(L_651)))))^(int32_t)((L_652)->GetAt(static_cast<il2cpp_array_size_t>(L_654)))))^(int32_t)((L_655)->GetAt(static_cast<il2cpp_array_size_t>(L_656)))));
		V_8 = ((int32_t)48);
		int32_t L_657 = __this->get_Nr_14();
		if ((((int32_t)L_657) <= ((int32_t)((int32_t)12))))
		{
			goto IL_0b08;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2133601851* L_658 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_659 = V_4;
		NullCheck(L_658);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_658, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_659>>((int32_t)24))))));
		uintptr_t L_660 = (((uintptr_t)((int32_t)((uint32_t)L_659>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_661 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_662 = V_7;
		NullCheck(L_661);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_661, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_662>>((int32_t)16)))))));
		int32_t L_663 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_662>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_664 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_665 = V_6;
		NullCheck(L_664);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_664, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_665>>8))))));
		int32_t L_666 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_665>>8)))));
		UInt32U5BU5D_t2133601851* L_667 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_668 = V_5;
		NullCheck(L_667);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_667, (((int32_t)((uint8_t)L_668))));
		int32_t L_669 = (((int32_t)((uint8_t)L_668)));
		UInt32U5BU5D_t2133601851* L_670 = ___ekey;
		NullCheck(L_670);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_670, ((int32_t)48));
		int32_t L_671 = ((int32_t)48);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_658)->GetAt(static_cast<il2cpp_array_size_t>(L_660)))^(int32_t)((L_661)->GetAt(static_cast<il2cpp_array_size_t>(L_663)))))^(int32_t)((L_664)->GetAt(static_cast<il2cpp_array_size_t>(L_666)))))^(int32_t)((L_667)->GetAt(static_cast<il2cpp_array_size_t>(L_669)))))^(int32_t)((L_670)->GetAt(static_cast<il2cpp_array_size_t>(L_671)))));
		UInt32U5BU5D_t2133601851* L_672 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_673 = V_5;
		NullCheck(L_672);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_672, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_673>>((int32_t)24))))));
		uintptr_t L_674 = (((uintptr_t)((int32_t)((uint32_t)L_673>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_675 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_676 = V_4;
		NullCheck(L_675);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_675, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_676>>((int32_t)16)))))));
		int32_t L_677 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_676>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_678 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_679 = V_7;
		NullCheck(L_678);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_678, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_679>>8))))));
		int32_t L_680 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_679>>8)))));
		UInt32U5BU5D_t2133601851* L_681 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_682 = V_6;
		NullCheck(L_681);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_681, (((int32_t)((uint8_t)L_682))));
		int32_t L_683 = (((int32_t)((uint8_t)L_682)));
		UInt32U5BU5D_t2133601851* L_684 = ___ekey;
		NullCheck(L_684);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_684, ((int32_t)49));
		int32_t L_685 = ((int32_t)49);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_672)->GetAt(static_cast<il2cpp_array_size_t>(L_674)))^(int32_t)((L_675)->GetAt(static_cast<il2cpp_array_size_t>(L_677)))))^(int32_t)((L_678)->GetAt(static_cast<il2cpp_array_size_t>(L_680)))))^(int32_t)((L_681)->GetAt(static_cast<il2cpp_array_size_t>(L_683)))))^(int32_t)((L_684)->GetAt(static_cast<il2cpp_array_size_t>(L_685)))));
		UInt32U5BU5D_t2133601851* L_686 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_687 = V_6;
		NullCheck(L_686);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_686, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_687>>((int32_t)24))))));
		uintptr_t L_688 = (((uintptr_t)((int32_t)((uint32_t)L_687>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_689 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_690 = V_5;
		NullCheck(L_689);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_689, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_690>>((int32_t)16)))))));
		int32_t L_691 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_690>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_692 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_693 = V_4;
		NullCheck(L_692);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_692, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_693>>8))))));
		int32_t L_694 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_693>>8)))));
		UInt32U5BU5D_t2133601851* L_695 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_696 = V_7;
		NullCheck(L_695);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_695, (((int32_t)((uint8_t)L_696))));
		int32_t L_697 = (((int32_t)((uint8_t)L_696)));
		UInt32U5BU5D_t2133601851* L_698 = ___ekey;
		NullCheck(L_698);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_698, ((int32_t)50));
		int32_t L_699 = ((int32_t)50);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_686)->GetAt(static_cast<il2cpp_array_size_t>(L_688)))^(int32_t)((L_689)->GetAt(static_cast<il2cpp_array_size_t>(L_691)))))^(int32_t)((L_692)->GetAt(static_cast<il2cpp_array_size_t>(L_694)))))^(int32_t)((L_695)->GetAt(static_cast<il2cpp_array_size_t>(L_697)))))^(int32_t)((L_698)->GetAt(static_cast<il2cpp_array_size_t>(L_699)))));
		UInt32U5BU5D_t2133601851* L_700 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_701 = V_7;
		NullCheck(L_700);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_700, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_701>>((int32_t)24))))));
		uintptr_t L_702 = (((uintptr_t)((int32_t)((uint32_t)L_701>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_703 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_704 = V_6;
		NullCheck(L_703);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_703, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_704>>((int32_t)16)))))));
		int32_t L_705 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_704>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_706 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_707 = V_5;
		NullCheck(L_706);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_706, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_707>>8))))));
		int32_t L_708 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_707>>8)))));
		UInt32U5BU5D_t2133601851* L_709 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_710 = V_4;
		NullCheck(L_709);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_709, (((int32_t)((uint8_t)L_710))));
		int32_t L_711 = (((int32_t)((uint8_t)L_710)));
		UInt32U5BU5D_t2133601851* L_712 = ___ekey;
		NullCheck(L_712);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_712, ((int32_t)51));
		int32_t L_713 = ((int32_t)51);
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_700)->GetAt(static_cast<il2cpp_array_size_t>(L_702)))^(int32_t)((L_703)->GetAt(static_cast<il2cpp_array_size_t>(L_705)))))^(int32_t)((L_706)->GetAt(static_cast<il2cpp_array_size_t>(L_708)))))^(int32_t)((L_709)->GetAt(static_cast<il2cpp_array_size_t>(L_711)))))^(int32_t)((L_712)->GetAt(static_cast<il2cpp_array_size_t>(L_713)))));
		UInt32U5BU5D_t2133601851* L_714 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_715 = V_0;
		NullCheck(L_714);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_714, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_715>>((int32_t)24))))));
		uintptr_t L_716 = (((uintptr_t)((int32_t)((uint32_t)L_715>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_717 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_718 = V_3;
		NullCheck(L_717);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_717, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_718>>((int32_t)16)))))));
		int32_t L_719 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_718>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_720 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_721 = V_2;
		NullCheck(L_720);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_720, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_721>>8))))));
		int32_t L_722 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_721>>8)))));
		UInt32U5BU5D_t2133601851* L_723 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_724 = V_1;
		NullCheck(L_723);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_723, (((int32_t)((uint8_t)L_724))));
		int32_t L_725 = (((int32_t)((uint8_t)L_724)));
		UInt32U5BU5D_t2133601851* L_726 = ___ekey;
		NullCheck(L_726);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_726, ((int32_t)52));
		int32_t L_727 = ((int32_t)52);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_714)->GetAt(static_cast<il2cpp_array_size_t>(L_716)))^(int32_t)((L_717)->GetAt(static_cast<il2cpp_array_size_t>(L_719)))))^(int32_t)((L_720)->GetAt(static_cast<il2cpp_array_size_t>(L_722)))))^(int32_t)((L_723)->GetAt(static_cast<il2cpp_array_size_t>(L_725)))))^(int32_t)((L_726)->GetAt(static_cast<il2cpp_array_size_t>(L_727)))));
		UInt32U5BU5D_t2133601851* L_728 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_729 = V_1;
		NullCheck(L_728);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_728, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_729>>((int32_t)24))))));
		uintptr_t L_730 = (((uintptr_t)((int32_t)((uint32_t)L_729>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_731 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_732 = V_0;
		NullCheck(L_731);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_731, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_732>>((int32_t)16)))))));
		int32_t L_733 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_732>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_734 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_735 = V_3;
		NullCheck(L_734);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_734, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_735>>8))))));
		int32_t L_736 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_735>>8)))));
		UInt32U5BU5D_t2133601851* L_737 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_738 = V_2;
		NullCheck(L_737);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_737, (((int32_t)((uint8_t)L_738))));
		int32_t L_739 = (((int32_t)((uint8_t)L_738)));
		UInt32U5BU5D_t2133601851* L_740 = ___ekey;
		NullCheck(L_740);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_740, ((int32_t)53));
		int32_t L_741 = ((int32_t)53);
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_728)->GetAt(static_cast<il2cpp_array_size_t>(L_730)))^(int32_t)((L_731)->GetAt(static_cast<il2cpp_array_size_t>(L_733)))))^(int32_t)((L_734)->GetAt(static_cast<il2cpp_array_size_t>(L_736)))))^(int32_t)((L_737)->GetAt(static_cast<il2cpp_array_size_t>(L_739)))))^(int32_t)((L_740)->GetAt(static_cast<il2cpp_array_size_t>(L_741)))));
		UInt32U5BU5D_t2133601851* L_742 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_743 = V_2;
		NullCheck(L_742);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_742, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_743>>((int32_t)24))))));
		uintptr_t L_744 = (((uintptr_t)((int32_t)((uint32_t)L_743>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_745 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_746 = V_1;
		NullCheck(L_745);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_745, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_746>>((int32_t)16)))))));
		int32_t L_747 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_746>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_748 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_749 = V_0;
		NullCheck(L_748);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_748, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_749>>8))))));
		int32_t L_750 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_749>>8)))));
		UInt32U5BU5D_t2133601851* L_751 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_752 = V_3;
		NullCheck(L_751);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_751, (((int32_t)((uint8_t)L_752))));
		int32_t L_753 = (((int32_t)((uint8_t)L_752)));
		UInt32U5BU5D_t2133601851* L_754 = ___ekey;
		NullCheck(L_754);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_754, ((int32_t)54));
		int32_t L_755 = ((int32_t)54);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_742)->GetAt(static_cast<il2cpp_array_size_t>(L_744)))^(int32_t)((L_745)->GetAt(static_cast<il2cpp_array_size_t>(L_747)))))^(int32_t)((L_748)->GetAt(static_cast<il2cpp_array_size_t>(L_750)))))^(int32_t)((L_751)->GetAt(static_cast<il2cpp_array_size_t>(L_753)))))^(int32_t)((L_754)->GetAt(static_cast<il2cpp_array_size_t>(L_755)))));
		UInt32U5BU5D_t2133601851* L_756 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT0_22();
		uint32_t L_757 = V_3;
		NullCheck(L_756);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_756, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_757>>((int32_t)24))))));
		uintptr_t L_758 = (((uintptr_t)((int32_t)((uint32_t)L_757>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_759 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT1_23();
		uint32_t L_760 = V_2;
		NullCheck(L_759);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_759, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_760>>((int32_t)16)))))));
		int32_t L_761 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_760>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_762 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT2_24();
		uint32_t L_763 = V_1;
		NullCheck(L_762);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_762, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_763>>8))))));
		int32_t L_764 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_763>>8)))));
		UInt32U5BU5D_t2133601851* L_765 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iT3_25();
		uint32_t L_766 = V_0;
		NullCheck(L_765);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_765, (((int32_t)((uint8_t)L_766))));
		int32_t L_767 = (((int32_t)((uint8_t)L_766)));
		UInt32U5BU5D_t2133601851* L_768 = ___ekey;
		NullCheck(L_768);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_768, ((int32_t)55));
		int32_t L_769 = ((int32_t)55);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((L_756)->GetAt(static_cast<il2cpp_array_size_t>(L_758)))^(int32_t)((L_759)->GetAt(static_cast<il2cpp_array_size_t>(L_761)))))^(int32_t)((L_762)->GetAt(static_cast<il2cpp_array_size_t>(L_764)))))^(int32_t)((L_765)->GetAt(static_cast<il2cpp_array_size_t>(L_767)))))^(int32_t)((L_768)->GetAt(static_cast<il2cpp_array_size_t>(L_769)))));
		V_8 = ((int32_t)56);
	}

IL_0b08:
	{
		ByteU5BU5D_t58506160* L_770 = ___outdata;
		IL2CPP_RUNTIME_CLASS_INIT(AesTransform_t2186883254_il2cpp_TypeInfo_var);
		ByteU5BU5D_t58506160* L_771 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_772 = V_4;
		NullCheck(L_771);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_771, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_772>>((int32_t)24))))));
		uintptr_t L_773 = (((uintptr_t)((int32_t)((uint32_t)L_772>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_774 = ___ekey;
		int32_t L_775 = V_8;
		NullCheck(L_774);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_774, L_775);
		int32_t L_776 = L_775;
		NullCheck(L_770);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_770, 0);
		(L_770)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_771)->GetAt(static_cast<il2cpp_array_size_t>(L_773)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_774)->GetAt(static_cast<il2cpp_array_size_t>(L_776)))>>((int32_t)24))))))))))));
		ByteU5BU5D_t58506160* L_777 = ___outdata;
		ByteU5BU5D_t58506160* L_778 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_779 = V_7;
		NullCheck(L_778);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_778, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_779>>((int32_t)16)))))));
		int32_t L_780 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_779>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_781 = ___ekey;
		int32_t L_782 = V_8;
		NullCheck(L_781);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_781, L_782);
		int32_t L_783 = L_782;
		NullCheck(L_777);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_777, 1);
		(L_777)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_778)->GetAt(static_cast<il2cpp_array_size_t>(L_780)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_781)->GetAt(static_cast<il2cpp_array_size_t>(L_783)))>>((int32_t)16))))))))))));
		ByteU5BU5D_t58506160* L_784 = ___outdata;
		ByteU5BU5D_t58506160* L_785 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_786 = V_6;
		NullCheck(L_785);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_785, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_786>>8))))));
		int32_t L_787 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_786>>8)))));
		UInt32U5BU5D_t2133601851* L_788 = ___ekey;
		int32_t L_789 = V_8;
		NullCheck(L_788);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_788, L_789);
		int32_t L_790 = L_789;
		NullCheck(L_784);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_784, 2);
		(L_784)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_785)->GetAt(static_cast<il2cpp_array_size_t>(L_787)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_788)->GetAt(static_cast<il2cpp_array_size_t>(L_790)))>>8)))))))))));
		ByteU5BU5D_t58506160* L_791 = ___outdata;
		ByteU5BU5D_t58506160* L_792 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_793 = V_5;
		NullCheck(L_792);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_792, (((int32_t)((uint8_t)L_793))));
		int32_t L_794 = (((int32_t)((uint8_t)L_793)));
		UInt32U5BU5D_t2133601851* L_795 = ___ekey;
		int32_t L_796 = V_8;
		int32_t L_797 = L_796;
		V_8 = ((int32_t)((int32_t)L_797+(int32_t)1));
		NullCheck(L_795);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_795, L_797);
		int32_t L_798 = L_797;
		NullCheck(L_791);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_791, 3);
		(L_791)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_792)->GetAt(static_cast<il2cpp_array_size_t>(L_794)))^(int32_t)(((int32_t)((uint8_t)((L_795)->GetAt(static_cast<il2cpp_array_size_t>(L_798))))))))))));
		ByteU5BU5D_t58506160* L_799 = ___outdata;
		ByteU5BU5D_t58506160* L_800 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_801 = V_5;
		NullCheck(L_800);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_800, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_801>>((int32_t)24))))));
		uintptr_t L_802 = (((uintptr_t)((int32_t)((uint32_t)L_801>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_803 = ___ekey;
		int32_t L_804 = V_8;
		NullCheck(L_803);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_803, L_804);
		int32_t L_805 = L_804;
		NullCheck(L_799);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_799, 4);
		(L_799)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_800)->GetAt(static_cast<il2cpp_array_size_t>(L_802)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_803)->GetAt(static_cast<il2cpp_array_size_t>(L_805)))>>((int32_t)24))))))))))));
		ByteU5BU5D_t58506160* L_806 = ___outdata;
		ByteU5BU5D_t58506160* L_807 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_808 = V_4;
		NullCheck(L_807);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_807, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_808>>((int32_t)16)))))));
		int32_t L_809 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_808>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_810 = ___ekey;
		int32_t L_811 = V_8;
		NullCheck(L_810);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_810, L_811);
		int32_t L_812 = L_811;
		NullCheck(L_806);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_806, 5);
		(L_806)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_807)->GetAt(static_cast<il2cpp_array_size_t>(L_809)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_810)->GetAt(static_cast<il2cpp_array_size_t>(L_812)))>>((int32_t)16))))))))))));
		ByteU5BU5D_t58506160* L_813 = ___outdata;
		ByteU5BU5D_t58506160* L_814 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_815 = V_7;
		NullCheck(L_814);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_814, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_815>>8))))));
		int32_t L_816 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_815>>8)))));
		UInt32U5BU5D_t2133601851* L_817 = ___ekey;
		int32_t L_818 = V_8;
		NullCheck(L_817);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_817, L_818);
		int32_t L_819 = L_818;
		NullCheck(L_813);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_813, 6);
		(L_813)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_814)->GetAt(static_cast<il2cpp_array_size_t>(L_816)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_817)->GetAt(static_cast<il2cpp_array_size_t>(L_819)))>>8)))))))))));
		ByteU5BU5D_t58506160* L_820 = ___outdata;
		ByteU5BU5D_t58506160* L_821 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_822 = V_6;
		NullCheck(L_821);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_821, (((int32_t)((uint8_t)L_822))));
		int32_t L_823 = (((int32_t)((uint8_t)L_822)));
		UInt32U5BU5D_t2133601851* L_824 = ___ekey;
		int32_t L_825 = V_8;
		int32_t L_826 = L_825;
		V_8 = ((int32_t)((int32_t)L_826+(int32_t)1));
		NullCheck(L_824);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_824, L_826);
		int32_t L_827 = L_826;
		NullCheck(L_820);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_820, 7);
		(L_820)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_821)->GetAt(static_cast<il2cpp_array_size_t>(L_823)))^(int32_t)(((int32_t)((uint8_t)((L_824)->GetAt(static_cast<il2cpp_array_size_t>(L_827))))))))))));
		ByteU5BU5D_t58506160* L_828 = ___outdata;
		ByteU5BU5D_t58506160* L_829 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_830 = V_6;
		NullCheck(L_829);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_829, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_830>>((int32_t)24))))));
		uintptr_t L_831 = (((uintptr_t)((int32_t)((uint32_t)L_830>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_832 = ___ekey;
		int32_t L_833 = V_8;
		NullCheck(L_832);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_832, L_833);
		int32_t L_834 = L_833;
		NullCheck(L_828);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_828, 8);
		(L_828)->SetAt(static_cast<il2cpp_array_size_t>(8), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_829)->GetAt(static_cast<il2cpp_array_size_t>(L_831)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_832)->GetAt(static_cast<il2cpp_array_size_t>(L_834)))>>((int32_t)24))))))))))));
		ByteU5BU5D_t58506160* L_835 = ___outdata;
		ByteU5BU5D_t58506160* L_836 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_837 = V_5;
		NullCheck(L_836);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_836, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_837>>((int32_t)16)))))));
		int32_t L_838 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_837>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_839 = ___ekey;
		int32_t L_840 = V_8;
		NullCheck(L_839);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_839, L_840);
		int32_t L_841 = L_840;
		NullCheck(L_835);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_835, ((int32_t)9));
		(L_835)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_836)->GetAt(static_cast<il2cpp_array_size_t>(L_838)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_839)->GetAt(static_cast<il2cpp_array_size_t>(L_841)))>>((int32_t)16))))))))))));
		ByteU5BU5D_t58506160* L_842 = ___outdata;
		ByteU5BU5D_t58506160* L_843 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_844 = V_4;
		NullCheck(L_843);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_843, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_844>>8))))));
		int32_t L_845 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_844>>8)))));
		UInt32U5BU5D_t2133601851* L_846 = ___ekey;
		int32_t L_847 = V_8;
		NullCheck(L_846);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_846, L_847);
		int32_t L_848 = L_847;
		NullCheck(L_842);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_842, ((int32_t)10));
		(L_842)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_843)->GetAt(static_cast<il2cpp_array_size_t>(L_845)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_846)->GetAt(static_cast<il2cpp_array_size_t>(L_848)))>>8)))))))))));
		ByteU5BU5D_t58506160* L_849 = ___outdata;
		ByteU5BU5D_t58506160* L_850 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_851 = V_7;
		NullCheck(L_850);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_850, (((int32_t)((uint8_t)L_851))));
		int32_t L_852 = (((int32_t)((uint8_t)L_851)));
		UInt32U5BU5D_t2133601851* L_853 = ___ekey;
		int32_t L_854 = V_8;
		int32_t L_855 = L_854;
		V_8 = ((int32_t)((int32_t)L_855+(int32_t)1));
		NullCheck(L_853);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_853, L_855);
		int32_t L_856 = L_855;
		NullCheck(L_849);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_849, ((int32_t)11));
		(L_849)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_850)->GetAt(static_cast<il2cpp_array_size_t>(L_852)))^(int32_t)(((int32_t)((uint8_t)((L_853)->GetAt(static_cast<il2cpp_array_size_t>(L_856))))))))))));
		ByteU5BU5D_t58506160* L_857 = ___outdata;
		ByteU5BU5D_t58506160* L_858 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_859 = V_7;
		NullCheck(L_858);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_858, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((uint32_t)L_859>>((int32_t)24))))));
		uintptr_t L_860 = (((uintptr_t)((int32_t)((uint32_t)L_859>>((int32_t)24)))));
		UInt32U5BU5D_t2133601851* L_861 = ___ekey;
		int32_t L_862 = V_8;
		NullCheck(L_861);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_861, L_862);
		int32_t L_863 = L_862;
		NullCheck(L_857);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_857, ((int32_t)12));
		(L_857)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_858)->GetAt(static_cast<il2cpp_array_size_t>(L_860)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_861)->GetAt(static_cast<il2cpp_array_size_t>(L_863)))>>((int32_t)24))))))))))));
		ByteU5BU5D_t58506160* L_864 = ___outdata;
		ByteU5BU5D_t58506160* L_865 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_866 = V_6;
		NullCheck(L_865);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_865, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_866>>((int32_t)16)))))));
		int32_t L_867 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_866>>((int32_t)16))))));
		UInt32U5BU5D_t2133601851* L_868 = ___ekey;
		int32_t L_869 = V_8;
		NullCheck(L_868);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_868, L_869);
		int32_t L_870 = L_869;
		NullCheck(L_864);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_864, ((int32_t)13));
		(L_864)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_865)->GetAt(static_cast<il2cpp_array_size_t>(L_867)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_868)->GetAt(static_cast<il2cpp_array_size_t>(L_870)))>>((int32_t)16))))))))))));
		ByteU5BU5D_t58506160* L_871 = ___outdata;
		ByteU5BU5D_t58506160* L_872 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_873 = V_5;
		NullCheck(L_872);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_872, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_873>>8))))));
		int32_t L_874 = (((int32_t)((uint8_t)((int32_t)((uint32_t)L_873>>8)))));
		UInt32U5BU5D_t2133601851* L_875 = ___ekey;
		int32_t L_876 = V_8;
		NullCheck(L_875);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_875, L_876);
		int32_t L_877 = L_876;
		NullCheck(L_871);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_871, ((int32_t)14));
		(L_871)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_872)->GetAt(static_cast<il2cpp_array_size_t>(L_874)))^(int32_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)((L_875)->GetAt(static_cast<il2cpp_array_size_t>(L_877)))>>8)))))))))));
		ByteU5BU5D_t58506160* L_878 = ___outdata;
		ByteU5BU5D_t58506160* L_879 = ((AesTransform_t2186883254_StaticFields*)AesTransform_t2186883254_il2cpp_TypeInfo_var->static_fields)->get_iSBox_17();
		uint32_t L_880 = V_4;
		NullCheck(L_879);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_879, (((int32_t)((uint8_t)L_880))));
		int32_t L_881 = (((int32_t)((uint8_t)L_880)));
		UInt32U5BU5D_t2133601851* L_882 = ___ekey;
		int32_t L_883 = V_8;
		int32_t L_884 = L_883;
		V_8 = ((int32_t)((int32_t)L_884+(int32_t)1));
		NullCheck(L_882);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_882, L_884);
		int32_t L_885 = L_884;
		NullCheck(L_878);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_878, ((int32_t)15));
		(L_878)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_879)->GetAt(static_cast<il2cpp_array_size_t>(L_881)))^(int32_t)(((int32_t)((uint8_t)((L_882)->GetAt(static_cast<il2cpp_array_size_t>(L_885))))))))))));
		return;
	}
}
// System.Void System.TimeZoneInfo::.ctor(System.String,System.TimeSpan,System.String,System.String,System.String,System.TimeZoneInfo/AdjustmentRule[],System.Boolean)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3355;
extern Il2CppCodeGenString* _stringLiteral1317597981;
extern Il2CppCodeGenString* _stringLiteral3748638826;
extern Il2CppCodeGenString* _stringLiteral916180356;
extern Il2CppCodeGenString* _stringLiteral2517002755;
extern Il2CppCodeGenString* _stringLiteral2053451110;
extern Il2CppCodeGenString* _stringLiteral3401696558;
extern Il2CppCodeGenString* _stringLiteral2239094016;
extern Il2CppCodeGenString* _stringLiteral907872332;
extern const uint32_t TimeZoneInfo__ctor_m1869617058_MetadataUsageId;
extern "C"  void TimeZoneInfo__ctor_m1869617058 (TimeZoneInfo_t4131446812 * __this, String_t* ___id, TimeSpan_t763862892  ___baseUtcOffset, String_t* ___displayName, String_t* ___standardDisplayName, String_t* ___daylightDisplayName, AdjustmentRuleU5BU5D_t1713960724* ___adjustmentRules, bool ___disableDaylightSavingTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo__ctor_m1869617058_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AdjustmentRule_t2290566697 * V_0 = NULL;
	AdjustmentRule_t2290566697 * V_1 = NULL;
	AdjustmentRuleU5BU5D_t1713960724* V_2 = NULL;
	int32_t V_3 = 0;
	String_t* G_B30_0 = NULL;
	TimeZoneInfo_t4131446812 * G_B30_1 = NULL;
	String_t* G_B29_0 = NULL;
	TimeZoneInfo_t4131446812 * G_B29_1 = NULL;
	String_t* G_B32_0 = NULL;
	TimeZoneInfo_t4131446812 * G_B32_1 = NULL;
	String_t* G_B31_0 = NULL;
	TimeZoneInfo_t4131446812 * G_B31_1 = NULL;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3355, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		String_t* L_2 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_4 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		ArgumentException_t124305799 * L_5 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_5, _stringLiteral1317597981, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0032:
	{
		int64_t L_6 = TimeSpan_get_Ticks_m315930342((&___baseUtcOffset), /*hidden argument*/NULL);
		if (!((int64_t)((int64_t)L_6%(int64_t)(((int64_t)((int64_t)((int32_t)600000000)))))))
		{
			goto IL_0050;
		}
	}
	{
		ArgumentException_t124305799 * L_7 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_7, _stringLiteral3748638826, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0050:
	{
		TimeSpan_t763862892  L_8 = ___baseUtcOffset;
		TimeSpan_t763862892  L_9;
		memset(&L_9, 0, sizeof(L_9));
		TimeSpan__ctor_m4160332047(&L_9, ((int32_t)14), 0, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_10 = TimeSpan_op_GreaterThan_m3920451985(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0078;
		}
	}
	{
		TimeSpan_t763862892  L_11 = ___baseUtcOffset;
		TimeSpan_t763862892  L_12;
		memset(&L_12, 0, sizeof(L_12));
		TimeSpan__ctor_m4160332047(&L_12, ((int32_t)-14), 0, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_13 = TimeSpan_op_LessThan_m4265983228(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0083;
		}
	}

IL_0078:
	{
		ArgumentOutOfRangeException_t3479058991 * L_14 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_14, _stringLiteral916180356, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_0083:
	{
		AdjustmentRuleU5BU5D_t1713960724* L_15 = ___adjustmentRules;
		if (!L_15)
		{
			goto IL_0181;
		}
	}
	{
		AdjustmentRuleU5BU5D_t1713960724* L_16 = ___adjustmentRules;
		NullCheck(L_16);
		if (!(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))
		{
			goto IL_0181;
		}
	}
	{
		V_0 = (AdjustmentRule_t2290566697 *)NULL;
		AdjustmentRuleU5BU5D_t1713960724* L_17 = ___adjustmentRules;
		V_2 = L_17;
		V_3 = 0;
		goto IL_0178;
	}

IL_009f:
	{
		AdjustmentRuleU5BU5D_t1713960724* L_18 = V_2;
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		V_1 = ((L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20)));
		AdjustmentRule_t2290566697 * L_21 = V_1;
		if (L_21)
		{
			goto IL_00b4;
		}
	}
	{
		InvalidTimeZoneException_t3180604084 * L_22 = (InvalidTimeZoneException_t3180604084 *)il2cpp_codegen_object_new(InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var);
		InvalidTimeZoneException__ctor_m707317189(L_22, _stringLiteral2517002755, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_00b4:
	{
		TimeSpan_t763862892  L_23 = ___baseUtcOffset;
		AdjustmentRule_t2290566697 * L_24 = V_1;
		NullCheck(L_24);
		TimeSpan_t763862892  L_25 = AdjustmentRule_get_DaylightDelta_m3833226735(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_26 = TimeSpan_op_Addition_m141072959(NULL /*static, unused*/, L_23, L_25, /*hidden argument*/NULL);
		TimeSpan_t763862892  L_27;
		memset(&L_27, 0, sizeof(L_27));
		TimeSpan__ctor_m4160332047(&L_27, ((int32_t)-14), 0, 0, /*hidden argument*/NULL);
		bool L_28 = TimeSpan_op_LessThan_m4265983228(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00f2;
		}
	}
	{
		TimeSpan_t763862892  L_29 = ___baseUtcOffset;
		AdjustmentRule_t2290566697 * L_30 = V_1;
		NullCheck(L_30);
		TimeSpan_t763862892  L_31 = AdjustmentRule_get_DaylightDelta_m3833226735(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_32 = TimeSpan_op_Addition_m141072959(NULL /*static, unused*/, L_29, L_31, /*hidden argument*/NULL);
		TimeSpan_t763862892  L_33;
		memset(&L_33, 0, sizeof(L_33));
		TimeSpan__ctor_m4160332047(&L_33, ((int32_t)14), 0, 0, /*hidden argument*/NULL);
		bool L_34 = TimeSpan_op_GreaterThan_m3920451985(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00fd;
		}
	}

IL_00f2:
	{
		InvalidTimeZoneException_t3180604084 * L_35 = (InvalidTimeZoneException_t3180604084 *)il2cpp_codegen_object_new(InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var);
		InvalidTimeZoneException__ctor_m707317189(L_35, _stringLiteral2053451110, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
	}

IL_00fd:
	{
		AdjustmentRule_t2290566697 * L_36 = V_0;
		if (!L_36)
		{
			goto IL_0124;
		}
	}
	{
		AdjustmentRule_t2290566697 * L_37 = V_0;
		NullCheck(L_37);
		DateTime_t339033936  L_38 = AdjustmentRule_get_DateStart_m2606242505(L_37, /*hidden argument*/NULL);
		AdjustmentRule_t2290566697 * L_39 = V_1;
		NullCheck(L_39);
		DateTime_t339033936  L_40 = AdjustmentRule_get_DateStart_m2606242505(L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_41 = DateTime_op_GreaterThan_m3846016869(NULL /*static, unused*/, L_38, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0124;
		}
	}
	{
		InvalidTimeZoneException_t3180604084 * L_42 = (InvalidTimeZoneException_t3180604084 *)il2cpp_codegen_object_new(InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var);
		InvalidTimeZoneException__ctor_m707317189(L_42, _stringLiteral3401696558, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_42);
	}

IL_0124:
	{
		AdjustmentRule_t2290566697 * L_43 = V_0;
		if (!L_43)
		{
			goto IL_014b;
		}
	}
	{
		AdjustmentRule_t2290566697 * L_44 = V_0;
		NullCheck(L_44);
		DateTime_t339033936  L_45 = AdjustmentRule_get_DateEnd_m3953845890(L_44, /*hidden argument*/NULL);
		AdjustmentRule_t2290566697 * L_46 = V_1;
		NullCheck(L_46);
		DateTime_t339033936  L_47 = AdjustmentRule_get_DateStart_m2606242505(L_46, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_48 = DateTime_op_GreaterThan_m3846016869(NULL /*static, unused*/, L_45, L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_014b;
		}
	}
	{
		InvalidTimeZoneException_t3180604084 * L_49 = (InvalidTimeZoneException_t3180604084 *)il2cpp_codegen_object_new(InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var);
		InvalidTimeZoneException__ctor_m707317189(L_49, _stringLiteral2239094016, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_49);
	}

IL_014b:
	{
		AdjustmentRule_t2290566697 * L_50 = V_0;
		if (!L_50)
		{
			goto IL_0172;
		}
	}
	{
		AdjustmentRule_t2290566697 * L_51 = V_0;
		NullCheck(L_51);
		DateTime_t339033936  L_52 = AdjustmentRule_get_DateEnd_m3953845890(L_51, /*hidden argument*/NULL);
		AdjustmentRule_t2290566697 * L_53 = V_1;
		NullCheck(L_53);
		DateTime_t339033936  L_54 = AdjustmentRule_get_DateStart_m2606242505(L_53, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_55 = DateTime_op_Equality_m2277436664(NULL /*static, unused*/, L_52, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0172;
		}
	}
	{
		InvalidTimeZoneException_t3180604084 * L_56 = (InvalidTimeZoneException_t3180604084 *)il2cpp_codegen_object_new(InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var);
		InvalidTimeZoneException__ctor_m707317189(L_56, _stringLiteral907872332, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_56);
	}

IL_0172:
	{
		AdjustmentRule_t2290566697 * L_57 = V_1;
		V_0 = L_57;
		int32_t L_58 = V_3;
		V_3 = ((int32_t)((int32_t)L_58+(int32_t)1));
	}

IL_0178:
	{
		int32_t L_59 = V_3;
		AdjustmentRuleU5BU5D_t1713960724* L_60 = V_2;
		NullCheck(L_60);
		if ((((int32_t)L_59) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_60)->max_length)))))))
		{
			goto IL_009f;
		}
	}

IL_0181:
	{
		String_t* L_61 = ___id;
		__this->set_id_3(L_61);
		TimeSpan_t763862892  L_62 = ___baseUtcOffset;
		__this->set_baseUtcOffset_0(L_62);
		String_t* L_63 = ___displayName;
		String_t* L_64 = L_63;
		G_B29_0 = L_64;
		G_B29_1 = __this;
		if (L_64)
		{
			G_B30_0 = L_64;
			G_B30_1 = __this;
			goto IL_0199;
		}
	}
	{
		String_t* L_65 = ___id;
		G_B30_0 = L_65;
		G_B30_1 = G_B29_1;
	}

IL_0199:
	{
		NullCheck(G_B30_1);
		G_B30_1->set_displayName_2(G_B30_0);
		String_t* L_66 = ___standardDisplayName;
		String_t* L_67 = L_66;
		G_B31_0 = L_67;
		G_B31_1 = __this;
		if (L_67)
		{
			G_B32_0 = L_67;
			G_B32_1 = __this;
			goto IL_01a9;
		}
	}
	{
		String_t* L_68 = ___id;
		G_B32_0 = L_68;
		G_B32_1 = G_B31_1;
	}

IL_01a9:
	{
		NullCheck(G_B32_1);
		G_B32_1->set_standardDisplayName_5(G_B32_0);
		String_t* L_69 = ___daylightDisplayName;
		__this->set_daylightDisplayName_1(L_69);
		bool L_70 = ___disableDaylightSavingTime;
		__this->set_disableDaylightSavingTime_6(L_70);
		AdjustmentRuleU5BU5D_t1713960724* L_71 = ___adjustmentRules;
		__this->set_adjustmentRules_9(L_71);
		return;
	}
}
// System.Void System.TimeZoneInfo::.cctor()
extern "C"  void TimeZoneInfo__cctor_m2439572696 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.TimeSpan System.TimeZoneInfo::get_BaseUtcOffset()
extern "C"  TimeSpan_t763862892  TimeZoneInfo_get_BaseUtcOffset_m418231789 (TimeZoneInfo_t4131446812 * __this, const MethodInfo* method)
{
	{
		TimeSpan_t763862892  L_0 = __this->get_baseUtcOffset_0();
		return L_0;
	}
}
// System.String System.TimeZoneInfo::get_DisplayName()
extern "C"  String_t* TimeZoneInfo_get_DisplayName_m3106101754 (TimeZoneInfo_t4131446812 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_displayName_2();
		return L_0;
	}
}
// System.String System.TimeZoneInfo::get_Id()
extern "C"  String_t* TimeZoneInfo_get_Id_m4206484496 (TimeZoneInfo_t4131446812 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_id_3();
		return L_0;
	}
}
// System.TimeZoneInfo System.TimeZoneInfo::get_Local()
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern TypeInfo* Path_t2029632748_il2cpp_TypeInfo_var;
extern TypeInfo* TimeZoneNotFoundException_t3613219068_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral73592651;
extern Il2CppCodeGenString* _stringLiteral873328174;
extern Il2CppCodeGenString* _stringLiteral3090711736;
extern const uint32_t TimeZoneInfo_get_Local_m1804805602_MetadataUsageId;
extern "C"  TimeZoneInfo_t4131446812 * TimeZoneInfo_get_Local_m1804805602 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_get_Local_m1804805602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_0 = ((TimeZoneInfo_t4131446812_StaticFields*)TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var->static_fields)->get_local_4();
		if (L_0)
		{
			goto IL_0058;
		}
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_1 = TimeZoneInfo_FindSystemTimeZoneByFileName_m3927988317(NULL /*static, unused*/, _stringLiteral73592651, _stringLiteral873328174, /*hidden argument*/NULL);
		((TimeZoneInfo_t4131446812_StaticFields*)TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var->static_fields)->set_local_4(L_1);
		goto IL_0058;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0023;
		throw e;
	}

CATCH_0023:
	{ // begin catch(System.Object)
		{
		}

IL_0024:
		try
		{ // begin try (depth: 2)
			IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
			String_t* L_2 = TimeZoneInfo_get_TimeZoneDirectory_m3879840545(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Path_t2029632748_il2cpp_TypeInfo_var);
			String_t* L_3 = Path_Combine_m4122812896(NULL /*static, unused*/, L_2, _stringLiteral3090711736, /*hidden argument*/NULL);
			TimeZoneInfo_t4131446812 * L_4 = TimeZoneInfo_FindSystemTimeZoneByFileName_m3927988317(NULL /*static, unused*/, _stringLiteral73592651, L_3, /*hidden argument*/NULL);
			((TimeZoneInfo_t4131446812_StaticFields*)TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var->static_fields)->set_local_4(L_4);
			goto IL_0053;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0047;
			throw e;
		}

CATCH_0047:
		{ // begin catch(System.Object)
			{
				TimeZoneNotFoundException_t3613219068 * L_5 = (TimeZoneNotFoundException_t3613219068 *)il2cpp_codegen_object_new(TimeZoneNotFoundException_t3613219068_il2cpp_TypeInfo_var);
				TimeZoneNotFoundException__ctor_m636423683(L_5, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
			}

IL_004e:
			{
				goto IL_0053;
			}
		} // end catch (depth: 2)

IL_0053:
		{
			goto IL_0058;
		}
	} // end catch (depth: 1)

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_6 = ((TimeZoneInfo_t4131446812_StaticFields*)TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var->static_fields)->get_local_4();
		return L_6;
	}
}
// System.Boolean System.TimeZoneInfo::get_SupportsDaylightSavingTime()
extern "C"  bool TimeZoneInfo_get_SupportsDaylightSavingTime_m2782986697 (TimeZoneInfo_t4131446812 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_disableDaylightSavingTime_6();
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.TimeZoneInfo System.TimeZoneInfo::get_Utc()
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral84356;
extern const uint32_t TimeZoneInfo_get_Utc_m175699803_MetadataUsageId;
extern "C"  TimeZoneInfo_t4131446812 * TimeZoneInfo_get_Utc_m175699803 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_get_Utc_m175699803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_0 = ((TimeZoneInfo_t4131446812_StaticFields*)TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var->static_fields)->get_utc_7();
		if (L_0)
		{
			goto IL_002a;
		}
	}
	{
		TimeSpan_t763862892  L_1;
		memset(&L_1, 0, sizeof(L_1));
		TimeSpan__ctor_m477860848(&L_1, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_2 = TimeZoneInfo_CreateCustomTimeZone_m2111048456(NULL /*static, unused*/, _stringLiteral84356, L_1, _stringLiteral84356, _stringLiteral84356, /*hidden argument*/NULL);
		((TimeZoneInfo_t4131446812_StaticFields*)TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var->static_fields)->set_utc_7(L_2);
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_3 = ((TimeZoneInfo_t4131446812_StaticFields*)TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var->static_fields)->get_utc_7();
		return L_3;
	}
}
// System.String System.TimeZoneInfo::get_TimeZoneDirectory()
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral665232084;
extern const uint32_t TimeZoneInfo_get_TimeZoneDirectory_m3879840545_MetadataUsageId;
extern "C"  String_t* TimeZoneInfo_get_TimeZoneDirectory_m3879840545 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_get_TimeZoneDirectory_m3879840545_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		String_t* L_0 = ((TimeZoneInfo_t4131446812_StaticFields*)TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var->static_fields)->get_timeZoneDirectory_8();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		((TimeZoneInfo_t4131446812_StaticFields*)TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var->static_fields)->set_timeZoneDirectory_8(_stringLiteral665232084);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		String_t* L_1 = ((TimeZoneInfo_t4131446812_StaticFields*)TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var->static_fields)->get_timeZoneDirectory_8();
		return L_1;
	}
}
// System.TimeZoneInfo System.TimeZoneInfo::CreateCustomTimeZone(System.String,System.TimeSpan,System.String,System.String)
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern const uint32_t TimeZoneInfo_CreateCustomTimeZone_m2111048456_MetadataUsageId;
extern "C"  TimeZoneInfo_t4131446812 * TimeZoneInfo_CreateCustomTimeZone_m2111048456 (Il2CppObject * __this /* static, unused */, String_t* ___id, TimeSpan_t763862892  ___baseUtcOffset, String_t* ___displayName, String_t* ___standardDisplayName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_CreateCustomTimeZone_m2111048456_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___id;
		TimeSpan_t763862892  L_1 = ___baseUtcOffset;
		String_t* L_2 = ___displayName;
		String_t* L_3 = ___standardDisplayName;
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_4 = TimeZoneInfo_CreateCustomTimeZone_m1574743151(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (String_t*)NULL, (AdjustmentRuleU5BU5D_t1713960724*)(AdjustmentRuleU5BU5D_t1713960724*)NULL, (bool)1, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.TimeZoneInfo System.TimeZoneInfo::CreateCustomTimeZone(System.String,System.TimeSpan,System.String,System.String,System.String,System.TimeZoneInfo/AdjustmentRule[])
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern const uint32_t TimeZoneInfo_CreateCustomTimeZone_m2240749902_MetadataUsageId;
extern "C"  TimeZoneInfo_t4131446812 * TimeZoneInfo_CreateCustomTimeZone_m2240749902 (Il2CppObject * __this /* static, unused */, String_t* ___id, TimeSpan_t763862892  ___baseUtcOffset, String_t* ___displayName, String_t* ___standardDisplayName, String_t* ___daylightDisplayName, AdjustmentRuleU5BU5D_t1713960724* ___adjustmentRules, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_CreateCustomTimeZone_m2240749902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___id;
		TimeSpan_t763862892  L_1 = ___baseUtcOffset;
		String_t* L_2 = ___displayName;
		String_t* L_3 = ___standardDisplayName;
		String_t* L_4 = ___daylightDisplayName;
		AdjustmentRuleU5BU5D_t1713960724* L_5 = ___adjustmentRules;
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_6 = TimeZoneInfo_CreateCustomTimeZone_m1574743151(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (bool)0, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.TimeZoneInfo System.TimeZoneInfo::CreateCustomTimeZone(System.String,System.TimeSpan,System.String,System.String,System.String,System.TimeZoneInfo/AdjustmentRule[],System.Boolean)
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern const uint32_t TimeZoneInfo_CreateCustomTimeZone_m1574743151_MetadataUsageId;
extern "C"  TimeZoneInfo_t4131446812 * TimeZoneInfo_CreateCustomTimeZone_m1574743151 (Il2CppObject * __this /* static, unused */, String_t* ___id, TimeSpan_t763862892  ___baseUtcOffset, String_t* ___displayName, String_t* ___standardDisplayName, String_t* ___daylightDisplayName, AdjustmentRuleU5BU5D_t1713960724* ___adjustmentRules, bool ___disableDaylightSavingTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_CreateCustomTimeZone_m1574743151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___id;
		TimeSpan_t763862892  L_1 = ___baseUtcOffset;
		String_t* L_2 = ___displayName;
		String_t* L_3 = ___standardDisplayName;
		String_t* L_4 = ___daylightDisplayName;
		AdjustmentRuleU5BU5D_t1713960724* L_5 = ___adjustmentRules;
		bool L_6 = ___disableDaylightSavingTime;
		TimeZoneInfo_t4131446812 * L_7 = (TimeZoneInfo_t4131446812 *)il2cpp_codegen_object_new(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo__ctor_m1869617058(L_7, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Boolean System.TimeZoneInfo::Equals(System.TimeZoneInfo)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TimeZoneInfo_Equals_m2011180050_MetadataUsageId;
extern "C"  bool TimeZoneInfo_Equals_m2011180050 (TimeZoneInfo_t4131446812 * __this, TimeZoneInfo_t4131446812 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_Equals_m2011180050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		TimeZoneInfo_t4131446812 * L_0 = ___other;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		TimeZoneInfo_t4131446812 * L_1 = ___other;
		NullCheck(L_1);
		String_t* L_2 = TimeZoneInfo_get_Id_m4206484496(L_1, /*hidden argument*/NULL);
		String_t* L_3 = TimeZoneInfo_get_Id_m4206484496(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0027;
		}
	}
	{
		TimeZoneInfo_t4131446812 * L_5 = ___other;
		bool L_6 = TimeZoneInfo_HasSameRules_m2815393690(__this, L_5, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_6));
		goto IL_0028;
	}

IL_0027:
	{
		G_B5_0 = 0;
	}

IL_0028:
	{
		return (bool)G_B5_0;
	}
}
// System.TimeZoneInfo System.TimeZoneInfo::FindSystemTimeZoneByFileName(System.String,System.String)
extern TypeInfo* TimeZoneNotFoundException_t3613219068_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1895030908;
extern const uint32_t TimeZoneInfo_FindSystemTimeZoneByFileName_m3927988317_MetadataUsageId;
extern "C"  TimeZoneInfo_t4131446812 * TimeZoneInfo_FindSystemTimeZoneByFileName_m3927988317 (Il2CppObject * __this /* static, unused */, String_t* ___id, String_t* ___filepath, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_FindSystemTimeZoneByFileName_m3927988317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t58506160* V_0 = NULL;
	int32_t V_1 = 0;
	FileStream_t1527309539 * V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	TimeZoneInfo_t4131446812 * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___filepath;
		bool L_1 = File_Exists_m1326262381(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		TimeZoneNotFoundException_t3613219068 * L_2 = (TimeZoneNotFoundException_t3613219068 *)il2cpp_codegen_object_new(TimeZoneNotFoundException_t3613219068_il2cpp_TypeInfo_var);
		TimeZoneNotFoundException__ctor_m636423683(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0011:
	{
		V_0 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16384)));
		String_t* L_3 = ___filepath;
		FileStream_t1527309539 * L_4 = File_OpenRead_m3104031109(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
	}

IL_0023:
	try
	{ // begin try (depth: 1)
		FileStream_t1527309539 * L_5 = V_2;
		ByteU5BU5D_t58506160* L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(16 /* System.Int32 System.IO.FileStream::Read(System.Byte[],System.Int32,System.Int32) */, L_5, L_6, 0, ((int32_t)16384));
		V_1 = L_7;
		IL2CPP_LEAVE(0x43, FINALLY_0036);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		{
			FileStream_t1527309539 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_0042;
			}
		}

IL_003c:
		{
			FileStream_t1527309539 * L_9 = V_2;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_9);
		}

IL_0042:
		{
			IL2CPP_END_FINALLY(54)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		ByteU5BU5D_t58506160* L_10 = V_0;
		int32_t L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		bool L_12 = TimeZoneInfo_ValidTZFile_m3794122419(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_005a;
		}
	}
	{
		InvalidTimeZoneException_t3180604084 * L_13 = (InvalidTimeZoneException_t3180604084 *)il2cpp_codegen_object_new(InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var);
		InvalidTimeZoneException__ctor_m707317189(L_13, _stringLiteral1895030908, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_14 = ___id;
			ByteU5BU5D_t58506160* L_15 = V_0;
			int32_t L_16 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
			TimeZoneInfo_t4131446812 * L_17 = TimeZoneInfo_ParseTZBuffer_m3136713963(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
			V_4 = L_17;
			goto IL_0080;
		}

IL_0069:
		{
			; // IL_0069: leave IL_0080
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_006e;
		throw e;
	}

CATCH_006e:
	{ // begin catch(System.Exception)
		{
			V_3 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_18 = V_3;
			NullCheck(L_18);
			String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_18);
			InvalidTimeZoneException_t3180604084 * L_20 = (InvalidTimeZoneException_t3180604084 *)il2cpp_codegen_object_new(InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var);
			InvalidTimeZoneException__ctor_m707317189(L_20, L_19, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
		}

IL_007b:
		{
			goto IL_0080;
		}
	} // end catch (depth: 1)

IL_0080:
	{
		TimeZoneInfo_t4131446812 * L_21 = V_4;
		return L_21;
	}
}
// System.TimeZoneInfo/AdjustmentRule[] System.TimeZoneInfo::GetAdjustmentRules()
extern TypeInfo* AdjustmentRuleU5BU5D_t1713960724_il2cpp_TypeInfo_var;
extern const uint32_t TimeZoneInfo_GetAdjustmentRules_m1143142906_MetadataUsageId;
extern "C"  AdjustmentRuleU5BU5D_t1713960724* TimeZoneInfo_GetAdjustmentRules_m1143142906 (TimeZoneInfo_t4131446812 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_GetAdjustmentRules_m1143142906_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_disableDaylightSavingTime_6();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		return ((AdjustmentRuleU5BU5D_t1713960724*)SZArrayNew(AdjustmentRuleU5BU5D_t1713960724_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0012:
	{
		AdjustmentRuleU5BU5D_t1713960724* L_1 = __this->get_adjustmentRules_9();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_1);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(5 /* System.Object System.Array::Clone() */, (Il2CppArray *)(Il2CppArray *)L_1);
		return ((AdjustmentRuleU5BU5D_t1713960724*)Castclass(L_2, AdjustmentRuleU5BU5D_t1713960724_il2cpp_TypeInfo_var));
	}
}
// System.Int32 System.TimeZoneInfo::GetHashCode()
extern "C"  int32_t TimeZoneInfo_GetHashCode_m3350099154 (TimeZoneInfo_t4131446812 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	AdjustmentRule_t2290566697 * V_1 = NULL;
	AdjustmentRuleU5BU5D_t1713960724* V_2 = NULL;
	int32_t V_3 = 0;
	{
		String_t* L_0 = TimeZoneInfo_get_Id_m4206484496(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m471729487(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		AdjustmentRuleU5BU5D_t1713960724* L_2 = TimeZoneInfo_GetAdjustmentRules_m1143142906(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		V_3 = 0;
		goto IL_002b;
	}

IL_001a:
	{
		AdjustmentRuleU5BU5D_t1713960724* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
		int32_t L_6 = V_0;
		AdjustmentRule_t2290566697 * L_7 = V_1;
		NullCheck(L_7);
		int32_t L_8 = AdjustmentRule_GetHashCode_m3596128762(L_7, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_6^(int32_t)L_8));
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_10 = V_3;
		AdjustmentRuleU5BU5D_t1713960724* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Void System.TimeZoneInfo::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t TimeZoneInfo_GetObjectData_m813942419_MetadataUsageId;
extern "C"  void TimeZoneInfo_GetObjectData_m813942419 (TimeZoneInfo_t4131446812 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_GetObjectData_m813942419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.TimeSpan System.TimeZoneInfo::GetUtcOffset(System.DateTime)
extern TypeInfo* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t TimeZoneInfo_GetUtcOffset_m2479866789_MetadataUsageId;
extern "C"  TimeSpan_t763862892  TimeZoneInfo_GetUtcOffset_m2479866789 (TimeZoneInfo_t4131446812 * __this, DateTime_t339033936  ___dateTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_GetUtcOffset_m2479866789_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AdjustmentRule_t2290566697 * V_0 = NULL;
	{
		DateTime_t339033936  L_0 = ___dateTime;
		bool L_1 = TimeZoneInfo_IsDaylightSavingTime_m1555557330(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		DateTime_t339033936  L_2 = ___dateTime;
		AdjustmentRule_t2290566697 * L_3 = TimeZoneInfo_GetApplicableRule_m2789838025(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		TimeSpan_t763862892  L_4 = TimeZoneInfo_get_BaseUtcOffset_m418231789(__this, /*hidden argument*/NULL);
		AdjustmentRule_t2290566697 * L_5 = V_0;
		NullCheck(L_5);
		TimeSpan_t763862892  L_6 = AdjustmentRule_get_DaylightDelta_m3833226735(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_7 = TimeSpan_op_Addition_m141072959(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0026:
	{
		TimeSpan_t763862892  L_8 = TimeZoneInfo_get_BaseUtcOffset_m418231789(__this, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean System.TimeZoneInfo::HasSameRules(System.TimeZoneInfo)
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106069776;
extern const uint32_t TimeZoneInfo_HasSameRules_m2815393690_MetadataUsageId;
extern "C"  bool TimeZoneInfo_HasSameRules_m2815393690 (TimeZoneInfo_t4131446812 * __this, TimeZoneInfo_t4131446812 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_HasSameRules_m2815393690_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		TimeZoneInfo_t4131446812 * L_0 = ___other;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral106069776, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		AdjustmentRuleU5BU5D_t1713960724* L_2 = __this->get_adjustmentRules_9();
		TimeZoneInfo_t4131446812 * L_3 = ___other;
		NullCheck(L_3);
		AdjustmentRuleU5BU5D_t1713960724* L_4 = L_3->get_adjustmentRules_9();
		if ((((int32_t)((((Il2CppObject*)(AdjustmentRuleU5BU5D_t1713960724*)L_2) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)((((Il2CppObject*)(AdjustmentRuleU5BU5D_t1713960724*)L_4) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0))))
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		AdjustmentRuleU5BU5D_t1713960724* L_5 = __this->get_adjustmentRules_9();
		if (L_5)
		{
			goto IL_0037;
		}
	}
	{
		return (bool)1;
	}

IL_0037:
	{
		TimeSpan_t763862892  L_6 = TimeZoneInfo_get_BaseUtcOffset_m418231789(__this, /*hidden argument*/NULL);
		TimeZoneInfo_t4131446812 * L_7 = ___other;
		NullCheck(L_7);
		TimeSpan_t763862892  L_8 = TimeZoneInfo_get_BaseUtcOffset_m418231789(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_9 = TimeSpan_op_Inequality_m2184437271(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004f;
		}
	}
	{
		return (bool)0;
	}

IL_004f:
	{
		AdjustmentRuleU5BU5D_t1713960724* L_10 = __this->get_adjustmentRules_9();
		NullCheck(L_10);
		TimeZoneInfo_t4131446812 * L_11 = ___other;
		NullCheck(L_11);
		AdjustmentRuleU5BU5D_t1713960724* L_12 = L_11->get_adjustmentRules_9();
		NullCheck(L_12);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0066;
		}
	}
	{
		return (bool)0;
	}

IL_0066:
	{
		V_0 = 0;
		goto IL_008d;
	}

IL_006d:
	{
		AdjustmentRuleU5BU5D_t1713960724* L_13 = __this->get_adjustmentRules_9();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		TimeZoneInfo_t4131446812 * L_16 = ___other;
		NullCheck(L_16);
		AdjustmentRuleU5BU5D_t1713960724* L_17 = L_16->get_adjustmentRules_9();
		int32_t L_18 = V_0;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		NullCheck(((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15))));
		bool L_20 = AdjustmentRule_Equals_m235226988(((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15))), ((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19))), /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0089;
		}
	}
	{
		return (bool)0;
	}

IL_0089:
	{
		int32_t L_21 = V_0;
		V_0 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_008d:
	{
		int32_t L_22 = V_0;
		AdjustmentRuleU5BU5D_t1713960724* L_23 = __this->get_adjustmentRules_9();
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
		{
			goto IL_006d;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean System.TimeZoneInfo::IsDaylightSavingTime(System.DateTime)
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4162367038;
extern const uint32_t TimeZoneInfo_IsDaylightSavingTime_m1555557330_MetadataUsageId;
extern "C"  bool TimeZoneInfo_IsDaylightSavingTime_m1555557330 (TimeZoneInfo_t4131446812 * __this, DateTime_t339033936  ___dateTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_IsDaylightSavingTime_m1555557330_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AdjustmentRule_t2290566697 * V_0 = NULL;
	DateTime_t339033936  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DateTime_t339033936  V_2;
	memset(&V_2, 0, sizeof(V_2));
	TransitionTime_t1289309666  V_3;
	memset(&V_3, 0, sizeof(V_3));
	TransitionTime_t1289309666  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B18_0 = 0;
	TransitionTime_t1289309666  G_B18_1;
	memset(&G_B18_1, 0, sizeof(G_B18_1));
	int32_t G_B17_0 = 0;
	TransitionTime_t1289309666  G_B17_1;
	memset(&G_B17_1, 0, sizeof(G_B17_1));
	int32_t G_B19_0 = 0;
	int32_t G_B19_1 = 0;
	TransitionTime_t1289309666  G_B19_2;
	memset(&G_B19_2, 0, sizeof(G_B19_2));
	int32_t G_B24_0 = 0;
	{
		int32_t L_0 = DateTime_get_Kind_m3496013602((&___dateTime), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0024;
		}
	}
	{
		DateTime_t339033936  L_1 = ___dateTime;
		bool L_2 = TimeZoneInfo_IsInvalidTime_m3622113175(__this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, _stringLiteral4162367038, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_4 = TimeZoneInfo_get_Utc_m175699803(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(TimeZoneInfo_t4131446812 *)__this) == ((Il2CppObject*)(TimeZoneInfo_t4131446812 *)L_4))))
		{
			goto IL_0031;
		}
	}
	{
		return (bool)0;
	}

IL_0031:
	{
		bool L_5 = TimeZoneInfo_get_SupportsDaylightSavingTime_m2782986697(__this, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003e;
		}
	}
	{
		return (bool)0;
	}

IL_003e:
	{
		int32_t L_6 = DateTime_get_Kind_m3496013602((&___dateTime), /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)2)))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_7 = DateTime_get_Kind_m3496013602((&___dateTime), /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_006a;
		}
	}

IL_0057:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_8 = TimeZoneInfo_get_Local_m1804805602(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(TimeZoneInfo_t4131446812 *)__this) == ((Il2CppObject*)(TimeZoneInfo_t4131446812 *)L_8))))
		{
			goto IL_006a;
		}
	}
	{
		bool L_9 = DateTime_IsDaylightSavingTime_m3751830931((&___dateTime), /*hidden argument*/NULL);
		return L_9;
	}

IL_006a:
	{
		int32_t L_10 = DateTime_get_Kind_m3496013602((&___dateTime), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_0096;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_11 = TimeZoneInfo_get_Utc_m175699803(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Il2CppObject*)(TimeZoneInfo_t4131446812 *)__this) == ((Il2CppObject*)(TimeZoneInfo_t4131446812 *)L_11)))
		{
			goto IL_0096;
		}
	}
	{
		DateTime_t339033936  L_12 = DateTime_ToUniversalTime_m691668206((&___dateTime), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_13 = DateTime_SpecifyKind_m2123544880(NULL /*static, unused*/, L_12, 1, /*hidden argument*/NULL);
		bool L_14 = TimeZoneInfo_IsDaylightSavingTime_m1555557330(__this, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0096:
	{
		DateTime_t339033936  L_15 = DateTime_get_Date_m42247688((&___dateTime), /*hidden argument*/NULL);
		AdjustmentRule_t2290566697 * L_16 = TimeZoneInfo_GetApplicableRule_m2789838025(__this, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		AdjustmentRule_t2290566697 * L_17 = V_0;
		if (L_17)
		{
			goto IL_00ac;
		}
	}
	{
		return (bool)0;
	}

IL_00ac:
	{
		AdjustmentRule_t2290566697 * L_18 = V_0;
		NullCheck(L_18);
		TransitionTime_t1289309666  L_19 = AdjustmentRule_get_DaylightTransitionStart_m3102376943(L_18, /*hidden argument*/NULL);
		int32_t L_20 = DateTime_get_Year_m2333942068((&___dateTime), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_21 = TimeZoneInfo_TransitionPoint_m229137691(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		V_1 = L_21;
		AdjustmentRule_t2290566697 * L_22 = V_0;
		NullCheck(L_22);
		TransitionTime_t1289309666  L_23 = AdjustmentRule_get_DaylightTransitionEnd_m1089560872(L_22, /*hidden argument*/NULL);
		int32_t L_24 = DateTime_get_Year_m2333942068((&___dateTime), /*hidden argument*/NULL);
		AdjustmentRule_t2290566697 * L_25 = V_0;
		NullCheck(L_25);
		TransitionTime_t1289309666  L_26 = AdjustmentRule_get_DaylightTransitionStart_m3102376943(L_25, /*hidden argument*/NULL);
		V_3 = L_26;
		int32_t L_27 = TransitionTime_get_Month_m2331873831((&V_3), /*hidden argument*/NULL);
		AdjustmentRule_t2290566697 * L_28 = V_0;
		NullCheck(L_28);
		TransitionTime_t1289309666  L_29 = AdjustmentRule_get_DaylightTransitionEnd_m1089560872(L_28, /*hidden argument*/NULL);
		V_4 = L_29;
		int32_t L_30 = TransitionTime_get_Month_m2331873831((&V_4), /*hidden argument*/NULL);
		G_B17_0 = L_24;
		G_B17_1 = L_23;
		if ((((int32_t)L_27) >= ((int32_t)L_30)))
		{
			G_B18_0 = L_24;
			G_B18_1 = L_23;
			goto IL_00f4;
		}
	}
	{
		G_B19_0 = 0;
		G_B19_1 = G_B17_0;
		G_B19_2 = G_B17_1;
		goto IL_00f5;
	}

IL_00f4:
	{
		G_B19_0 = 1;
		G_B19_1 = G_B18_0;
		G_B19_2 = G_B18_1;
	}

IL_00f5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_31 = TimeZoneInfo_TransitionPoint_m229137691(NULL /*static, unused*/, G_B19_2, ((int32_t)((int32_t)G_B19_1+(int32_t)G_B19_0)), /*hidden argument*/NULL);
		V_2 = L_31;
		int32_t L_32 = DateTime_get_Kind_m3496013602((&___dateTime), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_32) == ((uint32_t)1))))
		{
			goto IL_012e;
		}
	}
	{
		DateTime_t339033936  L_33 = V_1;
		TimeSpan_t763862892  L_34 = TimeZoneInfo_get_BaseUtcOffset_m418231789(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_35 = DateTime_op_Subtraction_m3609021319(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		V_1 = L_35;
		DateTime_t339033936  L_36 = V_2;
		TimeSpan_t763862892  L_37 = TimeZoneInfo_get_BaseUtcOffset_m418231789(__this, /*hidden argument*/NULL);
		AdjustmentRule_t2290566697 * L_38 = V_0;
		NullCheck(L_38);
		TimeSpan_t763862892  L_39 = AdjustmentRule_get_DaylightDelta_m3833226735(L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_40 = TimeSpan_op_Addition_m141072959(NULL /*static, unused*/, L_37, L_39, /*hidden argument*/NULL);
		DateTime_t339033936  L_41 = DateTime_op_Subtraction_m3609021319(NULL /*static, unused*/, L_36, L_40, /*hidden argument*/NULL);
		V_2 = L_41;
	}

IL_012e:
	{
		DateTime_t339033936  L_42 = ___dateTime;
		DateTime_t339033936  L_43 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_44 = DateTime_op_GreaterThanOrEqual_m717787228(NULL /*static, unused*/, L_42, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0143;
		}
	}
	{
		DateTime_t339033936  L_45 = ___dateTime;
		DateTime_t339033936  L_46 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_47 = DateTime_op_LessThan_m35073816(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		G_B24_0 = ((int32_t)(L_47));
		goto IL_0144;
	}

IL_0143:
	{
		G_B24_0 = 0;
	}

IL_0144:
	{
		return (bool)G_B24_0;
	}
}
// System.Boolean System.TimeZoneInfo::IsInvalidTime(System.DateTime)
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t TimeZoneInfo_IsInvalidTime_m3622113175_MetadataUsageId;
extern "C"  bool TimeZoneInfo_IsInvalidTime_m3622113175 (TimeZoneInfo_t4131446812 * __this, DateTime_t339033936  ___dateTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_IsInvalidTime_m3622113175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AdjustmentRule_t2290566697 * V_0 = NULL;
	DateTime_t339033936  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = DateTime_get_Kind_m3496013602((&___dateTime), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		int32_t L_1 = DateTime_get_Kind_m3496013602((&___dateTime), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_2 = TimeZoneInfo_get_Local_m1804805602(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Il2CppObject*)(TimeZoneInfo_t4131446812 *)__this) == ((Il2CppObject*)(TimeZoneInfo_t4131446812 *)L_2)))
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		DateTime_t339033936  L_3 = ___dateTime;
		AdjustmentRule_t2290566697 * L_4 = TimeZoneInfo_GetApplicableRule_m2789838025(__this, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		AdjustmentRule_t2290566697 * L_5 = V_0;
		NullCheck(L_5);
		TransitionTime_t1289309666  L_6 = AdjustmentRule_get_DaylightTransitionStart_m3102376943(L_5, /*hidden argument*/NULL);
		int32_t L_7 = DateTime_get_Year_m2333942068((&___dateTime), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_8 = TimeZoneInfo_TransitionPoint_m229137691(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		DateTime_t339033936  L_9 = ___dateTime;
		DateTime_t339033936  L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_11 = DateTime_op_GreaterThanOrEqual_m717787228(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0069;
		}
	}
	{
		DateTime_t339033936  L_12 = ___dateTime;
		DateTime_t339033936  L_13 = V_1;
		AdjustmentRule_t2290566697 * L_14 = V_0;
		NullCheck(L_14);
		TimeSpan_t763862892  L_15 = AdjustmentRule_get_DaylightDelta_m3833226735(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_16 = DateTime_op_Addition_m4061583523(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		bool L_17 = DateTime_op_LessThan_m35073816(NULL /*static, unused*/, L_12, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0069;
		}
	}
	{
		return (bool)1;
	}

IL_0069:
	{
		return (bool)0;
	}
}
// System.Void System.TimeZoneInfo::OnDeserialization(System.Object)
extern TypeInfo* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t TimeZoneInfo_OnDeserialization_m3643994545_MetadataUsageId;
extern "C"  void TimeZoneInfo_OnDeserialization_m3643994545 (TimeZoneInfo_t4131446812 * __this, Il2CppObject * ___sender, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_OnDeserialization_m3643994545_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.String System.TimeZoneInfo::ToString()
extern "C"  String_t* TimeZoneInfo_ToString_m371589912 (TimeZoneInfo_t4131446812 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = TimeZoneInfo_get_DisplayName_m3106101754(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.TimeZoneInfo/AdjustmentRule System.TimeZoneInfo::GetApplicableRule(System.DateTime)
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t TimeZoneInfo_GetApplicableRule_m2789838025_MetadataUsageId;
extern "C"  AdjustmentRule_t2290566697 * TimeZoneInfo_GetApplicableRule_m2789838025 (TimeZoneInfo_t4131446812 * __this, DateTime_t339033936  ___dateTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_GetApplicableRule_m2789838025_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AdjustmentRule_t2290566697 * V_1 = NULL;
	AdjustmentRuleU5BU5D_t1713960724* V_2 = NULL;
	int32_t V_3 = 0;
	{
		DateTime_t339033936  L_0 = ___dateTime;
		V_0 = L_0;
		int32_t L_1 = DateTime_get_Kind_m3496013602((&___dateTime), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_2 = TimeZoneInfo_get_Local_m1804805602(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Il2CppObject*)(TimeZoneInfo_t4131446812 *)__this) == ((Il2CppObject*)(TimeZoneInfo_t4131446812 *)L_2)))
		{
			goto IL_002d;
		}
	}
	{
		DateTime_t339033936  L_3 = DateTime_ToUniversalTime_m691668206((&V_0), /*hidden argument*/NULL);
		TimeSpan_t763862892  L_4 = TimeZoneInfo_get_BaseUtcOffset_m418231789(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_5 = DateTime_op_Addition_m4061583523(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_002d:
	{
		int32_t L_6 = DateTime_get_Kind_m3496013602((&___dateTime), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_7 = TimeZoneInfo_get_Utc_m175699803(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Il2CppObject*)(TimeZoneInfo_t4131446812 *)__this) == ((Il2CppObject*)(TimeZoneInfo_t4131446812 *)L_7)))
		{
			goto IL_0052;
		}
	}
	{
		DateTime_t339033936  L_8 = V_0;
		TimeSpan_t763862892  L_9 = TimeZoneInfo_get_BaseUtcOffset_m418231789(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_10 = DateTime_op_Addition_m4061583523(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
	}

IL_0052:
	{
		AdjustmentRuleU5BU5D_t1713960724* L_11 = __this->get_adjustmentRules_9();
		V_2 = L_11;
		V_3 = 0;
		goto IL_009f;
	}

IL_0060:
	{
		AdjustmentRuleU5BU5D_t1713960724* L_12 = V_2;
		int32_t L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_1 = ((L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14)));
		AdjustmentRule_t2290566697 * L_15 = V_1;
		NullCheck(L_15);
		DateTime_t339033936  L_16 = AdjustmentRule_get_DateStart_m2606242505(L_15, /*hidden argument*/NULL);
		DateTime_t339033936  L_17 = DateTime_get_Date_m42247688((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_18 = DateTime_op_GreaterThan_m3846016869(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_007d;
		}
	}
	{
		return (AdjustmentRule_t2290566697 *)NULL;
	}

IL_007d:
	{
		AdjustmentRule_t2290566697 * L_19 = V_1;
		NullCheck(L_19);
		DateTime_t339033936  L_20 = AdjustmentRule_get_DateEnd_m3953845890(L_19, /*hidden argument*/NULL);
		DateTime_t339033936  L_21 = DateTime_get_Date_m42247688((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_22 = DateTime_op_LessThan_m35073816(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0099;
		}
	}
	{
		goto IL_009b;
	}

IL_0099:
	{
		AdjustmentRule_t2290566697 * L_23 = V_1;
		return L_23;
	}

IL_009b:
	{
		int32_t L_24 = V_3;
		V_3 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009f:
	{
		int32_t L_25 = V_3;
		AdjustmentRuleU5BU5D_t1713960724* L_26 = V_2;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length)))))))
		{
			goto IL_0060;
		}
	}
	{
		return (AdjustmentRule_t2290566697 *)NULL;
	}
}
// System.DateTime System.TimeZoneInfo::TransitionPoint(System.TimeZoneInfo/TransitionTime,System.Int32)
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t TimeZoneInfo_TransitionPoint_m229137691_MetadataUsageId;
extern "C"  DateTime_t339033936  TimeZoneInfo_TransitionPoint_m229137691 (Il2CppObject * __this /* static, unused */, TransitionTime_t1289309666  ___transition, int32_t ___year, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_TransitionPoint_m229137691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	DateTime_t339033936  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DateTime_t339033936  V_3;
	memset(&V_3, 0, sizeof(V_3));
	DateTime_t339033936  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		bool L_0 = TransitionTime_get_IsFixedDateRule_m3223864213((&___transition), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_1 = ___year;
		int32_t L_2 = TransitionTime_get_Month_m2331873831((&___transition), /*hidden argument*/NULL);
		int32_t L_3 = TransitionTime_get_Day_m2862976451((&___transition), /*hidden argument*/NULL);
		DateTime_t339033936  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DateTime__ctor_m145640619(&L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		DateTime_t339033936  L_5 = TransitionTime_get_TimeOfDay_m3313234260((&___transition), /*hidden argument*/NULL);
		V_2 = L_5;
		TimeSpan_t763862892  L_6 = DateTime_get_TimeOfDay_m299590820((&V_2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_7 = DateTime_op_Addition_m4061583523(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0035:
	{
		int32_t L_8 = ___year;
		int32_t L_9 = TransitionTime_get_Month_m2331873831((&___transition), /*hidden argument*/NULL);
		DateTime__ctor_m145640619((&V_3), L_8, L_9, 1, /*hidden argument*/NULL);
		int32_t L_10 = DateTime_get_DayOfWeek_m1793620473((&V_3), /*hidden argument*/NULL);
		V_0 = L_10;
		int32_t L_11 = TransitionTime_get_Week_m3400040591((&___transition), /*hidden argument*/NULL);
		int32_t L_12 = TransitionTime_get_DayOfWeek_m2396869159((&___transition), /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)1+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_11-(int32_t)1))*(int32_t)7))))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_12-(int32_t)L_13))%(int32_t)7))));
		int32_t L_14 = V_1;
		int32_t L_15 = ___year;
		int32_t L_16 = TransitionTime_get_Month_m2331873831((&___transition), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		int32_t L_17 = DateTime_DaysInMonth_m1394183022(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_14) <= ((int32_t)L_17)))
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18-(int32_t)7));
	}

IL_007e:
	{
		int32_t L_19 = ___year;
		int32_t L_20 = TransitionTime_get_Month_m2331873831((&___transition), /*hidden argument*/NULL);
		int32_t L_21 = V_1;
		DateTime_t339033936  L_22;
		memset(&L_22, 0, sizeof(L_22));
		DateTime__ctor_m145640619(&L_22, L_19, L_20, L_21, /*hidden argument*/NULL);
		DateTime_t339033936  L_23 = TransitionTime_get_TimeOfDay_m3313234260((&___transition), /*hidden argument*/NULL);
		V_4 = L_23;
		TimeSpan_t763862892  L_24 = DateTime_get_TimeOfDay_m299590820((&V_4), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_25 = DateTime_op_Addition_m4061583523(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
		return L_25;
	}
}
// System.Boolean System.TimeZoneInfo::ValidTZFile(System.Byte[],System.Int32)
extern TypeInfo* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2592291;
extern const uint32_t TimeZoneInfo_ValidTZFile_m3794122419_MetadataUsageId;
extern "C"  bool TimeZoneInfo_ValidTZFile_m3794122419 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___buffer, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_ValidTZFile_m3794122419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_001c;
	}

IL_000d:
	{
		StringBuilder_t3822575854 * L_1 = V_0;
		ByteU5BU5D_t58506160* L_2 = ___buffer;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck(L_1);
		StringBuilder_Append_m2143093878(L_1, (((int32_t)((uint16_t)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)))))), /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001c:
	{
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) < ((int32_t)4)))
		{
			goto IL_000d;
		}
	}
	{
		StringBuilder_t3822575854 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = StringBuilder_ToString_m350379841(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_8, _stringLiteral2592291, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003a;
		}
	}
	{
		return (bool)0;
	}

IL_003a:
	{
		int32_t L_10 = ___length;
		if ((((int32_t)L_10) < ((int32_t)((int32_t)16384))))
		{
			goto IL_0047;
		}
	}
	{
		return (bool)0;
	}

IL_0047:
	{
		return (bool)1;
	}
}
// System.Int32 System.TimeZoneInfo::SwapInt32(System.Int32)
extern "C"  int32_t TimeZoneInfo_SwapInt32_m3601450189 (Il2CppObject * __this /* static, unused */, int32_t ___i, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		int32_t L_1 = ___i;
		int32_t L_2 = ___i;
		int32_t L_3 = ___i;
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0>>(int32_t)((int32_t)24)))&(int32_t)((int32_t)255)))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1>>(int32_t)8))&(int32_t)((int32_t)65280)))))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2<<(int32_t)8))&(int32_t)((int32_t)16711680)))))|(int32_t)((int32_t)((int32_t)L_3<<(int32_t)((int32_t)24)))));
	}
}
// System.Int32 System.TimeZoneInfo::ReadBigEndianInt32(System.Byte[],System.Int32)
extern TypeInfo* BitConverter_t3338308296_il2cpp_TypeInfo_var;
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern const uint32_t TimeZoneInfo_ReadBigEndianInt32_m2013217080_MetadataUsageId;
extern "C"  int32_t TimeZoneInfo_ReadBigEndianInt32_m2013217080 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___buffer, int32_t ___start, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_ReadBigEndianInt32_m2013217080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		ByteU5BU5D_t58506160* L_0 = ___buffer;
		int32_t L_1 = ___start;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3338308296_il2cpp_TypeInfo_var);
		int32_t L_2 = BitConverter_ToInt32_m510821053(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = ((BitConverter_t3338308296_StaticFields*)BitConverter_t3338308296_il2cpp_TypeInfo_var->static_fields)->get_IsLittleEndian_1();
		if (L_3)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_4 = V_0;
		return L_4;
	}

IL_0014:
	{
		int32_t L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		int32_t L_6 = TimeZoneInfo_SwapInt32_m3601450189(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.TimeZoneInfo System.TimeZoneInfo::ParseTZBuffer(System.String,System.Byte[],System.Int32)
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t3087525666_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3593011277_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1163845744_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1879345057_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1740619125_MethodInfo_var;
extern const uint32_t TimeZoneInfo_ParseTZBuffer_m3136713963_MetadataUsageId;
extern "C"  TimeZoneInfo_t4131446812 * TimeZoneInfo_ParseTZBuffer_m3136713963 (Il2CppObject * __this /* static, unused */, String_t* ___id, ByteU5BU5D_t58506160* ___buffer, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_ParseTZBuffer_m3136713963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Dictionary_2_t1459299685 * V_6 = NULL;
	Dictionary_2_t2773072230 * V_7 = NULL;
	List_1_t3660159136 * V_8 = NULL;
	TimeSpan_t763862892  V_9;
	memset(&V_9, 0, sizeof(V_9));
	TimeSpan_t763862892  V_10;
	memset(&V_10, 0, sizeof(V_10));
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	bool V_13 = false;
	DateTime_t339033936  V_14;
	memset(&V_14, 0, sizeof(V_14));
	List_1_t3087525666 * V_15 = NULL;
	int32_t V_16 = 0;
	KeyValuePair_2_t2863200167  V_17;
	memset(&V_17, 0, sizeof(V_17));
	DateTime_t339033936  V_18;
	memset(&V_18, 0, sizeof(V_18));
	TimeType_t2282261447  V_19;
	memset(&V_19, 0, sizeof(V_19));
	DateTime_t339033936  V_20;
	memset(&V_20, 0, sizeof(V_20));
	DateTime_t339033936  V_21;
	memset(&V_21, 0, sizeof(V_21));
	DateTime_t339033936  V_22;
	memset(&V_22, 0, sizeof(V_22));
	TransitionTime_t1289309666  V_23;
	memset(&V_23, 0, sizeof(V_23));
	TransitionTime_t1289309666  V_24;
	memset(&V_24, 0, sizeof(V_24));
	TimeType_t2282261447  V_25;
	memset(&V_25, 0, sizeof(V_25));
	TimeType_t2282261447  V_26;
	memset(&V_26, 0, sizeof(V_26));
	{
		ByteU5BU5D_t58506160* L_0 = ___buffer;
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		int32_t L_1 = TimeZoneInfo_ReadBigEndianInt32_m2013217080(NULL /*static, unused*/, L_0, ((int32_t)20), /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t58506160* L_2 = ___buffer;
		int32_t L_3 = TimeZoneInfo_ReadBigEndianInt32_m2013217080(NULL /*static, unused*/, L_2, ((int32_t)24), /*hidden argument*/NULL);
		V_1 = L_3;
		ByteU5BU5D_t58506160* L_4 = ___buffer;
		int32_t L_5 = TimeZoneInfo_ReadBigEndianInt32_m2013217080(NULL /*static, unused*/, L_4, ((int32_t)28), /*hidden argument*/NULL);
		V_2 = L_5;
		ByteU5BU5D_t58506160* L_6 = ___buffer;
		int32_t L_7 = TimeZoneInfo_ReadBigEndianInt32_m2013217080(NULL /*static, unused*/, L_6, ((int32_t)32), /*hidden argument*/NULL);
		V_3 = L_7;
		ByteU5BU5D_t58506160* L_8 = ___buffer;
		int32_t L_9 = TimeZoneInfo_ReadBigEndianInt32_m2013217080(NULL /*static, unused*/, L_8, ((int32_t)36), /*hidden argument*/NULL);
		V_4 = L_9;
		ByteU5BU5D_t58506160* L_10 = ___buffer;
		int32_t L_11 = TimeZoneInfo_ReadBigEndianInt32_m2013217080(NULL /*static, unused*/, L_10, ((int32_t)40), /*hidden argument*/NULL);
		V_5 = L_11;
		int32_t L_12 = ___length;
		int32_t L_13 = V_3;
		int32_t L_14 = V_4;
		int32_t L_15 = V_5;
		int32_t L_16 = V_2;
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_12) >= ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)44)+(int32_t)((int32_t)((int32_t)L_13*(int32_t)5))))+(int32_t)((int32_t)((int32_t)L_14*(int32_t)6))))+(int32_t)L_15))+(int32_t)((int32_t)((int32_t)L_16*(int32_t)8))))+(int32_t)L_17))+(int32_t)L_18)))))
		{
			goto IL_005a;
		}
	}
	{
		InvalidTimeZoneException_t3180604084 * L_19 = (InvalidTimeZoneException_t3180604084 *)il2cpp_codegen_object_new(InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var);
		InvalidTimeZoneException__ctor_m377819229(L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_005a:
	{
		ByteU5BU5D_t58506160* L_20 = ___buffer;
		int32_t L_21 = V_3;
		int32_t L_22 = V_3;
		int32_t L_23 = V_4;
		int32_t L_24 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		Dictionary_2_t1459299685 * L_25 = TimeZoneInfo_ParseAbbreviations_m883393013(NULL /*static, unused*/, L_20, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)44)+(int32_t)((int32_t)((int32_t)4*(int32_t)L_21))))+(int32_t)L_22))+(int32_t)((int32_t)((int32_t)6*(int32_t)L_23)))), L_24, /*hidden argument*/NULL);
		V_6 = L_25;
		ByteU5BU5D_t58506160* L_26 = ___buffer;
		int32_t L_27 = V_3;
		int32_t L_28 = V_3;
		int32_t L_29 = V_4;
		Dictionary_2_t1459299685 * L_30 = V_6;
		Dictionary_2_t2773072230 * L_31 = TimeZoneInfo_ParseTimesTypes_m298446159(NULL /*static, unused*/, L_26, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)44)+(int32_t)((int32_t)((int32_t)4*(int32_t)L_27))))+(int32_t)L_28)), L_29, L_30, /*hidden argument*/NULL);
		V_7 = L_31;
		ByteU5BU5D_t58506160* L_32 = ___buffer;
		int32_t L_33 = V_3;
		Dictionary_2_t2773072230 * L_34 = V_7;
		List_1_t3660159136 * L_35 = TimeZoneInfo_ParseTransitions_m2742103091(NULL /*static, unused*/, L_32, ((int32_t)44), L_33, L_34, /*hidden argument*/NULL);
		V_8 = L_35;
		Dictionary_2_t2773072230 * L_36 = V_7;
		NullCheck(L_36);
		int32_t L_37 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.TimeZoneInfo/TimeType>::get_Count() */, L_36);
		if (L_37)
		{
			goto IL_00a4;
		}
	}
	{
		InvalidTimeZoneException_t3180604084 * L_38 = (InvalidTimeZoneException_t3180604084 *)il2cpp_codegen_object_new(InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var);
		InvalidTimeZoneException__ctor_m377819229(L_38, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_38);
	}

IL_00a4:
	{
		Dictionary_2_t2773072230 * L_39 = V_7;
		NullCheck(L_39);
		int32_t L_40 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.TimeZoneInfo/TimeType>::get_Count() */, L_39);
		if ((!(((uint32_t)L_40) == ((uint32_t)1))))
		{
			goto IL_00cd;
		}
	}
	{
		Dictionary_2_t2773072230 * L_41 = V_7;
		NullCheck(L_41);
		TimeType_t2282261447  L_42 = VirtFuncInvoker1< TimeType_t2282261447 , int32_t >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,System.TimeZoneInfo/TimeType>::get_Item(!0) */, L_41, 0);
		V_26 = L_42;
		bool L_43 = (&V_26)->get_IsDst_1();
		if (!L_43)
		{
			goto IL_00cd;
		}
	}
	{
		InvalidTimeZoneException_t3180604084 * L_44 = (InvalidTimeZoneException_t3180604084 *)il2cpp_codegen_object_new(InvalidTimeZoneException_t3180604084_il2cpp_TypeInfo_var);
		InvalidTimeZoneException__ctor_m377819229(L_44, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_44);
	}

IL_00cd:
	{
		TimeSpan__ctor_m477860848((&V_9), (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		TimeSpan__ctor_m477860848((&V_10), (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		V_11 = (String_t*)NULL;
		V_12 = (String_t*)NULL;
		V_13 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_45 = ((DateTime_t339033936_StaticFields*)DateTime_t339033936_il2cpp_TypeInfo_var->static_fields)->get_MinValue_13();
		V_14 = L_45;
		List_1_t3087525666 * L_46 = (List_1_t3087525666 *)il2cpp_codegen_object_new(List_1_t3087525666_il2cpp_TypeInfo_var);
		List_1__ctor_m3593011277(L_46, /*hidden argument*/List_1__ctor_m3593011277_MethodInfo_var);
		V_15 = L_46;
		V_16 = 0;
		goto IL_031d;
	}

IL_00fe:
	{
		List_1_t3660159136 * L_47 = V_8;
		int32_t L_48 = V_16;
		NullCheck(L_47);
		KeyValuePair_2_t2863200167  L_49 = VirtFuncInvoker1< KeyValuePair_2_t2863200167 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::get_Item(System.Int32) */, L_47, L_48);
		V_17 = L_49;
		DateTime_t339033936  L_50 = KeyValuePair_2_get_Key_m1163845744((&V_17), /*hidden argument*/KeyValuePair_2_get_Key_m1163845744_MethodInfo_var);
		V_18 = L_50;
		TimeType_t2282261447  L_51 = KeyValuePair_2_get_Value_m1879345057((&V_17), /*hidden argument*/KeyValuePair_2_get_Value_m1879345057_MethodInfo_var);
		V_19 = L_51;
		bool L_52 = (&V_19)->get_IsDst_1();
		if (L_52)
		{
			goto IL_02c1;
		}
	}
	{
		String_t* L_53 = V_11;
		String_t* L_54 = (&V_19)->get_Name_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_55 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		if (L_55)
		{
			goto IL_014e;
		}
	}
	{
		double L_56 = TimeSpan_get_TotalSeconds_m3163750087((&V_9), /*hidden argument*/NULL);
		int32_t L_57 = (&V_19)->get_Offset_0();
		if ((((double)L_56) == ((double)(((double)((double)L_57))))))
		{
			goto IL_0174;
		}
	}

IL_014e:
	{
		String_t* L_58 = (&V_19)->get_Name_2();
		V_11 = L_58;
		V_12 = (String_t*)NULL;
		int32_t L_59 = (&V_19)->get_Offset_0();
		TimeSpan__ctor_m4160332047((&V_9), 0, 0, L_59, /*hidden argument*/NULL);
		List_1_t3087525666 * L_60 = (List_1_t3087525666 *)il2cpp_codegen_object_new(List_1_t3087525666_il2cpp_TypeInfo_var);
		List_1__ctor_m3593011277(L_60, /*hidden argument*/List_1__ctor_m3593011277_MethodInfo_var);
		V_15 = L_60;
		V_13 = (bool)0;
	}

IL_0174:
	{
		bool L_61 = V_13;
		if (!L_61)
		{
			goto IL_02b9;
		}
	}
	{
		DateTime_t339033936  L_62 = V_14;
		TimeSpan_t763862892  L_63 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_64 = DateTime_op_Addition_m4061583523(NULL /*static, unused*/, L_62, L_63, /*hidden argument*/NULL);
		V_14 = L_64;
		DateTime_t339033936  L_65 = V_18;
		TimeSpan_t763862892  L_66 = V_9;
		DateTime_t339033936  L_67 = DateTime_op_Addition_m4061583523(NULL /*static, unused*/, L_65, L_66, /*hidden argument*/NULL);
		TimeSpan_t763862892  L_68 = V_10;
		DateTime_t339033936  L_69 = DateTime_op_Addition_m4061583523(NULL /*static, unused*/, L_67, L_68, /*hidden argument*/NULL);
		V_20 = L_69;
		DateTime_t339033936  L_70 = DateTime_get_Date_m42247688((&V_20), /*hidden argument*/NULL);
		int32_t L_71 = DateTime_get_Year_m2333942068((&V_20), /*hidden argument*/NULL);
		DateTime_t339033936  L_72;
		memset(&L_72, 0, sizeof(L_72));
		DateTime__ctor_m145640619(&L_72, L_71, 1, 1, /*hidden argument*/NULL);
		bool L_73 = DateTime_op_Equality_m2277436664(NULL /*static, unused*/, L_70, L_72, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_01dc;
		}
	}
	{
		int32_t L_74 = DateTime_get_Year_m2333942068((&V_20), /*hidden argument*/NULL);
		int32_t L_75 = DateTime_get_Year_m2333942068((&V_14), /*hidden argument*/NULL);
		if ((((int32_t)L_74) <= ((int32_t)L_75)))
		{
			goto IL_01dc;
		}
	}
	{
		DateTime_t339033936  L_76 = V_20;
		TimeSpan_t763862892  L_77;
		memset(&L_77, 0, sizeof(L_77));
		TimeSpan__ctor_m4160332047(&L_77, ((int32_t)24), 0, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_78 = DateTime_op_Subtraction_m3609021319(NULL /*static, unused*/, L_76, L_77, /*hidden argument*/NULL);
		V_20 = L_78;
	}

IL_01dc:
	{
		int32_t L_79 = DateTime_get_Month_m1871036171((&V_14), /*hidden argument*/NULL);
		if ((((int32_t)L_79) >= ((int32_t)7)))
		{
			goto IL_01fe;
		}
	}
	{
		int32_t L_80 = DateTime_get_Year_m2333942068((&V_14), /*hidden argument*/NULL);
		DateTime__ctor_m145640619((&V_21), L_80, 1, 1, /*hidden argument*/NULL);
		goto IL_020e;
	}

IL_01fe:
	{
		int32_t L_81 = DateTime_get_Year_m2333942068((&V_14), /*hidden argument*/NULL);
		DateTime__ctor_m145640619((&V_21), L_81, 7, 1, /*hidden argument*/NULL);
	}

IL_020e:
	{
		int32_t L_82 = DateTime_get_Month_m1871036171((&V_20), /*hidden argument*/NULL);
		if ((((int32_t)L_82) < ((int32_t)7)))
		{
			goto IL_0232;
		}
	}
	{
		int32_t L_83 = DateTime_get_Year_m2333942068((&V_20), /*hidden argument*/NULL);
		DateTime__ctor_m145640619((&V_22), L_83, ((int32_t)12), ((int32_t)31), /*hidden argument*/NULL);
		goto IL_0243;
	}

IL_0232:
	{
		int32_t L_84 = DateTime_get_Year_m2333942068((&V_20), /*hidden argument*/NULL);
		DateTime__ctor_m145640619((&V_22), L_84, 6, ((int32_t)30), /*hidden argument*/NULL);
	}

IL_0243:
	{
		DateTime_t339033936  L_85;
		memset(&L_85, 0, sizeof(L_85));
		DateTime__ctor_m145640619(&L_85, 1, 1, 1, /*hidden argument*/NULL);
		TimeSpan_t763862892  L_86 = DateTime_get_TimeOfDay_m299590820((&V_14), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_87 = DateTime_op_Addition_m4061583523(NULL /*static, unused*/, L_85, L_86, /*hidden argument*/NULL);
		int32_t L_88 = DateTime_get_Month_m1871036171((&V_14), /*hidden argument*/NULL);
		int32_t L_89 = DateTime_get_Day_m609985447((&V_14), /*hidden argument*/NULL);
		TransitionTime_t1289309666  L_90 = TransitionTime_CreateFixedDateRule_m3570077514(NULL /*static, unused*/, L_87, L_88, L_89, /*hidden argument*/NULL);
		V_23 = L_90;
		DateTime_t339033936  L_91;
		memset(&L_91, 0, sizeof(L_91));
		DateTime__ctor_m145640619(&L_91, 1, 1, 1, /*hidden argument*/NULL);
		TimeSpan_t763862892  L_92 = DateTime_get_TimeOfDay_m299590820((&V_20), /*hidden argument*/NULL);
		DateTime_t339033936  L_93 = DateTime_op_Addition_m4061583523(NULL /*static, unused*/, L_91, L_92, /*hidden argument*/NULL);
		int32_t L_94 = DateTime_get_Month_m1871036171((&V_20), /*hidden argument*/NULL);
		int32_t L_95 = DateTime_get_Day_m609985447((&V_20), /*hidden argument*/NULL);
		TransitionTime_t1289309666  L_96 = TransitionTime_CreateFixedDateRule_m3570077514(NULL /*static, unused*/, L_93, L_94, L_95, /*hidden argument*/NULL);
		V_24 = L_96;
		TransitionTime_t1289309666  L_97 = V_23;
		TransitionTime_t1289309666  L_98 = V_24;
		bool L_99 = TransitionTime_op_Inequality_m3808521823(NULL /*static, unused*/, L_97, L_98, /*hidden argument*/NULL);
		if (!L_99)
		{
			goto IL_02b9;
		}
	}
	{
		List_1_t3087525666 * L_100 = V_15;
		DateTime_t339033936  L_101 = V_21;
		DateTime_t339033936  L_102 = V_22;
		TimeSpan_t763862892  L_103 = V_10;
		TransitionTime_t1289309666  L_104 = V_23;
		TransitionTime_t1289309666  L_105 = V_24;
		AdjustmentRule_t2290566697 * L_106 = AdjustmentRule_CreateAdjustmentRule_m1656297573(NULL /*static, unused*/, L_101, L_102, L_103, L_104, L_105, /*hidden argument*/NULL);
		NullCheck(L_100);
		VirtActionInvoker1< AdjustmentRule_t2290566697 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.TimeZoneInfo/AdjustmentRule>::Add(!0) */, L_100, L_106);
	}

IL_02b9:
	{
		V_13 = (bool)0;
		goto IL_0317;
	}

IL_02c1:
	{
		String_t* L_107 = V_12;
		String_t* L_108 = (&V_19)->get_Name_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_109 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_107, L_108, /*hidden argument*/NULL);
		if (L_109)
		{
			goto IL_02f0;
		}
	}
	{
		double L_110 = TimeSpan_get_TotalSeconds_m3163750087((&V_10), /*hidden argument*/NULL);
		int32_t L_111 = (&V_19)->get_Offset_0();
		double L_112 = TimeSpan_get_TotalSeconds_m3163750087((&V_9), /*hidden argument*/NULL);
		if ((((double)L_110) == ((double)((double)((double)(((double)((double)L_111)))-(double)L_112)))))
		{
			goto IL_0310;
		}
	}

IL_02f0:
	{
		String_t* L_113 = (&V_19)->get_Name_2();
		V_12 = L_113;
		int32_t L_114 = (&V_19)->get_Offset_0();
		TimeSpan_t763862892  L_115;
		memset(&L_115, 0, sizeof(L_115));
		TimeSpan__ctor_m4160332047(&L_115, 0, 0, L_114, /*hidden argument*/NULL);
		TimeSpan_t763862892  L_116 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_117 = TimeSpan_op_Subtraction_m3686790579(NULL /*static, unused*/, L_115, L_116, /*hidden argument*/NULL);
		V_10 = L_117;
	}

IL_0310:
	{
		DateTime_t339033936  L_118 = V_18;
		V_14 = L_118;
		V_13 = (bool)1;
	}

IL_0317:
	{
		int32_t L_119 = V_16;
		V_16 = ((int32_t)((int32_t)L_119+(int32_t)1));
	}

IL_031d:
	{
		int32_t L_120 = V_16;
		List_1_t3660159136 * L_121 = V_8;
		NullCheck(L_121);
		int32_t L_122 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::get_Count() */, L_121);
		if ((((int32_t)L_120) < ((int32_t)L_122)))
		{
			goto IL_00fe;
		}
	}
	{
		List_1_t3087525666 * L_123 = V_15;
		NullCheck(L_123);
		int32_t L_124 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.TimeZoneInfo/AdjustmentRule>::get_Count() */, L_123);
		if (L_124)
		{
			goto IL_036d;
		}
	}
	{
		Dictionary_2_t2773072230 * L_125 = V_7;
		NullCheck(L_125);
		TimeType_t2282261447  L_126 = VirtFuncInvoker1< TimeType_t2282261447 , int32_t >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,System.TimeZoneInfo/TimeType>::get_Item(!0) */, L_125, 0);
		V_25 = L_126;
		String_t* L_127 = V_11;
		if (L_127)
		{
			goto IL_0361;
		}
	}
	{
		String_t* L_128 = (&V_25)->get_Name_2();
		V_11 = L_128;
		int32_t L_129 = (&V_25)->get_Offset_0();
		TimeSpan__ctor_m4160332047((&V_9), 0, 0, L_129, /*hidden argument*/NULL);
	}

IL_0361:
	{
		String_t* L_130 = ___id;
		TimeSpan_t763862892  L_131 = V_9;
		String_t* L_132 = ___id;
		String_t* L_133 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_134 = TimeZoneInfo_CreateCustomTimeZone_m2111048456(NULL /*static, unused*/, L_130, L_131, L_132, L_133, /*hidden argument*/NULL);
		return L_134;
	}

IL_036d:
	{
		String_t* L_135 = ___id;
		TimeSpan_t763862892  L_136 = V_9;
		String_t* L_137 = ___id;
		String_t* L_138 = V_11;
		String_t* L_139 = V_12;
		List_1_t3087525666 * L_140 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		List_1_t3087525666 * L_141 = TimeZoneInfo_ValidateRules_m1082179371(NULL /*static, unused*/, L_140, /*hidden argument*/NULL);
		NullCheck(L_141);
		AdjustmentRuleU5BU5D_t1713960724* L_142 = List_1_ToArray_m1740619125(L_141, /*hidden argument*/List_1_ToArray_m1740619125_MethodInfo_var);
		TimeZoneInfo_t4131446812 * L_143 = TimeZoneInfo_CreateCustomTimeZone_m2240749902(NULL /*static, unused*/, L_135, L_136, L_137, L_138, L_139, L_142, /*hidden argument*/NULL);
		return L_143;
	}
}
// System.Collections.Generic.List`1<System.TimeZoneInfo/AdjustmentRule> System.TimeZoneInfo::ValidateRules(System.Collections.Generic.List`1<System.TimeZoneInfo/AdjustmentRule>)
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_ToArray_m1740619125_MethodInfo_var;
extern const uint32_t TimeZoneInfo_ValidateRules_m1082179371_MetadataUsageId;
extern "C"  List_1_t3087525666 * TimeZoneInfo_ValidateRules_m1082179371 (Il2CppObject * __this /* static, unused */, List_1_t3087525666 * ___adjustmentRules, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_ValidateRules_m1082179371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AdjustmentRule_t2290566697 * V_0 = NULL;
	AdjustmentRule_t2290566697 * V_1 = NULL;
	AdjustmentRuleU5BU5D_t1713960724* V_2 = NULL;
	int32_t V_3 = 0;
	{
		V_0 = (AdjustmentRule_t2290566697 *)NULL;
		List_1_t3087525666 * L_0 = ___adjustmentRules;
		NullCheck(L_0);
		AdjustmentRuleU5BU5D_t1713960724* L_1 = List_1_ToArray_m1740619125(L_0, /*hidden argument*/List_1_ToArray_m1740619125_MethodInfo_var);
		V_2 = L_1;
		V_3 = 0;
		goto IL_003e;
	}

IL_0010:
	{
		AdjustmentRuleU5BU5D_t1713960724* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_1 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)));
		AdjustmentRule_t2290566697 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		AdjustmentRule_t2290566697 * L_6 = V_0;
		NullCheck(L_6);
		DateTime_t339033936  L_7 = AdjustmentRule_get_DateEnd_m3953845890(L_6, /*hidden argument*/NULL);
		AdjustmentRule_t2290566697 * L_8 = V_1;
		NullCheck(L_8);
		DateTime_t339033936  L_9 = AdjustmentRule_get_DateStart_m2606242505(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_10 = DateTime_op_GreaterThan_m3846016869(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0038;
		}
	}
	{
		List_1_t3087525666 * L_11 = ___adjustmentRules;
		AdjustmentRule_t2290566697 * L_12 = V_1;
		NullCheck(L_11);
		VirtFuncInvoker1< bool, AdjustmentRule_t2290566697 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<System.TimeZoneInfo/AdjustmentRule>::Remove(!0) */, L_11, L_12);
	}

IL_0038:
	{
		AdjustmentRule_t2290566697 * L_13 = V_1;
		V_0 = L_13;
		int32_t L_14 = V_3;
		V_3 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_15 = V_3;
		AdjustmentRuleU5BU5D_t1713960724* L_16 = V_2;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		List_1_t3087525666 * L_17 = ___adjustmentRules;
		return L_17;
	}
}
// System.Collections.Generic.Dictionary`2<System.Int32,System.String> System.TimeZoneInfo::ParseAbbreviations(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* Dictionary_2_t1459299685_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3477359157_MethodInfo_var;
extern const uint32_t TimeZoneInfo_ParseAbbreviations_m883393013_MetadataUsageId;
extern "C"  Dictionary_2_t1459299685 * TimeZoneInfo_ParseAbbreviations_m883393013 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___buffer, int32_t ___index, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_ParseAbbreviations_m883393013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1459299685 * V_0 = NULL;
	int32_t V_1 = 0;
	StringBuilder_t3822575854 * V_2 = NULL;
	int32_t V_3 = 0;
	uint16_t V_4 = 0x0;
	int32_t V_5 = 0;
	{
		Dictionary_2_t1459299685 * L_0 = (Dictionary_2_t1459299685 *)il2cpp_codegen_object_new(Dictionary_2_t1459299685_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3477359157(L_0, /*hidden argument*/Dictionary_2__ctor_m3477359157_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		StringBuilder_t3822575854 * L_1 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_1, /*hidden argument*/NULL);
		V_2 = L_1;
		V_3 = 0;
		goto IL_0083;
	}

IL_0015:
	{
		ByteU5BU5D_t58506160* L_2 = ___buffer;
		int32_t L_3 = ___index;
		int32_t L_4 = V_3;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, ((int32_t)((int32_t)L_3+(int32_t)L_4)));
		int32_t L_5 = ((int32_t)((int32_t)L_3+(int32_t)L_4));
		V_4 = (((int32_t)((uint16_t)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_5))))));
		uint16_t L_6 = V_4;
		if (!L_6)
		{
			goto IL_0032;
		}
	}
	{
		StringBuilder_t3822575854 * L_7 = V_2;
		uint16_t L_8 = V_4;
		NullCheck(L_7);
		StringBuilder_Append_m2143093878(L_7, L_8, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_0032:
	{
		Dictionary_2_t1459299685 * L_9 = V_0;
		int32_t L_10 = V_1;
		StringBuilder_t3822575854 * L_11 = V_2;
		NullCheck(L_11);
		String_t* L_12 = StringBuilder_ToString_m350379841(L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker2< int32_t, String_t* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.String>::Add(!0,!1) */, L_9, L_10, L_12);
		V_5 = 1;
		goto IL_0068;
	}

IL_0047:
	{
		Dictionary_2_t1459299685 * L_13 = V_0;
		int32_t L_14 = V_1;
		int32_t L_15 = V_5;
		StringBuilder_t3822575854 * L_16 = V_2;
		int32_t L_17 = V_5;
		StringBuilder_t3822575854 * L_18 = V_2;
		NullCheck(L_18);
		int32_t L_19 = StringBuilder_get_Length_m2443133099(L_18, /*hidden argument*/NULL);
		int32_t L_20 = V_5;
		NullCheck(L_16);
		String_t* L_21 = StringBuilder_ToString_m3621056261(L_16, L_17, ((int32_t)((int32_t)L_19-(int32_t)L_20)), /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker2< int32_t, String_t* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.String>::Add(!0,!1) */, L_13, ((int32_t)((int32_t)L_14+(int32_t)L_15)), L_21);
		int32_t L_22 = V_5;
		V_5 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_23 = V_5;
		StringBuilder_t3822575854 * L_24 = V_2;
		NullCheck(L_24);
		int32_t L_25 = StringBuilder_get_Length_m2443133099(L_24, /*hidden argument*/NULL);
		if ((((int32_t)L_23) < ((int32_t)L_25)))
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_26 = V_3;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
		StringBuilder_t3822575854 * L_27 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_27, /*hidden argument*/NULL);
		V_2 = L_27;
	}

IL_007f:
	{
		int32_t L_28 = V_3;
		V_3 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = ___count;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_0015;
		}
	}
	{
		Dictionary_2_t1459299685 * L_31 = V_0;
		return L_31;
	}
}
// System.Collections.Generic.Dictionary`2<System.Int32,System.TimeZoneInfo/TimeType> System.TimeZoneInfo::ParseTimesTypes(System.Byte[],System.Int32,System.Int32,System.Collections.Generic.Dictionary`2<System.Int32,System.String>)
extern TypeInfo* Dictionary_2_t2773072230_il2cpp_TypeInfo_var;
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1734428230_MethodInfo_var;
extern const uint32_t TimeZoneInfo_ParseTimesTypes_m298446159_MetadataUsageId;
extern "C"  Dictionary_2_t2773072230 * TimeZoneInfo_ParseTimesTypes_m298446159 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___buffer, int32_t ___index, int32_t ___count, Dictionary_2_t1459299685 * ___abbreviations, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_ParseTimesTypes_m298446159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2773072230 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint8_t V_3 = 0x0;
	uint8_t V_4 = 0x0;
	{
		int32_t L_0 = ___count;
		Dictionary_2_t2773072230 * L_1 = (Dictionary_2_t2773072230 *)il2cpp_codegen_object_new(Dictionary_2_t2773072230_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1734428230(L_1, L_0, /*hidden argument*/Dictionary_2__ctor_m1734428230_MethodInfo_var);
		V_0 = L_1;
		V_1 = 0;
		goto IL_004f;
	}

IL_000e:
	{
		ByteU5BU5D_t58506160* L_2 = ___buffer;
		int32_t L_3 = ___index;
		int32_t L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		int32_t L_5 = TimeZoneInfo_ReadBigEndianInt32_m2013217080(NULL /*static, unused*/, L_2, ((int32_t)((int32_t)L_3+(int32_t)((int32_t)((int32_t)6*(int32_t)L_4)))), /*hidden argument*/NULL);
		V_2 = L_5;
		ByteU5BU5D_t58506160* L_6 = ___buffer;
		int32_t L_7 = ___index;
		int32_t L_8 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)((int32_t)((int32_t)L_7+(int32_t)((int32_t)((int32_t)6*(int32_t)L_8))))+(int32_t)4)));
		int32_t L_9 = ((int32_t)((int32_t)((int32_t)((int32_t)L_7+(int32_t)((int32_t)((int32_t)6*(int32_t)L_8))))+(int32_t)4));
		V_3 = ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9)));
		ByteU5BU5D_t58506160* L_10 = ___buffer;
		int32_t L_11 = ___index;
		int32_t L_12 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, ((int32_t)((int32_t)((int32_t)((int32_t)L_11+(int32_t)((int32_t)((int32_t)6*(int32_t)L_12))))+(int32_t)5)));
		int32_t L_13 = ((int32_t)((int32_t)((int32_t)((int32_t)L_11+(int32_t)((int32_t)((int32_t)6*(int32_t)L_12))))+(int32_t)5));
		V_4 = ((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_13)));
		Dictionary_2_t2773072230 * L_14 = V_0;
		int32_t L_15 = V_1;
		int32_t L_16 = V_2;
		uint8_t L_17 = V_3;
		Dictionary_2_t1459299685 * L_18 = ___abbreviations;
		uint8_t L_19 = V_4;
		NullCheck(L_18);
		String_t* L_20 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,System.String>::get_Item(!0) */, L_18, L_19);
		TimeType_t2282261447  L_21;
		memset(&L_21, 0, sizeof(L_21));
		TimeType__ctor_m2279875597(&L_21, L_16, (bool)((((int32_t)((((int32_t)L_17) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), L_20, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker2< int32_t, TimeType_t2282261447  >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.TimeZoneInfo/TimeType>::Add(!0,!1) */, L_14, L_15, L_21);
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_004f:
	{
		int32_t L_23 = V_1;
		int32_t L_24 = ___count;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_000e;
		}
	}
	{
		Dictionary_2_t2773072230 * L_25 = V_0;
		return L_25;
	}
}
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>> System.TimeZoneInfo::ParseTransitions(System.Byte[],System.Int32,System.Int32,System.Collections.Generic.Dictionary`2<System.Int32,System.TimeZoneInfo/TimeType>)
extern TypeInfo* List_1_t3660159136_il2cpp_TypeInfo_var;
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1715281837_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2__ctor_m435210039_MethodInfo_var;
extern const uint32_t TimeZoneInfo_ParseTransitions_m2742103091_MetadataUsageId;
extern "C"  List_1_t3660159136 * TimeZoneInfo_ParseTransitions_m2742103091 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___buffer, int32_t ___index, int32_t ___count, Dictionary_2_t2773072230 * ___time_types, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeZoneInfo_ParseTransitions_m2742103091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3660159136 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	DateTime_t339033936  V_3;
	memset(&V_3, 0, sizeof(V_3));
	uint8_t V_4 = 0x0;
	{
		int32_t L_0 = ___count;
		List_1_t3660159136 * L_1 = (List_1_t3660159136 *)il2cpp_codegen_object_new(List_1_t3660159136_il2cpp_TypeInfo_var);
		List_1__ctor_m1715281837(L_1, L_0, /*hidden argument*/List_1__ctor_m1715281837_MethodInfo_var);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0045;
	}

IL_000e:
	{
		ByteU5BU5D_t58506160* L_2 = ___buffer;
		int32_t L_3 = ___index;
		int32_t L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		int32_t L_5 = TimeZoneInfo_ReadBigEndianInt32_m2013217080(NULL /*static, unused*/, L_2, ((int32_t)((int32_t)L_3+(int32_t)((int32_t)((int32_t)4*(int32_t)L_4)))), /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = V_2;
		DateTime_t339033936  L_7 = TimeZoneInfo_DateTimeFromUnixTime_m1834568322(NULL /*static, unused*/, (((int64_t)((int64_t)L_6))), /*hidden argument*/NULL);
		V_3 = L_7;
		ByteU5BU5D_t58506160* L_8 = ___buffer;
		int32_t L_9 = ___index;
		int32_t L_10 = ___count;
		int32_t L_11 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)((int32_t)((int32_t)L_9+(int32_t)((int32_t)((int32_t)4*(int32_t)L_10))))+(int32_t)L_11)));
		int32_t L_12 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9+(int32_t)((int32_t)((int32_t)4*(int32_t)L_10))))+(int32_t)L_11));
		V_4 = ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		List_1_t3660159136 * L_13 = V_0;
		DateTime_t339033936  L_14 = V_3;
		Dictionary_2_t2773072230 * L_15 = ___time_types;
		uint8_t L_16 = V_4;
		NullCheck(L_15);
		TimeType_t2282261447  L_17 = VirtFuncInvoker1< TimeType_t2282261447 , int32_t >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,System.TimeZoneInfo/TimeType>::get_Item(!0) */, L_15, L_16);
		KeyValuePair_2_t2863200167  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m435210039(&L_18, L_14, L_17, /*hidden argument*/KeyValuePair_2__ctor_m435210039_MethodInfo_var);
		NullCheck(L_13);
		VirtActionInvoker1< KeyValuePair_2_t2863200167  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Add(!0) */, L_13, L_18);
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = ___count;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_000e;
		}
	}
	{
		List_1_t3660159136 * L_22 = V_0;
		return L_22;
	}
}
// System.DateTime System.TimeZoneInfo::DateTimeFromUnixTime(System.Int64)
extern "C"  DateTime_t339033936  TimeZoneInfo_DateTimeFromUnixTime_m1834568322 (Il2CppObject * __this /* static, unused */, int64_t ___unix_time, const MethodInfo* method)
{
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime__ctor_m145640619((&V_0), ((int32_t)1970), 1, 1, /*hidden argument*/NULL);
		int64_t L_0 = ___unix_time;
		DateTime_t339033936  L_1 = DateTime_AddSeconds_m2515640243((&V_0), (((double)((double)L_0))), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.TimeZoneInfo/AdjustmentRule::.ctor(System.DateTime,System.DateTime,System.TimeSpan,System.TimeZoneInfo/TransitionTime,System.TimeZoneInfo/TransitionTime)
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4278829272;
extern Il2CppCodeGenString* _stringLiteral2832154832;
extern Il2CppCodeGenString* _stringLiteral270533854;
extern Il2CppCodeGenString* _stringLiteral3992032792;
extern Il2CppCodeGenString* _stringLiteral1157808554;
extern Il2CppCodeGenString* _stringLiteral2853057970;
extern const uint32_t AdjustmentRule__ctor_m1713846023_MetadataUsageId;
extern "C"  void AdjustmentRule__ctor_m1713846023 (AdjustmentRule_t2290566697 * __this, DateTime_t339033936  ___dateStart, DateTime_t339033936  ___dateEnd, TimeSpan_t763862892  ___daylightDelta, TransitionTime_t1289309666  ___daylightTransitionStart, TransitionTime_t1289309666  ___daylightTransitionEnd, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdjustmentRule__ctor_m1713846023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = DateTime_get_Kind_m3496013602((&___dateStart), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = DateTime_get_Kind_m3496013602((&___dateEnd), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}

IL_001e:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, _stringLiteral4278829272, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0029:
	{
		TransitionTime_t1289309666  L_3 = ___daylightTransitionStart;
		TransitionTime_t1289309666  L_4 = ___daylightTransitionEnd;
		bool L_5 = TransitionTime_op_Equality_m3881305188(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0042;
		}
	}
	{
		ArgumentException_t124305799 * L_6 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_6, _stringLiteral2832154832, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0042:
	{
		int64_t L_7 = DateTime_get_Ticks_m386468226((&___dateStart), /*hidden argument*/NULL);
		if (((int64_t)((int64_t)L_7%(int64_t)((int64_t)864000000000LL))))
		{
			goto IL_006e;
		}
	}
	{
		int64_t L_8 = DateTime_get_Ticks_m386468226((&___dateEnd), /*hidden argument*/NULL);
		if (!((int64_t)((int64_t)L_8%(int64_t)((int64_t)864000000000LL))))
		{
			goto IL_0079;
		}
	}

IL_006e:
	{
		ArgumentException_t124305799 * L_9 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_9, _stringLiteral270533854, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0079:
	{
		DateTime_t339033936  L_10 = ___dateEnd;
		DateTime_t339033936  L_11 = ___dateStart;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_12 = DateTime_op_LessThan_m35073816(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0090;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_13 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_13, _stringLiteral3992032792, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_0090:
	{
		TimeSpan_t763862892  L_14 = ___daylightDelta;
		TimeSpan_t763862892  L_15;
		memset(&L_15, 0, sizeof(L_15));
		TimeSpan__ctor_m4160332047(&L_15, ((int32_t)14), 0, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_16 = TimeSpan_op_GreaterThan_m3920451985(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00b8;
		}
	}
	{
		TimeSpan_t763862892  L_17 = ___daylightDelta;
		TimeSpan_t763862892  L_18;
		memset(&L_18, 0, sizeof(L_18));
		TimeSpan__ctor_m4160332047(&L_18, ((int32_t)-14), 0, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_19 = TimeSpan_op_LessThan_m4265983228(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00c3;
		}
	}

IL_00b8:
	{
		ArgumentOutOfRangeException_t3479058991 * L_20 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_20, _stringLiteral1157808554, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}

IL_00c3:
	{
		int64_t L_21 = TimeSpan_get_Ticks_m315930342((&___daylightDelta), /*hidden argument*/NULL);
		if (!((int64_t)((int64_t)L_21%(int64_t)(((int64_t)((int64_t)((int32_t)10000000)))))))
		{
			goto IL_00e1;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_22 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_22, _stringLiteral2853057970, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_00e1:
	{
		DateTime_t339033936  L_23 = ___dateStart;
		__this->set_dateStart_1(L_23);
		DateTime_t339033936  L_24 = ___dateEnd;
		__this->set_dateEnd_0(L_24);
		TimeSpan_t763862892  L_25 = ___daylightDelta;
		__this->set_daylightDelta_2(L_25);
		TransitionTime_t1289309666  L_26 = ___daylightTransitionStart;
		__this->set_daylightTransitionStart_4(L_26);
		TransitionTime_t1289309666  L_27 = ___daylightTransitionEnd;
		__this->set_daylightTransitionEnd_3(L_27);
		return;
	}
}
// System.DateTime System.TimeZoneInfo/AdjustmentRule::get_DateEnd()
extern "C"  DateTime_t339033936  AdjustmentRule_get_DateEnd_m3953845890 (AdjustmentRule_t2290566697 * __this, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = __this->get_dateEnd_0();
		return L_0;
	}
}
// System.DateTime System.TimeZoneInfo/AdjustmentRule::get_DateStart()
extern "C"  DateTime_t339033936  AdjustmentRule_get_DateStart_m2606242505 (AdjustmentRule_t2290566697 * __this, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = __this->get_dateStart_1();
		return L_0;
	}
}
// System.TimeSpan System.TimeZoneInfo/AdjustmentRule::get_DaylightDelta()
extern "C"  TimeSpan_t763862892  AdjustmentRule_get_DaylightDelta_m3833226735 (AdjustmentRule_t2290566697 * __this, const MethodInfo* method)
{
	{
		TimeSpan_t763862892  L_0 = __this->get_daylightDelta_2();
		return L_0;
	}
}
// System.TimeZoneInfo/TransitionTime System.TimeZoneInfo/AdjustmentRule::get_DaylightTransitionEnd()
extern "C"  TransitionTime_t1289309666  AdjustmentRule_get_DaylightTransitionEnd_m1089560872 (AdjustmentRule_t2290566697 * __this, const MethodInfo* method)
{
	{
		TransitionTime_t1289309666  L_0 = __this->get_daylightTransitionEnd_3();
		return L_0;
	}
}
// System.TimeZoneInfo/TransitionTime System.TimeZoneInfo/AdjustmentRule::get_DaylightTransitionStart()
extern "C"  TransitionTime_t1289309666  AdjustmentRule_get_DaylightTransitionStart_m3102376943 (AdjustmentRule_t2290566697 * __this, const MethodInfo* method)
{
	{
		TransitionTime_t1289309666  L_0 = __this->get_daylightTransitionStart_4();
		return L_0;
	}
}
// System.TimeZoneInfo/AdjustmentRule System.TimeZoneInfo/AdjustmentRule::CreateAdjustmentRule(System.DateTime,System.DateTime,System.TimeSpan,System.TimeZoneInfo/TransitionTime,System.TimeZoneInfo/TransitionTime)
extern TypeInfo* AdjustmentRule_t2290566697_il2cpp_TypeInfo_var;
extern const uint32_t AdjustmentRule_CreateAdjustmentRule_m1656297573_MetadataUsageId;
extern "C"  AdjustmentRule_t2290566697 * AdjustmentRule_CreateAdjustmentRule_m1656297573 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___dateStart, DateTime_t339033936  ___dateEnd, TimeSpan_t763862892  ___daylightDelta, TransitionTime_t1289309666  ___daylightTransitionStart, TransitionTime_t1289309666  ___daylightTransitionEnd, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdjustmentRule_CreateAdjustmentRule_m1656297573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t339033936  L_0 = ___dateStart;
		DateTime_t339033936  L_1 = ___dateEnd;
		TimeSpan_t763862892  L_2 = ___daylightDelta;
		TransitionTime_t1289309666  L_3 = ___daylightTransitionStart;
		TransitionTime_t1289309666  L_4 = ___daylightTransitionEnd;
		AdjustmentRule_t2290566697 * L_5 = (AdjustmentRule_t2290566697 *)il2cpp_codegen_object_new(AdjustmentRule_t2290566697_il2cpp_TypeInfo_var);
		AdjustmentRule__ctor_m1713846023(L_5, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean System.TimeZoneInfo/AdjustmentRule::Equals(System.TimeZoneInfo/AdjustmentRule)
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t AdjustmentRule_Equals_m235226988_MetadataUsageId;
extern "C"  bool AdjustmentRule_Equals_m235226988 (AdjustmentRule_t2290566697 * __this, AdjustmentRule_t2290566697 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdjustmentRule_Equals_m235226988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B6_0 = 0;
	{
		DateTime_t339033936  L_0 = __this->get_dateStart_1();
		AdjustmentRule_t2290566697 * L_1 = ___other;
		NullCheck(L_1);
		DateTime_t339033936  L_2 = L_1->get_dateStart_1();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_3 = DateTime_op_Equality_m2277436664(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_006b;
		}
	}
	{
		DateTime_t339033936  L_4 = __this->get_dateEnd_0();
		AdjustmentRule_t2290566697 * L_5 = ___other;
		NullCheck(L_5);
		DateTime_t339033936  L_6 = L_5->get_dateEnd_0();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_7 = DateTime_op_Equality_m2277436664(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006b;
		}
	}
	{
		TimeSpan_t763862892  L_8 = __this->get_daylightDelta_2();
		AdjustmentRule_t2290566697 * L_9 = ___other;
		NullCheck(L_9);
		TimeSpan_t763862892  L_10 = L_9->get_daylightDelta_2();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_11 = TimeSpan_op_Equality_m2213378780(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006b;
		}
	}
	{
		TransitionTime_t1289309666  L_12 = __this->get_daylightTransitionStart_4();
		AdjustmentRule_t2290566697 * L_13 = ___other;
		NullCheck(L_13);
		TransitionTime_t1289309666  L_14 = L_13->get_daylightTransitionStart_4();
		bool L_15 = TransitionTime_op_Equality_m3881305188(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006b;
		}
	}
	{
		TransitionTime_t1289309666  L_16 = __this->get_daylightTransitionEnd_3();
		AdjustmentRule_t2290566697 * L_17 = ___other;
		NullCheck(L_17);
		TransitionTime_t1289309666  L_18 = L_17->get_daylightTransitionEnd_3();
		bool L_19 = TransitionTime_op_Equality_m3881305188(NULL /*static, unused*/, L_16, L_18, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_19));
		goto IL_006c;
	}

IL_006b:
	{
		G_B6_0 = 0;
	}

IL_006c:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 System.TimeZoneInfo/AdjustmentRule::GetHashCode()
extern "C"  int32_t AdjustmentRule_GetHashCode_m3596128762 (AdjustmentRule_t2290566697 * __this, const MethodInfo* method)
{
	{
		DateTime_t339033936 * L_0 = __this->get_address_of_dateStart_1();
		int32_t L_1 = DateTime_GetHashCode_m2255586565(L_0, /*hidden argument*/NULL);
		DateTime_t339033936 * L_2 = __this->get_address_of_dateEnd_0();
		int32_t L_3 = DateTime_GetHashCode_m2255586565(L_2, /*hidden argument*/NULL);
		TimeSpan_t763862892 * L_4 = __this->get_address_of_daylightDelta_2();
		int32_t L_5 = TimeSpan_GetHashCode_m3188156777(L_4, /*hidden argument*/NULL);
		TransitionTime_t1289309666 * L_6 = __this->get_address_of_daylightTransitionStart_4();
		int32_t L_7 = TransitionTime_GetHashCode_m2738946337(L_6, /*hidden argument*/NULL);
		TransitionTime_t1289309666 * L_8 = __this->get_address_of_daylightTransitionEnd_3();
		int32_t L_9 = TransitionTime_GetHashCode_m2738946337(L_8, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)L_3))^(int32_t)L_5))^(int32_t)L_7))^(int32_t)L_9));
	}
}
// System.Void System.TimeZoneInfo/AdjustmentRule::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t AdjustmentRule_GetObjectData_m3940129623_MetadataUsageId;
extern "C"  void AdjustmentRule_GetObjectData_m3940129623 (AdjustmentRule_t2290566697 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdjustmentRule_GetObjectData_m3940129623_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.TimeZoneInfo/AdjustmentRule::OnDeserialization(System.Object)
extern TypeInfo* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t AdjustmentRule_OnDeserialization_m1048465261_MetadataUsageId;
extern "C"  void AdjustmentRule_OnDeserialization_m1048465261 (AdjustmentRule_t2290566697 * __this, Il2CppObject * ___sender, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdjustmentRule_OnDeserialization_m1048465261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.TimeZoneInfo/TimeType::.ctor(System.Int32,System.Boolean,System.String)
extern "C"  void TimeType__ctor_m2279875597 (TimeType_t2282261447 * __this, int32_t ___offset, bool ___is_dst, String_t* ___abbrev, const MethodInfo* method)
{
	{
		int32_t L_0 = ___offset;
		__this->set_Offset_0(L_0);
		bool L_1 = ___is_dst;
		__this->set_IsDst_1(L_1);
		String_t* L_2 = ___abbrev;
		__this->set_Name_2(L_2);
		return;
	}
}
// System.String System.TimeZoneInfo/TimeType::ToString()
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t211005341_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3538981625;
extern Il2CppCodeGenString* _stringLiteral1854223997;
extern Il2CppCodeGenString* _stringLiteral3839090673;
extern const uint32_t TimeType_ToString_m560147672_MetadataUsageId;
extern "C"  String_t* TimeType_ToString_m560147672 (TimeType_t2282261447 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeType_ToString_m560147672_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3538981625);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3538981625);
		ObjectU5BU5D_t11523773* L_1 = L_0;
		int32_t L_2 = __this->get_Offset_0();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t11523773* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral1854223997);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1854223997);
		ObjectU5BU5D_t11523773* L_6 = L_5;
		bool L_7 = __this->get_IsDst_1();
		bool L_8 = L_7;
		Il2CppObject * L_9 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral3839090673);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3839090673);
		ObjectU5BU5D_t11523773* L_11 = L_10;
		String_t* L_12 = __this->get_Name_2();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m3016520001(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return L_13;
	}
}
// Conversion methods for marshalling of: System.TimeZoneInfo/TimeType
extern "C" void TimeType_t2282261447_marshal(const TimeType_t2282261447& unmarshaled, TimeType_t2282261447_marshaled& marshaled)
{
	marshaled.___Offset_0 = unmarshaled.get_Offset_0();
	marshaled.___IsDst_1 = unmarshaled.get_IsDst_1();
	marshaled.___Name_2 = il2cpp_codegen_marshal_string(unmarshaled.get_Name_2());
}
extern "C" void TimeType_t2282261447_marshal_back(const TimeType_t2282261447_marshaled& marshaled, TimeType_t2282261447& unmarshaled)
{
	int32_t unmarshaled_Offset_temp = 0;
	unmarshaled_Offset_temp = marshaled.___Offset_0;
	unmarshaled.set_Offset_0(unmarshaled_Offset_temp);
	bool unmarshaled_IsDst_temp = false;
	unmarshaled_IsDst_temp = marshaled.___IsDst_1;
	unmarshaled.set_IsDst_1(unmarshaled_IsDst_temp);
	unmarshaled.set_Name_2(il2cpp_codegen_marshal_string_result(marshaled.___Name_2));
}
// Conversion method for clean up from marshalling of: System.TimeZoneInfo/TimeType
extern "C" void TimeType_t2282261447_marshal_cleanup(TimeType_t2282261447_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___Name_2);
	marshaled.___Name_2 = NULL;
}
// System.Void System.TimeZoneInfo/TransitionTime::.ctor(System.DateTime,System.Int32,System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral581948668;
extern const uint32_t TransitionTime__ctor_m189938936_MetadataUsageId;
extern "C"  void TransitionTime__ctor_m189938936 (TransitionTime_t1289309666 * __this, DateTime_t339033936  ___timeOfDay, int32_t ___month, int32_t ___day, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransitionTime__ctor_m189938936_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t339033936  L_0 = ___timeOfDay;
		int32_t L_1 = ___month;
		TransitionTime__ctor_m108674623(__this, L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___day;
		if ((((int32_t)L_2) < ((int32_t)1)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = ___day;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)31))))
		{
			goto IL_0022;
		}
	}

IL_0017:
	{
		ArgumentOutOfRangeException_t3479058991 * L_4 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_4, _stringLiteral581948668, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0022:
	{
		int32_t L_5 = ___day;
		__this->set_day_2(L_5);
		__this->set_isFixedDateRule_5((bool)1);
		return;
	}
}
// System.Void System.TimeZoneInfo/TransitionTime::.ctor(System.DateTime,System.Int32)
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4286265326;
extern Il2CppCodeGenString* _stringLiteral1500651089;
extern Il2CppCodeGenString* _stringLiteral2080288345;
extern Il2CppCodeGenString* _stringLiteral354069923;
extern const uint32_t TransitionTime__ctor_m108674623_MetadataUsageId;
extern "C"  void TransitionTime__ctor_m108674623 (TransitionTime_t1289309666 * __this, DateTime_t339033936  ___timeOfDay, int32_t ___month, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransitionTime__ctor_m108674623_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = DateTime_get_Year_m2333942068((&___timeOfDay), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_1 = DateTime_get_Month_m1871036171((&___timeOfDay), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_2 = DateTime_get_Day_m609985447((&___timeOfDay), /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0032;
		}
	}

IL_0027:
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, _stringLiteral4286265326, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0032:
	{
		int32_t L_4 = DateTime_get_Kind_m3496013602((&___timeOfDay), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		ArgumentException_t124305799 * L_5 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_5, _stringLiteral1500651089, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0049:
	{
		int64_t L_6 = DateTime_get_Ticks_m386468226((&___timeOfDay), /*hidden argument*/NULL);
		if (!((int64_t)((int64_t)L_6%(int64_t)(((int64_t)((int64_t)((int32_t)10000)))))))
		{
			goto IL_0067;
		}
	}
	{
		ArgumentException_t124305799 * L_7 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_7, _stringLiteral2080288345, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0067:
	{
		int32_t L_8 = ___month;
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_9 = ___month;
		if ((((int32_t)L_9) <= ((int32_t)((int32_t)12))))
		{
			goto IL_0081;
		}
	}

IL_0076:
	{
		ArgumentOutOfRangeException_t3479058991 * L_10 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_10, _stringLiteral354069923, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0081:
	{
		DateTime_t339033936  L_11 = ___timeOfDay;
		__this->set_timeOfDay_0(L_11);
		int32_t L_12 = ___month;
		__this->set_month_1(L_12);
		__this->set_week_3((-1));
		__this->set_dayOfWeek_4((-1));
		__this->set_day_2((-1));
		__this->set_isFixedDateRule_5((bool)0);
		return;
	}
}
// System.DateTime System.TimeZoneInfo/TransitionTime::get_TimeOfDay()
extern "C"  DateTime_t339033936  TransitionTime_get_TimeOfDay_m3313234260 (TransitionTime_t1289309666 * __this, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = __this->get_timeOfDay_0();
		return L_0;
	}
}
// System.Int32 System.TimeZoneInfo/TransitionTime::get_Month()
extern "C"  int32_t TransitionTime_get_Month_m2331873831 (TransitionTime_t1289309666 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_month_1();
		return L_0;
	}
}
// System.Int32 System.TimeZoneInfo/TransitionTime::get_Day()
extern "C"  int32_t TransitionTime_get_Day_m2862976451 (TransitionTime_t1289309666 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_day_2();
		return L_0;
	}
}
// System.Int32 System.TimeZoneInfo/TransitionTime::get_Week()
extern "C"  int32_t TransitionTime_get_Week_m3400040591 (TransitionTime_t1289309666 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_week_3();
		return L_0;
	}
}
// System.DayOfWeek System.TimeZoneInfo/TransitionTime::get_DayOfWeek()
extern "C"  int32_t TransitionTime_get_DayOfWeek_m2396869159 (TransitionTime_t1289309666 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_dayOfWeek_4();
		return L_0;
	}
}
// System.Boolean System.TimeZoneInfo/TransitionTime::get_IsFixedDateRule()
extern "C"  bool TransitionTime_get_IsFixedDateRule_m3223864213 (TransitionTime_t1289309666 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isFixedDateRule_5();
		return L_0;
	}
}
// System.TimeZoneInfo/TransitionTime System.TimeZoneInfo/TransitionTime::CreateFixedDateRule(System.DateTime,System.Int32,System.Int32)
extern "C"  TransitionTime_t1289309666  TransitionTime_CreateFixedDateRule_m3570077514 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___timeOfDay, int32_t ___month, int32_t ___day, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = ___timeOfDay;
		int32_t L_1 = ___month;
		int32_t L_2 = ___day;
		TransitionTime_t1289309666  L_3;
		memset(&L_3, 0, sizeof(L_3));
		TransitionTime__ctor_m189938936(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.TimeZoneInfo/TransitionTime::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t TransitionTime_GetObjectData_m2320602174_MetadataUsageId;
extern "C"  void TransitionTime_GetObjectData_m2320602174 (TransitionTime_t1289309666 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransitionTime_GetObjectData_m2320602174_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.TimeZoneInfo/TransitionTime::Equals(System.Object)
extern TypeInfo* TransitionTime_t1289309666_il2cpp_TypeInfo_var;
extern const uint32_t TransitionTime_Equals_m2637525629_MetadataUsageId;
extern "C"  bool TransitionTime_Equals_m2637525629 (TransitionTime_t1289309666 * __this, Il2CppObject * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransitionTime_Equals_m2637525629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___other;
		if (!((Il2CppObject *)IsInstSealed(L_0, TransitionTime_t1289309666_il2cpp_TypeInfo_var)))
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = ___other;
		bool L_2 = TransitionTime_op_Equality_m3881305188(NULL /*static, unused*/, (*(TransitionTime_t1289309666 *)__this), ((*(TransitionTime_t1289309666 *)((TransitionTime_t1289309666 *)UnBox (L_1, TransitionTime_t1289309666_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Boolean System.TimeZoneInfo/TransitionTime::Equals(System.TimeZoneInfo/TransitionTime)
extern "C"  bool TransitionTime_Equals_m147082508 (TransitionTime_t1289309666 * __this, TransitionTime_t1289309666  ___other, const MethodInfo* method)
{
	{
		TransitionTime_t1289309666  L_0 = ___other;
		bool L_1 = TransitionTime_op_Equality_m3881305188(NULL /*static, unused*/, (*(TransitionTime_t1289309666 *)__this), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.TimeZoneInfo/TransitionTime::GetHashCode()
extern "C"  int32_t TransitionTime_GetHashCode_m2738946337 (TransitionTime_t1289309666 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_day_2();
		int32_t L_1 = __this->get_dayOfWeek_4();
		int32_t L_2 = __this->get_month_1();
		DateTime_t339033936 * L_3 = __this->get_address_of_timeOfDay_0();
		int64_t L_4 = DateTime_get_Ticks_m386468226(L_3, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_week_3();
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)L_1))^(int32_t)L_2))^(int32_t)(((int32_t)((int32_t)L_4)))))^(int32_t)L_5));
	}
}
// System.Void System.TimeZoneInfo/TransitionTime::OnDeserialization(System.Object)
extern TypeInfo* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t TransitionTime_OnDeserialization_m2922457318_MetadataUsageId;
extern "C"  void TransitionTime_OnDeserialization_m2922457318 (TransitionTime_t1289309666 * __this, Il2CppObject * ___sender, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransitionTime_OnDeserialization_m2922457318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.TimeZoneInfo/TransitionTime::op_Equality(System.TimeZoneInfo/TransitionTime,System.TimeZoneInfo/TransitionTime)
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t TransitionTime_op_Equality_m3881305188_MetadataUsageId;
extern "C"  bool TransitionTime_op_Equality_m3881305188 (Il2CppObject * __this /* static, unused */, TransitionTime_t1289309666  ___t1, TransitionTime_t1289309666  ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransitionTime_op_Equality_m3881305188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = (&___t1)->get_day_2();
		int32_t L_1 = (&___t2)->get_day_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_2 = (&___t1)->get_dayOfWeek_4();
		int32_t L_3 = (&___t2)->get_dayOfWeek_4();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0076;
		}
	}
	{
		bool L_4 = (&___t1)->get_isFixedDateRule_5();
		bool L_5 = (&___t2)->get_isFixedDateRule_5();
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_6 = (&___t1)->get_month_1();
		int32_t L_7 = (&___t2)->get_month_1();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_0076;
		}
	}
	{
		DateTime_t339033936  L_8 = (&___t1)->get_timeOfDay_0();
		DateTime_t339033936  L_9 = (&___t2)->get_timeOfDay_0();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		bool L_10 = DateTime_op_Equality_m2277436664(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_11 = (&___t1)->get_week_3();
		int32_t L_12 = (&___t2)->get_week_3();
		G_B7_0 = ((((int32_t)L_11) == ((int32_t)L_12))? 1 : 0);
		goto IL_0077;
	}

IL_0076:
	{
		G_B7_0 = 0;
	}

IL_0077:
	{
		return (bool)G_B7_0;
	}
}
// System.Boolean System.TimeZoneInfo/TransitionTime::op_Inequality(System.TimeZoneInfo/TransitionTime,System.TimeZoneInfo/TransitionTime)
extern "C"  bool TransitionTime_op_Inequality_m3808521823 (Il2CppObject * __this /* static, unused */, TransitionTime_t1289309666  ___t1, TransitionTime_t1289309666  ___t2, const MethodInfo* method)
{
	{
		TransitionTime_t1289309666  L_0 = ___t1;
		TransitionTime_t1289309666  L_1 = ___t2;
		bool L_2 = TransitionTime_op_Equality_m3881305188(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.TimeZoneNotFoundException::.ctor()
extern "C"  void TimeZoneNotFoundException__ctor_m636423683 (TimeZoneNotFoundException_t3613219068 * __this, const MethodInfo* method)
{
	{
		Exception__ctor_m3223090658(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TimeZoneNotFoundException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TimeZoneNotFoundException__ctor_m4152878276 (TimeZoneNotFoundException_t3613219068 * __this, SerializationInfo_t2995724695 * ___info, StreamingContext_t986364934  ___sc, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info;
		StreamingContext_t986364934  L_1 = ___sc;
		Exception__ctor_m3602014243(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
