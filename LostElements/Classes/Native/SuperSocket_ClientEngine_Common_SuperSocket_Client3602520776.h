﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.Sockets.Socket
struct Socket_t150013987;
// System.Exception
struct Exception_t1967233988;

#include "mscorlib_System_EventArgs516466188.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.ProxyEventArgs
struct  ProxyEventArgs_t3602520776  : public EventArgs_t516466188
{
public:
	// System.Boolean SuperSocket.ClientEngine.ProxyEventArgs::<Connected>k__BackingField
	bool ___U3CConnectedU3Ek__BackingField_1;
	// System.Net.Sockets.Socket SuperSocket.ClientEngine.ProxyEventArgs::<Socket>k__BackingField
	Socket_t150013987 * ___U3CSocketU3Ek__BackingField_2;
	// System.Exception SuperSocket.ClientEngine.ProxyEventArgs::<Exception>k__BackingField
	Exception_t1967233988 * ___U3CExceptionU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CConnectedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProxyEventArgs_t3602520776, ___U3CConnectedU3Ek__BackingField_1)); }
	inline bool get_U3CConnectedU3Ek__BackingField_1() const { return ___U3CConnectedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CConnectedU3Ek__BackingField_1() { return &___U3CConnectedU3Ek__BackingField_1; }
	inline void set_U3CConnectedU3Ek__BackingField_1(bool value)
	{
		___U3CConnectedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSocketU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProxyEventArgs_t3602520776, ___U3CSocketU3Ek__BackingField_2)); }
	inline Socket_t150013987 * get_U3CSocketU3Ek__BackingField_2() const { return ___U3CSocketU3Ek__BackingField_2; }
	inline Socket_t150013987 ** get_address_of_U3CSocketU3Ek__BackingField_2() { return &___U3CSocketU3Ek__BackingField_2; }
	inline void set_U3CSocketU3Ek__BackingField_2(Socket_t150013987 * value)
	{
		___U3CSocketU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSocketU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProxyEventArgs_t3602520776, ___U3CExceptionU3Ek__BackingField_3)); }
	inline Exception_t1967233988 * get_U3CExceptionU3Ek__BackingField_3() const { return ___U3CExceptionU3Ek__BackingField_3; }
	inline Exception_t1967233988 ** get_address_of_U3CExceptionU3Ek__BackingField_3() { return &___U3CExceptionU3Ek__BackingField_3; }
	inline void set_U3CExceptionU3Ek__BackingField_3(Exception_t1967233988 * value)
	{
		___U3CExceptionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CExceptionU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
