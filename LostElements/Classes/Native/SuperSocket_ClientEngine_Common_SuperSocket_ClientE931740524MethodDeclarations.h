﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Net.Sockets.SocketAsyncEventArgs
struct SocketAsyncEventArgs_t970431102;
// System.Net.EndPoint
struct EndPoint_t1294049535;
// SuperSocket.ClientEngine.ConnectedCallback
struct ConnectedCallback_t1584521517;
// System.Net.IPAddress
struct IPAddress_t3220500535;
// SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState
struct DnsConnectState_t2343074160;
// System.Net.Sockets.Socket
struct Socket_t150013987;
// System.IAsyncResult
struct IAsyncResult_t537683269;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "System_System_Net_Sockets_SocketAsyncEventArgs970431102.h"
#include "System_System_Net_EndPoint1294049535.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client1584521517.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client2343074160.h"
#include "System_System_Net_Sockets_Socket150013987.h"

// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::SocketAsyncEventCompleted(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern "C"  void ConnectAsyncExtension_SocketAsyncEventCompleted_m2832229679 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.SocketAsyncEventArgs SuperSocket.ClientEngine.ConnectAsyncExtension::CreateSocketAsyncEventArgs(System.Net.EndPoint,SuperSocket.ClientEngine.ConnectedCallback,System.Object)
extern "C"  SocketAsyncEventArgs_t970431102 * ConnectAsyncExtension_CreateSocketAsyncEventArgs_m2221185602 (Il2CppObject * __this /* static, unused */, EndPoint_t1294049535 * ___remoteEndPoint, ConnectedCallback_t1584521517 * ___callback, Il2CppObject * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::ConnectAsyncInternal(System.Net.EndPoint,SuperSocket.ClientEngine.ConnectedCallback,System.Object)
extern "C"  void ConnectAsyncExtension_ConnectAsyncInternal_m3327551872 (Il2CppObject * __this /* static, unused */, EndPoint_t1294049535 * ___remoteEndPoint, ConnectedCallback_t1584521517 * ___callback, Il2CppObject * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress SuperSocket.ClientEngine.ConnectAsyncExtension::GetNextAddress(SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState,System.Net.Sockets.Socket&)
extern "C"  IPAddress_t3220500535 * ConnectAsyncExtension_GetNextAddress_m2210336608 (Il2CppObject * __this /* static, unused */, DnsConnectState_t2343074160 * ___state, Socket_t150013987 ** ___attempSocket, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::OnGetHostAddresses(System.IAsyncResult)
extern "C"  void ConnectAsyncExtension_OnGetHostAddresses_m3640693833 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::SocketConnectCompleted(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern "C"  void ConnectAsyncExtension_SocketConnectCompleted_m1655121833 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::ClearSocketAsyncEventArgs(System.Net.Sockets.SocketAsyncEventArgs)
extern "C"  void ConnectAsyncExtension_ClearSocketAsyncEventArgs_m3348425600 (Il2CppObject * __this /* static, unused */, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::ConnectAsync(System.Net.EndPoint,SuperSocket.ClientEngine.ConnectedCallback,System.Object)
extern "C"  void ConnectAsyncExtension_ConnectAsync_m2701800175 (Il2CppObject * __this /* static, unused */, EndPoint_t1294049535 * ___remoteEndPoint, ConnectedCallback_t1584521517 * ___callback, Il2CppObject * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension::CreateAttempSocket(SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState)
extern "C"  void ConnectAsyncExtension_CreateAttempSocket_m1410490880 (Il2CppObject * __this /* static, unused */, DnsConnectState_t2343074160 * ___connectState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
