﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// IntroductionController
struct IntroductionController_t4287196022;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen1056199668.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IntroductionController/$Start$3/$
struct  U24_t36  : public GenericGeneratorEnumerator_1_t1056199668
{
public:
	// System.String IntroductionController/$Start$3/$::$currentPlayer$4
	String_t* ___U24currentPlayerU244_2;
	// IntroductionController IntroductionController/$Start$3/$::$self_$5
	IntroductionController_t4287196022 * ___U24self_U245_3;

public:
	inline static int32_t get_offset_of_U24currentPlayerU244_2() { return static_cast<int32_t>(offsetof(U24_t36, ___U24currentPlayerU244_2)); }
	inline String_t* get_U24currentPlayerU244_2() const { return ___U24currentPlayerU244_2; }
	inline String_t** get_address_of_U24currentPlayerU244_2() { return &___U24currentPlayerU244_2; }
	inline void set_U24currentPlayerU244_2(String_t* value)
	{
		___U24currentPlayerU244_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24currentPlayerU244_2, value);
	}

	inline static int32_t get_offset_of_U24self_U245_3() { return static_cast<int32_t>(offsetof(U24_t36, ___U24self_U245_3)); }
	inline IntroductionController_t4287196022 * get_U24self_U245_3() const { return ___U24self_U245_3; }
	inline IntroductionController_t4287196022 ** get_address_of_U24self_U245_3() { return &___U24self_U245_3; }
	inline void set_U24self_U245_3(IntroductionController_t4287196022 * value)
	{
		___U24self_U245_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U245_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
