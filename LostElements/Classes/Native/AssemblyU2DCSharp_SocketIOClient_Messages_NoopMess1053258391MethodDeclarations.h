﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Messages.NoopMessage
struct NoopMessage_t1053258391;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SocketIOClient.Messages.NoopMessage::.ctor()
extern "C"  void NoopMessage__ctor_m2237861598 (NoopMessage_t1053258391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.NoopMessage SocketIOClient.Messages.NoopMessage::Deserialize(System.String)
extern "C"  NoopMessage_t1053258391 * NoopMessage_Deserialize_m4108323233 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
