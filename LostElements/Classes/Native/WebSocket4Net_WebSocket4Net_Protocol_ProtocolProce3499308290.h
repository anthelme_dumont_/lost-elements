﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WebSocket4Net.Protocol.IProtocolProcessor[]
struct IProtocolProcessorU5BU5D_t2571124850;
// System.Func`2<WebSocket4Net.Protocol.IProtocolProcessor,System.Int32>
struct Func_2_t2410725500;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_t1649583772;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.ProtocolProcessorFactory
struct  ProtocolProcessorFactory_t3499308290  : public Il2CppObject
{
public:
	// WebSocket4Net.Protocol.IProtocolProcessor[] WebSocket4Net.Protocol.ProtocolProcessorFactory::m_OrderedProcessors
	IProtocolProcessorU5BU5D_t2571124850* ___m_OrderedProcessors_0;

public:
	inline static int32_t get_offset_of_m_OrderedProcessors_0() { return static_cast<int32_t>(offsetof(ProtocolProcessorFactory_t3499308290, ___m_OrderedProcessors_0)); }
	inline IProtocolProcessorU5BU5D_t2571124850* get_m_OrderedProcessors_0() const { return ___m_OrderedProcessors_0; }
	inline IProtocolProcessorU5BU5D_t2571124850** get_address_of_m_OrderedProcessors_0() { return &___m_OrderedProcessors_0; }
	inline void set_m_OrderedProcessors_0(IProtocolProcessorU5BU5D_t2571124850* value)
	{
		___m_OrderedProcessors_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_OrderedProcessors_0, value);
	}
};

struct ProtocolProcessorFactory_t3499308290_StaticFields
{
public:
	// System.Func`2<WebSocket4Net.Protocol.IProtocolProcessor,System.Int32> WebSocket4Net.Protocol.ProtocolProcessorFactory::CS$<>9__CachedAnonymousMethodDelegate1
	Func_2_t2410725500 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1;
	// System.Func`2<System.Int32,System.Int32> WebSocket4Net.Protocol.ProtocolProcessorFactory::CS$<>9__CachedAnonymousMethodDelegate6
	Func_2_t1649583772 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2;

public:
	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return static_cast<int32_t>(offsetof(ProtocolProcessorFactory_t3499308290_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1)); }
	inline Func_2_t2410725500 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline Func_2_t2410725500 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1(Func_2_t2410725500 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1_1, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2() { return static_cast<int32_t>(offsetof(ProtocolProcessorFactory_t3499308290_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2)); }
	inline Func_2_t1649583772 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2; }
	inline Func_2_t1649583772 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2(Func_2_t1649583772 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate6_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
