﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// SuperSocket.ClientEngine.SearchMarkState`1<System.Byte>
struct SearchMarkState_1_t304232942;

#include "WebSocket4Net_WebSocket4Net_Protocol_ReaderBase893022310.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.HandshakeReader
struct  HandshakeReader_t1078082956  : public ReaderBase_t893022310
{
public:
	// SuperSocket.ClientEngine.SearchMarkState`1<System.Byte> WebSocket4Net.Protocol.HandshakeReader::m_HeadSeachState
	SearchMarkState_1_t304232942 * ___m_HeadSeachState_5;

public:
	inline static int32_t get_offset_of_m_HeadSeachState_5() { return static_cast<int32_t>(offsetof(HandshakeReader_t1078082956, ___m_HeadSeachState_5)); }
	inline SearchMarkState_1_t304232942 * get_m_HeadSeachState_5() const { return ___m_HeadSeachState_5; }
	inline SearchMarkState_1_t304232942 ** get_address_of_m_HeadSeachState_5() { return &___m_HeadSeachState_5; }
	inline void set_m_HeadSeachState_5(SearchMarkState_1_t304232942 * value)
	{
		___m_HeadSeachState_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_HeadSeachState_5, value);
	}
};

struct HandshakeReader_t1078082956_StaticFields
{
public:
	// System.String WebSocket4Net.Protocol.HandshakeReader::BadRequestCode
	String_t* ___BadRequestCode_3;
	// System.Byte[] WebSocket4Net.Protocol.HandshakeReader::HeaderTerminator
	ByteU5BU5D_t58506160* ___HeaderTerminator_4;

public:
	inline static int32_t get_offset_of_BadRequestCode_3() { return static_cast<int32_t>(offsetof(HandshakeReader_t1078082956_StaticFields, ___BadRequestCode_3)); }
	inline String_t* get_BadRequestCode_3() const { return ___BadRequestCode_3; }
	inline String_t** get_address_of_BadRequestCode_3() { return &___BadRequestCode_3; }
	inline void set_BadRequestCode_3(String_t* value)
	{
		___BadRequestCode_3 = value;
		Il2CppCodeGenWriteBarrier(&___BadRequestCode_3, value);
	}

	inline static int32_t get_offset_of_HeaderTerminator_4() { return static_cast<int32_t>(offsetof(HandshakeReader_t1078082956_StaticFields, ___HeaderTerminator_4)); }
	inline ByteU5BU5D_t58506160* get_HeaderTerminator_4() const { return ___HeaderTerminator_4; }
	inline ByteU5BU5D_t58506160** get_address_of_HeaderTerminator_4() { return &___HeaderTerminator_4; }
	inline void set_HeaderTerminator_4(ByteU5BU5D_t58506160* value)
	{
		___HeaderTerminator_4 = value;
		Il2CppCodeGenWriteBarrier(&___HeaderTerminator_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
