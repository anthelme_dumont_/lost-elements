﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1992445980(__this, ___key, ___value, method) ((  void (*) (KeyValuePair_2_t3851545214 *, Type_t *, SafeDictionary_2_t2250466201 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>::get_Key()
#define KeyValuePair_2_get_Key_m1814692812(__this, method) ((  Type_t * (*) (KeyValuePair_2_t3851545214 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3765069965(__this, ___value, method) ((  void (*) (KeyValuePair_2_t3851545214 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>::get_Value()
#define KeyValuePair_2_get_Value_m961510512(__this, method) ((  SafeDictionary_2_t2250466201 * (*) (KeyValuePair_2_t3851545214 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1360309773(__this, ___value, method) ((  void (*) (KeyValuePair_2_t3851545214 *, SafeDictionary_2_t2250466201 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>::ToString()
#define KeyValuePair_2_ToString_m362884635(__this, method) ((  String_t* (*) (KeyValuePair_2_t3851545214 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
