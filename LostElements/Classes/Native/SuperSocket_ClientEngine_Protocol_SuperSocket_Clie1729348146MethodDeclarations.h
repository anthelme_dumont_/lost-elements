﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>
struct ArraySegmentEx_1_t1729348146;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"

// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>::.ctor(T[],System.Int32,System.Int32)
extern "C"  void ArraySegmentEx_1__ctor_m1787516805_gshared (ArraySegmentEx_1_t1729348146 * __this, ObjectU5BU5D_t11523773* ___array, int32_t ___offset, int32_t ___count, const MethodInfo* method);
#define ArraySegmentEx_1__ctor_m1787516805(__this, ___array, ___offset, ___count, method) ((  void (*) (ArraySegmentEx_1_t1729348146 *, ObjectU5BU5D_t11523773*, int32_t, int32_t, const MethodInfo*))ArraySegmentEx_1__ctor_m1787516805_gshared)(__this, ___array, ___offset, ___count, method)
// T[] SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>::get_Array()
extern "C"  ObjectU5BU5D_t11523773* ArraySegmentEx_1_get_Array_m1396641634_gshared (ArraySegmentEx_1_t1729348146 * __this, const MethodInfo* method);
#define ArraySegmentEx_1_get_Array_m1396641634(__this, method) ((  ObjectU5BU5D_t11523773* (*) (ArraySegmentEx_1_t1729348146 *, const MethodInfo*))ArraySegmentEx_1_get_Array_m1396641634_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>::set_Array(T[])
extern "C"  void ArraySegmentEx_1_set_Array_m4249035723_gshared (ArraySegmentEx_1_t1729348146 * __this, ObjectU5BU5D_t11523773* ___value, const MethodInfo* method);
#define ArraySegmentEx_1_set_Array_m4249035723(__this, ___value, method) ((  void (*) (ArraySegmentEx_1_t1729348146 *, ObjectU5BU5D_t11523773*, const MethodInfo*))ArraySegmentEx_1_set_Array_m4249035723_gshared)(__this, ___value, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>::get_Count()
extern "C"  int32_t ArraySegmentEx_1_get_Count_m2013480191_gshared (ArraySegmentEx_1_t1729348146 * __this, const MethodInfo* method);
#define ArraySegmentEx_1_get_Count_m2013480191(__this, method) ((  int32_t (*) (ArraySegmentEx_1_t1729348146 *, const MethodInfo*))ArraySegmentEx_1_get_Count_m2013480191_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>::set_Count(System.Int32)
extern "C"  void ArraySegmentEx_1_set_Count_m85630872_gshared (ArraySegmentEx_1_t1729348146 * __this, int32_t ___value, const MethodInfo* method);
#define ArraySegmentEx_1_set_Count_m85630872(__this, ___value, method) ((  void (*) (ArraySegmentEx_1_t1729348146 *, int32_t, const MethodInfo*))ArraySegmentEx_1_set_Count_m85630872_gshared)(__this, ___value, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>::get_Offset()
extern "C"  int32_t ArraySegmentEx_1_get_Offset_m1904439301_gshared (ArraySegmentEx_1_t1729348146 * __this, const MethodInfo* method);
#define ArraySegmentEx_1_get_Offset_m1904439301(__this, method) ((  int32_t (*) (ArraySegmentEx_1_t1729348146 *, const MethodInfo*))ArraySegmentEx_1_get_Offset_m1904439301_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>::set_Offset(System.Int32)
extern "C"  void ArraySegmentEx_1_set_Offset_m1873908494_gshared (ArraySegmentEx_1_t1729348146 * __this, int32_t ___value, const MethodInfo* method);
#define ArraySegmentEx_1_set_Offset_m1873908494(__this, ___value, method) ((  void (*) (ArraySegmentEx_1_t1729348146 *, int32_t, const MethodInfo*))ArraySegmentEx_1_set_Offset_m1873908494_gshared)(__this, ___value, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>::get_From()
extern "C"  int32_t ArraySegmentEx_1_get_From_m1677447580_gshared (ArraySegmentEx_1_t1729348146 * __this, const MethodInfo* method);
#define ArraySegmentEx_1_get_From_m1677447580(__this, method) ((  int32_t (*) (ArraySegmentEx_1_t1729348146 *, const MethodInfo*))ArraySegmentEx_1_get_From_m1677447580_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>::set_From(System.Int32)
extern "C"  void ArraySegmentEx_1_set_From_m4241176613_gshared (ArraySegmentEx_1_t1729348146 * __this, int32_t ___value, const MethodInfo* method);
#define ArraySegmentEx_1_set_From_m4241176613(__this, ___value, method) ((  void (*) (ArraySegmentEx_1_t1729348146 *, int32_t, const MethodInfo*))ArraySegmentEx_1_set_From_m4241176613_gshared)(__this, ___value, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>::get_To()
extern "C"  int32_t ArraySegmentEx_1_get_To_m1043497069_gshared (ArraySegmentEx_1_t1729348146 * __this, const MethodInfo* method);
#define ArraySegmentEx_1_get_To_m1043497069(__this, method) ((  int32_t (*) (ArraySegmentEx_1_t1729348146 *, const MethodInfo*))ArraySegmentEx_1_get_To_m1043497069_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>::set_To(System.Int32)
extern "C"  void ArraySegmentEx_1_set_To_m2196415350_gshared (ArraySegmentEx_1_t1729348146 * __this, int32_t ___value, const MethodInfo* method);
#define ArraySegmentEx_1_set_To_m2196415350(__this, ___value, method) ((  void (*) (ArraySegmentEx_1_t1729348146 *, int32_t, const MethodInfo*))ArraySegmentEx_1_set_To_m2196415350_gshared)(__this, ___value, method)
