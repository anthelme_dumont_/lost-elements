﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SigninController
struct SigninController_t415518974;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SigninController::.ctor()
extern "C"  void SigninController__ctor_m3717009618 (SigninController_t415518974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SigninController::.cctor()
extern "C"  void SigninController__cctor_m3076052251 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SigninController::start()
extern "C"  void SigninController_start_m999494130 (SigninController_t415518974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SigninController::setPlayerName(System.String)
extern "C"  void SigninController_setPlayerName_m1770407204 (SigninController_t415518974 * __this, String_t* ____playerName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SigninController::Main()
extern "C"  void SigninController_Main_m1282323883 (SigninController_t415518974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
