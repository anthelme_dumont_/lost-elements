﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// SimpleJson.DataContractJsonSerializerStrategy
struct DataContractJsonSerializerStrategy_t3752539591;
// System.Type
struct Type_t;
// SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>
struct SafeDictionary_2_t2250466201;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.String
struct String_t;
// SimpleJson.JsonArray
struct JsonArray_t2769828783;
// SimpleJson.JsonObject
struct JsonObject_t4190420965;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1434320288;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct KeyValuePair_2U5BU5D_t2880731747;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct IEnumerator_1_t3446442070;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// SimpleJson.PocoJsonSerializerStrategy
struct PocoJsonSerializerStrategy_t1373207638;
// System.Enum
struct Enum_t2778772662;
// SimpleJson.Reflection.CacheResolver
struct CacheResolver_t2193954381;
// SimpleJson.Reflection.MemberMapLoader
struct MemberMapLoader_t3073810362;
// System.Reflection.Emit.DynamicMethod
struct DynamicMethod_t2778460557;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// SimpleJson.Reflection.GetHandler
struct GetHandler_t3377211385;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// SimpleJson.Reflection.SetHandler
struct SetHandler_t1019049325;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// SimpleJson.Reflection.CacheResolver/CtorDelegate
struct CtorDelegate_t2720072665;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// SimpleJson.Reflection.CacheResolver/MemberMap
struct MemberMap_t3346170946;
// System.Attribute
struct Attribute_t498693649;
// SimpleJson.IJsonSerializerStrategy
struct IJsonSerializerStrategy_t4110884692;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t3650470111;
// System.Char[]
struct CharU5BU5D_t3416858730;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// System.Collections.IEnumerable
struct IEnumerable_t287189635;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "SimpleJson_U3CModuleU3E86524790.h"
#include "SimpleJson_U3CModuleU3E86524790MethodDeclarations.h"
#include "SimpleJson_SimpleJson_DataContractJsonSerializerSt3752539591.h"
#include "SimpleJson_SimpleJson_DataContractJsonSerializerSt3752539591MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "SimpleJson_SimpleJson_PocoJsonSerializerStrategy1373207638MethodDeclarations.h"
#include "SimpleJson_SimpleJson_Reflection_MemberMapLoader3073810362MethodDeclarations.h"
#include "SimpleJson_SimpleJson_Reflection_CacheResolver2193954381MethodDeclarations.h"
#include "SimpleJson_SimpleJson_PocoJsonSerializerStrategy1373207638.h"
#include "mscorlib_System_Type2779229935.h"
#include "SimpleJson_SimpleJson_Reflection_SafeDictionary_2_2250466201.h"
#include "SimpleJson_SimpleJson_Reflection_MemberMapLoader3073810362.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "SimpleJson_SimpleJson_Reflection_CacheResolver2193954381.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "SimpleJson_SimpleJson_Reflection_ReflectionUtils3367798763MethodDeclarations.h"
#include "SimpleJson_SimpleJson_Reflection_CacheResolver_Mem3346170946MethodDeclarations.h"
#include "SimpleJson_SimpleJson_Reflection_SafeDictionary_2_2250466201MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "mscorlib_System_Attribute498693649.h"
#include "mscorlib_System_Reflection_BindingFlags2090192240.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "SimpleJson_SimpleJson_Reflection_CacheResolver_Mem3346170946.h"
#include "System_Runtime_Serialization_System_Runtime_Serial3625462001MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "System_Runtime_Serialization_System_Runtime_Serial3625462001.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814MethodDeclarations.h"
#include "SimpleJson_SimpleJson_DateTimeUtils111370036.h"
#include "SimpleJson_SimpleJson_DateTimeUtils111370036MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_DateTime339033936.h"
#include "System_Core_System_TimeZoneInfo4131446812MethodDeclarations.h"
#include "System_Core_System_TimeZoneInfo4131446812.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_DateTime339033936MethodDeclarations.h"
#include "mscorlib_System_DateTimeKind3550648708.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "SimpleJson_SimpleJson_JsonArray2769828783.h"
#include "SimpleJson_SimpleJson_JsonArray2769828783MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "SimpleJson_SimpleJson_SimpleJson2437938008MethodDeclarations.h"
#include "SimpleJson_SimpleJson_JsonObject4190420965.h"
#include "SimpleJson_SimpleJson_JsonObject4190420965MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke503112308.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va101974122.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21963335622.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21963335622MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2241832265.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Nullable935284502MethodDeclarations.h"
#include "mscorlib_System_Int642847414882MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042MethodDeclarations.h"
#include "mscorlib_System_Guid2778838590MethodDeclarations.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_177432852MethodDeclarations.h"
#include "SimpleJson_SimpleJson_Reflection_SetHandler1019049325MethodDeclarations.h"
#include "mscorlib_System_Activator690001546MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_177432852.h"
#include "mscorlib_System_Guid2778838590.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042.h"
#include "mscorlib_System_Globalization_DateTimeStyles3832294611.h"
#include "mscorlib_System_Double534516614.h"
#include "SimpleJson_SimpleJson_Reflection_SetHandler1019049325.h"
#include "mscorlib_System_Enum2778772662.h"
#include "System_System_Uri2776692961.h"
#include "SimpleJson_SimpleJson_Reflection_GetHandler3377211385MethodDeclarations.h"
#include "SimpleJson_SimpleJson_Reflection_GetHandler3377211385.h"
#include "SimpleJson_SimpleJson_Reflection_SafeDictionary_2_1629611267MethodDeclarations.h"
#include "SimpleJson_SimpleJson_Reflection_SafeDictionary_2_1629611267.h"
#include "SimpleJson_SimpleJson_Reflection_SafeDictionary_2_2099217731MethodDeclarations.h"
#include "SimpleJson_SimpleJson_Reflection_CacheResolver_Cto2720072665MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_DynamicMethod2778460557MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "SimpleJson_SimpleJson_Reflection_CacheResolver_Cto2720072665.h"
#include "mscorlib_System_Reflection_Emit_DynamicMethod2778460557.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator3869071517.h"
#include "mscorlib_System_Reflection_ConstructorInfo3542137334.h"
#include "SimpleJson_SimpleJson_Reflection_SafeDictionary_2_2099217731.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator3869071517MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_LocalBuilder625143165.h"
#include "mscorlib_System_Reflection_Emit_OpCodes2031828562.h"
#include "mscorlib_System_Reflection_Emit_OpCodes2031828562MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_OpCode4028977979.h"
#include "mscorlib_System_Reflection_Binder4180926488.h"
#include "mscorlib_System_Reflection_ParameterModifier500203470.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "mscorlib_System_Reflection_Module206139610.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "SimpleJson_SimpleJson_Reflection_ReflectionUtils3367798763.h"
#include "mscorlib_System_Attribute498693649MethodDeclarations.h"
#include "SimpleJson_SimpleJson_SimpleJson2437938008.h"
#include "mscorlib_System_Runtime_Serialization_Serialization731558744MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization731558744.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_UInt32985925326MethodDeclarations.h"
#include "mscorlib_System_Char2778706699MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_Globalization_NumberStyles3988678145.h"
#include "mscorlib_System_Double534516614MethodDeclarations.h"
#include "mscorlib_System_StringComparison1653470895.h"
#include "mscorlib_System_UInt64985925421MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_Decimal1688557254MethodDeclarations.h"
#include "mscorlib_System_Single958209021MethodDeclarations.h"
#include "mscorlib_System_UInt64985925421.h"
#include "mscorlib_System_Decimal1688557254.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_SByte2855346064.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_UInt16985925268.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleJson.DataContractJsonSerializerStrategy::.ctor()
extern TypeInfo* PocoJsonSerializerStrategy_t1373207638_il2cpp_TypeInfo_var;
extern TypeInfo* MemberMapLoader_t3073810362_il2cpp_TypeInfo_var;
extern TypeInfo* CacheResolver_t2193954381_il2cpp_TypeInfo_var;
extern const uint32_t DataContractJsonSerializerStrategy__ctor_m696556770_MetadataUsageId;
extern "C"  void DataContractJsonSerializerStrategy__ctor_m696556770 (DataContractJsonSerializerStrategy_t3752539591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataContractJsonSerializerStrategy__ctor_m696556770_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PocoJsonSerializerStrategy_t1373207638_il2cpp_TypeInfo_var);
		PocoJsonSerializerStrategy__ctor_m1303007027(__this, /*hidden argument*/NULL);
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)GetVirtualMethodInfo(__this, 6));
		MemberMapLoader_t3073810362 * L_1 = (MemberMapLoader_t3073810362 *)il2cpp_codegen_object_new(MemberMapLoader_t3073810362_il2cpp_TypeInfo_var);
		MemberMapLoader__ctor_m4182933641(L_1, __this, L_0, /*hidden argument*/NULL);
		CacheResolver_t2193954381 * L_2 = (CacheResolver_t2193954381 *)il2cpp_codegen_object_new(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		CacheResolver__ctor_m3244435910(L_2, L_1, /*hidden argument*/NULL);
		((PocoJsonSerializerStrategy_t1373207638 *)__this)->set_CacheResolver_0(L_2);
		return;
	}
}
// System.Void SimpleJson.DataContractJsonSerializerStrategy::BuildMap(System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>)
extern const Il2CppType* DataContractAttribute_t2606959609_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* MemberMap_t3346170946_il2cpp_TypeInfo_var;
extern const MethodInfo* SafeDictionary_2_Add_m2940163100_MethodInfo_var;
extern const uint32_t DataContractJsonSerializerStrategy_BuildMap_m2479422991_MetadataUsageId;
extern "C"  void DataContractJsonSerializerStrategy_BuildMap_m2479422991 (DataContractJsonSerializerStrategy_t3752539591 * __this, Type_t * ___type, SafeDictionary_2_t2250466201 * ___map, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataContractJsonSerializerStrategy_BuildMap_m2479422991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	PropertyInfo_t * V_2 = NULL;
	FieldInfo_t * V_3 = NULL;
	PropertyInfoU5BU5D_t1348579340* V_4 = NULL;
	int32_t V_5 = 0;
	FieldInfoU5BU5D_t1144794227* V_6 = NULL;
	int32_t V_7 = 0;
	{
		Type_t * L_0 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DataContractAttribute_t2606959609_0_0_0_var), /*hidden argument*/NULL);
		Attribute_t498693649 * L_2 = ReflectionUtils_GetAttribute_m3007953666(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)((((Il2CppObject*)(Attribute_t498693649 *)L_2) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_0;
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		Type_t * L_4 = ___type;
		SafeDictionary_2_t2250466201 * L_5 = ___map;
		PocoJsonSerializerStrategy_BuildMap_m2906231328(__this, L_4, L_5, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		Type_t * L_6 = ___type;
		NullCheck(L_6);
		PropertyInfoU5BU5D_t1348579340* L_7 = VirtFuncInvoker1< PropertyInfoU5BU5D_t1348579340*, int32_t >::Invoke(55 /* System.Reflection.PropertyInfo[] System.Type::GetProperties(System.Reflection.BindingFlags) */, L_6, ((int32_t)52));
		V_4 = L_7;
		V_5 = 0;
		goto IL_0055;
	}

IL_0032:
	{
		PropertyInfoU5BU5D_t1348579340* L_8 = V_4;
		int32_t L_9 = V_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		V_2 = ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
		PropertyInfo_t * L_11 = V_2;
		bool L_12 = DataContractJsonSerializerStrategy_CanAdd_m308335317(NULL /*static, unused*/, L_11, (&V_1), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004f;
		}
	}
	{
		SafeDictionary_2_t2250466201 * L_13 = ___map;
		String_t* L_14 = V_1;
		PropertyInfo_t * L_15 = V_2;
		MemberMap_t3346170946 * L_16 = (MemberMap_t3346170946 *)il2cpp_codegen_object_new(MemberMap_t3346170946_il2cpp_TypeInfo_var);
		MemberMap__ctor_m3239711532(L_16, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		SafeDictionary_2_Add_m2940163100(L_13, L_14, L_16, /*hidden argument*/SafeDictionary_2_Add_m2940163100_MethodInfo_var);
	}

IL_004f:
	{
		int32_t L_17 = V_5;
		V_5 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0055:
	{
		int32_t L_18 = V_5;
		PropertyInfoU5BU5D_t1348579340* L_19 = V_4;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0032;
		}
	}
	{
		Type_t * L_20 = ___type;
		NullCheck(L_20);
		FieldInfoU5BU5D_t1144794227* L_21 = VirtFuncInvoker1< FieldInfoU5BU5D_t1144794227*, int32_t >::Invoke(48 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_20, ((int32_t)52));
		V_6 = L_21;
		V_7 = 0;
		goto IL_008f;
	}

IL_006c:
	{
		FieldInfoU5BU5D_t1144794227* L_22 = V_6;
		int32_t L_23 = V_7;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		V_3 = ((L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24)));
		FieldInfo_t * L_25 = V_3;
		bool L_26 = DataContractJsonSerializerStrategy_CanAdd_m308335317(NULL /*static, unused*/, L_25, (&V_1), /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0089;
		}
	}
	{
		SafeDictionary_2_t2250466201 * L_27 = ___map;
		String_t* L_28 = V_1;
		FieldInfo_t * L_29 = V_3;
		MemberMap_t3346170946 * L_30 = (MemberMap_t3346170946 *)il2cpp_codegen_object_new(MemberMap_t3346170946_il2cpp_TypeInfo_var);
		MemberMap__ctor_m3335798619(L_30, L_29, /*hidden argument*/NULL);
		NullCheck(L_27);
		SafeDictionary_2_Add_m2940163100(L_27, L_28, L_30, /*hidden argument*/SafeDictionary_2_Add_m2940163100_MethodInfo_var);
	}

IL_0089:
	{
		int32_t L_31 = V_7;
		V_7 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_008f:
	{
		int32_t L_32 = V_7;
		FieldInfoU5BU5D_t1144794227* L_33 = V_6;
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length)))))))
		{
			goto IL_006c;
		}
	}
	{
		return;
	}
}
// System.Boolean SimpleJson.DataContractJsonSerializerStrategy::CanAdd(System.Reflection.MemberInfo,System.String&)
extern const Il2CppType* IgnoreDataMemberAttribute_t583091647_0_0_0_var;
extern const Il2CppType* DataMemberAttribute_t3625462001_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* DataMemberAttribute_t3625462001_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DataContractJsonSerializerStrategy_CanAdd_m308335317_MetadataUsageId;
extern "C"  bool DataContractJsonSerializerStrategy_CanAdd_m308335317 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___info, String_t** ___jsonKey, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataContractJsonSerializerStrategy_CanAdd_m308335317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataMemberAttribute_t3625462001 * V_0 = NULL;
	String_t** G_B6_0 = NULL;
	String_t** G_B5_0 = NULL;
	String_t* G_B7_0 = NULL;
	String_t** G_B7_1 = NULL;
	{
		String_t** L_0 = ___jsonKey;
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)NULL);
		MemberInfo_t * L_1 = ___info;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IgnoreDataMemberAttribute_t583091647_0_0_0_var), /*hidden argument*/NULL);
		Attribute_t498693649 * L_3 = ReflectionUtils_GetAttribute_m2472672625(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		MemberInfo_t * L_4 = ___info;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DataMemberAttribute_t3625462001_0_0_0_var), /*hidden argument*/NULL);
		Attribute_t498693649 * L_6 = ReflectionUtils_GetAttribute_m2472672625(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = ((DataMemberAttribute_t3625462001 *)CastclassSealed(L_6, DataMemberAttribute_t3625462001_il2cpp_TypeInfo_var));
		DataMemberAttribute_t3625462001 * L_7 = V_0;
		if (L_7)
		{
			goto IL_0032;
		}
	}
	{
		return (bool)0;
	}

IL_0032:
	{
		String_t** L_8 = ___jsonKey;
		DataMemberAttribute_t3625462001 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = DataMemberAttribute_get_Name_m2466648376(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		G_B5_0 = L_8;
		if (L_11)
		{
			G_B6_0 = L_8;
			goto IL_0048;
		}
	}
	{
		DataMemberAttribute_t3625462001 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = DataMemberAttribute_get_Name_m2466648376(L_12, /*hidden argument*/NULL);
		G_B7_0 = L_13;
		G_B7_1 = G_B5_0;
		goto IL_004e;
	}

IL_0048:
	{
		MemberInfo_t * L_14 = ___info;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_14);
		G_B7_0 = L_15;
		G_B7_1 = G_B6_0;
	}

IL_004e:
	{
		*((Il2CppObject **)(G_B7_1)) = (Il2CppObject *)G_B7_0;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(G_B7_1), (Il2CppObject *)G_B7_0);
		return (bool)1;
	}
}
// System.TimeSpan SimpleJson.DateTimeUtils::GetUtcOffset(System.DateTime)
extern TypeInfo* TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var;
extern const uint32_t DateTimeUtils_GetUtcOffset_m816805979_MetadataUsageId;
extern "C"  TimeSpan_t763862892  DateTimeUtils_GetUtcOffset_m816805979 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___d, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DateTimeUtils_GetUtcOffset_m816805979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZoneInfo_t4131446812_il2cpp_TypeInfo_var);
		TimeZoneInfo_t4131446812 * L_0 = TimeZoneInfo_get_Local_m1804805602(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t339033936  L_1 = ___d;
		NullCheck(L_0);
		TimeSpan_t763862892  L_2 = TimeZoneInfo_GetUtcOffset_m2479866789(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int64 SimpleJson.DateTimeUtils::ToUniversalTicks(System.DateTime)
extern TypeInfo* DateTimeUtils_t111370036_il2cpp_TypeInfo_var;
extern const uint32_t DateTimeUtils_ToUniversalTicks_m4026444844_MetadataUsageId;
extern "C"  int64_t DateTimeUtils_ToUniversalTicks_m4026444844 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___dateTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DateTimeUtils_ToUniversalTicks_m4026444844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = DateTime_get_Kind_m3496013602((&___dateTime), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		int64_t L_1 = DateTime_get_Ticks_m386468226((&___dateTime), /*hidden argument*/NULL);
		return L_1;
	}

IL_0012:
	{
		DateTime_t339033936  L_2 = ___dateTime;
		DateTime_t339033936  L_3 = ___dateTime;
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeUtils_t111370036_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_4 = DateTimeUtils_GetUtcOffset_m816805979(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int64_t L_5 = DateTimeUtils_ToUniversalTicks_m368464002(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int64 SimpleJson.DateTimeUtils::ToUniversalTicks(System.DateTime,System.TimeSpan)
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t DateTimeUtils_ToUniversalTicks_m368464002_MetadataUsageId;
extern "C"  int64_t DateTimeUtils_ToUniversalTicks_m368464002 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___dateTime, TimeSpan_t763862892  ___offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DateTimeUtils_ToUniversalTicks_m368464002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		int32_t L_0 = DateTime_get_Kind_m3496013602((&___dateTime), /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0024;
		}
	}
	{
		DateTime_t339033936  L_1 = ___dateTime;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_2 = ((DateTime_t339033936_StaticFields*)DateTime_t339033936_il2cpp_TypeInfo_var->static_fields)->get_MaxValue_12();
		bool L_3 = DateTime_op_Equality_m2277436664(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		DateTime_t339033936  L_4 = ___dateTime;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_5 = ((DateTime_t339033936_StaticFields*)DateTime_t339033936_il2cpp_TypeInfo_var->static_fields)->get_MinValue_13();
		bool L_6 = DateTime_op_Equality_m2277436664(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002c;
		}
	}

IL_0024:
	{
		int64_t L_7 = DateTime_get_Ticks_m386468226((&___dateTime), /*hidden argument*/NULL);
		return L_7;
	}

IL_002c:
	{
		int64_t L_8 = DateTime_get_Ticks_m386468226((&___dateTime), /*hidden argument*/NULL);
		int64_t L_9 = TimeSpan_get_Ticks_m315930342((&___offset), /*hidden argument*/NULL);
		V_0 = ((int64_t)((int64_t)L_8-(int64_t)L_9));
		int64_t L_10 = V_0;
		if ((((int64_t)L_10) <= ((int64_t)((int64_t)3155378975999999999LL))))
		{
			goto IL_0052;
		}
	}
	{
		return ((int64_t)3155378975999999999LL);
	}

IL_0052:
	{
		int64_t L_11 = V_0;
		if ((((int64_t)L_11) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_005a;
		}
	}
	{
		return (((int64_t)((int64_t)0)));
	}

IL_005a:
	{
		int64_t L_12 = V_0;
		return L_12;
	}
}
// System.Int64 SimpleJson.DateTimeUtils::ConvertDateTimeToJavaScriptTicks(System.DateTime)
extern TypeInfo* DateTimeUtils_t111370036_il2cpp_TypeInfo_var;
extern const uint32_t DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m2628017042_MetadataUsageId;
extern "C"  int64_t DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m2628017042 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___dateTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m2628017042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t339033936  L_0 = ___dateTime;
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeUtils_t111370036_il2cpp_TypeInfo_var);
		int64_t L_1 = DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m2435522987(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int64 SimpleJson.DateTimeUtils::ConvertDateTimeToJavaScriptTicks(System.DateTime,System.Boolean)
extern TypeInfo* DateTimeUtils_t111370036_il2cpp_TypeInfo_var;
extern const uint32_t DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m2435522987_MetadataUsageId;
extern "C"  int64_t DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m2435522987 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___dateTime, bool ___convertToUtc, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m2435522987_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	int64_t G_B3_0 = 0;
	{
		bool L_0 = ___convertToUtc;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		int64_t L_1 = DateTime_get_Ticks_m386468226((&___dateTime), /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0012;
	}

IL_000c:
	{
		DateTime_t339033936  L_2 = ___dateTime;
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeUtils_t111370036_il2cpp_TypeInfo_var);
		int64_t L_3 = DateTimeUtils_ToUniversalTicks_m4026444844(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0012:
	{
		V_0 = G_B3_0;
		int64_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeUtils_t111370036_il2cpp_TypeInfo_var);
		int64_t L_5 = DateTimeUtils_UniversialTicksToJavaScriptTicks_m2404214532(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int64 SimpleJson.DateTimeUtils::UniversialTicksToJavaScriptTicks(System.Int64)
extern TypeInfo* DateTimeUtils_t111370036_il2cpp_TypeInfo_var;
extern const uint32_t DateTimeUtils_UniversialTicksToJavaScriptTicks_m2404214532_MetadataUsageId;
extern "C"  int64_t DateTimeUtils_UniversialTicksToJavaScriptTicks_m2404214532 (Il2CppObject * __this /* static, unused */, int64_t ___universialTicks, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DateTimeUtils_UniversialTicksToJavaScriptTicks_m2404214532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		int64_t L_0 = ___universialTicks;
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeUtils_t111370036_il2cpp_TypeInfo_var);
		int64_t L_1 = ((DateTimeUtils_t111370036_StaticFields*)DateTimeUtils_t111370036_il2cpp_TypeInfo_var->static_fields)->get_InitialJavaScriptDateTicks_0();
		V_0 = ((int64_t)((int64_t)((int64_t)((int64_t)L_0-(int64_t)L_1))/(int64_t)(((int64_t)((int64_t)((int32_t)10000))))));
		int64_t L_2 = V_0;
		return L_2;
	}
}
// System.DateTime SimpleJson.DateTimeUtils::ConvertJavaScriptTicksToDateTime(System.Int64)
extern TypeInfo* DateTimeUtils_t111370036_il2cpp_TypeInfo_var;
extern const uint32_t DateTimeUtils_ConvertJavaScriptTicksToDateTime_m4164648278_MetadataUsageId;
extern "C"  DateTime_t339033936  DateTimeUtils_ConvertJavaScriptTicksToDateTime_m4164648278 (Il2CppObject * __this /* static, unused */, int64_t ___javaScriptTicks, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DateTimeUtils_ConvertJavaScriptTicksToDateTime_m4164648278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int64_t L_0 = ___javaScriptTicks;
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeUtils_t111370036_il2cpp_TypeInfo_var);
		int64_t L_1 = ((DateTimeUtils_t111370036_StaticFields*)DateTimeUtils_t111370036_il2cpp_TypeInfo_var->static_fields)->get_InitialJavaScriptDateTicks_0();
		DateTime__ctor_m2747883466((&V_0), ((int64_t)((int64_t)((int64_t)((int64_t)L_0*(int64_t)(((int64_t)((int64_t)((int32_t)10000))))))+(int64_t)L_1)), 1, /*hidden argument*/NULL);
		DateTime_t339033936  L_2 = V_0;
		return L_2;
	}
}
// System.Void SimpleJson.DateTimeUtils::.cctor()
extern TypeInfo* DateTimeUtils_t111370036_il2cpp_TypeInfo_var;
extern const uint32_t DateTimeUtils__cctor_m2619914312_MetadataUsageId;
extern "C"  void DateTimeUtils__cctor_m2619914312 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DateTimeUtils__cctor_m2619914312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((DateTimeUtils_t111370036_StaticFields*)DateTimeUtils_t111370036_il2cpp_TypeInfo_var->static_fields)->set_InitialJavaScriptDateTicks_0(((int64_t)621355968000000000LL));
		return;
	}
}
// System.Void SimpleJson.JsonArray::.ctor()
extern TypeInfo* List_1_t1634065389_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3120258633_MethodInfo_var;
extern const uint32_t JsonArray__ctor_m7376362_MetadataUsageId;
extern "C"  void JsonArray__ctor_m7376362 (JsonArray_t2769828783 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonArray__ctor_m7376362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t1634065389_il2cpp_TypeInfo_var);
		List_1__ctor_m3120258633(__this, /*hidden argument*/List_1__ctor_m3120258633_MethodInfo_var);
		return;
	}
}
// System.String SimpleJson.JsonArray::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JsonArray_ToString_m3266477609_MetadataUsageId;
extern "C"  String_t* JsonArray_ToString_m3266477609 (JsonArray_t2769828783 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonArray_ToString_m3266477609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = SimpleJson_SerializeObject_m2690171523(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000f:
	{
		return G_B2_0;
	}
}
// System.Void SimpleJson.JsonObject::Add(System.String,System.Object)
extern "C"  void JsonObject_Add_m580162093 (JsonObject_t4190420965 * __this, String_t* ___key, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		String_t* L_1 = ___key;
		Il2CppObject * L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1) */, L_0, L_1, L_2);
		return;
	}
}
// System.Boolean SimpleJson.JsonObject::ContainsKey(System.String)
extern "C"  bool JsonObject_ContainsKey_m3991490132 (JsonObject_t4190420965 * __this, String_t* ___key, const MethodInfo* method)
{
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		String_t* L_1 = ___key;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<System.String> SimpleJson.JsonObject::get_Keys()
extern const MethodInfo* Dictionary_2_get_Keys_m114819208_MethodInfo_var;
extern const uint32_t JsonObject_get_Keys_m2207953707_MetadataUsageId;
extern "C"  Il2CppObject* JsonObject_get_Keys_m2207953707 (JsonObject_t4190420965 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_get_Keys_m2207953707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		NullCheck(L_0);
		KeyCollection_t503112308 * L_1 = Dictionary_2_get_Keys_m114819208(L_0, /*hidden argument*/Dictionary_2_get_Keys_m114819208_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean SimpleJson.JsonObject::TryGetValue(System.String,System.Object&)
extern "C"  bool JsonObject_TryGetValue_m3413057088 (JsonObject_t4190420965 * __this, String_t* ___key, Il2CppObject ** ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		String_t* L_1 = ___key;
		Il2CppObject ** L_2 = ___value;
		NullCheck(L_0);
		bool L_3 = VirtFuncInvoker2< bool, String_t*, Il2CppObject ** >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Collections.Generic.ICollection`1<System.Object> SimpleJson.JsonObject::get_Values()
extern const MethodInfo* Dictionary_2_get_Values_m2331734856_MethodInfo_var;
extern const uint32_t JsonObject_get_Values_m123951851_MetadataUsageId;
extern "C"  Il2CppObject* JsonObject_get_Values_m123951851 (JsonObject_t4190420965 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_get_Values_m123951851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		NullCheck(L_0);
		ValueCollection_t101974122 * L_1 = Dictionary_2_get_Values_m2331734856(L_0, /*hidden argument*/Dictionary_2_get_Values_m2331734856_MethodInfo_var);
		return L_1;
	}
}
// System.Object SimpleJson.JsonObject::get_Item(System.String)
extern "C"  Il2CppObject * JsonObject_get_Item_m1320065883 (JsonObject_t4190420965 * __this, String_t* ___key, const MethodInfo* method)
{
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		String_t* L_1 = ___key;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void SimpleJson.JsonObject::set_Item(System.String,System.Object)
extern "C"  void JsonObject_set_Item_m3024313024 (JsonObject_t4190420965 * __this, String_t* ___key, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		String_t* L_1 = ___key;
		Il2CppObject * L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void SimpleJson.JsonObject::Add(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern const MethodInfo* KeyValuePair_2_get_Key_m1817333386_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2539855901_MethodInfo_var;
extern const uint32_t JsonObject_Add_m4255310104_MetadataUsageId;
extern "C"  void JsonObject_Add_m4255310104 (JsonObject_t4190420965 * __this, KeyValuePair_2_t1963335622  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_Add_m4255310104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		String_t* L_1 = KeyValuePair_2_get_Key_m1817333386((&___item), /*hidden argument*/KeyValuePair_2_get_Key_m1817333386_MethodInfo_var);
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m2539855901((&___item), /*hidden argument*/KeyValuePair_2_get_Value_m2539855901_MethodInfo_var);
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void SimpleJson.JsonObject::Clear()
extern "C"  void JsonObject_Clear_m1157327023 (JsonObject_t4190420965 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Clear() */, L_0);
		return;
	}
}
// System.Boolean SimpleJson.JsonObject::Contains(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern const MethodInfo* KeyValuePair_2_get_Key_m1817333386_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2539855901_MethodInfo_var;
extern const uint32_t JsonObject_Contains_m1056266920_MetadataUsageId;
extern "C"  bool JsonObject_Contains_m1056266920 (JsonObject_t4190420965 * __this, KeyValuePair_2_t1963335622  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_Contains_m1056266920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		String_t* L_1 = KeyValuePair_2_get_Key_m1817333386((&___item), /*hidden argument*/KeyValuePair_2_get_Key_m1817333386_MethodInfo_var);
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		Dictionary_2_t2474804324 * L_3 = __this->get__members_0();
		String_t* L_4 = KeyValuePair_2_get_Key_m1817333386((&___item), /*hidden argument*/KeyValuePair_2_get_Key_m1817333386_MethodInfo_var);
		NullCheck(L_3);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_3, L_4);
		Il2CppObject * L_6 = KeyValuePair_2_get_Value_m2539855901((&___item), /*hidden argument*/KeyValuePair_2_get_Value_m2539855901_MethodInfo_var);
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_5) == ((Il2CppObject*)(Il2CppObject *)L_6))? 1 : 0);
	}

IL_0030:
	{
		return (bool)0;
	}
}
// System.Void SimpleJson.JsonObject::CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[],System.Int32)
extern TypeInfo* IEnumerator_1_t3446442070_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t JsonObject_CopyTo_m479212916_MetadataUsageId;
extern "C"  void JsonObject_CopyTo_m479212916 (JsonObject_t4190420965 * __this, KeyValuePair_2U5BU5D_t2880731747* ___array, int32_t ___arrayIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_CopyTo_m479212916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t1963335622  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject* V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = JsonObject_get_Count_m2158435374(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Il2CppObject* L_1 = JsonObject_GetEnumerator_m3037445227(__this, /*hidden argument*/NULL);
		V_2 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0033;
		}

IL_0010:
		{
			Il2CppObject* L_2 = V_2;
			NullCheck(L_2);
			KeyValuePair_2_t1963335622  L_3 = InterfaceFuncInvoker0< KeyValuePair_2_t1963335622  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::get_Current() */, IEnumerator_1_t3446442070_il2cpp_TypeInfo_var, L_2);
			V_1 = L_3;
			KeyValuePair_2U5BU5D_t2880731747* L_4 = ___array;
			int32_t L_5 = ___arrayIndex;
			int32_t L_6 = L_5;
			___arrayIndex = ((int32_t)((int32_t)L_6+(int32_t)1));
			NullCheck(L_4);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_6);
			KeyValuePair_2_t1963335622  L_7 = V_1;
			(*(KeyValuePair_2_t1963335622 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
			int32_t L_8 = V_0;
			int32_t L_9 = ((int32_t)((int32_t)L_8-(int32_t)1));
			V_0 = L_9;
			if ((((int32_t)L_9) > ((int32_t)0)))
			{
				goto IL_0033;
			}
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x47, FINALLY_003d);
		}

IL_0033:
		{
			Il2CppObject* L_10 = V_2;
			NullCheck(L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_10);
			if (L_11)
			{
				goto IL_0010;
			}
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x47, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_12 = V_2;
			if (!L_12)
			{
				goto IL_0046;
			}
		}

IL_0040:
		{
			Il2CppObject* L_13 = V_2;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_13);
		}

IL_0046:
		{
			IL2CPP_END_FINALLY(61)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0047:
	{
		return;
	}
}
// System.Int32 SimpleJson.JsonObject::get_Count()
extern "C"  int32_t JsonObject_get_Count_m2158435374 (JsonObject_t4190420965 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Count() */, L_0);
		return L_1;
	}
}
// System.Boolean SimpleJson.JsonObject::get_IsReadOnly()
extern "C"  bool JsonObject_get_IsReadOnly_m3272693801 (JsonObject_t4190420965 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean SimpleJson.JsonObject::Remove(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern const MethodInfo* KeyValuePair_2_get_Key_m1817333386_MethodInfo_var;
extern const uint32_t JsonObject_Remove_m2553868493_MetadataUsageId;
extern "C"  bool JsonObject_Remove_m2553868493 (JsonObject_t4190420965 * __this, KeyValuePair_2_t1963335622  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_Remove_m2553868493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		String_t* L_1 = KeyValuePair_2_get_Key_m1817333386((&___item), /*hidden argument*/KeyValuePair_2_get_Key_m1817333386_MethodInfo_var);
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(34 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::Remove(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> SimpleJson.JsonObject::GetEnumerator()
extern TypeInfo* Enumerator_t2241832265_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m908826428_MethodInfo_var;
extern const uint32_t JsonObject_GetEnumerator_m3037445227_MetadataUsageId;
extern "C"  Il2CppObject* JsonObject_GetEnumerator_m3037445227 (JsonObject_t4190420965 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_GetEnumerator_m3037445227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		NullCheck(L_0);
		Enumerator_t2241832265  L_1 = Dictionary_2_GetEnumerator_m908826428(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m908826428_MethodInfo_var);
		Enumerator_t2241832265  L_2 = L_1;
		Il2CppObject * L_3 = Box(Enumerator_t2241832265_il2cpp_TypeInfo_var, &L_2);
		return (Il2CppObject*)L_3;
	}
}
// System.Collections.IEnumerator SimpleJson.JsonObject::System.Collections.IEnumerable.GetEnumerator()
extern TypeInfo* Enumerator_t2241832265_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m908826428_MethodInfo_var;
extern const uint32_t JsonObject_System_Collections_IEnumerable_GetEnumerator_m2129236293_MetadataUsageId;
extern "C"  Il2CppObject * JsonObject_System_Collections_IEnumerable_GetEnumerator_m2129236293 (JsonObject_t4190420965 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_System_Collections_IEnumerable_GetEnumerator_m2129236293_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get__members_0();
		NullCheck(L_0);
		Enumerator_t2241832265  L_1 = Dictionary_2_GetEnumerator_m908826428(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m908826428_MethodInfo_var);
		Enumerator_t2241832265  L_2 = L_1;
		Il2CppObject * L_3 = Box(Enumerator_t2241832265_il2cpp_TypeInfo_var, &L_2);
		return (Il2CppObject *)L_3;
	}
}
// System.String SimpleJson.JsonObject::ToString()
extern "C"  String_t* JsonObject_ToString_m2916787049 (JsonObject_t4190420965 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = SimpleJson_SerializeObject_m2690171523(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void SimpleJson.JsonObject::.ctor()
extern TypeInfo* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m993803206_MethodInfo_var;
extern const uint32_t JsonObject__ctor_m3751193732_MetadataUsageId;
extern "C"  void JsonObject__ctor_m3751193732 (JsonObject_t4190420965 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject__ctor_m3751193732_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = (Dictionary_2_t2474804324 *)il2cpp_codegen_object_new(Dictionary_2_t2474804324_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m993803206(L_0, /*hidden argument*/Dictionary_2__ctor_m993803206_MethodInfo_var);
		__this->set__members_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleJson.PocoJsonSerializerStrategy::.ctor()
extern TypeInfo* MemberMapLoader_t3073810362_il2cpp_TypeInfo_var;
extern TypeInfo* CacheResolver_t2193954381_il2cpp_TypeInfo_var;
extern const uint32_t PocoJsonSerializerStrategy__ctor_m1303007027_MetadataUsageId;
extern "C"  void PocoJsonSerializerStrategy__ctor_m1303007027 (PocoJsonSerializerStrategy_t1373207638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy__ctor_m1303007027_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)GetVirtualMethodInfo(__this, 6));
		MemberMapLoader_t3073810362 * L_1 = (MemberMapLoader_t3073810362 *)il2cpp_codegen_object_new(MemberMapLoader_t3073810362_il2cpp_TypeInfo_var);
		MemberMapLoader__ctor_m4182933641(L_1, __this, L_0, /*hidden argument*/NULL);
		CacheResolver_t2193954381 * L_2 = (CacheResolver_t2193954381 *)il2cpp_codegen_object_new(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		CacheResolver__ctor_m3244435910(L_2, L_1, /*hidden argument*/NULL);
		__this->set_CacheResolver_0(L_2);
		return;
	}
}
// System.Void SimpleJson.PocoJsonSerializerStrategy::BuildMap(System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>)
extern TypeInfo* MemberMap_t3346170946_il2cpp_TypeInfo_var;
extern const MethodInfo* SafeDictionary_2_Add_m2940163100_MethodInfo_var;
extern const uint32_t PocoJsonSerializerStrategy_BuildMap_m2906231328_MetadataUsageId;
extern "C"  void PocoJsonSerializerStrategy_BuildMap_m2906231328 (PocoJsonSerializerStrategy_t1373207638 * __this, Type_t * ___type, SafeDictionary_2_t2250466201 * ___memberMaps, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy_BuildMap_m2906231328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PropertyInfo_t * V_0 = NULL;
	FieldInfo_t * V_1 = NULL;
	PropertyInfoU5BU5D_t1348579340* V_2 = NULL;
	int32_t V_3 = 0;
	FieldInfoU5BU5D_t1144794227* V_4 = NULL;
	int32_t V_5 = 0;
	{
		Type_t * L_0 = ___type;
		NullCheck(L_0);
		PropertyInfoU5BU5D_t1348579340* L_1 = VirtFuncInvoker1< PropertyInfoU5BU5D_t1348579340*, int32_t >::Invoke(55 /* System.Reflection.PropertyInfo[] System.Type::GetProperties(System.Reflection.BindingFlags) */, L_0, ((int32_t)20));
		V_2 = L_1;
		V_3 = 0;
		goto IL_0027;
	}

IL_000d:
	{
		PropertyInfoU5BU5D_t1348579340* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)));
		SafeDictionary_2_t2250466201 * L_5 = ___memberMaps;
		PropertyInfo_t * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_6);
		PropertyInfo_t * L_8 = V_0;
		MemberMap_t3346170946 * L_9 = (MemberMap_t3346170946 *)il2cpp_codegen_object_new(MemberMap_t3346170946_il2cpp_TypeInfo_var);
		MemberMap__ctor_m3239711532(L_9, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		SafeDictionary_2_Add_m2940163100(L_5, L_7, L_9, /*hidden argument*/SafeDictionary_2_Add_m2940163100_MethodInfo_var);
		int32_t L_10 = V_3;
		V_3 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_11 = V_3;
		PropertyInfoU5BU5D_t1348579340* L_12 = V_2;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		Type_t * L_13 = ___type;
		NullCheck(L_13);
		FieldInfoU5BU5D_t1144794227* L_14 = VirtFuncInvoker1< FieldInfoU5BU5D_t1144794227*, int32_t >::Invoke(48 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_13, ((int32_t)20));
		V_4 = L_14;
		V_5 = 0;
		goto IL_005a;
	}

IL_003c:
	{
		FieldInfoU5BU5D_t1144794227* L_15 = V_4;
		int32_t L_16 = V_5;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		V_1 = ((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17)));
		SafeDictionary_2_t2250466201 * L_18 = ___memberMaps;
		FieldInfo_t * L_19 = V_1;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		FieldInfo_t * L_21 = V_1;
		MemberMap_t3346170946 * L_22 = (MemberMap_t3346170946 *)il2cpp_codegen_object_new(MemberMap_t3346170946_il2cpp_TypeInfo_var);
		MemberMap__ctor_m3335798619(L_22, L_21, /*hidden argument*/NULL);
		NullCheck(L_18);
		SafeDictionary_2_Add_m2940163100(L_18, L_20, L_22, /*hidden argument*/SafeDictionary_2_Add_m2940163100_MethodInfo_var);
		int32_t L_23 = V_5;
		V_5 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_005a:
	{
		int32_t L_24 = V_5;
		FieldInfoU5BU5D_t1144794227* L_25 = V_4;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))))))
		{
			goto IL_003c;
		}
	}
	{
		return;
	}
}
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::SerializeNonPrimitiveObject(System.Object,System.Object&)
extern "C"  bool PocoJsonSerializerStrategy_SerializeNonPrimitiveObject_m2970911232 (PocoJsonSerializerStrategy_t1373207638 * __this, Il2CppObject * ___input, Il2CppObject ** ___output, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___input;
		Il2CppObject ** L_1 = ___output;
		bool L_2 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(10 /* System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeKnownTypes(System.Object,System.Object&) */, __this, L_0, L_1);
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		Il2CppObject * L_3 = ___input;
		Il2CppObject ** L_4 = ___output;
		bool L_5 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(11 /* System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeUnknownTypes(System.Object,System.Object&) */, __this, L_3, L_4);
		return L_5;
	}

IL_0013:
	{
		return (bool)1;
	}
}
// System.Object SimpleJson.PocoJsonSerializerStrategy::DeserializeObject(System.Object,System.Type)
extern const Il2CppType* DateTime_t339033936_0_0_0_var;
extern const Il2CppType* Guid_t2778838590_0_0_0_var;
extern const Il2CppType* Int64_t2847414882_0_0_0_var;
extern const Il2CppType* Double_t534516614_0_0_0_var;
extern const Il2CppType* IConvertible_t4194222097_0_0_0_var;
extern const Il2CppType* Dictionary_2_t2776849293_0_0_0_var;
extern const Il2CppType* IList_t1612618265_0_0_0_var;
extern const Il2CppType* List_1_t475681172_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* DateTimeUtils_t111370036_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern TypeInfo* PocoJsonSerializerStrategy_t1373207638_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern TypeInfo* Guid_t2778838590_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t211005341_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t2847414882_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t534516614_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t3650470111_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern TypeInfo* CacheResolver_t2193954381_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1654916945_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t540522682_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t3446442070_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t1660539300_il2cpp_TypeInfo_var;
extern TypeInfo* IList_1_t3003598734_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* ICollection_1_t1302937806_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t1612618265_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t3709260776_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t2320212868_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1817333386_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2539855901_MethodInfo_var;
extern const MethodInfo* SafeDictionary_2_GetEnumerator_m1565498084_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m108871723_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m904945176_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1411373899;
extern const uint32_t PocoJsonSerializerStrategy_DeserializeObject_m59513699_MetadataUsageId;
extern "C"  Il2CppObject * PocoJsonSerializerStrategy_DeserializeObject_m59513699 (PocoJsonSerializerStrategy_t1373207638 * __this, Il2CppObject * ___value, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy_DeserializeObject_m59513699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	int64_t V_3 = 0;
	Il2CppObject* V_4 = NULL;
	Type_t * V_5 = NULL;
	Type_t * V_6 = NULL;
	Type_t * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	KeyValuePair_2_t1963335622  V_9;
	memset(&V_9, 0, sizeof(V_9));
	SafeDictionary_2_t2250466201 * V_10 = NULL;
	KeyValuePair_2_t177432852  V_11;
	memset(&V_11, 0, sizeof(V_11));
	MemberMap_t3346170946 * V_12 = NULL;
	String_t* V_13 = NULL;
	Il2CppObject * V_14 = NULL;
	Il2CppObject* V_15 = NULL;
	Il2CppObject * V_16 = NULL;
	int32_t V_17 = 0;
	Il2CppObject * V_18 = NULL;
	Type_t * V_19 = NULL;
	Type_t * V_20 = NULL;
	Il2CppObject * V_21 = NULL;
	Guid_t2778838590  V_22;
	memset(&V_22, 0, sizeof(V_22));
	TypeU5BU5D_t3431720054* V_23 = NULL;
	Il2CppObject* V_24 = NULL;
	Il2CppObject* V_25 = NULL;
	ObjectU5BU5D_t11523773* V_26 = NULL;
	Il2CppObject* V_27 = NULL;
	TypeU5BU5D_t3431720054* V_28 = NULL;
	Il2CppObject* V_29 = NULL;
	Guid_t2778838590  V_30;
	memset(&V_30, 0, sizeof(V_30));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Il2CppObject * G_B38_0 = NULL;
	{
		V_0 = NULL;
		Il2CppObject * L_0 = ___value;
		if (!((String_t*)IsInstSealed(L_0, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0144;
		}
	}
	{
		Il2CppObject * L_1 = ___value;
		V_1 = ((String_t*)IsInstSealed(L_1, String_t_il2cpp_TypeInfo_var));
		String_t* L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_00fa;
		}
	}
	{
		Type_t * L_4 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DateTime_t339033936_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_0049;
		}
	}
	{
		Type_t * L_6 = ___type;
		bool L_7 = ReflectionUtils_IsNullableType_m2201186244(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00bb;
		}
	}
	{
		Type_t * L_8 = ___type;
		Type_t * L_9 = Nullable_GetUnderlyingType_m3559493248(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DateTime_t339033936_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_9) == ((Il2CppObject*)(Type_t *)L_10))))
		{
			goto IL_00bb;
		}
	}

IL_0049:
	{
		String_t* L_11 = V_1;
		NullCheck(L_11);
		bool L_12 = String_StartsWith_m1500793453(L_11, _stringLiteral1411373899, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_009e;
		}
	}
	{
		String_t* L_13 = V_1;
		NullCheck(L_13);
		int32_t L_14 = String_IndexOf_m2775210486(L_13, ((int32_t)43), /*hidden argument*/NULL);
		V_2 = L_14;
		V_3 = (((int64_t)((int64_t)0)));
		int32_t L_15 = V_2;
		if ((((int32_t)L_15) < ((int32_t)0)))
		{
			goto IL_0078;
		}
	}
	{
		String_t* L_16 = V_1;
		int32_t L_17 = V_2;
		NullCheck(L_16);
		String_t* L_18 = String_Substring_m675079568(L_16, 6, ((int32_t)((int32_t)L_17-(int32_t)6)), /*hidden argument*/NULL);
		int64_t L_19 = Int64_Parse_m2426231402(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		goto IL_008d;
	}

IL_0078:
	{
		String_t* L_20 = V_1;
		String_t* L_21 = V_1;
		NullCheck(L_21);
		int32_t L_22 = String_get_Length_m2979997331(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_23 = String_Substring_m675079568(L_20, 6, ((int32_t)((int32_t)L_22-(int32_t)7)), /*hidden argument*/NULL);
		int64_t L_24 = Int64_Parse_m2426231402(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		V_3 = L_24;
	}

IL_008d:
	{
		int64_t L_25 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeUtils_t111370036_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_26 = DateTimeUtils_ConvertJavaScriptTicksToDateTime_m4164648278(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		DateTime_t339033936  L_27 = L_26;
		Il2CppObject * L_28 = Box(DateTime_t339033936_il2cpp_TypeInfo_var, &L_27);
		V_0 = L_28;
		goto IL_0458;
	}

IL_009e:
	{
		String_t* L_29 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(PocoJsonSerializerStrategy_t1373207638_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_30 = ((PocoJsonSerializerStrategy_t1373207638_StaticFields*)PocoJsonSerializerStrategy_t1373207638_il2cpp_TypeInfo_var->static_fields)->get_Iso8601Format_1();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_31 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_32 = DateTime_ParseExact_m4017898168(NULL /*static, unused*/, L_29, L_30, L_31, ((int32_t)80), /*hidden argument*/NULL);
		DateTime_t339033936  L_33 = L_32;
		Il2CppObject * L_34 = Box(DateTime_t339033936_il2cpp_TypeInfo_var, &L_33);
		V_0 = L_34;
		goto IL_0458;
	}

IL_00bb:
	{
		Type_t * L_35 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Guid_t2778838590_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_35) == ((Il2CppObject*)(Type_t *)L_36)))
		{
			goto IL_00e2;
		}
	}
	{
		Type_t * L_37 = ___type;
		bool L_38 = ReflectionUtils_IsNullableType_m2201186244(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00f3;
		}
	}
	{
		Type_t * L_39 = ___type;
		Type_t * L_40 = Nullable_GetUnderlyingType_m3559493248(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_41 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Guid_t2778838590_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_40) == ((Il2CppObject*)(Type_t *)L_41))))
		{
			goto IL_00f3;
		}
	}

IL_00e2:
	{
		String_t* L_42 = V_1;
		Guid_t2778838590  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Guid__ctor_m1994687478(&L_43, L_42, /*hidden argument*/NULL);
		Guid_t2778838590  L_44 = L_43;
		Il2CppObject * L_45 = Box(Guid_t2778838590_il2cpp_TypeInfo_var, &L_44);
		V_0 = L_45;
		goto IL_0458;
	}

IL_00f3:
	{
		String_t* L_46 = V_1;
		V_0 = L_46;
		goto IL_0458;
	}

IL_00fa:
	{
		Type_t * L_47 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_48 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Guid_t2778838590_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_47) == ((Il2CppObject*)(Type_t *)L_48))))
		{
			goto IL_011c;
		}
	}
	{
		Initobj (Guid_t2778838590_il2cpp_TypeInfo_var, (&V_22));
		Guid_t2778838590  L_49 = V_22;
		Guid_t2778838590  L_50 = L_49;
		Il2CppObject * L_51 = Box(Guid_t2778838590_il2cpp_TypeInfo_var, &L_50);
		V_0 = L_51;
		goto IL_0458;
	}

IL_011c:
	{
		Type_t * L_52 = ___type;
		bool L_53 = ReflectionUtils_IsNullableType_m2201186244(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_013d;
		}
	}
	{
		Type_t * L_54 = ___type;
		Type_t * L_55 = Nullable_GetUnderlyingType_m3559493248(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_56 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Guid_t2778838590_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_55) == ((Il2CppObject*)(Type_t *)L_56))))
		{
			goto IL_013d;
		}
	}
	{
		V_0 = NULL;
		goto IL_0458;
	}

IL_013d:
	{
		String_t* L_57 = V_1;
		V_0 = L_57;
		goto IL_0458;
	}

IL_0144:
	{
		Il2CppObject * L_58 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_58, Boolean_t211005341_il2cpp_TypeInfo_var)))
		{
			goto IL_0153;
		}
	}
	{
		Il2CppObject * L_59 = ___value;
		V_0 = L_59;
		goto IL_0458;
	}

IL_0153:
	{
		Il2CppObject * L_60 = ___value;
		if (L_60)
		{
			goto IL_015d;
		}
	}
	{
		V_0 = NULL;
		goto IL_0458;
	}

IL_015d:
	{
		Il2CppObject * L_61 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_61, Int64_t2847414882_il2cpp_TypeInfo_var)))
		{
			goto IL_0172;
		}
	}
	{
		Type_t * L_62 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_63 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int64_t2847414882_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_62) == ((Il2CppObject*)(Type_t *)L_63)))
		{
			goto IL_0187;
		}
	}

IL_0172:
	{
		Il2CppObject * L_64 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_64, Double_t534516614_il2cpp_TypeInfo_var)))
		{
			goto IL_018e;
		}
	}
	{
		Type_t * L_65 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_66 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Double_t534516614_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_65) == ((Il2CppObject*)(Type_t *)L_66))))
		{
			goto IL_018e;
		}
	}

IL_0187:
	{
		Il2CppObject * L_67 = ___value;
		V_0 = L_67;
		goto IL_0458;
	}

IL_018e:
	{
		Il2CppObject * L_68 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_68, Double_t534516614_il2cpp_TypeInfo_var)))
		{
			goto IL_01a3;
		}
	}
	{
		Type_t * L_69 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_70 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Double_t534516614_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_69) == ((Il2CppObject*)(Type_t *)L_70))))
		{
			goto IL_01b8;
		}
	}

IL_01a3:
	{
		Il2CppObject * L_71 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_71, Int64_t2847414882_il2cpp_TypeInfo_var)))
		{
			goto IL_01df;
		}
	}
	{
		Type_t * L_72 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_73 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int64_t2847414882_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_72) == ((Il2CppObject*)(Type_t *)L_73)))
		{
			goto IL_01df;
		}
	}

IL_01b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_74 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IConvertible_t4194222097_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_75 = ___type;
		NullCheck(L_74);
		bool L_76 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_74, L_75);
		if (L_76)
		{
			goto IL_01cd;
		}
	}
	{
		Il2CppObject * L_77 = ___value;
		G_B38_0 = L_77;
		goto IL_01d9;
	}

IL_01cd:
	{
		Il2CppObject * L_78 = ___value;
		Type_t * L_79 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_80 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		Il2CppObject * L_81 = Convert_ChangeType_m39028498(NULL /*static, unused*/, L_78, L_79, L_80, /*hidden argument*/NULL);
		G_B38_0 = L_81;
	}

IL_01d9:
	{
		V_0 = G_B38_0;
		goto IL_0458;
	}

IL_01df:
	{
		Il2CppObject * L_82 = ___value;
		if (!((Il2CppObject*)IsInst(L_82, IDictionary_2_t3650470111_il2cpp_TypeInfo_var)))
		{
			goto IL_0331;
		}
	}
	{
		Il2CppObject * L_83 = ___value;
		V_4 = ((Il2CppObject*)Castclass(L_83, IDictionary_2_t3650470111_il2cpp_TypeInfo_var));
		Type_t * L_84 = ___type;
		bool L_85 = ReflectionUtils_IsTypeDictionary_m3763562223(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		if (!L_85)
		{
			goto IL_0296;
		}
	}
	{
		Type_t * L_86 = ___type;
		NullCheck(L_86);
		TypeU5BU5D_t3431720054* L_87 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(77 /* System.Type[] System.Type::GetGenericArguments() */, L_86);
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, 0);
		int32_t L_88 = 0;
		V_5 = ((L_87)->GetAt(static_cast<il2cpp_array_size_t>(L_88)));
		Type_t * L_89 = ___type;
		NullCheck(L_89);
		TypeU5BU5D_t3431720054* L_90 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(77 /* System.Type[] System.Type::GetGenericArguments() */, L_89);
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, 1);
		int32_t L_91 = 1;
		V_6 = ((L_90)->GetAt(static_cast<il2cpp_array_size_t>(L_91)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_92 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Dictionary_2_t2776849293_0_0_0_var), /*hidden argument*/NULL);
		V_23 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)2));
		TypeU5BU5D_t3431720054* L_93 = V_23;
		Type_t * L_94 = V_5;
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, 0);
		ArrayElementTypeCheck (L_93, L_94);
		(L_93)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_94);
		TypeU5BU5D_t3431720054* L_95 = V_23;
		Type_t * L_96 = V_6;
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, 1);
		ArrayElementTypeCheck (L_95, L_96);
		(L_95)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_96);
		TypeU5BU5D_t3431720054* L_97 = V_23;
		NullCheck(L_92);
		Type_t * L_98 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3431720054* >::Invoke(82 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_92, L_97);
		V_7 = L_98;
		Type_t * L_99 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		Il2CppObject * L_100 = CacheResolver_GetNewInstance_m3764201201(NULL /*static, unused*/, L_99, /*hidden argument*/NULL);
		V_8 = ((Il2CppObject *)Castclass(L_100, IDictionary_t1654916945_il2cpp_TypeInfo_var));
		Il2CppObject* L_101 = V_4;
		NullCheck(L_101);
		Il2CppObject* L_102 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::GetEnumerator() */, IEnumerable_1_t540522682_il2cpp_TypeInfo_var, L_101);
		V_24 = L_102;
	}

IL_024f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0277;
		}

IL_0251:
		{
			Il2CppObject* L_103 = V_24;
			NullCheck(L_103);
			KeyValuePair_2_t1963335622  L_104 = InterfaceFuncInvoker0< KeyValuePair_2_t1963335622  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::get_Current() */, IEnumerator_1_t3446442070_il2cpp_TypeInfo_var, L_103);
			V_9 = L_104;
			Il2CppObject * L_105 = V_8;
			String_t* L_106 = KeyValuePair_2_get_Key_m1817333386((&V_9), /*hidden argument*/KeyValuePair_2_get_Key_m1817333386_MethodInfo_var);
			Il2CppObject * L_107 = KeyValuePair_2_get_Value_m2539855901((&V_9), /*hidden argument*/KeyValuePair_2_get_Value_m2539855901_MethodInfo_var);
			Type_t * L_108 = V_6;
			Il2CppObject * L_109 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, Type_t * >::Invoke(8 /* System.Object SimpleJson.PocoJsonSerializerStrategy::DeserializeObject(System.Object,System.Type) */, __this, L_107, L_108);
			NullCheck(L_105);
			InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Void System.Collections.IDictionary::Add(System.Object,System.Object) */, IDictionary_t1654916945_il2cpp_TypeInfo_var, L_105, L_106, L_109);
		}

IL_0277:
		{
			Il2CppObject* L_110 = V_24;
			NullCheck(L_110);
			bool L_111 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_110);
			if (L_111)
			{
				goto IL_0251;
			}
		}

IL_0280:
		{
			IL2CPP_LEAVE(0x28E, FINALLY_0282);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0282;
	}

FINALLY_0282:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_112 = V_24;
			if (!L_112)
			{
				goto IL_028d;
			}
		}

IL_0286:
		{
			Il2CppObject* L_113 = V_24;
			NullCheck(L_113);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_113);
		}

IL_028d:
		{
			IL2CPP_END_FINALLY(642)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(642)
	{
		IL2CPP_JUMP_TBL(0x28E, IL_028e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_028e:
	{
		Il2CppObject * L_114 = V_8;
		V_0 = L_114;
		goto IL_0456;
	}

IL_0296:
	{
		Type_t * L_115 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		Il2CppObject * L_116 = CacheResolver_GetNewInstance_m3764201201(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
		V_0 = L_116;
		CacheResolver_t2193954381 * L_117 = __this->get_CacheResolver_0();
		Type_t * L_118 = ___type;
		NullCheck(L_117);
		SafeDictionary_2_t2250466201 * L_119 = CacheResolver_LoadMaps_m2248807851(L_117, L_118, /*hidden argument*/NULL);
		V_10 = L_119;
		SafeDictionary_2_t2250466201 * L_120 = V_10;
		if (L_120)
		{
			goto IL_02b6;
		}
	}
	{
		Il2CppObject * L_121 = ___value;
		V_0 = L_121;
		goto IL_0456;
	}

IL_02b6:
	{
		SafeDictionary_2_t2250466201 * L_122 = V_10;
		NullCheck(L_122);
		Il2CppObject* L_123 = SafeDictionary_2_GetEnumerator_m1565498084(L_122, /*hidden argument*/SafeDictionary_2_GetEnumerator_m1565498084_MethodInfo_var);
		V_25 = L_123;
	}

IL_02bf:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0317;
		}

IL_02c1:
		{
			Il2CppObject* L_124 = V_25;
			NullCheck(L_124);
			KeyValuePair_2_t177432852  L_125 = InterfaceFuncInvoker0< KeyValuePair_2_t177432852  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>::get_Current() */, IEnumerator_1_t1660539300_il2cpp_TypeInfo_var, L_124);
			V_11 = L_125;
			MemberMap_t3346170946 * L_126 = KeyValuePair_2_get_Value_m108871723((&V_11), /*hidden argument*/KeyValuePair_2_get_Value_m108871723_MethodInfo_var);
			V_12 = L_126;
			MemberMap_t3346170946 * L_127 = V_12;
			NullCheck(L_127);
			SetHandler_t1019049325 * L_128 = L_127->get_Setter_3();
			if (!L_128)
			{
				goto IL_0317;
			}
		}

IL_02dc:
		{
			String_t* L_129 = KeyValuePair_2_get_Key_m904945176((&V_11), /*hidden argument*/KeyValuePair_2_get_Key_m904945176_MethodInfo_var);
			V_13 = L_129;
			Il2CppObject* L_130 = V_4;
			String_t* L_131 = V_13;
			NullCheck(L_130);
			bool L_132 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::ContainsKey(!0) */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, L_130, L_131);
			if (!L_132)
			{
				goto IL_0317;
			}
		}

IL_02f0:
		{
			Il2CppObject* L_133 = V_4;
			String_t* L_134 = V_13;
			NullCheck(L_133);
			Il2CppObject * L_135 = InterfaceFuncInvoker1< Il2CppObject *, String_t* >::Invoke(3 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Item(!0) */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, L_133, L_134);
			MemberMap_t3346170946 * L_136 = V_12;
			NullCheck(L_136);
			Type_t * L_137 = L_136->get_Type_1();
			Il2CppObject * L_138 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, Type_t * >::Invoke(8 /* System.Object SimpleJson.PocoJsonSerializerStrategy::DeserializeObject(System.Object,System.Type) */, __this, L_135, L_137);
			V_14 = L_138;
			MemberMap_t3346170946 * L_139 = V_12;
			NullCheck(L_139);
			SetHandler_t1019049325 * L_140 = L_139->get_Setter_3();
			Il2CppObject * L_141 = V_0;
			Il2CppObject * L_142 = V_14;
			NullCheck(L_140);
			SetHandler_Invoke_m2910967830(L_140, L_141, L_142, /*hidden argument*/NULL);
		}

IL_0317:
		{
			Il2CppObject* L_143 = V_25;
			NullCheck(L_143);
			bool L_144 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_143);
			if (L_144)
			{
				goto IL_02c1;
			}
		}

IL_0320:
		{
			IL2CPP_LEAVE(0x456, FINALLY_0325);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0325;
	}

FINALLY_0325:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_145 = V_25;
			if (!L_145)
			{
				goto IL_0330;
			}
		}

IL_0329:
		{
			Il2CppObject* L_146 = V_25;
			NullCheck(L_146);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_146);
		}

IL_0330:
		{
			IL2CPP_END_FINALLY(805)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(805)
	{
		IL2CPP_JUMP_TBL(0x456, IL_0456)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0331:
	{
		Il2CppObject * L_147 = ___value;
		if (!((Il2CppObject*)IsInst(L_147, IList_1_t3003598734_il2cpp_TypeInfo_var)))
		{
			goto IL_0456;
		}
	}
	{
		Il2CppObject * L_148 = ___value;
		V_15 = ((Il2CppObject*)Castclass(L_148, IList_1_t3003598734_il2cpp_TypeInfo_var));
		V_16 = (Il2CppObject *)NULL;
		Type_t * L_149 = ___type;
		NullCheck(L_149);
		bool L_150 = VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Type::get_IsArray() */, L_149);
		if (!L_150)
		{
			goto IL_03c3;
		}
	}
	{
		Type_t * L_151 = ___type;
		V_26 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t11523773* L_152 = V_26;
		Il2CppObject* L_153 = V_15;
		NullCheck(L_153);
		int32_t L_154 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t1302937806_il2cpp_TypeInfo_var, L_153);
		int32_t L_155 = L_154;
		Il2CppObject * L_156 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_155);
		NullCheck(L_152);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_152, 0);
		ArrayElementTypeCheck (L_152, L_156);
		(L_152)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_156);
		ObjectU5BU5D_t11523773* L_157 = V_26;
		Il2CppObject * L_158 = Activator_CreateInstance_m2161363287(NULL /*static, unused*/, L_151, L_157, /*hidden argument*/NULL);
		V_16 = ((Il2CppObject *)Castclass(L_158, IList_t1612618265_il2cpp_TypeInfo_var));
		V_17 = 0;
		Il2CppObject* L_159 = V_15;
		NullCheck(L_159);
		Il2CppObject* L_160 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IEnumerable_1_t3709260776_il2cpp_TypeInfo_var, L_159);
		V_27 = L_160;
	}

IL_0382:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03a9;
		}

IL_0384:
		{
			Il2CppObject* L_161 = V_27;
			NullCheck(L_161);
			Il2CppObject * L_162 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IEnumerator_1_t2320212868_il2cpp_TypeInfo_var, L_161);
			V_18 = L_162;
			Il2CppObject * L_163 = V_16;
			int32_t L_164 = V_17;
			int32_t L_165 = L_164;
			V_17 = ((int32_t)((int32_t)L_165+(int32_t)1));
			Il2CppObject * L_166 = V_18;
			Type_t * L_167 = ___type;
			NullCheck(L_167);
			Type_t * L_168 = VirtFuncInvoker0< Type_t * >::Invoke(45 /* System.Type System.Type::GetElementType() */, L_167);
			Il2CppObject * L_169 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, Type_t * >::Invoke(8 /* System.Object SimpleJson.PocoJsonSerializerStrategy::DeserializeObject(System.Object,System.Type) */, __this, L_166, L_168);
			NullCheck(L_163);
			InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(3 /* System.Void System.Collections.IList::set_Item(System.Int32,System.Object) */, IList_t1612618265_il2cpp_TypeInfo_var, L_163, L_165, L_169);
		}

IL_03a9:
		{
			Il2CppObject* L_170 = V_27;
			NullCheck(L_170);
			bool L_171 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_170);
			if (L_171)
			{
				goto IL_0384;
			}
		}

IL_03b2:
		{
			IL2CPP_LEAVE(0x453, FINALLY_03b7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_03b7;
	}

FINALLY_03b7:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_172 = V_27;
			if (!L_172)
			{
				goto IL_03c2;
			}
		}

IL_03bb:
		{
			Il2CppObject* L_173 = V_27;
			NullCheck(L_173);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_173);
		}

IL_03c2:
		{
			IL2CPP_END_FINALLY(951)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(951)
	{
		IL2CPP_JUMP_TBL(0x453, IL_0453)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_03c3:
	{
		Type_t * L_174 = ___type;
		bool L_175 = ReflectionUtils_IsTypeGenericeCollectionInterface_m1283721406(NULL /*static, unused*/, L_174, /*hidden argument*/NULL);
		if (L_175)
		{
			goto IL_03dd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_176 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IList_t1612618265_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_177 = ___type;
		NullCheck(L_176);
		bool L_178 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_176, L_177);
		if (!L_178)
		{
			goto IL_0453;
		}
	}

IL_03dd:
	{
		Type_t * L_179 = ___type;
		NullCheck(L_179);
		TypeU5BU5D_t3431720054* L_180 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(77 /* System.Type[] System.Type::GetGenericArguments() */, L_179);
		NullCheck(L_180);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_180, 0);
		int32_t L_181 = 0;
		V_19 = ((L_180)->GetAt(static_cast<il2cpp_array_size_t>(L_181)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_182 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(List_1_t475681172_0_0_0_var), /*hidden argument*/NULL);
		V_28 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)1));
		TypeU5BU5D_t3431720054* L_183 = V_28;
		Type_t * L_184 = V_19;
		NullCheck(L_183);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_183, 0);
		ArrayElementTypeCheck (L_183, L_184);
		(L_183)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_184);
		TypeU5BU5D_t3431720054* L_185 = V_28;
		NullCheck(L_182);
		Type_t * L_186 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3431720054* >::Invoke(82 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_182, L_185);
		V_20 = L_186;
		Type_t * L_187 = V_20;
		IL2CPP_RUNTIME_CLASS_INIT(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		Il2CppObject * L_188 = CacheResolver_GetNewInstance_m3764201201(NULL /*static, unused*/, L_187, /*hidden argument*/NULL);
		V_16 = ((Il2CppObject *)Castclass(L_188, IList_t1612618265_il2cpp_TypeInfo_var));
		Il2CppObject* L_189 = V_15;
		NullCheck(L_189);
		Il2CppObject* L_190 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IEnumerable_1_t3709260776_il2cpp_TypeInfo_var, L_189);
		V_29 = L_190;
	}

IL_041f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_043c;
		}

IL_0421:
		{
			Il2CppObject* L_191 = V_29;
			NullCheck(L_191);
			Il2CppObject * L_192 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IEnumerator_1_t2320212868_il2cpp_TypeInfo_var, L_191);
			V_21 = L_192;
			Il2CppObject * L_193 = V_16;
			Il2CppObject * L_194 = V_21;
			Type_t * L_195 = V_19;
			Il2CppObject * L_196 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, Type_t * >::Invoke(8 /* System.Object SimpleJson.PocoJsonSerializerStrategy::DeserializeObject(System.Object,System.Type) */, __this, L_194, L_195);
			NullCheck(L_193);
			InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1612618265_il2cpp_TypeInfo_var, L_193, L_196);
		}

IL_043c:
		{
			Il2CppObject* L_197 = V_29;
			NullCheck(L_197);
			bool L_198 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_197);
			if (L_198)
			{
				goto IL_0421;
			}
		}

IL_0445:
		{
			IL2CPP_LEAVE(0x453, FINALLY_0447);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0447;
	}

FINALLY_0447:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_199 = V_29;
			if (!L_199)
			{
				goto IL_0452;
			}
		}

IL_044b:
		{
			Il2CppObject* L_200 = V_29;
			NullCheck(L_200);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_200);
		}

IL_0452:
		{
			IL2CPP_END_FINALLY(1095)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1095)
	{
		IL2CPP_JUMP_TBL(0x453, IL_0453)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0453:
	{
		Il2CppObject * L_201 = V_16;
		V_0 = L_201;
	}

IL_0456:
	{
		Il2CppObject * L_202 = V_0;
		return L_202;
	}

IL_0458:
	{
		Type_t * L_203 = ___type;
		bool L_204 = ReflectionUtils_IsNullableType_m2201186244(NULL /*static, unused*/, L_203, /*hidden argument*/NULL);
		if (!L_204)
		{
			goto IL_0468;
		}
	}
	{
		Il2CppObject * L_205 = V_0;
		Type_t * L_206 = ___type;
		Il2CppObject * L_207 = ReflectionUtils_ToNullableType_m4235305546(NULL /*static, unused*/, L_205, L_206, /*hidden argument*/NULL);
		return L_207;
	}

IL_0468:
	{
		Il2CppObject * L_208 = V_0;
		if (L_208)
		{
			goto IL_0488;
		}
	}
	{
		Type_t * L_209 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_210 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Guid_t2778838590_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_209) == ((Il2CppObject*)(Type_t *)L_210))))
		{
			goto IL_0488;
		}
	}
	{
		Initobj (Guid_t2778838590_il2cpp_TypeInfo_var, (&V_30));
		Guid_t2778838590  L_211 = V_30;
		Guid_t2778838590  L_212 = L_211;
		Il2CppObject * L_213 = Box(Guid_t2778838590_il2cpp_TypeInfo_var, &L_212);
		return L_213;
	}

IL_0488:
	{
		Il2CppObject * L_214 = V_0;
		return L_214;
	}
}
// System.Object SimpleJson.PocoJsonSerializerStrategy::SerializeEnum(System.Enum)
extern TypeInfo* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t PocoJsonSerializerStrategy_SerializeEnum_m3979086379_MetadataUsageId;
extern "C"  Il2CppObject * PocoJsonSerializerStrategy_SerializeEnum_m3979086379 (PocoJsonSerializerStrategy_t1373207638 * __this, Enum_t2778772662 * ___p, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy_SerializeEnum_m3979086379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enum_t2778772662 * L_0 = ___p;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_1 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		double L_2 = Convert_ToDouble_m1171958389(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		double L_3 = L_2;
		Il2CppObject * L_4 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_3);
		return L_4;
	}
}
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeKnownTypes(System.Object,System.Object&)
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern TypeInfo* DateTimeUtils_t111370036_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t2847414882_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Guid_t2778838590_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t2776692961_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t2778772662_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral65803802;
extern Il2CppCodeGenString* _stringLiteral1386;
extern Il2CppCodeGenString* _stringLiteral68;
extern const uint32_t PocoJsonSerializerStrategy_TrySerializeKnownTypes_m484211082_MetadataUsageId;
extern "C"  bool PocoJsonSerializerStrategy_TrySerializeKnownTypes_m484211082 (PocoJsonSerializerStrategy_t1373207638 * __this, Il2CppObject * ___input, Il2CppObject ** ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy_TrySerializeKnownTypes_m484211082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	DateTime_t339033936  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Guid_t2778838590  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = (bool)1;
		Il2CppObject * L_0 = ___input;
		if (!((Il2CppObject *)IsInstSealed(L_0, DateTime_t339033936_il2cpp_TypeInfo_var)))
		{
			goto IL_0035;
		}
	}
	{
		Il2CppObject ** L_1 = ___output;
		Il2CppObject * L_2 = ___input;
		V_1 = ((*(DateTime_t339033936 *)((DateTime_t339033936 *)UnBox (L_2, DateTime_t339033936_il2cpp_TypeInfo_var))));
		DateTime_t339033936  L_3 = DateTime_ToUniversalTime_m691668206((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeUtils_t111370036_il2cpp_TypeInfo_var);
		int64_t L_4 = DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m2628017042(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int64_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral65803802, L_6, _stringLiteral1386, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_1)) = (Il2CppObject *)L_7;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_1), (Il2CppObject *)L_7);
		goto IL_0083;
	}

IL_0035:
	{
		Il2CppObject * L_8 = ___input;
		if (!((Il2CppObject *)IsInstSealed(L_8, Guid_t2778838590_il2cpp_TypeInfo_var)))
		{
			goto IL_0054;
		}
	}
	{
		Il2CppObject ** L_9 = ___output;
		Il2CppObject * L_10 = ___input;
		V_2 = ((*(Guid_t2778838590 *)((Guid_t2778838590 *)UnBox (L_10, Guid_t2778838590_il2cpp_TypeInfo_var))));
		String_t* L_11 = Guid_ToString_m2135662273((&V_2), _stringLiteral68, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_9)) = (Il2CppObject *)L_11;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_9), (Il2CppObject *)L_11);
		goto IL_0083;
	}

IL_0054:
	{
		Il2CppObject * L_12 = ___input;
		if (!((Uri_t2776692961 *)IsInstClass(L_12, Uri_t2776692961_il2cpp_TypeInfo_var)))
		{
			goto IL_0066;
		}
	}
	{
		Il2CppObject ** L_13 = ___output;
		Il2CppObject * L_14 = ___input;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		*((Il2CppObject **)(L_13)) = (Il2CppObject *)L_15;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_13), (Il2CppObject *)L_15);
		goto IL_0083;
	}

IL_0066:
	{
		Il2CppObject * L_16 = ___input;
		if (!((Enum_t2778772662 *)IsInstClass(L_16, Enum_t2778772662_il2cpp_TypeInfo_var)))
		{
			goto IL_007e;
		}
	}
	{
		Il2CppObject ** L_17 = ___output;
		Il2CppObject * L_18 = ___input;
		Il2CppObject * L_19 = VirtFuncInvoker1< Il2CppObject *, Enum_t2778772662 * >::Invoke(9 /* System.Object SimpleJson.PocoJsonSerializerStrategy::SerializeEnum(System.Enum) */, __this, ((Enum_t2778772662 *)CastclassClass(L_18, Enum_t2778772662_il2cpp_TypeInfo_var)));
		*((Il2CppObject **)(L_17)) = (Il2CppObject *)L_19;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_17), (Il2CppObject *)L_19);
		goto IL_0083;
	}

IL_007e:
	{
		V_0 = (bool)0;
		Il2CppObject ** L_20 = ___output;
		*((Il2CppObject **)(L_20)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_20), (Il2CppObject *)NULL);
	}

IL_0083:
	{
		bool L_21 = V_0;
		return L_21;
	}
}
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeUnknownTypes(System.Object,System.Object&)
extern TypeInfo* JsonObject_t4190420965_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t1660539300_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t3650470111_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* SafeDictionary_2_GetEnumerator_m1565498084_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m108871723_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m904945176_MethodInfo_var;
extern const uint32_t PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m3846546659_MetadataUsageId;
extern "C"  bool PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m3846546659 (PocoJsonSerializerStrategy_t1373207638 * __this, Il2CppObject * ___input, Il2CppObject ** ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m3846546659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	SafeDictionary_2_t2250466201 * V_2 = NULL;
	KeyValuePair_2_t177432852  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Il2CppObject* V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject ** L_0 = ___output;
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)NULL);
		Il2CppObject * L_1 = ___input;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m2022236990(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Type_t * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_3);
		if (L_4)
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		JsonObject_t4190420965 * L_5 = (JsonObject_t4190420965 *)il2cpp_codegen_object_new(JsonObject_t4190420965_il2cpp_TypeInfo_var);
		JsonObject__ctor_m3751193732(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		CacheResolver_t2193954381 * L_6 = __this->get_CacheResolver_0();
		Type_t * L_7 = V_0;
		NullCheck(L_6);
		SafeDictionary_2_t2250466201 * L_8 = CacheResolver_LoadMaps_m2248807851(L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		SafeDictionary_2_t2250466201 * L_9 = V_2;
		NullCheck(L_9);
		Il2CppObject* L_10 = SafeDictionary_2_GetEnumerator_m1565498084(L_9, /*hidden argument*/SafeDictionary_2_GetEnumerator_m1565498084_MethodInfo_var);
		V_4 = L_10;
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0066;
		}

IL_0031:
		{
			Il2CppObject* L_11 = V_4;
			NullCheck(L_11);
			KeyValuePair_2_t177432852  L_12 = InterfaceFuncInvoker0< KeyValuePair_2_t177432852  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>::get_Current() */, IEnumerator_1_t1660539300_il2cpp_TypeInfo_var, L_11);
			V_3 = L_12;
			MemberMap_t3346170946 * L_13 = KeyValuePair_2_get_Value_m108871723((&V_3), /*hidden argument*/KeyValuePair_2_get_Value_m108871723_MethodInfo_var);
			NullCheck(L_13);
			GetHandler_t3377211385 * L_14 = L_13->get_Getter_2();
			if (!L_14)
			{
				goto IL_0066;
			}
		}

IL_0047:
		{
			Il2CppObject* L_15 = V_1;
			String_t* L_16 = KeyValuePair_2_get_Key_m904945176((&V_3), /*hidden argument*/KeyValuePair_2_get_Key_m904945176_MethodInfo_var);
			MemberMap_t3346170946 * L_17 = KeyValuePair_2_get_Value_m108871723((&V_3), /*hidden argument*/KeyValuePair_2_get_Value_m108871723_MethodInfo_var);
			NullCheck(L_17);
			GetHandler_t3377211385 * L_18 = L_17->get_Getter_2();
			Il2CppObject * L_19 = ___input;
			NullCheck(L_18);
			Il2CppObject * L_20 = GetHandler_Invoke_m865915879(L_18, L_19, /*hidden argument*/NULL);
			NullCheck(L_15);
			InterfaceActionInvoker2< String_t*, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Object>::Add(!0,!1) */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, L_15, L_16, L_20);
		}

IL_0066:
		{
			Il2CppObject* L_21 = V_4;
			NullCheck(L_21);
			bool L_22 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_21);
			if (L_22)
			{
				goto IL_0031;
			}
		}

IL_006f:
		{
			IL2CPP_LEAVE(0x7D, FINALLY_0071);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0071;
	}

FINALLY_0071:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_23 = V_4;
			if (!L_23)
			{
				goto IL_007c;
			}
		}

IL_0075:
		{
			Il2CppObject* L_24 = V_4;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_24);
		}

IL_007c:
		{
			IL2CPP_END_FINALLY(113)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(113)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		Il2CppObject ** L_25 = ___output;
		Il2CppObject* L_26 = V_1;
		*((Il2CppObject **)(L_25)) = (Il2CppObject *)L_26;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_25), (Il2CppObject *)L_26);
		return (bool)1;
	}
}
// System.Void SimpleJson.PocoJsonSerializerStrategy::.cctor()
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* PocoJsonSerializerStrategy_t1373207638_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2386708110;
extern Il2CppCodeGenString* _stringLiteral3905099382;
extern Il2CppCodeGenString* _stringLiteral3451106899;
extern const uint32_t PocoJsonSerializerStrategy__cctor_m1256415962_MetadataUsageId;
extern "C"  void PocoJsonSerializerStrategy__cctor_m1256415962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy__cctor_m1256415962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	DateTime_t339033936  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		V_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)3));
		StringU5BU5D_t2956870243* L_0 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2386708110);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2386708110);
		StringU5BU5D_t2956870243* L_1 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral3905099382);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3905099382);
		StringU5BU5D_t2956870243* L_2 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		ArrayElementTypeCheck (L_2, _stringLiteral3451106899);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3451106899);
		StringU5BU5D_t2956870243* L_3 = V_0;
		((PocoJsonSerializerStrategy_t1373207638_StaticFields*)PocoJsonSerializerStrategy_t1373207638_il2cpp_TypeInfo_var->static_fields)->set_Iso8601Format_1(L_3);
		DateTime_t339033936  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DateTime__ctor_m145640619(&L_4, ((int32_t)1970), 1, 1, /*hidden argument*/NULL);
		V_1 = L_4;
		DateTime_t339033936  L_5 = DateTime_ToUniversalTime_m691668206((&V_1), /*hidden argument*/NULL);
		((PocoJsonSerializerStrategy_t1373207638_StaticFields*)PocoJsonSerializerStrategy_t1373207638_il2cpp_TypeInfo_var->static_fields)->set__offsetDateTime_2(L_5);
		return;
	}
}
// System.Void SimpleJson.Reflection.CacheResolver::.ctor(SimpleJson.Reflection.MemberMapLoader)
extern TypeInfo* SafeDictionary_2_t1629611267_il2cpp_TypeInfo_var;
extern const MethodInfo* SafeDictionary_2__ctor_m4284956700_MethodInfo_var;
extern const uint32_t CacheResolver__ctor_m3244435910_MetadataUsageId;
extern "C"  void CacheResolver__ctor_m3244435910 (CacheResolver_t2193954381 * __this, MemberMapLoader_t3073810362 * ___memberMapLoader, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CacheResolver__ctor_m3244435910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SafeDictionary_2_t1629611267 * L_0 = (SafeDictionary_2_t1629611267 *)il2cpp_codegen_object_new(SafeDictionary_2_t1629611267_il2cpp_TypeInfo_var);
		SafeDictionary_2__ctor_m4284956700(L_0, /*hidden argument*/SafeDictionary_2__ctor_m4284956700_MethodInfo_var);
		__this->set__memberMapsCache_1(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		MemberMapLoader_t3073810362 * L_1 = ___memberMapLoader;
		__this->set__memberMapLoader_0(L_1);
		return;
	}
}
// System.Object SimpleJson.Reflection.CacheResolver::GetNewInstance(System.Type)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern const Il2CppType* CtorDelegate_t2720072665_0_0_0_var;
extern TypeInfo* CacheResolver_t2193954381_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* DynamicMethod_t2778460557_il2cpp_TypeInfo_var;
extern TypeInfo* OpCodes_t2031828562_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* CtorDelegate_t2720072665_il2cpp_TypeInfo_var;
extern const MethodInfo* SafeDictionary_2_TryGetValue_m1544795075_MethodInfo_var;
extern const MethodInfo* SafeDictionary_2_Add_m2117596466_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2026540316;
extern Il2CppCodeGenString* _stringLiteral2301956569;
extern const uint32_t CacheResolver_GetNewInstance_m3764201201_MetadataUsageId;
extern "C"  Il2CppObject * CacheResolver_GetNewInstance_m3764201201 (Il2CppObject * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CacheResolver_GetNewInstance_m3764201201_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CtorDelegate_t2720072665 * V_0 = NULL;
	DynamicMethod_t2778460557 * V_1 = NULL;
	ILGenerator_t3869071517 * V_2 = NULL;
	ConstructorInfo_t3542137334 * V_3 = NULL;
	ObjectU5BU5D_t11523773* V_4 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		SafeDictionary_2_t2099217731 * L_0 = ((CacheResolver_t2193954381_StaticFields*)CacheResolver_t2193954381_il2cpp_TypeInfo_var->static_fields)->get_ConstructorCache_2();
		Type_t * L_1 = ___type;
		NullCheck(L_0);
		bool L_2 = SafeDictionary_2_TryGetValue_m1544795075(L_0, L_1, (&V_0), /*hidden argument*/SafeDictionary_2_TryGetValue_m1544795075_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		CtorDelegate_t2720072665 * L_3 = V_0;
		NullCheck(L_3);
		Il2CppObject * L_4 = CtorDelegate_Invoke_m4052220313(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0016:
	{
		Type_t * L_5 = ___type;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2026540316, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3431720054* L_9 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		Type_t * L_10 = ___type;
		DynamicMethod_t2778460557 * L_11 = (DynamicMethod_t2778460557 *)il2cpp_codegen_object_new(DynamicMethod_t2778460557_il2cpp_TypeInfo_var);
		DynamicMethod__ctor_m2648831853(L_11, L_7, L_8, L_9, L_10, (bool)1, /*hidden argument*/NULL);
		V_1 = L_11;
		DynamicMethod_t2778460557 * L_12 = V_1;
		NullCheck(L_12);
		DynamicMethod_set_InitLocals_m2602959335(L_12, (bool)1, /*hidden argument*/NULL);
		DynamicMethod_t2778460557 * L_13 = V_1;
		NullCheck(L_13);
		ILGenerator_t3869071517 * L_14 = DynamicMethod_GetILGenerator_m3180908743(L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		Type_t * L_15 = ___type;
		NullCheck(L_15);
		bool L_16 = VirtFuncInvoker0< bool >::Invoke(35 /* System.Boolean System.Type::get_IsValueType() */, L_15);
		if (!L_16)
		{
			goto IL_0074;
		}
	}
	{
		ILGenerator_t3869071517 * L_17 = V_2;
		Type_t * L_18 = ___type;
		NullCheck(L_17);
		VirtFuncInvoker1< LocalBuilder_t625143165 *, Type_t * >::Invoke(4 /* System.Reflection.Emit.LocalBuilder System.Reflection.Emit.ILGenerator::DeclareLocal(System.Type) */, L_17, L_18);
		ILGenerator_t3869071517 * L_19 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_20 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ldloc_0_6();
		NullCheck(L_19);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(6 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_19, L_20);
		ILGenerator_t3869071517 * L_21 = V_2;
		OpCode_t4028977979  L_22 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Box_137();
		Type_t * L_23 = ___type;
		NullCheck(L_21);
		VirtActionInvoker2< OpCode_t4028977979 , Type_t * >::Invoke(10 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Type) */, L_21, L_22, L_23);
		goto IL_00b7;
	}

IL_0074:
	{
		Type_t * L_24 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3431720054* L_25 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		NullCheck(L_24);
		ConstructorInfo_t3542137334 * L_26 = VirtFuncInvoker4< ConstructorInfo_t3542137334 *, int32_t, Binder_t4180926488 *, TypeU5BU5D_t3431720054*, ParameterModifierU5BU5D_t3379147067* >::Invoke(73 /* System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Reflection.BindingFlags,System.Reflection.Binder,System.Type[],System.Reflection.ParameterModifier[]) */, L_24, ((int32_t)52), (Binder_t4180926488 *)NULL, L_25, (ParameterModifierU5BU5D_t3379147067*)(ParameterModifierU5BU5D_t3379147067*)NULL);
		V_3 = L_26;
		ConstructorInfo_t3542137334 * L_27 = V_3;
		if (L_27)
		{
			goto IL_00ab;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_28 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t11523773* L_29 = V_4;
		Type_t * L_30 = ___type;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 0);
		ArrayElementTypeCheck (L_29, L_30);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_30);
		ObjectU5BU5D_t11523773* L_31 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Format_m3351777162(NULL /*static, unused*/, L_28, _stringLiteral2301956569, L_31, /*hidden argument*/NULL);
		Exception_t1967233988 * L_33 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_33, L_32, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_00ab:
	{
		ILGenerator_t3869071517 * L_34 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_35 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Newobj_114();
		ConstructorInfo_t3542137334 * L_36 = V_3;
		NullCheck(L_34);
		VirtActionInvoker2< OpCode_t4028977979 , ConstructorInfo_t3542137334 * >::Invoke(7 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.ConstructorInfo) */, L_34, L_35, L_36);
	}

IL_00b7:
	{
		ILGenerator_t3869071517 * L_37 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_38 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ret_41();
		NullCheck(L_37);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(6 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_37, L_38);
		DynamicMethod_t2778460557 * L_39 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_40 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(CtorDelegate_t2720072665_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_39);
		Delegate_t3660574010 * L_41 = DynamicMethod_CreateDelegate_m2432808878(L_39, L_40, /*hidden argument*/NULL);
		V_0 = ((CtorDelegate_t2720072665 *)CastclassSealed(L_41, CtorDelegate_t2720072665_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		SafeDictionary_2_t2099217731 * L_42 = ((CacheResolver_t2193954381_StaticFields*)CacheResolver_t2193954381_il2cpp_TypeInfo_var->static_fields)->get_ConstructorCache_2();
		Type_t * L_43 = ___type;
		CtorDelegate_t2720072665 * L_44 = V_0;
		NullCheck(L_42);
		SafeDictionary_2_Add_m2117596466(L_42, L_43, L_44, /*hidden argument*/SafeDictionary_2_Add_m2117596466_MethodInfo_var);
		CtorDelegate_t2720072665 * L_45 = V_0;
		NullCheck(L_45);
		Il2CppObject * L_46 = CtorDelegate_Invoke_m4052220313(L_45, /*hidden argument*/NULL);
		return L_46;
	}
}
// SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap> SimpleJson.Reflection.CacheResolver::LoadMaps(System.Type)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* SafeDictionary_2_t2250466201_il2cpp_TypeInfo_var;
extern const MethodInfo* SafeDictionary_2_TryGetValue_m3671974100_MethodInfo_var;
extern const MethodInfo* SafeDictionary_2__ctor_m609132729_MethodInfo_var;
extern const MethodInfo* SafeDictionary_2_Add_m3838858265_MethodInfo_var;
extern const uint32_t CacheResolver_LoadMaps_m2248807851_MetadataUsageId;
extern "C"  SafeDictionary_2_t2250466201 * CacheResolver_LoadMaps_m2248807851 (CacheResolver_t2193954381 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CacheResolver_LoadMaps_m2248807851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SafeDictionary_2_t2250466201 * V_0 = NULL;
	{
		Type_t * L_0 = ___type;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Type_t * L_1 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_1) == ((Il2CppObject*)(Type_t *)L_2))))
		{
			goto IL_0012;
		}
	}

IL_0010:
	{
		return (SafeDictionary_2_t2250466201 *)NULL;
	}

IL_0012:
	{
		SafeDictionary_2_t1629611267 * L_3 = __this->get__memberMapsCache_1();
		Type_t * L_4 = ___type;
		NullCheck(L_3);
		bool L_5 = SafeDictionary_2_TryGetValue_m3671974100(L_3, L_4, (&V_0), /*hidden argument*/SafeDictionary_2_TryGetValue_m3671974100_MethodInfo_var);
		if (!L_5)
		{
			goto IL_0024;
		}
	}
	{
		SafeDictionary_2_t2250466201 * L_6 = V_0;
		return L_6;
	}

IL_0024:
	{
		SafeDictionary_2_t2250466201 * L_7 = (SafeDictionary_2_t2250466201 *)il2cpp_codegen_object_new(SafeDictionary_2_t2250466201_il2cpp_TypeInfo_var);
		SafeDictionary_2__ctor_m609132729(L_7, /*hidden argument*/SafeDictionary_2__ctor_m609132729_MethodInfo_var);
		V_0 = L_7;
		MemberMapLoader_t3073810362 * L_8 = __this->get__memberMapLoader_0();
		Type_t * L_9 = ___type;
		SafeDictionary_2_t2250466201 * L_10 = V_0;
		NullCheck(L_8);
		MemberMapLoader_Invoke_m1090529212(L_8, L_9, L_10, /*hidden argument*/NULL);
		SafeDictionary_2_t1629611267 * L_11 = __this->get__memberMapsCache_1();
		Type_t * L_12 = ___type;
		SafeDictionary_2_t2250466201 * L_13 = V_0;
		NullCheck(L_11);
		SafeDictionary_2_Add_m3838858265(L_11, L_12, L_13, /*hidden argument*/SafeDictionary_2_Add_m3838858265_MethodInfo_var);
		SafeDictionary_2_t2250466201 * L_14 = V_0;
		return L_14;
	}
}
// System.Reflection.Emit.DynamicMethod SimpleJson.Reflection.CacheResolver::CreateDynamicMethod(System.String,System.Type,System.Type[],System.Type)
extern TypeInfo* DynamicMethod_t2778460557_il2cpp_TypeInfo_var;
extern const uint32_t CacheResolver_CreateDynamicMethod_m3348281029_MetadataUsageId;
extern "C"  DynamicMethod_t2778460557 * CacheResolver_CreateDynamicMethod_m3348281029 (Il2CppObject * __this /* static, unused */, String_t* ___name, Type_t * ___returnType, TypeU5BU5D_t3431720054* ___parameterTypes, Type_t * ___owner, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CacheResolver_CreateDynamicMethod_m3348281029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DynamicMethod_t2778460557 * V_0 = NULL;
	DynamicMethod_t2778460557 * G_B3_0 = NULL;
	{
		Type_t * L_0 = ___owner;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(29 /* System.Boolean System.Type::get_IsInterface() */, L_0);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_2 = ___name;
		Type_t * L_3 = ___returnType;
		TypeU5BU5D_t3431720054* L_4 = ___parameterTypes;
		DynamicMethod_t2778460557 * L_5 = (DynamicMethod_t2778460557 *)il2cpp_codegen_object_new(DynamicMethod_t2778460557_il2cpp_TypeInfo_var);
		DynamicMethod__ctor_m3527141956(L_5, L_2, L_3, L_4, (Module_t206139610 *)NULL, (bool)1, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_001e;
	}

IL_0014:
	{
		String_t* L_6 = ___name;
		Type_t * L_7 = ___returnType;
		TypeU5BU5D_t3431720054* L_8 = ___parameterTypes;
		Type_t * L_9 = ___owner;
		DynamicMethod_t2778460557 * L_10 = (DynamicMethod_t2778460557 *)il2cpp_codegen_object_new(DynamicMethod_t2778460557_il2cpp_TypeInfo_var);
		DynamicMethod__ctor_m2648831853(L_10, L_6, L_7, L_8, L_9, (bool)1, /*hidden argument*/NULL);
		G_B3_0 = L_10;
	}

IL_001e:
	{
		V_0 = G_B3_0;
		DynamicMethod_t2778460557 * L_11 = V_0;
		return L_11;
	}
}
// SimpleJson.Reflection.GetHandler SimpleJson.Reflection.CacheResolver::CreateGetHandler(System.Reflection.FieldInfo)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern const Il2CppType* GetHandler_t3377211385_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* CacheResolver_t2193954381_il2cpp_TypeInfo_var;
extern TypeInfo* OpCodes_t2031828562_il2cpp_TypeInfo_var;
extern TypeInfo* GetHandler_t3377211385_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral71478;
extern const uint32_t CacheResolver_CreateGetHandler_m3079080210_MetadataUsageId;
extern "C"  GetHandler_t3377211385 * CacheResolver_CreateGetHandler_m3079080210 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CacheResolver_CreateGetHandler_m3079080210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	DynamicMethod_t2778460557 * V_1 = NULL;
	ILGenerator_t3869071517 * V_2 = NULL;
	TypeU5BU5D_t3431720054* V_3 = NULL;
	{
		FieldInfo_t * L_0 = ___fieldInfo;
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_0);
		V_0 = L_1;
		FieldInfo_t * L_2 = ___fieldInfo;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral71478, L_3, /*hidden argument*/NULL);
		FieldInfo_t * L_5 = ___fieldInfo;
		NullCheck(L_5);
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_5);
		V_3 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)1));
		TypeU5BU5D_t3431720054* L_7 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_8);
		TypeU5BU5D_t3431720054* L_9 = V_3;
		FieldInfo_t * L_10 = ___fieldInfo;
		NullCheck(L_10);
		Type_t * L_11 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		DynamicMethod_t2778460557 * L_12 = CacheResolver_CreateDynamicMethod_m3348281029(NULL /*static, unused*/, L_4, L_6, L_9, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		DynamicMethod_t2778460557 * L_13 = V_1;
		NullCheck(L_13);
		ILGenerator_t3869071517 * L_14 = DynamicMethod_GetILGenerator_m3180908743(L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		ILGenerator_t3869071517 * L_15 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_16 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ldarg_0_2();
		NullCheck(L_15);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(6 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_15, L_16);
		ILGenerator_t3869071517 * L_17 = V_2;
		OpCode_t4028977979  L_18 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ldfld_120();
		FieldInfo_t * L_19 = ___fieldInfo;
		NullCheck(L_17);
		VirtActionInvoker2< OpCode_t4028977979 , FieldInfo_t * >::Invoke(8 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.FieldInfo) */, L_17, L_18, L_19);
		Type_t * L_20 = V_0;
		NullCheck(L_20);
		bool L_21 = VirtFuncInvoker0< bool >::Invoke(35 /* System.Boolean System.Type::get_IsValueType() */, L_20);
		if (!L_21)
		{
			goto IL_0070;
		}
	}
	{
		ILGenerator_t3869071517 * L_22 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_23 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Box_137();
		Type_t * L_24 = V_0;
		NullCheck(L_22);
		VirtActionInvoker2< OpCode_t4028977979 , Type_t * >::Invoke(10 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Type) */, L_22, L_23, L_24);
	}

IL_0070:
	{
		ILGenerator_t3869071517 * L_25 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_26 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ret_41();
		NullCheck(L_25);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(6 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_25, L_26);
		DynamicMethod_t2778460557 * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GetHandler_t3377211385_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_27);
		Delegate_t3660574010 * L_29 = DynamicMethod_CreateDelegate_m2432808878(L_27, L_28, /*hidden argument*/NULL);
		return ((GetHandler_t3377211385 *)CastclassSealed(L_29, GetHandler_t3377211385_il2cpp_TypeInfo_var));
	}
}
// SimpleJson.Reflection.SetHandler SimpleJson.Reflection.CacheResolver::CreateSetHandler(System.Reflection.FieldInfo)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern const Il2CppType* SetHandler_t1019049325_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* CacheResolver_t2193954381_il2cpp_TypeInfo_var;
extern TypeInfo* OpCodes_t2031828562_il2cpp_TypeInfo_var;
extern TypeInfo* SetHandler_t1019049325_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral83010;
extern const uint32_t CacheResolver_CreateSetHandler_m3885692458_MetadataUsageId;
extern "C"  SetHandler_t1019049325 * CacheResolver_CreateSetHandler_m3885692458 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CacheResolver_CreateSetHandler_m3885692458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	DynamicMethod_t2778460557 * V_1 = NULL;
	ILGenerator_t3869071517 * V_2 = NULL;
	TypeU5BU5D_t3431720054* V_3 = NULL;
	{
		FieldInfo_t * L_0 = ___fieldInfo;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Reflection.FieldInfo::get_IsInitOnly() */, L_0);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		FieldInfo_t * L_2 = ___fieldInfo;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(18 /* System.Boolean System.Reflection.FieldInfo::get_IsLiteral() */, L_2);
		if (!L_3)
		{
			goto IL_0012;
		}
	}

IL_0010:
	{
		return (SetHandler_t1019049325 *)NULL;
	}

IL_0012:
	{
		FieldInfo_t * L_4 = ___fieldInfo;
		NullCheck(L_4);
		Type_t * L_5 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_4);
		V_0 = L_5;
		FieldInfo_t * L_6 = ___fieldInfo;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral83010, L_7, /*hidden argument*/NULL);
		V_3 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)2));
		TypeU5BU5D_t3431720054* L_9 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_10);
		TypeU5BU5D_t3431720054* L_11 = V_3;
		Type_t * L_12 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_12);
		TypeU5BU5D_t3431720054* L_13 = V_3;
		FieldInfo_t * L_14 = ___fieldInfo;
		NullCheck(L_14);
		Type_t * L_15 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		DynamicMethod_t2778460557 * L_16 = CacheResolver_CreateDynamicMethod_m3348281029(NULL /*static, unused*/, L_8, (Type_t *)NULL, L_13, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		DynamicMethod_t2778460557 * L_17 = V_1;
		NullCheck(L_17);
		ILGenerator_t3869071517 * L_18 = DynamicMethod_GetILGenerator_m3180908743(L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		ILGenerator_t3869071517 * L_19 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_20 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ldarg_0_2();
		NullCheck(L_19);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(6 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_19, L_20);
		ILGenerator_t3869071517 * L_21 = V_2;
		OpCode_t4028977979  L_22 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ldarg_1_3();
		NullCheck(L_21);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(6 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_21, L_22);
		Type_t * L_23 = V_0;
		NullCheck(L_23);
		bool L_24 = VirtFuncInvoker0< bool >::Invoke(35 /* System.Boolean System.Type::get_IsValueType() */, L_23);
		if (!L_24)
		{
			goto IL_0089;
		}
	}
	{
		ILGenerator_t3869071517 * L_25 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_26 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Unbox_Any_162();
		Type_t * L_27 = V_0;
		NullCheck(L_25);
		VirtActionInvoker2< OpCode_t4028977979 , Type_t * >::Invoke(10 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Type) */, L_25, L_26, L_27);
	}

IL_0089:
	{
		ILGenerator_t3869071517 * L_28 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_29 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Stfld_122();
		FieldInfo_t * L_30 = ___fieldInfo;
		NullCheck(L_28);
		VirtActionInvoker2< OpCode_t4028977979 , FieldInfo_t * >::Invoke(8 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.FieldInfo) */, L_28, L_29, L_30);
		ILGenerator_t3869071517 * L_31 = V_2;
		OpCode_t4028977979  L_32 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ret_41();
		NullCheck(L_31);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(6 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_31, L_32);
		DynamicMethod_t2778460557 * L_33 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_34 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(SetHandler_t1019049325_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_33);
		Delegate_t3660574010 * L_35 = DynamicMethod_CreateDelegate_m2432808878(L_33, L_34, /*hidden argument*/NULL);
		return ((SetHandler_t1019049325 *)CastclassSealed(L_35, SetHandler_t1019049325_il2cpp_TypeInfo_var));
	}
}
// SimpleJson.Reflection.GetHandler SimpleJson.Reflection.CacheResolver::CreateGetHandler(System.Reflection.PropertyInfo)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern const Il2CppType* GetHandler_t3377211385_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* CacheResolver_t2193954381_il2cpp_TypeInfo_var;
extern TypeInfo* OpCodes_t2031828562_il2cpp_TypeInfo_var;
extern TypeInfo* GetHandler_t3377211385_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral71478;
extern const uint32_t CacheResolver_CreateGetHandler_m383375893_MetadataUsageId;
extern "C"  GetHandler_t3377211385 * CacheResolver_CreateGetHandler_m383375893 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CacheResolver_CreateGetHandler_m383375893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	DynamicMethod_t2778460557 * V_2 = NULL;
	ILGenerator_t3869071517 * V_3 = NULL;
	TypeU5BU5D_t3431720054* V_4 = NULL;
	{
		PropertyInfo_t * L_0 = ___propertyInfo;
		NullCheck(L_0);
		MethodInfo_t * L_1 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(19 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetGetMethod(System.Boolean) */, L_0, (bool)1);
		V_0 = L_1;
		MethodInfo_t * L_2 = V_0;
		if (L_2)
		{
			goto IL_000d;
		}
	}
	{
		return (GetHandler_t3377211385 *)NULL;
	}

IL_000d:
	{
		PropertyInfo_t * L_3 = ___propertyInfo;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_3);
		V_1 = L_4;
		PropertyInfo_t * L_5 = ___propertyInfo;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral71478, L_6, /*hidden argument*/NULL);
		PropertyInfo_t * L_8 = ___propertyInfo;
		NullCheck(L_8);
		Type_t * L_9 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_8);
		V_4 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)1));
		TypeU5BU5D_t3431720054* L_10 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_11);
		TypeU5BU5D_t3431720054* L_12 = V_4;
		PropertyInfo_t * L_13 = ___propertyInfo;
		NullCheck(L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_13);
		IL2CPP_RUNTIME_CLASS_INIT(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		DynamicMethod_t2778460557 * L_15 = CacheResolver_CreateDynamicMethod_m3348281029(NULL /*static, unused*/, L_7, L_9, L_12, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		DynamicMethod_t2778460557 * L_16 = V_2;
		NullCheck(L_16);
		ILGenerator_t3869071517 * L_17 = DynamicMethod_GetILGenerator_m3180908743(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		ILGenerator_t3869071517 * L_18 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_19 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ldarg_0_2();
		NullCheck(L_18);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(6 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_18, L_19);
		ILGenerator_t3869071517 * L_20 = V_3;
		OpCode_t4028977979  L_21 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Call_39();
		MethodInfo_t * L_22 = V_0;
		NullCheck(L_20);
		VirtActionInvoker2< OpCode_t4028977979 , MethodInfo_t * >::Invoke(9 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.MethodInfo) */, L_20, L_21, L_22);
		Type_t * L_23 = V_1;
		NullCheck(L_23);
		bool L_24 = VirtFuncInvoker0< bool >::Invoke(35 /* System.Boolean System.Type::get_IsValueType() */, L_23);
		if (!L_24)
		{
			goto IL_0080;
		}
	}
	{
		ILGenerator_t3869071517 * L_25 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_26 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Box_137();
		Type_t * L_27 = V_1;
		NullCheck(L_25);
		VirtActionInvoker2< OpCode_t4028977979 , Type_t * >::Invoke(10 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Type) */, L_25, L_26, L_27);
	}

IL_0080:
	{
		ILGenerator_t3869071517 * L_28 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_29 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ret_41();
		NullCheck(L_28);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(6 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_28, L_29);
		DynamicMethod_t2778460557 * L_30 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_31 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GetHandler_t3377211385_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_30);
		Delegate_t3660574010 * L_32 = DynamicMethod_CreateDelegate_m2432808878(L_30, L_31, /*hidden argument*/NULL);
		return ((GetHandler_t3377211385 *)CastclassSealed(L_32, GetHandler_t3377211385_il2cpp_TypeInfo_var));
	}
}
// SimpleJson.Reflection.SetHandler SimpleJson.Reflection.CacheResolver::CreateSetHandler(System.Reflection.PropertyInfo)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern const Il2CppType* SetHandler_t1019049325_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* CacheResolver_t2193954381_il2cpp_TypeInfo_var;
extern TypeInfo* OpCodes_t2031828562_il2cpp_TypeInfo_var;
extern TypeInfo* SetHandler_t1019049325_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral83010;
extern const uint32_t CacheResolver_CreateSetHandler_m4121802237_MetadataUsageId;
extern "C"  SetHandler_t1019049325 * CacheResolver_CreateSetHandler_m4121802237 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CacheResolver_CreateSetHandler_m4121802237_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	DynamicMethod_t2778460557 * V_2 = NULL;
	ILGenerator_t3869071517 * V_3 = NULL;
	TypeU5BU5D_t3431720054* V_4 = NULL;
	{
		PropertyInfo_t * L_0 = ___propertyInfo;
		NullCheck(L_0);
		MethodInfo_t * L_1 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(21 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetSetMethod(System.Boolean) */, L_0, (bool)1);
		V_0 = L_1;
		MethodInfo_t * L_2 = V_0;
		if (L_2)
		{
			goto IL_000d;
		}
	}
	{
		return (SetHandler_t1019049325 *)NULL;
	}

IL_000d:
	{
		PropertyInfo_t * L_3 = ___propertyInfo;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_3);
		V_1 = L_4;
		PropertyInfo_t * L_5 = ___propertyInfo;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral83010, L_6, /*hidden argument*/NULL);
		V_4 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)2));
		TypeU5BU5D_t3431720054* L_8 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_9);
		TypeU5BU5D_t3431720054* L_10 = V_4;
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_11);
		TypeU5BU5D_t3431720054* L_12 = V_4;
		PropertyInfo_t * L_13 = ___propertyInfo;
		NullCheck(L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_13);
		IL2CPP_RUNTIME_CLASS_INIT(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		DynamicMethod_t2778460557 * L_15 = CacheResolver_CreateDynamicMethod_m3348281029(NULL /*static, unused*/, L_7, (Type_t *)NULL, L_12, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		DynamicMethod_t2778460557 * L_16 = V_2;
		NullCheck(L_16);
		ILGenerator_t3869071517 * L_17 = DynamicMethod_GetILGenerator_m3180908743(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		ILGenerator_t3869071517 * L_18 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_19 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ldarg_0_2();
		NullCheck(L_18);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(6 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_18, L_19);
		ILGenerator_t3869071517 * L_20 = V_3;
		OpCode_t4028977979  L_21 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ldarg_1_3();
		NullCheck(L_20);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(6 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_20, L_21);
		Type_t * L_22 = V_1;
		NullCheck(L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(35 /* System.Boolean System.Type::get_IsValueType() */, L_22);
		if (!L_23)
		{
			goto IL_0088;
		}
	}
	{
		ILGenerator_t3869071517 * L_24 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_25 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Unbox_Any_162();
		Type_t * L_26 = V_1;
		NullCheck(L_24);
		VirtActionInvoker2< OpCode_t4028977979 , Type_t * >::Invoke(10 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Type) */, L_24, L_25, L_26);
	}

IL_0088:
	{
		ILGenerator_t3869071517 * L_27 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(OpCodes_t2031828562_il2cpp_TypeInfo_var);
		OpCode_t4028977979  L_28 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Call_39();
		MethodInfo_t * L_29 = V_0;
		NullCheck(L_27);
		VirtActionInvoker2< OpCode_t4028977979 , MethodInfo_t * >::Invoke(9 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.MethodInfo) */, L_27, L_28, L_29);
		ILGenerator_t3869071517 * L_30 = V_3;
		OpCode_t4028977979  L_31 = ((OpCodes_t2031828562_StaticFields*)OpCodes_t2031828562_il2cpp_TypeInfo_var->static_fields)->get_Ret_41();
		NullCheck(L_30);
		VirtActionInvoker1< OpCode_t4028977979  >::Invoke(6 /* System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode) */, L_30, L_31);
		DynamicMethod_t2778460557 * L_32 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(SetHandler_t1019049325_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_32);
		Delegate_t3660574010 * L_34 = DynamicMethod_CreateDelegate_m2432808878(L_32, L_33, /*hidden argument*/NULL);
		return ((SetHandler_t1019049325 *)CastclassSealed(L_34, SetHandler_t1019049325_il2cpp_TypeInfo_var));
	}
}
// System.Void SimpleJson.Reflection.CacheResolver::.cctor()
extern TypeInfo* SafeDictionary_2_t2099217731_il2cpp_TypeInfo_var;
extern TypeInfo* CacheResolver_t2193954381_il2cpp_TypeInfo_var;
extern const MethodInfo* SafeDictionary_2__ctor_m1982222051_MethodInfo_var;
extern const uint32_t CacheResolver__cctor_m4243496679_MetadataUsageId;
extern "C"  void CacheResolver__cctor_m4243496679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CacheResolver__cctor_m4243496679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SafeDictionary_2_t2099217731 * L_0 = (SafeDictionary_2_t2099217731 *)il2cpp_codegen_object_new(SafeDictionary_2_t2099217731_il2cpp_TypeInfo_var);
		SafeDictionary_2__ctor_m1982222051(L_0, /*hidden argument*/SafeDictionary_2__ctor_m1982222051_MethodInfo_var);
		((CacheResolver_t2193954381_StaticFields*)CacheResolver_t2193954381_il2cpp_TypeInfo_var->static_fields)->set_ConstructorCache_2(L_0);
		return;
	}
}
// System.Void SimpleJson.Reflection.CacheResolver/CtorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void CtorDelegate__ctor_m1466384714 (CtorDelegate_t2720072665 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Object SimpleJson.Reflection.CacheResolver/CtorDelegate::Invoke()
extern "C"  Il2CppObject * CtorDelegate_Invoke_m4052220313 (CtorDelegate_t2720072665 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CtorDelegate_Invoke_m4052220313((CtorDelegate_t2720072665 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" Il2CppObject * pinvoke_delegate_wrapper_CtorDelegate_t2720072665(Il2CppObject* delegate)
{
	typedef Il2CppObject * (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation and marshaling of return value back from native representation
	Il2CppObject * _return_value = _il2cpp_pinvoke_func();
	Il2CppObject * __return_value_unmarshaled = NULL;
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	return __return_value_unmarshaled;
}
// System.IAsyncResult SimpleJson.Reflection.CacheResolver/CtorDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CtorDelegate_BeginInvoke_m4237814367 (CtorDelegate_t2720072665 * __this, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Object SimpleJson.Reflection.CacheResolver/CtorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CtorDelegate_EndInvoke_m3633296911 (CtorDelegate_t2720072665 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Il2CppObject *)__result;
}
// System.Void SimpleJson.Reflection.CacheResolver/MemberMap::.ctor(System.Reflection.PropertyInfo)
extern TypeInfo* CacheResolver_t2193954381_il2cpp_TypeInfo_var;
extern const uint32_t MemberMap__ctor_m3239711532_MetadataUsageId;
extern "C"  void MemberMap__ctor_m3239711532 (MemberMap_t3346170946 * __this, PropertyInfo_t * ___propertyInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MemberMap__ctor_m3239711532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		PropertyInfo_t * L_0 = ___propertyInfo;
		__this->set_MemberInfo_0(L_0);
		PropertyInfo_t * L_1 = ___propertyInfo;
		NullCheck(L_1);
		Type_t * L_2 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_1);
		__this->set_Type_1(L_2);
		PropertyInfo_t * L_3 = ___propertyInfo;
		IL2CPP_RUNTIME_CLASS_INIT(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		GetHandler_t3377211385 * L_4 = CacheResolver_CreateGetHandler_m383375893(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_Getter_2(L_4);
		PropertyInfo_t * L_5 = ___propertyInfo;
		SetHandler_t1019049325 * L_6 = CacheResolver_CreateSetHandler_m4121802237(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_Setter_3(L_6);
		return;
	}
}
// System.Void SimpleJson.Reflection.CacheResolver/MemberMap::.ctor(System.Reflection.FieldInfo)
extern TypeInfo* CacheResolver_t2193954381_il2cpp_TypeInfo_var;
extern const uint32_t MemberMap__ctor_m3335798619_MetadataUsageId;
extern "C"  void MemberMap__ctor_m3335798619 (MemberMap_t3346170946 * __this, FieldInfo_t * ___fieldInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MemberMap__ctor_m3335798619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		FieldInfo_t * L_0 = ___fieldInfo;
		__this->set_MemberInfo_0(L_0);
		FieldInfo_t * L_1 = ___fieldInfo;
		NullCheck(L_1);
		Type_t * L_2 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_1);
		__this->set_Type_1(L_2);
		FieldInfo_t * L_3 = ___fieldInfo;
		IL2CPP_RUNTIME_CLASS_INIT(CacheResolver_t2193954381_il2cpp_TypeInfo_var);
		GetHandler_t3377211385 * L_4 = CacheResolver_CreateGetHandler_m3079080210(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_Getter_2(L_4);
		FieldInfo_t * L_5 = ___fieldInfo;
		SetHandler_t1019049325 * L_6 = CacheResolver_CreateSetHandler_m3885692458(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_Setter_3(L_6);
		return;
	}
}
// System.Void SimpleJson.Reflection.GetHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void GetHandler__ctor_m3294234494 (GetHandler_t3377211385 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Object SimpleJson.Reflection.GetHandler::Invoke(System.Object)
extern "C"  Il2CppObject * GetHandler_Invoke_m865915879 (GetHandler_t3377211385 * __this, Il2CppObject * ___source, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetHandler_Invoke_m865915879((GetHandler_t3377211385 *)__this->get_prev_9(),___source, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___source, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___source,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___source, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___source,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___source,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" Il2CppObject * pinvoke_delegate_wrapper_GetHandler_t3377211385(Il2CppObject* delegate, Il2CppObject * ___source)
{
	// Marshaling of parameter '___source' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult SimpleJson.Reflection.GetHandler::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetHandler_BeginInvoke_m2440018781 (GetHandler_t3377211385 * __this, Il2CppObject * ___source, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___source;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Object SimpleJson.Reflection.GetHandler::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * GetHandler_EndInvoke_m1813626947 (GetHandler_t3377211385 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Il2CppObject *)__result;
}
// System.Void SimpleJson.Reflection.MemberMapLoader::.ctor(System.Object,System.IntPtr)
extern "C"  void MemberMapLoader__ctor_m4182933641 (MemberMapLoader_t3073810362 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Void SimpleJson.Reflection.MemberMapLoader::Invoke(System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>)
extern "C"  void MemberMapLoader_Invoke_m1090529212 (MemberMapLoader_t3073810362 * __this, Type_t * ___type, SafeDictionary_2_t2250466201 * ___memberMaps, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		MemberMapLoader_Invoke_m1090529212((MemberMapLoader_t3073810362 *)__this->get_prev_9(),___type, ___memberMaps, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Type_t * ___type, SafeDictionary_2_t2250466201 * ___memberMaps, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___type, ___memberMaps,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Type_t * ___type, SafeDictionary_2_t2250466201 * ___memberMaps, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___type, ___memberMaps,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, SafeDictionary_2_t2250466201 * ___memberMaps, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___type, ___memberMaps,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult SimpleJson.Reflection.MemberMapLoader::BeginInvoke(System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MemberMapLoader_BeginInvoke_m2496604015 (MemberMapLoader_t3073810362 * __this, Type_t * ___type, SafeDictionary_2_t2250466201 * ___memberMaps, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___type;
	__d_args[1] = ___memberMaps;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void SimpleJson.Reflection.MemberMapLoader::EndInvoke(System.IAsyncResult)
extern "C"  void MemberMapLoader_EndInvoke_m2416287513 (MemberMapLoader_t3073810362 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Attribute SimpleJson.Reflection.ReflectionUtils::GetAttribute(System.Reflection.MemberInfo,System.Type)
extern "C"  Attribute_t498693649 * ReflectionUtils_GetAttribute_m2472672625 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___info, Type_t * ___type, const MethodInfo* method)
{
	{
		MemberInfo_t * L_0 = ___info;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Type_t * L_1 = ___type;
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		MemberInfo_t * L_2 = ___info;
		Type_t * L_3 = ___type;
		bool L_4 = Attribute_IsDefined_m3508553943(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0011;
		}
	}

IL_000f:
	{
		return (Attribute_t498693649 *)NULL;
	}

IL_0011:
	{
		MemberInfo_t * L_5 = ___info;
		Type_t * L_6 = ___type;
		Attribute_t498693649 * L_7 = Attribute_GetCustomAttribute_m506754809(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Attribute SimpleJson.Reflection.ReflectionUtils::GetAttribute(System.Type,System.Type)
extern "C"  Attribute_t498693649 * ReflectionUtils_GetAttribute_m3007953666 (Il2CppObject * __this /* static, unused */, Type_t * ___objectType, Type_t * ___attributeType, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___objectType;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Type_t * L_1 = ___attributeType;
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		Type_t * L_2 = ___objectType;
		Type_t * L_3 = ___attributeType;
		bool L_4 = Attribute_IsDefined_m3508553943(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0011;
		}
	}

IL_000f:
	{
		return (Attribute_t498693649 *)NULL;
	}

IL_0011:
	{
		Type_t * L_5 = ___objectType;
		Type_t * L_6 = ___attributeType;
		Attribute_t498693649 * L_7 = Attribute_GetCustomAttribute_m506754809(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils::IsTypeGenericeCollectionInterface(System.Type)
extern const Il2CppType* IList_1_t838940445_0_0_0_var;
extern const Il2CppType* ICollection_1_t56169053_0_0_0_var;
extern const Il2CppType* IEnumerable_1_t2707313927_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtils_IsTypeGenericeCollectionInterface_m1283721406_MetadataUsageId;
extern "C"  bool ReflectionUtils_IsTypeGenericeCollectionInterface_m1283721406 (Il2CppObject * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_IsTypeGenericeCollectionInterface_m1283721406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		Type_t * L_0 = ___type;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(81 /* System.Boolean System.Type::get_IsGenericType() */, L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return (bool)0;
	}

IL_000a:
	{
		Type_t * L_2 = ___type;
		NullCheck(L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(80 /* System.Type System.Type::GetGenericTypeDefinition() */, L_2);
		V_0 = L_3;
		Type_t * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IList_1_t838940445_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_0039;
		}
	}
	{
		Type_t * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ICollection_1_t56169053_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0039;
		}
	}
	{
		Type_t * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IEnumerable_1_t2707313927_0_0_0_var), /*hidden argument*/NULL);
		return (bool)((((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))? 1 : 0);
	}

IL_0039:
	{
		return (bool)1;
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils::IsTypeDictionary(System.Type)
extern const Il2CppType* IDictionary_t1654916945_0_0_0_var;
extern const Il2CppType* IDictionary_2_t2833266262_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtils_IsTypeDictionary_m3763562223_MetadataUsageId;
extern "C"  bool ReflectionUtils_IsTypeDictionary_m3763562223 (Il2CppObject * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_IsTypeDictionary_m3763562223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IDictionary_t1654916945_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_1 = ___type;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0014;
		}
	}
	{
		return (bool)1;
	}

IL_0014:
	{
		Type_t * L_3 = ___type;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(81 /* System.Boolean System.Type::get_IsGenericType() */, L_3);
		if (L_4)
		{
			goto IL_001e;
		}
	}
	{
		return (bool)0;
	}

IL_001e:
	{
		Type_t * L_5 = ___type;
		NullCheck(L_5);
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(80 /* System.Type System.Type::GetGenericTypeDefinition() */, L_5);
		V_0 = L_6;
		Type_t * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IDictionary_2_t2833266262_0_0_0_var), /*hidden argument*/NULL);
		return (bool)((((Il2CppObject*)(Type_t *)L_7) == ((Il2CppObject*)(Type_t *)L_8))? 1 : 0);
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils::IsNullableType(System.Type)
extern const Il2CppType* Nullable_1_t2791603879_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtils_IsNullableType_m2201186244_MetadataUsageId;
extern "C"  bool ReflectionUtils_IsNullableType_m2201186244 (Il2CppObject * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_IsNullableType_m2201186244_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___type;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(81 /* System.Boolean System.Type::get_IsGenericType() */, L_0);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Type_t * L_2 = ___type;
		NullCheck(L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(80 /* System.Type System.Type::GetGenericTypeDefinition() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Nullable_1_t2791603879_0_0_0_var), /*hidden argument*/NULL);
		return (bool)((((Il2CppObject*)(Type_t *)L_3) == ((Il2CppObject*)(Type_t *)L_4))? 1 : 0);
	}

IL_001b:
	{
		return (bool)0;
	}
}
// System.Object SimpleJson.Reflection.ReflectionUtils::ToNullableType(System.Object,System.Type)
extern TypeInfo* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtils_ToNullableType_m4235305546_MetadataUsageId;
extern "C"  Il2CppObject * ReflectionUtils_ToNullableType_m4235305546 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj, Type_t * ___nullableType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_ToNullableType_m4235305546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_1 = ___obj;
		Type_t * L_2 = ___nullableType;
		Type_t * L_3 = Nullable_GetUnderlyingType_m3559493248(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_4 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		Il2CppObject * L_5 = Convert_ChangeType_m39028498(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0015:
	{
		return NULL;
	}
}
// System.Void SimpleJson.Reflection.SetHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SetHandler__ctor_m1978968818 (SetHandler_t1019049325 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Void SimpleJson.Reflection.SetHandler::Invoke(System.Object,System.Object)
extern "C"  void SetHandler_Invoke_m2910967830 (SetHandler_t1019049325 * __this, Il2CppObject * ___source, Il2CppObject * ___value, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SetHandler_Invoke_m2910967830((SetHandler_t1019049325 *)__this->get_prev_9(),___source, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___source, Il2CppObject * ___value, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___source, ___value,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___source, Il2CppObject * ___value, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___source, ___value,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___value, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___source, ___value,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_SetHandler_t1019049325(Il2CppObject* delegate, Il2CppObject * ___source, Il2CppObject * ___value)
{
	// Marshaling of parameter '___source' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult SimpleJson.Reflection.SetHandler::BeginInvoke(System.Object,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SetHandler_BeginInvoke_m362632603 (SetHandler_t1019049325 * __this, Il2CppObject * ___source, Il2CppObject * ___value, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___source;
	__d_args[1] = ___value;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void SimpleJson.Reflection.SetHandler::EndInvoke(System.IAsyncResult)
extern "C"  void SetHandler_EndInvoke_m304571906 (SetHandler_t1019049325 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Object SimpleJson.SimpleJson::DeserializeObject(System.String)
extern TypeInfo* SerializationException_t731558744_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2114206080;
extern const uint32_t SimpleJson_DeserializeObject_m2643007008_MetadataUsageId;
extern "C"  Il2CppObject * SimpleJson_DeserializeObject_m2643007008 (Il2CppObject * __this /* static, unused */, String_t* ___json, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_DeserializeObject_m2643007008_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		String_t* L_0 = ___json;
		bool L_1 = SimpleJson_TryDeserializeObject_m2228521542(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000c;
		}
	}
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}

IL_000c:
	{
		SerializationException_t731558744 * L_3 = (SerializationException_t731558744 *)il2cpp_codegen_object_new(SerializationException_t731558744_il2cpp_TypeInfo_var);
		SerializationException__ctor_m4216356480(L_3, _stringLiteral2114206080, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Boolean SimpleJson.SimpleJson::TryDeserializeObject(System.String,System.Object&)
extern "C"  bool SimpleJson_TryDeserializeObject_m2228521542 (Il2CppObject * __this /* static, unused */, String_t* ___json, Il2CppObject ** ___object, const MethodInfo* method)
{
	bool V_0 = false;
	CharU5BU5D_t3416858730* V_1 = NULL;
	int32_t V_2 = 0;
	{
		V_0 = (bool)1;
		String_t* L_0 = ___json;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = ___json;
		NullCheck(L_1);
		CharU5BU5D_t3416858730* L_2 = String_ToCharArray_m1208288742(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		V_2 = 0;
		Il2CppObject ** L_3 = ___object;
		CharU5BU5D_t3416858730* L_4 = V_1;
		Il2CppObject * L_5 = SimpleJson_ParseValue_m1670953315(NULL /*static, unused*/, L_4, (&V_2), (&V_0), /*hidden argument*/NULL);
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)L_5;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)L_5);
		goto IL_001f;
	}

IL_001c:
	{
		Il2CppObject ** L_6 = ___object;
		*((Il2CppObject **)(L_6)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_6), (Il2CppObject *)NULL);
	}

IL_001f:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Object SimpleJson.SimpleJson::DeserializeObject(System.String,System.Type,SimpleJson.IJsonSerializerStrategy)
extern TypeInfo* IJsonSerializerStrategy_t4110884692_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_DeserializeObject_m1396843921_MetadataUsageId;
extern "C"  Il2CppObject * SimpleJson_DeserializeObject_m1396843921 (Il2CppObject * __this /* static, unused */, String_t* ___json, Type_t * ___type, Il2CppObject * ___jsonSerializerStrategy, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_DeserializeObject_m1396843921_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * G_B5_0 = NULL;
	Il2CppObject * G_B4_0 = NULL;
	{
		String_t* L_0 = ___json;
		Il2CppObject * L_1 = SimpleJson_DeserializeObject_m2643007008(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Type_t * L_2 = ___type;
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck(L_4);
		Type_t * L_5 = Object_GetType_m2022236990(L_4, /*hidden argument*/NULL);
		Type_t * L_6 = ___type;
		NullCheck(L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_5, L_6);
		if (L_7)
		{
			goto IL_002d;
		}
	}

IL_001b:
	{
		Il2CppObject * L_8 = ___jsonSerializerStrategy;
		Il2CppObject * L_9 = L_8;
		G_B4_0 = L_9;
		if (L_9)
		{
			G_B5_0 = L_9;
			goto IL_0025;
		}
	}
	{
		Il2CppObject * L_10 = SimpleJson_get_CurrentJsonSerializerStrategy_m2508654719(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_10;
	}

IL_0025:
	{
		Il2CppObject * L_11 = V_0;
		Type_t * L_12 = ___type;
		NullCheck(G_B5_0);
		Il2CppObject * L_13 = InterfaceFuncInvoker2< Il2CppObject *, Il2CppObject *, Type_t * >::Invoke(1 /* System.Object SimpleJson.IJsonSerializerStrategy::DeserializeObject(System.Object,System.Type) */, IJsonSerializerStrategy_t4110884692_il2cpp_TypeInfo_var, G_B5_0, L_11, L_12);
		return L_13;
	}

IL_002d:
	{
		Il2CppObject * L_14 = V_0;
		return L_14;
	}
}
// System.String SimpleJson.SimpleJson::SerializeObject(System.Object,SimpleJson.IJsonSerializerStrategy)
extern TypeInfo* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_SerializeObject_m3542174305_MetadataUsageId;
extern "C"  String_t* SimpleJson_SerializeObject_m3542174305 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json, Il2CppObject * ___jsonSerializerStrategy, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_SerializeObject_m3542174305_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	bool V_1 = false;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_0, ((int32_t)2000), /*hidden argument*/NULL);
		V_0 = L_0;
		Il2CppObject * L_1 = ___jsonSerializerStrategy;
		Il2CppObject * L_2 = ___json;
		StringBuilder_t3822575854 * L_3 = V_0;
		bool L_4 = SimpleJson_SerializeValue_m2154484296(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (L_5)
		{
			goto IL_0019;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0019:
	{
		StringBuilder_t3822575854 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		return L_7;
	}
}
// System.String SimpleJson.SimpleJson::SerializeObject(System.Object)
extern "C"  String_t* SimpleJson_SerializeObject_m2690171523 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___json;
		Il2CppObject * L_1 = SimpleJson_get_CurrentJsonSerializerStrategy_m2508654719(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = SimpleJson_SerializeObject_m3542174305(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.Generic.IDictionary`2<System.String,System.Object> SimpleJson.SimpleJson::ParseObject(System.Char[],System.Int32&,System.Boolean&)
extern TypeInfo* JsonObject_t4190420965_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t3650470111_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_ParseObject_m2573007585_MetadataUsageId;
extern "C"  Il2CppObject* SimpleJson_ParseObject_m2573007585 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, bool* ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_ParseObject_m2573007585_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	String_t* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	{
		JsonObject_t4190420965 * L_0 = (JsonObject_t4190420965 *)il2cpp_codegen_object_new(JsonObject_t4190420965_il2cpp_TypeInfo_var);
		JsonObject__ctor_m3751193732(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t3416858730* L_1 = ___json;
		int32_t* L_2 = ___index;
		SimpleJson_NextToken_m2355341557(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_2 = (bool)0;
		goto IL_007e;
	}

IL_0012:
	{
		CharU5BU5D_t3416858730* L_3 = ___json;
		int32_t* L_4 = ___index;
		int32_t L_5 = SimpleJson_LookAhead_m3509516115(NULL /*static, unused*/, L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (L_6)
		{
			goto IL_0023;
		}
	}
	{
		bool* L_7 = ___success;
		*((int8_t*)(L_7)) = (int8_t)0;
		return (Il2CppObject*)NULL;
	}

IL_0023:
	{
		int32_t L_8 = V_1;
		if ((!(((uint32_t)L_8) == ((uint32_t)6))))
		{
			goto IL_0031;
		}
	}
	{
		CharU5BU5D_t3416858730* L_9 = ___json;
		int32_t* L_10 = ___index;
		SimpleJson_NextToken_m2355341557(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		goto IL_007e;
	}

IL_0031:
	{
		int32_t L_11 = V_1;
		if ((!(((uint32_t)L_11) == ((uint32_t)2))))
		{
			goto IL_003f;
		}
	}
	{
		CharU5BU5D_t3416858730* L_12 = ___json;
		int32_t* L_13 = ___index;
		SimpleJson_NextToken_m2355341557(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Il2CppObject* L_14 = V_0;
		return L_14;
	}

IL_003f:
	{
		CharU5BU5D_t3416858730* L_15 = ___json;
		int32_t* L_16 = ___index;
		bool* L_17 = ___success;
		String_t* L_18 = SimpleJson_ParseString_m3765835561(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		bool* L_19 = ___success;
		if ((*((int8_t*)L_19)))
		{
			goto IL_0051;
		}
	}
	{
		bool* L_20 = ___success;
		*((int8_t*)(L_20)) = (int8_t)0;
		return (Il2CppObject*)NULL;
	}

IL_0051:
	{
		CharU5BU5D_t3416858730* L_21 = ___json;
		int32_t* L_22 = ___index;
		int32_t L_23 = SimpleJson_NextToken_m2355341557(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) == ((int32_t)5)))
		{
			goto IL_0062;
		}
	}
	{
		bool* L_25 = ___success;
		*((int8_t*)(L_25)) = (int8_t)0;
		return (Il2CppObject*)NULL;
	}

IL_0062:
	{
		CharU5BU5D_t3416858730* L_26 = ___json;
		int32_t* L_27 = ___index;
		bool* L_28 = ___success;
		Il2CppObject * L_29 = SimpleJson_ParseValue_m1670953315(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		V_4 = L_29;
		bool* L_30 = ___success;
		if ((*((int8_t*)L_30)))
		{
			goto IL_0075;
		}
	}
	{
		bool* L_31 = ___success;
		*((int8_t*)(L_31)) = (int8_t)0;
		return (Il2CppObject*)NULL;
	}

IL_0075:
	{
		Il2CppObject* L_32 = V_0;
		String_t* L_33 = V_3;
		Il2CppObject * L_34 = V_4;
		NullCheck(L_32);
		InterfaceActionInvoker2< String_t*, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Object>::set_Item(!0,!1) */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, L_32, L_33, L_34);
	}

IL_007e:
	{
		bool L_35 = V_2;
		if (!L_35)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject* L_36 = V_0;
		return L_36;
	}
}
// SimpleJson.JsonArray SimpleJson.SimpleJson::ParseArray(System.Char[],System.Int32&,System.Boolean&)
extern TypeInfo* JsonArray_t2769828783_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_ParseArray_m3288489822_MetadataUsageId;
extern "C"  JsonArray_t2769828783 * SimpleJson_ParseArray_m3288489822 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, bool* ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_ParseArray_m3288489822_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonArray_t2769828783 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	{
		JsonArray_t2769828783 * L_0 = (JsonArray_t2769828783 *)il2cpp_codegen_object_new(JsonArray_t2769828783_il2cpp_TypeInfo_var);
		JsonArray__ctor_m7376362(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t3416858730* L_1 = ___json;
		int32_t* L_2 = ___index;
		SimpleJson_NextToken_m2355341557(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = (bool)0;
		goto IL_0055;
	}

IL_0012:
	{
		CharU5BU5D_t3416858730* L_3 = ___json;
		int32_t* L_4 = ___index;
		int32_t L_5 = SimpleJson_LookAhead_m3509516115(NULL /*static, unused*/, L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if (L_6)
		{
			goto IL_0023;
		}
	}
	{
		bool* L_7 = ___success;
		*((int8_t*)(L_7)) = (int8_t)0;
		return (JsonArray_t2769828783 *)NULL;
	}

IL_0023:
	{
		int32_t L_8 = V_2;
		if ((!(((uint32_t)L_8) == ((uint32_t)6))))
		{
			goto IL_0031;
		}
	}
	{
		CharU5BU5D_t3416858730* L_9 = ___json;
		int32_t* L_10 = ___index;
		SimpleJson_NextToken_m2355341557(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		goto IL_0055;
	}

IL_0031:
	{
		int32_t L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)4))))
		{
			goto IL_003f;
		}
	}
	{
		CharU5BU5D_t3416858730* L_12 = ___json;
		int32_t* L_13 = ___index;
		SimpleJson_NextToken_m2355341557(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0058;
	}

IL_003f:
	{
		CharU5BU5D_t3416858730* L_14 = ___json;
		int32_t* L_15 = ___index;
		bool* L_16 = ___success;
		Il2CppObject * L_17 = SimpleJson_ParseValue_m1670953315(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		bool* L_18 = ___success;
		if ((*((int8_t*)L_18)))
		{
			goto IL_004e;
		}
	}
	{
		return (JsonArray_t2769828783 *)NULL;
	}

IL_004e:
	{
		JsonArray_t2769828783 * L_19 = V_0;
		Il2CppObject * L_20 = V_3;
		NullCheck(L_19);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, L_19, L_20);
	}

IL_0055:
	{
		bool L_21 = V_1;
		if (!L_21)
		{
			goto IL_0012;
		}
	}

IL_0058:
	{
		JsonArray_t2769828783 * L_22 = V_0;
		return L_22;
	}
}
// System.Object SimpleJson.SimpleJson::ParseValue(System.Char[],System.Int32&,System.Boolean&)
extern TypeInfo* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_ParseValue_m1670953315_MetadataUsageId;
extern "C"  Il2CppObject * SimpleJson_ParseValue_m1670953315 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, bool* ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_ParseValue_m1670953315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		CharU5BU5D_t3416858730* L_0 = ___json;
		int32_t* L_1 = ___index;
		int32_t L_2 = SimpleJson_LookAhead_m3509516115(NULL /*static, unused*/, L_0, (*((int32_t*)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3 == 0)
		{
			goto IL_008d;
		}
		if (L_3 == 1)
		{
			goto IL_0053;
		}
		if (L_3 == 2)
		{
			goto IL_008d;
		}
		if (L_3 == 3)
		{
			goto IL_005c;
		}
		if (L_3 == 4)
		{
			goto IL_008d;
		}
		if (L_3 == 5)
		{
			goto IL_008d;
		}
		if (L_3 == 6)
		{
			goto IL_008d;
		}
		if (L_3 == 7)
		{
			goto IL_0041;
		}
		if (L_3 == 8)
		{
			goto IL_004a;
		}
		if (L_3 == 9)
		{
			goto IL_0065;
		}
		if (L_3 == 10)
		{
			goto IL_0074;
		}
		if (L_3 == 11)
		{
			goto IL_0083;
		}
	}
	{
		goto IL_008d;
	}

IL_0041:
	{
		CharU5BU5D_t3416858730* L_4 = ___json;
		int32_t* L_5 = ___index;
		bool* L_6 = ___success;
		String_t* L_7 = SimpleJson_ParseString_m3765835561(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_004a:
	{
		CharU5BU5D_t3416858730* L_8 = ___json;
		int32_t* L_9 = ___index;
		bool* L_10 = ___success;
		Il2CppObject * L_11 = SimpleJson_ParseNumber_m1182805535(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0053:
	{
		CharU5BU5D_t3416858730* L_12 = ___json;
		int32_t* L_13 = ___index;
		bool* L_14 = ___success;
		Il2CppObject* L_15 = SimpleJson_ParseObject_m2573007585(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_005c:
	{
		CharU5BU5D_t3416858730* L_16 = ___json;
		int32_t* L_17 = ___index;
		bool* L_18 = ___success;
		JsonArray_t2769828783 * L_19 = SimpleJson_ParseArray_m3288489822(NULL /*static, unused*/, L_16, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}

IL_0065:
	{
		CharU5BU5D_t3416858730* L_20 = ___json;
		int32_t* L_21 = ___index;
		SimpleJson_NextToken_m2355341557(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		bool L_22 = ((bool)1);
		Il2CppObject * L_23 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_22);
		return L_23;
	}

IL_0074:
	{
		CharU5BU5D_t3416858730* L_24 = ___json;
		int32_t* L_25 = ___index;
		SimpleJson_NextToken_m2355341557(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		bool L_26 = ((bool)0);
		Il2CppObject * L_27 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_26);
		return L_27;
	}

IL_0083:
	{
		CharU5BU5D_t3416858730* L_28 = ___json;
		int32_t* L_29 = ___index;
		SimpleJson_NextToken_m2355341557(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		return NULL;
	}

IL_008d:
	{
		bool* L_30 = ___success;
		*((int8_t*)(L_30)) = (int8_t)0;
		return NULL;
	}
}
// System.String SimpleJson.SimpleJson::ParseString(System.Char[],System.Int32&,System.Boolean&)
extern TypeInfo* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral0;
extern Il2CppCodeGenString* _stringLiteral2969;
extern const uint32_t SimpleJson_ParseString_m3765835561_MetadataUsageId;
extern "C"  String_t* SimpleJson_ParseString_m3765835561 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, bool* ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_ParseString_m3765835561_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	uint16_t V_1 = 0x0;
	bool V_2 = false;
	int32_t V_3 = 0;
	uint32_t V_4 = 0;
	uint32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	bool V_9 = false;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_0, ((int32_t)2000), /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t3416858730* L_1 = ___json;
		int32_t* L_2 = ___index;
		SimpleJson_EatWhitespace_m3395499660(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		CharU5BU5D_t3416858730* L_3 = ___json;
		int32_t* L_4 = ___index;
		int32_t* L_5 = L_4;
		int32_t L_6 = (*((int32_t*)L_5));
		V_6 = L_6;
		*((int32_t*)(L_5)) = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
		int32_t L_7 = V_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_7);
		int32_t L_8 = L_7;
		V_1 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_8)));
		V_2 = (bool)0;
		goto IL_01ef;
	}

IL_0027:
	{
		int32_t* L_9 = ___index;
		CharU5BU5D_t3416858730* L_10 = ___json;
		NullCheck(L_10);
		if ((((int32_t)(*((int32_t*)L_9))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_01f5;
		}
	}
	{
		CharU5BU5D_t3416858730* L_11 = ___json;
		int32_t* L_12 = ___index;
		int32_t* L_13 = L_12;
		int32_t L_14 = (*((int32_t*)L_13));
		V_7 = L_14;
		*((int32_t*)(L_13)) = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		int32_t L_15 = V_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_15);
		int32_t L_16 = L_15;
		V_1 = ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_16)));
		uint16_t L_17 = V_1;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_004b;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_01f5;
	}

IL_004b:
	{
		uint16_t L_18 = V_1;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_01e7;
		}
	}
	{
		int32_t* L_19 = ___index;
		CharU5BU5D_t3416858730* L_20 = ___json;
		NullCheck(L_20);
		if ((((int32_t)(*((int32_t*)L_19))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_01f5;
		}
	}
	{
		CharU5BU5D_t3416858730* L_21 = ___json;
		int32_t* L_22 = ___index;
		int32_t* L_23 = L_22;
		int32_t L_24 = (*((int32_t*)L_23));
		V_8 = L_24;
		*((int32_t*)(L_23)) = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
		int32_t L_25 = V_8;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_25);
		int32_t L_26 = L_25;
		V_1 = ((L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_26)));
		uint16_t L_27 = V_1;
		if ((!(((uint32_t)L_27) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_007e;
		}
	}
	{
		StringBuilder_t3822575854 * L_28 = V_0;
		NullCheck(L_28);
		StringBuilder_Append_m2143093878(L_28, ((int32_t)34), /*hidden argument*/NULL);
		goto IL_01ef;
	}

IL_007e:
	{
		uint16_t L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0091;
		}
	}
	{
		StringBuilder_t3822575854 * L_30 = V_0;
		NullCheck(L_30);
		StringBuilder_Append_m2143093878(L_30, ((int32_t)92), /*hidden argument*/NULL);
		goto IL_01ef;
	}

IL_0091:
	{
		uint16_t L_31 = V_1;
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00a4;
		}
	}
	{
		StringBuilder_t3822575854 * L_32 = V_0;
		NullCheck(L_32);
		StringBuilder_Append_m2143093878(L_32, ((int32_t)47), /*hidden argument*/NULL);
		goto IL_01ef;
	}

IL_00a4:
	{
		uint16_t L_33 = V_1;
		if ((!(((uint32_t)L_33) == ((uint32_t)((int32_t)98)))))
		{
			goto IL_00b6;
		}
	}
	{
		StringBuilder_t3822575854 * L_34 = V_0;
		NullCheck(L_34);
		StringBuilder_Append_m2143093878(L_34, 8, /*hidden argument*/NULL);
		goto IL_01ef;
	}

IL_00b6:
	{
		uint16_t L_35 = V_1;
		if ((!(((uint32_t)L_35) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_00c9;
		}
	}
	{
		StringBuilder_t3822575854 * L_36 = V_0;
		NullCheck(L_36);
		StringBuilder_Append_m2143093878(L_36, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_01ef;
	}

IL_00c9:
	{
		uint16_t L_37 = V_1;
		if ((!(((uint32_t)L_37) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_00dc;
		}
	}
	{
		StringBuilder_t3822575854 * L_38 = V_0;
		NullCheck(L_38);
		StringBuilder_Append_m2143093878(L_38, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_01ef;
	}

IL_00dc:
	{
		uint16_t L_39 = V_1;
		if ((!(((uint32_t)L_39) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_00ef;
		}
	}
	{
		StringBuilder_t3822575854 * L_40 = V_0;
		NullCheck(L_40);
		StringBuilder_Append_m2143093878(L_40, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_01ef;
	}

IL_00ef:
	{
		uint16_t L_41 = V_1;
		if ((!(((uint32_t)L_41) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_0102;
		}
	}
	{
		StringBuilder_t3822575854 * L_42 = V_0;
		NullCheck(L_42);
		StringBuilder_Append_m2143093878(L_42, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_01ef;
	}

IL_0102:
	{
		uint16_t L_43 = V_1;
		if ((!(((uint32_t)L_43) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_01ef;
		}
	}
	{
		CharU5BU5D_t3416858730* L_44 = ___json;
		NullCheck(L_44);
		int32_t* L_45 = ___index;
		V_3 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_44)->max_length))))-(int32_t)(*((int32_t*)L_45))));
		int32_t L_46 = V_3;
		if ((((int32_t)L_46) < ((int32_t)4)))
		{
			goto IL_01f5;
		}
	}
	{
		bool* L_47 = ___success;
		CharU5BU5D_t3416858730* L_48 = ___json;
		int32_t* L_49 = ___index;
		String_t* L_50 = String_CreateString_m3402832113(NULL, L_48, (*((int32_t*)L_49)), 4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_51 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_52 = UInt32_TryParse_m3950000893(NULL /*static, unused*/, L_50, ((int32_t)515), L_51, (&V_4), /*hidden argument*/NULL);
		bool L_53 = L_52;
		V_9 = L_53;
		*((int8_t*)(L_47)) = (int8_t)L_53;
		bool L_54 = V_9;
		if (L_54)
		{
			goto IL_0141;
		}
	}
	{
		return _stringLiteral0;
	}

IL_0141:
	{
		uint32_t L_55 = V_4;
		if ((!(((uint32_t)((int32_t)55296)) <= ((uint32_t)L_55))))
		{
			goto IL_01d1;
		}
	}
	{
		uint32_t L_56 = V_4;
		if ((!(((uint32_t)L_56) <= ((uint32_t)((int32_t)56319)))))
		{
			goto IL_01d1;
		}
	}
	{
		int32_t* L_57 = ___index;
		int32_t* L_58 = L_57;
		*((int32_t*)(L_58)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_58))+(int32_t)4));
		CharU5BU5D_t3416858730* L_59 = ___json;
		NullCheck(L_59);
		int32_t* L_60 = ___index;
		V_3 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_59)->max_length))))-(int32_t)(*((int32_t*)L_60))));
		int32_t L_61 = V_3;
		if ((((int32_t)L_61) < ((int32_t)6)))
		{
			goto IL_01c8;
		}
	}
	{
		CharU5BU5D_t3416858730* L_62 = ___json;
		int32_t* L_63 = ___index;
		String_t* L_64 = String_CreateString_m3402832113(NULL, L_62, (*((int32_t*)L_63)), 2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_65 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_64, _stringLiteral2969, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_01c8;
		}
	}
	{
		CharU5BU5D_t3416858730* L_66 = ___json;
		int32_t* L_67 = ___index;
		String_t* L_68 = String_CreateString_m3402832113(NULL, L_66, ((int32_t)((int32_t)(*((int32_t*)L_67))+(int32_t)2)), 4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_69 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_70 = UInt32_TryParse_m3950000893(NULL /*static, unused*/, L_68, ((int32_t)515), L_69, (&V_5), /*hidden argument*/NULL);
		if (!L_70)
		{
			goto IL_01c8;
		}
	}
	{
		uint32_t L_71 = V_5;
		if ((!(((uint32_t)((int32_t)56320)) <= ((uint32_t)L_71))))
		{
			goto IL_01c8;
		}
	}
	{
		uint32_t L_72 = V_5;
		if ((!(((uint32_t)L_72) <= ((uint32_t)((int32_t)57343)))))
		{
			goto IL_01c8;
		}
	}
	{
		StringBuilder_t3822575854 * L_73 = V_0;
		uint32_t L_74 = V_4;
		NullCheck(L_73);
		StringBuilder_Append_m2143093878(L_73, (((int32_t)((uint16_t)L_74))), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_75 = V_0;
		uint32_t L_76 = V_5;
		NullCheck(L_75);
		StringBuilder_Append_m2143093878(L_75, (((int32_t)((uint16_t)L_76))), /*hidden argument*/NULL);
		int32_t* L_77 = ___index;
		int32_t* L_78 = L_77;
		*((int32_t*)(L_78)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_78))+(int32_t)6));
		goto IL_01ef;
	}

IL_01c8:
	{
		bool* L_79 = ___success;
		*((int8_t*)(L_79)) = (int8_t)0;
		return _stringLiteral0;
	}

IL_01d1:
	{
		StringBuilder_t3822575854 * L_80 = V_0;
		uint32_t L_81 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2778706699_il2cpp_TypeInfo_var);
		String_t* L_82 = Char_ConvertFromUtf32_m567781788(NULL /*static, unused*/, L_81, /*hidden argument*/NULL);
		NullCheck(L_80);
		StringBuilder_Append_m3898090075(L_80, L_82, /*hidden argument*/NULL);
		int32_t* L_83 = ___index;
		int32_t* L_84 = L_83;
		*((int32_t*)(L_84)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_84))+(int32_t)4));
		goto IL_01ef;
	}

IL_01e7:
	{
		StringBuilder_t3822575854 * L_85 = V_0;
		uint16_t L_86 = V_1;
		NullCheck(L_85);
		StringBuilder_Append_m2143093878(L_85, L_86, /*hidden argument*/NULL);
	}

IL_01ef:
	{
		bool L_87 = V_2;
		if (!L_87)
		{
			goto IL_0027;
		}
	}

IL_01f5:
	{
		bool L_88 = V_2;
		if (L_88)
		{
			goto IL_01fd;
		}
	}
	{
		bool* L_89 = ___success;
		*((int8_t*)(L_89)) = (int8_t)0;
		return (String_t*)NULL;
	}

IL_01fd:
	{
		StringBuilder_t3822575854 * L_90 = V_0;
		NullCheck(L_90);
		String_t* L_91 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_90);
		return L_91;
	}
}
// System.Object SimpleJson.SimpleJson::ParseNumber(System.Char[],System.Int32&,System.Boolean&)
extern TypeInfo* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t534516614_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral101;
extern const uint32_t SimpleJson_ParseNumber_m1182805535_MetadataUsageId;
extern "C"  Il2CppObject * SimpleJson_ParseNumber_m1182805535 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, bool* ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_ParseNumber_m1182805535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	String_t* V_3 = NULL;
	double V_4 = 0.0;
	int64_t V_5 = 0;
	{
		CharU5BU5D_t3416858730* L_0 = ___json;
		int32_t* L_1 = ___index;
		SimpleJson_EatWhitespace_m3395499660(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		CharU5BU5D_t3416858730* L_2 = ___json;
		int32_t* L_3 = ___index;
		int32_t L_4 = SimpleJson_GetLastIndexOfNumber_m2912405839(NULL /*static, unused*/, L_2, (*((int32_t*)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		int32_t* L_6 = ___index;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)(*((int32_t*)L_6))))+(int32_t)1));
		CharU5BU5D_t3416858730* L_7 = ___json;
		int32_t* L_8 = ___index;
		int32_t L_9 = V_1;
		String_t* L_10 = String_CreateString_m3402832113(NULL, L_7, (*((int32_t*)L_8)), L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		String_t* L_11 = V_3;
		NullCheck(L_11);
		int32_t L_12 = String_IndexOf_m864002126(L_11, _stringLiteral46, 5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)(-1)))))
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_13 = V_3;
		NullCheck(L_13);
		int32_t L_14 = String_IndexOf_m864002126(L_13, _stringLiteral101, 5, /*hidden argument*/NULL);
		if ((((int32_t)L_14) == ((int32_t)(-1))))
		{
			goto IL_0065;
		}
	}

IL_003f:
	{
		bool* L_15 = ___success;
		CharU5BU5D_t3416858730* L_16 = ___json;
		int32_t* L_17 = ___index;
		int32_t L_18 = V_1;
		String_t* L_19 = String_CreateString_m3402832113(NULL, L_16, (*((int32_t*)L_17)), L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_20 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_21 = Double_TryParse_m707798013(NULL /*static, unused*/, L_19, ((int32_t)511), L_20, (&V_4), /*hidden argument*/NULL);
		*((int8_t*)(L_15)) = (int8_t)L_21;
		double L_22 = V_4;
		double L_23 = L_22;
		Il2CppObject * L_24 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_23);
		V_2 = L_24;
		goto IL_0089;
	}

IL_0065:
	{
		bool* L_25 = ___success;
		CharU5BU5D_t3416858730* L_26 = ___json;
		int32_t* L_27 = ___index;
		int32_t L_28 = V_1;
		String_t* L_29 = String_CreateString_m3402832113(NULL, L_26, (*((int32_t*)L_27)), L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_30 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_31 = Int64_TryParse_m1199101227(NULL /*static, unused*/, L_29, ((int32_t)511), L_30, (&V_5), /*hidden argument*/NULL);
		*((int8_t*)(L_25)) = (int8_t)L_31;
		int64_t L_32 = V_5;
		int64_t L_33 = L_32;
		Il2CppObject * L_34 = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &L_33);
		V_2 = L_34;
	}

IL_0089:
	{
		int32_t* L_35 = ___index;
		int32_t L_36 = V_0;
		*((int32_t*)(L_35)) = (int32_t)((int32_t)((int32_t)L_36+(int32_t)1));
		Il2CppObject * L_37 = V_2;
		return L_37;
	}
}
// System.Int32 SimpleJson.SimpleJson::GetLastIndexOfNumber(System.Char[],System.Int32)
extern Il2CppCodeGenString* _stringLiteral2451559847;
extern const uint32_t SimpleJson_GetLastIndexOfNumber_m2912405839_MetadataUsageId;
extern "C"  int32_t SimpleJson_GetLastIndexOfNumber_m2912405839 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_GetLastIndexOfNumber_m2912405839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		goto IL_0018;
	}

IL_0004:
	{
		CharU5BU5D_t3416858730* L_1 = ___json;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck(_stringLiteral2451559847);
		int32_t L_4 = String_IndexOf_m2775210486(_stringLiteral2451559847, ((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3))), /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0018:
	{
		int32_t L_6 = V_0;
		CharU5BU5D_t3416858730* L_7 = ___json;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0004;
		}
	}

IL_001e:
	{
		int32_t L_8 = V_0;
		return ((int32_t)((int32_t)L_8-(int32_t)1));
	}
}
// System.Void SimpleJson.SimpleJson::EatWhitespace(System.Char[],System.Int32&)
extern Il2CppCodeGenString* _stringLiteral924755184;
extern const uint32_t SimpleJson_EatWhitespace_m3395499660_MetadataUsageId;
extern "C"  void SimpleJson_EatWhitespace_m3395499660 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_EatWhitespace_m3395499660_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_001a;
	}

IL_0002:
	{
		CharU5BU5D_t3416858730* L_0 = ___json;
		int32_t* L_1 = ___index;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, (*((int32_t*)L_1)));
		int32_t L_2 = (*((int32_t*)L_1));
		NullCheck(_stringLiteral924755184);
		int32_t L_3 = String_IndexOf_m2775210486(_stringLiteral924755184, ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		int32_t* L_4 = ___index;
		int32_t* L_5 = L_4;
		*((int32_t*)(L_5)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_5))+(int32_t)1));
	}

IL_001a:
	{
		int32_t* L_6 = ___index;
		CharU5BU5D_t3416858730* L_7 = ___json;
		NullCheck(L_7);
		if ((((int32_t)(*((int32_t*)L_6))) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0002;
		}
	}
	{
		return;
	}
}
// System.Int32 SimpleJson.SimpleJson::LookAhead(System.Char[],System.Int32)
extern "C"  int32_t SimpleJson_LookAhead_m3509516115 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t ___index, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		CharU5BU5D_t3416858730* L_1 = ___json;
		int32_t L_2 = SimpleJson_NextToken_m2355341557(NULL /*static, unused*/, L_1, (&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 SimpleJson.SimpleJson::NextToken(System.Char[],System.Int32&)
extern "C"  int32_t SimpleJson_NextToken_m2355341557 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method)
{
	uint16_t V_0 = 0x0;
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	{
		CharU5BU5D_t3416858730* L_0 = ___json;
		int32_t* L_1 = ___index;
		SimpleJson_EatWhitespace_m3395499660(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		int32_t* L_2 = ___index;
		CharU5BU5D_t3416858730* L_3 = ___json;
		NullCheck(L_3);
		if ((!(((uint32_t)(*((int32_t*)L_2))) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))))))
		{
			goto IL_0010;
		}
	}
	{
		return 0;
	}

IL_0010:
	{
		CharU5BU5D_t3416858730* L_4 = ___json;
		int32_t* L_5 = ___index;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, (*((int32_t*)L_5)));
		int32_t L_6 = (*((int32_t*)L_5));
		V_0 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		int32_t* L_7 = ___index;
		int32_t* L_8 = L_7;
		*((int32_t*)(L_8)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_8))+(int32_t)1));
		uint16_t L_9 = V_0;
		V_2 = L_9;
		uint16_t L_10 = V_2;
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 0)
		{
			goto IL_00c0;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 1)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 2)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 3)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 4)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 5)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 6)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 7)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 8)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 9)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 10)
		{
			goto IL_00be;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 11)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 12)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 13)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 14)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 15)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 16)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 17)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 18)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 19)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 20)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 21)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 22)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 23)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 24)
		{
			goto IL_00c4;
		}
	}
	{
		uint16_t L_11 = V_2;
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_00ba;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_00bc;
		}
	}
	{
		uint16_t L_12 = V_2;
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_00b6;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_00b8;
		}
	}
	{
		goto IL_00c6;
	}

IL_00b6:
	{
		return 1;
	}

IL_00b8:
	{
		return 2;
	}

IL_00ba:
	{
		return 3;
	}

IL_00bc:
	{
		return 4;
	}

IL_00be:
	{
		return 6;
	}

IL_00c0:
	{
		return 7;
	}

IL_00c2:
	{
		return 8;
	}

IL_00c4:
	{
		return 5;
	}

IL_00c6:
	{
		int32_t* L_13 = ___index;
		int32_t* L_14 = L_13;
		*((int32_t*)(L_14)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_14))-(int32_t)1));
		CharU5BU5D_t3416858730* L_15 = ___json;
		NullCheck(L_15);
		int32_t* L_16 = ___index;
		V_1 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length))))-(int32_t)(*((int32_t*)L_16))));
		int32_t L_17 = V_1;
		if ((((int32_t)L_17) < ((int32_t)5)))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3416858730* L_18 = ___json;
		int32_t* L_19 = ___index;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, (*((int32_t*)L_19)));
		int32_t L_20 = (*((int32_t*)L_19));
		if ((!(((uint32_t)((L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20)))) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3416858730* L_21 = ___json;
		int32_t* L_22 = ___index;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)((int32_t)(*((int32_t*)L_22))+(int32_t)1)));
		int32_t L_23 = ((int32_t)((int32_t)(*((int32_t*)L_22))+(int32_t)1));
		if ((!(((uint32_t)((L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23)))) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3416858730* L_24 = ___json;
		int32_t* L_25 = ___index;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)((int32_t)(*((int32_t*)L_25))+(int32_t)2)));
		int32_t L_26 = ((int32_t)((int32_t)(*((int32_t*)L_25))+(int32_t)2));
		if ((!(((uint32_t)((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26)))) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3416858730* L_27 = ___json;
		int32_t* L_28 = ___index;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)(*((int32_t*)L_28))+(int32_t)3)));
		int32_t L_29 = ((int32_t)((int32_t)(*((int32_t*)L_28))+(int32_t)3));
		if ((!(((uint32_t)((L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29)))) == ((uint32_t)((int32_t)115)))))
		{
			goto IL_0110;
		}
	}
	{
		CharU5BU5D_t3416858730* L_30 = ___json;
		int32_t* L_31 = ___index;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)((int32_t)(*((int32_t*)L_31))+(int32_t)4)));
		int32_t L_32 = ((int32_t)((int32_t)(*((int32_t*)L_31))+(int32_t)4));
		if ((!(((uint32_t)((L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32)))) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0110;
		}
	}
	{
		int32_t* L_33 = ___index;
		int32_t* L_34 = L_33;
		*((int32_t*)(L_34)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_34))+(int32_t)5));
		return ((int32_t)10);
	}

IL_0110:
	{
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) < ((int32_t)4)))
		{
			goto IL_0143;
		}
	}
	{
		CharU5BU5D_t3416858730* L_36 = ___json;
		int32_t* L_37 = ___index;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, (*((int32_t*)L_37)));
		int32_t L_38 = (*((int32_t*)L_37));
		if ((!(((uint32_t)((L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38)))) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_0143;
		}
	}
	{
		CharU5BU5D_t3416858730* L_39 = ___json;
		int32_t* L_40 = ___index;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)((int32_t)(*((int32_t*)L_40))+(int32_t)1)));
		int32_t L_41 = ((int32_t)((int32_t)(*((int32_t*)L_40))+(int32_t)1));
		if ((!(((uint32_t)((L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41)))) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_0143;
		}
	}
	{
		CharU5BU5D_t3416858730* L_42 = ___json;
		int32_t* L_43 = ___index;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)((int32_t)(*((int32_t*)L_43))+(int32_t)2)));
		int32_t L_44 = ((int32_t)((int32_t)(*((int32_t*)L_43))+(int32_t)2));
		if ((!(((uint32_t)((L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44)))) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0143;
		}
	}
	{
		CharU5BU5D_t3416858730* L_45 = ___json;
		int32_t* L_46 = ___index;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)((int32_t)(*((int32_t*)L_46))+(int32_t)3)));
		int32_t L_47 = ((int32_t)((int32_t)(*((int32_t*)L_46))+(int32_t)3));
		if ((!(((uint32_t)((L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47)))) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0143;
		}
	}
	{
		int32_t* L_48 = ___index;
		int32_t* L_49 = L_48;
		*((int32_t*)(L_49)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_49))+(int32_t)4));
		return ((int32_t)9);
	}

IL_0143:
	{
		int32_t L_50 = V_1;
		if ((((int32_t)L_50) < ((int32_t)4)))
		{
			goto IL_0176;
		}
	}
	{
		CharU5BU5D_t3416858730* L_51 = ___json;
		int32_t* L_52 = ___index;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, (*((int32_t*)L_52)));
		int32_t L_53 = (*((int32_t*)L_52));
		if ((!(((uint32_t)((L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53)))) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_0176;
		}
	}
	{
		CharU5BU5D_t3416858730* L_54 = ___json;
		int32_t* L_55 = ___index;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)((int32_t)(*((int32_t*)L_55))+(int32_t)1)));
		int32_t L_56 = ((int32_t)((int32_t)(*((int32_t*)L_55))+(int32_t)1));
		if ((!(((uint32_t)((L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56)))) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0176;
		}
	}
	{
		CharU5BU5D_t3416858730* L_57 = ___json;
		int32_t* L_58 = ___index;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)((int32_t)(*((int32_t*)L_58))+(int32_t)2)));
		int32_t L_59 = ((int32_t)((int32_t)(*((int32_t*)L_58))+(int32_t)2));
		if ((!(((uint32_t)((L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59)))) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0176;
		}
	}
	{
		CharU5BU5D_t3416858730* L_60 = ___json;
		int32_t* L_61 = ___index;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)((int32_t)(*((int32_t*)L_61))+(int32_t)3)));
		int32_t L_62 = ((int32_t)((int32_t)(*((int32_t*)L_61))+(int32_t)3));
		if ((!(((uint32_t)((L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62)))) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0176;
		}
	}
	{
		int32_t* L_63 = ___index;
		int32_t* L_64 = L_63;
		*((int32_t*)(L_64)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_64))+(int32_t)4));
		return ((int32_t)11);
	}

IL_0176:
	{
		return 0;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeValue(SimpleJson.IJsonSerializerStrategy,System.Object,System.Text.StringBuilder)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t3650470111_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t3781852593_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t211005341_il2cpp_TypeInfo_var;
extern TypeInfo* IJsonSerializerStrategy_t4110884692_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t SimpleJson_SerializeValue_m2154484296_MetadataUsageId;
extern "C"  bool SimpleJson_SerializeValue_m2154484296 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonSerializerStrategy, Il2CppObject * ___value, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_SerializeValue_m2154484296_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject* V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	StringBuilder_t3822575854 * G_B13_0 = NULL;
	StringBuilder_t3822575854 * G_B12_0 = NULL;
	String_t* G_B14_0 = NULL;
	StringBuilder_t3822575854 * G_B14_1 = NULL;
	{
		V_0 = (bool)1;
		Il2CppObject * L_0 = ___value;
		if (!((String_t*)IsInstSealed(L_0, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_1 = ___value;
		StringBuilder_t3822575854 * L_2 = ___builder;
		bool L_3 = SimpleJson_SerializeString_m3428852602(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_1, String_t_il2cpp_TypeInfo_var)), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_00df;
	}

IL_001c:
	{
		Il2CppObject * L_4 = ___value;
		if (!((Il2CppObject*)IsInst(L_4, IDictionary_2_t3650470111_il2cpp_TypeInfo_var)))
		{
			goto IL_0044;
		}
	}
	{
		Il2CppObject * L_5 = ___value;
		V_1 = ((Il2CppObject*)Castclass(L_5, IDictionary_2_t3650470111_il2cpp_TypeInfo_var));
		Il2CppObject * L_6 = ___jsonSerializerStrategy;
		Il2CppObject* L_7 = V_1;
		NullCheck(L_7);
		Il2CppObject* L_8 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Keys() */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, L_7);
		Il2CppObject* L_9 = V_1;
		NullCheck(L_9);
		Il2CppObject* L_10 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.ICollection`1<!1> System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Values() */, IDictionary_2_t3650470111_il2cpp_TypeInfo_var, L_9);
		StringBuilder_t3822575854 * L_11 = ___builder;
		bool L_12 = SimpleJson_SerializeObject_m3822754844(NULL /*static, unused*/, L_6, L_8, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_00df;
	}

IL_0044:
	{
		Il2CppObject * L_13 = ___value;
		if (!((Il2CppObject*)IsInst(L_13, IDictionary_2_t3781852593_il2cpp_TypeInfo_var)))
		{
			goto IL_0069;
		}
	}
	{
		Il2CppObject * L_14 = ___value;
		V_2 = ((Il2CppObject*)Castclass(L_14, IDictionary_2_t3781852593_il2cpp_TypeInfo_var));
		Il2CppObject * L_15 = ___jsonSerializerStrategy;
		Il2CppObject* L_16 = V_2;
		NullCheck(L_16);
		Il2CppObject* L_17 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Keys() */, IDictionary_2_t3781852593_il2cpp_TypeInfo_var, L_16);
		Il2CppObject* L_18 = V_2;
		NullCheck(L_18);
		Il2CppObject* L_19 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.ICollection`1<!1> System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Values() */, IDictionary_2_t3781852593_il2cpp_TypeInfo_var, L_18);
		StringBuilder_t3822575854 * L_20 = ___builder;
		bool L_21 = SimpleJson_SerializeObject_m3822754844(NULL /*static, unused*/, L_15, L_17, L_19, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		goto IL_00df;
	}

IL_0069:
	{
		Il2CppObject * L_22 = ___value;
		if (!((Il2CppObject *)IsInst(L_22, IEnumerable_t287189635_il2cpp_TypeInfo_var)))
		{
			goto IL_0081;
		}
	}
	{
		Il2CppObject * L_23 = ___jsonSerializerStrategy;
		Il2CppObject * L_24 = ___value;
		StringBuilder_t3822575854 * L_25 = ___builder;
		bool L_26 = SimpleJson_SerializeArray_m2191531891(NULL /*static, unused*/, L_23, ((Il2CppObject *)Castclass(L_24, IEnumerable_t287189635_il2cpp_TypeInfo_var)), L_25, /*hidden argument*/NULL);
		V_0 = L_26;
		goto IL_00df;
	}

IL_0081:
	{
		Il2CppObject * L_27 = ___value;
		bool L_28 = SimpleJson_IsNumeric_m2618022454(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0093;
		}
	}
	{
		Il2CppObject * L_29 = ___value;
		StringBuilder_t3822575854 * L_30 = ___builder;
		bool L_31 = SimpleJson_SerializeNumber_m1649989460(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		goto IL_00df;
	}

IL_0093:
	{
		Il2CppObject * L_32 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_32, Boolean_t211005341_il2cpp_TypeInfo_var)))
		{
			goto IL_00b8;
		}
	}
	{
		StringBuilder_t3822575854 * L_33 = ___builder;
		Il2CppObject * L_34 = ___value;
		G_B12_0 = L_33;
		if (((*(bool*)((bool*)UnBox (L_34, Boolean_t211005341_il2cpp_TypeInfo_var)))))
		{
			G_B13_0 = L_33;
			goto IL_00ab;
		}
	}
	{
		G_B14_0 = _stringLiteral97196323;
		G_B14_1 = G_B12_0;
		goto IL_00b0;
	}

IL_00ab:
	{
		G_B14_0 = _stringLiteral3569038;
		G_B14_1 = G_B13_0;
	}

IL_00b0:
	{
		NullCheck(G_B14_1);
		StringBuilder_Append_m3898090075(G_B14_1, G_B14_0, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_00b8:
	{
		Il2CppObject * L_35 = ___value;
		if (L_35)
		{
			goto IL_00c9;
		}
	}
	{
		StringBuilder_t3822575854 * L_36 = ___builder;
		NullCheck(L_36);
		StringBuilder_Append_m3898090075(L_36, _stringLiteral3392903, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_00c9:
	{
		Il2CppObject * L_37 = ___jsonSerializerStrategy;
		Il2CppObject * L_38 = ___value;
		NullCheck(L_37);
		bool L_39 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(0 /* System.Boolean SimpleJson.IJsonSerializerStrategy::SerializeNonPrimitiveObject(System.Object,System.Object&) */, IJsonSerializerStrategy_t4110884692_il2cpp_TypeInfo_var, L_37, L_38, (&V_3));
		V_0 = L_39;
		bool L_40 = V_0;
		if (!L_40)
		{
			goto IL_00df;
		}
	}
	{
		Il2CppObject * L_41 = ___jsonSerializerStrategy;
		Il2CppObject * L_42 = V_3;
		StringBuilder_t3822575854 * L_43 = ___builder;
		SimpleJson_SerializeValue_m2154484296(NULL /*static, unused*/, L_41, L_42, L_43, /*hidden argument*/NULL);
	}

IL_00df:
	{
		bool L_44 = V_0;
		return L_44;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeObject(SimpleJson.IJsonSerializerStrategy,System.Collections.IEnumerable,System.Collections.IEnumerable,System.Text.StringBuilder)
extern TypeInfo* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral125;
extern const uint32_t SimpleJson_SerializeObject_m3822754844_MetadataUsageId;
extern "C"  bool SimpleJson_SerializeObject_m3822754844 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonSerializerStrategy, Il2CppObject * ___keys, Il2CppObject * ___values, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_SerializeObject_m3822754844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	{
		StringBuilder_t3822575854 * L_0 = ___builder;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, _stringLiteral123, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___keys;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_1);
		V_0 = L_2;
		Il2CppObject * L_3 = ___values;
		NullCheck(L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_3);
		V_1 = L_4;
		V_2 = (bool)1;
		goto IL_007b;
	}

IL_001e:
	{
		Il2CppObject * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_5);
		V_3 = L_6;
		Il2CppObject * L_7 = V_1;
		NullCheck(L_7);
		Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_7);
		V_4 = L_8;
		bool L_9 = V_2;
		if (L_9)
		{
			goto IL_003c;
		}
	}
	{
		StringBuilder_t3822575854 * L_10 = ___builder;
		NullCheck(L_10);
		StringBuilder_Append_m3898090075(L_10, _stringLiteral44, /*hidden argument*/NULL);
	}

IL_003c:
	{
		Il2CppObject * L_11 = V_3;
		if (!((String_t*)IsInstSealed(L_11, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0053;
		}
	}
	{
		Il2CppObject * L_12 = V_3;
		StringBuilder_t3822575854 * L_13 = ___builder;
		SimpleJson_SerializeString_m3428852602(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_12, String_t_il2cpp_TypeInfo_var)), L_13, /*hidden argument*/NULL);
		goto IL_0060;
	}

IL_0053:
	{
		Il2CppObject * L_14 = ___jsonSerializerStrategy;
		Il2CppObject * L_15 = V_4;
		StringBuilder_t3822575854 * L_16 = ___builder;
		bool L_17 = SimpleJson_SerializeValue_m2154484296(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return (bool)0;
	}

IL_0060:
	{
		StringBuilder_t3822575854 * L_18 = ___builder;
		NullCheck(L_18);
		StringBuilder_Append_m3898090075(L_18, _stringLiteral58, /*hidden argument*/NULL);
		Il2CppObject * L_19 = ___jsonSerializerStrategy;
		Il2CppObject * L_20 = V_4;
		StringBuilder_t3822575854 * L_21 = ___builder;
		bool L_22 = SimpleJson_SerializeValue_m2154484296(NULL /*static, unused*/, L_19, L_20, L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_0079;
		}
	}
	{
		return (bool)0;
	}

IL_0079:
	{
		V_2 = (bool)0;
	}

IL_007b:
	{
		Il2CppObject * L_23 = V_0;
		NullCheck(L_23);
		bool L_24 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_23);
		if (!L_24)
		{
			goto IL_008b;
		}
	}
	{
		Il2CppObject * L_25 = V_1;
		NullCheck(L_25);
		bool L_26 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_25);
		if (L_26)
		{
			goto IL_001e;
		}
	}

IL_008b:
	{
		StringBuilder_t3822575854 * L_27 = ___builder;
		NullCheck(L_27);
		StringBuilder_Append_m3898090075(L_27, _stringLiteral125, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeArray(SimpleJson.IJsonSerializerStrategy,System.Collections.IEnumerable,System.Text.StringBuilder)
extern TypeInfo* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t SimpleJson_SerializeArray_m2191531891_MetadataUsageId;
extern "C"  bool SimpleJson_SerializeArray_m2191531891 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonSerializerStrategy, Il2CppObject * ___anArray, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_SerializeArray_m2191531891_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t3822575854 * L_0 = ___builder;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, _stringLiteral91, /*hidden argument*/NULL);
		V_0 = (bool)1;
		Il2CppObject * L_1 = ___anArray;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_1);
		V_3 = L_2;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003d;
		}

IL_0017:
		{
			Il2CppObject * L_3 = V_3;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			bool L_5 = V_0;
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0021:
		{
			StringBuilder_t3822575854 * L_6 = ___builder;
			NullCheck(L_6);
			StringBuilder_Append_m3898090075(L_6, _stringLiteral44, /*hidden argument*/NULL);
		}

IL_002d:
		{
			Il2CppObject * L_7 = ___jsonSerializerStrategy;
			Il2CppObject * L_8 = V_1;
			StringBuilder_t3822575854 * L_9 = ___builder;
			bool L_10 = SimpleJson_SerializeValue_m2154484296(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
			if (L_10)
			{
				goto IL_003b;
			}
		}

IL_0037:
		{
			V_2 = (bool)0;
			IL2CPP_LEAVE(0x69, FINALLY_0047);
		}

IL_003b:
		{
			V_0 = (bool)0;
		}

IL_003d:
		{
			Il2CppObject * L_11 = V_3;
			NullCheck(L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_11);
			if (L_12)
			{
				goto IL_0017;
			}
		}

IL_0045:
		{
			IL2CPP_LEAVE(0x5B, FINALLY_0047);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_13 = V_3;
			V_4 = ((Il2CppObject *)IsInst(L_13, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_14 = V_4;
			if (!L_14)
			{
				goto IL_005a;
			}
		}

IL_0053:
		{
			Il2CppObject * L_15 = V_4;
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_15);
		}

IL_005a:
		{
			IL2CPP_END_FINALLY(71)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x69, IL_0069)
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005b:
	{
		StringBuilder_t3822575854 * L_16 = ___builder;
		NullCheck(L_16);
		StringBuilder_Append_m3898090075(L_16, _stringLiteral93, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0069:
	{
		bool L_17 = V_2;
		return L_17;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeString(System.String,System.Text.StringBuilder)
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral2886;
extern Il2CppCodeGenString* _stringLiteral2944;
extern Il2CppCodeGenString* _stringLiteral2950;
extern Il2CppCodeGenString* _stringLiteral2954;
extern Il2CppCodeGenString* _stringLiteral2962;
extern Il2CppCodeGenString* _stringLiteral2966;
extern Il2CppCodeGenString* _stringLiteral2968;
extern const uint32_t SimpleJson_SerializeString_m3428852602_MetadataUsageId;
extern "C"  bool SimpleJson_SerializeString_m3428852602 (Il2CppObject * __this /* static, unused */, String_t* ___aString, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_SerializeString_m3428852602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t3416858730* V_0 = NULL;
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	{
		StringBuilder_t3822575854 * L_0 = ___builder;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, _stringLiteral34, /*hidden argument*/NULL);
		String_t* L_1 = ___aString;
		NullCheck(L_1);
		CharU5BU5D_t3416858730* L_2 = String_ToCharArray_m1208288742(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		goto IL_00ae;
	}

IL_001a:
	{
		CharU5BU5D_t3416858730* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_2 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
		uint16_t L_6 = V_2;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0031;
		}
	}
	{
		StringBuilder_t3822575854 * L_7 = ___builder;
		NullCheck(L_7);
		StringBuilder_Append_m3898090075(L_7, _stringLiteral2886, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_0031:
	{
		uint16_t L_8 = V_2;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0044;
		}
	}
	{
		StringBuilder_t3822575854 * L_9 = ___builder;
		NullCheck(L_9);
		StringBuilder_Append_m3898090075(L_9, _stringLiteral2944, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_0044:
	{
		uint16_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)8))))
		{
			goto IL_0056;
		}
	}
	{
		StringBuilder_t3822575854 * L_11 = ___builder;
		NullCheck(L_11);
		StringBuilder_Append_m3898090075(L_11, _stringLiteral2950, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_0056:
	{
		uint16_t L_12 = V_2;
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0069;
		}
	}
	{
		StringBuilder_t3822575854 * L_13 = ___builder;
		NullCheck(L_13);
		StringBuilder_Append_m3898090075(L_13, _stringLiteral2954, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_0069:
	{
		uint16_t L_14 = V_2;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_007c;
		}
	}
	{
		StringBuilder_t3822575854 * L_15 = ___builder;
		NullCheck(L_15);
		StringBuilder_Append_m3898090075(L_15, _stringLiteral2962, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_007c:
	{
		uint16_t L_16 = V_2;
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_008f;
		}
	}
	{
		StringBuilder_t3822575854 * L_17 = ___builder;
		NullCheck(L_17);
		StringBuilder_Append_m3898090075(L_17, _stringLiteral2966, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_008f:
	{
		uint16_t L_18 = V_2;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00a2;
		}
	}
	{
		StringBuilder_t3822575854 * L_19 = ___builder;
		NullCheck(L_19);
		StringBuilder_Append_m3898090075(L_19, _stringLiteral2968, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_00a2:
	{
		StringBuilder_t3822575854 * L_20 = ___builder;
		uint16_t L_21 = V_2;
		NullCheck(L_20);
		StringBuilder_Append_m2143093878(L_20, L_21, /*hidden argument*/NULL);
	}

IL_00aa:
	{
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_00ae:
	{
		int32_t L_23 = V_1;
		CharU5BU5D_t3416858730* L_24 = V_0;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		StringBuilder_t3822575854 * L_25 = ___builder;
		NullCheck(L_25);
		StringBuilder_Append_m3898090075(L_25, _stringLiteral34, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeNumber(System.Object,System.Text.StringBuilder)
extern TypeInfo* Int64_t2847414882_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern TypeInfo* UInt64_t985925421_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t985925326_il2cpp_TypeInfo_var;
extern TypeInfo* Decimal_t1688557254_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t958209021_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral114;
extern const uint32_t SimpleJson_SerializeNumber_m1649989460_MetadataUsageId;
extern "C"  bool SimpleJson_SerializeNumber_m1649989460 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___number, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_SerializeNumber_m1649989460_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	uint64_t V_1 = 0;
	int32_t V_2 = 0;
	uint32_t V_3 = 0;
	Decimal_t1688557254  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	double V_6 = 0.0;
	{
		Il2CppObject * L_0 = ___number;
		if (!((Il2CppObject *)IsInstSealed(L_0, Int64_t2847414882_il2cpp_TypeInfo_var)))
		{
			goto IL_0027;
		}
	}
	{
		StringBuilder_t3822575854 * L_1 = ___builder;
		Il2CppObject * L_2 = ___number;
		V_0 = ((*(int64_t*)((int64_t*)UnBox (L_2, Int64_t2847414882_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_3 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = Int64_ToString_m1593114429((&V_0), L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m3898090075(L_1, L_4, /*hidden argument*/NULL);
		goto IL_0108;
	}

IL_0027:
	{
		Il2CppObject * L_5 = ___number;
		if (!((Il2CppObject *)IsInstSealed(L_5, UInt64_t985925421_il2cpp_TypeInfo_var)))
		{
			goto IL_004e;
		}
	}
	{
		StringBuilder_t3822575854 * L_6 = ___builder;
		Il2CppObject * L_7 = ___number;
		V_1 = ((*(uint64_t*)((uint64_t*)UnBox (L_7, UInt64_t985925421_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_8 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_9 = UInt64_ToString_m2926838398((&V_1), L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		StringBuilder_Append_m3898090075(L_6, L_9, /*hidden argument*/NULL);
		goto IL_0108;
	}

IL_004e:
	{
		Il2CppObject * L_10 = ___number;
		if (!((Il2CppObject *)IsInstSealed(L_10, Int32_t2847414787_il2cpp_TypeInfo_var)))
		{
			goto IL_0075;
		}
	}
	{
		StringBuilder_t3822575854 * L_11 = ___builder;
		Il2CppObject * L_12 = ___number;
		V_2 = ((*(int32_t*)((int32_t*)UnBox (L_12, Int32_t2847414787_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_13 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_14 = Int32_ToString_m881186462((&V_2), L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		StringBuilder_Append_m3898090075(L_11, L_14, /*hidden argument*/NULL);
		goto IL_0108;
	}

IL_0075:
	{
		Il2CppObject * L_15 = ___number;
		if (!((Il2CppObject *)IsInstSealed(L_15, UInt32_t985925326_il2cpp_TypeInfo_var)))
		{
			goto IL_0099;
		}
	}
	{
		StringBuilder_t3822575854 * L_16 = ___builder;
		Il2CppObject * L_17 = ___number;
		V_3 = ((*(uint32_t*)((uint32_t*)UnBox (L_17, UInt32_t985925326_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_18 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_19 = UInt32_ToString_m2214910431((&V_3), L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		StringBuilder_Append_m3898090075(L_16, L_19, /*hidden argument*/NULL);
		goto IL_0108;
	}

IL_0099:
	{
		Il2CppObject * L_20 = ___number;
		if (!((Il2CppObject *)IsInstSealed(L_20, Decimal_t1688557254_il2cpp_TypeInfo_var)))
		{
			goto IL_00be;
		}
	}
	{
		StringBuilder_t3822575854 * L_21 = ___builder;
		Il2CppObject * L_22 = ___number;
		V_4 = ((*(Decimal_t1688557254 *)((Decimal_t1688557254 *)UnBox (L_22, Decimal_t1688557254_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_23 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_24 = Decimal_ToString_m1778338145((&V_4), L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		StringBuilder_Append_m3898090075(L_21, L_24, /*hidden argument*/NULL);
		goto IL_0108;
	}

IL_00be:
	{
		Il2CppObject * L_25 = ___number;
		if (!((Il2CppObject *)IsInstSealed(L_25, Single_t958209021_il2cpp_TypeInfo_var)))
		{
			goto IL_00e3;
		}
	}
	{
		StringBuilder_t3822575854 * L_26 = ___builder;
		Il2CppObject * L_27 = ___number;
		V_5 = ((*(float*)((float*)UnBox (L_27, Single_t958209021_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_28 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_29 = Single_ToString_m1436803918((&V_5), L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		StringBuilder_Append_m3898090075(L_26, L_29, /*hidden argument*/NULL);
		goto IL_0108;
	}

IL_00e3:
	{
		StringBuilder_t3822575854 * L_30 = ___builder;
		Il2CppObject * L_31 = ___number;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_32 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		double L_33 = Convert_ToDouble_m1171958389(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		V_6 = L_33;
		CultureInfo_t3603717042 * L_34 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_35 = Double_ToString_m2573497243((&V_6), _stringLiteral114, L_34, /*hidden argument*/NULL);
		NullCheck(L_30);
		StringBuilder_Append_m3898090075(L_30, L_35, /*hidden argument*/NULL);
	}

IL_0108:
	{
		return (bool)1;
	}
}
// System.Boolean SimpleJson.SimpleJson::IsNumeric(System.Object)
extern TypeInfo* SByte_t2855346064_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t2778693821_il2cpp_TypeInfo_var;
extern TypeInfo* Int16_t2847414729_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t985925268_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t985925326_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t2847414882_il2cpp_TypeInfo_var;
extern TypeInfo* UInt64_t985925421_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t958209021_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t534516614_il2cpp_TypeInfo_var;
extern TypeInfo* Decimal_t1688557254_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_IsNumeric_m2618022454_MetadataUsageId;
extern "C"  bool SimpleJson_IsNumeric_m2618022454 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_IsNumeric_m2618022454_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_0, SByte_t2855346064_il2cpp_TypeInfo_var)))
		{
			goto IL_000a;
		}
	}
	{
		return (bool)1;
	}

IL_000a:
	{
		Il2CppObject * L_1 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_1, Byte_t2778693821_il2cpp_TypeInfo_var)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)1;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_2, Int16_t2847414729_il2cpp_TypeInfo_var)))
		{
			goto IL_001e;
		}
	}
	{
		return (bool)1;
	}

IL_001e:
	{
		Il2CppObject * L_3 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_3, UInt16_t985925268_il2cpp_TypeInfo_var)))
		{
			goto IL_0028;
		}
	}
	{
		return (bool)1;
	}

IL_0028:
	{
		Il2CppObject * L_4 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_4, Int32_t2847414787_il2cpp_TypeInfo_var)))
		{
			goto IL_0032;
		}
	}
	{
		return (bool)1;
	}

IL_0032:
	{
		Il2CppObject * L_5 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_5, UInt32_t985925326_il2cpp_TypeInfo_var)))
		{
			goto IL_003c;
		}
	}
	{
		return (bool)1;
	}

IL_003c:
	{
		Il2CppObject * L_6 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_6, Int64_t2847414882_il2cpp_TypeInfo_var)))
		{
			goto IL_0046;
		}
	}
	{
		return (bool)1;
	}

IL_0046:
	{
		Il2CppObject * L_7 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_7, UInt64_t985925421_il2cpp_TypeInfo_var)))
		{
			goto IL_0050;
		}
	}
	{
		return (bool)1;
	}

IL_0050:
	{
		Il2CppObject * L_8 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_8, Single_t958209021_il2cpp_TypeInfo_var)))
		{
			goto IL_005a;
		}
	}
	{
		return (bool)1;
	}

IL_005a:
	{
		Il2CppObject * L_9 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_9, Double_t534516614_il2cpp_TypeInfo_var)))
		{
			goto IL_0064;
		}
	}
	{
		return (bool)1;
	}

IL_0064:
	{
		Il2CppObject * L_10 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_10, Decimal_t1688557254_il2cpp_TypeInfo_var)))
		{
			goto IL_006e;
		}
	}
	{
		return (bool)1;
	}

IL_006e:
	{
		return (bool)0;
	}
}
// SimpleJson.IJsonSerializerStrategy SimpleJson.SimpleJson::get_CurrentJsonSerializerStrategy()
extern TypeInfo* SimpleJson_t2437938008_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_get_CurrentJsonSerializerStrategy_m2508654719_MetadataUsageId;
extern "C"  Il2CppObject * SimpleJson_get_CurrentJsonSerializerStrategy_m2508654719 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_get_CurrentJsonSerializerStrategy_m2508654719_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject * L_0 = ((SimpleJson_t2437938008_StaticFields*)SimpleJson_t2437938008_il2cpp_TypeInfo_var->static_fields)->get_currentJsonSerializerStrategy_0();
		Il2CppObject * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0014;
		}
	}
	{
		DataContractJsonSerializerStrategy_t3752539591 * L_2 = SimpleJson_get_DataContractJsonSerializerStrategy_m4288620559(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataContractJsonSerializerStrategy_t3752539591 * L_3 = L_2;
		((SimpleJson_t2437938008_StaticFields*)SimpleJson_t2437938008_il2cpp_TypeInfo_var->static_fields)->set_currentJsonSerializerStrategy_0(L_3);
		G_B2_0 = ((Il2CppObject *)(L_3));
	}

IL_0014:
	{
		return G_B2_0;
	}
}
// SimpleJson.DataContractJsonSerializerStrategy SimpleJson.SimpleJson::get_DataContractJsonSerializerStrategy()
extern TypeInfo* SimpleJson_t2437938008_il2cpp_TypeInfo_var;
extern TypeInfo* DataContractJsonSerializerStrategy_t3752539591_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_get_DataContractJsonSerializerStrategy_m4288620559_MetadataUsageId;
extern "C"  DataContractJsonSerializerStrategy_t3752539591 * SimpleJson_get_DataContractJsonSerializerStrategy_m4288620559 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_get_DataContractJsonSerializerStrategy_m4288620559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataContractJsonSerializerStrategy_t3752539591 * G_B2_0 = NULL;
	DataContractJsonSerializerStrategy_t3752539591 * G_B1_0 = NULL;
	{
		DataContractJsonSerializerStrategy_t3752539591 * L_0 = ((SimpleJson_t2437938008_StaticFields*)SimpleJson_t2437938008_il2cpp_TypeInfo_var->static_fields)->get_dataContractJsonSerializerStrategy_1();
		DataContractJsonSerializerStrategy_t3752539591 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0014;
		}
	}
	{
		DataContractJsonSerializerStrategy_t3752539591 * L_2 = (DataContractJsonSerializerStrategy_t3752539591 *)il2cpp_codegen_object_new(DataContractJsonSerializerStrategy_t3752539591_il2cpp_TypeInfo_var);
		DataContractJsonSerializerStrategy__ctor_m696556770(L_2, /*hidden argument*/NULL);
		DataContractJsonSerializerStrategy_t3752539591 * L_3 = L_2;
		((SimpleJson_t2437938008_StaticFields*)SimpleJson_t2437938008_il2cpp_TypeInfo_var->static_fields)->set_dataContractJsonSerializerStrategy_1(L_3);
		G_B2_0 = L_3;
	}

IL_0014:
	{
		return G_B2_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
