﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SocketIOClient.IClient
struct IClient_t1294359360;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIOClient.EndPointClient
struct  EndPointClient_t1953350540  : public Il2CppObject
{
public:
	// SocketIOClient.IClient SocketIOClient.EndPointClient::<Client>k__BackingField
	Il2CppObject * ___U3CClientU3Ek__BackingField_0;
	// System.String SocketIOClient.EndPointClient::<EndPoint>k__BackingField
	String_t* ___U3CEndPointU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CClientU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EndPointClient_t1953350540, ___U3CClientU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CClientU3Ek__BackingField_0() const { return ___U3CClientU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CClientU3Ek__BackingField_0() { return &___U3CClientU3Ek__BackingField_0; }
	inline void set_U3CClientU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CClientU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CClientU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CEndPointU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EndPointClient_t1953350540, ___U3CEndPointU3Ek__BackingField_1)); }
	inline String_t* get_U3CEndPointU3Ek__BackingField_1() const { return ___U3CEndPointU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CEndPointU3Ek__BackingField_1() { return &___U3CEndPointU3Ek__BackingField_1; }
	inline void set_U3CEndPointU3Ek__BackingField_1(String_t* value)
	{
		___U3CEndPointU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEndPointU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
