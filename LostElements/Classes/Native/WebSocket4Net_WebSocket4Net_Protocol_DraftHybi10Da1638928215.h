﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<WebSocket4Net.Protocol.WebSocketDataFrame>
struct List_1_t1019692775;
// WebSocket4Net.Protocol.WebSocketDataFrame
struct WebSocketDataFrame_t222733806;
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader
struct IDataFramePartReader_t2102555929;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.DraftHybi10DataReader
struct  DraftHybi10DataReader_t1638928215  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<WebSocket4Net.Protocol.WebSocketDataFrame> WebSocket4Net.Protocol.DraftHybi10DataReader::m_PreviousFrames
	List_1_t1019692775 * ___m_PreviousFrames_0;
	// WebSocket4Net.Protocol.WebSocketDataFrame WebSocket4Net.Protocol.DraftHybi10DataReader::m_Frame
	WebSocketDataFrame_t222733806 * ___m_Frame_1;
	// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.DraftHybi10DataReader::m_PartReader
	Il2CppObject * ___m_PartReader_2;
	// System.Int32 WebSocket4Net.Protocol.DraftHybi10DataReader::m_LastPartLength
	int32_t ___m_LastPartLength_3;

public:
	inline static int32_t get_offset_of_m_PreviousFrames_0() { return static_cast<int32_t>(offsetof(DraftHybi10DataReader_t1638928215, ___m_PreviousFrames_0)); }
	inline List_1_t1019692775 * get_m_PreviousFrames_0() const { return ___m_PreviousFrames_0; }
	inline List_1_t1019692775 ** get_address_of_m_PreviousFrames_0() { return &___m_PreviousFrames_0; }
	inline void set_m_PreviousFrames_0(List_1_t1019692775 * value)
	{
		___m_PreviousFrames_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_PreviousFrames_0, value);
	}

	inline static int32_t get_offset_of_m_Frame_1() { return static_cast<int32_t>(offsetof(DraftHybi10DataReader_t1638928215, ___m_Frame_1)); }
	inline WebSocketDataFrame_t222733806 * get_m_Frame_1() const { return ___m_Frame_1; }
	inline WebSocketDataFrame_t222733806 ** get_address_of_m_Frame_1() { return &___m_Frame_1; }
	inline void set_m_Frame_1(WebSocketDataFrame_t222733806 * value)
	{
		___m_Frame_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Frame_1, value);
	}

	inline static int32_t get_offset_of_m_PartReader_2() { return static_cast<int32_t>(offsetof(DraftHybi10DataReader_t1638928215, ___m_PartReader_2)); }
	inline Il2CppObject * get_m_PartReader_2() const { return ___m_PartReader_2; }
	inline Il2CppObject ** get_address_of_m_PartReader_2() { return &___m_PartReader_2; }
	inline void set_m_PartReader_2(Il2CppObject * value)
	{
		___m_PartReader_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_PartReader_2, value);
	}

	inline static int32_t get_offset_of_m_LastPartLength_3() { return static_cast<int32_t>(offsetof(DraftHybi10DataReader_t1638928215, ___m_LastPartLength_3)); }
	inline int32_t get_m_LastPartLength_3() const { return ___m_LastPartLength_3; }
	inline int32_t* get_address_of_m_LastPartLength_3() { return &___m_LastPartLength_3; }
	inline void set_m_LastPartLength_3(int32_t value)
	{
		___m_LastPartLength_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
