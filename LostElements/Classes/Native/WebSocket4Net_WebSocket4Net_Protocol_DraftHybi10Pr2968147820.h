﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Random
struct Random_t922188920;

#include "WebSocket4Net_WebSocket4Net_Protocol_ProtocolProces894595261.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.DraftHybi10Processor
struct  DraftHybi10Processor_t2968147820  : public ProtocolProcessorBase_t894595261
{
public:
	// System.String WebSocket4Net.Protocol.DraftHybi10Processor::m_ExpectedAcceptKey
	String_t* ___m_ExpectedAcceptKey_3;

public:
	inline static int32_t get_offset_of_m_ExpectedAcceptKey_3() { return static_cast<int32_t>(offsetof(DraftHybi10Processor_t2968147820, ___m_ExpectedAcceptKey_3)); }
	inline String_t* get_m_ExpectedAcceptKey_3() const { return ___m_ExpectedAcceptKey_3; }
	inline String_t** get_address_of_m_ExpectedAcceptKey_3() { return &___m_ExpectedAcceptKey_3; }
	inline void set_m_ExpectedAcceptKey_3(String_t* value)
	{
		___m_ExpectedAcceptKey_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_ExpectedAcceptKey_3, value);
	}
};

struct DraftHybi10Processor_t2968147820_StaticFields
{
public:
	// System.Random WebSocket4Net.Protocol.DraftHybi10Processor::m_Random
	Random_t922188920 * ___m_Random_4;

public:
	inline static int32_t get_offset_of_m_Random_4() { return static_cast<int32_t>(offsetof(DraftHybi10Processor_t2968147820_StaticFields, ___m_Random_4)); }
	inline Random_t922188920 * get_m_Random_4() const { return ___m_Random_4; }
	inline Random_t922188920 ** get_address_of_m_Random_4() { return &___m_Random_4; }
	inline void set_m_Random_4(Random_t922188920 * value)
	{
		___m_Random_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Random_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
