﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1327917203MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::.ctor()
#define Dictionary_2__ctor_m1906966347(__this, method) ((  void (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2__ctor_m1859298524_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m2806091225(__this, ___comparer, method) ((  void (*) (Dictionary_2_t1476369908 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3610002771_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m3256923827(__this, ___capacity, method) ((  void (*) (Dictionary_2_t1476369908 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3273912365_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m1533259427(__this, ___info, ___context, method) ((  void (*) (Dictionary_2_t1476369908 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))Dictionary_2__ctor_m1549788189_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m729935532(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1428264956_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1563267820(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2955279704_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m796335078(__this, ___key, method) ((  Il2CppObject * (*) (Dictionary_2_t1476369908 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2843055522_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1983156235(__this, ___key, ___value, method) ((  void (*) (Dictionary_2_t1476369908 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2020057553_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m1746178918(__this, ___key, ___value, method) ((  void (*) (Dictionary_2_t1476369908 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2894265824_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m4203685584(__this, ___key, method) ((  bool (*) (Dictionary_2_t1476369908 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2093434578_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m1751547977(__this, ___key, method) ((  void (*) (Dictionary_2_t1476369908 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m127000079_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2186312324(__this, method) ((  bool (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4062325186_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1133072624(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4065571764_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m109413832(__this, method) ((  bool (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m840305542_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3144332383(__this, ___keyValuePair, method) ((  void (*) (Dictionary_2_t1476369908 *, KeyValuePair_2_t964901206 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2185230117_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3970094659(__this, ___keyValuePair, method) ((  bool (*) (Dictionary_2_t1476369908 *, KeyValuePair_2_t964901206 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3713378305_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1627187843(__this, ___array, ___index, method) ((  void (*) (Dictionary_2_t1476369908 *, KeyValuePair_2U5BU5D_t2743498899*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4220340169_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2959585832(__this, ___keyValuePair, method) ((  bool (*) (Dictionary_2_t1476369908 *, KeyValuePair_2_t964901206 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3330268006_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3387683618(__this, ___array, ___index, method) ((  void (*) (Dictionary_2_t1476369908 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m323672040_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1346115997(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m464503287_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3726773210(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1289662318_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1747036469(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3187662523_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::get_Count()
#define Dictionary_2_get_Count_m2207810250(__this, method) ((  int32_t (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_get_Count_m655926012_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::get_Item(TKey)
#define Dictionary_2_get_Item_m3498287457(__this, ___key, method) ((  Action_1_t985559125 * (*) (Dictionary_2_t1476369908 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m542157067_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m4073481890(__this, ___key, ___value, method) ((  void (*) (Dictionary_2_t1476369908 *, int32_t, Action_1_t985559125 *, const MethodInfo*))Dictionary_2_set_Item_m3219597724_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m3346090970(__this, ___capacity, ___hcp, method) ((  void (*) (Dictionary_2_t1476369908 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3161627732_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m2061149981(__this, ___size, method) ((  void (*) (Dictionary_2_t1476369908 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3089254883_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m2433240153(__this, ___array, ___index, method) ((  void (*) (Dictionary_2_t1476369908 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3741359263_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m3060544933(__this /* static, unused */, ___key, ___value, method) ((  KeyValuePair_2_t964901206  (*) (Il2CppObject * /* static, unused */, int32_t, Action_1_t985559125 *, const MethodInfo*))Dictionary_2_make_pair_m2338171699_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m3240263089(__this /* static, unused */, ___key, ___value, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Action_1_t985559125 *, const MethodInfo*))Dictionary_2_pick_key_m1394751787_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m3937092657(__this /* static, unused */, ___key, ___value, method) ((  Action_1_t985559125 * (*) (Il2CppObject * /* static, unused */, int32_t, Action_1_t985559125 *, const MethodInfo*))Dictionary_2_pick_value_m1281047495_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m4240229718(__this, ___array, ___index, method) ((  void (*) (Dictionary_2_t1476369908 *, KeyValuePair_2U5BU5D_t2743498899*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2503627344_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::Resize()
#define Dictionary_2_Resize_m1601302550(__this, method) ((  void (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_Resize_m1861476060_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::Add(TKey,TValue)
#define Dictionary_2_Add_m979926675(__this, ___key, ___value, method) ((  void (*) (Dictionary_2_t1476369908 *, int32_t, Action_1_t985559125 *, const MethodInfo*))Dictionary_2_Add_m2232043353_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::Clear()
#define Dictionary_2_Clear_m3413459085(__this, method) ((  void (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_Clear_m3560399111_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m747089203(__this, ___key, method) ((  bool (*) (Dictionary_2_t1476369908 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2612169713_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m236138419(__this, ___value, method) ((  bool (*) (Dictionary_2_t1476369908 *, Action_1_t985559125 *, const MethodInfo*))Dictionary_2_ContainsValue_m454328177_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m3014238976(__this, ___info, ___context, method) ((  void (*) (Dictionary_2_t1476369908 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))Dictionary_2_GetObjectData_m3426598522_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m3959533412(__this, ___sender, method) ((  void (*) (Dictionary_2_t1476369908 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3983879210_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::Remove(TKey)
#define Dictionary_2_Remove_m3260536797(__this, ___key, method) ((  bool (*) (Dictionary_2_t1476369908 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m183515743_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m2377720460(__this, ___key, ___value, method) ((  bool (*) (Dictionary_2_t1476369908 *, int32_t, Action_1_t985559125 **, const MethodInfo*))Dictionary_2_TryGetValue_m2515559242_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::get_Keys()
#define Dictionary_2_get_Keys_m2346021235(__this, method) ((  KeyCollection_t3799645188 * (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_get_Keys_m4120714641_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::get_Values()
#define Dictionary_2_get_Values_m1741800755(__this, method) ((  ValueCollection_t3398507002 * (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_get_Values_m1815086189_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m2690121996(__this, ___key, method) ((  int32_t (*) (Dictionary_2_t1476369908 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m844610694_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m2249406796(__this, ___value, method) ((  Action_1_t985559125 * (*) (Dictionary_2_t1476369908 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3888328930_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m3096157632(__this, ___pair, method) ((  bool (*) (Dictionary_2_t1476369908 *, KeyValuePair_2_t964901206 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m139391042_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m4210638951(__this, method) ((  Enumerator_t1243397849  (*) (Dictionary_2_t1476369908 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3720989159_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m4154239990(__this /* static, unused */, ___key, ___value, method) ((  DictionaryEntry_t130027246  (*) (Il2CppObject * /* static, unused */, int32_t, Action_1_t985559125 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2937104030_gshared)(__this /* static, unused */, ___key, ___value, method)
