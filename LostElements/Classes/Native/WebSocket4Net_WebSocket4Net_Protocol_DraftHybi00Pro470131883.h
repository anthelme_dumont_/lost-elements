﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Char>
struct List_1_t3575665668;
// System.Random
struct Random_t922188920;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Func`2<System.Char,System.Boolean>
struct Func_2_t620049934;

#include "WebSocket4Net_WebSocket4Net_Protocol_ProtocolProces894595261.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.DraftHybi00Processor
struct  DraftHybi00Processor_t470131883  : public ProtocolProcessorBase_t894595261
{
public:
	// System.Byte[] WebSocket4Net.Protocol.DraftHybi00Processor::m_ExpectedChallenge
	ByteU5BU5D_t58506160* ___m_ExpectedChallenge_7;

public:
	inline static int32_t get_offset_of_m_ExpectedChallenge_7() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_t470131883, ___m_ExpectedChallenge_7)); }
	inline ByteU5BU5D_t58506160* get_m_ExpectedChallenge_7() const { return ___m_ExpectedChallenge_7; }
	inline ByteU5BU5D_t58506160** get_address_of_m_ExpectedChallenge_7() { return &___m_ExpectedChallenge_7; }
	inline void set_m_ExpectedChallenge_7(ByteU5BU5D_t58506160* value)
	{
		___m_ExpectedChallenge_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_ExpectedChallenge_7, value);
	}
};

struct DraftHybi00Processor_t470131883_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Char> WebSocket4Net.Protocol.DraftHybi00Processor::m_CharLib
	List_1_t3575665668 * ___m_CharLib_3;
	// System.Collections.Generic.List`1<System.Char> WebSocket4Net.Protocol.DraftHybi00Processor::m_DigLib
	List_1_t3575665668 * ___m_DigLib_4;
	// System.Random WebSocket4Net.Protocol.DraftHybi00Processor::m_Random
	Random_t922188920 * ___m_Random_5;
	// System.Byte[] WebSocket4Net.Protocol.DraftHybi00Processor::CloseHandshake
	ByteU5BU5D_t58506160* ___CloseHandshake_6;
	// System.Func`2<System.Char,System.Boolean> WebSocket4Net.Protocol.DraftHybi00Processor::CS$<>9__CachedAnonymousMethodDelegate2
	Func_2_t620049934 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8;
	// System.Func`2<System.Char,System.Boolean> WebSocket4Net.Protocol.DraftHybi00Processor::CS$<>9__CachedAnonymousMethodDelegate3
	Func_2_t620049934 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9;

public:
	inline static int32_t get_offset_of_m_CharLib_3() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_t470131883_StaticFields, ___m_CharLib_3)); }
	inline List_1_t3575665668 * get_m_CharLib_3() const { return ___m_CharLib_3; }
	inline List_1_t3575665668 ** get_address_of_m_CharLib_3() { return &___m_CharLib_3; }
	inline void set_m_CharLib_3(List_1_t3575665668 * value)
	{
		___m_CharLib_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_CharLib_3, value);
	}

	inline static int32_t get_offset_of_m_DigLib_4() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_t470131883_StaticFields, ___m_DigLib_4)); }
	inline List_1_t3575665668 * get_m_DigLib_4() const { return ___m_DigLib_4; }
	inline List_1_t3575665668 ** get_address_of_m_DigLib_4() { return &___m_DigLib_4; }
	inline void set_m_DigLib_4(List_1_t3575665668 * value)
	{
		___m_DigLib_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_DigLib_4, value);
	}

	inline static int32_t get_offset_of_m_Random_5() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_t470131883_StaticFields, ___m_Random_5)); }
	inline Random_t922188920 * get_m_Random_5() const { return ___m_Random_5; }
	inline Random_t922188920 ** get_address_of_m_Random_5() { return &___m_Random_5; }
	inline void set_m_Random_5(Random_t922188920 * value)
	{
		___m_Random_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_Random_5, value);
	}

	inline static int32_t get_offset_of_CloseHandshake_6() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_t470131883_StaticFields, ___CloseHandshake_6)); }
	inline ByteU5BU5D_t58506160* get_CloseHandshake_6() const { return ___CloseHandshake_6; }
	inline ByteU5BU5D_t58506160** get_address_of_CloseHandshake_6() { return &___CloseHandshake_6; }
	inline void set_CloseHandshake_6(ByteU5BU5D_t58506160* value)
	{
		___CloseHandshake_6 = value;
		Il2CppCodeGenWriteBarrier(&___CloseHandshake_6, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_t470131883_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8)); }
	inline Func_2_t620049934 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8; }
	inline Func_2_t620049934 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8(Func_2_t620049934 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_8, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9() { return static_cast<int32_t>(offsetof(DraftHybi00Processor_t470131883_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9)); }
	inline Func_2_t620049934 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9; }
	inline Func_2_t620049934 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9(Func_2_t620049934 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
