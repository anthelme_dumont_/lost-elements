﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3540781296MethodDeclarations.h"

// System.Void System.Comparison`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m1630547249(__this, ___object, ___method, method) ((  void (*) (Comparison_1_t2079643127 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::Invoke(T,T)
#define Comparison_1_Invoke_m2757019183(__this, ___x, ___y, method) ((  int32_t (*) (Comparison_1_t2079643127 *, ArraySegmentEx_1_t3670935547 *, ArraySegmentEx_1_t3670935547 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m2480557736(__this, ___x, ___y, ___callback, ___object, method) ((  Il2CppObject * (*) (Comparison_1_t2079643127 *, ArraySegmentEx_1_t3670935547 *, ArraySegmentEx_1_t3670935547 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m1440155101(__this, ___result, method) ((  int32_t (*) (Comparison_1_t2079643127 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result, method)
