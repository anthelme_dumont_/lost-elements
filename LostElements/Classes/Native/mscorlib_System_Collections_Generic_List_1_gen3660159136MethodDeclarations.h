﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct List_1_t3660159136;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct IEnumerable_1_t1440387227;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct IEnumerator_1_t51339319;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct ICollection_1_t3329031553;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct ReadOnlyCollection_1_t1731378219;
// System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>[]
struct KeyValuePair_2U5BU5D_t140085726;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct Predicate_1_t3434164065;
// System.Collections.Generic.IComparer`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct IComparer_1_t1267940280;
// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct Comparison_1_t1271907747;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22863200167.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1745942128.h"

// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::.ctor()
extern "C"  void List_1__ctor_m2021390147_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1__ctor_m2021390147(__this, method) ((  void (*) (List_1_t3660159136 *, const MethodInfo*))List_1__ctor_m2021390147_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m276113756_gshared (List_1_t3660159136 * __this, Il2CppObject* ___collection, const MethodInfo* method);
#define List_1__ctor_m276113756(__this, ___collection, method) ((  void (*) (List_1_t3660159136 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m276113756_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1715281837_gshared (List_1_t3660159136 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1715281837(__this, ___capacity, method) ((  void (*) (List_1_t3660159136 *, int32_t, const MethodInfo*))List_1__ctor_m1715281837_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::.cctor()
extern "C"  void List_1__cctor_m2051456202_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2051456202(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2051456202_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1922469077_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1922469077(__this, method) ((  Il2CppObject* (*) (List_1_t3660159136 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1922469077_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m311300961_gshared (List_1_t3660159136 * __this, Il2CppArray * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m311300961(__this, ___array, ___arrayIndex, method) ((  void (*) (List_1_t3660159136 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m311300961_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3352967964_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3352967964(__this, method) ((  Il2CppObject * (*) (List_1_t3660159136 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3352967964_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1535021781_gshared (List_1_t3660159136 * __this, Il2CppObject * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1535021781(__this, ___item, method) ((  int32_t (*) (List_1_t3660159136 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1535021781_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1118845591_gshared (List_1_t3660159136 * __this, Il2CppObject * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1118845591(__this, ___item, method) ((  bool (*) (List_1_t3660159136 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1118845591_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m819943085_gshared (List_1_t3660159136 * __this, Il2CppObject * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m819943085(__this, ___item, method) ((  int32_t (*) (List_1_t3660159136 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m819943085_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3264256088_gshared (List_1_t3660159136 * __this, int32_t ___index, Il2CppObject * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3264256088(__this, ___index, ___item, method) ((  void (*) (List_1_t3660159136 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3264256088_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m75129296_gshared (List_1_t3660159136 * __this, Il2CppObject * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m75129296(__this, ___item, method) ((  void (*) (List_1_t3660159136 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m75129296_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3921746392_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3921746392(__this, method) ((  bool (*) (List_1_t3660159136 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3921746392_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2577760613_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2577760613(__this, method) ((  bool (*) (List_1_t3660159136 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2577760613_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m4119800529_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m4119800529(__this, method) ((  Il2CppObject * (*) (List_1_t3660159136 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4119800529_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m767649606_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m767649606(__this, method) ((  bool (*) (List_1_t3660159136 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m767649606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3942455027_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3942455027(__this, method) ((  bool (*) (List_1_t3660159136 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3942455027_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m234073112_gshared (List_1_t3660159136 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m234073112(__this, ___index, method) ((  Il2CppObject * (*) (List_1_t3660159136 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m234073112_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2237926959_gshared (List_1_t3660159136 * __this, int32_t ___index, Il2CppObject * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2237926959(__this, ___index, ___value, method) ((  void (*) (List_1_t3660159136 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2237926959_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Add(T)
extern "C"  void List_1_Add_m2272105692_gshared (List_1_t3660159136 * __this, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define List_1_Add_m2272105692(__this, ___item, method) ((  void (*) (List_1_t3660159136 *, KeyValuePair_2_t2863200167 , const MethodInfo*))List_1_Add_m2272105692_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2080488727_gshared (List_1_t3660159136 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2080488727(__this, ___newCount, method) ((  void (*) (List_1_t3660159136 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2080488727_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m4179621909_gshared (List_1_t3660159136 * __this, Il2CppObject* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m4179621909(__this, ___collection, method) ((  void (*) (List_1_t3660159136 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m4179621909_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3440594133_gshared (List_1_t3660159136 * __this, Il2CppObject* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m3440594133(__this, ___enumerable, method) ((  void (*) (List_1_t3660159136 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3440594133_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m598487202_gshared (List_1_t3660159136 * __this, Il2CppObject* ___collection, const MethodInfo* method);
#define List_1_AddRange_m598487202(__this, ___collection, method) ((  void (*) (List_1_t3660159136 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m598487202_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1731378219 * List_1_AsReadOnly_m1458958535_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1458958535(__this, method) ((  ReadOnlyCollection_1_t1731378219 * (*) (List_1_t3660159136 *, const MethodInfo*))List_1_AsReadOnly_m1458958535_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Clear()
extern "C"  void List_1_Clear_m3722490734_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_Clear_m3722490734(__this, method) ((  void (*) (List_1_t3660159136 *, const MethodInfo*))List_1_Clear_m3722490734_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Contains(T)
extern "C"  bool List_1_Contains_m2547344412_gshared (List_1_t3660159136 * __this, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define List_1_Contains_m2547344412(__this, ___item, method) ((  bool (*) (List_1_t3660159136 *, KeyValuePair_2_t2863200167 , const MethodInfo*))List_1_Contains_m2547344412_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1916960204_gshared (List_1_t3660159136 * __this, KeyValuePair_2U5BU5D_t140085726* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1916960204(__this, ___array, ___arrayIndex, method) ((  void (*) (List_1_t3660159136 *, KeyValuePair_2U5BU5D_t140085726*, int32_t, const MethodInfo*))List_1_CopyTo_m1916960204_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Find(System.Predicate`1<T>)
extern "C"  KeyValuePair_2_t2863200167  List_1_Find_m363559260_gshared (List_1_t3660159136 * __this, Predicate_1_t3434164065 * ___match, const MethodInfo* method);
#define List_1_Find_m363559260(__this, ___match, method) ((  KeyValuePair_2_t2863200167  (*) (List_1_t3660159136 *, Predicate_1_t3434164065 *, const MethodInfo*))List_1_Find_m363559260_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3976357527_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3434164065 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m3976357527(__this /* static, unused */, ___match, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3434164065 *, const MethodInfo*))List_1_CheckMatch_m3976357527_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m923538364_gshared (List_1_t3660159136 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3434164065 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m923538364(__this, ___startIndex, ___count, ___match, method) ((  int32_t (*) (List_1_t3660159136 *, int32_t, int32_t, Predicate_1_t3434164065 *, const MethodInfo*))List_1_GetIndex_m923538364_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::GetEnumerator()
extern "C"  Enumerator_t1745942128  List_1_GetEnumerator_m110357337_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m110357337(__this, method) ((  Enumerator_t1745942128  (*) (List_1_t3660159136 *, const MethodInfo*))List_1_GetEnumerator_m110357337_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3936423248_gshared (List_1_t3660159136 * __this, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define List_1_IndexOf_m3936423248(__this, ___item, method) ((  int32_t (*) (List_1_t3660159136 *, KeyValuePair_2_t2863200167 , const MethodInfo*))List_1_IndexOf_m3936423248_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m223725603_gshared (List_1_t3660159136 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m223725603(__this, ___start, ___delta, method) ((  void (*) (List_1_t3660159136 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m223725603_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1663327388_gshared (List_1_t3660159136 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1663327388(__this, ___index, method) ((  void (*) (List_1_t3660159136 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1663327388_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3518863171_gshared (List_1_t3660159136 * __this, int32_t ___index, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define List_1_Insert_m3518863171(__this, ___index, ___item, method) ((  void (*) (List_1_t3660159136 *, int32_t, KeyValuePair_2_t2863200167 , const MethodInfo*))List_1_Insert_m3518863171_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1348901240_gshared (List_1_t3660159136 * __this, Il2CppObject* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1348901240(__this, ___collection, method) ((  void (*) (List_1_t3660159136 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1348901240_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Remove(T)
extern "C"  bool List_1_Remove_m1208777495_gshared (List_1_t3660159136 * __this, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define List_1_Remove_m1208777495(__this, ___item, method) ((  bool (*) (List_1_t3660159136 *, KeyValuePair_2_t2863200167 , const MethodInfo*))List_1_Remove_m1208777495_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3175348499_gshared (List_1_t3660159136 * __this, Predicate_1_t3434164065 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m3175348499(__this, ___match, method) ((  int32_t (*) (List_1_t3660159136 *, Predicate_1_t3434164065 *, const MethodInfo*))List_1_RemoveAll_m3175348499_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1392716041_gshared (List_1_t3660159136 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1392716041(__this, ___index, method) ((  void (*) (List_1_t3660159136 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1392716041_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Reverse()
extern "C"  void List_1_Reverse_m1268524739_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_Reverse_m1268524739(__this, method) ((  void (*) (List_1_t3660159136 *, const MethodInfo*))List_1_Reverse_m1268524739_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Sort()
extern "C"  void List_1_Sort_m2798077887_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_Sort_m2798077887(__this, method) ((  void (*) (List_1_t3660159136 *, const MethodInfo*))List_1_Sort_m2798077887_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2331803909_gshared (List_1_t3660159136 * __this, Il2CppObject* ___comparer, const MethodInfo* method);
#define List_1_Sort_m2331803909(__this, ___comparer, method) ((  void (*) (List_1_t3660159136 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2331803909_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m227502802_gshared (List_1_t3660159136 * __this, Comparison_1_t1271907747 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m227502802(__this, ___comparison, method) ((  void (*) (List_1_t3660159136 *, Comparison_1_t1271907747 *, const MethodInfo*))List_1_Sort_m227502802_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t140085726* List_1_ToArray_m998832130_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_ToArray_m998832130(__this, method) ((  KeyValuePair_2U5BU5D_t140085726* (*) (List_1_t3660159136 *, const MethodInfo*))List_1_ToArray_m998832130_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1877695512_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1877695512(__this, method) ((  void (*) (List_1_t3660159136 *, const MethodInfo*))List_1_TrimExcess_m1877695512_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1306123648_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1306123648(__this, method) ((  int32_t (*) (List_1_t3660159136 *, const MethodInfo*))List_1_get_Capacity_m1306123648_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3585644841_gshared (List_1_t3660159136 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m3585644841(__this, ___value, method) ((  void (*) (List_1_t3660159136 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3585644841_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::get_Count()
extern "C"  int32_t List_1_get_Count_m1587343915_gshared (List_1_t3660159136 * __this, const MethodInfo* method);
#define List_1_get_Count_m1587343915(__this, method) ((  int32_t (*) (List_1_t3660159136 *, const MethodInfo*))List_1_get_Count_m1587343915_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t2863200167  List_1_get_Item_m2443679693_gshared (List_1_t3660159136 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m2443679693(__this, ___index, method) ((  KeyValuePair_2_t2863200167  (*) (List_1_t3660159136 *, int32_t, const MethodInfo*))List_1_get_Item_m2443679693_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3297842010_gshared (List_1_t3660159136 * __this, int32_t ___index, KeyValuePair_2_t2863200167  ___value, const MethodInfo* method);
#define List_1_set_Item_m3297842010(__this, ___index, ___value, method) ((  void (*) (List_1_t3660159136 *, int32_t, KeyValuePair_2_t2863200167 , const MethodInfo*))List_1_set_Item_m3297842010_gshared)(__this, ___index, ___value, method)
