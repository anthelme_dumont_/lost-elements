﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct Collection_1_t537220601;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>[]
struct KeyValuePair_2U5BU5D_t140085726;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct IEnumerator_1_t51339319;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct IList_1_t734725185;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22863200167.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::.ctor()
extern "C"  void Collection_1__ctor_m31660432_gshared (Collection_1_t537220601 * __this, const MethodInfo* method);
#define Collection_1__ctor_m31660432(__this, method) ((  void (*) (Collection_1_t537220601 *, const MethodInfo*))Collection_1__ctor_m31660432_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m998550507_gshared (Collection_1_t537220601 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m998550507(__this, method) ((  bool (*) (Collection_1_t537220601 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m998550507_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3613992372_gshared (Collection_1_t537220601 * __this, Il2CppArray * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3613992372(__this, ___array, ___index, method) ((  void (*) (Collection_1_t537220601 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3613992372_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2799659183_gshared (Collection_1_t537220601 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2799659183(__this, method) ((  Il2CppObject * (*) (Collection_1_t537220601 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2799659183_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1434592674_gshared (Collection_1_t537220601 * __this, Il2CppObject * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1434592674(__this, ___value, method) ((  int32_t (*) (Collection_1_t537220601 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1434592674_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1001175146_gshared (Collection_1_t537220601 * __this, Il2CppObject * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1001175146(__this, ___value, method) ((  bool (*) (Collection_1_t537220601 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1001175146_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2249374458_gshared (Collection_1_t537220601 * __this, Il2CppObject * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2249374458(__this, ___value, method) ((  int32_t (*) (Collection_1_t537220601 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2249374458_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2092047909_gshared (Collection_1_t537220601 * __this, int32_t ___index, Il2CppObject * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2092047909(__this, ___index, ___value, method) ((  void (*) (Collection_1_t537220601 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2092047909_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1932686051_gshared (Collection_1_t537220601 * __this, Il2CppObject * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1932686051(__this, ___value, method) ((  void (*) (Collection_1_t537220601 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1932686051_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1750847154_gshared (Collection_1_t537220601 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1750847154(__this, method) ((  bool (*) (Collection_1_t537220601 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1750847154_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2152591774_gshared (Collection_1_t537220601 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2152591774(__this, method) ((  Il2CppObject * (*) (Collection_1_t537220601 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2152591774_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1176076121_gshared (Collection_1_t537220601 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1176076121(__this, method) ((  bool (*) (Collection_1_t537220601 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1176076121_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m4094177408_gshared (Collection_1_t537220601 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m4094177408(__this, method) ((  bool (*) (Collection_1_t537220601 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m4094177408_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3827037093_gshared (Collection_1_t537220601 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3827037093(__this, ___index, method) ((  Il2CppObject * (*) (Collection_1_t537220601 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3827037093_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1027298492_gshared (Collection_1_t537220601 * __this, int32_t ___index, Il2CppObject * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1027298492(__this, ___index, ___value, method) ((  void (*) (Collection_1_t537220601 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1027298492_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Add(T)
extern "C"  void Collection_1_Add_m2762110191_gshared (Collection_1_t537220601 * __this, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define Collection_1_Add_m2762110191(__this, ___item, method) ((  void (*) (Collection_1_t537220601 *, KeyValuePair_2_t2863200167 , const MethodInfo*))Collection_1_Add_m2762110191_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Clear()
extern "C"  void Collection_1_Clear_m1732761019_gshared (Collection_1_t537220601 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1732761019(__this, method) ((  void (*) (Collection_1_t537220601 *, const MethodInfo*))Collection_1_Clear_m1732761019_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::ClearItems()
extern "C"  void Collection_1_ClearItems_m467347495_gshared (Collection_1_t537220601 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m467347495(__this, method) ((  void (*) (Collection_1_t537220601 *, const MethodInfo*))Collection_1_ClearItems_m467347495_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Contains(T)
extern "C"  bool Collection_1_Contains_m898941545_gshared (Collection_1_t537220601 * __this, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define Collection_1_Contains_m898941545(__this, ___item, method) ((  bool (*) (Collection_1_t537220601 *, KeyValuePair_2_t2863200167 , const MethodInfo*))Collection_1_Contains_m898941545_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2208603935_gshared (Collection_1_t537220601 * __this, KeyValuePair_2U5BU5D_t140085726* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m2208603935(__this, ___array, ___index, method) ((  void (*) (Collection_1_t537220601 *, KeyValuePair_2U5BU5D_t140085726*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2208603935_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1791296844_gshared (Collection_1_t537220601 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1791296844(__this, method) ((  Il2CppObject* (*) (Collection_1_t537220601 *, const MethodInfo*))Collection_1_GetEnumerator_m1791296844_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1865709795_gshared (Collection_1_t537220601 * __this, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1865709795(__this, ___item, method) ((  int32_t (*) (Collection_1_t537220601 *, KeyValuePair_2_t2863200167 , const MethodInfo*))Collection_1_IndexOf_m1865709795_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2781737302_gshared (Collection_1_t537220601 * __this, int32_t ___index, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define Collection_1_Insert_m2781737302(__this, ___index, ___item, method) ((  void (*) (Collection_1_t537220601 *, int32_t, KeyValuePair_2_t2863200167 , const MethodInfo*))Collection_1_Insert_m2781737302_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2715845769_gshared (Collection_1_t537220601 * __this, int32_t ___index, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m2715845769(__this, ___index, ___item, method) ((  void (*) (Collection_1_t537220601 *, int32_t, KeyValuePair_2_t2863200167 , const MethodInfo*))Collection_1_InsertItem_m2715845769_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Remove(T)
extern "C"  bool Collection_1_Remove_m3736668324_gshared (Collection_1_t537220601 * __this, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define Collection_1_Remove_m3736668324(__this, ___item, method) ((  bool (*) (Collection_1_t537220601 *, KeyValuePair_2_t2863200167 , const MethodInfo*))Collection_1_Remove_m3736668324_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m655590172_gshared (Collection_1_t537220601 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m655590172(__this, ___index, method) ((  void (*) (Collection_1_t537220601 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m655590172_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1561587772_gshared (Collection_1_t537220601 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1561587772(__this, ___index, method) ((  void (*) (Collection_1_t537220601 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1561587772_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1819736312_gshared (Collection_1_t537220601 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1819736312(__this, method) ((  int32_t (*) (Collection_1_t537220601 *, const MethodInfo*))Collection_1_get_Count_m1819736312_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t2863200167  Collection_1_get_Item_m1480720160_gshared (Collection_1_t537220601 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1480720160(__this, ___index, method) ((  KeyValuePair_2_t2863200167  (*) (Collection_1_t537220601 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1480720160_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3589485741_gshared (Collection_1_t537220601 * __this, int32_t ___index, KeyValuePair_2_t2863200167  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m3589485741(__this, ___index, ___value, method) ((  void (*) (Collection_1_t537220601 *, int32_t, KeyValuePair_2_t2863200167 , const MethodInfo*))Collection_1_set_Item_m3589485741_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1651100204_gshared (Collection_1_t537220601 * __this, int32_t ___index, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1651100204(__this, ___index, ___item, method) ((  void (*) (Collection_1_t537220601 *, int32_t, KeyValuePair_2_t2863200167 , const MethodInfo*))Collection_1_SetItem_m1651100204_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m841183395_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m841183395(__this /* static, unused */, ___item, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m841183395_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::ConvertItem(System.Object)
extern "C"  KeyValuePair_2_t2863200167  Collection_1_ConvertItem_m2921799167_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m2921799167(__this /* static, unused */, ___item, method) ((  KeyValuePair_2_t2863200167  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2921799167_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2698564703_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m2698564703(__this /* static, unused */, ___list, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2698564703_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3748987969_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3748987969(__this /* static, unused */, ___list, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3748987969_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m167547390_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m167547390(__this /* static, unused */, ___list, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m167547390_gshared)(__this /* static, unused */, ___list, method)
