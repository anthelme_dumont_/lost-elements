﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIOClient.SocketIOHandshake
struct  SocketIOHandshake_t3315670474  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> SocketIOClient.SocketIOHandshake::Transports
	List_1_t1765447871 * ___Transports_0;
	// System.String SocketIOClient.SocketIOHandshake::<SID>k__BackingField
	String_t* ___U3CSIDU3Ek__BackingField_1;
	// System.Int32 SocketIOClient.SocketIOHandshake::<HeartbeatTimeout>k__BackingField
	int32_t ___U3CHeartbeatTimeoutU3Ek__BackingField_2;
	// System.String SocketIOClient.SocketIOHandshake::<ErrorMessage>k__BackingField
	String_t* ___U3CErrorMessageU3Ek__BackingField_3;
	// System.Int32 SocketIOClient.SocketIOHandshake::<ConnectionTimeout>k__BackingField
	int32_t ___U3CConnectionTimeoutU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_Transports_0() { return static_cast<int32_t>(offsetof(SocketIOHandshake_t3315670474, ___Transports_0)); }
	inline List_1_t1765447871 * get_Transports_0() const { return ___Transports_0; }
	inline List_1_t1765447871 ** get_address_of_Transports_0() { return &___Transports_0; }
	inline void set_Transports_0(List_1_t1765447871 * value)
	{
		___Transports_0 = value;
		Il2CppCodeGenWriteBarrier(&___Transports_0, value);
	}

	inline static int32_t get_offset_of_U3CSIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SocketIOHandshake_t3315670474, ___U3CSIDU3Ek__BackingField_1)); }
	inline String_t* get_U3CSIDU3Ek__BackingField_1() const { return ___U3CSIDU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSIDU3Ek__BackingField_1() { return &___U3CSIDU3Ek__BackingField_1; }
	inline void set_U3CSIDU3Ek__BackingField_1(String_t* value)
	{
		___U3CSIDU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSIDU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CHeartbeatTimeoutU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SocketIOHandshake_t3315670474, ___U3CHeartbeatTimeoutU3Ek__BackingField_2)); }
	inline int32_t get_U3CHeartbeatTimeoutU3Ek__BackingField_2() const { return ___U3CHeartbeatTimeoutU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CHeartbeatTimeoutU3Ek__BackingField_2() { return &___U3CHeartbeatTimeoutU3Ek__BackingField_2; }
	inline void set_U3CHeartbeatTimeoutU3Ek__BackingField_2(int32_t value)
	{
		___U3CHeartbeatTimeoutU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CErrorMessageU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SocketIOHandshake_t3315670474, ___U3CErrorMessageU3Ek__BackingField_3)); }
	inline String_t* get_U3CErrorMessageU3Ek__BackingField_3() const { return ___U3CErrorMessageU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CErrorMessageU3Ek__BackingField_3() { return &___U3CErrorMessageU3Ek__BackingField_3; }
	inline void set_U3CErrorMessageU3Ek__BackingField_3(String_t* value)
	{
		___U3CErrorMessageU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CErrorMessageU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CConnectionTimeoutU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SocketIOHandshake_t3315670474, ___U3CConnectionTimeoutU3Ek__BackingField_4)); }
	inline int32_t get_U3CConnectionTimeoutU3Ek__BackingField_4() const { return ___U3CConnectionTimeoutU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CConnectionTimeoutU3Ek__BackingField_4() { return &___U3CConnectionTimeoutU3Ek__BackingField_4; }
	inline void set_U3CConnectionTimeoutU3Ek__BackingField_4(int32_t value)
	{
		___U3CConnectionTimeoutU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
