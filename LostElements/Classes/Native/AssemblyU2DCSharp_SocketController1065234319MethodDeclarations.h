﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketController
struct SocketController_t1065234319;
// SocketIOClient.Messages.IMessage
struct IMessage_t3628529648;

#include "codegen/il2cpp-codegen.h"

// System.Void SocketController::.ctor()
extern "C"  void SocketController__ctor_m660735980 (SocketController_t1065234319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketController::Start()
extern "C"  void SocketController_Start_m3902841068 (SocketController_t1065234319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketController::<Start>m__0(SocketIOClient.Messages.IMessage)
extern "C"  void SocketController_U3CStartU3Em__0_m1268981551 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketController::<Start>m__1(SocketIOClient.Messages.IMessage)
extern "C"  void SocketController_U3CStartU3Em__1_m2994462448 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketController::<Start>m__2(SocketIOClient.Messages.IMessage)
extern "C"  void SocketController_U3CStartU3Em__2_m424976049 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
