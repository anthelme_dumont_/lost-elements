﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.OrderedSequence`2<System.Int32,System.Int32>
struct OrderedSequence_2_t3552526410;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1424601847;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_t1649583772;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t1252154900;
// System.Linq.SortContext`1<System.Int32>
struct SortContext_1_t2080786391;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_SortDirection2805156518.h"

// System.Void System.Linq.OrderedSequence`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m2605715234_gshared (OrderedSequence_2_t3552526410 * __this, Il2CppObject* ___source, Func_2_t1649583772 * ___key_selector, Il2CppObject* ___comparer, int32_t ___direction, const MethodInfo* method);
#define OrderedSequence_2__ctor_m2605715234(__this, ___source, ___key_selector, ___comparer, ___direction, method) ((  void (*) (OrderedSequence_2_t3552526410 *, Il2CppObject*, Func_2_t1649583772 *, Il2CppObject*, int32_t, const MethodInfo*))OrderedSequence_2__ctor_m2605715234_gshared)(__this, ___source, ___key_selector, ___comparer, ___direction, method)
// System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Int32,System.Int32>::CreateContext(System.Linq.SortContext`1<TElement>)
extern "C"  SortContext_1_t2080786391 * OrderedSequence_2_CreateContext_m3240264396_gshared (OrderedSequence_2_t3552526410 * __this, SortContext_1_t2080786391 * ___current, const MethodInfo* method);
#define OrderedSequence_2_CreateContext_m3240264396(__this, ___current, method) ((  SortContext_1_t2080786391 * (*) (OrderedSequence_2_t3552526410 *, SortContext_1_t2080786391 *, const MethodInfo*))OrderedSequence_2_CreateContext_m3240264396_gshared)(__this, ___current, method)
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedSequence`2<System.Int32,System.Int32>::Sort(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  Il2CppObject* OrderedSequence_2_Sort_m2491854103_gshared (OrderedSequence_2_t3552526410 * __this, Il2CppObject* ___source, const MethodInfo* method);
#define OrderedSequence_2_Sort_m2491854103(__this, ___source, method) ((  Il2CppObject* (*) (OrderedSequence_2_t3552526410 *, Il2CppObject*, const MethodInfo*))OrderedSequence_2_Sort_m2491854103_gshared)(__this, ___source, method)
