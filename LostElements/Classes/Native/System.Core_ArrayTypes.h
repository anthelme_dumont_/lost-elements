﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.TimeZoneInfo/AdjustmentRule
struct AdjustmentRule_t2290566697;

#include "mscorlib_System_Array2840145358.h"
#include "System_Core_System_TimeZoneInfo_AdjustmentRule2290566697.h"
#include "System_Core_System_TimeZoneInfo_TimeType2282261447.h"

#pragma once
// System.TimeZoneInfo/AdjustmentRule[]
struct AdjustmentRuleU5BU5D_t1713960724  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) AdjustmentRule_t2290566697 * m_Items[1];

public:
	inline AdjustmentRule_t2290566697 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AdjustmentRule_t2290566697 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AdjustmentRule_t2290566697 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.TimeZoneInfo/TimeType[]
struct TimeTypeU5BU5D_t202998078  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) TimeType_t2282261447  m_Items[1];

public:
	inline TimeType_t2282261447  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TimeType_t2282261447 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TimeType_t2282261447  value)
	{
		m_Items[index] = value;
	}
};
