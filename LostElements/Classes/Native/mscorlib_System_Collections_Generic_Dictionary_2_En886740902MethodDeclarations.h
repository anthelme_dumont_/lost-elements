﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m814652871(__this, ___dictionary, method) ((  void (*) (Enumerator_t886740902 *, Dictionary_2_t1119712961 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3500516922(__this, method) ((  Il2CppObject * (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3560178830(__this, method) ((  void (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3379524119(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m352964502(__this, method) ((  Il2CppObject * (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m944906408(__this, method) ((  Il2CppObject * (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::MoveNext()
#define Enumerator_MoveNext_m2234745722(__this, method) ((  bool (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::get_Current()
#define Enumerator_get_Current_m727752566(__this, method) ((  KeyValuePair_2_t608244259  (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1783554759(__this, method) ((  String_t* (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3261157547(__this, method) ((  Action_1_t3776982353 * (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::Reset()
#define Enumerator_Reset_m2088205721(__this, method) ((  void (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::VerifyState()
#define Enumerator_VerifyState_m1104312034(__this, method) ((  void (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3131814346(__this, method) ((  void (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::Dispose()
#define Enumerator_Dispose_m3539399721(__this, method) ((  void (*) (Enumerator_t886740902 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
