﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3883920346MethodDeclarations.h"

// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2862021666(__this, ___object, ___method, method) ((  void (*) (Predicate_1_t2665682002 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m252712190_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::Invoke(T)
#define Predicate_1_Invoke_m1644666912(__this, ___obj, method) ((  bool (*) (Predicate_1_t2665682002 *, KeyValuePair_2_t2094718104 , const MethodInfo*))Predicate_1_Invoke_m684566468_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m395475631(__this, ___obj, ___callback, ___object, method) ((  Il2CppObject * (*) (Predicate_1_t2665682002 *, KeyValuePair_2_t2094718104 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2552303187_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m2626156468(__this, ___result, method) ((  bool (*) (Predicate_1_t2665682002 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3276267152_gshared)(__this, ___result, method)
