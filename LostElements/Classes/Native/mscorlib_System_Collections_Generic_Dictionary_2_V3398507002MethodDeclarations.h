﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3250054297MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3567033085(__this, ___dictionary, method) ((  void (*) (ValueCollection_t3398507002 *, Dictionary_2_t1476369908 *, const MethodInfo*))ValueCollection__ctor_m30082295_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4279487893(__this, ___item, method) ((  void (*) (ValueCollection_t3398507002 *, Action_1_t985559125 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m701709403_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m726781150(__this, method) ((  void (*) (ValueCollection_t3398507002 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3824389796_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2655893425(__this, ___item, method) ((  bool (*) (ValueCollection_t3398507002 *, Action_1_t985559125 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m91415663_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1138952342(__this, ___item, method) ((  bool (*) (ValueCollection_t3398507002 *, Action_1_t985559125 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4014492884_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1448992236(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3398507002 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4048472420_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2735501410(__this, ___array, ___index, method) ((  void (*) (ValueCollection_t3398507002 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1511207592_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m150698717(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3398507002 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3055859895_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m931069284(__this, method) ((  bool (*) (ValueCollection_t3398507002 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2661558818_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3416996164(__this, method) ((  bool (*) (ValueCollection_t3398507002 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3650032386_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2512856496(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3398507002 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m179750644_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m52873220(__this, ___array, ___index, method) ((  void (*) (ValueCollection_t3398507002 *, Action_1U5BU5D_t3271680696*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1295975294_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1814233575(__this, method) ((  Enumerator_t1243397851  (*) (ValueCollection_t3398507002 *, const MethodInfo*))ValueCollection_GetEnumerator_m848222311_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Action`1<System.Object>>::get_Count()
#define ValueCollection_get_Count_m3739481482(__this, method) ((  int32_t (*) (ValueCollection_t3398507002 *, const MethodInfo*))ValueCollection_get_Count_m2227591228_gshared)(__this, method)
