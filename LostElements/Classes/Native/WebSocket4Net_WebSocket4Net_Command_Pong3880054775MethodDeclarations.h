﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Command.Pong
struct Pong_t3880054775;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketCommandInfo3536916738.h"

// System.Void WebSocket4Net.Command.Pong::ExecuteCommand(WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo)
extern "C"  void Pong_ExecuteCommand_m3870274435 (Pong_t3880054775 * __this, WebSocket_t713846903 * ___session, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.Command.Pong::get_Name()
extern "C"  String_t* Pong_get_Name_m83446231 (Pong_t3880054775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Command.Pong::.ctor()
extern "C"  void Pong__ctor_m3174063268 (Pong_t3880054775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
