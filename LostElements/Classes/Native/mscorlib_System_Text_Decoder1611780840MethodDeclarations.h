﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.Decoder
struct Decoder_t1611780840;
// System.Text.DecoderFallback
struct DecoderFallback_t4033313258;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t1215858122;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Char[]
struct CharU5BU5D_t3416858730;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_DecoderFallback4033313258.h"

// System.Void System.Text.Decoder::.ctor()
extern "C"  void Decoder__ctor_m1448672242 (Decoder_t1611780840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::set_Fallback(System.Text.DecoderFallback)
extern "C"  void Decoder_set_Fallback_m4287157405 (Decoder_t1611780840 * __this, DecoderFallback_t4033313258 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.Decoder::get_FallbackBuffer()
extern "C"  DecoderFallbackBuffer_t1215858122 * Decoder_get_FallbackBuffer_m1620793422 (Decoder_t1611780840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Decoder::GetCharCount(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern "C"  int32_t Decoder_GetCharCount_m781020173 (Decoder_t1611780840 * __this, ByteU5BU5D_t58506160* ___bytes, int32_t ___index, int32_t ___count, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Decoder::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32,System.Boolean)
extern "C"  int32_t Decoder_GetChars_m2988413735 (Decoder_t1611780840 * __this, ByteU5BU5D_t58506160* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, CharU5BU5D_t3416858730* ___chars, int32_t ___charIndex, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::Reset()
extern "C"  void Decoder_Reset_m3390072479 (Decoder_t1611780840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::Convert(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32,System.Int32,System.Boolean,System.Int32&,System.Int32&,System.Boolean&)
extern "C"  void Decoder_Convert_m1578632859 (Decoder_t1611780840 * __this, ByteU5BU5D_t58506160* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, CharU5BU5D_t3416858730* ___chars, int32_t ___charIndex, int32_t ___charCount, bool ___flush, int32_t* ___bytesUsed, int32_t* ___charsUsed, bool* ___completed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::CheckArguments(System.Char[],System.Int32)
extern "C"  void Decoder_CheckArguments_m3674559868 (Decoder_t1611780840 * __this, CharU5BU5D_t3416858730* ___chars, int32_t ___charIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::CheckArguments(System.Byte[],System.Int32,System.Int32)
extern "C"  void Decoder_CheckArguments_m24918121 (Decoder_t1611780840 * __this, ByteU5BU5D_t58506160* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
