﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>
struct ArraySegmentEx_1_t1729348146;
// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>
struct ArraySegmentEx_1_t3670935547;
// SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>
struct ICommand_2_t625221883;

#include "mscorlib_System_Array2840145358.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Clie1729348146.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Clie3670935547.h"

#pragma once
// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>[]
struct ArraySegmentEx_1U5BU5D_t3416179911  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ArraySegmentEx_1_t1729348146 * m_Items[1];

public:
	inline ArraySegmentEx_1_t1729348146 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ArraySegmentEx_1_t1729348146 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ArraySegmentEx_1_t1729348146 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>[]
struct ArraySegmentEx_1U5BU5D_t3463162298  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ArraySegmentEx_1_t3670935547 * m_Items[1];

public:
	inline ArraySegmentEx_1_t3670935547 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ArraySegmentEx_1_t3670935547 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ArraySegmentEx_1_t3670935547 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>[]
struct ICommand_2U5BU5D_t2806320314  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
