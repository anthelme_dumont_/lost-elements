﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.MessageEventArgs
struct MessageEventArgs_t1401678908;
// SocketIOClient.Messages.IMessage
struct IMessage_t3628529648;

#include "codegen/il2cpp-codegen.h"

// System.Void SocketIOClient.MessageEventArgs::.ctor(SocketIOClient.Messages.IMessage)
extern "C"  void MessageEventArgs__ctor_m4268379643 (MessageEventArgs_t1401678908 * __this, Il2CppObject * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.IMessage SocketIOClient.MessageEventArgs::get_Message()
extern "C"  Il2CppObject * MessageEventArgs_get_Message_m3408976564 (MessageEventArgs_t1401678908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.MessageEventArgs::set_Message(SocketIOClient.Messages.IMessage)
extern "C"  void MessageEventArgs_set_Message_m3529502531 (MessageEventArgs_t1401678908 * __this, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
