﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>
struct Dictionary_2_t2262919787;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>
struct  ValueCollection_t4185056881  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::dictionary
	Dictionary_2_t2262919787 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(ValueCollection_t4185056881, ___dictionary_0)); }
	inline Dictionary_2_t2262919787 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2262919787 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2262919787 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier(&___dictionary_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
