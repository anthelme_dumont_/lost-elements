﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.FramePartReader.MaskKeyReader
struct MaskKeyReader_t360839071;
// WebSocket4Net.Protocol.WebSocketDataFrame
struct WebSocketDataFrame_t222733806;
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader
struct IDataFramePartReader_t2102555929;

#include "codegen/il2cpp-codegen.h"
#include "WebSocket4Net_WebSocket4Net_Protocol_WebSocketDataF222733806.h"

// System.Int32 WebSocket4Net.Protocol.FramePartReader.MaskKeyReader::Process(System.Int32,WebSocket4Net.Protocol.WebSocketDataFrame,WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader&)
extern "C"  int32_t MaskKeyReader_Process_m3881637349 (MaskKeyReader_t360839071 * __this, int32_t ___lastLength, WebSocketDataFrame_t222733806 * ___frame, Il2CppObject ** ___nextPartReader, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.FramePartReader.MaskKeyReader::.ctor()
extern "C"  void MaskKeyReader__ctor_m2819127594 (MaskKeyReader_t360839071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
