﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonController
struct ButtonController_t2447168654;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonController::.ctor()
extern "C"  void ButtonController__ctor_m497520450 (ButtonController_t2447168654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonController::newGame()
extern "C"  void ButtonController_newGame_m2357226994 (ButtonController_t2447168654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonController::loadGame()
extern "C"  void ButtonController_loadGame_m3378449690 (ButtonController_t2447168654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonController::settings()
extern "C"  void ButtonController_settings_m3951577221 (ButtonController_t2447168654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonController::learnMore()
extern "C"  void ButtonController_learnMore_m1930016377 (ButtonController_t2447168654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonController::credits()
extern "C"  void ButtonController_credits_m3988745946 (ButtonController_t2447168654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonController::Main()
extern "C"  void ButtonController_Main_m2563942715 (ButtonController_t2447168654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
