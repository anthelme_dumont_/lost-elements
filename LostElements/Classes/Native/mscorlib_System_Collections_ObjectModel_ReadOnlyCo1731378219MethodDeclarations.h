﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct ReadOnlyCollection_1_t1731378219;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct IList_1_t734725185;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>[]
struct KeyValuePair_2U5BU5D_t140085726;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct IEnumerator_1_t51339319;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22863200167.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3855444163_gshared (ReadOnlyCollection_1_t1731378219 * __this, Il2CppObject* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3855444163(__this, ___list, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3855444163_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4051271981_gshared (ReadOnlyCollection_1_t1731378219 * __this, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4051271981(__this, ___item, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, KeyValuePair_2_t2863200167 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4051271981_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3042070845_gshared (ReadOnlyCollection_1_t1731378219 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3042070845(__this, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3042070845_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m972828628_gshared (ReadOnlyCollection_1_t1731378219 * __this, int32_t ___index, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m972828628(__this, ___index, ___item, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, int32_t, KeyValuePair_2_t2863200167 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m972828628_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m313770022_gshared (ReadOnlyCollection_1_t1731378219 * __this, KeyValuePair_2_t2863200167  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m313770022(__this, ___item, method) ((  bool (*) (ReadOnlyCollection_1_t1731378219 *, KeyValuePair_2_t2863200167 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m313770022_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3141648794_gshared (ReadOnlyCollection_1_t1731378219 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3141648794(__this, ___index, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3141648794_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  KeyValuePair_2_t2863200167  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1756044062_gshared (ReadOnlyCollection_1_t1731378219 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1756044062(__this, ___index, method) ((  KeyValuePair_2_t2863200167  (*) (ReadOnlyCollection_1_t1731378219 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1756044062_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m395037611_gshared (ReadOnlyCollection_1_t1731378219 * __this, int32_t ___index, KeyValuePair_2_t2863200167  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m395037611(__this, ___index, ___value, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, int32_t, KeyValuePair_2_t2863200167 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m395037611_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3354961577_gshared (ReadOnlyCollection_1_t1731378219 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3354961577(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1731378219 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3354961577_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3587062770_gshared (ReadOnlyCollection_1_t1731378219 * __this, Il2CppArray * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3587062770(__this, ___array, ___index, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3587062770_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m597179501_gshared (ReadOnlyCollection_1_t1731378219 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m597179501(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1731378219 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m597179501_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3233956004_gshared (ReadOnlyCollection_1_t1731378219 * __this, Il2CppObject * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3233956004(__this, ___value, method) ((  int32_t (*) (ReadOnlyCollection_1_t1731378219 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3233956004_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3675755264_gshared (ReadOnlyCollection_1_t1731378219 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3675755264(__this, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3675755264_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m81356968_gshared (ReadOnlyCollection_1_t1731378219 * __this, Il2CppObject * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m81356968(__this, ___value, method) ((  bool (*) (ReadOnlyCollection_1_t1731378219 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m81356968_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3454633212_gshared (ReadOnlyCollection_1_t1731378219 * __this, Il2CppObject * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3454633212(__this, ___value, method) ((  int32_t (*) (ReadOnlyCollection_1_t1731378219 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3454633212_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1902876967_gshared (ReadOnlyCollection_1_t1731378219 * __this, int32_t ___index, Il2CppObject * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1902876967(__this, ___index, ___value, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1902876967_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1919106209_gshared (ReadOnlyCollection_1_t1731378219 * __this, Il2CppObject * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1919106209(__this, ___value, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1919106209_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m948106039_gshared (ReadOnlyCollection_1_t1731378219 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m948106039(__this, ___index, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m948106039_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1338854836_gshared (ReadOnlyCollection_1_t1731378219 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1338854836(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1731378219 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1338854836_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4249443616_gshared (ReadOnlyCollection_1_t1731378219 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4249443616(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1731378219 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4249443616_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3651581463_gshared (ReadOnlyCollection_1_t1731378219 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3651581463(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1731378219 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3651581463_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3065653762_gshared (ReadOnlyCollection_1_t1731378219 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3065653762(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1731378219 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3065653762_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m267028135_gshared (ReadOnlyCollection_1_t1731378219 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m267028135(__this, ___index, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1731378219 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m267028135_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3917616958_gshared (ReadOnlyCollection_1_t1731378219 * __this, int32_t ___index, Il2CppObject * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3917616958(__this, ___index, ___value, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3917616958_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1122167659_gshared (ReadOnlyCollection_1_t1731378219 * __this, KeyValuePair_2_t2863200167  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1122167659(__this, ___value, method) ((  bool (*) (ReadOnlyCollection_1_t1731378219 *, KeyValuePair_2_t2863200167 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1122167659_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1146833757_gshared (ReadOnlyCollection_1_t1731378219 * __this, KeyValuePair_2U5BU5D_t140085726* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1146833757(__this, ___array, ___index, method) ((  void (*) (ReadOnlyCollection_1_t1731378219 *, KeyValuePair_2U5BU5D_t140085726*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1146833757_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3933689678_gshared (ReadOnlyCollection_1_t1731378219 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3933689678(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1731378219 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3933689678_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m412511649_gshared (ReadOnlyCollection_1_t1731378219 * __this, KeyValuePair_2_t2863200167  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m412511649(__this, ___value, method) ((  int32_t (*) (ReadOnlyCollection_1_t1731378219 *, KeyValuePair_2_t2863200167 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m412511649_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m4015234042_gshared (ReadOnlyCollection_1_t1731378219 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m4015234042(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1731378219 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m4015234042_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t2863200167  ReadOnlyCollection_1_get_Item_m3819697118_gshared (ReadOnlyCollection_1_t1731378219 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3819697118(__this, ___index, method) ((  KeyValuePair_2_t2863200167  (*) (ReadOnlyCollection_1_t1731378219 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3819697118_gshared)(__this, ___index, method)
