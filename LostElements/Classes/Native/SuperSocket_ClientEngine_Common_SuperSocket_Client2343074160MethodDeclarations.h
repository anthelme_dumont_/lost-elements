﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState
struct DnsConnectState_t2343074160;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t3494006030;
// System.Net.Sockets.Socket
struct Socket_t150013987;
// System.Object
struct Il2CppObject;
// SuperSocket.ClientEngine.ConnectedCallback
struct ConnectedCallback_t1584521517;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Sockets_Socket150013987.h"
#include "mscorlib_System_Object837106420.h"
#include "SuperSocket_ClientEngine_Common_SuperSocket_Client1584521517.h"

// System.Net.IPAddress[] SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_Addresses()
extern "C"  IPAddressU5BU5D_t3494006030* DnsConnectState_get_Addresses_m1195483209 (DnsConnectState_t2343074160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_Addresses(System.Net.IPAddress[])
extern "C"  void DnsConnectState_set_Addresses_m1679752656 (DnsConnectState_t2343074160 * __this, IPAddressU5BU5D_t3494006030* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_NextAddressIndex()
extern "C"  int32_t DnsConnectState_get_NextAddressIndex_m3972185532 (DnsConnectState_t2343074160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_NextAddressIndex(System.Int32)
extern "C"  void DnsConnectState_set_NextAddressIndex_m2086294711 (DnsConnectState_t2343074160 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_Port()
extern "C"  int32_t DnsConnectState_get_Port_m30529900 (DnsConnectState_t2343074160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_Port(System.Int32)
extern "C"  void DnsConnectState_set_Port_m3846838119 (DnsConnectState_t2343074160 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.Socket SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_Socket4()
extern "C"  Socket_t150013987 * DnsConnectState_get_Socket4_m3757121632 (DnsConnectState_t2343074160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_Socket4(System.Net.Sockets.Socket)
extern "C"  void DnsConnectState_set_Socket4_m1444128755 (DnsConnectState_t2343074160 * __this, Socket_t150013987 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.Socket SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_Socket6()
extern "C"  Socket_t150013987 * DnsConnectState_get_Socket6_m3757123554 (DnsConnectState_t2343074160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_Socket6(System.Net.Sockets.Socket)
extern "C"  void DnsConnectState_set_Socket6_m934655665 (DnsConnectState_t2343074160 * __this, Socket_t150013987 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_State()
extern "C"  Il2CppObject * DnsConnectState_get_State_m113258909 (DnsConnectState_t2343074160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_State(System.Object)
extern "C"  void DnsConnectState_set_State_m885005398 (DnsConnectState_t2343074160 * __this, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SuperSocket.ClientEngine.ConnectedCallback SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::get_Callback()
extern "C"  ConnectedCallback_t1584521517 * DnsConnectState_get_Callback_m816038482 (DnsConnectState_t2343074160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::set_Callback(SuperSocket.ClientEngine.ConnectedCallback)
extern "C"  void DnsConnectState_set_Callback_m2832258125 (DnsConnectState_t2343074160 * __this, ConnectedCallback_t1584521517 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectAsyncExtension/DnsConnectState::.ctor()
extern "C"  void DnsConnectState__ctor_m837116908 (DnsConnectState_t2343074160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
