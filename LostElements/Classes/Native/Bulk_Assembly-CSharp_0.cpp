﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// SocketController
struct SocketController_t1065234319;
// SocketIOClient.Messages.IMessage
struct IMessage_t3628529648;
// SocketIOClient.Client
struct Client_t931100087;
// System.String
struct String_t;
// System.EventHandler
struct EventHandler_t247020293;
// System.EventHandler`1<SocketIOClient.MessageEventArgs>
struct EventHandler_1_t2244256551;
// System.EventHandler`1<SocketIOClient.ErrorEventArgs>
struct EventHandler_1_t1185090118;
// SocketIOClient.SocketIOHandshake
struct SocketIOHandshake_t3315670474;
// SocketIOClient.IEndPointClient
struct IEndPointClient_t335482901;
// System.Action`1<SocketIOClient.Messages.IMessage>
struct Action_1_t3776982353;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.EventArgs
struct EventArgs_t516466188;
// WebSocket4Net.MessageReceivedEventArgs
struct MessageReceivedEventArgs_t3871897919;
// SuperSocket.ClientEngine.ErrorEventArgs
struct ErrorEventArgs_t1329266798;
// SocketIOClient.ErrorEventArgs
struct ErrorEventArgs_t342512475;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.Uri
struct Uri_t2776692961;
// SocketIOClient.EndPointClient
struct EndPointClient_t1953350540;
// SocketIOClient.IClient
struct IClient_t1294359360;
// System.Collections.Generic.IEnumerable`1<System.Char>
struct IEnumerable_1_t1355893759;
// System.Exception
struct Exception_t1967233988;
// SocketIOClient.Eventing.RegistrationManager
struct RegistrationManager_t998967514;
// SocketIOClient.Messages.JsonEncodedEventMessage
struct JsonEncodedEventMessage_t989636229;
// SocketIOClient.MessageEventArgs
struct MessageEventArgs_t1401678908;
// SocketIOClient.Messages.AckMessage
struct AckMessage_t21730256;
// SocketIOClient.Messages.ConnectMessage
struct ConnectMessage_t3539960911;
// SocketIOClient.Messages.DisconnectMessage
struct DisconnectMessage_t1581018141;
// SocketIOClient.Messages.ErrorMessage
struct ErrorMessage_t4000372081;
// SocketIOClient.Messages.EventMessage
struct EventMessage_t365350047;
// SocketIOClient.Messages.Heartbeat
struct Heartbeat_t542713166;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// SocketIOClient.Messages.JSONMessage
struct JSONMessage_t3736498801;
// SocketIOClient.Messages.Message
struct Message_t957426777;
// SocketIOClient.Messages.NoopMessage
struct NoopMessage_t1053258391;
// SocketIOClient.Messages.TextMessage
struct TextMessage_t120705644;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3840643258;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// WaterController
struct WaterController_t632785907;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_ElementsController3637326867.h"
#include "AssemblyU2DCSharp_ElementsController3637326867MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Single958209021.h"
#include "AssemblyU2DCSharp_ExtensionMethods1227265683.h"
#include "AssemblyU2DCSharp_ExtensionMethods1227265683MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketController1065234319.h"
#include "AssemblyU2DCSharp_SocketController1065234319MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Client931100087MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3776982353MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Client931100087.h"
#include "mscorlib_System_Action_1_gen3776982353.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "mscorlib_System_Single958209021MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketVersion3981039553.h"
#include "mscorlib_System_Threading_ManualResetEvent2399218676MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "System_System_Uri2776692961MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Eventing_Registrat998967514MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Core_System_Collections_Co702095371MethodDeclarations.h"
#include "mscorlib_System_Threading_ThreadStart2758142267MethodDeclarations.h"
#include "mscorlib_System_Threading_Thread1674723085MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Threading_ManualResetEvent2399218676.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "System_System_Uri2776692961.h"
#include "AssemblyU2DCSharp_SocketIOClient_Eventing_Registrat998967514.h"
#include "SuperSocket_ClientEngine_Core_System_Collections_Co702095371.h"
#include "mscorlib_System_Threading_ThreadStart2758142267.h"
#include "mscorlib_System_Threading_Thread1674723085.h"
#include "mscorlib_System_EventHandler247020293.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "mscorlib_System_EventHandler_1_gen2244256551.h"
#include "mscorlib_System_EventHandler_1_gen1185090118.h"
#include "AssemblyU2DCSharp_SocketIOClient_SocketIOHandshake3315670474.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketState1654176186.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903MethodDeclarations.h"
#include "WebSocket4Net_WebSocket4Net_WebSocket713846903.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "mscorlib_System_Threading_EventWaitHandle1233883898MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_SocketIOHandshake3315670474MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_ErrorEventArgs342512475MethodDeclarations.h"
#include "mscorlib_System_EventHandler247020293MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen419508266MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen2171844441MethodDeclarations.h"
#include "System_System_Diagnostics_Trace3229917284MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharp_SocketIOClient_ErrorEventArgs342512475.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_EventArgs516466188.h"
#include "WebSocket4Net_WebSocket4Net_MessageReceivedEventAr3871897919.h"
#include "mscorlib_System_EventHandler_1_gen419508266.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1329266798.h"
#include "mscorlib_System_EventHandler_1_gen2171844441.h"
#include "AssemblyU2DCSharp_SocketIOClient_EndPointClient1953350540MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_ConnectM3539960911MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_EndPointClient1953350540.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_ConnectM3539960911.h"
#include "mscorlib_System_EventArgs516466188MethodDeclarations.h"
#include "mscorlib_System_Threading_WaitHandle361062656MethodDeclarations.h"
#include "mscorlib_System_Threading_WaitHandle361062656.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_TextMessa120705644MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_Message957426777MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_JSONMess3736498801MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_EventMess365350047MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_TextMessa120705644.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_JSONMess3736498801.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_EventMess365350047.h"
#include "AssemblyU2DCSharp_SocketIOClient_MessageEventArgs1401678908MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen2244256551MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_MessageEventArgs1401678908.h"
#include "mscorlib_System_Threading_Timer3546110984MethodDeclarations.h"
#include "mscorlib_System_Threading_Timer3546110984.h"
#include "mscorlib_System_Threading_TimerCallback4291881837MethodDeclarations.h"
#include "mscorlib_System_Threading_TimerCallback4291881837.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_Message957426777.h"
#include "WebSocket4Net_WebSocket4Net_MessageReceivedEventAr3871897919MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_SocketIOMessageTyp905678103.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_JsonEncod989636229.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1329266798MethodDeclarations.h"
#include "mscorlib_System_EventHandler_1_gen1185090118MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_Heartbeat542713166MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1363551830MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_Heartbeat542713166.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncRes477528403.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncRes477528403MethodDeclarations.h"
#include "System_System_Net_WebClient4210428809MethodDeclarations.h"
#include "System_System_Net_WebClient4210428809.h"
#include "mscorlib_System_GC2776609905MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "mscorlib_System_Char2778706699.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1476369908MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1119712961MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1476369908.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1119712961.h"
#include "mscorlib_System_Nullable_1_gen1438485399MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_AckMessage21730256.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_AckMessage21730256MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Regex3802381858MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Regex3802381858.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Group3792618586MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_JsonEncod989636229MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_GroupCollect2158306392MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Capture1645813025MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Match3797657504.h"
#include "System_System_Text_RegularExpressions_Match3797657504MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_GroupCollect2158306392.h"
#include "System_System_Text_RegularExpressions_Group3792618586.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_Disconne1581018141.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_Disconne1581018141MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_ErrorMes4000372081.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_ErrorMes4000372081MethodDeclarations.h"
#include "SimpleJson_SimpleJson_SimpleJson2437938008MethodDeclarations.h"
#include "SimpleJson_SimpleJson_SimpleJson2437938008.h"
#include "System_System_Text_RegularExpressions_RegexOptions2783621746.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_NoopMess1053258391MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_NoopMess1053258391.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "AssemblyU2DCSharp_SocketIOClient_SocketIOMessageTyp905678103MethodDeclarations.h"
#include "AssemblyU2DCSharp_WaterController632785907.h"
#include "AssemblyU2DCSharp_WaterController632785907MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"

// System.Boolean System.Linq.Enumerable::Contains<System.Char>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
extern "C"  bool Enumerable_Contains_TisChar_t2778706699_m1628586787_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, uint16_t p1, const MethodInfo* method);
#define Enumerable_Contains_TisChar_t2778706699_m1628586787(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, uint16_t, const MethodInfo*))Enumerable_Contains_TisChar_t2778706699_m1628586787_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 SimpleJson.SimpleJson::DeserializeObject<System.Object>(System.String)
extern "C"  Il2CppObject * SimpleJson_DeserializeObject_TisIl2CppObject_m2707068672_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define SimpleJson_DeserializeObject_TisIl2CppObject_m2707068672(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))SimpleJson_DeserializeObject_TisIl2CppObject_m2707068672_gshared)(__this /* static, unused */, p0, method)
// !!0 SimpleJson.SimpleJson::DeserializeObject<SocketIOClient.Messages.JsonEncodedEventMessage>(System.String)
#define SimpleJson_DeserializeObject_TisJsonEncodedEventMessage_t989636229_m3693202557(__this /* static, unused */, p0, method) ((  JsonEncodedEventMessage_t989636229 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))SimpleJson_DeserializeObject_TisIl2CppObject_m2707068672_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::First<System.Char>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  uint16_t Enumerable_First_TisChar_t2778706699_m2365985763_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_First_TisChar_t2778706699_m2365985763(__this /* static, unused */, p0, method) ((  uint16_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisChar_t2778706699_m2365985763_gshared)(__this /* static, unused */, p0, method)
// System.Int32 System.Linq.Enumerable::Count<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  int32_t Enumerable_Count_TisIl2CppObject_m1136236352_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Count_TisIl2CppObject_m1136236352(__this /* static, unused */, p0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Count_TisIl2CppObject_m1136236352_gshared)(__this /* static, unused */, p0, method)
// System.Int32 System.Linq.Enumerable::Count<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Count_TisString_t_m2139531282(__this /* static, unused */, p0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Count_TisIl2CppObject_m1136236352_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ElementsController::.cctor()
extern TypeInfo* ElementsController_t3637326867_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral83350775;
extern const uint32_t ElementsController__cctor_m2377340613_MetadataUsageId;
extern "C"  void ElementsController__cctor_m2377340613 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ElementsController__cctor_m2377340613_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((ElementsController_t3637326867_StaticFields*)ElementsController_t3637326867_il2cpp_TypeInfo_var->static_fields)->set_activeElement_0(_stringLiteral83350775);
		((ElementsController_t3637326867_StaticFields*)ElementsController_t3637326867_il2cpp_TypeInfo_var->static_fields)->set_coefficientRealIntensity_2((0.0066f));
		return;
	}
}
// System.Void ElementsController::switchToEarth()
extern TypeInfo* ElementsController_t3637326867_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral66725930;
extern const uint32_t ElementsController_switchToEarth_m2292208673_MetadataUsageId;
extern "C"  void ElementsController_switchToEarth_m2292208673 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ElementsController_switchToEarth_m2292208673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ElementsController_t3637326867_il2cpp_TypeInfo_var);
		((ElementsController_t3637326867_StaticFields*)ElementsController_t3637326867_il2cpp_TypeInfo_var->static_fields)->set_activeElement_0(_stringLiteral66725930);
		return;
	}
}
// System.Void ElementsController::switchToWind()
extern TypeInfo* ElementsController_t3637326867_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2696232;
extern const uint32_t ElementsController_switchToWind_m2813275891_MetadataUsageId;
extern "C"  void ElementsController_switchToWind_m2813275891 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ElementsController_switchToWind_m2813275891_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ElementsController_t3637326867_il2cpp_TypeInfo_var);
		((ElementsController_t3637326867_StaticFields*)ElementsController_t3637326867_il2cpp_TypeInfo_var->static_fields)->set_activeElement_0(_stringLiteral2696232);
		return;
	}
}
// System.Void ElementsController::switchToWater()
extern TypeInfo* ElementsController_t3637326867_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral83350775;
extern const uint32_t ElementsController_switchToWater_m1088815534_MetadataUsageId;
extern "C"  void ElementsController_switchToWater_m1088815534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ElementsController_switchToWater_m1088815534_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ElementsController_t3637326867_il2cpp_TypeInfo_var);
		((ElementsController_t3637326867_StaticFields*)ElementsController_t3637326867_il2cpp_TypeInfo_var->static_fields)->set_activeElement_0(_stringLiteral83350775);
		return;
	}
}
// System.Void ElementsController::switchToSun()
extern TypeInfo* ElementsController_t3637326867_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral83500;
extern const uint32_t ElementsController_switchToSun_m225959715_MetadataUsageId;
extern "C"  void ElementsController_switchToSun_m225959715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ElementsController_switchToSun_m225959715_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ElementsController_t3637326867_il2cpp_TypeInfo_var);
		((ElementsController_t3637326867_StaticFields*)ElementsController_t3637326867_il2cpp_TypeInfo_var->static_fields)->set_activeElement_0(_stringLiteral83500);
		return;
	}
}
// System.Void ElementsController::changeIntesity(System.Single)
extern TypeInfo* ElementsController_t3637326867_il2cpp_TypeInfo_var;
extern const uint32_t ElementsController_changeIntesity_m1797890574_MetadataUsageId;
extern "C"  void ElementsController_changeIntesity_m1797890574 (Il2CppObject * __this /* static, unused */, float ___intensity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ElementsController_changeIntesity_m1797890574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___intensity;
		IL2CPP_RUNTIME_CLASS_INIT(ElementsController_t3637326867_il2cpp_TypeInfo_var);
		float L_1 = ((ElementsController_t3637326867_StaticFields*)ElementsController_t3637326867_il2cpp_TypeInfo_var->static_fields)->get_coefficientRealIntensity_2();
		((ElementsController_t3637326867_StaticFields*)ElementsController_t3637326867_il2cpp_TypeInfo_var->static_fields)->set_intensityOfElement_1(((float)((float)L_0*(float)L_1)));
		return;
	}
}
// System.Single ExtensionMethods::Map(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  float ExtensionMethods_Map_m3708687785 (Il2CppObject * __this /* static, unused */, float ___value, float ___fromSource, float ___toSource, float ___fromTarget, float ___toTarget, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		float L_1 = ___fromSource;
		float L_2 = ___toSource;
		float L_3 = ___fromSource;
		float L_4 = ___toTarget;
		float L_5 = ___fromTarget;
		float L_6 = ___fromTarget;
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0-(float)L_1))/(float)((float)((float)L_2-(float)L_3))))*(float)((float)((float)L_4-(float)L_5))))+(float)L_6));
	}
}
// System.Single ExtensionMethods::Round(System.Single)
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t ExtensionMethods_Round_m1332053347_MetadataUsageId;
extern "C"  float ExtensionMethods_Round_m1332053347 (Il2CppObject * __this /* static, unused */, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExtensionMethods_Round_m1332053347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_1 = bankers_roundf(((float)((float)L_0*(float)(100.0f))));
		return ((float)((float)L_1/(float)(100.0f)));
	}
}
// System.Void SocketController::.ctor()
extern "C"  void SocketController__ctor_m660735980 (SocketController_t1065234319 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketController::Start()
extern TypeInfo* Client_t931100087_il2cpp_TypeInfo_var;
extern TypeInfo* SocketController_t1065234319_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t3776982353_il2cpp_TypeInfo_var;
extern const MethodInfo* SocketController_U3CStartU3Em__0_m1268981551_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m683306377_MethodInfo_var;
extern const MethodInfo* SocketController_U3CStartU3Em__1_m2994462448_MethodInfo_var;
extern const MethodInfo* SocketController_U3CStartU3Em__2_m424976049_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral120694675;
extern Il2CppCodeGenString* _stringLiteral951351530;
extern Il2CppCodeGenString* _stringLiteral954925063;
extern Il2CppCodeGenString* _stringLiteral3117011;
extern const uint32_t SocketController_Start_m3902841068_MetadataUsageId;
extern "C"  void SocketController_Start_m3902841068 (SocketController_t1065234319 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SocketController_Start_m3902841068_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	Client_t931100087 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	Client_t931100087 * G_B1_1 = NULL;
	String_t* G_B4_0 = NULL;
	Client_t931100087 * G_B4_1 = NULL;
	String_t* G_B3_0 = NULL;
	Client_t931100087 * G_B3_1 = NULL;
	String_t* G_B6_0 = NULL;
	Client_t931100087 * G_B6_1 = NULL;
	String_t* G_B5_0 = NULL;
	Client_t931100087 * G_B5_1 = NULL;
	{
		V_0 = _stringLiteral120694675;
		String_t* L_0 = V_0;
		Client_t931100087 * L_1 = (Client_t931100087 *)il2cpp_codegen_object_new(Client_t931100087_il2cpp_TypeInfo_var);
		Client__ctor_m3888737708(L_1, L_0, /*hidden argument*/NULL);
		__this->set_client_2(L_1);
		Client_t931100087 * L_2 = __this->get_client_2();
		Action_1_t3776982353 * L_3 = ((SocketController_t1065234319_StaticFields*)SocketController_t1065234319_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		G_B1_0 = _stringLiteral951351530;
		G_B1_1 = L_2;
		if (L_3)
		{
			G_B2_0 = _stringLiteral951351530;
			G_B2_1 = L_2;
			goto IL_0035;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)SocketController_U3CStartU3Em__0_m1268981551_MethodInfo_var);
		Action_1_t3776982353 * L_5 = (Action_1_t3776982353 *)il2cpp_codegen_object_new(Action_1_t3776982353_il2cpp_TypeInfo_var);
		Action_1__ctor_m683306377(L_5, NULL, L_4, /*hidden argument*/Action_1__ctor_m683306377_MethodInfo_var);
		((SocketController_t1065234319_StaticFields*)SocketController_t1065234319_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_3(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_0035:
	{
		Action_1_t3776982353 * L_6 = ((SocketController_t1065234319_StaticFields*)SocketController_t1065234319_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		NullCheck(G_B2_1);
		VirtActionInvoker2< String_t*, Action_1_t3776982353 * >::Invoke(25 /* System.Void SocketIOClient.Client::On(System.String,System.Action`1<SocketIOClient.Messages.IMessage>) */, G_B2_1, G_B2_0, L_6);
		Client_t931100087 * L_7 = __this->get_client_2();
		Action_1_t3776982353 * L_8 = ((SocketController_t1065234319_StaticFields*)SocketController_t1065234319_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_4();
		G_B3_0 = _stringLiteral954925063;
		G_B3_1 = L_7;
		if (L_8)
		{
			G_B4_0 = _stringLiteral954925063;
			G_B4_1 = L_7;
			goto IL_0062;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)SocketController_U3CStartU3Em__1_m2994462448_MethodInfo_var);
		Action_1_t3776982353 * L_10 = (Action_1_t3776982353 *)il2cpp_codegen_object_new(Action_1_t3776982353_il2cpp_TypeInfo_var);
		Action_1__ctor_m683306377(L_10, NULL, L_9, /*hidden argument*/Action_1__ctor_m683306377_MethodInfo_var);
		((SocketController_t1065234319_StaticFields*)SocketController_t1065234319_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_4(L_10);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0062:
	{
		Action_1_t3776982353 * L_11 = ((SocketController_t1065234319_StaticFields*)SocketController_t1065234319_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_4();
		NullCheck(G_B4_1);
		VirtActionInvoker2< String_t*, Action_1_t3776982353 * >::Invoke(25 /* System.Void SocketIOClient.Client::On(System.String,System.Action`1<SocketIOClient.Messages.IMessage>) */, G_B4_1, G_B4_0, L_11);
		Client_t931100087 * L_12 = __this->get_client_2();
		Action_1_t3776982353 * L_13 = ((SocketController_t1065234319_StaticFields*)SocketController_t1065234319_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_5();
		G_B5_0 = _stringLiteral3117011;
		G_B5_1 = L_12;
		if (L_13)
		{
			G_B6_0 = _stringLiteral3117011;
			G_B6_1 = L_12;
			goto IL_008f;
		}
	}
	{
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)SocketController_U3CStartU3Em__2_m424976049_MethodInfo_var);
		Action_1_t3776982353 * L_15 = (Action_1_t3776982353 *)il2cpp_codegen_object_new(Action_1_t3776982353_il2cpp_TypeInfo_var);
		Action_1__ctor_m683306377(L_15, NULL, L_14, /*hidden argument*/Action_1__ctor_m683306377_MethodInfo_var);
		((SocketController_t1065234319_StaticFields*)SocketController_t1065234319_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_5(L_15);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
	}

IL_008f:
	{
		Action_1_t3776982353 * L_16 = ((SocketController_t1065234319_StaticFields*)SocketController_t1065234319_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_5();
		NullCheck(G_B6_1);
		VirtActionInvoker2< String_t*, Action_1_t3776982353 * >::Invoke(25 /* System.Void SocketIOClient.Client::On(System.String,System.Action`1<SocketIOClient.Messages.IMessage>) */, G_B6_1, G_B6_0, L_16);
		Client_t931100087 * L_17 = __this->get_client_2();
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(15 /* System.Void SocketIOClient.Client::Connect() */, L_17);
		return;
	}
}
// System.Void SocketController::<Start>m__0(SocketIOClient.Messages.IMessage)
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392340138;
extern const uint32_t SocketController_U3CStartU3Em__0_m1268981551_MetadataUsageId;
extern "C"  void SocketController_U3CStartU3Em__0_m1268981551 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SocketController_U3CStartU3Em__0_m1268981551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3392340138, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketController::<Start>m__1(SocketIOClient.Messages.IMessage)
extern TypeInfo* IMessage_t3628529648_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* ElementsController_t3637326867_il2cpp_TypeInfo_var;
extern const uint32_t SocketController_U3CStartU3Em__1_m2994462448_MetadataUsageId;
extern "C"  void SocketController_U3CStartU3Em__1_m2994462448 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SocketController_U3CStartU3Em__1_m2994462448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		Il2CppObject * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(6 /* System.String SocketIOClient.Messages.IMessage::get_MessageText() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_0);
		Single_TryParse_m3535027506(NULL /*static, unused*/, L_1, (&V_0), /*hidden argument*/NULL);
		float L_2 = V_0;
		float L_3 = ExtensionMethods_Map_m3708687785(NULL /*static, unused*/, L_2, (0.0f), (360.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = V_1;
		float L_5 = ExtensionMethods_Round_m1332053347(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Il2CppObject * L_6 = ___message;
		NullCheck(L_6);
		String_t* L_7 = InterfaceFuncInvoker0< String_t* >::Invoke(6 /* System.String SocketIOClient.Messages.IMessage::get_MessageText() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		float L_8 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(ElementsController_t3637326867_il2cpp_TypeInfo_var);
		ElementsController_changeIntesity_m1797890574(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketController::<Start>m__2(SocketIOClient.Messages.IMessage)
extern TypeInfo* IMessage_t3628529648_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t SocketController_U3CStartU3Em__2_m424976049_MetadataUsageId;
extern "C"  void SocketController_U3CStartU3Em__2_m424976049 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SocketController_U3CStartU3Em__2_m424976049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___data;
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(6 /* System.String SocketIOClient.Messages.IMessage::get_MessageText() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Client::.ctor(System.String)
extern "C"  void Client__ctor_m3888737708 (Client_t931100087 * __this, String_t* ___url, const MethodInfo* method)
{
	{
		String_t* L_0 = ___url;
		Client__ctor_m4197002827(__this, L_0, ((int32_t)13), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Client::.ctor(System.String,WebSocket4Net.WebSocketVersion)
extern TypeInfo* ManualResetEvent_t2399218676_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t2776692961_il2cpp_TypeInfo_var;
extern TypeInfo* RegistrationManager_t998967514_il2cpp_TypeInfo_var;
extern TypeInfo* ConcurrentQueue_1_t702095371_il2cpp_TypeInfo_var;
extern TypeInfo* ThreadStart_t2758142267_il2cpp_TypeInfo_var;
extern TypeInfo* Thread_t1674723085_il2cpp_TypeInfo_var;
extern const MethodInfo* ConcurrentQueue_1__ctor_m1291609117_MethodInfo_var;
extern const MethodInfo* Client_dequeuOutboundMessages_m370415647_MethodInfo_var;
extern const uint32_t Client__ctor_m4197002827_MetadataUsageId;
extern "C"  void Client__ctor_m4197002827 (Client_t931100087 * __this, String_t* ___url, int32_t ___socketVersion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client__ctor_m4197002827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_retryConnectionAttempts_4(3);
		__this->set_socketVersion_9(((int32_t)13));
		ManualResetEvent_t2399218676 * L_0 = (ManualResetEvent_t2399218676 *)il2cpp_codegen_object_new(ManualResetEvent_t2399218676_il2cpp_TypeInfo_var);
		ManualResetEvent__ctor_m927697317(L_0, (bool)1, /*hidden argument*/NULL);
		__this->set_MessageQueueEmptyEvent_10(L_0);
		ManualResetEvent_t2399218676 * L_1 = (ManualResetEvent_t2399218676 *)il2cpp_codegen_object_new(ManualResetEvent_t2399218676_il2cpp_TypeInfo_var);
		ManualResetEvent__ctor_m927697317(L_1, (bool)0, /*hidden argument*/NULL);
		__this->set_ConnectionOpenEvent_11(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_LastErrorMessage_12(L_2);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_3 = ___url;
		Uri_t2776692961 * L_4 = (Uri_t2776692961 *)il2cpp_codegen_object_new(Uri_t2776692961_il2cpp_TypeInfo_var);
		Uri__ctor_m1721267859(L_4, L_3, /*hidden argument*/NULL);
		__this->set_uri_6(L_4);
		int32_t L_5 = ___socketVersion;
		__this->set_socketVersion_9(L_5);
		RegistrationManager_t998967514 * L_6 = (RegistrationManager_t998967514 *)il2cpp_codegen_object_new(RegistrationManager_t998967514_il2cpp_TypeInfo_var);
		RegistrationManager__ctor_m1057237267(L_6, /*hidden argument*/NULL);
		__this->set_registrationManager_8(L_6);
		ConcurrentQueue_1_t702095371 * L_7 = (ConcurrentQueue_1_t702095371 *)il2cpp_codegen_object_new(ConcurrentQueue_1_t702095371_il2cpp_TypeInfo_var);
		ConcurrentQueue_1__ctor_m1291609117(L_7, /*hidden argument*/ConcurrentQueue_1__ctor_m1291609117_MethodInfo_var);
		__this->set_outboundQueue_2(L_7);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)Client_dequeuOutboundMessages_m370415647_MethodInfo_var);
		ThreadStart_t2758142267 * L_9 = (ThreadStart_t2758142267 *)il2cpp_codegen_object_new(ThreadStart_t2758142267_il2cpp_TypeInfo_var);
		ThreadStart__ctor_m346361139(L_9, __this, L_8, /*hidden argument*/NULL);
		Thread_t1674723085 * L_10 = (Thread_t1674723085 *)il2cpp_codegen_object_new(Thread_t1674723085_il2cpp_TypeInfo_var);
		Thread__ctor_m4293845138(L_10, L_9, /*hidden argument*/NULL);
		__this->set_dequeuOutBoundMsgTask_1(L_10);
		Thread_t1674723085 * L_11 = __this->get_dequeuOutBoundMsgTask_1();
		NullCheck(L_11);
		Thread_Start_m4066427317(L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Client::.cctor()
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern TypeInfo* Client_t931100087_il2cpp_TypeInfo_var;
extern const uint32_t Client__cctor_m3359426903_MetadataUsageId;
extern "C"  void Client__cctor_m3359426903 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client__cctor_m3359426903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		((Client_t931100087_StaticFields*)Client_t931100087_il2cpp_TypeInfo_var->static_fields)->set_padLock_5(L_0);
		return;
	}
}
// System.Void SocketIOClient.Client::add_Opened(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t Client_add_Opened_m3947533902_MetadataUsageId;
extern "C"  void Client_add_Opened_m3947533902 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_add_Opened_m3947533902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_Opened_13();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_Opened_13(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SocketIOClient.Client::remove_Opened(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t Client_remove_Opened_m3906053675_MetadataUsageId;
extern "C"  void Client_remove_Opened_m3906053675 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_remove_Opened_m3906053675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_Opened_13();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_Opened_13(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SocketIOClient.Client::add_Message(System.EventHandler`1<SocketIOClient.MessageEventArgs>)
extern TypeInfo* EventHandler_1_t2244256551_il2cpp_TypeInfo_var;
extern const uint32_t Client_add_Message_m3258323997_MetadataUsageId;
extern "C"  void Client_add_Message_m3258323997 (Client_t931100087 * __this, EventHandler_1_t2244256551 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_add_Message_m3258323997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t2244256551 * L_0 = __this->get_Message_14();
		EventHandler_1_t2244256551 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_Message_14(((EventHandler_1_t2244256551 *)CastclassSealed(L_2, EventHandler_1_t2244256551_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SocketIOClient.Client::remove_Message(System.EventHandler`1<SocketIOClient.MessageEventArgs>)
extern TypeInfo* EventHandler_1_t2244256551_il2cpp_TypeInfo_var;
extern const uint32_t Client_remove_Message_m2508405114_MetadataUsageId;
extern "C"  void Client_remove_Message_m2508405114 (Client_t931100087 * __this, EventHandler_1_t2244256551 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_remove_Message_m2508405114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t2244256551 * L_0 = __this->get_Message_14();
		EventHandler_1_t2244256551 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_Message_14(((EventHandler_1_t2244256551 *)CastclassSealed(L_2, EventHandler_1_t2244256551_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SocketIOClient.Client::add_ConnectionRetryAttempt(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t Client_add_ConnectionRetryAttempt_m985458676_MetadataUsageId;
extern "C"  void Client_add_ConnectionRetryAttempt_m985458676 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_add_ConnectionRetryAttempt_m985458676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_ConnectionRetryAttempt_15();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ConnectionRetryAttempt_15(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SocketIOClient.Client::remove_ConnectionRetryAttempt(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t Client_remove_ConnectionRetryAttempt_m3773472209_MetadataUsageId;
extern "C"  void Client_remove_ConnectionRetryAttempt_m3773472209 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_remove_ConnectionRetryAttempt_m3773472209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_ConnectionRetryAttempt_15();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ConnectionRetryAttempt_15(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SocketIOClient.Client::add_HeartBeatTimerEvent(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t Client_add_HeartBeatTimerEvent_m765053468_MetadataUsageId;
extern "C"  void Client_add_HeartBeatTimerEvent_m765053468 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_add_HeartBeatTimerEvent_m765053468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_HeartBeatTimerEvent_16();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_HeartBeatTimerEvent_16(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SocketIOClient.Client::remove_HeartBeatTimerEvent(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t Client_remove_HeartBeatTimerEvent_m2663000415_MetadataUsageId;
extern "C"  void Client_remove_HeartBeatTimerEvent_m2663000415 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_remove_HeartBeatTimerEvent_m2663000415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_HeartBeatTimerEvent_16();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_HeartBeatTimerEvent_16(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SocketIOClient.Client::add_SocketConnectionClosed(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t Client_add_SocketConnectionClosed_m4008164794_MetadataUsageId;
extern "C"  void Client_add_SocketConnectionClosed_m4008164794 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_add_SocketConnectionClosed_m4008164794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_SocketConnectionClosed_17();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_SocketConnectionClosed_17(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SocketIOClient.Client::remove_SocketConnectionClosed(System.EventHandler)
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern const uint32_t Client_remove_SocketConnectionClosed_m2501211031_MetadataUsageId;
extern "C"  void Client_remove_SocketConnectionClosed_m2501211031 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_remove_SocketConnectionClosed_m2501211031_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t247020293 * L_0 = __this->get_SocketConnectionClosed_17();
		EventHandler_t247020293 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_SocketConnectionClosed_17(((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SocketIOClient.Client::add_Error(System.EventHandler`1<SocketIOClient.ErrorEventArgs>)
extern TypeInfo* EventHandler_1_t1185090118_il2cpp_TypeInfo_var;
extern const uint32_t Client_add_Error_m2029420669_MetadataUsageId;
extern "C"  void Client_add_Error_m2029420669 (Client_t931100087 * __this, EventHandler_1_t1185090118 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_add_Error_m2029420669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t1185090118 * L_0 = __this->get_Error_18();
		EventHandler_1_t1185090118 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_Error_18(((EventHandler_1_t1185090118 *)CastclassSealed(L_2, EventHandler_1_t1185090118_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SocketIOClient.Client::remove_Error(System.EventHandler`1<SocketIOClient.ErrorEventArgs>)
extern TypeInfo* EventHandler_1_t1185090118_il2cpp_TypeInfo_var;
extern const uint32_t Client_remove_Error_m3787381850_MetadataUsageId;
extern "C"  void Client_remove_Error_m3787381850 (Client_t931100087 * __this, EventHandler_1_t1185090118 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_remove_Error_m3787381850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t1185090118 * L_0 = __this->get_Error_18();
		EventHandler_1_t1185090118 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_Error_18(((EventHandler_1_t1185090118 *)CastclassSealed(L_2, EventHandler_1_t1185090118_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Int32 SocketIOClient.Client::get_RetryConnectionAttempts()
extern "C"  int32_t Client_get_RetryConnectionAttempts_m14417765 (Client_t931100087 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_retryConnectionAttempts_4();
		return L_0;
	}
}
// System.Void SocketIOClient.Client::set_RetryConnectionAttempts(System.Int32)
extern "C"  void Client_set_RetryConnectionAttempts_m1697322804 (Client_t931100087 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_retryConnectionAttempts_4(L_0);
		return;
	}
}
// SocketIOClient.SocketIOHandshake SocketIOClient.Client::get_HandShake()
extern "C"  SocketIOHandshake_t3315670474 * Client_get_HandShake_m3549459883 (Client_t931100087 * __this, const MethodInfo* method)
{
	{
		SocketIOHandshake_t3315670474 * L_0 = __this->get_U3CHandShakeU3Ek__BackingField_19();
		return L_0;
	}
}
// System.Void SocketIOClient.Client::set_HandShake(SocketIOClient.SocketIOHandshake)
extern "C"  void Client_set_HandShake_m1094163898 (Client_t931100087 * __this, SocketIOHandshake_t3315670474 * ___value, const MethodInfo* method)
{
	{
		SocketIOHandshake_t3315670474 * L_0 = ___value;
		__this->set_U3CHandShakeU3Ek__BackingField_19(L_0);
		return;
	}
}
// System.Boolean SocketIOClient.Client::get_IsConnected()
extern "C"  bool Client_get_IsConnected_m2845721918 (Client_t931100087 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(14 /* WebSocket4Net.WebSocketState SocketIOClient.Client::get_ReadyState() */, __this);
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// WebSocket4Net.WebSocketState SocketIOClient.Client::get_ReadyState()
extern "C"  int32_t Client_get_ReadyState_m3231756144 (Client_t931100087 * __this, const MethodInfo* method)
{
	{
		WebSocket_t713846903 * L_0 = __this->get_wsClient_7();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		WebSocket_t713846903 * L_1 = __this->get_wsClient_7();
		NullCheck(L_1);
		int32_t L_2 = WebSocket_get_State_m3481149631(L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0017:
	{
		return (int32_t)((-1));
	}
}
// System.Void SocketIOClient.Client::Connect()
extern TypeInfo* Client_t931100087_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* ErrorEventArgs_t342512475_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t2776692961_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* WebSocket_t713846903_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_1_t419508266_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_1_t2171844441_il2cpp_TypeInfo_var;
extern const MethodInfo* Client_wsClient_OpenEvent_m1278167232_MethodInfo_var;
extern const MethodInfo* Client_wsClient_MessageReceived_m182254307_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3500534924_MethodInfo_var;
extern const MethodInfo* Client_wsClient_Error_m50370738_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3300027855_MethodInfo_var;
extern const MethodInfo* Client_wsClient_Closed_m3514215282_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral827348018;
extern Il2CppCodeGenString* _stringLiteral118039;
extern Il2CppCodeGenString* _stringLiteral3804;
extern Il2CppCodeGenString* _stringLiteral38328131;
extern Il2CppCodeGenString* _stringLiteral2341455100;
extern Il2CppCodeGenString* _stringLiteral2349835984;
extern const uint32_t Client_Connect_m2191290334_MetadataUsageId;
extern "C"  void Client_Connect_m2191290334 (Client_t931100087 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_Connect_m2191290334_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B10_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Client_t931100087_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((Client_t931100087_StaticFields*)Client_t931100087_il2cpp_TypeInfo_var->static_fields)->get_padLock_5();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(14 /* WebSocket4Net.WebSocketState SocketIOClient.Client::get_ReadyState() */, __this);
			if (!L_2)
			{
				goto IL_01d0;
			}
		}

IL_0017:
		{
			int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(14 /* WebSocket4Net.WebSocketState SocketIOClient.Client::get_ReadyState() */, __this);
			if ((((int32_t)L_3) == ((int32_t)1)))
			{
				goto IL_01d0;
			}
		}

IL_0023:
		try
		{ // begin try (depth: 2)
			{
				ManualResetEvent_t2399218676 * L_4 = __this->get_ConnectionOpenEvent_11();
				NullCheck(L_4);
				EventWaitHandle_Reset_m330657435(L_4, /*hidden argument*/NULL);
				Uri_t2776692961 * L_5 = __this->get_uri_6();
				SocketIOHandshake_t3315670474 * L_6 = Client_requestHandshake_m1889130752(__this, L_5, /*hidden argument*/NULL);
				Client_set_HandShake_m1094163898(__this, L_6, /*hidden argument*/NULL);
				SocketIOHandshake_t3315670474 * L_7 = VirtFuncInvoker0< SocketIOHandshake_t3315670474 * >::Invoke(12 /* SocketIOClient.SocketIOHandshake SocketIOClient.Client::get_HandShake() */, __this);
				if (!L_7)
				{
					goto IL_0071;
				}
			}

IL_004c:
			{
				SocketIOHandshake_t3315670474 * L_8 = VirtFuncInvoker0< SocketIOHandshake_t3315670474 * >::Invoke(12 /* SocketIOClient.SocketIOHandshake SocketIOClient.Client::get_HandShake() */, __this);
				NullCheck(L_8);
				String_t* L_9 = SocketIOHandshake_get_SID_m3604612263(L_8, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				bool L_10 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
				if (L_10)
				{
					goto IL_0071;
				}
			}

IL_0061:
			{
				SocketIOHandshake_t3315670474 * L_11 = VirtFuncInvoker0< SocketIOHandshake_t3315670474 * >::Invoke(12 /* SocketIOClient.SocketIOHandshake SocketIOClient.Client::get_HandShake() */, __this);
				NullCheck(L_11);
				bool L_12 = SocketIOHandshake_get_HadError_m2556394295(L_11, /*hidden argument*/NULL);
				if (!L_12)
				{
					goto IL_00a8;
				}
			}

IL_0071:
			{
				Uri_t2776692961 * L_13 = __this->get_uri_6();
				NullCheck(L_13);
				String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_13);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_15 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral827348018, L_14, /*hidden argument*/NULL);
				__this->set_LastErrorMessage_12(L_15);
				String_t* L_16 = __this->get_LastErrorMessage_12();
				Exception_t1967233988 * L_17 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
				Exception__ctor_m3223090658(L_17, /*hidden argument*/NULL);
				ErrorEventArgs_t342512475 * L_18 = (ErrorEventArgs_t342512475 *)il2cpp_codegen_object_new(ErrorEventArgs_t342512475_il2cpp_TypeInfo_var);
				ErrorEventArgs__ctor_m2711369958(L_18, L_16, L_17, /*hidden argument*/NULL);
				Client_OnErrorEvent_m447362248(__this, __this, L_18, /*hidden argument*/NULL);
				goto IL_019e;
			}

IL_00a8:
			{
				Uri_t2776692961 * L_19 = __this->get_uri_6();
				NullCheck(L_19);
				String_t* L_20 = Uri_get_Scheme_m2606456870(L_19, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Uri_t2776692961_il2cpp_TypeInfo_var);
				String_t* L_21 = ((Uri_t2776692961_StaticFields*)Uri_t2776692961_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeHttps_25();
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				bool L_22 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
				if (!L_22)
				{
					goto IL_00cc;
				}
			}

IL_00c2:
			{
				G_B10_0 = _stringLiteral118039;
				goto IL_00d1;
			}

IL_00cc:
			{
				G_B10_0 = _stringLiteral3804;
			}

IL_00d1:
			{
				V_1 = G_B10_0;
				ObjectU5BU5D_t11523773* L_23 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
				String_t* L_24 = V_1;
				NullCheck(L_23);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 0);
				ArrayElementTypeCheck (L_23, L_24);
				(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_24);
				ObjectU5BU5D_t11523773* L_25 = L_23;
				Uri_t2776692961 * L_26 = __this->get_uri_6();
				NullCheck(L_26);
				String_t* L_27 = Uri_get_Host_m1446697833(L_26, /*hidden argument*/NULL);
				NullCheck(L_25);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 1);
				ArrayElementTypeCheck (L_25, L_27);
				(L_25)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_27);
				ObjectU5BU5D_t11523773* L_28 = L_25;
				Uri_t2776692961 * L_29 = __this->get_uri_6();
				NullCheck(L_29);
				int32_t L_30 = Uri_get_Port_m2253782543(L_29, /*hidden argument*/NULL);
				int32_t L_31 = L_30;
				Il2CppObject * L_32 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_31);
				NullCheck(L_28);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 2);
				ArrayElementTypeCheck (L_28, L_32);
				(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_32);
				ObjectU5BU5D_t11523773* L_33 = L_28;
				SocketIOHandshake_t3315670474 * L_34 = VirtFuncInvoker0< SocketIOHandshake_t3315670474 * >::Invoke(12 /* SocketIOClient.SocketIOHandshake SocketIOClient.Client::get_HandShake() */, __this);
				NullCheck(L_34);
				String_t* L_35 = SocketIOHandshake_get_SID_m3604612263(L_34, /*hidden argument*/NULL);
				NullCheck(L_33);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 3);
				ArrayElementTypeCheck (L_33, L_35);
				(L_33)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_35);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_36 = String_Format_m4050103162(NULL /*static, unused*/, _stringLiteral38328131, L_33, /*hidden argument*/NULL);
				String_t* L_37 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
				int32_t L_38 = __this->get_socketVersion_9();
				WebSocket_t713846903 * L_39 = (WebSocket_t713846903 *)il2cpp_codegen_object_new(WebSocket_t713846903_il2cpp_TypeInfo_var);
				WebSocket__ctor_m3811251545(L_39, L_36, L_37, L_38, /*hidden argument*/NULL);
				__this->set_wsClient_7(L_39);
				WebSocket_t713846903 * L_40 = __this->get_wsClient_7();
				NullCheck(L_40);
				WebSocket_set_EnableAutoSendPing_m1331589028(L_40, (bool)1, /*hidden argument*/NULL);
				WebSocket_t713846903 * L_41 = __this->get_wsClient_7();
				IntPtr_t L_42;
				L_42.set_m_value_0((void*)Client_wsClient_OpenEvent_m1278167232_MethodInfo_var);
				EventHandler_t247020293 * L_43 = (EventHandler_t247020293 *)il2cpp_codegen_object_new(EventHandler_t247020293_il2cpp_TypeInfo_var);
				EventHandler__ctor_m4096468317(L_43, __this, L_42, /*hidden argument*/NULL);
				NullCheck(L_41);
				WebSocket_add_Opened_m1939620504(L_41, L_43, /*hidden argument*/NULL);
				WebSocket_t713846903 * L_44 = __this->get_wsClient_7();
				IntPtr_t L_45;
				L_45.set_m_value_0((void*)Client_wsClient_MessageReceived_m182254307_MethodInfo_var);
				EventHandler_1_t419508266 * L_46 = (EventHandler_1_t419508266 *)il2cpp_codegen_object_new(EventHandler_1_t419508266_il2cpp_TypeInfo_var);
				EventHandler_1__ctor_m3500534924(L_46, __this, L_45, /*hidden argument*/EventHandler_1__ctor_m3500534924_MethodInfo_var);
				NullCheck(L_44);
				WebSocket_add_MessageReceived_m1609756021(L_44, L_46, /*hidden argument*/NULL);
				WebSocket_t713846903 * L_47 = __this->get_wsClient_7();
				IntPtr_t L_48;
				L_48.set_m_value_0((void*)Client_wsClient_Error_m50370738_MethodInfo_var);
				EventHandler_1_t2171844441 * L_49 = (EventHandler_1_t2171844441 *)il2cpp_codegen_object_new(EventHandler_1_t2171844441_il2cpp_TypeInfo_var);
				EventHandler_1__ctor_m3300027855(L_49, __this, L_48, /*hidden argument*/EventHandler_1__ctor_m3300027855_MethodInfo_var);
				NullCheck(L_47);
				WebSocket_add_Error_m447077374(L_47, L_49, /*hidden argument*/NULL);
				WebSocket_t713846903 * L_50 = __this->get_wsClient_7();
				IntPtr_t L_51;
				L_51.set_m_value_0((void*)Client_wsClient_Closed_m3514215282_MethodInfo_var);
				EventHandler_t247020293 * L_52 = (EventHandler_t247020293 *)il2cpp_codegen_object_new(EventHandler_t247020293_il2cpp_TypeInfo_var);
				EventHandler__ctor_m4096468317(L_52, __this, L_51, /*hidden argument*/NULL);
				NullCheck(L_50);
				WebSocket_add_Closed_m3528784245(L_50, L_52, /*hidden argument*/NULL);
				WebSocket_t713846903 * L_53 = __this->get_wsClient_7();
				NullCheck(L_53);
				WebSocket_Open_m1696656814(L_53, /*hidden argument*/NULL);
			}

IL_019e:
			{
				goto IL_01d0;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_01a3;
			throw e;
		}

CATCH_01a3:
		{ // begin catch(System.Exception)
			V_2 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_54 = V_2;
			NullCheck(L_54);
			String_t* L_55 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_54);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_56 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral2341455100, L_55, /*hidden argument*/NULL);
			Trace_WriteLine_m3302529945(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
			Exception_t1967233988 * L_57 = V_2;
			ErrorEventArgs_t342512475 * L_58 = (ErrorEventArgs_t342512475 *)il2cpp_codegen_object_new(ErrorEventArgs_t342512475_il2cpp_TypeInfo_var);
			ErrorEventArgs__ctor_m2711369958(L_58, _stringLiteral2349835984, L_57, /*hidden argument*/NULL);
			Client_OnErrorEvent_m447362248(__this, __this, L_58, /*hidden argument*/NULL);
			goto IL_01d0;
		} // end catch (depth: 2)

IL_01d0:
		{
			IL2CPP_LEAVE(0x1DC, FINALLY_01d5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01d5;
	}

FINALLY_01d5:
	{ // begin finally (depth: 1)
		Il2CppObject * L_59 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(469)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(469)
	{
		IL2CPP_JUMP_TBL(0x1DC, IL_01dc)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01dc:
	{
		return;
	}
}
// SocketIOClient.IEndPointClient SocketIOClient.Client::Connect(System.String)
extern TypeInfo* EndPointClient_t1953350540_il2cpp_TypeInfo_var;
extern TypeInfo* ConnectMessage_t3539960911_il2cpp_TypeInfo_var;
extern const uint32_t Client_Connect_m2054495046_MetadataUsageId;
extern "C"  Il2CppObject * Client_Connect_m2054495046 (Client_t931100087 * __this, String_t* ___endPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_Connect_m2054495046_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EndPointClient_t1953350540 * V_0 = NULL;
	{
		String_t* L_0 = ___endPoint;
		EndPointClient_t1953350540 * L_1 = (EndPointClient_t1953350540 *)il2cpp_codegen_object_new(EndPointClient_t1953350540_il2cpp_TypeInfo_var);
		EndPointClient__ctor_m889041267(L_1, __this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		VirtActionInvoker0::Invoke(15 /* System.Void SocketIOClient.Client::Connect() */, __this);
		String_t* L_2 = ___endPoint;
		ConnectMessage_t3539960911 * L_3 = (ConnectMessage_t3539960911 *)il2cpp_codegen_object_new(ConnectMessage_t3539960911_il2cpp_TypeInfo_var);
		ConnectMessage__ctor_m1926351452(L_3, L_2, /*hidden argument*/NULL);
		VirtActionInvoker1< Il2CppObject * >::Invoke(23 /* System.Void SocketIOClient.Client::Send(SocketIOClient.Messages.IMessage) */, __this, L_3);
		EndPointClient_t1953350540 * L_4 = V_0;
		return L_4;
	}
}
// System.Void SocketIOClient.Client::ReConnect()
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t211005341_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1709701652;
extern const uint32_t Client_ReConnect_m3931142699_MetadataUsageId;
extern "C"  void Client_ReConnect_m3931142699 (Client_t931100087 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_ReConnect_m3931142699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_retryConnectionCount_3();
		__this->set_retryConnectionCount_3(((int32_t)((int32_t)L_0+(int32_t)1)));
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs_t516466188 * L_1 = ((EventArgs_t516466188_StaticFields*)EventArgs_t516466188_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		Client_OnConnectionRetryAttemptEvent_m3751111942(__this, __this, L_1, /*hidden argument*/NULL);
		Client_closeHeartBeatTimer_m1939381365(__this, /*hidden argument*/NULL);
		Client_closeWebSocketClient_m254982728(__this, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(15 /* System.Void SocketIOClient.Client::Connect() */, __this);
		ManualResetEvent_t2399218676 * L_2 = __this->get_ConnectionOpenEvent_11();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker1< bool, int32_t >::Invoke(11 /* System.Boolean System.Threading.WaitHandle::WaitOne(System.Int32) */, L_2, ((int32_t)4000));
		V_0 = L_3;
		bool L_4 = V_0;
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1709701652, L_6, /*hidden argument*/NULL);
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0064;
		}
	}
	{
		__this->set_retryConnectionCount_3(0);
		goto IL_0092;
	}

IL_0064:
	{
		int32_t L_9 = __this->get_retryConnectionCount_3();
		int32_t L_10 = Client_get_RetryConnectionAttempts_m14417765(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_9) >= ((int32_t)L_10)))
		{
			goto IL_0080;
		}
	}
	{
		Client_ReConnect_m3931142699(__this, /*hidden argument*/NULL);
		goto IL_0092;
	}

IL_0080:
	{
		VirtActionInvoker0::Invoke(17 /* System.Void SocketIOClient.Client::Close() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs_t516466188 * L_11 = ((EventArgs_t516466188_StaticFields*)EventArgs_t516466188_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		Client_OnSocketConnectionClosedEvent_m3101161164(__this, __this, L_11, /*hidden argument*/NULL);
	}

IL_0092:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::On(System.String,System.Action`1<SocketIOClient.Messages.IMessage>)
extern "C"  void Client_On_m873427727 (Client_t931100087 * __this, String_t* ___eventName, Action_1_t3776982353 * ___action, const MethodInfo* method)
{
	{
		RegistrationManager_t998967514 * L_0 = __this->get_registrationManager_8();
		String_t* L_1 = ___eventName;
		Action_1_t3776982353 * L_2 = ___action;
		NullCheck(L_0);
		RegistrationManager_AddOnEvent_m1569321649(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Client::On(System.String,System.String,System.Action`1<SocketIOClient.Messages.IMessage>)
extern "C"  void Client_On_m3237792651 (Client_t931100087 * __this, String_t* ___eventName, String_t* ___endPoint, Action_1_t3776982353 * ___action, const MethodInfo* method)
{
	{
		RegistrationManager_t998967514 * L_0 = __this->get_registrationManager_8();
		String_t* L_1 = ___eventName;
		String_t* L_2 = ___endPoint;
		Action_1_t3776982353 * L_3 = ___action;
		NullCheck(L_0);
		RegistrationManager_AddOnEvent_m2206615469(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Client::Emit(System.String,System.Object,System.String,System.Action`1<System.Object>)
extern TypeInfo* Client_t931100087_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TextMessage_t120705644_il2cpp_TypeInfo_var;
extern TypeInfo* JSONMessage_t3736498801_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern TypeInfo* EventMessage_t365350047_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral954925063;
extern Il2CppCodeGenString* _stringLiteral951351530;
extern Il2CppCodeGenString* _stringLiteral530405532;
extern Il2CppCodeGenString* _stringLiteral3417674;
extern Il2CppCodeGenString* _stringLiteral94756344;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern Il2CppCodeGenString* _stringLiteral108405416;
extern Il2CppCodeGenString* _stringLiteral990157655;
extern Il2CppCodeGenString* _stringLiteral2193524708;
extern Il2CppCodeGenString* _stringLiteral47;
extern const uint32_t Client_Emit_m1575080133_MetadataUsageId;
extern "C"  void Client_Emit_m1575080133 (Client_t931100087 * __this, String_t* ___eventName, Il2CppObject * ___payload, String_t* ___endPoint, Action_1_t985559125 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_Emit_m1575080133_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	String_t* V_2 = NULL;
	Dictionary_2_t190145395 * V_3 = NULL;
	int32_t V_4 = 0;
	TextMessage_t120705644 * V_5 = NULL;
	{
		String_t* L_0 = ___eventName;
		NullCheck(L_0);
		String_t* L_1 = String_ToLower_m2421900555(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = (Il2CppObject *)NULL;
		String_t* L_2 = V_0;
		V_2 = L_2;
		String_t* L_3 = V_2;
		if (!L_3)
		{
			goto IL_00f4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Client_t931100087_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_4 = ((Client_t931100087_StaticFields*)Client_t931100087_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_20();
		if (L_4)
		{
			goto IL_0088;
		}
	}
	{
		Dictionary_2_t190145395 * L_5 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_5, 8, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_3 = L_5;
		Dictionary_2_t190145395 * L_6 = V_3;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_6, _stringLiteral954925063, 0);
		Dictionary_2_t190145395 * L_7 = V_3;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_7, _stringLiteral951351530, 1);
		Dictionary_2_t190145395 * L_8 = V_3;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_8, _stringLiteral530405532, 1);
		Dictionary_2_t190145395 * L_9 = V_3;
		NullCheck(L_9);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_9, _stringLiteral3417674, 1);
		Dictionary_2_t190145395 * L_10 = V_3;
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_10, _stringLiteral94756344, 1);
		Dictionary_2_t190145395 * L_11 = V_3;
		NullCheck(L_11);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_11, _stringLiteral96784904, 1);
		Dictionary_2_t190145395 * L_12 = V_3;
		NullCheck(L_12);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_12, _stringLiteral108405416, 1);
		Dictionary_2_t190145395 * L_13 = V_3;
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_13, _stringLiteral990157655, 1);
		Dictionary_2_t190145395 * L_14 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Client_t931100087_il2cpp_TypeInfo_var);
		((Client_t931100087_StaticFields*)Client_t931100087_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map0_20(L_14);
	}

IL_0088:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Client_t931100087_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_15 = ((Client_t931100087_StaticFields*)Client_t931100087_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_20();
		String_t* L_16 = V_2;
		NullCheck(L_15);
		bool L_17 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_15, L_16, (&V_4));
		if (!L_17)
		{
			goto IL_00f4;
		}
	}
	{
		int32_t L_18 = V_4;
		if (!L_18)
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_19 = V_4;
		if ((((int32_t)L_19) == ((int32_t)1)))
		{
			goto IL_00e8;
		}
	}
	{
		goto IL_00f4;
	}

IL_00ae:
	{
		Il2CppObject * L_20 = ___payload;
		if (!((String_t*)IsInstSealed(L_20, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_00d5;
		}
	}
	{
		TextMessage_t120705644 * L_21 = (TextMessage_t120705644 *)il2cpp_codegen_object_new(TextMessage_t120705644_il2cpp_TypeInfo_var);
		TextMessage__ctor_m374255273(L_21, /*hidden argument*/NULL);
		V_5 = L_21;
		TextMessage_t120705644 * L_22 = V_5;
		Il2CppObject * L_23 = ___payload;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_23);
		NullCheck(L_22);
		Message_set_MessageText_m231655857(L_22, L_24, /*hidden argument*/NULL);
		TextMessage_t120705644 * L_25 = V_5;
		V_1 = L_25;
		goto IL_00dc;
	}

IL_00d5:
	{
		Il2CppObject * L_26 = ___payload;
		JSONMessage_t3736498801 * L_27 = (JSONMessage_t3736498801 *)il2cpp_codegen_object_new(JSONMessage_t3736498801_il2cpp_TypeInfo_var);
		JSONMessage__ctor_m3855190992(L_27, L_26, /*hidden argument*/NULL);
		V_1 = L_27;
	}

IL_00dc:
	{
		Il2CppObject * L_28 = V_1;
		VirtActionInvoker1< Il2CppObject * >::Invoke(23 /* System.Void SocketIOClient.Client::Send(SocketIOClient.Messages.IMessage) */, __this, L_28);
		goto IL_0146;
	}

IL_00e8:
	{
		String_t* L_29 = ___eventName;
		ArgumentOutOfRangeException_t3479058991 * L_30 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_30, L_29, _stringLiteral2193524708, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_00f4:
	{
		String_t* L_31 = ___endPoint;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_32 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_011c;
		}
	}
	{
		String_t* L_33 = ___endPoint;
		NullCheck(L_33);
		bool L_34 = String_StartsWith_m1500793453(L_33, _stringLiteral47, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_011c;
		}
	}
	{
		String_t* L_35 = ___endPoint;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral47, L_35, /*hidden argument*/NULL);
		___endPoint = L_36;
	}

IL_011c:
	{
		String_t* L_37 = ___eventName;
		Il2CppObject * L_38 = ___payload;
		String_t* L_39 = ___endPoint;
		Action_1_t985559125 * L_40 = ___callback;
		EventMessage_t365350047 * L_41 = (EventMessage_t365350047 *)il2cpp_codegen_object_new(EventMessage_t365350047_il2cpp_TypeInfo_var);
		EventMessage__ctor_m820155226(L_41, L_37, L_38, L_39, L_40, /*hidden argument*/NULL);
		V_1 = L_41;
		Action_1_t985559125 * L_42 = ___callback;
		if (!L_42)
		{
			goto IL_013a;
		}
	}
	{
		RegistrationManager_t998967514 * L_43 = __this->get_registrationManager_8();
		Il2CppObject * L_44 = V_1;
		NullCheck(L_43);
		RegistrationManager_AddCallBack_m2141734945(L_43, L_44, /*hidden argument*/NULL);
	}

IL_013a:
	{
		Il2CppObject * L_45 = V_1;
		VirtActionInvoker1< Il2CppObject * >::Invoke(23 /* System.Void SocketIOClient.Client::Send(SocketIOClient.Messages.IMessage) */, __this, L_45);
		goto IL_0146;
	}

IL_0146:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::Emit(System.String,System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Client_Emit_m1676394799_MetadataUsageId;
extern "C"  void Client_Emit_m1676394799 (Client_t931100087 * __this, String_t* ___eventName, Il2CppObject * ___payload, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_Emit_m1676394799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___eventName;
		Il2CppObject * L_1 = ___payload;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		VirtActionInvoker4< String_t*, Il2CppObject *, String_t*, Action_1_t985559125 * >::Invoke(22 /* System.Void SocketIOClient.Client::Emit(System.String,System.Object,System.String,System.Action`1<System.Object>) */, __this, L_0, L_1, L_2, (Action_1_t985559125 *)NULL);
		return;
	}
}
// System.Void SocketIOClient.Client::Send(SocketIOClient.Messages.IMessage)
extern TypeInfo* IMessage_t3628529648_il2cpp_TypeInfo_var;
extern const MethodInfo* ConcurrentQueue_1_Enqueue_m1527058932_MethodInfo_var;
extern const uint32_t Client_Send_m1064201792_MetadataUsageId;
extern "C"  void Client_Send_m1064201792 (Client_t931100087 * __this, Il2CppObject * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_Send_m1064201792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ManualResetEvent_t2399218676 * L_0 = __this->get_MessageQueueEmptyEvent_10();
		NullCheck(L_0);
		EventWaitHandle_Reset_m330657435(L_0, /*hidden argument*/NULL);
		ConcurrentQueue_1_t702095371 * L_1 = __this->get_outboundQueue_2();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		ConcurrentQueue_1_t702095371 * L_2 = __this->get_outboundQueue_2();
		Il2CppObject * L_3 = ___msg;
		NullCheck(L_3);
		String_t* L_4 = InterfaceFuncInvoker0< String_t* >::Invoke(9 /* System.String SocketIOClient.Messages.IMessage::get_Encoded() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_3);
		NullCheck(L_2);
		ConcurrentQueue_1_Enqueue_m1527058932(L_2, L_4, /*hidden argument*/ConcurrentQueue_1_Enqueue_m1527058932_MethodInfo_var);
	}

IL_0028:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::Send(System.String)
extern TypeInfo* TextMessage_t120705644_il2cpp_TypeInfo_var;
extern const uint32_t Client_Send_m3049882572_MetadataUsageId;
extern "C"  void Client_Send_m3049882572 (Client_t931100087 * __this, String_t* ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_Send_m3049882572_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	TextMessage_t120705644 * V_1 = NULL;
	{
		TextMessage_t120705644 * L_0 = (TextMessage_t120705644 *)il2cpp_codegen_object_new(TextMessage_t120705644_il2cpp_TypeInfo_var);
		TextMessage__ctor_m374255273(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		TextMessage_t120705644 * L_1 = V_1;
		String_t* L_2 = ___msg;
		NullCheck(L_1);
		Message_set_MessageText_m231655857(L_1, L_2, /*hidden argument*/NULL);
		TextMessage_t120705644 * L_3 = V_1;
		V_0 = L_3;
		Il2CppObject * L_4 = V_0;
		VirtActionInvoker1< Il2CppObject * >::Invoke(23 /* System.Void SocketIOClient.Client::Send(SocketIOClient.Messages.IMessage) */, __this, L_4);
		return;
	}
}
// System.Void SocketIOClient.Client::Send_backup(System.String)
extern const MethodInfo* ConcurrentQueue_1_Enqueue_m1527058932_MethodInfo_var;
extern const uint32_t Client_Send_backup_m2549701653_MetadataUsageId;
extern "C"  void Client_Send_backup_m2549701653 (Client_t931100087 * __this, String_t* ___rawEncodedMessageText, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_Send_backup_m2549701653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ManualResetEvent_t2399218676 * L_0 = __this->get_MessageQueueEmptyEvent_10();
		NullCheck(L_0);
		EventWaitHandle_Reset_m330657435(L_0, /*hidden argument*/NULL);
		ConcurrentQueue_1_t702095371 * L_1 = __this->get_outboundQueue_2();
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ConcurrentQueue_1_t702095371 * L_2 = __this->get_outboundQueue_2();
		String_t* L_3 = ___rawEncodedMessageText;
		NullCheck(L_2);
		ConcurrentQueue_1_Enqueue_m1527058932(L_2, L_3, /*hidden argument*/ConcurrentQueue_1_Enqueue_m1527058932_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::OnMessageEvent(SocketIOClient.Messages.IMessage)
extern TypeInfo* IMessage_t3628529648_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MessageEventArgs_t1401678908_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m862903716_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1817752178;
extern const uint32_t Client_OnMessageEvent_m1312391754_MetadataUsageId;
extern "C"  void Client_OnMessageEvent_m1312391754 (Client_t931100087 * __this, Il2CppObject * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_OnMessageEvent_m1312391754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	EventHandler_1_t2244256551 * V_1 = NULL;
	{
		V_0 = (bool)0;
		Il2CppObject * L_0 = ___msg;
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String SocketIOClient.Messages.IMessage::get_Event() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		RegistrationManager_t998967514 * L_3 = __this->get_registrationManager_8();
		Il2CppObject * L_4 = ___msg;
		NullCheck(L_3);
		bool L_5 = RegistrationManager_InvokeOnEvent_m2394835410(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_001f:
	{
		EventHandler_1_t2244256551 * L_6 = __this->get_Message_14();
		V_1 = L_6;
		EventHandler_1_t2244256551 * L_7 = V_1;
		if (!L_7)
		{
			goto IL_0054;
		}
	}
	{
		bool L_8 = V_0;
		if (L_8)
		{
			goto IL_0054;
		}
	}
	{
		Il2CppObject * L_9 = ___msg;
		NullCheck(L_9);
		String_t* L_10 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String SocketIOClient.Messages.IMessage::get_RawMessage() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1817752178, L_10, /*hidden argument*/NULL);
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		EventHandler_1_t2244256551 * L_12 = V_1;
		Il2CppObject * L_13 = ___msg;
		MessageEventArgs_t1401678908 * L_14 = (MessageEventArgs_t1401678908 *)il2cpp_codegen_object_new(MessageEventArgs_t1401678908_il2cpp_TypeInfo_var);
		MessageEventArgs__ctor_m4268379643(L_14, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		EventHandler_1_Invoke_m862903716(L_12, __this, L_14, /*hidden argument*/EventHandler_1_Invoke_m862903716_MethodInfo_var);
	}

IL_0054:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::Close()
extern "C"  void Client_Close_m3497347628 (Client_t931100087 * __this, const MethodInfo* method)
{
	{
		__this->set_retryConnectionCount_3(0);
		Client_closeHeartBeatTimer_m1939381365(__this, /*hidden argument*/NULL);
		Client_closeOutboundQueue_m72276279(__this, /*hidden argument*/NULL);
		Client_closeWebSocketClient_m254982728(__this, /*hidden argument*/NULL);
		RegistrationManager_t998967514 * L_0 = __this->get_registrationManager_8();
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		RegistrationManager_t998967514 * L_1 = __this->get_registrationManager_8();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(4 /* System.Void SocketIOClient.Eventing.RegistrationManager::Dispose() */, L_1);
		__this->set_registrationManager_8((RegistrationManager_t998967514 *)NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::closeHeartBeatTimer()
extern "C"  void Client_closeHeartBeatTimer_m1939381365 (Client_t931100087 * __this, const MethodInfo* method)
{
	{
		Timer_t3546110984 * L_0 = __this->get_socketHeartBeatTimer_0();
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		Timer_t3546110984 * L_1 = __this->get_socketHeartBeatTimer_0();
		NullCheck(L_1);
		Timer_Change_m2492385938(L_1, (-1), (-1), /*hidden argument*/NULL);
		Timer_t3546110984 * L_2 = __this->get_socketHeartBeatTimer_0();
		NullCheck(L_2);
		Timer_Dispose_m4229130271(L_2, /*hidden argument*/NULL);
		__this->set_socketHeartBeatTimer_0((Timer_t3546110984 *)NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::closeOutboundQueue()
extern "C"  void Client_closeOutboundQueue_m72276279 (Client_t931100087 * __this, const MethodInfo* method)
{
	{
		ConcurrentQueue_1_t702095371 * L_0 = __this->get_outboundQueue_2();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		__this->set_outboundQueue_2((ConcurrentQueue_1_t702095371 *)NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::closeWebSocketClient()
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_1_t419508266_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_1_t2171844441_il2cpp_TypeInfo_var;
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* Client_wsClient_Closed_m3514215282_MethodInfo_var;
extern const MethodInfo* Client_wsClient_MessageReceived_m182254307_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3500534924_MethodInfo_var;
extern const MethodInfo* Client_wsClient_Error_m50370738_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m3300027855_MethodInfo_var;
extern const MethodInfo* Client_wsClient_OpenEvent_m1278167232_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3804795280;
extern const uint32_t Client_closeWebSocketClient_m254982728_MetadataUsageId;
extern "C"  void Client_closeWebSocketClient_m254982728 (Client_t931100087 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_closeWebSocketClient_m254982728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		WebSocket_t713846903 * L_0 = __this->get_wsClient_7();
		if (!L_0)
		{
			goto IL_00af;
		}
	}
	{
		WebSocket_t713846903 * L_1 = __this->get_wsClient_7();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)Client_wsClient_Closed_m3514215282_MethodInfo_var);
		EventHandler_t247020293 * L_3 = (EventHandler_t247020293 *)il2cpp_codegen_object_new(EventHandler_t247020293_il2cpp_TypeInfo_var);
		EventHandler__ctor_m4096468317(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		WebSocket_remove_Closed_m3756708990(L_1, L_3, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_4 = __this->get_wsClient_7();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)Client_wsClient_MessageReceived_m182254307_MethodInfo_var);
		EventHandler_1_t419508266 * L_6 = (EventHandler_1_t419508266 *)il2cpp_codegen_object_new(EventHandler_1_t419508266_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m3500534924(L_6, __this, L_5, /*hidden argument*/EventHandler_1__ctor_m3500534924_MethodInfo_var);
		NullCheck(L_4);
		WebSocket_remove_MessageReceived_m4109017036(L_4, L_6, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_7 = __this->get_wsClient_7();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)Client_wsClient_Error_m50370738_MethodInfo_var);
		EventHandler_1_t2171844441 * L_9 = (EventHandler_1_t2171844441 *)il2cpp_codegen_object_new(EventHandler_1_t2171844441_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m3300027855(L_9, __this, L_8, /*hidden argument*/EventHandler_1__ctor_m3300027855_MethodInfo_var);
		NullCheck(L_7);
		WebSocket_remove_Error_m1728502727(L_7, L_9, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_10 = __this->get_wsClient_7();
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)Client_wsClient_OpenEvent_m1278167232_MethodInfo_var);
		EventHandler_t247020293 * L_12 = (EventHandler_t247020293 *)il2cpp_codegen_object_new(EventHandler_t247020293_il2cpp_TypeInfo_var);
		EventHandler__ctor_m4096468317(L_12, __this, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		WebSocket_remove_Opened_m2167545249(L_10, L_12, /*hidden argument*/NULL);
		WebSocket_t713846903 * L_13 = __this->get_wsClient_7();
		NullCheck(L_13);
		int32_t L_14 = WebSocket_get_State_m3481149631(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0088;
		}
	}
	{
		WebSocket_t713846903 * L_15 = __this->get_wsClient_7();
		NullCheck(L_15);
		int32_t L_16 = WebSocket_get_State_m3481149631(L_15, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_00a8;
		}
	}

IL_0088:
	try
	{ // begin try (depth: 1)
		WebSocket_t713846903 * L_17 = __this->get_wsClient_7();
		NullCheck(L_17);
		WebSocket_Close_m3186537590(L_17, /*hidden argument*/NULL);
		goto IL_00a8;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0098;
		throw e;
	}

CATCH_0098:
	{ // begin catch(System.Object)
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, _stringLiteral3804795280, /*hidden argument*/NULL);
		goto IL_00a8;
	} // end catch (depth: 1)

IL_00a8:
	{
		__this->set_wsClient_7((WebSocket_t713846903 *)NULL);
	}

IL_00af:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::wsClient_OpenEvent(System.Object,System.EventArgs)
extern TypeInfo* TimerCallback_t4291881837_il2cpp_TypeInfo_var;
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern TypeInfo* Timer_t3546110984_il2cpp_TypeInfo_var;
extern TypeInfo* EventMessage_t365350047_il2cpp_TypeInfo_var;
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* Client_OnHeartBeatTimerCallback_m3114059801_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3417674;
extern const uint32_t Client_wsClient_OpenEvent_m1278167232_MetadataUsageId;
extern "C"  void Client_wsClient_OpenEvent_m1278167232 (Client_t931100087 * __this, Il2CppObject * ___sender, EventArgs_t516466188 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_wsClient_OpenEvent_m1278167232_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	EventMessage_t365350047 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)Client_OnHeartBeatTimerCallback_m3114059801_MethodInfo_var);
		TimerCallback_t4291881837 * L_1 = (TimerCallback_t4291881837 *)il2cpp_codegen_object_new(TimerCallback_t4291881837_il2cpp_TypeInfo_var);
		TimerCallback__ctor_m1834223461(L_1, __this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_2, /*hidden argument*/NULL);
		SocketIOHandshake_t3315670474 * L_3 = VirtFuncInvoker0< SocketIOHandshake_t3315670474 * >::Invoke(12 /* SocketIOClient.SocketIOHandshake SocketIOClient.Client::get_HandShake() */, __this);
		NullCheck(L_3);
		TimeSpan_t763862892  L_4 = SocketIOHandshake_get_HeartbeatInterval_m4089311776(L_3, /*hidden argument*/NULL);
		SocketIOHandshake_t3315670474 * L_5 = VirtFuncInvoker0< SocketIOHandshake_t3315670474 * >::Invoke(12 /* SocketIOClient.SocketIOHandshake SocketIOClient.Client::get_HandShake() */, __this);
		NullCheck(L_5);
		TimeSpan_t763862892  L_6 = SocketIOHandshake_get_HeartbeatInterval_m4089311776(L_5, /*hidden argument*/NULL);
		Timer_t3546110984 * L_7 = (Timer_t3546110984 *)il2cpp_codegen_object_new(Timer_t3546110984_il2cpp_TypeInfo_var);
		Timer__ctor_m425594887(L_7, L_1, L_2, L_4, L_6, /*hidden argument*/NULL);
		__this->set_socketHeartBeatTimer_0(L_7);
		ManualResetEvent_t2399218676 * L_8 = __this->get_ConnectionOpenEvent_11();
		NullCheck(L_8);
		EventWaitHandle_Set_m224730030(L_8, /*hidden argument*/NULL);
		EventMessage_t365350047 * L_9 = (EventMessage_t365350047 *)il2cpp_codegen_object_new(EventMessage_t365350047_il2cpp_TypeInfo_var);
		EventMessage__ctor_m1254573078(L_9, /*hidden argument*/NULL);
		V_1 = L_9;
		EventMessage_t365350047 * L_10 = V_1;
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void SocketIOClient.Messages.Message::set_Event(System.String) */, L_10, _stringLiteral3417674);
		EventMessage_t365350047 * L_11 = V_1;
		Client_OnMessageEvent_m1312391754(__this, L_11, /*hidden argument*/NULL);
		EventHandler_t247020293 * L_12 = __this->get_Opened_13();
		if (!L_12)
		{
			goto IL_0083;
		}
	}

IL_0061:
	try
	{ // begin try (depth: 1)
		EventHandler_t247020293 * L_13 = __this->get_Opened_13();
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs_t516466188 * L_14 = ((EventArgs_t516466188_StaticFields*)EventArgs_t516466188_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck(L_13);
		EventHandler_Invoke_m1659461265(L_13, __this, L_14, /*hidden argument*/NULL);
		goto IL_0083;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0077;
		throw e;
	}

CATCH_0077:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t1967233988 *)__exception_local);
		Exception_t1967233988 * L_15 = V_0;
		Trace_WriteLine_m3524640299(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		goto IL_0083;
	} // end catch (depth: 1)

IL_0083:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::wsClient_MessageReceived(System.Object,WebSocket4Net.MessageReceivedEventArgs)
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern TypeInfo* IMessage_t3628529648_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1847535488;
extern Il2CppCodeGenString* _stringLiteral817926335;
extern Il2CppCodeGenString* _stringLiteral3332402801;
extern const uint32_t Client_wsClient_MessageReceived_m182254307_MetadataUsageId;
extern "C"  void Client_wsClient_MessageReceived_m182254307 (Client_t931100087 * __this, Il2CppObject * ___sender, MessageReceivedEventArgs_t3871897919 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_wsClient_MessageReceived_m182254307_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	{
		MessageReceivedEventArgs_t3871897919 * L_0 = ___e;
		NullCheck(L_0);
		String_t* L_1 = MessageReceivedEventArgs_get_Message_m1329338057(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = Message_Factory_m1793067991(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Il2CppObject * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String SocketIOClient.Messages.IMessage::get_Event() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, _stringLiteral1847535488, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		Il2CppObject * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String SocketIOClient.Messages.IMessage::get_RawMessage() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral817926335, L_7, /*hidden argument*/NULL);
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0036:
	{
		Il2CppObject * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* SocketIOClient.SocketIOMessageTypes SocketIOClient.Messages.IMessage::get_MessageType() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_9);
		V_1 = L_10;
		int32_t L_11 = V_1;
		if (L_11 == 0)
		{
			goto IL_0068;
		}
		if (L_11 == 1)
		{
			goto IL_0096;
		}
		if (L_11 == 2)
		{
			goto IL_008a;
		}
		if (L_11 == 3)
		{
			goto IL_0096;
		}
		if (L_11 == 4)
		{
			goto IL_0096;
		}
		if (L_11 == 5)
		{
			goto IL_0096;
		}
		if (L_11 == 6)
		{
			goto IL_00a2;
		}
		if (L_11 == 7)
		{
			goto IL_0096;
		}
	}
	{
		goto IL_00be;
	}

IL_0068:
	{
		Il2CppObject * L_12 = V_0;
		Client_OnMessageEvent_m1312391754(__this, L_12, /*hidden argument*/NULL);
		Il2CppObject * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = InterfaceFuncInvoker0< String_t* >::Invoke(4 /* System.String SocketIOClient.Messages.IMessage::get_Endpoint() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0085;
		}
	}
	{
		VirtActionInvoker0::Invoke(17 /* System.Void SocketIOClient.Client::Close() */, __this);
	}

IL_0085:
	{
		goto IL_00cd;
	}

IL_008a:
	{
		Client_OnHeartBeatTimerCallback_m3114059801(__this, NULL, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_0096:
	{
		Il2CppObject * L_16 = V_0;
		Client_OnMessageEvent_m1312391754(__this, L_16, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00a2:
	{
		RegistrationManager_t998967514 * L_17 = __this->get_registrationManager_8();
		Il2CppObject * L_18 = V_0;
		NullCheck(L_18);
		Nullable_1_t1438485399  L_19 = InterfaceFuncInvoker0< Nullable_1_t1438485399  >::Invoke(3 /* System.Nullable`1<System.Int32> SocketIOClient.Messages.IMessage::get_AckId() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_18);
		Il2CppObject * L_20 = V_0;
		NullCheck(L_20);
		JsonEncodedEventMessage_t989636229 * L_21 = InterfaceFuncInvoker0< JsonEncodedEventMessage_t989636229 * >::Invoke(7 /* SocketIOClient.Messages.JsonEncodedEventMessage SocketIOClient.Messages.IMessage::get_Json() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_20);
		NullCheck(L_17);
		RegistrationManager_InvokeCallBack_m1001874289(L_17, L_19, L_21, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00be:
	{
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, _stringLiteral3332402801, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00cd:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::wsClient_Closed(System.Object,System.EventArgs)
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t Client_wsClient_Closed_m3514215282_MetadataUsageId;
extern "C"  void Client_wsClient_Closed_m3514215282 (Client_t931100087 * __this, Il2CppObject * ___sender, EventArgs_t516466188 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_wsClient_Closed_m3514215282_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_retryConnectionCount_3();
		int32_t L_1 = Client_get_RetryConnectionAttempts_m14417765(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0028;
		}
	}
	{
		ManualResetEvent_t2399218676 * L_2 = __this->get_ConnectionOpenEvent_11();
		NullCheck(L_2);
		EventWaitHandle_Reset_m330657435(L_2, /*hidden argument*/NULL);
		Client_ReConnect_m3931142699(__this, /*hidden argument*/NULL);
		goto IL_003a;
	}

IL_0028:
	{
		VirtActionInvoker0::Invoke(17 /* System.Void SocketIOClient.Client::Close() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs_t516466188 * L_3 = ((EventArgs_t516466188_StaticFields*)EventArgs_t516466188_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		Client_OnSocketConnectionClosedEvent_m3101161164(__this, __this, L_3, /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::wsClient_Error(System.Object,SuperSocket.ClientEngine.ErrorEventArgs)
extern TypeInfo* ErrorEventArgs_t342512475_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral192819366;
extern const uint32_t Client_wsClient_Error_m50370738_MetadataUsageId;
extern "C"  void Client_wsClient_Error_m50370738 (Client_t931100087 * __this, Il2CppObject * ___sender, ErrorEventArgs_t1329266798 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_wsClient_Error_m50370738_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___sender;
		ErrorEventArgs_t1329266798 * L_1 = ___e;
		NullCheck(L_1);
		Exception_t1967233988 * L_2 = ErrorEventArgs_get_Exception_m2582201342(L_1, /*hidden argument*/NULL);
		ErrorEventArgs_t342512475 * L_3 = (ErrorEventArgs_t342512475 *)il2cpp_codegen_object_new(ErrorEventArgs_t342512475_il2cpp_TypeInfo_var);
		ErrorEventArgs__ctor_m2711369958(L_3, _stringLiteral192819366, L_2, /*hidden argument*/NULL);
		Client_OnErrorEvent_m447362248(__this, L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Client::OnErrorEvent(System.Object,SocketIOClient.ErrorEventArgs)
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m3621755301_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral55713979;
extern const uint32_t Client_OnErrorEvent_m447362248_MetadataUsageId;
extern "C"  void Client_OnErrorEvent_m447362248 (Client_t931100087 * __this, Il2CppObject * ___sender, ErrorEventArgs_t342512475 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_OnErrorEvent_m447362248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ErrorEventArgs_t342512475 * L_0 = ___e;
		NullCheck(L_0);
		String_t* L_1 = ErrorEventArgs_get_Message_m1223520785(L_0, /*hidden argument*/NULL);
		__this->set_LastErrorMessage_12(L_1);
		EventHandler_1_t1185090118 * L_2 = __this->get_Error_18();
		if (!L_2)
		{
			goto IL_002f;
		}
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t1185090118 * L_3 = __this->get_Error_18();
		ErrorEventArgs_t342512475 * L_4 = ___e;
		NullCheck(L_3);
		EventHandler_1_Invoke_m3621755301(L_3, __this, L_4, /*hidden argument*/EventHandler_1_Invoke_m3621755301_MethodInfo_var);
		goto IL_002f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0029;
		throw e;
	}

CATCH_0029:
	{ // begin catch(System.Object)
		goto IL_002f;
	} // end catch (depth: 1)

IL_002f:
	{
		ErrorEventArgs_t342512475 * L_5 = ___e;
		NullCheck(L_5);
		String_t* L_6 = ErrorEventArgs_get_Message_m1223520785(L_5, /*hidden argument*/NULL);
		ErrorEventArgs_t342512475 * L_7 = ___e;
		NullCheck(L_7);
		Exception_t1967233988 * L_8 = ErrorEventArgs_get_Exception_m4262431971(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral55713979, L_6, L_8, /*hidden argument*/NULL);
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Client::OnSocketConnectionClosedEvent(System.Object,System.EventArgs)
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1278788861;
extern const uint32_t Client_OnSocketConnectionClosedEvent_m3101161164_MetadataUsageId;
extern "C"  void Client_OnSocketConnectionClosedEvent_m3101161164 (Client_t931100087 * __this, Il2CppObject * ___sender, EventArgs_t516466188 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_OnSocketConnectionClosedEvent_m3101161164_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		EventHandler_t247020293 * L_0 = __this->get_SocketConnectionClosed_17();
		if (!L_0)
		{
			goto IL_0023;
		}
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		EventHandler_t247020293 * L_1 = __this->get_SocketConnectionClosed_17();
		Il2CppObject * L_2 = ___sender;
		EventArgs_t516466188 * L_3 = ___e;
		NullCheck(L_1);
		EventHandler_Invoke_m1659461265(L_1, L_2, L_3, /*hidden argument*/NULL);
		goto IL_0023;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_001d:
	{ // begin catch(System.Object)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, _stringLiteral1278788861, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Client::OnConnectionRetryAttemptEvent(System.Object,System.EventArgs)
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2338129189;
extern const uint32_t Client_OnConnectionRetryAttemptEvent_m3751111942_MetadataUsageId;
extern "C"  void Client_OnConnectionRetryAttemptEvent_m3751111942 (Client_t931100087 * __this, Il2CppObject * ___sender, EventArgs_t516466188 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_OnConnectionRetryAttemptEvent_m3751111942_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		EventHandler_t247020293 * L_0 = __this->get_ConnectionRetryAttempt_15();
		if (!L_0)
		{
			goto IL_0029;
		}
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		EventHandler_t247020293 * L_1 = __this->get_ConnectionRetryAttempt_15();
		Il2CppObject * L_2 = ___sender;
		EventArgs_t516466188 * L_3 = ___e;
		NullCheck(L_1);
		EventHandler_Invoke_m1659461265(L_1, L_2, L_3, /*hidden argument*/NULL);
		goto IL_0029;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_001d:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t1967233988 *)__exception_local);
		Exception_t1967233988 * L_4 = V_0;
		Trace_WriteLine_m3524640299(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0029;
	} // end catch (depth: 1)

IL_0029:
	{
		int32_t L_5 = __this->get_retryConnectionCount_3();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral2338129189, L_7, /*hidden argument*/NULL);
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Client::OnHeartBeatTimerCallback(System.Object)
extern TypeInfo* Heartbeat_t542713166_il2cpp_TypeInfo_var;
extern TypeInfo* IMessage_t3628529648_il2cpp_TypeInfo_var;
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern TypeInfo* AsyncCallback_t1363551830_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* ConcurrentQueue_1_Enqueue_m1527058932_MethodInfo_var;
extern const MethodInfo* Client_EndAsyncEvent_m2464837398_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2326578798;
extern const uint32_t Client_OnHeartBeatTimerCallback_m3114059801_MetadataUsageId;
extern "C"  void Client_OnHeartBeatTimerCallback_m3114059801 (Client_t931100087 * __this, Il2CppObject * ___state, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_OnHeartBeatTimerCallback_m3114059801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(14 /* WebSocket4Net.WebSocketState SocketIOClient.Client::get_ReadyState() */, __this);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_007e;
		}
	}
	{
		Heartbeat_t542713166 * L_1 = (Heartbeat_t542713166 *)il2cpp_codegen_object_new(Heartbeat_t542713166_il2cpp_TypeInfo_var);
		Heartbeat__ctor_m2318316039(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			ConcurrentQueue_1_t702095371 * L_2 = __this->get_outboundQueue_2();
			if (!L_2)
			{
				goto IL_0058;
			}
		}

IL_001d:
		{
			ConcurrentQueue_1_t702095371 * L_3 = __this->get_outboundQueue_2();
			Il2CppObject * L_4 = V_0;
			NullCheck(L_4);
			String_t* L_5 = InterfaceFuncInvoker0< String_t* >::Invoke(9 /* System.String SocketIOClient.Messages.IMessage::get_Encoded() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_4);
			NullCheck(L_3);
			ConcurrentQueue_1_Enqueue_m1527058932(L_3, L_5, /*hidden argument*/ConcurrentQueue_1_Enqueue_m1527058932_MethodInfo_var);
			EventHandler_t247020293 * L_6 = __this->get_HeartBeatTimerEvent_16();
			if (!L_6)
			{
				goto IL_0058;
			}
		}

IL_0039:
		{
			EventHandler_t247020293 * L_7 = __this->get_HeartBeatTimerEvent_16();
			IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
			EventArgs_t516466188 * L_8 = ((EventArgs_t516466188_StaticFields*)EventArgs_t516466188_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
			IntPtr_t L_9;
			L_9.set_m_value_0((void*)Client_EndAsyncEvent_m2464837398_MethodInfo_var);
			AsyncCallback_t1363551830 * L_10 = (AsyncCallback_t1363551830 *)il2cpp_codegen_object_new(AsyncCallback_t1363551830_il2cpp_TypeInfo_var);
			AsyncCallback__ctor_m1986959762(L_10, __this, L_9, /*hidden argument*/NULL);
			NullCheck(L_7);
			EventHandler_BeginInvoke_m76205456(L_7, __this, L_8, L_10, NULL, /*hidden argument*/NULL);
		}

IL_0058:
		{
			goto IL_007e;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005d;
		throw e;
	}

CATCH_005d:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t1967233988 *)__exception_local);
		Exception_t1967233988 * L_11 = V_1;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_11);
		Exception_t1967233988 * L_13 = V_1;
		NullCheck(L_13);
		Exception_t1967233988 * L_14 = VirtFuncInvoker0< Exception_t1967233988 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2326578798, L_12, L_14, /*hidden argument*/NULL);
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		goto IL_007e;
	} // end catch (depth: 1)

IL_007e:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::EndAsyncEvent(System.IAsyncResult)
extern TypeInfo* AsyncResult_t477528403_il2cpp_TypeInfo_var;
extern TypeInfo* EventHandler_t247020293_il2cpp_TypeInfo_var;
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4164049287;
extern const uint32_t Client_EndAsyncEvent_m2464837398_MetadataUsageId;
extern "C"  void Client_EndAsyncEvent_m2464837398 (Client_t931100087 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_EndAsyncEvent_m2464837398_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AsyncResult_t477528403 * V_0 = NULL;
	EventHandler_t247020293 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___result;
		V_0 = ((AsyncResult_t477528403 *)CastclassClass(L_0, AsyncResult_t477528403_il2cpp_TypeInfo_var));
		AsyncResult_t477528403 * L_1 = V_0;
		NullCheck(L_1);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Object System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncDelegate() */, L_1);
		V_1 = ((EventHandler_t247020293 *)CastclassSealed(L_2, EventHandler_t247020293_il2cpp_TypeInfo_var));
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		EventHandler_t247020293 * L_3 = V_1;
		Il2CppObject * L_4 = ___result;
		NullCheck(L_3);
		EventHandler_EndInvoke_m2432290285(L_3, L_4, /*hidden argument*/NULL);
		goto IL_002f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.Object)
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, _stringLiteral4164049287, /*hidden argument*/NULL);
		goto IL_002f;
	} // end catch (depth: 1)

IL_002f:
	{
		return;
	}
}
// System.Void SocketIOClient.Client::dequeuOutboundMessages()
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* ConcurrentQueue_1_TryDequeue_m3652952203_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral274779812;
extern const uint32_t Client_dequeuOutboundMessages_m370415647_MetadataUsageId;
extern "C"  void Client_dequeuOutboundMessages_m370415647 (Client_t931100087 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_dequeuOutboundMessages_m370415647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		goto IL_006b;
	}

IL_0005:
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(14 /* WebSocket4Net.WebSocketState SocketIOClient.Client::get_ReadyState() */, __this);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_005a;
		}
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			ConcurrentQueue_1_t702095371 * L_1 = __this->get_outboundQueue_2();
			NullCheck(L_1);
			bool L_2 = ConcurrentQueue_1_TryDequeue_m3652952203(L_1, (&V_0), /*hidden argument*/ConcurrentQueue_1_TryDequeue_m3652952203_MethodInfo_var);
			if (!L_2)
			{
				goto IL_0034;
			}
		}

IL_0023:
		{
			WebSocket_t713846903 * L_3 = __this->get_wsClient_7();
			String_t* L_4 = V_0;
			NullCheck(L_3);
			WebSocket_Send_m1750322966(L_3, L_4, /*hidden argument*/NULL);
			goto IL_0040;
		}

IL_0034:
		{
			ManualResetEvent_t2399218676 * L_5 = __this->get_MessageQueueEmptyEvent_10();
			NullCheck(L_5);
			EventWaitHandle_Set_m224730030(L_5, /*hidden argument*/NULL);
		}

IL_0040:
		{
			goto IL_0055;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0045;
		throw e;
	}

CATCH_0045:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t1967233988 *)__exception_local);
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, _stringLiteral274779812, /*hidden argument*/NULL);
		goto IL_0055;
	} // end catch (depth: 1)

IL_0055:
	{
		goto IL_006b;
	}

IL_005a:
	{
		ManualResetEvent_t2399218676 * L_6 = __this->get_ConnectionOpenEvent_11();
		NullCheck(L_6);
		VirtFuncInvoker1< bool, int32_t >::Invoke(11 /* System.Boolean System.Threading.WaitHandle::WaitOne(System.Int32) */, L_6, ((int32_t)2000));
	}

IL_006b:
	{
		ConcurrentQueue_1_t702095371 * L_7 = __this->get_outboundQueue_2();
		if (L_7)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// SocketIOClient.SocketIOHandshake SocketIOClient.Client::requestHandshake(System.Uri)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WebClient_t4210428809_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern TypeInfo* SocketIOHandshake_t3315670474_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2903159051;
extern Il2CppCodeGenString* _stringLiteral3413691806;
extern Il2CppCodeGenString* _stringLiteral3011958741;
extern const uint32_t Client_requestHandshake_m1889130752_MetadataUsageId;
extern "C"  SocketIOHandshake_t3315670474 * Client_requestHandshake_m1889130752 (Client_t931100087 * __this, Uri_t2776692961 * ___uri, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Client_requestHandshake_m1889130752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	SocketIOHandshake_t3315670474 * V_2 = NULL;
	WebClient_t4210428809 * V_3 = NULL;
	Exception_t1967233988 * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_1 = L_1;
		V_2 = (SocketIOHandshake_t3315670474 *)NULL;
		WebClient_t4210428809 * L_2 = (WebClient_t4210428809 *)il2cpp_codegen_object_new(WebClient_t4210428809_il2cpp_TypeInfo_var);
		WebClient__ctor_m2477960557(L_2, /*hidden argument*/NULL);
		V_3 = L_2;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				WebClient_t4210428809 * L_3 = V_3;
				ObjectU5BU5D_t11523773* L_4 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
				Uri_t2776692961 * L_5 = ___uri;
				NullCheck(L_5);
				String_t* L_6 = Uri_get_Scheme_m2606456870(L_5, /*hidden argument*/NULL);
				NullCheck(L_4);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
				ArrayElementTypeCheck (L_4, L_6);
				(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_6);
				ObjectU5BU5D_t11523773* L_7 = L_4;
				Uri_t2776692961 * L_8 = ___uri;
				NullCheck(L_8);
				String_t* L_9 = Uri_get_Host_m1446697833(L_8, /*hidden argument*/NULL);
				NullCheck(L_7);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
				ArrayElementTypeCheck (L_7, L_9);
				(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
				ObjectU5BU5D_t11523773* L_10 = L_7;
				Uri_t2776692961 * L_11 = ___uri;
				NullCheck(L_11);
				int32_t L_12 = Uri_get_Port_m2253782543(L_11, /*hidden argument*/NULL);
				int32_t L_13 = L_12;
				Il2CppObject * L_14 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_13);
				NullCheck(L_10);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
				ArrayElementTypeCheck (L_10, L_14);
				(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_14);
				ObjectU5BU5D_t11523773* L_15 = L_10;
				Uri_t2776692961 * L_16 = ___uri;
				NullCheck(L_16);
				String_t* L_17 = Uri_get_Query_m1454422281(L_16, /*hidden argument*/NULL);
				NullCheck(L_15);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 3);
				ArrayElementTypeCheck (L_15, L_17);
				(L_15)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_17);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_18 = String_Format_m4050103162(NULL /*static, unused*/, _stringLiteral2903159051, L_15, /*hidden argument*/NULL);
				NullCheck(L_3);
				String_t* L_19 = WebClient_DownloadString_m2198465167(L_3, L_18, /*hidden argument*/NULL);
				V_0 = L_19;
				String_t* L_20 = V_0;
				bool L_21 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
				if (!L_21)
				{
					goto IL_0065;
				}
			}

IL_005f:
			{
				V_1 = _stringLiteral3413691806;
			}

IL_0065:
			{
				goto IL_0083;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_006a;
			throw e;
		}

CATCH_006a:
		{ // begin catch(System.Exception)
			V_4 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_22 = V_4;
			NullCheck(L_22);
			String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_22);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_24 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3011958741, L_23, /*hidden argument*/NULL);
			V_1 = L_24;
			goto IL_0083;
		} // end catch (depth: 2)

IL_0083:
		{
			IL2CPP_LEAVE(0x95, FINALLY_0088);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0088;
	}

FINALLY_0088:
	{ // begin finally (depth: 1)
		{
			WebClient_t4210428809 * L_25 = V_3;
			if (!L_25)
			{
				goto IL_0094;
			}
		}

IL_008e:
		{
			WebClient_t4210428809 * L_26 = V_3;
			NullCheck(L_26);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_26);
		}

IL_0094:
		{
			IL2CPP_END_FINALLY(136)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(136)
	{
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0095:
	{
		String_t* L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_28 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00ac;
		}
	}
	{
		String_t* L_29 = V_0;
		SocketIOHandshake_t3315670474 * L_30 = SocketIOHandshake_LoadFromString_m766272635(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		V_2 = L_30;
		goto IL_00b9;
	}

IL_00ac:
	{
		SocketIOHandshake_t3315670474 * L_31 = (SocketIOHandshake_t3315670474 *)il2cpp_codegen_object_new(SocketIOHandshake_t3315670474_il2cpp_TypeInfo_var);
		SocketIOHandshake__ctor_m2023930727(L_31, /*hidden argument*/NULL);
		V_2 = L_31;
		SocketIOHandshake_t3315670474 * L_32 = V_2;
		String_t* L_33 = V_1;
		NullCheck(L_32);
		SocketIOHandshake_set_ErrorMessage_m2459766281(L_32, L_33, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		SocketIOHandshake_t3315670474 * L_34 = V_2;
		return L_34;
	}
}
// System.Void SocketIOClient.Client::Dispose()
extern "C"  void Client_Dispose_m3021382611 (Client_t931100087 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(27 /* System.Void SocketIOClient.Client::Dispose(System.Boolean) */, __this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Client::Dispose(System.Boolean)
extern "C"  void Client_Dispose_m2625416202 (Client_t931100087 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		bool L_0 = ___disposing;
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		VirtActionInvoker0::Invoke(17 /* System.Void SocketIOClient.Client::Close() */, __this);
		ManualResetEvent_t2399218676 * L_1 = __this->get_MessageQueueEmptyEvent_10();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(5 /* System.Void System.Threading.WaitHandle::Close() */, L_1);
		ManualResetEvent_t2399218676 * L_2 = __this->get_ConnectionOpenEvent_11();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(5 /* System.Void System.Threading.WaitHandle::Close() */, L_2);
	}

IL_0022:
	{
		return;
	}
}
// System.Void SocketIOClient.EndPointClient::.ctor(SocketIOClient.IClient,System.String)
extern "C"  void EndPointClient__ctor_m889041267 (EndPointClient_t1953350540 * __this, Il2CppObject * ___client, String_t* ___endPoint, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___endPoint;
		EndPointClient_validateNameSpace_m1391818974(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___client;
		EndPointClient_set_Client_m3866586145(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___endPoint;
		EndPointClient_set_EndPoint_m3833550509(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// SocketIOClient.IClient SocketIOClient.EndPointClient::get_Client()
extern "C"  Il2CppObject * EndPointClient_get_Client_m4232033828 (EndPointClient_t1953350540 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CClientU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void SocketIOClient.EndPointClient::set_Client(SocketIOClient.IClient)
extern "C"  void EndPointClient_set_Client_m3866586145 (EndPointClient_t1953350540 * __this, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value;
		__this->set_U3CClientU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String SocketIOClient.EndPointClient::get_EndPoint()
extern "C"  String_t* EndPointClient_get_EndPoint_m3940392382 (EndPointClient_t1953350540 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CEndPointU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SocketIOClient.EndPointClient::set_EndPoint(System.String)
extern "C"  void EndPointClient_set_EndPoint_m3833550509 (EndPointClient_t1953350540 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CEndPointU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void SocketIOClient.EndPointClient::validateNameSpace(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Contains_TisChar_t2778706699_m1628586787_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1222665531;
extern Il2CppCodeGenString* _stringLiteral3907186334;
extern Il2CppCodeGenString* _stringLiteral1803571880;
extern const uint32_t EndPointClient_validateNameSpace_m1391818974_MetadataUsageId;
extern "C"  void EndPointClient_validateNameSpace_m1391818974 (EndPointClient_t1953350540 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EndPointClient_validateNameSpace_m1391818974_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_2 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3341069848(L_2, _stringLiteral1222665531, _stringLiteral3907186334, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001b:
	{
		String_t* L_3 = ___name;
		bool L_4 = Enumerable_Contains_TisChar_t2778706699_m1628586787(NULL /*static, unused*/, L_3, ((int32_t)58), /*hidden argument*/Enumerable_Contains_TisChar_t2778706699_m1628586787_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		ArgumentException_t124305799 * L_5 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_5, _stringLiteral1803571880, _stringLiteral1222665531, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0038:
	{
		return;
	}
}
// System.Void SocketIOClient.EndPointClient::On(System.String,System.Action`1<SocketIOClient.Messages.IMessage>)
extern TypeInfo* IClient_t1294359360_il2cpp_TypeInfo_var;
extern const uint32_t EndPointClient_On_m300133466_MetadataUsageId;
extern "C"  void EndPointClient_On_m300133466 (EndPointClient_t1953350540 * __this, String_t* ___eventName, Action_1_t3776982353 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EndPointClient_On_m300133466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = EndPointClient_get_Client_m4232033828(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___eventName;
		String_t* L_2 = EndPointClient_get_EndPoint_m3940392382(__this, /*hidden argument*/NULL);
		Action_1_t3776982353 * L_3 = ___action;
		NullCheck(L_0);
		InterfaceActionInvoker3< String_t*, String_t*, Action_1_t3776982353 * >::Invoke(16 /* System.Void SocketIOClient.IClient::On(System.String,System.String,System.Action`1<SocketIOClient.Messages.IMessage>) */, IClient_t1294359360_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3);
		return;
	}
}
// System.Void SocketIOClient.EndPointClient::Emit(System.String,System.Object,System.Action`1<System.Object>)
extern TypeInfo* IClient_t1294359360_il2cpp_TypeInfo_var;
extern const uint32_t EndPointClient_Emit_m1168915670_MetadataUsageId;
extern "C"  void EndPointClient_Emit_m1168915670 (EndPointClient_t1953350540 * __this, String_t* ___eventName, Il2CppObject * ___payload, Action_1_t985559125 * ___callBack, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EndPointClient_Emit_m1168915670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = EndPointClient_get_Client_m4232033828(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___eventName;
		Il2CppObject * L_2 = ___payload;
		String_t* L_3 = EndPointClient_get_EndPoint_m3940392382(__this, /*hidden argument*/NULL);
		Action_1_t985559125 * L_4 = ___callBack;
		NullCheck(L_0);
		InterfaceActionInvoker4< String_t*, Il2CppObject *, String_t*, Action_1_t985559125 * >::Invoke(18 /* System.Void SocketIOClient.IClient::Emit(System.String,System.Object,System.String,System.Action`1<System.Object>) */, IClient_t1294359360_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3, L_4);
		return;
	}
}
// System.Void SocketIOClient.EndPointClient::Send(SocketIOClient.Messages.IMessage)
extern TypeInfo* IMessage_t3628529648_il2cpp_TypeInfo_var;
extern TypeInfo* IClient_t1294359360_il2cpp_TypeInfo_var;
extern const uint32_t EndPointClient_Send_m3396223253_MetadataUsageId;
extern "C"  void EndPointClient_Send_m3396223253 (EndPointClient_t1953350540 * __this, Il2CppObject * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EndPointClient_Send_m3396223253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___msg;
		String_t* L_1 = EndPointClient_get_EndPoint_m3940392382(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(5 /* System.Void SocketIOClient.Messages.IMessage::set_Endpoint(System.String) */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_0, L_1);
		Il2CppObject * L_2 = EndPointClient_get_Client_m4232033828(__this, /*hidden argument*/NULL);
		Il2CppObject * L_3 = ___msg;
		NullCheck(L_2);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(19 /* System.Void SocketIOClient.IClient::Send(SocketIOClient.Messages.IMessage) */, IClient_t1294359360_il2cpp_TypeInfo_var, L_2, L_3);
		return;
	}
}
// System.Void SocketIOClient.ErrorEventArgs::.ctor(System.String)
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t ErrorEventArgs__ctor_m2633561680_MetadataUsageId;
extern "C"  void ErrorEventArgs__ctor_m2633561680 (ErrorEventArgs_t342512475 * __this, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ErrorEventArgs__ctor_m2633561680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1904770202(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message;
		ErrorEventArgs_set_Message_m1564870344(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.ErrorEventArgs::.ctor(System.String,System.Exception)
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t ErrorEventArgs__ctor_m2711369958_MetadataUsageId;
extern "C"  void ErrorEventArgs__ctor_m2711369958 (ErrorEventArgs_t342512475 * __this, String_t* ___message, Exception_t1967233988 * ___exception, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ErrorEventArgs__ctor_m2711369958_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1904770202(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message;
		ErrorEventArgs_set_Message_m1564870344(__this, L_0, /*hidden argument*/NULL);
		Exception_t1967233988 * L_1 = ___exception;
		ErrorEventArgs_set_Exception_m315626738(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String SocketIOClient.ErrorEventArgs::get_Message()
extern "C"  String_t* ErrorEventArgs_get_Message_m1223520785 (ErrorEventArgs_t342512475 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CMessageU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SocketIOClient.ErrorEventArgs::set_Message(System.String)
extern "C"  void ErrorEventArgs_set_Message_m1564870344 (ErrorEventArgs_t342512475 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CMessageU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Exception SocketIOClient.ErrorEventArgs::get_Exception()
extern "C"  Exception_t1967233988 * ErrorEventArgs_get_Exception_m4262431971 (ErrorEventArgs_t342512475 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = __this->get_U3CExceptionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SocketIOClient.ErrorEventArgs::set_Exception(System.Exception)
extern "C"  void ErrorEventArgs_set_Exception_m315626738 (ErrorEventArgs_t342512475 * __this, Exception_t1967233988 * ___value, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___value;
		__this->set_U3CExceptionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void SocketIOClient.Eventing.RegistrationManager::.ctor()
extern TypeInfo* Dictionary_2_t1476369908_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1119712961_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1906966347_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1865686094_MethodInfo_var;
extern const uint32_t RegistrationManager__ctor_m1057237267_MetadataUsageId;
extern "C"  void RegistrationManager__ctor_m1057237267 (RegistrationManager_t998967514 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RegistrationManager__ctor_m1057237267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Dictionary_2_t1476369908 * L_0 = (Dictionary_2_t1476369908 *)il2cpp_codegen_object_new(Dictionary_2_t1476369908_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1906966347(L_0, /*hidden argument*/Dictionary_2__ctor_m1906966347_MethodInfo_var);
		__this->set_callBackRegistry_0(L_0);
		Dictionary_2_t1119712961 * L_1 = (Dictionary_2_t1119712961 *)il2cpp_codegen_object_new(Dictionary_2_t1119712961_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1865686094(L_1, /*hidden argument*/Dictionary_2__ctor_m1865686094_MethodInfo_var);
		__this->set_eventNameRegistry_1(L_1);
		return;
	}
}
// System.Void SocketIOClient.Eventing.RegistrationManager::AddCallBack(SocketIOClient.Messages.IMessage)
extern TypeInfo* EventMessage_t365350047_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3808413405_MethodInfo_var;
extern const uint32_t RegistrationManager_AddCallBack_m2141734945_MetadataUsageId;
extern "C"  void RegistrationManager_AddCallBack_m2141734945 (RegistrationManager_t998967514 * __this, Il2CppObject * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RegistrationManager_AddCallBack_m2141734945_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EventMessage_t365350047 * V_0 = NULL;
	Nullable_1_t1438485399  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = ___message;
		V_0 = ((EventMessage_t365350047 *)IsInstClass(L_0, EventMessage_t365350047_il2cpp_TypeInfo_var));
		EventMessage_t365350047 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Dictionary_2_t1476369908 * L_2 = __this->get_callBackRegistry_0();
		EventMessage_t365350047 * L_3 = V_0;
		NullCheck(L_3);
		Nullable_1_t1438485399  L_4 = VirtFuncInvoker0< Nullable_1_t1438485399  >::Invoke(7 /* System.Nullable`1<System.Int32> SocketIOClient.Messages.Message::get_AckId() */, L_3);
		V_1 = L_4;
		int32_t L_5 = Nullable_1_get_Value_m3808413405((&V_1), /*hidden argument*/Nullable_1_get_Value_m3808413405_MethodInfo_var);
		EventMessage_t365350047 * L_6 = V_0;
		NullCheck(L_6);
		Action_1_t985559125 * L_7 = L_6->get_Callback_12();
		NullCheck(L_2);
		VirtActionInvoker2< int32_t, Action_1_t985559125 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::Add(!0,!1) */, L_2, L_5, L_7);
	}

IL_002c:
	{
		return;
	}
}
// System.Void SocketIOClient.Eventing.RegistrationManager::AddCallBack(System.Int32,System.Action`1<System.Object>)
extern "C"  void RegistrationManager_AddCallBack_m2506737832 (RegistrationManager_t998967514 * __this, int32_t ___ackId, Action_1_t985559125 * ___callback, const MethodInfo* method)
{
	{
		Dictionary_2_t1476369908 * L_0 = __this->get_callBackRegistry_0();
		int32_t L_1 = ___ackId;
		Action_1_t985559125 * L_2 = ___callback;
		NullCheck(L_0);
		VirtActionInvoker2< int32_t, Action_1_t985559125 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::Add(!0,!1) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void SocketIOClient.Eventing.RegistrationManager::InvokeCallBack(System.Nullable`1<System.Int32>,System.String)
extern TypeInfo* AsyncCallback_t1363551830_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m404638567_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3808413405_MethodInfo_var;
extern const MethodInfo* Action_1_EndInvoke_m3557179757_MethodInfo_var;
extern const MethodInfo* Action_1_BeginInvoke_m2409567265_MethodInfo_var;
extern const uint32_t RegistrationManager_InvokeCallBack_m4077058364_MetadataUsageId;
extern "C"  void RegistrationManager_InvokeCallBack_m4077058364 (RegistrationManager_t998967514 * __this, Nullable_1_t1438485399  ___ackId, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RegistrationManager_InvokeCallBack_m4077058364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t985559125 * V_0 = NULL;
	{
		V_0 = (Action_1_t985559125 *)NULL;
		bool L_0 = Nullable_1_get_HasValue_m404638567((&___ackId), /*hidden argument*/Nullable_1_get_HasValue_m404638567_MethodInfo_var);
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		Dictionary_2_t1476369908 * L_1 = __this->get_callBackRegistry_0();
		int32_t L_2 = Nullable_1_get_Value_m3808413405((&___ackId), /*hidden argument*/Nullable_1_get_Value_m3808413405_MethodInfo_var);
		NullCheck(L_1);
		bool L_3 = VirtFuncInvoker2< bool, int32_t, Action_1_t985559125 ** >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::TryGetValue(!0,!1&) */, L_1, L_2, (&V_0));
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		Action_1_t985559125 * L_4 = V_0;
		String_t* L_5 = ___value;
		Action_1_t985559125 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)Action_1_EndInvoke_m3557179757_MethodInfo_var);
		AsyncCallback_t1363551830 * L_8 = (AsyncCallback_t1363551830 *)il2cpp_codegen_object_new(AsyncCallback_t1363551830_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_8, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Action_1_BeginInvoke_m2409567265(L_4, L_5, L_8, NULL, /*hidden argument*/Action_1_BeginInvoke_m2409567265_MethodInfo_var);
	}

IL_003c:
	{
		return;
	}
}
// System.Void SocketIOClient.Eventing.RegistrationManager::InvokeCallBack(System.Nullable`1<System.Int32>,SocketIOClient.Messages.JsonEncodedEventMessage)
extern const MethodInfo* Nullable_1_get_HasValue_m404638567_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3808413405_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m1684751784_MethodInfo_var;
extern const uint32_t RegistrationManager_InvokeCallBack_m1001874289_MetadataUsageId;
extern "C"  void RegistrationManager_InvokeCallBack_m1001874289 (RegistrationManager_t998967514 * __this, Nullable_1_t1438485399  ___ackId, JsonEncodedEventMessage_t989636229 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RegistrationManager_InvokeCallBack_m1001874289_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t985559125 * V_0 = NULL;
	{
		V_0 = (Action_1_t985559125 *)NULL;
		bool L_0 = Nullable_1_get_HasValue_m404638567((&___ackId), /*hidden argument*/Nullable_1_get_HasValue_m404638567_MethodInfo_var);
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Dictionary_2_t1476369908 * L_1 = __this->get_callBackRegistry_0();
		int32_t L_2 = Nullable_1_get_Value_m3808413405((&___ackId), /*hidden argument*/Nullable_1_get_Value_m3808413405_MethodInfo_var);
		NullCheck(L_1);
		bool L_3 = VirtFuncInvoker2< bool, int32_t, Action_1_t985559125 ** >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::TryGetValue(!0,!1&) */, L_1, L_2, (&V_0));
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		Action_1_t985559125 * L_4 = V_0;
		JsonEncodedEventMessage_t989636229 * L_5 = ___value;
		NullCheck(L_4);
		Action_1_Invoke_m1684751784(L_4, L_5, /*hidden argument*/Action_1_Invoke_m1684751784_MethodInfo_var);
	}

IL_002e:
	{
		return;
	}
}
// System.Void SocketIOClient.Eventing.RegistrationManager::AddOnEvent(System.String,System.Action`1<SocketIOClient.Messages.IMessage>)
extern "C"  void RegistrationManager_AddOnEvent_m1569321649 (RegistrationManager_t998967514 * __this, String_t* ___eventName, Action_1_t3776982353 * ___callback, const MethodInfo* method)
{
	{
		Dictionary_2_t1119712961 * L_0 = __this->get_eventNameRegistry_1();
		String_t* L_1 = ___eventName;
		Action_1_t3776982353 * L_2 = ___callback;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Action_1_t3776982353 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::Add(!0,!1) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void SocketIOClient.Eventing.RegistrationManager::AddOnEvent(System.String,System.String,System.Action`1<SocketIOClient.Messages.IMessage>)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2901865951;
extern const uint32_t RegistrationManager_AddOnEvent_m2206615469_MetadataUsageId;
extern "C"  void RegistrationManager_AddOnEvent_m2206615469 (RegistrationManager_t998967514 * __this, String_t* ___eventName, String_t* ___endPoint, Action_1_t3776982353 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RegistrationManager_AddOnEvent_m2206615469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1119712961 * L_0 = __this->get_eventNameRegistry_1();
		String_t* L_1 = ___eventName;
		String_t* L_2 = ___endPoint;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2901865951, L_1, L_2, /*hidden argument*/NULL);
		Action_1_t3776982353 * L_4 = ___callback;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Action_1_t3776982353 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::Add(!0,!1) */, L_0, L_3, L_4);
		return;
	}
}
// System.Boolean SocketIOClient.Eventing.RegistrationManager::InvokeOnEvent(SocketIOClient.Messages.IMessage)
extern TypeInfo* IMessage_t3628529648_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2196250196_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2901865951;
extern Il2CppCodeGenString* _stringLiteral2130388473;
extern const uint32_t RegistrationManager_InvokeOnEvent_m2394835410_MetadataUsageId;
extern "C"  bool RegistrationManager_InvokeOnEvent_m2394835410 (RegistrationManager_t998967514 * __this, Il2CppObject * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RegistrationManager_InvokeOnEvent_m2394835410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Action_1_t3776982353 * V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)0;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___value;
			NullCheck(L_0);
			String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String SocketIOClient.Messages.IMessage::get_Event() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_0);
			V_2 = L_1;
			Il2CppObject * L_2 = ___value;
			NullCheck(L_2);
			String_t* L_3 = InterfaceFuncInvoker0< String_t* >::Invoke(4 /* System.String SocketIOClient.Messages.IMessage::get_Endpoint() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_2);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_4 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_0030;
			}
		}

IL_0019:
		{
			Il2CppObject * L_5 = ___value;
			NullCheck(L_5);
			String_t* L_6 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String SocketIOClient.Messages.IMessage::get_Event() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_5);
			Il2CppObject * L_7 = ___value;
			NullCheck(L_7);
			String_t* L_8 = InterfaceFuncInvoker0< String_t* >::Invoke(4 /* System.String SocketIOClient.Messages.IMessage::get_Endpoint() */, IMessage_t3628529648_il2cpp_TypeInfo_var, L_7);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_9 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2901865951, L_6, L_8, /*hidden argument*/NULL);
			V_2 = L_9;
		}

IL_0030:
		{
			Dictionary_2_t1119712961 * L_10 = __this->get_eventNameRegistry_1();
			String_t* L_11 = V_2;
			NullCheck(L_10);
			bool L_12 = VirtFuncInvoker2< bool, String_t*, Action_1_t3776982353 ** >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::TryGetValue(!0,!1&) */, L_10, L_11, (&V_1));
			if (!L_12)
			{
				goto IL_004c;
			}
		}

IL_0043:
		{
			V_0 = (bool)1;
			Action_1_t3776982353 * L_13 = V_1;
			Il2CppObject * L_14 = ___value;
			NullCheck(L_13);
			Action_1_Invoke_m2196250196(L_13, L_14, /*hidden argument*/Action_1_Invoke_m2196250196_MethodInfo_var);
		}

IL_004c:
		{
			goto IL_006c;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0051;
		throw e;
	}

CATCH_0051:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t1967233988 *)__exception_local);
		Exception_t1967233988 * L_15 = V_3;
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2130388473, L_16, /*hidden argument*/NULL);
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		goto IL_006c;
	} // end catch (depth: 1)

IL_006c:
	{
		bool L_18 = V_0;
		return L_18;
	}
}
// System.Void SocketIOClient.Eventing.RegistrationManager::Dispose()
extern "C"  void RegistrationManager_Dispose_m2291014800 (RegistrationManager_t998967514 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void SocketIOClient.Eventing.RegistrationManager::Dispose(System.Boolean) */, __this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Eventing.RegistrationManager::Dispose(System.Boolean)
extern "C"  void RegistrationManager_Dispose_m92247559 (RegistrationManager_t998967514 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		Dictionary_2_t1476369908 * L_0 = __this->get_callBackRegistry_0();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.Object>>::Clear() */, L_0);
		Dictionary_2_t1119712961 * L_1 = __this->get_eventNameRegistry_1();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`1<SocketIOClient.Messages.IMessage>>::Clear() */, L_1);
		return;
	}
}
// System.Void SocketIOClient.MessageEventArgs::.ctor(SocketIOClient.Messages.IMessage)
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t MessageEventArgs__ctor_m4268379643_MetadataUsageId;
extern "C"  void MessageEventArgs__ctor_m4268379643 (MessageEventArgs_t1401678908 * __this, Il2CppObject * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageEventArgs__ctor_m4268379643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1904770202(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___msg;
		MessageEventArgs_set_Message_m3529502531(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// SocketIOClient.Messages.IMessage SocketIOClient.MessageEventArgs::get_Message()
extern "C"  Il2CppObject * MessageEventArgs_get_Message_m3408976564 (MessageEventArgs_t1401678908 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CMessageU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SocketIOClient.MessageEventArgs::set_Message(SocketIOClient.Messages.IMessage)
extern "C"  void MessageEventArgs_set_Message_m3529502531 (MessageEventArgs_t1401678908 * __this, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value;
		__this->set_U3CMessageU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void SocketIOClient.Messages.AckMessage::.ctor()
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t AckMessage__ctor_m760400901_MetadataUsageId;
extern "C"  void AckMessage__ctor_m760400901 (AckMessage_t21730256 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AckMessage__ctor_m760400901_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message__ctor_m3082395484(__this, /*hidden argument*/NULL);
		Message_set_MessageType_m2319013183(__this, 6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.AckMessage::.cctor()
extern TypeInfo* Regex_t3802381858_il2cpp_TypeInfo_var;
extern TypeInfo* AckMessage_t21730256_il2cpp_TypeInfo_var;
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1619915856;
extern Il2CppCodeGenString* _stringLiteral1582681139;
extern Il2CppCodeGenString* _stringLiteral1977710254;
extern const uint32_t AckMessage__cctor_m1615495240_MetadataUsageId;
extern "C"  void AckMessage__cctor_m1615495240 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AckMessage__cctor_m1615495240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Regex_t3802381858 * L_0 = (Regex_t3802381858 *)il2cpp_codegen_object_new(Regex_t3802381858_il2cpp_TypeInfo_var);
		Regex__ctor_m2980635200(L_0, _stringLiteral1619915856, /*hidden argument*/NULL);
		((AckMessage_t21730256_StaticFields*)AckMessage_t21730256_il2cpp_TypeInfo_var->static_fields)->set_reAckId_10(L_0);
		Regex_t3802381858 * L_1 = (Regex_t3802381858 *)il2cpp_codegen_object_new(Regex_t3802381858_il2cpp_TypeInfo_var);
		Regex__ctor_m2980635200(L_1, _stringLiteral1582681139, /*hidden argument*/NULL);
		((AckMessage_t21730256_StaticFields*)AckMessage_t21730256_il2cpp_TypeInfo_var->static_fields)->set_reAckPayload_11(L_1);
		Regex_t3802381858 * L_2 = (Regex_t3802381858 *)il2cpp_codegen_object_new(Regex_t3802381858_il2cpp_TypeInfo_var);
		Regex__ctor_m2980635200(L_2, _stringLiteral1977710254, /*hidden argument*/NULL);
		((AckMessage_t21730256_StaticFields*)AckMessage_t21730256_il2cpp_TypeInfo_var->static_fields)->set_reAckComplex_12(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_3, /*hidden argument*/NULL);
		((AckMessage_t21730256_StaticFields*)AckMessage_t21730256_il2cpp_TypeInfo_var->static_fields)->set_ackLock_13(L_3);
		((AckMessage_t21730256_StaticFields*)AckMessage_t21730256_il2cpp_TypeInfo_var->static_fields)->set__akid_14(0);
		return;
	}
}
// System.Int32 SocketIOClient.Messages.AckMessage::get_NextAckID()
extern TypeInfo* AckMessage_t21730256_il2cpp_TypeInfo_var;
extern const uint32_t AckMessage_get_NextAckID_m1593256669_MetadataUsageId;
extern "C"  int32_t AckMessage_get_NextAckID_m1593256669 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AckMessage_get_NextAckID_m1593256669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(AckMessage_t21730256_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((AckMessage_t21730256_StaticFields*)AckMessage_t21730256_il2cpp_TypeInfo_var->static_fields)->get_ackLock_13();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(AckMessage_t21730256_il2cpp_TypeInfo_var);
			int32_t L_2 = ((AckMessage_t21730256_StaticFields*)AckMessage_t21730256_il2cpp_TypeInfo_var->static_fields)->get__akid_14();
			((AckMessage_t21730256_StaticFields*)AckMessage_t21730256_il2cpp_TypeInfo_var->static_fields)->set__akid_14(((int32_t)((int32_t)L_2+(int32_t)1)));
			int32_t L_3 = ((AckMessage_t21730256_StaticFields*)AckMessage_t21730256_il2cpp_TypeInfo_var->static_fields)->get__akid_14();
			if ((((int32_t)L_3) >= ((int32_t)0)))
			{
				goto IL_0029;
			}
		}

IL_0023:
		{
			IL2CPP_RUNTIME_CLASS_INIT(AckMessage_t21730256_il2cpp_TypeInfo_var);
			((AckMessage_t21730256_StaticFields*)AckMessage_t21730256_il2cpp_TypeInfo_var->static_fields)->set__akid_14(0);
		}

IL_0029:
		{
			IL2CPP_RUNTIME_CLASS_INIT(AckMessage_t21730256_il2cpp_TypeInfo_var);
			int32_t L_4 = ((AckMessage_t21730256_StaticFields*)AckMessage_t21730256_il2cpp_TypeInfo_var->static_fields)->get__akid_14();
			V_1 = L_4;
			IL2CPP_LEAVE(0x40, FINALLY_0039);
		}

IL_0034:
		{
			; // IL_0034: leave IL_0040
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(57)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0040:
	{
		int32_t L_6 = V_1;
		return L_6;
	}
}
// SocketIOClient.Messages.AckMessage SocketIOClient.Messages.AckMessage::Deserialize(System.String)
extern TypeInfo* AckMessage_t21730256_il2cpp_TypeInfo_var;
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* JsonEncodedEventMessage_t989636229_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m2954236726_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3508265358;
extern const uint32_t AckMessage_Deserialize_m3207619207_MetadataUsageId;
extern "C"  AckMessage_t21730256 * AckMessage_Deserialize_m3207619207 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AckMessage_Deserialize_m3207619207_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AckMessage_t21730256 * V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	int32_t V_2 = 0;
	StringU5BU5D_t2956870243* V_3 = NULL;
	Match_t3797657504 * V_4 = NULL;
	{
		AckMessage_t21730256 * L_0 = (AckMessage_t21730256 *)il2cpp_codegen_object_new(AckMessage_t21730256_il2cpp_TypeInfo_var);
		AckMessage__ctor_m760400901(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		AckMessage_t21730256 * L_1 = V_0;
		String_t* L_2 = ___rawMessage;
		NullCheck(L_1);
		Message_set_RawMessage_m2720466974(L_1, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___rawMessage;
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_4 = ((Message_t957426777_StaticFields*)Message_t957426777_il2cpp_TypeInfo_var->static_fields)->get_SPLITCHARS_1();
		NullCheck(L_3);
		StringU5BU5D_t2956870243* L_5 = String_Split_m434660345(L_3, L_4, 4, /*hidden argument*/NULL);
		V_1 = L_5;
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)4))))
		{
			goto IL_00c0;
		}
	}
	{
		AckMessage_t21730256 * L_7 = V_0;
		StringU5BU5D_t2956870243* L_8 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		int32_t L_9 = 2;
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String) */, L_7, ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9))));
		StringU5BU5D_t2956870243* L_10 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		int32_t L_11 = 3;
		CharU5BU5D_t3416858730* L_12 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)43));
		NullCheck(((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		StringU5BU5D_t2956870243* L_13 = String_Split_m290179486(((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		StringU5BU5D_t2956870243* L_14 = V_3;
		NullCheck(L_14);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_00c0;
		}
	}
	{
		StringU5BU5D_t2956870243* L_15 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		int32_t L_16 = 0;
		bool L_17 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16))), (&V_2), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00c0;
		}
	}
	{
		AckMessage_t21730256 * L_18 = V_0;
		int32_t L_19 = V_2;
		Nullable_1_t1438485399  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Nullable_1__ctor_m2954236726(&L_20, L_19, /*hidden argument*/Nullable_1__ctor_m2954236726_MethodInfo_var);
		NullCheck(L_18);
		Message_set_AckId_m1799485069(L_18, L_20, /*hidden argument*/NULL);
		AckMessage_t21730256 * L_21 = V_0;
		StringU5BU5D_t2956870243* L_22 = V_3;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 1);
		int32_t L_23 = 1;
		NullCheck(L_21);
		Message_set_MessageText_m231655857(L_21, ((L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AckMessage_t21730256_il2cpp_TypeInfo_var);
		Regex_t3802381858 * L_24 = ((AckMessage_t21730256_StaticFields*)AckMessage_t21730256_il2cpp_TypeInfo_var->static_fields)->get_reAckComplex_12();
		AckMessage_t21730256 * L_25 = V_0;
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, L_25);
		NullCheck(L_24);
		Match_t3797657504 * L_27 = Regex_Match_m2003175236(L_24, L_26, /*hidden argument*/NULL);
		V_4 = L_27;
		Match_t3797657504 * L_28 = V_4;
		NullCheck(L_28);
		bool L_29 = Group_get_Success_m3627958764(L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00c0;
		}
	}
	{
		AckMessage_t21730256 * L_30 = V_0;
		JsonEncodedEventMessage_t989636229 * L_31 = (JsonEncodedEventMessage_t989636229 *)il2cpp_codegen_object_new(JsonEncodedEventMessage_t989636229_il2cpp_TypeInfo_var);
		JsonEncodedEventMessage__ctor_m614799024(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		Message_set_Json_m2290864074(L_30, L_31, /*hidden argument*/NULL);
		AckMessage_t21730256 * L_32 = V_0;
		NullCheck(L_32);
		JsonEncodedEventMessage_t989636229 * L_33 = VirtFuncInvoker0< JsonEncodedEventMessage_t989636229 * >::Invoke(11 /* SocketIOClient.Messages.JsonEncodedEventMessage SocketIOClient.Messages.Message::get_Json() */, L_32);
		StringU5BU5D_t2956870243* L_34 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)1));
		Match_t3797657504 * L_35 = V_4;
		NullCheck(L_35);
		GroupCollection_t2158306392 * L_36 = VirtFuncInvoker0< GroupCollection_t2158306392 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_35);
		NullCheck(L_36);
		Group_t3792618586 * L_37 = GroupCollection_get_Item_m4186628929(L_36, _stringLiteral3508265358, /*hidden argument*/NULL);
		NullCheck(L_37);
		String_t* L_38 = Capture_get_Value_m2353629574(L_37, /*hidden argument*/NULL);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 0);
		ArrayElementTypeCheck (L_34, L_38);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_38);
		NullCheck(L_33);
		JsonEncodedEventMessage_set_args_m3437467364(L_33, (ObjectU5BU5D_t11523773*)(ObjectU5BU5D_t11523773*)L_34, /*hidden argument*/NULL);
	}

IL_00c0:
	{
		AckMessage_t21730256 * L_39 = V_0;
		return L_39;
	}
}
// System.String SocketIOClient.Messages.AckMessage::get_Encoded()
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m404638567_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3808413405_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3146308204;
extern Il2CppCodeGenString* _stringLiteral1050844365;
extern Il2CppCodeGenString* _stringLiteral3403096107;
extern const uint32_t AckMessage_get_Encoded_m1476365253_MetadataUsageId;
extern "C"  String_t* AckMessage_get_Encoded_m1476365253 (AckMessage_t21730256 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AckMessage_get_Encoded_m1476365253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Nullable_1_t1438485399  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Nullable_1_t1438485399  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Nullable_1_t1438485399  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t11523773* G_B4_1 = NULL;
	ObjectU5BU5D_t11523773* G_B4_2 = NULL;
	String_t* G_B4_3 = NULL;
	int32_t G_B3_0 = 0;
	ObjectU5BU5D_t11523773* G_B3_1 = NULL;
	ObjectU5BU5D_t11523773* G_B3_2 = NULL;
	String_t* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	int32_t G_B5_1 = 0;
	ObjectU5BU5D_t11523773* G_B5_2 = NULL;
	ObjectU5BU5D_t11523773* G_B5_3 = NULL;
	String_t* G_B5_4 = NULL;
	int32_t G_B8_0 = 0;
	ObjectU5BU5D_t11523773* G_B8_1 = NULL;
	ObjectU5BU5D_t11523773* G_B8_2 = NULL;
	String_t* G_B8_3 = NULL;
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t11523773* G_B7_1 = NULL;
	ObjectU5BU5D_t11523773* G_B7_2 = NULL;
	String_t* G_B7_3 = NULL;
	int32_t G_B9_0 = 0;
	int32_t G_B9_1 = 0;
	ObjectU5BU5D_t11523773* G_B9_2 = NULL;
	ObjectU5BU5D_t11523773* G_B9_3 = NULL;
	String_t* G_B9_4 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* SocketIOClient.SocketIOMessageTypes SocketIOClient.Messages.Message::get_MessageType() */, __this);
		V_0 = L_0;
		Nullable_1_t1438485399  L_1 = VirtFuncInvoker0< Nullable_1_t1438485399  >::Invoke(7 /* System.Nullable`1<System.Int32> SocketIOClient.Messages.Message::get_AckId() */, __this);
		V_1 = L_1;
		bool L_2 = Nullable_1_get_HasValue_m404638567((&V_1), /*hidden argument*/Nullable_1_get_HasValue_m404638567_MethodInfo_var);
		if (!L_2)
		{
			goto IL_00cd;
		}
	}
	{
		Action_t437523947 * L_3 = __this->get_Callback_15();
		if (L_3)
		{
			goto IL_0079;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_4 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_5 = V_0;
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_7);
		ObjectU5BU5D_t11523773* L_8 = L_4;
		Nullable_1_t1438485399  L_9 = VirtFuncInvoker0< Nullable_1_t1438485399  >::Invoke(7 /* System.Nullable`1<System.Int32> SocketIOClient.Messages.Message::get_AckId() */, __this);
		V_2 = L_9;
		bool L_10 = Nullable_1_get_HasValue_m404638567((&V_2), /*hidden argument*/Nullable_1_get_HasValue_m404638567_MethodInfo_var);
		G_B3_0 = 1;
		G_B3_1 = L_8;
		G_B3_2 = L_8;
		G_B3_3 = _stringLiteral3146308204;
		if (!L_10)
		{
			G_B4_0 = 1;
			G_B4_1 = L_8;
			G_B4_2 = L_8;
			G_B4_3 = _stringLiteral3146308204;
			goto IL_005a;
		}
	}
	{
		int32_t L_11 = Nullable_1_get_Value_m3808413405((&V_2), /*hidden argument*/Nullable_1_get_Value_m3808413405_MethodInfo_var);
		G_B5_0 = L_11;
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		G_B5_3 = G_B3_2;
		G_B5_4 = G_B3_3;
		goto IL_005b;
	}

IL_005a:
	{
		G_B5_0 = (-1);
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
		G_B5_3 = G_B4_2;
		G_B5_4 = G_B4_3;
	}

IL_005b:
	{
		int32_t L_12 = G_B5_0;
		Il2CppObject * L_13 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_12);
		NullCheck(G_B5_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B5_2, G_B5_1);
		ArrayElementTypeCheck (G_B5_2, L_13);
		(G_B5_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B5_1), (Il2CppObject *)L_13);
		ObjectU5BU5D_t11523773* L_14 = G_B5_3;
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String SocketIOClient.Messages.Message::get_Endpoint() */, __this);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_15);
		ObjectU5BU5D_t11523773* L_16 = L_14;
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, __this);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m4050103162(NULL /*static, unused*/, G_B5_4, L_16, /*hidden argument*/NULL);
		return L_18;
	}

IL_0079:
	{
		ObjectU5BU5D_t11523773* L_19 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_20 = V_0;
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 0);
		ArrayElementTypeCheck (L_19, L_22);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = L_19;
		Nullable_1_t1438485399  L_24 = VirtFuncInvoker0< Nullable_1_t1438485399  >::Invoke(7 /* System.Nullable`1<System.Int32> SocketIOClient.Messages.Message::get_AckId() */, __this);
		V_3 = L_24;
		bool L_25 = Nullable_1_get_HasValue_m404638567((&V_3), /*hidden argument*/Nullable_1_get_HasValue_m404638567_MethodInfo_var);
		G_B7_0 = 1;
		G_B7_1 = L_23;
		G_B7_2 = L_23;
		G_B7_3 = _stringLiteral1050844365;
		if (!L_25)
		{
			G_B8_0 = 1;
			G_B8_1 = L_23;
			G_B8_2 = L_23;
			G_B8_3 = _stringLiteral1050844365;
			goto IL_00ae;
		}
	}
	{
		int32_t L_26 = Nullable_1_get_Value_m3808413405((&V_3), /*hidden argument*/Nullable_1_get_Value_m3808413405_MethodInfo_var);
		G_B9_0 = L_26;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		G_B9_4 = G_B7_3;
		goto IL_00af;
	}

IL_00ae:
	{
		G_B9_0 = (-1);
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
		G_B9_4 = G_B8_3;
	}

IL_00af:
	{
		int32_t L_27 = G_B9_0;
		Il2CppObject * L_28 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_27);
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		ArrayElementTypeCheck (G_B9_2, L_28);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (Il2CppObject *)L_28);
		ObjectU5BU5D_t11523773* L_29 = G_B9_3;
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String SocketIOClient.Messages.Message::get_Endpoint() */, __this);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 2);
		ArrayElementTypeCheck (L_29, L_30);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_30);
		ObjectU5BU5D_t11523773* L_31 = L_29;
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, __this);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 3);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Format_m4050103162(NULL /*static, unused*/, G_B9_4, L_31, /*hidden argument*/NULL);
		return L_33;
	}

IL_00cd:
	{
		int32_t L_34 = V_0;
		int32_t L_35 = L_34;
		Il2CppObject * L_36 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_35);
		String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String SocketIOClient.Messages.Message::get_Endpoint() */, __this);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral3403096107, L_36, L_37, L_38, /*hidden argument*/NULL);
		return L_39;
	}
}
// System.Void SocketIOClient.Messages.ConnectMessage::.ctor()
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t ConnectMessage__ctor_m4222249574_MetadataUsageId;
extern "C"  void ConnectMessage__ctor_m4222249574 (ConnectMessage_t3539960911 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectMessage__ctor_m4222249574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message__ctor_m3082395484(__this, /*hidden argument*/NULL);
		Message_set_MessageType_m2319013183(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.ConnectMessage::.ctor(System.String)
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t ConnectMessage__ctor_m1926351452_MetadataUsageId;
extern "C"  void ConnectMessage__ctor_m1926351452 (ConnectMessage_t3539960911 * __this, String_t* ___endPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectMessage__ctor_m1926351452_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ConnectMessage__ctor_m4222249574(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___endPoint;
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String) */, __this, L_0);
		return;
	}
}
// System.String SocketIOClient.Messages.ConnectMessage::get_Query()
extern "C"  String_t* ConnectMessage_get_Query_m2105175520 (ConnectMessage_t3539960911 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CQueryU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.ConnectMessage::set_Query(System.String)
extern "C"  void ConnectMessage_set_Query_m2817543539 (ConnectMessage_t3539960911 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CQueryU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.String SocketIOClient.Messages.ConnectMessage::get_Event()
extern Il2CppCodeGenString* _stringLiteral951351530;
extern const uint32_t ConnectMessage_get_Event_m73571122_MetadataUsageId;
extern "C"  String_t* ConnectMessage_get_Event_m73571122 (ConnectMessage_t3539960911 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectMessage_get_Event_m73571122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral951351530;
	}
}
// SocketIOClient.Messages.ConnectMessage SocketIOClient.Messages.ConnectMessage::Deserialize(System.String)
extern TypeInfo* ConnectMessage_t3539960911_il2cpp_TypeInfo_var;
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const uint32_t ConnectMessage_Deserialize_m3399944103_MetadataUsageId;
extern "C"  ConnectMessage_t3539960911 * ConnectMessage_Deserialize_m3399944103 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectMessage_Deserialize_m3399944103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConnectMessage_t3539960911 * V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	StringU5BU5D_t2956870243* V_2 = NULL;
	{
		ConnectMessage_t3539960911 * L_0 = (ConnectMessage_t3539960911 *)il2cpp_codegen_object_new(ConnectMessage_t3539960911_il2cpp_TypeInfo_var);
		ConnectMessage__ctor_m4222249574(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ConnectMessage_t3539960911 * L_1 = V_0;
		String_t* L_2 = ___rawMessage;
		NullCheck(L_1);
		Message_set_RawMessage_m2720466974(L_1, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___rawMessage;
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_4 = ((Message_t957426777_StaticFields*)Message_t957426777_il2cpp_TypeInfo_var->static_fields)->get_SPLITCHARS_1();
		NullCheck(L_3);
		StringU5BU5D_t2956870243* L_5 = String_Split_m434660345(L_3, L_4, 3, /*hidden argument*/NULL);
		V_1 = L_5;
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)3))))
		{
			goto IL_005b;
		}
	}
	{
		StringU5BU5D_t2956870243* L_7 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		int32_t L_8 = 2;
		CharU5BU5D_t3416858730* L_9 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)63));
		NullCheck(((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		StringU5BU5D_t2956870243* L_10 = String_Split_m290179486(((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		StringU5BU5D_t2956870243* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0049;
		}
	}
	{
		ConnectMessage_t3539960911 * L_12 = V_0;
		StringU5BU5D_t2956870243* L_13 = V_2;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		int32_t L_14 = 0;
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String) */, L_12, ((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14))));
	}

IL_0049:
	{
		StringU5BU5D_t2956870243* L_15 = V_2;
		NullCheck(L_15);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_005b;
		}
	}
	{
		ConnectMessage_t3539960911 * L_16 = V_0;
		StringU5BU5D_t2956870243* L_17 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		int32_t L_18 = 1;
		NullCheck(L_16);
		ConnectMessage_set_Query_m2817543539(L_16, ((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/NULL);
	}

IL_005b:
	{
		ConnectMessage_t3539960911 * L_19 = V_0;
		return L_19;
	}
}
// System.String SocketIOClient.Messages.ConnectMessage::get_Encoded()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3137993104;
extern Il2CppCodeGenString* _stringLiteral1996649;
extern const uint32_t ConnectMessage_get_Encoded_m3460478694_MetadataUsageId;
extern "C"  String_t* ConnectMessage_get_Encoded_m3460478694 (ConnectMessage_t3539960911 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ConnectMessage_get_Encoded_m3460478694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String SocketIOClient.Messages.Message::get_Endpoint() */, __this);
		String_t* L_1 = ConnectMessage_get_Query_m2105175520(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		G_B1_1 = _stringLiteral3137993104;
		if (!L_2)
		{
			G_B2_0 = L_0;
			G_B2_1 = _stringLiteral3137993104;
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0035;
	}

IL_0025:
	{
		String_t* L_4 = ConnectMessage_get_Query_m2105175520(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1996649, L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m2398979370(NULL /*static, unused*/, G_B3_2, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void SocketIOClient.Messages.DisconnectMessage::.ctor()
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t DisconnectMessage__ctor_m1144767512_MetadataUsageId;
extern "C"  void DisconnectMessage__ctor_m1144767512 (DisconnectMessage_t1581018141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisconnectMessage__ctor_m1144767512_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message__ctor_m3082395484(__this, /*hidden argument*/NULL);
		Message_set_MessageType_m2319013183(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.DisconnectMessage::.ctor(System.String)
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t DisconnectMessage__ctor_m2234466794_MetadataUsageId;
extern "C"  void DisconnectMessage__ctor_m2234466794 (DisconnectMessage_t1581018141 * __this, String_t* ___endPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisconnectMessage__ctor_m2234466794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DisconnectMessage__ctor_m1144767512(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___endPoint;
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String) */, __this, L_0);
		return;
	}
}
// System.String SocketIOClient.Messages.DisconnectMessage::get_Event()
extern Il2CppCodeGenString* _stringLiteral530405532;
extern const uint32_t DisconnectMessage_get_Event_m2485685450_MetadataUsageId;
extern "C"  String_t* DisconnectMessage_get_Event_m2485685450 (DisconnectMessage_t1581018141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisconnectMessage_get_Event_m2485685450_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral530405532;
	}
}
// SocketIOClient.Messages.DisconnectMessage SocketIOClient.Messages.DisconnectMessage::Deserialize(System.String)
extern TypeInfo* DisconnectMessage_t1581018141_il2cpp_TypeInfo_var;
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DisconnectMessage_Deserialize_m2775835117_MetadataUsageId;
extern "C"  DisconnectMessage_t1581018141 * DisconnectMessage_Deserialize_m2775835117 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisconnectMessage_Deserialize_m2775835117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DisconnectMessage_t1581018141 * V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	{
		DisconnectMessage_t1581018141 * L_0 = (DisconnectMessage_t1581018141 *)il2cpp_codegen_object_new(DisconnectMessage_t1581018141_il2cpp_TypeInfo_var);
		DisconnectMessage__ctor_m1144767512(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		DisconnectMessage_t1581018141 * L_1 = V_0;
		String_t* L_2 = ___rawMessage;
		NullCheck(L_1);
		Message_set_RawMessage_m2720466974(L_1, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___rawMessage;
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_4 = ((Message_t957426777_StaticFields*)Message_t957426777_il2cpp_TypeInfo_var->static_fields)->get_SPLITCHARS_1();
		NullCheck(L_3);
		StringU5BU5D_t2956870243* L_5 = String_Split_m434660345(L_3, L_4, 3, /*hidden argument*/NULL);
		V_1 = L_5;
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)3))))
		{
			goto IL_0039;
		}
	}
	{
		StringU5BU5D_t2956870243* L_7 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		int32_t L_8 = 2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0039;
		}
	}
	{
		DisconnectMessage_t1581018141 * L_10 = V_0;
		StringU5BU5D_t2956870243* L_11 = V_1;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		int32_t L_12 = 2;
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String) */, L_10, ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12))));
	}

IL_0039:
	{
		DisconnectMessage_t1581018141 * L_13 = V_0;
		return L_13;
	}
}
// System.String SocketIOClient.Messages.DisconnectMessage::get_Encoded()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1429611160;
extern const uint32_t DisconnectMessage_get_Encoded_m2220008062_MetadataUsageId;
extern "C"  String_t* DisconnectMessage_get_Encoded_m2220008062 (DisconnectMessage_t1581018141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisconnectMessage_get_Encoded_m2220008062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String SocketIOClient.Messages.Message::get_Endpoint() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1429611160, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void SocketIOClient.Messages.ErrorMessage::.ctor()
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t ErrorMessage__ctor_m1092945796_MetadataUsageId;
extern "C"  void ErrorMessage__ctor_m1092945796 (ErrorMessage_t4000372081 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ErrorMessage__ctor_m1092945796_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message__ctor_m3082395484(__this, /*hidden argument*/NULL);
		Message_set_MessageType_m2319013183(__this, 7, /*hidden argument*/NULL);
		return;
	}
}
// System.String SocketIOClient.Messages.ErrorMessage::get_Reason()
extern "C"  String_t* ErrorMessage_get_Reason_m608053616 (ErrorMessage_t4000372081 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CReasonU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.ErrorMessage::set_Reason(System.String)
extern "C"  void ErrorMessage_set_Reason_m3822803489 (ErrorMessage_t4000372081 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CReasonU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.String SocketIOClient.Messages.ErrorMessage::get_Advice()
extern "C"  String_t* ErrorMessage_get_Advice_m749156132 (ErrorMessage_t4000372081 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAdviceU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.ErrorMessage::set_Advice(System.String)
extern "C"  void ErrorMessage_set_Advice_m2705404141 (ErrorMessage_t4000372081 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CAdviceU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String SocketIOClient.Messages.ErrorMessage::get_Event()
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t ErrorMessage_get_Event_m3519779984_MetadataUsageId;
extern "C"  String_t* ErrorMessage_get_Event_m3519779984 (ErrorMessage_t4000372081 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ErrorMessage_get_Event_m3519779984_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral96784904;
	}
}
// SocketIOClient.Messages.ErrorMessage SocketIOClient.Messages.ErrorMessage::Deserialize(System.String)
extern TypeInfo* ErrorMessage_t4000372081_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const uint32_t ErrorMessage_Deserialize_m334360103_MetadataUsageId;
extern "C"  ErrorMessage_t4000372081 * ErrorMessage_Deserialize_m334360103 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ErrorMessage_Deserialize_m334360103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ErrorMessage_t4000372081 * V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	StringU5BU5D_t2956870243* V_2 = NULL;
	{
		ErrorMessage_t4000372081 * L_0 = (ErrorMessage_t4000372081 *)il2cpp_codegen_object_new(ErrorMessage_t4000372081_il2cpp_TypeInfo_var);
		ErrorMessage__ctor_m1092945796(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___rawMessage;
		CharU5BU5D_t3416858730* L_2 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)58));
		NullCheck(L_1);
		StringU5BU5D_t2956870243* L_3 = String_Split_m290179486(L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		StringU5BU5D_t2956870243* L_4 = V_1;
		NullCheck(L_4);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))) == ((uint32_t)4))))
		{
			goto IL_0062;
		}
	}
	{
		ErrorMessage_t4000372081 * L_5 = V_0;
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		int32_t L_7 = 2;
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String) */, L_5, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		ErrorMessage_t4000372081 * L_8 = V_0;
		StringU5BU5D_t2956870243* L_9 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		int32_t L_10 = 3;
		NullCheck(L_8);
		Message_set_MessageText_m231655857(L_8, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), /*hidden argument*/NULL);
		StringU5BU5D_t2956870243* L_11 = V_1;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		int32_t L_12 = 3;
		CharU5BU5D_t3416858730* L_13 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)43));
		NullCheck(((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12))));
		StringU5BU5D_t2956870243* L_14 = String_Split_m290179486(((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12))), L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		StringU5BU5D_t2956870243* L_15 = V_2;
		NullCheck(L_15);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_0062;
		}
	}
	{
		ErrorMessage_t4000372081 * L_16 = V_0;
		StringU5BU5D_t2956870243* L_17 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		int32_t L_18 = 1;
		NullCheck(L_16);
		ErrorMessage_set_Advice_m2705404141(L_16, ((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/NULL);
		ErrorMessage_t4000372081 * L_19 = V_0;
		StringU5BU5D_t2956870243* L_20 = V_2;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		int32_t L_21 = 0;
		NullCheck(L_19);
		ErrorMessage_set_Reason_m3822803489(L_19, ((L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21))), /*hidden argument*/NULL);
	}

IL_0062:
	{
		ErrorMessage_t4000372081 * L_22 = V_0;
		return L_22;
	}
}
// System.Void SocketIOClient.Messages.EventMessage::.ctor()
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t EventMessage__ctor_m1254573078_MetadataUsageId;
extern "C"  void EventMessage__ctor_m1254573078 (EventMessage_t365350047 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventMessage__ctor_m1254573078_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message__ctor_m3082395484(__this, /*hidden argument*/NULL);
		Message_set_MessageType_m2319013183(__this, 5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.EventMessage::.ctor(System.String,System.Object,System.String,System.Action`1<System.Object>)
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern TypeInfo* EventMessage_t365350047_il2cpp_TypeInfo_var;
extern TypeInfo* JsonEncodedEventMessage_t989636229_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m2954236726_MethodInfo_var;
extern const uint32_t EventMessage__ctor_m820155226_MetadataUsageId;
extern "C"  void EventMessage__ctor_m820155226 (EventMessage_t365350047 * __this, String_t* ___eventName, Il2CppObject * ___jsonObject, String_t* ___endpoint, Action_1_t985559125 * ___callBack, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventMessage__ctor_m820155226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventMessage__ctor_m1254573078(__this, /*hidden argument*/NULL);
		Action_1_t985559125 * L_0 = ___callBack;
		__this->set_Callback_12(L_0);
		String_t* L_1 = ___endpoint;
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String) */, __this, L_1);
		Action_1_t985559125 * L_2 = ___callBack;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventMessage_t365350047_il2cpp_TypeInfo_var);
		int32_t L_3 = EventMessage_get_NextAckID_m800587118(NULL /*static, unused*/, /*hidden argument*/NULL);
		Nullable_1_t1438485399  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Nullable_1__ctor_m2954236726(&L_4, L_3, /*hidden argument*/Nullable_1__ctor_m2954236726_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message_set_AckId_m1799485069(__this, L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		String_t* L_5 = ___eventName;
		Il2CppObject * L_6 = ___jsonObject;
		JsonEncodedEventMessage_t989636229 * L_7 = (JsonEncodedEventMessage_t989636229 *)il2cpp_codegen_object_new(JsonEncodedEventMessage_t989636229_il2cpp_TypeInfo_var);
		JsonEncodedEventMessage__ctor_m1402911136(L_7, L_5, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message_set_JsonEncodedMessage_m1452199601(__this, L_7, /*hidden argument*/NULL);
		JsonEncodedEventMessage_t989636229 * L_8 = VirtFuncInvoker0< JsonEncodedEventMessage_t989636229 * >::Invoke(11 /* SocketIOClient.Messages.JsonEncodedEventMessage SocketIOClient.Messages.Message::get_Json() */, __this);
		NullCheck(L_8);
		String_t* L_9 = JsonEncodedEventMessage_ToJsonString_m4146288133(L_8, /*hidden argument*/NULL);
		Message_set_MessageText_m231655857(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.EventMessage::.cctor()
extern TypeInfo* Il2CppObject_il2cpp_TypeInfo_var;
extern TypeInfo* EventMessage_t365350047_il2cpp_TypeInfo_var;
extern const uint32_t EventMessage__cctor_m4049930839_MetadataUsageId;
extern "C"  void EventMessage__cctor_m4049930839 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventMessage__cctor_m4049930839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		((EventMessage_t365350047_StaticFields*)EventMessage_t365350047_il2cpp_TypeInfo_var->static_fields)->set_ackLock_10(L_0);
		((EventMessage_t365350047_StaticFields*)EventMessage_t365350047_il2cpp_TypeInfo_var->static_fields)->set__akid_11(0);
		return;
	}
}
// System.Int32 SocketIOClient.Messages.EventMessage::get_NextAckID()
extern TypeInfo* EventMessage_t365350047_il2cpp_TypeInfo_var;
extern const uint32_t EventMessage_get_NextAckID_m800587118_MetadataUsageId;
extern "C"  int32_t EventMessage_get_NextAckID_m800587118 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventMessage_get_NextAckID_m800587118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventMessage_t365350047_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((EventMessage_t365350047_StaticFields*)EventMessage_t365350047_il2cpp_TypeInfo_var->static_fields)->get_ackLock_10();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(EventMessage_t365350047_il2cpp_TypeInfo_var);
			int32_t L_2 = ((EventMessage_t365350047_StaticFields*)EventMessage_t365350047_il2cpp_TypeInfo_var->static_fields)->get__akid_11();
			((EventMessage_t365350047_StaticFields*)EventMessage_t365350047_il2cpp_TypeInfo_var->static_fields)->set__akid_11(((int32_t)((int32_t)L_2+(int32_t)1)));
			int32_t L_3 = ((EventMessage_t365350047_StaticFields*)EventMessage_t365350047_il2cpp_TypeInfo_var->static_fields)->get__akid_11();
			if ((((int32_t)L_3) >= ((int32_t)0)))
			{
				goto IL_0029;
			}
		}

IL_0023:
		{
			IL2CPP_RUNTIME_CLASS_INIT(EventMessage_t365350047_il2cpp_TypeInfo_var);
			((EventMessage_t365350047_StaticFields*)EventMessage_t365350047_il2cpp_TypeInfo_var->static_fields)->set__akid_11(0);
		}

IL_0029:
		{
			IL2CPP_RUNTIME_CLASS_INIT(EventMessage_t365350047_il2cpp_TypeInfo_var);
			int32_t L_4 = ((EventMessage_t365350047_StaticFields*)EventMessage_t365350047_il2cpp_TypeInfo_var->static_fields)->get__akid_11();
			V_1 = L_4;
			IL2CPP_LEAVE(0x40, FINALLY_0039);
		}

IL_0034:
		{
			; // IL_0034: leave IL_0040
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(57)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0040:
	{
		int32_t L_6 = V_1;
		return L_6;
	}
}
// SocketIOClient.Messages.EventMessage SocketIOClient.Messages.EventMessage::Deserialize(System.String)
extern TypeInfo* EventMessage_t365350047_il2cpp_TypeInfo_var;
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* JsonEncodedEventMessage_t989636229_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m2954236726_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3002589;
extern const uint32_t EventMessage_Deserialize_m4156674791_MetadataUsageId;
extern "C"  EventMessage_t365350047 * EventMessage_Deserialize_m4156674791 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventMessage_Deserialize_m4156674791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EventMessage_t365350047 * V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	int32_t V_2 = 0;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		EventMessage_t365350047 * L_0 = (EventMessage_t365350047 *)il2cpp_codegen_object_new(EventMessage_t365350047_il2cpp_TypeInfo_var);
		EventMessage__ctor_m1254573078(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		EventMessage_t365350047 * L_1 = V_0;
		String_t* L_2 = ___rawMessage;
		NullCheck(L_1);
		Message_set_RawMessage_m2720466974(L_1, L_2, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_3 = ___rawMessage;
			IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
			CharU5BU5D_t3416858730* L_4 = ((Message_t957426777_StaticFields*)Message_t957426777_il2cpp_TypeInfo_var->static_fields)->get_SPLITCHARS_1();
			NullCheck(L_3);
			StringU5BU5D_t2956870243* L_5 = String_Split_m434660345(L_3, L_4, 4, /*hidden argument*/NULL);
			V_1 = L_5;
			StringU5BU5D_t2956870243* L_6 = V_1;
			NullCheck(L_6);
			if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)4))))
			{
				goto IL_00bc;
			}
		}

IL_0023:
		{
			StringU5BU5D_t2956870243* L_7 = V_1;
			NullCheck(L_7);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
			int32_t L_8 = 1;
			bool L_9 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), (&V_2), /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_003e;
			}
		}

IL_0032:
		{
			EventMessage_t365350047 * L_10 = V_0;
			int32_t L_11 = V_2;
			Nullable_1_t1438485399  L_12;
			memset(&L_12, 0, sizeof(L_12));
			Nullable_1__ctor_m2954236726(&L_12, L_11, /*hidden argument*/Nullable_1__ctor_m2954236726_MethodInfo_var);
			NullCheck(L_10);
			Message_set_AckId_m1799485069(L_10, L_12, /*hidden argument*/NULL);
		}

IL_003e:
		{
			EventMessage_t365350047 * L_13 = V_0;
			StringU5BU5D_t2956870243* L_14 = V_1;
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
			int32_t L_15 = 2;
			NullCheck(L_13);
			VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String) */, L_13, ((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15))));
			EventMessage_t365350047 * L_16 = V_0;
			StringU5BU5D_t2956870243* L_17 = V_1;
			NullCheck(L_17);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
			int32_t L_18 = 3;
			NullCheck(L_16);
			Message_set_MessageText_m231655857(L_16, ((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/NULL);
			EventMessage_t365350047 * L_19 = V_0;
			NullCheck(L_19);
			String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, L_19);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_21 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
			if (L_21)
			{
				goto IL_00b1;
			}
		}

IL_0060:
		{
			EventMessage_t365350047 * L_22 = V_0;
			NullCheck(L_22);
			String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, L_22);
			NullCheck(L_23);
			bool L_24 = String_Contains_m3032019141(L_23, _stringLiteral3373707, /*hidden argument*/NULL);
			if (!L_24)
			{
				goto IL_00b1;
			}
		}

IL_0075:
		{
			EventMessage_t365350047 * L_25 = V_0;
			NullCheck(L_25);
			String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, L_25);
			NullCheck(L_26);
			bool L_27 = String_Contains_m3032019141(L_26, _stringLiteral3002589, /*hidden argument*/NULL);
			if (!L_27)
			{
				goto IL_00b1;
			}
		}

IL_008a:
		{
			EventMessage_t365350047 * L_28 = V_0;
			EventMessage_t365350047 * L_29 = V_0;
			NullCheck(L_29);
			String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, L_29);
			JsonEncodedEventMessage_t989636229 * L_31 = JsonEncodedEventMessage_Deserialize_m1080785149(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
			NullCheck(L_28);
			Message_set_Json_m2290864074(L_28, L_31, /*hidden argument*/NULL);
			EventMessage_t365350047 * L_32 = V_0;
			EventMessage_t365350047 * L_33 = V_0;
			NullCheck(L_33);
			JsonEncodedEventMessage_t989636229 * L_34 = VirtFuncInvoker0< JsonEncodedEventMessage_t989636229 * >::Invoke(11 /* SocketIOClient.Messages.JsonEncodedEventMessage SocketIOClient.Messages.Message::get_Json() */, L_33);
			NullCheck(L_34);
			String_t* L_35 = JsonEncodedEventMessage_get_name_m2613943237(L_34, /*hidden argument*/NULL);
			NullCheck(L_32);
			VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void SocketIOClient.Messages.Message::set_Event(System.String) */, L_32, L_35);
			goto IL_00bc;
		}

IL_00b1:
		{
			EventMessage_t365350047 * L_36 = V_0;
			JsonEncodedEventMessage_t989636229 * L_37 = (JsonEncodedEventMessage_t989636229 *)il2cpp_codegen_object_new(JsonEncodedEventMessage_t989636229_il2cpp_TypeInfo_var);
			JsonEncodedEventMessage__ctor_m614799024(L_37, /*hidden argument*/NULL);
			NullCheck(L_36);
			Message_set_Json_m2290864074(L_36, L_37, /*hidden argument*/NULL);
		}

IL_00bc:
		{
			goto IL_00cd;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00c1;
		throw e;
	}

CATCH_00c1:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t1967233988 *)__exception_local);
		Exception_t1967233988 * L_38 = V_3;
		Trace_WriteLine_m3524640299(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		goto IL_00cd;
	} // end catch (depth: 1)

IL_00cd:
	{
		EventMessage_t365350047 * L_39 = V_0;
		return L_39;
	}
}
// System.String SocketIOClient.Messages.EventMessage::get_Encoded()
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m404638567_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3808413405_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3146308204;
extern Il2CppCodeGenString* _stringLiteral1050844365;
extern Il2CppCodeGenString* _stringLiteral3403096107;
extern const uint32_t EventMessage_get_Encoded_m3466975190_MetadataUsageId;
extern "C"  String_t* EventMessage_get_Encoded_m3466975190 (EventMessage_t365350047 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventMessage_get_Encoded_m3466975190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Nullable_1_t1438485399  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Nullable_1_t1438485399  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Nullable_1_t1438485399  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t11523773* G_B4_1 = NULL;
	ObjectU5BU5D_t11523773* G_B4_2 = NULL;
	String_t* G_B4_3 = NULL;
	int32_t G_B3_0 = 0;
	ObjectU5BU5D_t11523773* G_B3_1 = NULL;
	ObjectU5BU5D_t11523773* G_B3_2 = NULL;
	String_t* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	int32_t G_B5_1 = 0;
	ObjectU5BU5D_t11523773* G_B5_2 = NULL;
	ObjectU5BU5D_t11523773* G_B5_3 = NULL;
	String_t* G_B5_4 = NULL;
	int32_t G_B8_0 = 0;
	ObjectU5BU5D_t11523773* G_B8_1 = NULL;
	ObjectU5BU5D_t11523773* G_B8_2 = NULL;
	String_t* G_B8_3 = NULL;
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t11523773* G_B7_1 = NULL;
	ObjectU5BU5D_t11523773* G_B7_2 = NULL;
	String_t* G_B7_3 = NULL;
	int32_t G_B9_0 = 0;
	int32_t G_B9_1 = 0;
	ObjectU5BU5D_t11523773* G_B9_2 = NULL;
	ObjectU5BU5D_t11523773* G_B9_3 = NULL;
	String_t* G_B9_4 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* SocketIOClient.SocketIOMessageTypes SocketIOClient.Messages.Message::get_MessageType() */, __this);
		V_0 = L_0;
		Nullable_1_t1438485399  L_1 = VirtFuncInvoker0< Nullable_1_t1438485399  >::Invoke(7 /* System.Nullable`1<System.Int32> SocketIOClient.Messages.Message::get_AckId() */, __this);
		V_1 = L_1;
		bool L_2 = Nullable_1_get_HasValue_m404638567((&V_1), /*hidden argument*/Nullable_1_get_HasValue_m404638567_MethodInfo_var);
		if (!L_2)
		{
			goto IL_00cd;
		}
	}
	{
		Action_1_t985559125 * L_3 = __this->get_Callback_12();
		if (L_3)
		{
			goto IL_0079;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_4 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_5 = V_0;
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_7);
		ObjectU5BU5D_t11523773* L_8 = L_4;
		Nullable_1_t1438485399  L_9 = VirtFuncInvoker0< Nullable_1_t1438485399  >::Invoke(7 /* System.Nullable`1<System.Int32> SocketIOClient.Messages.Message::get_AckId() */, __this);
		V_2 = L_9;
		bool L_10 = Nullable_1_get_HasValue_m404638567((&V_2), /*hidden argument*/Nullable_1_get_HasValue_m404638567_MethodInfo_var);
		G_B3_0 = 1;
		G_B3_1 = L_8;
		G_B3_2 = L_8;
		G_B3_3 = _stringLiteral3146308204;
		if (!L_10)
		{
			G_B4_0 = 1;
			G_B4_1 = L_8;
			G_B4_2 = L_8;
			G_B4_3 = _stringLiteral3146308204;
			goto IL_005a;
		}
	}
	{
		int32_t L_11 = Nullable_1_get_Value_m3808413405((&V_2), /*hidden argument*/Nullable_1_get_Value_m3808413405_MethodInfo_var);
		G_B5_0 = L_11;
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		G_B5_3 = G_B3_2;
		G_B5_4 = G_B3_3;
		goto IL_005b;
	}

IL_005a:
	{
		G_B5_0 = (-1);
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
		G_B5_3 = G_B4_2;
		G_B5_4 = G_B4_3;
	}

IL_005b:
	{
		int32_t L_12 = G_B5_0;
		Il2CppObject * L_13 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_12);
		NullCheck(G_B5_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B5_2, G_B5_1);
		ArrayElementTypeCheck (G_B5_2, L_13);
		(G_B5_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B5_1), (Il2CppObject *)L_13);
		ObjectU5BU5D_t11523773* L_14 = G_B5_3;
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String SocketIOClient.Messages.Message::get_Endpoint() */, __this);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_15);
		ObjectU5BU5D_t11523773* L_16 = L_14;
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, __this);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m4050103162(NULL /*static, unused*/, G_B5_4, L_16, /*hidden argument*/NULL);
		return L_18;
	}

IL_0079:
	{
		ObjectU5BU5D_t11523773* L_19 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_20 = V_0;
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 0);
		ArrayElementTypeCheck (L_19, L_22);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = L_19;
		Nullable_1_t1438485399  L_24 = VirtFuncInvoker0< Nullable_1_t1438485399  >::Invoke(7 /* System.Nullable`1<System.Int32> SocketIOClient.Messages.Message::get_AckId() */, __this);
		V_3 = L_24;
		bool L_25 = Nullable_1_get_HasValue_m404638567((&V_3), /*hidden argument*/Nullable_1_get_HasValue_m404638567_MethodInfo_var);
		G_B7_0 = 1;
		G_B7_1 = L_23;
		G_B7_2 = L_23;
		G_B7_3 = _stringLiteral1050844365;
		if (!L_25)
		{
			G_B8_0 = 1;
			G_B8_1 = L_23;
			G_B8_2 = L_23;
			G_B8_3 = _stringLiteral1050844365;
			goto IL_00ae;
		}
	}
	{
		int32_t L_26 = Nullable_1_get_Value_m3808413405((&V_3), /*hidden argument*/Nullable_1_get_Value_m3808413405_MethodInfo_var);
		G_B9_0 = L_26;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		G_B9_4 = G_B7_3;
		goto IL_00af;
	}

IL_00ae:
	{
		G_B9_0 = (-1);
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
		G_B9_4 = G_B8_3;
	}

IL_00af:
	{
		int32_t L_27 = G_B9_0;
		Il2CppObject * L_28 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_27);
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		ArrayElementTypeCheck (G_B9_2, L_28);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (Il2CppObject *)L_28);
		ObjectU5BU5D_t11523773* L_29 = G_B9_3;
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String SocketIOClient.Messages.Message::get_Endpoint() */, __this);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 2);
		ArrayElementTypeCheck (L_29, L_30);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_30);
		ObjectU5BU5D_t11523773* L_31 = L_29;
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, __this);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 3);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Format_m4050103162(NULL /*static, unused*/, G_B9_4, L_31, /*hidden argument*/NULL);
		return L_33;
	}

IL_00cd:
	{
		int32_t L_34 = V_0;
		int32_t L_35 = L_34;
		Il2CppObject * L_36 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_35);
		String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String SocketIOClient.Messages.Message::get_Endpoint() */, __this);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral3403096107, L_36, L_37, L_38, /*hidden argument*/NULL);
		return L_39;
	}
}
// System.Void SocketIOClient.Messages.Heartbeat::.ctor()
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t Heartbeat__ctor_m2318316039_MetadataUsageId;
extern "C"  void Heartbeat__ctor_m2318316039 (Heartbeat_t542713166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Heartbeat__ctor_m2318316039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message__ctor_m3082395484(__this, /*hidden argument*/NULL);
		Message_set_MessageType_m2319013183(__this, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.Heartbeat::.cctor()
extern TypeInfo* Heartbeat_t542713166_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral49906;
extern const uint32_t Heartbeat__cctor_m2666224262_MetadataUsageId;
extern "C"  void Heartbeat__cctor_m2666224262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Heartbeat__cctor_m2666224262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Heartbeat_t542713166_StaticFields*)Heartbeat_t542713166_il2cpp_TypeInfo_var->static_fields)->set_HEARTBEAT_10(_stringLiteral49906);
		return;
	}
}
// System.String SocketIOClient.Messages.Heartbeat::get_Encoded()
extern TypeInfo* Heartbeat_t542713166_il2cpp_TypeInfo_var;
extern const uint32_t Heartbeat_get_Encoded_m1904079917_MetadataUsageId;
extern "C"  String_t* Heartbeat_get_Encoded_m1904079917 (Heartbeat_t542713166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Heartbeat_get_Encoded_m1904079917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Heartbeat_t542713166_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Heartbeat_t542713166_StaticFields*)Heartbeat_t542713166_il2cpp_TypeInfo_var->static_fields)->get_HEARTBEAT_10();
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.JsonEncodedEventMessage::.ctor()
extern "C"  void JsonEncodedEventMessage__ctor_m614799024 (JsonEncodedEventMessage_t989636229 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.JsonEncodedEventMessage::.ctor(System.String,System.Object)
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t JsonEncodedEventMessage__ctor_m1402911136_MetadataUsageId;
extern "C"  void JsonEncodedEventMessage__ctor_m1402911136 (JsonEncodedEventMessage_t989636229 * __this, String_t* ___name, Il2CppObject * ___payload, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonEncodedEventMessage__ctor_m1402911136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		ObjectU5BU5D_t11523773* L_1 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_2 = ___payload;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		JsonEncodedEventMessage__ctor_m3872889022(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.JsonEncodedEventMessage::.ctor(System.String,System.Object[])
extern "C"  void JsonEncodedEventMessage__ctor_m3872889022 (JsonEncodedEventMessage_t989636229 * __this, String_t* ___name, ObjectU5BU5D_t11523773* ___payloads, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		JsonEncodedEventMessage_set_name_m3782929222(__this, L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_1 = ___payloads;
		JsonEncodedEventMessage_set_args_m3437467364(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String SocketIOClient.Messages.JsonEncodedEventMessage::get_name()
extern "C"  String_t* JsonEncodedEventMessage_get_name_m2613943237 (JsonEncodedEventMessage_t989636229 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.JsonEncodedEventMessage::set_name(System.String)
extern "C"  void JsonEncodedEventMessage_set_name_m3782929222 (JsonEncodedEventMessage_t989636229 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CnameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Object[] SocketIOClient.Messages.JsonEncodedEventMessage::get_args()
extern "C"  ObjectU5BU5D_t11523773* JsonEncodedEventMessage_get_args_m908799847 (JsonEncodedEventMessage_t989636229 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = __this->get_U3CargsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.JsonEncodedEventMessage::set_args(System.Object[])
extern "C"  void JsonEncodedEventMessage_set_args_m3437467364 (JsonEncodedEventMessage_t989636229 * __this, ObjectU5BU5D_t11523773* ___value, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = ___value;
		__this->set_U3CargsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String SocketIOClient.Messages.JsonEncodedEventMessage::ToJsonString()
extern "C"  String_t* JsonEncodedEventMessage_ToJsonString_m4146288133 (JsonEncodedEventMessage_t989636229 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = SimpleJson_SerializeObject_m2690171523(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// SocketIOClient.Messages.JsonEncodedEventMessage SocketIOClient.Messages.JsonEncodedEventMessage::Deserialize(System.String)
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* SimpleJson_DeserializeObject_TisJsonEncodedEventMessage_t989636229_m3693202557_MethodInfo_var;
extern const uint32_t JsonEncodedEventMessage_Deserialize_m1080785149_MetadataUsageId;
extern "C"  JsonEncodedEventMessage_t989636229 * JsonEncodedEventMessage_Deserialize_m1080785149 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonEncodedEventMessage_Deserialize_m1080785149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonEncodedEventMessage_t989636229 * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (JsonEncodedEventMessage_t989636229 *)NULL;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		String_t* L_0 = ___jsonString;
		JsonEncodedEventMessage_t989636229 * L_1 = SimpleJson_DeserializeObject_TisJsonEncodedEventMessage_t989636229_m3693202557(NULL /*static, unused*/, L_0, /*hidden argument*/SimpleJson_DeserializeObject_TisJsonEncodedEventMessage_t989636229_m3693202557_MethodInfo_var);
		V_0 = L_1;
		goto IL_001a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t1967233988 *)__exception_local);
		Exception_t1967233988 * L_2 = V_1;
		Trace_WriteLine_m3524640299(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_001a;
	} // end catch (depth: 1)

IL_001a:
	{
		JsonEncodedEventMessage_t989636229 * L_3 = V_0;
		return L_3;
	}
}
// System.Void SocketIOClient.Messages.JSONMessage::.ctor()
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t JSONMessage__ctor_m527254596_MetadataUsageId;
extern "C"  void JSONMessage__ctor_m527254596 (JSONMessage_t3736498801 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONMessage__ctor_m527254596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message__ctor_m3082395484(__this, /*hidden argument*/NULL);
		Message_set_MessageType_m2319013183(__this, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.JSONMessage::.ctor(System.Object)
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t JSONMessage__ctor_m3855190992_MetadataUsageId;
extern "C"  void JSONMessage__ctor_m3855190992 (JSONMessage_t3736498801 * __this, Il2CppObject * ___jsonObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONMessage__ctor_m3855190992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSONMessage__ctor_m527254596(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___jsonObject;
		String_t* L_1 = SimpleJson_SerializeObject_m2690171523(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message_set_MessageText_m231655857(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.JSONMessage::.ctor(System.Object,System.Nullable`1<System.Int32>,System.String)
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t JSONMessage__ctor_m3161118484_MetadataUsageId;
extern "C"  void JSONMessage__ctor_m3161118484 (JSONMessage_t3736498801 * __this, Il2CppObject * ___jsonObject, Nullable_1_t1438485399  ___ackId, String_t* ___endpoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONMessage__ctor_m3161118484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSONMessage__ctor_m527254596(__this, /*hidden argument*/NULL);
		Nullable_1_t1438485399  L_0 = ___ackId;
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message_set_AckId_m1799485069(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___endpoint;
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String) */, __this, L_1);
		Il2CppObject * L_2 = ___jsonObject;
		String_t* L_3 = SimpleJson_SerializeObject_m2690171523(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Message_set_MessageText_m231655857(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.JSONMessage::SetMessage(System.Object)
extern "C"  void JSONMessage_SetMessage_m3407837295 (JSONMessage_t3736498801 * __this, Il2CppObject * ___value, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value;
		String_t* L_1 = SimpleJson_SerializeObject_m2690171523(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Message_set_MessageText_m231655857(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// SocketIOClient.Messages.JSONMessage SocketIOClient.Messages.JSONMessage::Deserialize(System.String)
extern TypeInfo* JSONMessage_t3736498801_il2cpp_TypeInfo_var;
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m2954236726_MethodInfo_var;
extern const uint32_t JSONMessage_Deserialize_m1677086293_MetadataUsageId;
extern "C"  JSONMessage_t3736498801 * JSONMessage_Deserialize_m1677086293 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONMessage_Deserialize_m1677086293_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONMessage_t3736498801 * V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	int32_t V_2 = 0;
	{
		JSONMessage_t3736498801 * L_0 = (JSONMessage_t3736498801 *)il2cpp_codegen_object_new(JSONMessage_t3736498801_il2cpp_TypeInfo_var);
		JSONMessage__ctor_m527254596(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONMessage_t3736498801 * L_1 = V_0;
		String_t* L_2 = ___rawMessage;
		NullCheck(L_1);
		Message_set_RawMessage_m2720466974(L_1, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___rawMessage;
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_4 = ((Message_t957426777_StaticFields*)Message_t957426777_il2cpp_TypeInfo_var->static_fields)->get_SPLITCHARS_1();
		NullCheck(L_3);
		StringU5BU5D_t2956870243* L_5 = String_Split_m434660345(L_3, L_4, 4, /*hidden argument*/NULL);
		V_1 = L_5;
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)4))))
		{
			goto IL_0050;
		}
	}
	{
		StringU5BU5D_t2956870243* L_7 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		int32_t L_8 = 1;
		bool L_9 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), (&V_2), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		JSONMessage_t3736498801 * L_10 = V_0;
		int32_t L_11 = V_2;
		Nullable_1_t1438485399  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Nullable_1__ctor_m2954236726(&L_12, L_11, /*hidden argument*/Nullable_1__ctor_m2954236726_MethodInfo_var);
		NullCheck(L_10);
		Message_set_AckId_m1799485069(L_10, L_12, /*hidden argument*/NULL);
	}

IL_003e:
	{
		JSONMessage_t3736498801 * L_13 = V_0;
		StringU5BU5D_t2956870243* L_14 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		int32_t L_15 = 2;
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String) */, L_13, ((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15))));
		JSONMessage_t3736498801 * L_16 = V_0;
		StringU5BU5D_t2956870243* L_17 = V_1;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		int32_t L_18 = 3;
		NullCheck(L_16);
		Message_set_MessageText_m231655857(L_16, ((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/NULL);
	}

IL_0050:
	{
		JSONMessage_t3736498801 * L_19 = V_0;
		return L_19;
	}
}
// System.Void SocketIOClient.Messages.Message::.ctor()
extern "C"  void Message__ctor_m3082395484 (Message_t957426777 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Message_set_MessageType_m2319013183(__this, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.Message::.ctor(System.String)
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m2954236726_MethodInfo_var;
extern const uint32_t Message__ctor_m523687206_MetadataUsageId;
extern "C"  void Message__ctor_m523687206 (Message_t957426777 * __this, String_t* ___rawMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Message__ctor_m523687206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Message__ctor_m3082395484(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___rawMessage;
		Message_set_RawMessage_m2720466974(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___rawMessage;
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_2 = ((Message_t957426777_StaticFields*)Message_t957426777_il2cpp_TypeInfo_var->static_fields)->get_SPLITCHARS_1();
		NullCheck(L_1);
		StringU5BU5D_t2956870243* L_3 = String_Split_m434660345(L_1, L_2, 4, /*hidden argument*/NULL);
		V_0 = L_3;
		StringU5BU5D_t2956870243* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))) == ((uint32_t)4))))
		{
			goto IL_0050;
		}
	}
	{
		StringU5BU5D_t2956870243* L_5 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		int32_t L_6 = 1;
		bool L_7 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6))), (&V_1), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_8 = V_1;
		Nullable_1_t1438485399  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Nullable_1__ctor_m2954236726(&L_9, L_8, /*hidden argument*/Nullable_1__ctor_m2954236726_MethodInfo_var);
		Message_set_AckId_m1799485069(__this, L_9, /*hidden argument*/NULL);
	}

IL_003e:
	{
		StringU5BU5D_t2956870243* L_10 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		int32_t L_11 = 2;
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String) */, __this, ((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		StringU5BU5D_t2956870243* L_12 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		int32_t L_13 = 3;
		Message_set_MessageText_m231655857(__this, ((L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13))), /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void SocketIOClient.Messages.Message::.cctor()
extern TypeInfo* Regex_t3802381858_il2cpp_TypeInfo_var;
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4095415371;
extern Il2CppCodeGenString* _stringLiteral2464313490;
extern const uint32_t Message__cctor_m582883281_MetadataUsageId;
extern "C"  void Message__cctor_m582883281 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Message__cctor_m582883281_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Regex_t3802381858 * L_0 = (Regex_t3802381858 *)il2cpp_codegen_object_new(Regex_t3802381858_il2cpp_TypeInfo_var);
		Regex__ctor_m2980635200(L_0, _stringLiteral4095415371, /*hidden argument*/NULL);
		((Message_t957426777_StaticFields*)Message_t957426777_il2cpp_TypeInfo_var->static_fields)->set_re_0(L_0);
		CharU5BU5D_t3416858730* L_1 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)58));
		((Message_t957426777_StaticFields*)Message_t957426777_il2cpp_TypeInfo_var->static_fields)->set_SPLITCHARS_1(L_1);
		Regex_t3802381858 * L_2 = (Regex_t3802381858 *)il2cpp_codegen_object_new(Regex_t3802381858_il2cpp_TypeInfo_var);
		Regex__ctor_m2068483208(L_2, _stringLiteral2464313490, 1, /*hidden argument*/NULL);
		((Message_t957426777_StaticFields*)Message_t957426777_il2cpp_TypeInfo_var->static_fields)->set_reMessageType_3(L_2);
		return;
	}
}
// System.String SocketIOClient.Messages.Message::get_RawMessage()
extern "C"  String_t* Message_get_RawMessage_m2306002733 (Message_t957426777 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CRawMessageU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.Message::set_RawMessage(System.String)
extern "C"  void Message_set_RawMessage_m2720466974 (Message_t957426777 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CRawMessageU3Ek__BackingField_4(L_0);
		return;
	}
}
// SocketIOClient.SocketIOMessageTypes SocketIOClient.Messages.Message::get_MessageType()
extern "C"  int32_t Message_get_MessageType_m2815837072 (Message_t957426777 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CMessageTypeU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.Message::set_MessageType(SocketIOClient.SocketIOMessageTypes)
extern "C"  void Message_set_MessageType_m2319013183 (Message_t957426777 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CMessageTypeU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> SocketIOClient.Messages.Message::get_AckId()
extern "C"  Nullable_1_t1438485399  Message_get_AckId_m1933451524 (Message_t957426777 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CAckIdU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.Message::set_AckId(System.Nullable`1<System.Int32>)
extern "C"  void Message_set_AckId_m1799485069 (Message_t957426777 * __this, Nullable_1_t1438485399  ___value, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value;
		__this->set_U3CAckIdU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String SocketIOClient.Messages.Message::get_Endpoint()
extern "C"  String_t* Message_get_Endpoint_m3485176771 (Message_t957426777 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CEndpointU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String)
extern "C"  void Message_set_Endpoint_m2483199368 (Message_t957426777 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CEndpointU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.String SocketIOClient.Messages.Message::get_MessageText()
extern "C"  String_t* Message_get_MessageText_m3757705288 (Message_t957426777 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CMessageTextU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.Message::set_MessageText(System.String)
extern "C"  void Message_set_MessageText_m231655857 (Message_t957426777 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CMessageTextU3Ek__BackingField_8(L_0);
		return;
	}
}
// SocketIOClient.Messages.JsonEncodedEventMessage SocketIOClient.Messages.Message::get_JsonEncodedMessage()
extern "C"  JsonEncodedEventMessage_t989636229 * Message_get_JsonEncodedMessage_m3705134714 (Message_t957426777 * __this, const MethodInfo* method)
{
	{
		JsonEncodedEventMessage_t989636229 * L_0 = VirtFuncInvoker0< JsonEncodedEventMessage_t989636229 * >::Invoke(11 /* SocketIOClient.Messages.JsonEncodedEventMessage SocketIOClient.Messages.Message::get_Json() */, __this);
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.Message::set_JsonEncodedMessage(SocketIOClient.Messages.JsonEncodedEventMessage)
extern "C"  void Message_set_JsonEncodedMessage_m1452199601 (Message_t957426777 * __this, JsonEncodedEventMessage_t989636229 * ___value, const MethodInfo* method)
{
	{
		JsonEncodedEventMessage_t989636229 * L_0 = ___value;
		__this->set__json_2(L_0);
		return;
	}
}
// SocketIOClient.Messages.JsonEncodedEventMessage SocketIOClient.Messages.Message::get_Json()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* JsonEncodedEventMessage_t989636229_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3002589;
extern const uint32_t Message_get_Json_m1852634497_MetadataUsageId;
extern "C"  JsonEncodedEventMessage_t989636229 * Message_get_Json_m1852634497 (Message_t957426777 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Message_get_Json_m1852634497_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonEncodedEventMessage_t989636229 * L_0 = __this->get__json_2();
		if (L_0)
		{
			goto IL_0066;
		}
	}
	{
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_005b;
		}
	}
	{
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, __this);
		NullCheck(L_3);
		bool L_4 = String_Contains_m3032019141(L_3, _stringLiteral3373707, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005b;
		}
	}
	{
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, __this);
		NullCheck(L_5);
		bool L_6 = String_Contains_m3032019141(L_5, _stringLiteral3002589, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, __this);
		JsonEncodedEventMessage_t989636229 * L_8 = JsonEncodedEventMessage_Deserialize_m1080785149(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->set__json_2(L_8);
		goto IL_0066;
	}

IL_005b:
	{
		JsonEncodedEventMessage_t989636229 * L_9 = (JsonEncodedEventMessage_t989636229 *)il2cpp_codegen_object_new(JsonEncodedEventMessage_t989636229_il2cpp_TypeInfo_var);
		JsonEncodedEventMessage__ctor_m614799024(L_9, /*hidden argument*/NULL);
		__this->set__json_2(L_9);
	}

IL_0066:
	{
		JsonEncodedEventMessage_t989636229 * L_10 = __this->get__json_2();
		return L_10;
	}
}
// System.Void SocketIOClient.Messages.Message::set_Json(SocketIOClient.Messages.JsonEncodedEventMessage)
extern "C"  void Message_set_Json_m2290864074 (Message_t957426777 * __this, JsonEncodedEventMessage_t989636229 * ___value, const MethodInfo* method)
{
	{
		JsonEncodedEventMessage_t989636229 * L_0 = ___value;
		__this->set__json_2(L_0);
		return;
	}
}
// System.String SocketIOClient.Messages.Message::get_Event()
extern "C"  String_t* Message_get_Event_m1041405390 (Message_t957426777 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CEventU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.Message::set_Event(System.String)
extern "C"  void Message_set_Event_m2037541675 (Message_t957426777 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CEventU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.String SocketIOClient.Messages.Message::get_Encoded()
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m404638567_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3808413405_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3146308204;
extern Il2CppCodeGenString* _stringLiteral3403096107;
extern const uint32_t Message_get_Encoded_m1541307010_MetadataUsageId;
extern "C"  String_t* Message_get_Encoded_m1541307010 (Message_t957426777 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Message_get_Encoded_m1541307010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Nullable_1_t1438485399  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Nullable_1_t1438485399  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t G_B3_0 = 0;
	ObjectU5BU5D_t11523773* G_B3_1 = NULL;
	ObjectU5BU5D_t11523773* G_B3_2 = NULL;
	String_t* G_B3_3 = NULL;
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t11523773* G_B2_1 = NULL;
	ObjectU5BU5D_t11523773* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B4_1 = 0;
	ObjectU5BU5D_t11523773* G_B4_2 = NULL;
	ObjectU5BU5D_t11523773* G_B4_3 = NULL;
	String_t* G_B4_4 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* SocketIOClient.SocketIOMessageTypes SocketIOClient.Messages.Message::get_MessageType() */, __this);
		V_0 = L_0;
		Nullable_1_t1438485399  L_1 = VirtFuncInvoker0< Nullable_1_t1438485399  >::Invoke(7 /* System.Nullable`1<System.Int32> SocketIOClient.Messages.Message::get_AckId() */, __this);
		V_1 = L_1;
		bool L_2 = Nullable_1_get_HasValue_m404638567((&V_1), /*hidden argument*/Nullable_1_get_HasValue_m404638567_MethodInfo_var);
		if (!L_2)
		{
			goto IL_006e;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_3 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_4 = V_0;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_6);
		ObjectU5BU5D_t11523773* L_7 = L_3;
		Nullable_1_t1438485399  L_8 = VirtFuncInvoker0< Nullable_1_t1438485399  >::Invoke(7 /* System.Nullable`1<System.Int32> SocketIOClient.Messages.Message::get_AckId() */, __this);
		V_2 = L_8;
		bool L_9 = Nullable_1_get_HasValue_m404638567((&V_2), /*hidden argument*/Nullable_1_get_HasValue_m404638567_MethodInfo_var);
		G_B2_0 = 1;
		G_B2_1 = L_7;
		G_B2_2 = L_7;
		G_B2_3 = _stringLiteral3146308204;
		if (!L_9)
		{
			G_B3_0 = 1;
			G_B3_1 = L_7;
			G_B3_2 = L_7;
			G_B3_3 = _stringLiteral3146308204;
			goto IL_004f;
		}
	}
	{
		int32_t L_10 = Nullable_1_get_Value_m3808413405((&V_2), /*hidden argument*/Nullable_1_get_Value_m3808413405_MethodInfo_var);
		G_B4_0 = L_10;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		G_B4_3 = G_B2_2;
		G_B4_4 = G_B2_3;
		goto IL_0050;
	}

IL_004f:
	{
		G_B4_0 = (-1);
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
		G_B4_3 = G_B3_2;
		G_B4_4 = G_B3_3;
	}

IL_0050:
	{
		int32_t L_11 = G_B4_0;
		Il2CppObject * L_12 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_11);
		NullCheck(G_B4_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B4_2, G_B4_1);
		ArrayElementTypeCheck (G_B4_2, L_12);
		(G_B4_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B4_1), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = G_B4_3;
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String SocketIOClient.Messages.Message::get_Endpoint() */, __this);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = L_13;
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, __this);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 3);
		ArrayElementTypeCheck (L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m4050103162(NULL /*static, unused*/, G_B4_4, L_15, /*hidden argument*/NULL);
		return L_17;
	}

IL_006e:
	{
		int32_t L_18 = V_0;
		int32_t L_19 = L_18;
		Il2CppObject * L_20 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_19);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String SocketIOClient.Messages.Message::get_Endpoint() */, __this);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String SocketIOClient.Messages.Message::get_MessageText() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral3403096107, L_20, L_21, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// SocketIOClient.Messages.IMessage SocketIOClient.Messages.Message::Factory(System.String)
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern TypeInfo* Heartbeat_t542713166_il2cpp_TypeInfo_var;
extern TypeInfo* EventMessage_t365350047_il2cpp_TypeInfo_var;
extern TypeInfo* AckMessage_t21730256_il2cpp_TypeInfo_var;
extern TypeInfo* NoopMessage_t1053258391_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TextMessage_t120705644_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_First_TisChar_t2778706699_m2365985763_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2846019092;
extern Il2CppCodeGenString* _stringLiteral3422793480;
extern const uint32_t Message_Factory_m1793067991_MetadataUsageId;
extern "C"  Il2CppObject * Message_Factory_m1793067991 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Message_Factory_m1793067991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0x0;
	uint16_t V_1 = 0x0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Regex_t3802381858 * L_0 = ((Message_t957426777_StaticFields*)Message_t957426777_il2cpp_TypeInfo_var->static_fields)->get_reMessageType_3();
		String_t* L_1 = ___rawMessage;
		NullCheck(L_0);
		bool L_2 = Regex_IsMatch_m2967892253(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_009e;
		}
	}
	{
		String_t* L_3 = ___rawMessage;
		uint16_t L_4 = Enumerable_First_TisChar_t2778706699_m2365985763(NULL /*static, unused*/, L_3, /*hidden argument*/Enumerable_First_TisChar_t2778706699_m2365985763_MethodInfo_var);
		V_0 = L_4;
		uint16_t L_5 = V_0;
		V_1 = L_5;
		uint16_t L_6 = V_1;
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)48))) == 0)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)48))) == 1)
		{
			goto IL_0052;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)48))) == 2)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)48))) == 3)
		{
			goto IL_005f;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)48))) == 4)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)48))) == 5)
		{
			goto IL_006d;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)48))) == 6)
		{
			goto IL_0074;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)48))) == 7)
		{
			goto IL_007b;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)48))) == 8)
		{
			goto IL_0082;
		}
	}
	{
		goto IL_0088;
	}

IL_004b:
	{
		String_t* L_7 = ___rawMessage;
		DisconnectMessage_t1581018141 * L_8 = DisconnectMessage_Deserialize_m2775835117(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0052:
	{
		String_t* L_9 = ___rawMessage;
		ConnectMessage_t3539960911 * L_10 = ConnectMessage_Deserialize_m3399944103(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0059:
	{
		Heartbeat_t542713166 * L_11 = (Heartbeat_t542713166 *)il2cpp_codegen_object_new(Heartbeat_t542713166_il2cpp_TypeInfo_var);
		Heartbeat__ctor_m2318316039(L_11, /*hidden argument*/NULL);
		return L_11;
	}

IL_005f:
	{
		String_t* L_12 = ___rawMessage;
		TextMessage_t120705644 * L_13 = TextMessage_Deserialize_m2513453003(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0066:
	{
		String_t* L_14 = ___rawMessage;
		JSONMessage_t3736498801 * L_15 = JSONMessage_Deserialize_m1677086293(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_006d:
	{
		String_t* L_16 = ___rawMessage;
		IL2CPP_RUNTIME_CLASS_INIT(EventMessage_t365350047_il2cpp_TypeInfo_var);
		EventMessage_t365350047 * L_17 = EventMessage_Deserialize_m4156674791(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_0074:
	{
		String_t* L_18 = ___rawMessage;
		IL2CPP_RUNTIME_CLASS_INIT(AckMessage_t21730256_il2cpp_TypeInfo_var);
		AckMessage_t21730256 * L_19 = AckMessage_Deserialize_m3207619207(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_19;
	}

IL_007b:
	{
		String_t* L_20 = ___rawMessage;
		ErrorMessage_t4000372081 * L_21 = ErrorMessage_Deserialize_m334360103(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_0082:
	{
		NoopMessage_t1053258391 * L_22 = (NoopMessage_t1053258391 *)il2cpp_codegen_object_new(NoopMessage_t1053258391_il2cpp_TypeInfo_var);
		NoopMessage__ctor_m2237861598(L_22, /*hidden argument*/NULL);
		return L_22;
	}

IL_0088:
	{
		String_t* L_23 = ___rawMessage;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral2846019092, L_23, /*hidden argument*/NULL);
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		TextMessage_t120705644 * L_25 = (TextMessage_t120705644 *)il2cpp_codegen_object_new(TextMessage_t120705644_il2cpp_TypeInfo_var);
		TextMessage__ctor_m374255273(L_25, /*hidden argument*/NULL);
		return L_25;
	}

IL_009e:
	{
		String_t* L_26 = ___rawMessage;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3422793480, L_26, /*hidden argument*/NULL);
		Trace_WriteLine_m3302529945(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		NoopMessage_t1053258391 * L_28 = (NoopMessage_t1053258391 *)il2cpp_codegen_object_new(NoopMessage_t1053258391_il2cpp_TypeInfo_var);
		NoopMessage__ctor_m2237861598(L_28, /*hidden argument*/NULL);
		return L_28;
	}
}
// System.Void SocketIOClient.Messages.NoopMessage::.ctor()
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t NoopMessage__ctor_m2237861598_MetadataUsageId;
extern "C"  void NoopMessage__ctor_m2237861598 (NoopMessage_t1053258391 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NoopMessage__ctor_m2237861598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message__ctor_m3082395484(__this, /*hidden argument*/NULL);
		Message_set_MessageType_m2319013183(__this, 8, /*hidden argument*/NULL);
		return;
	}
}
// SocketIOClient.Messages.NoopMessage SocketIOClient.Messages.NoopMessage::Deserialize(System.String)
extern TypeInfo* NoopMessage_t1053258391_il2cpp_TypeInfo_var;
extern const uint32_t NoopMessage_Deserialize_m4108323233_MetadataUsageId;
extern "C"  NoopMessage_t1053258391 * NoopMessage_Deserialize_m4108323233 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NoopMessage_Deserialize_m4108323233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NoopMessage_t1053258391 * L_0 = (NoopMessage_t1053258391 *)il2cpp_codegen_object_new(NoopMessage_t1053258391_il2cpp_TypeInfo_var);
		NoopMessage__ctor_m2237861598(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void SocketIOClient.Messages.TextMessage::.ctor()
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral954925063;
extern const uint32_t TextMessage__ctor_m374255273_MetadataUsageId;
extern "C"  void TextMessage__ctor_m374255273 (TextMessage_t120705644 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextMessage__ctor_m374255273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_eventName_10(_stringLiteral954925063);
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message__ctor_m3082395484(__this, /*hidden argument*/NULL);
		Message_set_MessageType_m2319013183(__this, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SocketIOClient.Messages.TextMessage::.ctor(System.String)
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const uint32_t TextMessage__ctor_m3376925433_MetadataUsageId;
extern "C"  void TextMessage__ctor_m3376925433 (TextMessage_t120705644 * __this, String_t* ___textMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextMessage__ctor_m3376925433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextMessage__ctor_m374255273(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___textMessage;
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		Message_set_MessageText_m231655857(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String SocketIOClient.Messages.TextMessage::get_Event()
extern "C"  String_t* TextMessage_get_Event_m3886918939 (TextMessage_t120705644 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_eventName_10();
		return L_0;
	}
}
// SocketIOClient.Messages.TextMessage SocketIOClient.Messages.TextMessage::Deserialize(System.String)
extern TypeInfo* TextMessage_t120705644_il2cpp_TypeInfo_var;
extern TypeInfo* Message_t957426777_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m2954236726_MethodInfo_var;
extern const uint32_t TextMessage_Deserialize_m2513453003_MetadataUsageId;
extern "C"  TextMessage_t120705644 * TextMessage_Deserialize_m2513453003 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextMessage_Deserialize_m2513453003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextMessage_t120705644 * V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	int32_t V_2 = 0;
	{
		TextMessage_t120705644 * L_0 = (TextMessage_t120705644 *)il2cpp_codegen_object_new(TextMessage_t120705644_il2cpp_TypeInfo_var);
		TextMessage__ctor_m374255273(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		TextMessage_t120705644 * L_1 = V_0;
		String_t* L_2 = ___rawMessage;
		NullCheck(L_1);
		Message_set_RawMessage_m2720466974(L_1, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___rawMessage;
		IL2CPP_RUNTIME_CLASS_INIT(Message_t957426777_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_4 = ((Message_t957426777_StaticFields*)Message_t957426777_il2cpp_TypeInfo_var->static_fields)->get_SPLITCHARS_1();
		NullCheck(L_3);
		StringU5BU5D_t2956870243* L_5 = String_Split_m434660345(L_3, L_4, 4, /*hidden argument*/NULL);
		V_1 = L_5;
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)4))))
		{
			goto IL_0055;
		}
	}
	{
		StringU5BU5D_t2956870243* L_7 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		int32_t L_8 = 1;
		bool L_9 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), (&V_2), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		TextMessage_t120705644 * L_10 = V_0;
		int32_t L_11 = V_2;
		Nullable_1_t1438485399  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Nullable_1__ctor_m2954236726(&L_12, L_11, /*hidden argument*/Nullable_1__ctor_m2954236726_MethodInfo_var);
		NullCheck(L_10);
		Message_set_AckId_m1799485069(L_10, L_12, /*hidden argument*/NULL);
	}

IL_003e:
	{
		TextMessage_t120705644 * L_13 = V_0;
		StringU5BU5D_t2956870243* L_14 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		int32_t L_15 = 2;
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void SocketIOClient.Messages.Message::set_Endpoint(System.String) */, L_13, ((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15))));
		TextMessage_t120705644 * L_16 = V_0;
		StringU5BU5D_t2956870243* L_17 = V_1;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		int32_t L_18 = 3;
		NullCheck(L_16);
		Message_set_MessageText_m231655857(L_16, ((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/NULL);
		goto IL_005c;
	}

IL_0055:
	{
		TextMessage_t120705644 * L_19 = V_0;
		String_t* L_20 = ___rawMessage;
		NullCheck(L_19);
		Message_set_MessageText_m231655857(L_19, L_20, /*hidden argument*/NULL);
	}

IL_005c:
	{
		TextMessage_t120705644 * L_21 = V_0;
		return L_21;
	}
}
// System.Void SocketIOClient.SocketIOHandshake::.ctor()
extern TypeInfo* List_1_t1765447871_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const uint32_t SocketIOHandshake__ctor_m2023930727_MetadataUsageId;
extern "C"  void SocketIOHandshake__ctor_m2023930727 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SocketIOHandshake__ctor_m2023930727_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1765447871 * L_0 = (List_1_t1765447871 *)il2cpp_codegen_object_new(List_1_t1765447871_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_0, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_Transports_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SocketIOClient.SocketIOHandshake::get_SID()
extern "C"  String_t* SocketIOHandshake_get_SID_m3604612263 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSIDU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SocketIOClient.SocketIOHandshake::set_SID(System.String)
extern "C"  void SocketIOHandshake_set_SID_m776236044 (SocketIOHandshake_t3315670474 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CSIDU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 SocketIOClient.SocketIOHandshake::get_HeartbeatTimeout()
extern "C"  int32_t SocketIOHandshake_get_HeartbeatTimeout_m1020887065 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CHeartbeatTimeoutU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SocketIOClient.SocketIOHandshake::set_HeartbeatTimeout(System.Int32)
extern "C"  void SocketIOHandshake_set_HeartbeatTimeout_m1801981520 (SocketIOHandshake_t3315670474 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CHeartbeatTimeoutU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String SocketIOClient.SocketIOHandshake::get_ErrorMessage()
extern "C"  String_t* SocketIOHandshake_get_ErrorMessage_m282465992 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CErrorMessageU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SocketIOClient.SocketIOHandshake::set_ErrorMessage(System.String)
extern "C"  void SocketIOHandshake_set_ErrorMessage_m2459766281 (SocketIOHandshake_t3315670474 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->set_U3CErrorMessageU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Boolean SocketIOClient.SocketIOHandshake::get_HadError()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SocketIOHandshake_get_HadError_m2556394295_MetadataUsageId;
extern "C"  bool SocketIOHandshake_get_HadError_m2556394295 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SocketIOHandshake_get_HadError_m2556394295_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = SocketIOHandshake_get_ErrorMessage_m282465992(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.TimeSpan SocketIOClient.SocketIOHandshake::get_HeartbeatInterval()
extern "C"  TimeSpan_t763862892  SocketIOHandshake_get_HeartbeatInterval_m4089311776 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = SocketIOHandshake_get_HeartbeatTimeout_m1020887065(__this, /*hidden argument*/NULL);
		TimeSpan_t763862892  L_1;
		memset(&L_1, 0, sizeof(L_1));
		TimeSpan__ctor_m4160332047(&L_1, 0, 0, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 SocketIOClient.SocketIOHandshake::get_ConnectionTimeout()
extern "C"  int32_t SocketIOHandshake_get_ConnectionTimeout_m586544049 (SocketIOHandshake_t3315670474 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CConnectionTimeoutU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void SocketIOClient.SocketIOHandshake::set_ConnectionTimeout(System.Int32)
extern "C"  void SocketIOHandshake_set_ConnectionTimeout_m3774365148 (SocketIOHandshake_t3315670474 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->set_U3CConnectionTimeoutU3Ek__BackingField_4(L_0);
		return;
	}
}
// SocketIOClient.SocketIOHandshake SocketIOClient.SocketIOHandshake::LoadFromString(System.String)
extern TypeInfo* SocketIOHandshake_t3315670474_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Count_TisString_t_m2139531282_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m1631114187_MethodInfo_var;
extern const uint32_t SocketIOHandshake_LoadFromString_m766272635_MetadataUsageId;
extern "C"  SocketIOHandshake_t3315670474 * SocketIOHandshake_LoadFromString_m766272635 (Il2CppObject * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SocketIOHandshake_LoadFromString_m766272635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SocketIOHandshake_t3315670474 * V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		SocketIOHandshake_t3315670474 * L_0 = (SocketIOHandshake_t3315670474 *)il2cpp_codegen_object_new(SocketIOHandshake_t3315670474_il2cpp_TypeInfo_var);
		SocketIOHandshake__ctor_m2023930727(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0098;
		}
	}
	{
		String_t* L_3 = ___value;
		CharU5BU5D_t3416858730* L_4 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)58));
		NullCheck(L_3);
		StringU5BU5D_t2956870243* L_5 = String_Split_m290179486(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		StringU5BU5D_t2956870243* L_6 = V_1;
		int32_t L_7 = Enumerable_Count_TisString_t_m2139531282(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_6, /*hidden argument*/Enumerable_Count_TisString_t_m2139531282_MethodInfo_var);
		if ((!(((uint32_t)L_7) == ((uint32_t)4))))
		{
			goto IL_0098;
		}
	}
	{
		V_2 = 0;
		V_3 = 0;
		SocketIOHandshake_t3315670474 * L_8 = V_0;
		StringU5BU5D_t2956870243* L_9 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		NullCheck(L_8);
		SocketIOHandshake_set_SID_m776236044(L_8, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), /*hidden argument*/NULL);
		StringU5BU5D_t2956870243* L_11 = V_1;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		int32_t L_12 = 1;
		bool L_13 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12))), (&V_2), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_14 = V_2;
		V_4 = (((int32_t)((int32_t)((double)((double)(((double)((double)L_14)))*(double)(0.75))))));
		SocketIOHandshake_t3315670474 * L_15 = V_0;
		int32_t L_16 = V_4;
		NullCheck(L_15);
		SocketIOHandshake_set_HeartbeatTimeout_m1801981520(L_15, L_16, /*hidden argument*/NULL);
	}

IL_0062:
	{
		StringU5BU5D_t2956870243* L_17 = V_1;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 2);
		int32_t L_18 = 2;
		bool L_19 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18))), (&V_3), /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0078;
		}
	}
	{
		SocketIOHandshake_t3315670474 * L_20 = V_0;
		int32_t L_21 = V_3;
		NullCheck(L_20);
		SocketIOHandshake_set_ConnectionTimeout_m3774365148(L_20, L_21, /*hidden argument*/NULL);
	}

IL_0078:
	{
		SocketIOHandshake_t3315670474 * L_22 = V_0;
		NullCheck(L_22);
		List_1_t1765447871 * L_23 = L_22->get_Transports_0();
		StringU5BU5D_t2956870243* L_24 = V_1;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		int32_t L_25 = 3;
		CharU5BU5D_t3416858730* L_26 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 0);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25))));
		StringU5BU5D_t2956870243* L_27 = String_Split_m290179486(((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25))), L_26, /*hidden argument*/NULL);
		NullCheck(L_23);
		List_1_AddRange_m1631114187(L_23, (Il2CppObject*)(Il2CppObject*)L_27, /*hidden argument*/List_1_AddRange_m1631114187_MethodInfo_var);
		SocketIOHandshake_t3315670474 * L_28 = V_0;
		return L_28;
	}

IL_0098:
	{
		return (SocketIOHandshake_t3315670474 *)NULL;
	}
}
// System.Void WaterController::.ctor()
extern "C"  void WaterController__ctor_m3966958808 (WaterController_t632785907 * __this, const MethodInfo* method)
{
	{
		__this->set_minScale_4((1.0f));
		__this->set_maxScale_5((7.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterController::Start()
extern "C"  void WaterController_Start_m2914096600 (WaterController_t632785907 * __this, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		__this->set_defaultYPosition_3(L_2);
		return;
	}
}
// System.Void WaterController::Update()
extern TypeInfo* ElementsController_t3637326867_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral83350775;
extern const uint32_t WaterController_Update_m148533557_MetadataUsageId;
extern "C"  void WaterController_Update_m148533557 (WaterController_t632785907 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaterController_Update_m148533557_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(ElementsController_t3637326867_il2cpp_TypeInfo_var);
		String_t* L_0 = ((ElementsController_t3637326867_StaticFields*)ElementsController_t3637326867_il2cpp_TypeInfo_var->static_fields)->get_activeElement_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_0, _stringLiteral83350775, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ElementsController_t3637326867_il2cpp_TypeInfo_var);
		float L_2 = ((ElementsController_t3637326867_StaticFields*)ElementsController_t3637326867_il2cpp_TypeInfo_var->static_fields)->get_intensityOfElement_1();
		if ((!(((float)L_2) > ((float)(0.0f)))))
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ElementsController_t3637326867_il2cpp_TypeInfo_var);
		float L_3 = ((ElementsController_t3637326867_StaticFields*)ElementsController_t3637326867_il2cpp_TypeInfo_var->static_fields)->get_intensityOfElement_1();
		V_0 = L_3;
		float L_4 = __this->get_waterLevel_2();
		if ((!(((float)L_4) < ((float)(1.0f)))))
		{
			goto IL_004d;
		}
	}
	{
		float L_5 = __this->get_waterLevel_2();
		float L_6 = V_0;
		__this->set_waterLevel_2(((float)((float)L_5+(float)L_6)));
		WaterController_changeWaterLevel_m2315523593(__this, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
// System.Void WaterController::changeWaterLevel()
extern "C"  void WaterController_changeWaterLevel_m2315523593 (WaterController_t632785907 * __this, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_localScale_m3886572677(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = __this->get_waterLevel_2();
		float L_3 = __this->get_maxScale_5();
		float L_4 = __this->get_minScale_4();
		float L_5 = __this->get_minScale_4();
		(&V_0)->set_y_2(((float)((float)((float)((float)((float)((float)L_2/(float)(1.0f)))*(float)((float)((float)L_3-(float)L_4))))+(float)L_5)));
		Transform_t284553113 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_7 = V_0;
		NullCheck(L_6);
		Transform_set_localScale_m310756934(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
