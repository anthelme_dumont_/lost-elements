﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "SimpleJson_SimpleJson_Reflection_SafeDictionary_2_1091022501MethodDeclarations.h"

// System.Boolean SimpleJson.Reflection.SafeDictionary`2<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>::TryGetValue(TKey,TValue&)
#define SafeDictionary_2_TryGetValue_m3671974100(__this, ___key, ___value, method) ((  bool (*) (SafeDictionary_2_t1629611267 *, Type_t *, SafeDictionary_2_t2250466201 **, const MethodInfo*))SafeDictionary_2_TryGetValue_m2241969233_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.SafeDictionary`2<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>::GetEnumerator()
#define SafeDictionary_2_GetEnumerator_m2982278087(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t1629611267 *, const MethodInfo*))SafeDictionary_2_GetEnumerator_m3507230980_gshared)(__this, method)
// System.Void SimpleJson.Reflection.SafeDictionary`2<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>::Add(TKey,TValue)
#define SafeDictionary_2_Add_m3838858265(__this, ___key, ___value, method) ((  void (*) (SafeDictionary_2_t1629611267 *, Type_t *, SafeDictionary_2_t2250466201 *, const MethodInfo*))SafeDictionary_2_Add_m3862242812_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.SafeDictionary`2<System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>>::.ctor()
#define SafeDictionary_2__ctor_m4284956700(__this, method) ((  void (*) (SafeDictionary_2_t1629611267 *, const MethodInfo*))SafeDictionary_2__ctor_m3700110041_gshared)(__this, method)
