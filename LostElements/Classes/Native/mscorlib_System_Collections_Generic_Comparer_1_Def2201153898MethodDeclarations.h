﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>
struct DefaultComparer_t2201153899;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22863200167.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::.ctor()
extern "C"  void DefaultComparer__ctor_m651254257_gshared (DefaultComparer_t2201153899 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m651254257(__this, method) ((  void (*) (DefaultComparer_t2201153899 *, const MethodInfo*))DefaultComparer__ctor_m651254257_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1966853598_gshared (DefaultComparer_t2201153899 * __this, KeyValuePair_2_t2863200167  ___x, KeyValuePair_2_t2863200167  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m1966853598(__this, ___x, ___y, method) ((  int32_t (*) (DefaultComparer_t2201153899 *, KeyValuePair_2_t2863200167 , KeyValuePair_2_t2863200167 , const MethodInfo*))DefaultComparer_Compare_m1966853598_gshared)(__this, ___x, ___y, method)
