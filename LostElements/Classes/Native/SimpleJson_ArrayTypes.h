﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// SimpleJson.Reflection.CacheResolver/MemberMap
struct MemberMap_t3346170946;
// SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>
struct SafeDictionary_2_t2250466201;
// SimpleJson.Reflection.CacheResolver/CtorDelegate
struct CtorDelegate_t2720072665;

#include "mscorlib_System_Array2840145358.h"
#include "SimpleJson_SimpleJson_Reflection_CacheResolver_Mem3346170946.h"
#include "SimpleJson_SimpleJson_Reflection_SafeDictionary_2_2250466201.h"
#include "SimpleJson_SimpleJson_Reflection_CacheResolver_Cto2720072665.h"

#pragma once
// SimpleJson.Reflection.CacheResolver/MemberMap[]
struct MemberMapU5BU5D_t1998383223  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) MemberMap_t3346170946 * m_Items[1];

public:
	inline MemberMap_t3346170946 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MemberMap_t3346170946 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MemberMap_t3346170946 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>[]
struct SafeDictionary_2U5BU5D_t615974372  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SafeDictionary_2_t2250466201 * m_Items[1];

public:
	inline SafeDictionary_2_t2250466201 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SafeDictionary_2_t2250466201 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SafeDictionary_2_t2250466201 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SimpleJson.Reflection.CacheResolver/CtorDelegate[]
struct CtorDelegateU5BU5D_t345480356  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CtorDelegate_t2720072665 * m_Items[1];

public:
	inline CtorDelegate_t2720072665 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CtorDelegate_t2720072665 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CtorDelegate_t2720072665 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
