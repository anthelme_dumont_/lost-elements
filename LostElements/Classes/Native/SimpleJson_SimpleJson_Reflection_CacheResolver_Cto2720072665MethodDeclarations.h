﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.Reflection.CacheResolver/CtorDelegate
struct CtorDelegate_t2720072665;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SimpleJson.Reflection.CacheResolver/CtorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void CtorDelegate__ctor_m1466384714 (CtorDelegate_t2720072665 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJson.Reflection.CacheResolver/CtorDelegate::Invoke()
extern "C"  Il2CppObject * CtorDelegate_Invoke_m4052220313 (CtorDelegate_t2720072665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" Il2CppObject * pinvoke_delegate_wrapper_CtorDelegate_t2720072665(Il2CppObject* delegate);
// System.IAsyncResult SimpleJson.Reflection.CacheResolver/CtorDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CtorDelegate_BeginInvoke_m4237814367 (CtorDelegate_t2720072665 * __this, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJson.Reflection.CacheResolver/CtorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CtorDelegate_EndInvoke_m3633296911 (CtorDelegate_t2720072665 * __this, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
