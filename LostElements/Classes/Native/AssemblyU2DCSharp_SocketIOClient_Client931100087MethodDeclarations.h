﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Client
struct Client_t931100087;
// System.String
struct String_t;
// System.EventHandler
struct EventHandler_t247020293;
// System.EventHandler`1<SocketIOClient.MessageEventArgs>
struct EventHandler_1_t2244256551;
// System.EventHandler`1<SocketIOClient.ErrorEventArgs>
struct EventHandler_1_t1185090118;
// SocketIOClient.SocketIOHandshake
struct SocketIOHandshake_t3315670474;
// SocketIOClient.IEndPointClient
struct IEndPointClient_t335482901;
// System.Action`1<SocketIOClient.Messages.IMessage>
struct Action_1_t3776982353;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// SocketIOClient.Messages.IMessage
struct IMessage_t3628529648;
// System.EventArgs
struct EventArgs_t516466188;
// WebSocket4Net.MessageReceivedEventArgs
struct MessageReceivedEventArgs_t3871897919;
// SuperSocket.ClientEngine.ErrorEventArgs
struct ErrorEventArgs_t1329266798;
// SocketIOClient.ErrorEventArgs
struct ErrorEventArgs_t342512475;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.Uri
struct Uri_t2776692961;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketVersion3981039553.h"
#include "mscorlib_System_EventHandler247020293.h"
#include "AssemblyU2DCSharp_SocketIOClient_SocketIOHandshake3315670474.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketState1654176186.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_EventArgs516466188.h"
#include "WebSocket4Net_WebSocket4Net_MessageReceivedEventAr3871897919.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1329266798.h"
#include "AssemblyU2DCSharp_SocketIOClient_ErrorEventArgs342512475.h"
#include "System_System_Uri2776692961.h"

// System.Void SocketIOClient.Client::.ctor(System.String)
extern "C"  void Client__ctor_m3888737708 (Client_t931100087 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::.ctor(System.String,WebSocket4Net.WebSocketVersion)
extern "C"  void Client__ctor_m4197002827 (Client_t931100087 * __this, String_t* ___url, int32_t ___socketVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::.cctor()
extern "C"  void Client__cctor_m3359426903 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::add_Opened(System.EventHandler)
extern "C"  void Client_add_Opened_m3947533902 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::remove_Opened(System.EventHandler)
extern "C"  void Client_remove_Opened_m3906053675 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::add_Message(System.EventHandler`1<SocketIOClient.MessageEventArgs>)
extern "C"  void Client_add_Message_m3258323997 (Client_t931100087 * __this, EventHandler_1_t2244256551 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::remove_Message(System.EventHandler`1<SocketIOClient.MessageEventArgs>)
extern "C"  void Client_remove_Message_m2508405114 (Client_t931100087 * __this, EventHandler_1_t2244256551 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::add_ConnectionRetryAttempt(System.EventHandler)
extern "C"  void Client_add_ConnectionRetryAttempt_m985458676 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::remove_ConnectionRetryAttempt(System.EventHandler)
extern "C"  void Client_remove_ConnectionRetryAttempt_m3773472209 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::add_HeartBeatTimerEvent(System.EventHandler)
extern "C"  void Client_add_HeartBeatTimerEvent_m765053468 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::remove_HeartBeatTimerEvent(System.EventHandler)
extern "C"  void Client_remove_HeartBeatTimerEvent_m2663000415 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::add_SocketConnectionClosed(System.EventHandler)
extern "C"  void Client_add_SocketConnectionClosed_m4008164794 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::remove_SocketConnectionClosed(System.EventHandler)
extern "C"  void Client_remove_SocketConnectionClosed_m2501211031 (Client_t931100087 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::add_Error(System.EventHandler`1<SocketIOClient.ErrorEventArgs>)
extern "C"  void Client_add_Error_m2029420669 (Client_t931100087 * __this, EventHandler_1_t1185090118 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::remove_Error(System.EventHandler`1<SocketIOClient.ErrorEventArgs>)
extern "C"  void Client_remove_Error_m3787381850 (Client_t931100087 * __this, EventHandler_1_t1185090118 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SocketIOClient.Client::get_RetryConnectionAttempts()
extern "C"  int32_t Client_get_RetryConnectionAttempts_m14417765 (Client_t931100087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::set_RetryConnectionAttempts(System.Int32)
extern "C"  void Client_set_RetryConnectionAttempts_m1697322804 (Client_t931100087 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.SocketIOHandshake SocketIOClient.Client::get_HandShake()
extern "C"  SocketIOHandshake_t3315670474 * Client_get_HandShake_m3549459883 (Client_t931100087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::set_HandShake(SocketIOClient.SocketIOHandshake)
extern "C"  void Client_set_HandShake_m1094163898 (Client_t931100087 * __this, SocketIOHandshake_t3315670474 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SocketIOClient.Client::get_IsConnected()
extern "C"  bool Client_get_IsConnected_m2845721918 (Client_t931100087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.WebSocketState SocketIOClient.Client::get_ReadyState()
extern "C"  int32_t Client_get_ReadyState_m3231756144 (Client_t931100087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::Connect()
extern "C"  void Client_Connect_m2191290334 (Client_t931100087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.IEndPointClient SocketIOClient.Client::Connect(System.String)
extern "C"  Il2CppObject * Client_Connect_m2054495046 (Client_t931100087 * __this, String_t* ___endPoint, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::ReConnect()
extern "C"  void Client_ReConnect_m3931142699 (Client_t931100087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::On(System.String,System.Action`1<SocketIOClient.Messages.IMessage>)
extern "C"  void Client_On_m873427727 (Client_t931100087 * __this, String_t* ___eventName, Action_1_t3776982353 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::On(System.String,System.String,System.Action`1<SocketIOClient.Messages.IMessage>)
extern "C"  void Client_On_m3237792651 (Client_t931100087 * __this, String_t* ___eventName, String_t* ___endPoint, Action_1_t3776982353 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::Emit(System.String,System.Object,System.String,System.Action`1<System.Object>)
extern "C"  void Client_Emit_m1575080133 (Client_t931100087 * __this, String_t* ___eventName, Il2CppObject * ___payload, String_t* ___endPoint, Action_1_t985559125 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::Emit(System.String,System.Object)
extern "C"  void Client_Emit_m1676394799 (Client_t931100087 * __this, String_t* ___eventName, Il2CppObject * ___payload, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::Send(SocketIOClient.Messages.IMessage)
extern "C"  void Client_Send_m1064201792 (Client_t931100087 * __this, Il2CppObject * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::Send(System.String)
extern "C"  void Client_Send_m3049882572 (Client_t931100087 * __this, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::Send_backup(System.String)
extern "C"  void Client_Send_backup_m2549701653 (Client_t931100087 * __this, String_t* ___rawEncodedMessageText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::OnMessageEvent(SocketIOClient.Messages.IMessage)
extern "C"  void Client_OnMessageEvent_m1312391754 (Client_t931100087 * __this, Il2CppObject * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::Close()
extern "C"  void Client_Close_m3497347628 (Client_t931100087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::closeHeartBeatTimer()
extern "C"  void Client_closeHeartBeatTimer_m1939381365 (Client_t931100087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::closeOutboundQueue()
extern "C"  void Client_closeOutboundQueue_m72276279 (Client_t931100087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::closeWebSocketClient()
extern "C"  void Client_closeWebSocketClient_m254982728 (Client_t931100087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::wsClient_OpenEvent(System.Object,System.EventArgs)
extern "C"  void Client_wsClient_OpenEvent_m1278167232 (Client_t931100087 * __this, Il2CppObject * ___sender, EventArgs_t516466188 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::wsClient_MessageReceived(System.Object,WebSocket4Net.MessageReceivedEventArgs)
extern "C"  void Client_wsClient_MessageReceived_m182254307 (Client_t931100087 * __this, Il2CppObject * ___sender, MessageReceivedEventArgs_t3871897919 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::wsClient_Closed(System.Object,System.EventArgs)
extern "C"  void Client_wsClient_Closed_m3514215282 (Client_t931100087 * __this, Il2CppObject * ___sender, EventArgs_t516466188 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::wsClient_Error(System.Object,SuperSocket.ClientEngine.ErrorEventArgs)
extern "C"  void Client_wsClient_Error_m50370738 (Client_t931100087 * __this, Il2CppObject * ___sender, ErrorEventArgs_t1329266798 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::OnErrorEvent(System.Object,SocketIOClient.ErrorEventArgs)
extern "C"  void Client_OnErrorEvent_m447362248 (Client_t931100087 * __this, Il2CppObject * ___sender, ErrorEventArgs_t342512475 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::OnSocketConnectionClosedEvent(System.Object,System.EventArgs)
extern "C"  void Client_OnSocketConnectionClosedEvent_m3101161164 (Client_t931100087 * __this, Il2CppObject * ___sender, EventArgs_t516466188 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::OnConnectionRetryAttemptEvent(System.Object,System.EventArgs)
extern "C"  void Client_OnConnectionRetryAttemptEvent_m3751111942 (Client_t931100087 * __this, Il2CppObject * ___sender, EventArgs_t516466188 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::OnHeartBeatTimerCallback(System.Object)
extern "C"  void Client_OnHeartBeatTimerCallback_m3114059801 (Client_t931100087 * __this, Il2CppObject * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::EndAsyncEvent(System.IAsyncResult)
extern "C"  void Client_EndAsyncEvent_m2464837398 (Client_t931100087 * __this, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::dequeuOutboundMessages()
extern "C"  void Client_dequeuOutboundMessages_m370415647 (Client_t931100087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.SocketIOHandshake SocketIOClient.Client::requestHandshake(System.Uri)
extern "C"  SocketIOHandshake_t3315670474 * Client_requestHandshake_m1889130752 (Client_t931100087 * __this, Uri_t2776692961 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::Dispose()
extern "C"  void Client_Dispose_m3021382611 (Client_t931100087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Client::Dispose(System.Boolean)
extern "C"  void Client_Dispose_m2625416202 (Client_t931100087 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
