﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.Timer
struct Timer_t3546110984;
// System.Threading.Thread
struct Thread_t1674723085;
// System.Collections.Concurrent.ConcurrentQueue`1<System.String>
struct ConcurrentQueue_1_t702095371;
// System.Object
struct Il2CppObject;
// System.Uri
struct Uri_t2776692961;
// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// SocketIOClient.Eventing.RegistrationManager
struct RegistrationManager_t998967514;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t2399218676;
// System.String
struct String_t;
// System.EventHandler
struct EventHandler_t247020293;
// System.EventHandler`1<SocketIOClient.MessageEventArgs>
struct EventHandler_1_t2244256551;
// System.EventHandler`1<SocketIOClient.ErrorEventArgs>
struct EventHandler_1_t1185090118;
// SocketIOClient.SocketIOHandshake
struct SocketIOHandshake_t3315670474;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "mscorlib_System_Object837106420.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketVersion3981039553.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIOClient.Client
struct  Client_t931100087  : public Il2CppObject
{
public:
	// System.Threading.Timer SocketIOClient.Client::socketHeartBeatTimer
	Timer_t3546110984 * ___socketHeartBeatTimer_0;
	// System.Threading.Thread SocketIOClient.Client::dequeuOutBoundMsgTask
	Thread_t1674723085 * ___dequeuOutBoundMsgTask_1;
	// System.Collections.Concurrent.ConcurrentQueue`1<System.String> SocketIOClient.Client::outboundQueue
	ConcurrentQueue_1_t702095371 * ___outboundQueue_2;
	// System.Int32 SocketIOClient.Client::retryConnectionCount
	int32_t ___retryConnectionCount_3;
	// System.Int32 SocketIOClient.Client::retryConnectionAttempts
	int32_t ___retryConnectionAttempts_4;
	// System.Uri SocketIOClient.Client::uri
	Uri_t2776692961 * ___uri_6;
	// WebSocket4Net.WebSocket SocketIOClient.Client::wsClient
	WebSocket_t713846903 * ___wsClient_7;
	// SocketIOClient.Eventing.RegistrationManager SocketIOClient.Client::registrationManager
	RegistrationManager_t998967514 * ___registrationManager_8;
	// WebSocket4Net.WebSocketVersion SocketIOClient.Client::socketVersion
	int32_t ___socketVersion_9;
	// System.Threading.ManualResetEvent SocketIOClient.Client::MessageQueueEmptyEvent
	ManualResetEvent_t2399218676 * ___MessageQueueEmptyEvent_10;
	// System.Threading.ManualResetEvent SocketIOClient.Client::ConnectionOpenEvent
	ManualResetEvent_t2399218676 * ___ConnectionOpenEvent_11;
	// System.String SocketIOClient.Client::LastErrorMessage
	String_t* ___LastErrorMessage_12;
	// System.EventHandler SocketIOClient.Client::Opened
	EventHandler_t247020293 * ___Opened_13;
	// System.EventHandler`1<SocketIOClient.MessageEventArgs> SocketIOClient.Client::Message
	EventHandler_1_t2244256551 * ___Message_14;
	// System.EventHandler SocketIOClient.Client::ConnectionRetryAttempt
	EventHandler_t247020293 * ___ConnectionRetryAttempt_15;
	// System.EventHandler SocketIOClient.Client::HeartBeatTimerEvent
	EventHandler_t247020293 * ___HeartBeatTimerEvent_16;
	// System.EventHandler SocketIOClient.Client::SocketConnectionClosed
	EventHandler_t247020293 * ___SocketConnectionClosed_17;
	// System.EventHandler`1<SocketIOClient.ErrorEventArgs> SocketIOClient.Client::Error
	EventHandler_1_t1185090118 * ___Error_18;
	// SocketIOClient.SocketIOHandshake SocketIOClient.Client::<HandShake>k__BackingField
	SocketIOHandshake_t3315670474 * ___U3CHandShakeU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_socketHeartBeatTimer_0() { return static_cast<int32_t>(offsetof(Client_t931100087, ___socketHeartBeatTimer_0)); }
	inline Timer_t3546110984 * get_socketHeartBeatTimer_0() const { return ___socketHeartBeatTimer_0; }
	inline Timer_t3546110984 ** get_address_of_socketHeartBeatTimer_0() { return &___socketHeartBeatTimer_0; }
	inline void set_socketHeartBeatTimer_0(Timer_t3546110984 * value)
	{
		___socketHeartBeatTimer_0 = value;
		Il2CppCodeGenWriteBarrier(&___socketHeartBeatTimer_0, value);
	}

	inline static int32_t get_offset_of_dequeuOutBoundMsgTask_1() { return static_cast<int32_t>(offsetof(Client_t931100087, ___dequeuOutBoundMsgTask_1)); }
	inline Thread_t1674723085 * get_dequeuOutBoundMsgTask_1() const { return ___dequeuOutBoundMsgTask_1; }
	inline Thread_t1674723085 ** get_address_of_dequeuOutBoundMsgTask_1() { return &___dequeuOutBoundMsgTask_1; }
	inline void set_dequeuOutBoundMsgTask_1(Thread_t1674723085 * value)
	{
		___dequeuOutBoundMsgTask_1 = value;
		Il2CppCodeGenWriteBarrier(&___dequeuOutBoundMsgTask_1, value);
	}

	inline static int32_t get_offset_of_outboundQueue_2() { return static_cast<int32_t>(offsetof(Client_t931100087, ___outboundQueue_2)); }
	inline ConcurrentQueue_1_t702095371 * get_outboundQueue_2() const { return ___outboundQueue_2; }
	inline ConcurrentQueue_1_t702095371 ** get_address_of_outboundQueue_2() { return &___outboundQueue_2; }
	inline void set_outboundQueue_2(ConcurrentQueue_1_t702095371 * value)
	{
		___outboundQueue_2 = value;
		Il2CppCodeGenWriteBarrier(&___outboundQueue_2, value);
	}

	inline static int32_t get_offset_of_retryConnectionCount_3() { return static_cast<int32_t>(offsetof(Client_t931100087, ___retryConnectionCount_3)); }
	inline int32_t get_retryConnectionCount_3() const { return ___retryConnectionCount_3; }
	inline int32_t* get_address_of_retryConnectionCount_3() { return &___retryConnectionCount_3; }
	inline void set_retryConnectionCount_3(int32_t value)
	{
		___retryConnectionCount_3 = value;
	}

	inline static int32_t get_offset_of_retryConnectionAttempts_4() { return static_cast<int32_t>(offsetof(Client_t931100087, ___retryConnectionAttempts_4)); }
	inline int32_t get_retryConnectionAttempts_4() const { return ___retryConnectionAttempts_4; }
	inline int32_t* get_address_of_retryConnectionAttempts_4() { return &___retryConnectionAttempts_4; }
	inline void set_retryConnectionAttempts_4(int32_t value)
	{
		___retryConnectionAttempts_4 = value;
	}

	inline static int32_t get_offset_of_uri_6() { return static_cast<int32_t>(offsetof(Client_t931100087, ___uri_6)); }
	inline Uri_t2776692961 * get_uri_6() const { return ___uri_6; }
	inline Uri_t2776692961 ** get_address_of_uri_6() { return &___uri_6; }
	inline void set_uri_6(Uri_t2776692961 * value)
	{
		___uri_6 = value;
		Il2CppCodeGenWriteBarrier(&___uri_6, value);
	}

	inline static int32_t get_offset_of_wsClient_7() { return static_cast<int32_t>(offsetof(Client_t931100087, ___wsClient_7)); }
	inline WebSocket_t713846903 * get_wsClient_7() const { return ___wsClient_7; }
	inline WebSocket_t713846903 ** get_address_of_wsClient_7() { return &___wsClient_7; }
	inline void set_wsClient_7(WebSocket_t713846903 * value)
	{
		___wsClient_7 = value;
		Il2CppCodeGenWriteBarrier(&___wsClient_7, value);
	}

	inline static int32_t get_offset_of_registrationManager_8() { return static_cast<int32_t>(offsetof(Client_t931100087, ___registrationManager_8)); }
	inline RegistrationManager_t998967514 * get_registrationManager_8() const { return ___registrationManager_8; }
	inline RegistrationManager_t998967514 ** get_address_of_registrationManager_8() { return &___registrationManager_8; }
	inline void set_registrationManager_8(RegistrationManager_t998967514 * value)
	{
		___registrationManager_8 = value;
		Il2CppCodeGenWriteBarrier(&___registrationManager_8, value);
	}

	inline static int32_t get_offset_of_socketVersion_9() { return static_cast<int32_t>(offsetof(Client_t931100087, ___socketVersion_9)); }
	inline int32_t get_socketVersion_9() const { return ___socketVersion_9; }
	inline int32_t* get_address_of_socketVersion_9() { return &___socketVersion_9; }
	inline void set_socketVersion_9(int32_t value)
	{
		___socketVersion_9 = value;
	}

	inline static int32_t get_offset_of_MessageQueueEmptyEvent_10() { return static_cast<int32_t>(offsetof(Client_t931100087, ___MessageQueueEmptyEvent_10)); }
	inline ManualResetEvent_t2399218676 * get_MessageQueueEmptyEvent_10() const { return ___MessageQueueEmptyEvent_10; }
	inline ManualResetEvent_t2399218676 ** get_address_of_MessageQueueEmptyEvent_10() { return &___MessageQueueEmptyEvent_10; }
	inline void set_MessageQueueEmptyEvent_10(ManualResetEvent_t2399218676 * value)
	{
		___MessageQueueEmptyEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___MessageQueueEmptyEvent_10, value);
	}

	inline static int32_t get_offset_of_ConnectionOpenEvent_11() { return static_cast<int32_t>(offsetof(Client_t931100087, ___ConnectionOpenEvent_11)); }
	inline ManualResetEvent_t2399218676 * get_ConnectionOpenEvent_11() const { return ___ConnectionOpenEvent_11; }
	inline ManualResetEvent_t2399218676 ** get_address_of_ConnectionOpenEvent_11() { return &___ConnectionOpenEvent_11; }
	inline void set_ConnectionOpenEvent_11(ManualResetEvent_t2399218676 * value)
	{
		___ConnectionOpenEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionOpenEvent_11, value);
	}

	inline static int32_t get_offset_of_LastErrorMessage_12() { return static_cast<int32_t>(offsetof(Client_t931100087, ___LastErrorMessage_12)); }
	inline String_t* get_LastErrorMessage_12() const { return ___LastErrorMessage_12; }
	inline String_t** get_address_of_LastErrorMessage_12() { return &___LastErrorMessage_12; }
	inline void set_LastErrorMessage_12(String_t* value)
	{
		___LastErrorMessage_12 = value;
		Il2CppCodeGenWriteBarrier(&___LastErrorMessage_12, value);
	}

	inline static int32_t get_offset_of_Opened_13() { return static_cast<int32_t>(offsetof(Client_t931100087, ___Opened_13)); }
	inline EventHandler_t247020293 * get_Opened_13() const { return ___Opened_13; }
	inline EventHandler_t247020293 ** get_address_of_Opened_13() { return &___Opened_13; }
	inline void set_Opened_13(EventHandler_t247020293 * value)
	{
		___Opened_13 = value;
		Il2CppCodeGenWriteBarrier(&___Opened_13, value);
	}

	inline static int32_t get_offset_of_Message_14() { return static_cast<int32_t>(offsetof(Client_t931100087, ___Message_14)); }
	inline EventHandler_1_t2244256551 * get_Message_14() const { return ___Message_14; }
	inline EventHandler_1_t2244256551 ** get_address_of_Message_14() { return &___Message_14; }
	inline void set_Message_14(EventHandler_1_t2244256551 * value)
	{
		___Message_14 = value;
		Il2CppCodeGenWriteBarrier(&___Message_14, value);
	}

	inline static int32_t get_offset_of_ConnectionRetryAttempt_15() { return static_cast<int32_t>(offsetof(Client_t931100087, ___ConnectionRetryAttempt_15)); }
	inline EventHandler_t247020293 * get_ConnectionRetryAttempt_15() const { return ___ConnectionRetryAttempt_15; }
	inline EventHandler_t247020293 ** get_address_of_ConnectionRetryAttempt_15() { return &___ConnectionRetryAttempt_15; }
	inline void set_ConnectionRetryAttempt_15(EventHandler_t247020293 * value)
	{
		___ConnectionRetryAttempt_15 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionRetryAttempt_15, value);
	}

	inline static int32_t get_offset_of_HeartBeatTimerEvent_16() { return static_cast<int32_t>(offsetof(Client_t931100087, ___HeartBeatTimerEvent_16)); }
	inline EventHandler_t247020293 * get_HeartBeatTimerEvent_16() const { return ___HeartBeatTimerEvent_16; }
	inline EventHandler_t247020293 ** get_address_of_HeartBeatTimerEvent_16() { return &___HeartBeatTimerEvent_16; }
	inline void set_HeartBeatTimerEvent_16(EventHandler_t247020293 * value)
	{
		___HeartBeatTimerEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___HeartBeatTimerEvent_16, value);
	}

	inline static int32_t get_offset_of_SocketConnectionClosed_17() { return static_cast<int32_t>(offsetof(Client_t931100087, ___SocketConnectionClosed_17)); }
	inline EventHandler_t247020293 * get_SocketConnectionClosed_17() const { return ___SocketConnectionClosed_17; }
	inline EventHandler_t247020293 ** get_address_of_SocketConnectionClosed_17() { return &___SocketConnectionClosed_17; }
	inline void set_SocketConnectionClosed_17(EventHandler_t247020293 * value)
	{
		___SocketConnectionClosed_17 = value;
		Il2CppCodeGenWriteBarrier(&___SocketConnectionClosed_17, value);
	}

	inline static int32_t get_offset_of_Error_18() { return static_cast<int32_t>(offsetof(Client_t931100087, ___Error_18)); }
	inline EventHandler_1_t1185090118 * get_Error_18() const { return ___Error_18; }
	inline EventHandler_1_t1185090118 ** get_address_of_Error_18() { return &___Error_18; }
	inline void set_Error_18(EventHandler_1_t1185090118 * value)
	{
		___Error_18 = value;
		Il2CppCodeGenWriteBarrier(&___Error_18, value);
	}

	inline static int32_t get_offset_of_U3CHandShakeU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(Client_t931100087, ___U3CHandShakeU3Ek__BackingField_19)); }
	inline SocketIOHandshake_t3315670474 * get_U3CHandShakeU3Ek__BackingField_19() const { return ___U3CHandShakeU3Ek__BackingField_19; }
	inline SocketIOHandshake_t3315670474 ** get_address_of_U3CHandShakeU3Ek__BackingField_19() { return &___U3CHandShakeU3Ek__BackingField_19; }
	inline void set_U3CHandShakeU3Ek__BackingField_19(SocketIOHandshake_t3315670474 * value)
	{
		___U3CHandShakeU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHandShakeU3Ek__BackingField_19, value);
	}
};

struct Client_t931100087_StaticFields
{
public:
	// System.Object SocketIOClient.Client::padLock
	Il2CppObject * ___padLock_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> SocketIOClient.Client::<>f__switch$map0
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map0_20;

public:
	inline static int32_t get_offset_of_padLock_5() { return static_cast<int32_t>(offsetof(Client_t931100087_StaticFields, ___padLock_5)); }
	inline Il2CppObject * get_padLock_5() const { return ___padLock_5; }
	inline Il2CppObject ** get_address_of_padLock_5() { return &___padLock_5; }
	inline void set_padLock_5(Il2CppObject * value)
	{
		___padLock_5 = value;
		Il2CppCodeGenWriteBarrier(&___padLock_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_20() { return static_cast<int32_t>(offsetof(Client_t931100087_StaticFields, ___U3CU3Ef__switchU24map0_20)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map0_20() const { return ___U3CU3Ef__switchU24map0_20; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map0_20() { return &___U3CU3Ef__switchU24map0_20; }
	inline void set_U3CU3Ef__switchU24map0_20(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map0_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
