﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SocketIOClient.Client
struct Client_t931100087;
// System.Action`1<SocketIOClient.Messages.IMessage>
struct Action_1_t3776982353;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketController
struct  SocketController_t1065234319  : public MonoBehaviour_t3012272455
{
public:
	// SocketIOClient.Client SocketController::client
	Client_t931100087 * ___client_2;

public:
	inline static int32_t get_offset_of_client_2() { return static_cast<int32_t>(offsetof(SocketController_t1065234319, ___client_2)); }
	inline Client_t931100087 * get_client_2() const { return ___client_2; }
	inline Client_t931100087 ** get_address_of_client_2() { return &___client_2; }
	inline void set_client_2(Client_t931100087 * value)
	{
		___client_2 = value;
		Il2CppCodeGenWriteBarrier(&___client_2, value);
	}
};

struct SocketController_t1065234319_StaticFields
{
public:
	// System.Action`1<SocketIOClient.Messages.IMessage> SocketController::<>f__am$cache1
	Action_1_t3776982353 * ___U3CU3Ef__amU24cache1_3;
	// System.Action`1<SocketIOClient.Messages.IMessage> SocketController::<>f__am$cache2
	Action_1_t3776982353 * ___U3CU3Ef__amU24cache2_4;
	// System.Action`1<SocketIOClient.Messages.IMessage> SocketController::<>f__am$cache3
	Action_1_t3776982353 * ___U3CU3Ef__amU24cache3_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(SocketController_t1065234319_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_1_t3776982353 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_1_t3776982353 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_1_t3776982353 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(SocketController_t1065234319_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Action_1_t3776982353 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Action_1_t3776982353 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Action_1_t3776982353 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_5() { return static_cast<int32_t>(offsetof(SocketController_t1065234319_StaticFields, ___U3CU3Ef__amU24cache3_5)); }
	inline Action_1_t3776982353 * get_U3CU3Ef__amU24cache3_5() const { return ___U3CU3Ef__amU24cache3_5; }
	inline Action_1_t3776982353 ** get_address_of_U3CU3Ef__amU24cache3_5() { return &___U3CU3Ef__amU24cache3_5; }
	inline void set_U3CU3Ef__amU24cache3_5(Action_1_t3776982353 * value)
	{
		___U3CU3Ef__amU24cache3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
