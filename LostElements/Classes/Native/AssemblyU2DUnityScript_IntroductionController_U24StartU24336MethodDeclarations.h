﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IntroductionController/$Start$3/$
struct U24_t36;
// IntroductionController
struct IntroductionController_t4287196022;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_IntroductionController4287196022.h"

// System.Void IntroductionController/$Start$3/$::.ctor(IntroductionController)
extern "C"  void U24__ctor_m3366870597 (U24_t36 * __this, IntroductionController_t4287196022 * ___self_, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IntroductionController/$Start$3/$::MoveNext()
extern "C"  bool U24_MoveNext_m3900849807 (U24_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
