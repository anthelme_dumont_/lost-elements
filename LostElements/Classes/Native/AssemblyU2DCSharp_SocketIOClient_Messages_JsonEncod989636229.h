﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIOClient.Messages.JsonEncodedEventMessage
struct  JsonEncodedEventMessage_t989636229  : public Il2CppObject
{
public:
	// System.String SocketIOClient.Messages.JsonEncodedEventMessage::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Object[] SocketIOClient.Messages.JsonEncodedEventMessage::<args>k__BackingField
	ObjectU5BU5D_t11523773* ___U3CargsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonEncodedEventMessage_t989636229, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnameU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CargsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonEncodedEventMessage_t989636229, ___U3CargsU3Ek__BackingField_1)); }
	inline ObjectU5BU5D_t11523773* get_U3CargsU3Ek__BackingField_1() const { return ___U3CargsU3Ek__BackingField_1; }
	inline ObjectU5BU5D_t11523773** get_address_of_U3CargsU3Ek__BackingField_1() { return &___U3CargsU3Ek__BackingField_1; }
	inline void set_U3CargsU3Ek__BackingField_1(ObjectU5BU5D_t11523773* value)
	{
		___U3CargsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CargsU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
