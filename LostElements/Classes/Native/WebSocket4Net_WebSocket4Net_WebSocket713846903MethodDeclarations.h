﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.WebSocket
struct WebSocket_t713846903;
// SuperSocket.ClientEngine.TcpClientSession
struct TcpClientSession_t1301539049;
// WebSocket4Net.Protocol.IProtocolProcessor
struct IProtocolProcessor_t4208156067;
// System.Uri
struct Uri_t2776692961;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t3650470111;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct List_1_t2891677073;
// SuperSocket.ClientEngine.IProxyConnector
struct IProxyConnector_t1374585095;
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>
struct IClientCommandReader_1_t3175310917;
// System.Net.EndPoint
struct EndPoint_t1294049535;
// System.Object
struct Il2CppObject;
// SuperSocket.ClientEngine.DataEventArgs
struct DataEventArgs_t3216211148;
// SuperSocket.ClientEngine.ErrorEventArgs
struct ErrorEventArgs_t1329266798;
// System.EventArgs
struct EventArgs_t516466188;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// System.EventHandler
struct EventHandler_t247020293;
// System.EventHandler`1<WebSocket4Net.MessageReceivedEventArgs>
struct EventHandler_1_t419508266;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// WebSocket4Net.WebSocketCommandInfo
struct WebSocketCommandInfo_t3536916738;
// System.Exception
struct Exception_t1967233988;
// System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>
struct EventHandler_1_t2171844441;

#include "codegen/il2cpp-codegen.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1301539049.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketVersion3981039553.h"
#include "mscorlib_System_DateTime339033936.h"
#include "System_System_Uri2776692961.h"
#include "mscorlib_System_String968488902.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketState1654176186.h"
#include "mscorlib_System_Object837106420.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn3216211148.h"
#include "SuperSocket_ClientEngine_Core_SuperSocket_ClientEn1329266798.h"
#include "mscorlib_System_EventArgs516466188.h"
#include "mscorlib_System_EventHandler247020293.h"
#include "WebSocket4Net_WebSocket4Net_WebSocketCommandInfo3536916738.h"
#include "mscorlib_System_Exception1967233988.h"

// SuperSocket.ClientEngine.TcpClientSession WebSocket4Net.WebSocket::get_Client()
extern "C"  TcpClientSession_t1301539049 * WebSocket_get_Client_m2705060304 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_Client(SuperSocket.ClientEngine.TcpClientSession)
extern "C"  void WebSocket_set_Client_m1471644827 (WebSocket_t713846903 * __this, TcpClientSession_t1301539049 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_Version(WebSocket4Net.WebSocketVersion)
extern "C"  void WebSocket_set_Version_m3858432938 (WebSocket_t713846903 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_LastActiveTime(System.DateTime)
extern "C"  void WebSocket_set_LastActiveTime_m147571246 (WebSocket_t713846903 * __this, DateTime_t339033936  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.WebSocket::get_EnableAutoSendPing()
extern "C"  bool WebSocket_get_EnableAutoSendPing_m3480068097 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_EnableAutoSendPing(System.Boolean)
extern "C"  void WebSocket_set_EnableAutoSendPing_m1331589028 (WebSocket_t713846903 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocket4Net.WebSocket::get_AutoSendPingInterval()
extern "C"  int32_t WebSocket_get_AutoSendPingInterval_m458397481 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_AutoSendPingInterval(System.Int32)
extern "C"  void WebSocket_set_AutoSendPingInterval_m3191023104 (WebSocket_t713846903 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.Protocol.IProtocolProcessor WebSocket4Net.WebSocket::get_ProtocolProcessor()
extern "C"  Il2CppObject * WebSocket_get_ProtocolProcessor_m1265601209 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_ProtocolProcessor(WebSocket4Net.Protocol.IProtocolProcessor)
extern "C"  void WebSocket_set_ProtocolProcessor_m2566430094 (WebSocket_t713846903 * __this, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri WebSocket4Net.WebSocket::get_TargetUri()
extern "C"  Uri_t2776692961 * WebSocket_get_TargetUri_m4211545892 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_TargetUri(System.Uri)
extern "C"  void WebSocket_set_TargetUri_m3334962991 (WebSocket_t713846903 * __this, Uri_t2776692961 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.WebSocket::get_SubProtocol()
extern "C"  String_t* WebSocket_get_SubProtocol_m1721628912 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_SubProtocol(System.String)
extern "C"  void WebSocket_set_SubProtocol_m2456245641 (WebSocket_t713846903 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.Object> WebSocket4Net.WebSocket::get_Items()
extern "C"  Il2CppObject* WebSocket_get_Items_m1025846418 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_Items(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C"  void WebSocket_set_Items_m3678737243 (WebSocket_t713846903 * __this, Il2CppObject* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> WebSocket4Net.WebSocket::get_Cookies()
extern "C"  List_1_t2891677073 * WebSocket_get_Cookies_m794256048 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_Cookies(System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern "C"  void WebSocket_set_Cookies_m3968108603 (WebSocket_t713846903 * __this, List_1_t2891677073 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> WebSocket4Net.WebSocket::get_CustomHeaderItems()
extern "C"  List_1_t2891677073 * WebSocket_get_CustomHeaderItems_m1966151779 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_CustomHeaderItems(System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern "C"  void WebSocket_set_CustomHeaderItems_m3818569704 (WebSocket_t713846903 * __this, List_1_t2891677073 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.WebSocketState WebSocket4Net.WebSocket::get_State()
extern "C"  int32_t WebSocket_get_State_m3481149631 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_State(WebSocket4Net.WebSocketState)
extern "C"  void WebSocket_set_State_m2318924618 (WebSocket_t713846903 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.WebSocket::get_Handshaked()
extern "C"  bool WebSocket_get_Handshaked_m3019381250 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_Handshaked(System.Boolean)
extern "C"  void WebSocket_set_Handshaked_m224100325 (WebSocket_t713846903 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SuperSocket.ClientEngine.IProxyConnector WebSocket4Net.WebSocket::get_Proxy()
extern "C"  Il2CppObject * WebSocket_get_Proxy_m857243229 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo> WebSocket4Net.WebSocket::get_CommandReader()
extern "C"  Il2CppObject* WebSocket_get_CommandReader_m2191282226 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_CommandReader(SuperSocket.ClientEngine.Protocol.IClientCommandReader`1<WebSocket4Net.WebSocketCommandInfo>)
extern "C"  void WebSocket_set_CommandReader_m748575549 (WebSocket_t713846903 * __this, Il2CppObject* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.WebSocket::get_NotSpecifiedVersion()
extern "C"  bool WebSocket_get_NotSpecifiedVersion_m3038260680 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_NotSpecifiedVersion(System.Boolean)
extern "C"  void WebSocket_set_NotSpecifiedVersion_m795243219 (WebSocket_t713846903 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.WebSocket::get_LastPongResponse()
extern "C"  String_t* WebSocket_get_LastPongResponse_m1283402937 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_LastPongResponse(System.String)
extern "C"  void WebSocket_set_LastPongResponse_m668946386 (WebSocket_t713846903 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.WebSocket::get_HandshakeHost()
extern "C"  String_t* WebSocket_get_HandshakeHost_m3944612535 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_HandshakeHost(System.String)
extern "C"  void WebSocket_set_HandshakeHost_m3139511650 (WebSocket_t713846903 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocket4Net.WebSocket::get_Origin()
extern "C"  String_t* WebSocket_get_Origin_m775398224 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::set_Origin(System.String)
extern "C"  void WebSocket_set_Origin_m1035068891 (WebSocket_t713846903 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::.cctor()
extern "C"  void WebSocket__cctor_m2314250317 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.EndPoint WebSocket4Net.WebSocket::ResolveUri(System.String,System.Int32,System.Int32&)
extern "C"  EndPoint_t1294049535 * WebSocket_ResolveUri_m2866265440 (WebSocket_t713846903 * __this, String_t* ___uri, int32_t ___defaultPort, int32_t* ___port, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SuperSocket.ClientEngine.TcpClientSession WebSocket4Net.WebSocket::CreateClient(System.String)
extern "C"  TcpClientSession_t1301539049 * WebSocket_CreateClient_m3769000575 (WebSocket_t713846903 * __this, String_t* ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SuperSocket.ClientEngine.TcpClientSession WebSocket4Net.WebSocket::CreateSecureClient(System.String)
extern "C"  TcpClientSession_t1301539049 * WebSocket_CreateSecureClient_m3104541832 (WebSocket_t713846903 * __this, String_t* ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::Initialize(System.String,System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.String,System.String,WebSocket4Net.WebSocketVersion)
extern "C"  void WebSocket_Initialize_m3081830635 (WebSocket_t713846903 * __this, String_t* ___uri, String_t* ___subProtocol, List_1_t2891677073 * ___cookies, List_1_t2891677073 * ___customHeaderItems, String_t* ___userAgent, String_t* ___origin, int32_t ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::client_DataReceived(System.Object,SuperSocket.ClientEngine.DataEventArgs)
extern "C"  void WebSocket_client_DataReceived_m3695014531 (WebSocket_t713846903 * __this, Il2CppObject * ___sender, DataEventArgs_t3216211148 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::client_Error(System.Object,SuperSocket.ClientEngine.ErrorEventArgs)
extern "C"  void WebSocket_client_Error_m829371704 (WebSocket_t713846903 * __this, Il2CppObject * ___sender, ErrorEventArgs_t1329266798 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::client_Closed(System.Object,System.EventArgs)
extern "C"  void WebSocket_client_Closed_m2085449848 (WebSocket_t713846903 * __this, Il2CppObject * ___sender, EventArgs_t516466188 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::client_Connected(System.Object,System.EventArgs)
extern "C"  void WebSocket_client_Connected_m4243894931 (WebSocket_t713846903 * __this, Il2CppObject * ___sender, EventArgs_t516466188 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.WebSocket::GetAvailableProcessor(System.Int32[])
extern "C"  bool WebSocket_GetAvailableProcessor_m3999224164 (WebSocket_t713846903 * __this, Int32U5BU5D_t1809983122* ___availableVersions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::Open()
extern "C"  void WebSocket_Open_m1696656814 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.Protocol.IProtocolProcessor WebSocket4Net.WebSocket::GetProtocolProcessor(WebSocket4Net.WebSocketVersion)
extern "C"  Il2CppObject * WebSocket_GetProtocolProcessor_m3470676079 (Il2CppObject * __this /* static, unused */, int32_t ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::OnConnected()
extern "C"  void WebSocket_OnConnected_m3753639976 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::OnHandshaked()
extern "C"  void WebSocket_OnHandshaked_m3855060592 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::OnPingTimerCallback(System.Object)
extern "C"  void WebSocket_OnPingTimerCallback_m125910333 (WebSocket_t713846903 * __this, Il2CppObject * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::add_Opened(System.EventHandler)
extern "C"  void WebSocket_add_Opened_m1939620504 (WebSocket_t713846903 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::remove_Opened(System.EventHandler)
extern "C"  void WebSocket_remove_Opened_m2167545249 (WebSocket_t713846903 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::add_MessageReceived(System.EventHandler`1<WebSocket4Net.MessageReceivedEventArgs>)
extern "C"  void WebSocket_add_MessageReceived_m1609756021 (WebSocket_t713846903 * __this, EventHandler_1_t419508266 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::remove_MessageReceived(System.EventHandler`1<WebSocket4Net.MessageReceivedEventArgs>)
extern "C"  void WebSocket_remove_MessageReceived_m4109017036 (WebSocket_t713846903 * __this, EventHandler_1_t419508266 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::FireMessageReceived(System.String)
extern "C"  void WebSocket_FireMessageReceived_m663897554 (WebSocket_t713846903 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::FireDataReceived(System.Byte[])
extern "C"  void WebSocket_FireDataReceived_m2475134404 (WebSocket_t713846903 * __this, ByteU5BU5D_t58506160* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocket4Net.WebSocket::EnsureWebSocketOpen()
extern "C"  bool WebSocket_EnsureWebSocketOpen_m1878363049 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::Send(System.String)
extern "C"  void WebSocket_Send_m1750322966 (WebSocket_t713846903 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::OnClosed()
extern "C"  void WebSocket_OnClosed_m161702351 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::Close()
extern "C"  void WebSocket_Close_m3186537590 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::Close(System.String)
extern "C"  void WebSocket_Close_m3107084876 (WebSocket_t713846903 * __this, String_t* ___reason, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::Close(System.Int32,System.String)
extern "C"  void WebSocket_Close_m946219203 (WebSocket_t713846903 * __this, int32_t ___statusCode, String_t* ___reason, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::CloseWithoutHandshake()
extern "C"  void WebSocket_CloseWithoutHandshake_m1514871525 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::ExecuteCommand(WebSocket4Net.WebSocketCommandInfo)
extern "C"  void WebSocket_ExecuteCommand_m123150858 (WebSocket_t713846903 * __this, WebSocketCommandInfo_t3536916738 * ___commandInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::OnDataReceived(System.Byte[],System.Int32,System.Int32)
extern "C"  void WebSocket_OnDataReceived_m2703049051 (WebSocket_t713846903 * __this, ByteU5BU5D_t58506160* ___data, int32_t ___offset, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::FireError(System.Exception)
extern "C"  void WebSocket_FireError_m4124567456 (WebSocket_t713846903 * __this, Exception_t1967233988 * ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::add_Closed(System.EventHandler)
extern "C"  void WebSocket_add_Closed_m3528784245 (WebSocket_t713846903 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::remove_Closed(System.EventHandler)
extern "C"  void WebSocket_remove_Closed_m3756708990 (WebSocket_t713846903 * __this, EventHandler_t247020293 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::FireClosed()
extern "C"  void WebSocket_FireClosed_m2436478566 (WebSocket_t713846903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::add_Error(System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>)
extern "C"  void WebSocket_add_Error_m447077374 (WebSocket_t713846903 * __this, EventHandler_1_t2171844441 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::remove_Error(System.EventHandler`1<SuperSocket.ClientEngine.ErrorEventArgs>)
extern "C"  void WebSocket_remove_Error_m1728502727 (WebSocket_t713846903 * __this, EventHandler_1_t2171844441 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::OnError(SuperSocket.ClientEngine.ErrorEventArgs)
extern "C"  void WebSocket_OnError_m112179675 (WebSocket_t713846903 * __this, ErrorEventArgs_t1329266798 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::OnError(System.Exception)
extern "C"  void WebSocket_OnError_m1918295319 (WebSocket_t713846903 * __this, Exception_t1967233988 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::.ctor(System.String,System.String,WebSocket4Net.WebSocketVersion)
extern "C"  void WebSocket__ctor_m3811251545 (WebSocket_t713846903 * __this, String_t* ___uri, String_t* ___subProtocol, int32_t ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::.ctor(System.String,System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,WebSocket4Net.WebSocketVersion)
extern "C"  void WebSocket__ctor_m1186845652 (WebSocket_t713846903 * __this, String_t* ___uri, String_t* ___subProtocol, List_1_t2891677073 * ___cookies, int32_t ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::.ctor(System.String,System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.String,WebSocket4Net.WebSocketVersion)
extern "C"  void WebSocket__ctor_m2339403475 (WebSocket_t713846903 * __this, String_t* ___uri, String_t* ___subProtocol, List_1_t2891677073 * ___cookies, List_1_t2891677073 * ___customHeaderItems, String_t* ___userAgent, int32_t ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.WebSocket::.ctor(System.String,System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.String,System.String,WebSocket4Net.WebSocketVersion)
extern "C"  void WebSocket__ctor_m253146647 (WebSocket_t713846903 * __this, String_t* ___uri, String_t* ___subProtocol, List_1_t2891677073 * ___cookies, List_1_t2891677073 * ___customHeaderItems, String_t* ___userAgent, String_t* ___origin, int32_t ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
