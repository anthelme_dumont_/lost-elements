﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.OrderedEnumerable`1<System.Int32>
struct OrderedEnumerable_1_t3912325915;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1424601847;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t35553939;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.OrderedEnumerable`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void OrderedEnumerable_1__ctor_m2414700580_gshared (OrderedEnumerable_1_t3912325915 * __this, Il2CppObject* ___source, const MethodInfo* method);
#define OrderedEnumerable_1__ctor_m2414700580(__this, ___source, method) ((  void (*) (OrderedEnumerable_1_t3912325915 *, Il2CppObject*, const MethodInfo*))OrderedEnumerable_1__ctor_m2414700580_gshared)(__this, ___source, method)
// System.Collections.IEnumerator System.Linq.OrderedEnumerable`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m533113392_gshared (OrderedEnumerable_1_t3912325915 * __this, const MethodInfo* method);
#define OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m533113392(__this, method) ((  Il2CppObject * (*) (OrderedEnumerable_1_t3912325915 *, const MethodInfo*))OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m533113392_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1<System.Int32>::GetEnumerator()
extern "C"  Il2CppObject* OrderedEnumerable_1_GetEnumerator_m182312797_gshared (OrderedEnumerable_1_t3912325915 * __this, const MethodInfo* method);
#define OrderedEnumerable_1_GetEnumerator_m182312797(__this, method) ((  Il2CppObject* (*) (OrderedEnumerable_1_t3912325915 *, const MethodInfo*))OrderedEnumerable_1_GetEnumerator_m182312797_gshared)(__this, method)
