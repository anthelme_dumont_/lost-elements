﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1545219184(__this, ___l, method) ((  void (*) (Enumerator_t612090107 *, List_1_t2526307115 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4016497122(__this, method) ((  void (*) (Enumerator_t612090107 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1379467790(__this, method) ((  Il2CppObject * (*) (Enumerator_t612090107 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>::Dispose()
#define Enumerator_Dispose_m2517947989(__this, method) ((  void (*) (Enumerator_t612090107 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>::VerifyState()
#define Enumerator_VerifyState_m2711315214(__this, method) ((  void (*) (Enumerator_t612090107 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>::MoveNext()
#define Enumerator_MoveNext_m1931998670(__this, method) ((  bool (*) (Enumerator_t612090107 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Object>>::get_Current()
#define Enumerator_get_Current_m611797381(__this, method) ((  ArraySegmentEx_1_t1729348146 * (*) (Enumerator_t612090107 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
