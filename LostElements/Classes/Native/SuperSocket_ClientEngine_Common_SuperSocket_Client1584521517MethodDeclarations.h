﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.ConnectedCallback
struct ConnectedCallback_t1584521517;
// System.Object
struct Il2CppObject;
// System.Net.Sockets.Socket
struct Socket_t150013987;
// System.Net.Sockets.SocketAsyncEventArgs
struct SocketAsyncEventArgs_t970431102;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_Sockets_Socket150013987.h"
#include "System_System_Net_Sockets_SocketAsyncEventArgs970431102.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SuperSocket.ClientEngine.ConnectedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ConnectedCallback__ctor_m3119060258 (ConnectedCallback_t1584521517 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectedCallback::Invoke(System.Net.Sockets.Socket,System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern "C"  void ConnectedCallback_Invoke_m3294233759 (ConnectedCallback_t1584521517 * __this, Socket_t150013987 * ___socket, Il2CppObject * ___state, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ConnectedCallback_t1584521517(Il2CppObject* delegate, Socket_t150013987 * ___socket, Il2CppObject * ___state, SocketAsyncEventArgs_t970431102 * ___e);
// System.IAsyncResult SuperSocket.ClientEngine.ConnectedCallback::BeginInvoke(System.Net.Sockets.Socket,System.Object,System.Net.Sockets.SocketAsyncEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ConnectedCallback_BeginInvoke_m697442090 (ConnectedCallback_t1584521517 * __this, Socket_t150013987 * ___socket, Il2CppObject * ___state, SocketAsyncEventArgs_t970431102 * ___e, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.ConnectedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ConnectedCallback_EndInvoke_m1095264934 (ConnectedCallback_t1584521517 * __this, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
