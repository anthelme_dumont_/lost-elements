﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_EventHandler_1_gen1679684063MethodDeclarations.h"

// System.Void System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>::.ctor(System.Object,System.IntPtr)
#define EventHandler_1__ctor_m575740431(__this, ___object, ___method, method) ((  void (*) (EventHandler_1_t4058788791 *, Il2CppObject *, IntPtr_t, const MethodInfo*))EventHandler_1__ctor_m1337593804_gshared)(__this, ___object, ___method, method)
// System.Void System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>::Invoke(System.Object,TEventArgs)
#define EventHandler_1_Invoke_m1625816252(__this, ___sender, ___e, method) ((  void (*) (EventHandler_1_t4058788791 *, Il2CppObject *, DataEventArgs_t3216211148 *, const MethodInfo*))EventHandler_1_Invoke_m2623239957_gshared)(__this, ___sender, ___e, method)
// System.IAsyncResult System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>::BeginInvoke(System.Object,TEventArgs,System.AsyncCallback,System.Object)
#define EventHandler_1_BeginInvoke_m730818274(__this, ___sender, ___e, ___callback, ___object, method) ((  Il2CppObject * (*) (EventHandler_1_t4058788791 *, Il2CppObject *, DataEventArgs_t3216211148 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))EventHandler_1_BeginInvoke_m996893970_gshared)(__this, ___sender, ___e, ___callback, ___object, method)
// System.Void System.EventHandler`1<SuperSocket.ClientEngine.DataEventArgs>::EndInvoke(System.IAsyncResult)
#define EventHandler_1_EndInvoke_m1082694196(__this, ___result, method) ((  void (*) (EventHandler_1_t4058788791 *, Il2CppObject *, const MethodInfo*))EventHandler_1_EndInvoke_m2479179740_gshared)(__this, ___result, method)
