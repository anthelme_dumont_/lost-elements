﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "SuperSocket_ClientEngine_Core_System_Collections_Co570712889MethodDeclarations.h"

// System.Void System.Collections.Concurrent.ConcurrentQueue`1<System.String>::.ctor()
#define ConcurrentQueue_1__ctor_m1291609117(__this, method) ((  void (*) (ConcurrentQueue_1_t702095371 *, const MethodInfo*))ConcurrentQueue_1__ctor_m3561420925_gshared)(__this, method)
// System.Void System.Collections.Concurrent.ConcurrentQueue`1<System.String>::Enqueue(T)
#define ConcurrentQueue_1_Enqueue_m1527058932(__this, ___item, method) ((  void (*) (ConcurrentQueue_1_t702095371 *, String_t*, const MethodInfo*))ConcurrentQueue_1_Enqueue_m879009435_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Concurrent.ConcurrentQueue`1<System.String>::TryDequeue(T&)
#define ConcurrentQueue_1_TryDequeue_m3652952203(__this, ___item, method) ((  bool (*) (ConcurrentQueue_1_t702095371 *, String_t**, const MethodInfo*))ConcurrentQueue_1_TryDequeue_m1762985058_gshared)(__this, ___item, method)
