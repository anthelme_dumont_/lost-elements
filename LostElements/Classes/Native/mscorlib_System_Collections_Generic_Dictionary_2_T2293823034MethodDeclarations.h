﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3328633407MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Action`1<System.Object>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m343121464(__this, ___object, ___method, method) ((  void (*) (Transform_1_t2293823034 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3355330134_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Action`1<System.Object>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m151790400(__this, ___key, ___value, method) ((  DictionaryEntry_t130027246  (*) (Transform_1_t2293823034 *, int32_t, Action_1_t985559125 *, const MethodInfo*))Transform_1_Invoke_m4168400550_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Action`1<System.Object>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m2382949355(__this, ___key, ___value, ___callback, ___object, method) ((  Il2CppObject * (*) (Transform_1_t2293823034 *, int32_t, Action_1_t985559125 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1630268421_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Action`1<System.Object>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m1478751562(__this, ___result, method) ((  DictionaryEntry_t130027246  (*) (Transform_1_t2293823034 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3617873444_gshared)(__this, ___result, method)
