﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader
struct IDataFramePartReader_t2102555929;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Protocol.FramePartReader.DataFramePartReader
struct  DataFramePartReader_t1881953282  : public Il2CppObject
{
public:

public:
};

struct DataFramePartReader_t1881953282_StaticFields
{
public:
	// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::<FixPartReader>k__BackingField
	Il2CppObject * ___U3CFixPartReaderU3Ek__BackingField_0;
	// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::<ExtendedLenghtReader>k__BackingField
	Il2CppObject * ___U3CExtendedLenghtReaderU3Ek__BackingField_1;
	// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::<MaskKeyReader>k__BackingField
	Il2CppObject * ___U3CMaskKeyReaderU3Ek__BackingField_2;
	// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::<PayloadDataReader>k__BackingField
	Il2CppObject * ___U3CPayloadDataReaderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CFixPartReaderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DataFramePartReader_t1881953282_StaticFields, ___U3CFixPartReaderU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CFixPartReaderU3Ek__BackingField_0() const { return ___U3CFixPartReaderU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CFixPartReaderU3Ek__BackingField_0() { return &___U3CFixPartReaderU3Ek__BackingField_0; }
	inline void set_U3CFixPartReaderU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CFixPartReaderU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFixPartReaderU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CExtendedLenghtReaderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DataFramePartReader_t1881953282_StaticFields, ___U3CExtendedLenghtReaderU3Ek__BackingField_1)); }
	inline Il2CppObject * get_U3CExtendedLenghtReaderU3Ek__BackingField_1() const { return ___U3CExtendedLenghtReaderU3Ek__BackingField_1; }
	inline Il2CppObject ** get_address_of_U3CExtendedLenghtReaderU3Ek__BackingField_1() { return &___U3CExtendedLenghtReaderU3Ek__BackingField_1; }
	inline void set_U3CExtendedLenghtReaderU3Ek__BackingField_1(Il2CppObject * value)
	{
		___U3CExtendedLenghtReaderU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CExtendedLenghtReaderU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CMaskKeyReaderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DataFramePartReader_t1881953282_StaticFields, ___U3CMaskKeyReaderU3Ek__BackingField_2)); }
	inline Il2CppObject * get_U3CMaskKeyReaderU3Ek__BackingField_2() const { return ___U3CMaskKeyReaderU3Ek__BackingField_2; }
	inline Il2CppObject ** get_address_of_U3CMaskKeyReaderU3Ek__BackingField_2() { return &___U3CMaskKeyReaderU3Ek__BackingField_2; }
	inline void set_U3CMaskKeyReaderU3Ek__BackingField_2(Il2CppObject * value)
	{
		___U3CMaskKeyReaderU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMaskKeyReaderU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CPayloadDataReaderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DataFramePartReader_t1881953282_StaticFields, ___U3CPayloadDataReaderU3Ek__BackingField_3)); }
	inline Il2CppObject * get_U3CPayloadDataReaderU3Ek__BackingField_3() const { return ___U3CPayloadDataReaderU3Ek__BackingField_3; }
	inline Il2CppObject ** get_address_of_U3CPayloadDataReaderU3Ek__BackingField_3() { return &___U3CPayloadDataReaderU3Ek__BackingField_3; }
	inline void set_U3CPayloadDataReaderU3Ek__BackingField_3(Il2CppObject * value)
	{
		___U3CPayloadDataReaderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPayloadDataReaderU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
