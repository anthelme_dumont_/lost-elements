﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Concurrent.ConcurrentQueue`1<System.ArraySegment`1<System.Byte>>
struct ConcurrentQueue_1_t2535351335;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_ArraySegment_1_gen2801744866.h"

// System.Void System.Collections.Concurrent.ConcurrentQueue`1<System.ArraySegment`1<System.Byte>>::.ctor()
extern "C"  void ConcurrentQueue_1__ctor_m3093470100_gshared (ConcurrentQueue_1_t2535351335 * __this, const MethodInfo* method);
#define ConcurrentQueue_1__ctor_m3093470100(__this, method) ((  void (*) (ConcurrentQueue_1_t2535351335 *, const MethodInfo*))ConcurrentQueue_1__ctor_m3093470100_gshared)(__this, method)
// System.Void System.Collections.Concurrent.ConcurrentQueue`1<System.ArraySegment`1<System.Byte>>::Enqueue(T)
extern "C"  void ConcurrentQueue_1_Enqueue_m1619824676_gshared (ConcurrentQueue_1_t2535351335 * __this, ArraySegment_1_t2801744866  ___item, const MethodInfo* method);
#define ConcurrentQueue_1_Enqueue_m1619824676(__this, ___item, method) ((  void (*) (ConcurrentQueue_1_t2535351335 *, ArraySegment_1_t2801744866 , const MethodInfo*))ConcurrentQueue_1_Enqueue_m1619824676_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Concurrent.ConcurrentQueue`1<System.ArraySegment`1<System.Byte>>::TryDequeue(T&)
extern "C"  bool ConcurrentQueue_1_TryDequeue_m1933623995_gshared (ConcurrentQueue_1_t2535351335 * __this, ArraySegment_1_t2801744866 * ___item, const MethodInfo* method);
#define ConcurrentQueue_1_TryDequeue_m1933623995(__this, ___item, method) ((  bool (*) (ConcurrentQueue_1_t2535351335 *, ArraySegment_1_t2801744866 *, const MethodInfo*))ConcurrentQueue_1_TryDequeue_m1933623995_gshared)(__this, ___item, method)
