﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.DataReceivedEventArgs
struct DataReceivedEventArgs_t1979092924;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocket4Net.DataReceivedEventArgs::.ctor(System.Byte[])
extern "C"  void DataReceivedEventArgs__ctor_m2970247150 (DataReceivedEventArgs_t1979092924 * __this, ByteU5BU5D_t58506160* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.DataReceivedEventArgs::set_Data(System.Byte[])
extern "C"  void DataReceivedEventArgs_set_Data_m589273081 (DataReceivedEventArgs_t1979092924 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
