﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.DataContractJsonSerializerStrategy
struct DataContractJsonSerializerStrategy_t3752539591;
// System.Type
struct Type_t;
// SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>
struct SafeDictionary_2_t2250466201;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "mscorlib_System_String968488902.h"

// System.Void SimpleJson.DataContractJsonSerializerStrategy::.ctor()
extern "C"  void DataContractJsonSerializerStrategy__ctor_m696556770 (DataContractJsonSerializerStrategy_t3752539591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJson.DataContractJsonSerializerStrategy::BuildMap(System.Type,SimpleJson.Reflection.SafeDictionary`2<System.String,SimpleJson.Reflection.CacheResolver/MemberMap>)
extern "C"  void DataContractJsonSerializerStrategy_BuildMap_m2479422991 (DataContractJsonSerializerStrategy_t3752539591 * __this, Type_t * ___type, SafeDictionary_2_t2250466201 * ___map, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleJson.DataContractJsonSerializerStrategy::CanAdd(System.Reflection.MemberInfo,System.String&)
extern "C"  bool DataContractJsonSerializerStrategy_CanAdd_m308335317 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___info, String_t** ___jsonKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
