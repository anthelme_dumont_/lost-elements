﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader
struct IDataFramePartReader_t2102555929;
// WebSocket4Net.Protocol.FramePartReader.DataFramePartReader
struct DataFramePartReader_t1881953282;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::.cctor()
extern "C"  void DataFramePartReader__cctor_m3471580710 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::get_NewReader()
extern "C"  Il2CppObject * DataFramePartReader_get_NewReader_m3969627167 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::get_FixPartReader()
extern "C"  Il2CppObject * DataFramePartReader_get_FixPartReader_m3522342567 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::set_FixPartReader(WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader)
extern "C"  void DataFramePartReader_set_FixPartReader_m97364172 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::get_ExtendedLenghtReader()
extern "C"  Il2CppObject * DataFramePartReader_get_ExtendedLenghtReader_m901854720 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::set_ExtendedLenghtReader(WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader)
extern "C"  void DataFramePartReader_set_ExtendedLenghtReader_m1629640683 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::get_MaskKeyReader()
extern "C"  Il2CppObject * DataFramePartReader_get_MaskKeyReader_m287680978 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::set_MaskKeyReader(WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader)
extern "C"  void DataFramePartReader_set_MaskKeyReader_m3664049857 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::get_PayloadDataReader()
extern "C"  Il2CppObject * DataFramePartReader_get_PayloadDataReader_m2911966679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::set_PayloadDataReader(WebSocket4Net.Protocol.FramePartReader.IDataFramePartReader)
extern "C"  void DataFramePartReader_set_PayloadDataReader_m2933328284 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocket4Net.Protocol.FramePartReader.DataFramePartReader::.ctor()
extern "C"  void DataFramePartReader__ctor_m2067200615 (DataFramePartReader_t1881953282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
