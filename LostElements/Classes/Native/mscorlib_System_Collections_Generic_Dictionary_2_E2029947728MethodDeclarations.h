﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3372796995(__this, ___dictionary, method) ((  void (*) (Enumerator_t2029947728 *, Dictionary_2_t2262919787 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m665064072(__this, method) ((  Il2CppObject * (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m298729618(__this, method) ((  void (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m419829577(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4104997220(__this, method) ((  Il2CppObject * (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3170787062(__this, method) ((  Il2CppObject * (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::MoveNext()
#define Enumerator_MoveNext_m868089410(__this, method) ((  bool (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::get_Current()
#define Enumerator_get_Current_m2639361594(__this, method) ((  KeyValuePair_2_t1751451085  (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2379158091(__this, method) ((  String_t* (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3231422667(__this, method) ((  Il2CppObject* (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::Reset()
#define Enumerator_Reset_m2742399509(__this, method) ((  void (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::VerifyState()
#define Enumerator_VerifyState_m366894686(__this, method) ((  void (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3143346758(__this, method) ((  void (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::Dispose()
#define Enumerator_Dispose_m859437477(__this, method) ((  void (*) (Enumerator_t2029947728 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
