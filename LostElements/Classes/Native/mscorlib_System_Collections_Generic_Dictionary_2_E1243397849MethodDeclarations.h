﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1094945144MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1388078916(__this, ___dictionary, method) ((  void (*) (Enumerator_t1243397849 *, Dictionary_2_t1476369908 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3377804765(__this, method) ((  Il2CppObject * (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2568398577(__this, method) ((  void (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3390198202(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3626621177(__this, method) ((  Il2CppObject * (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3012910411(__this, method) ((  Il2CppObject * (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::MoveNext()
#define Enumerator_MoveNext_m1108726621(__this, method) ((  bool (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::get_Current()
#define Enumerator_get_Current_m896381363(__this, method) ((  KeyValuePair_2_t964901206  (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m537186666(__this, method) ((  int32_t (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m190439310(__this, method) ((  Action_1_t985559125 * (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::Reset()
#define Enumerator_Reset_m2020105110(__this, method) ((  void (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::VerifyState()
#define Enumerator_VerifyState_m2456953631(__this, method) ((  void (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1645298375(__this, method) ((  void (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Action`1<System.Object>>::Dispose()
#define Enumerator_Dispose_m2519221990(__this, method) ((  void (*) (Enumerator_t1243397849 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
