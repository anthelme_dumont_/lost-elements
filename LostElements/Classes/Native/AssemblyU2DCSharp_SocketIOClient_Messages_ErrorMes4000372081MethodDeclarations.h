﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Messages.ErrorMessage
struct ErrorMessage_t4000372081;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SocketIOClient.Messages.ErrorMessage::.ctor()
extern "C"  void ErrorMessage__ctor_m1092945796 (ErrorMessage_t4000372081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.ErrorMessage::get_Reason()
extern "C"  String_t* ErrorMessage_get_Reason_m608053616 (ErrorMessage_t4000372081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.ErrorMessage::set_Reason(System.String)
extern "C"  void ErrorMessage_set_Reason_m3822803489 (ErrorMessage_t4000372081 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.ErrorMessage::get_Advice()
extern "C"  String_t* ErrorMessage_get_Advice_m749156132 (ErrorMessage_t4000372081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.ErrorMessage::set_Advice(System.String)
extern "C"  void ErrorMessage_set_Advice_m2705404141 (ErrorMessage_t4000372081 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.Messages.ErrorMessage::get_Event()
extern "C"  String_t* ErrorMessage_get_Event_m3519779984 (ErrorMessage_t4000372081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.ErrorMessage SocketIOClient.Messages.ErrorMessage::Deserialize(System.String)
extern "C"  ErrorMessage_t4000372081 * ErrorMessage_Deserialize_m334360103 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
