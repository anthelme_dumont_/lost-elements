﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.Security.SslStream
struct SslStream_t2729491676;
// System.Net.Sockets.Socket
struct Socket_t150013987;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState
struct  SslAsyncState_t3174738561  : public Il2CppObject
{
public:
	// System.Net.Security.SslStream SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState::<SslStream>k__BackingField
	SslStream_t2729491676 * ___U3CSslStreamU3Ek__BackingField_0;
	// System.Net.Sockets.Socket SuperSocket.ClientEngine.SslStreamTcpSession/SslAsyncState::<Client>k__BackingField
	Socket_t150013987 * ___U3CClientU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CSslStreamU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SslAsyncState_t3174738561, ___U3CSslStreamU3Ek__BackingField_0)); }
	inline SslStream_t2729491676 * get_U3CSslStreamU3Ek__BackingField_0() const { return ___U3CSslStreamU3Ek__BackingField_0; }
	inline SslStream_t2729491676 ** get_address_of_U3CSslStreamU3Ek__BackingField_0() { return &___U3CSslStreamU3Ek__BackingField_0; }
	inline void set_U3CSslStreamU3Ek__BackingField_0(SslStream_t2729491676 * value)
	{
		___U3CSslStreamU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSslStreamU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CClientU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SslAsyncState_t3174738561, ___U3CClientU3Ek__BackingField_1)); }
	inline Socket_t150013987 * get_U3CClientU3Ek__BackingField_1() const { return ___U3CClientU3Ek__BackingField_1; }
	inline Socket_t150013987 ** get_address_of_U3CClientU3Ek__BackingField_1() { return &___U3CClientU3Ek__BackingField_1; }
	inline void set_U3CClientU3Ek__BackingField_1(Socket_t150013987 * value)
	{
		___U3CClientU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CClientU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
