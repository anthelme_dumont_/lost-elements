﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Messages.JSONMessage
struct JSONMessage_t3736498801;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_String968488902.h"

// System.Void SocketIOClient.Messages.JSONMessage::.ctor()
extern "C"  void JSONMessage__ctor_m527254596 (JSONMessage_t3736498801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.JSONMessage::.ctor(System.Object)
extern "C"  void JSONMessage__ctor_m3855190992 (JSONMessage_t3736498801 * __this, Il2CppObject * ___jsonObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.JSONMessage::.ctor(System.Object,System.Nullable`1<System.Int32>,System.String)
extern "C"  void JSONMessage__ctor_m3161118484 (JSONMessage_t3736498801 * __this, Il2CppObject * ___jsonObject, Nullable_1_t1438485399  ___ackId, String_t* ___endpoint, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Messages.JSONMessage::SetMessage(System.Object)
extern "C"  void JSONMessage_SetMessage_m3407837295 (JSONMessage_t3736498801 * __this, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.Messages.JSONMessage SocketIOClient.Messages.JSONMessage::Deserialize(System.String)
extern "C"  JSONMessage_t3736498801 * JSONMessage_Deserialize_m1677086293 (Il2CppObject * __this /* static, unused */, String_t* ___rawMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
