﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t2956870243;

#include "WebSocket4Net_WebSocket4Net_Command_WebSocketComma3263712852.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocket4Net.Command.BadRequest
struct  BadRequest_t1215901385  : public WebSocketCommandBase_t3263712852
{
public:

public:
};

struct BadRequest_t1215901385_StaticFields
{
public:
	// System.String[] WebSocket4Net.Command.BadRequest::m_ValueSeparator
	StringU5BU5D_t2956870243* ___m_ValueSeparator_0;

public:
	inline static int32_t get_offset_of_m_ValueSeparator_0() { return static_cast<int32_t>(offsetof(BadRequest_t1215901385_StaticFields, ___m_ValueSeparator_0)); }
	inline StringU5BU5D_t2956870243* get_m_ValueSeparator_0() const { return ___m_ValueSeparator_0; }
	inline StringU5BU5D_t2956870243** get_address_of_m_ValueSeparator_0() { return &___m_ValueSeparator_0; }
	inline void set_m_ValueSeparator_0(StringU5BU5D_t2956870243* value)
	{
		___m_ValueSeparator_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_ValueSeparator_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
