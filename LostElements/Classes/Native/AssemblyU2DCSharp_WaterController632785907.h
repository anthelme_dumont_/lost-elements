﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaterController
struct  WaterController_t632785907  : public MonoBehaviour_t3012272455
{
public:
	// System.Single WaterController::waterLevel
	float ___waterLevel_2;
	// System.Single WaterController::defaultYPosition
	float ___defaultYPosition_3;
	// System.Single WaterController::minScale
	float ___minScale_4;
	// System.Single WaterController::maxScale
	float ___maxScale_5;

public:
	inline static int32_t get_offset_of_waterLevel_2() { return static_cast<int32_t>(offsetof(WaterController_t632785907, ___waterLevel_2)); }
	inline float get_waterLevel_2() const { return ___waterLevel_2; }
	inline float* get_address_of_waterLevel_2() { return &___waterLevel_2; }
	inline void set_waterLevel_2(float value)
	{
		___waterLevel_2 = value;
	}

	inline static int32_t get_offset_of_defaultYPosition_3() { return static_cast<int32_t>(offsetof(WaterController_t632785907, ___defaultYPosition_3)); }
	inline float get_defaultYPosition_3() const { return ___defaultYPosition_3; }
	inline float* get_address_of_defaultYPosition_3() { return &___defaultYPosition_3; }
	inline void set_defaultYPosition_3(float value)
	{
		___defaultYPosition_3 = value;
	}

	inline static int32_t get_offset_of_minScale_4() { return static_cast<int32_t>(offsetof(WaterController_t632785907, ___minScale_4)); }
	inline float get_minScale_4() const { return ___minScale_4; }
	inline float* get_address_of_minScale_4() { return &___minScale_4; }
	inline void set_minScale_4(float value)
	{
		___minScale_4 = value;
	}

	inline static int32_t get_offset_of_maxScale_5() { return static_cast<int32_t>(offsetof(WaterController_t632785907, ___maxScale_5)); }
	inline float get_maxScale_5() const { return ___maxScale_5; }
	inline float* get_address_of_maxScale_5() { return &___maxScale_5; }
	inline void set_maxScale_5(float value)
	{
		___maxScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
