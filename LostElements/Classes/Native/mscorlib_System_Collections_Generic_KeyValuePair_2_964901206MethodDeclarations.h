﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_816448501MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Action`1<System.Object>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2343782716(__this, ___key, ___value, method) ((  void (*) (KeyValuePair_2_t964901206 *, int32_t, Action_1_t985559125 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Action`1<System.Object>>::get_Key()
#define KeyValuePair_2_get_Key_m1732650668(__this, method) ((  int32_t (*) (KeyValuePair_2_t964901206 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Action`1<System.Object>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3335142253(__this, ___value, method) ((  void (*) (KeyValuePair_2_t964901206 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Action`1<System.Object>>::get_Value()
#define KeyValuePair_2_get_Value_m2834649168(__this, method) ((  Action_1_t985559125 * (*) (KeyValuePair_2_t964901206 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Action`1<System.Object>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2341474541(__this, ___value, method) ((  void (*) (KeyValuePair_2_t964901206 *, Action_1_t985559125 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Action`1<System.Object>>::ToString()
#define KeyValuePair_2_ToString_m1554786491(__this, method) ((  String_t* (*) (KeyValuePair_2_t964901206 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
