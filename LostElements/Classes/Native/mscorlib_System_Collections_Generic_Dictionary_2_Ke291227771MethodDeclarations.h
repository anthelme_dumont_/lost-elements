﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1852733134MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3450947196(__this, ___dictionary, method) ((  void (*) (KeyCollection_t291227771 *, Dictionary_2_t2262919787 *, const MethodInfo*))KeyCollection__ctor_m3432069128_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m401143130(__this, ___item, method) ((  void (*) (KeyCollection_t291227771 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3101899854_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1153476945(__this, method) ((  void (*) (KeyCollection_t291227771 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m164109637_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1540583860(__this, ___item, method) ((  bool (*) (KeyCollection_t291227771 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2402136956_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2549661721(__this, ___item, method) ((  bool (*) (KeyCollection_t291227771 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1325978593_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4101401379(__this, method) ((  Il2CppObject* (*) (KeyCollection_t291227771 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3150060033_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3689028675(__this, ___array, ___index, method) ((  void (*) (KeyCollection_t291227771 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2134327863_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3230888594(__this, method) ((  Il2CppObject * (*) (KeyCollection_t291227771 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3067601266_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1618430997(__this, method) ((  bool (*) (KeyCollection_t291227771 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m642268125_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1504738055(__this, method) ((  bool (*) (KeyCollection_t291227771 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1412236495_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3670013177(__this, method) ((  Il2CppObject * (*) (KeyCollection_t291227771 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3575527099_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1702337841(__this, ___array, ___index, method) ((  void (*) (KeyCollection_t291227771 *, StringU5BU5D_t2956870243*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2803941053_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2568186750(__this, method) ((  Enumerator_t2029947729  (*) (KeyCollection_t291227771 *, const MethodInfo*))KeyCollection_GetEnumerator_m2980864032_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SuperSocket.ClientEngine.Protocol.ICommand`2<WebSocket4Net.WebSocket,WebSocket4Net.WebSocketCommandInfo>>::get_Count()
#define KeyCollection_get_Count_m2030668993(__this, method) ((  int32_t (*) (KeyCollection_t291227771 *, const MethodInfo*))KeyCollection_get_Count_m1374340501_gshared)(__this, method)
