﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>
struct ArraySegmentList_1_t27518574;
// System.Collections.Generic.IList`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>
struct IList_1_t1542460565;
// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>
struct ArraySegmentEx_1_t3670935547;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t4261800269;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.IList`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<T>> SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Segments()
extern "C"  Il2CppObject* ArraySegmentList_1_get_Segments_m4138644685_gshared (ArraySegmentList_1_t27518574 * __this, const MethodInfo* method);
#define ArraySegmentList_1_get_Segments_m4138644685(__this, method) ((  Il2CppObject* (*) (ArraySegmentList_1_t27518574 *, const MethodInfo*))ArraySegmentList_1_get_Segments_m4138644685_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::.ctor()
extern "C"  void ArraySegmentList_1__ctor_m309370475_gshared (ArraySegmentList_1_t27518574 * __this, const MethodInfo* method);
#define ArraySegmentList_1__ctor_m309370475(__this, method) ((  void (*) (ArraySegmentList_1_t27518574 *, const MethodInfo*))ArraySegmentList_1__ctor_m309370475_gshared)(__this, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::IndexOf(T)
extern "C"  int32_t ArraySegmentList_1_IndexOf_m1412553800_gshared (ArraySegmentList_1_t27518574 * __this, uint8_t ___item, const MethodInfo* method);
#define ArraySegmentList_1_IndexOf_m1412553800(__this, ___item, method) ((  int32_t (*) (ArraySegmentList_1_t27518574 *, uint8_t, const MethodInfo*))ArraySegmentList_1_IndexOf_m1412553800_gshared)(__this, ___item, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::Insert(System.Int32,T)
extern "C"  void ArraySegmentList_1_Insert_m4157651739_gshared (ArraySegmentList_1_t27518574 * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method);
#define ArraySegmentList_1_Insert_m4157651739(__this, ___index, ___item, method) ((  void (*) (ArraySegmentList_1_t27518574 *, int32_t, uint8_t, const MethodInfo*))ArraySegmentList_1_Insert_m4157651739_gshared)(__this, ___index, ___item, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::RemoveAt(System.Int32)
extern "C"  void ArraySegmentList_1_RemoveAt_m2031504609_gshared (ArraySegmentList_1_t27518574 * __this, int32_t ___index, const MethodInfo* method);
#define ArraySegmentList_1_RemoveAt_m2031504609(__this, ___index, method) ((  void (*) (ArraySegmentList_1_t27518574 *, int32_t, const MethodInfo*))ArraySegmentList_1_RemoveAt_m2031504609_gshared)(__this, ___index, method)
// T SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Item(System.Int32)
extern "C"  uint8_t ArraySegmentList_1_get_Item_m4155346375_gshared (ArraySegmentList_1_t27518574 * __this, int32_t ___index, const MethodInfo* method);
#define ArraySegmentList_1_get_Item_m4155346375(__this, ___index, method) ((  uint8_t (*) (ArraySegmentList_1_t27518574 *, int32_t, const MethodInfo*))ArraySegmentList_1_get_Item_m4155346375_gshared)(__this, ___index, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::set_Item(System.Int32,T)
extern "C"  void ArraySegmentList_1_set_Item_m2993332530_gshared (ArraySegmentList_1_t27518574 * __this, int32_t ___index, uint8_t ___value, const MethodInfo* method);
#define ArraySegmentList_1_set_Item_m2993332530(__this, ___index, ___value, method) ((  void (*) (ArraySegmentList_1_t27518574 *, int32_t, uint8_t, const MethodInfo*))ArraySegmentList_1_set_Item_m2993332530_gshared)(__this, ___index, ___value, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::GetElementInternalIndex(System.Int32,SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<T>&)
extern "C"  int32_t ArraySegmentList_1_GetElementInternalIndex_m3714391494_gshared (ArraySegmentList_1_t27518574 * __this, int32_t ___index, ArraySegmentEx_1_t3670935547 ** ___segment, const MethodInfo* method);
#define ArraySegmentList_1_GetElementInternalIndex_m3714391494(__this, ___index, ___segment, method) ((  int32_t (*) (ArraySegmentList_1_t27518574 *, int32_t, ArraySegmentEx_1_t3670935547 **, const MethodInfo*))ArraySegmentList_1_GetElementInternalIndex_m3714391494_gshared)(__this, ___index, ___segment, method)
// SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<T> SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::QuickSearchSegment(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C"  ArraySegmentEx_1_t3670935547 * ArraySegmentList_1_QuickSearchSegment_m1626119581_gshared (ArraySegmentList_1_t27518574 * __this, int32_t ___from, int32_t ___to, int32_t ___index, int32_t* ___segmentIndex, const MethodInfo* method);
#define ArraySegmentList_1_QuickSearchSegment_m1626119581(__this, ___from, ___to, ___index, ___segmentIndex, method) ((  ArraySegmentEx_1_t3670935547 * (*) (ArraySegmentList_1_t27518574 *, int32_t, int32_t, int32_t, int32_t*, const MethodInfo*))ArraySegmentList_1_QuickSearchSegment_m1626119581_gshared)(__this, ___from, ___to, ___index, ___segmentIndex, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::Add(T)
extern "C"  void ArraySegmentList_1_Add_m3325257908_gshared (ArraySegmentList_1_t27518574 * __this, uint8_t ___item, const MethodInfo* method);
#define ArraySegmentList_1_Add_m3325257908(__this, ___item, method) ((  void (*) (ArraySegmentList_1_t27518574 *, uint8_t, const MethodInfo*))ArraySegmentList_1_Add_m3325257908_gshared)(__this, ___item, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::Clear()
extern "C"  void ArraySegmentList_1_Clear_m2010471062_gshared (ArraySegmentList_1_t27518574 * __this, const MethodInfo* method);
#define ArraySegmentList_1_Clear_m2010471062(__this, method) ((  void (*) (ArraySegmentList_1_t27518574 *, const MethodInfo*))ArraySegmentList_1_Clear_m2010471062_gshared)(__this, method)
// System.Boolean SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::Contains(T)
extern "C"  bool ArraySegmentList_1_Contains_m3671599408_gshared (ArraySegmentList_1_t27518574 * __this, uint8_t ___item, const MethodInfo* method);
#define ArraySegmentList_1_Contains_m3671599408(__this, ___item, method) ((  bool (*) (ArraySegmentList_1_t27518574 *, uint8_t, const MethodInfo*))ArraySegmentList_1_Contains_m3671599408_gshared)(__this, ___item, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::CopyTo(T[],System.Int32)
extern "C"  void ArraySegmentList_1_CopyTo_m1612450724_gshared (ArraySegmentList_1_t27518574 * __this, ByteU5BU5D_t58506160* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ArraySegmentList_1_CopyTo_m1612450724(__this, ___array, ___arrayIndex, method) ((  void (*) (ArraySegmentList_1_t27518574 *, ByteU5BU5D_t58506160*, int32_t, const MethodInfo*))ArraySegmentList_1_CopyTo_m1612450724_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_Count()
extern "C"  int32_t ArraySegmentList_1_get_Count_m656802355_gshared (ArraySegmentList_1_t27518574 * __this, const MethodInfo* method);
#define ArraySegmentList_1_get_Count_m656802355(__this, method) ((  int32_t (*) (ArraySegmentList_1_t27518574 *, const MethodInfo*))ArraySegmentList_1_get_Count_m656802355_gshared)(__this, method)
// System.Boolean SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_IsReadOnly()
extern "C"  bool ArraySegmentList_1_get_IsReadOnly_m1615278596_gshared (ArraySegmentList_1_t27518574 * __this, const MethodInfo* method);
#define ArraySegmentList_1_get_IsReadOnly_m1615278596(__this, method) ((  bool (*) (ArraySegmentList_1_t27518574 *, const MethodInfo*))ArraySegmentList_1_get_IsReadOnly_m1615278596_gshared)(__this, method)
// System.Boolean SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::Remove(T)
extern "C"  bool ArraySegmentList_1_Remove_m2103801131_gshared (ArraySegmentList_1_t27518574 * __this, uint8_t ___item, const MethodInfo* method);
#define ArraySegmentList_1_Remove_m2103801131(__this, ___item, method) ((  bool (*) (ArraySegmentList_1_t27518574 *, uint8_t, const MethodInfo*))ArraySegmentList_1_Remove_m2103801131_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::GetEnumerator()
extern "C"  Il2CppObject* ArraySegmentList_1_GetEnumerator_m2850125023_gshared (ArraySegmentList_1_t27518574 * __this, const MethodInfo* method);
#define ArraySegmentList_1_GetEnumerator_m2850125023(__this, method) ((  Il2CppObject* (*) (ArraySegmentList_1_t27518574 *, const MethodInfo*))ArraySegmentList_1_GetEnumerator_m2850125023_gshared)(__this, method)
// System.Collections.IEnumerator SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArraySegmentList_1_System_Collections_IEnumerable_GetEnumerator_m3833734624_gshared (ArraySegmentList_1_t27518574 * __this, const MethodInfo* method);
#define ArraySegmentList_1_System_Collections_IEnumerable_GetEnumerator_m3833734624(__this, method) ((  Il2CppObject * (*) (ArraySegmentList_1_t27518574 *, const MethodInfo*))ArraySegmentList_1_System_Collections_IEnumerable_GetEnumerator_m3833734624_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::RemoveSegmentAt(System.Int32)
extern "C"  void ArraySegmentList_1_RemoveSegmentAt_m610797692_gshared (ArraySegmentList_1_t27518574 * __this, int32_t ___index, const MethodInfo* method);
#define ArraySegmentList_1_RemoveSegmentAt_m610797692(__this, ___index, method) ((  void (*) (ArraySegmentList_1_t27518574 *, int32_t, const MethodInfo*))ArraySegmentList_1_RemoveSegmentAt_m610797692_gshared)(__this, ___index, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::AddSegment(T[],System.Int32,System.Int32,System.Boolean)
extern "C"  void ArraySegmentList_1_AddSegment_m727323212_gshared (ArraySegmentList_1_t27518574 * __this, ByteU5BU5D_t58506160* ___array, int32_t ___offset, int32_t ___length, bool ___toBeCopied, const MethodInfo* method);
#define ArraySegmentList_1_AddSegment_m727323212(__this, ___array, ___offset, ___length, ___toBeCopied, method) ((  void (*) (ArraySegmentList_1_t27518574 *, ByteU5BU5D_t58506160*, int32_t, int32_t, bool, const MethodInfo*))ArraySegmentList_1_AddSegment_m727323212_gshared)(__this, ___array, ___offset, ___length, ___toBeCopied, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::get_SegmentCount()
extern "C"  int32_t ArraySegmentList_1_get_SegmentCount_m673386074_gshared (ArraySegmentList_1_t27518574 * __this, const MethodInfo* method);
#define ArraySegmentList_1_get_SegmentCount_m673386074(__this, method) ((  int32_t (*) (ArraySegmentList_1_t27518574 *, const MethodInfo*))ArraySegmentList_1_get_SegmentCount_m673386074_gshared)(__this, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::ClearSegements()
extern "C"  void ArraySegmentList_1_ClearSegements_m3355833128_gshared (ArraySegmentList_1_t27518574 * __this, const MethodInfo* method);
#define ArraySegmentList_1_ClearSegements_m3355833128(__this, method) ((  void (*) (ArraySegmentList_1_t27518574 *, const MethodInfo*))ArraySegmentList_1_ClearSegements_m3355833128_gshared)(__this, method)
// T[] SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::ToArrayData(System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t58506160* ArraySegmentList_1_ToArrayData_m509443740_gshared (ArraySegmentList_1_t27518574 * __this, int32_t ___startIndex, int32_t ___length, const MethodInfo* method);
#define ArraySegmentList_1_ToArrayData_m509443740(__this, ___startIndex, ___length, method) ((  ByteU5BU5D_t58506160* (*) (ArraySegmentList_1_t27518574 *, int32_t, int32_t, const MethodInfo*))ArraySegmentList_1_ToArrayData_m509443740_gshared)(__this, ___startIndex, ___length, method)
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::TrimEnd(System.Int32)
extern "C"  void ArraySegmentList_1_TrimEnd_m2413284860_gshared (ArraySegmentList_1_t27518574 * __this, int32_t ___trimSize, const MethodInfo* method);
#define ArraySegmentList_1_TrimEnd_m2413284860(__this, ___trimSize, method) ((  void (*) (ArraySegmentList_1_t27518574 *, int32_t, const MethodInfo*))ArraySegmentList_1_TrimEnd_m2413284860_gshared)(__this, ___trimSize, method)
// System.Int32 SuperSocket.ClientEngine.Protocol.ArraySegmentList`1<System.Byte>::CopyTo(T[],System.Int32,System.Int32,System.Int32)
extern "C"  int32_t ArraySegmentList_1_CopyTo_m815842042_gshared (ArraySegmentList_1_t27518574 * __this, ByteU5BU5D_t58506160* ___to, int32_t ___srcIndex, int32_t ___toIndex, int32_t ___length, const MethodInfo* method);
#define ArraySegmentList_1_CopyTo_m815842042(__this, ___to, ___srcIndex, ___toIndex, ___length, method) ((  int32_t (*) (ArraySegmentList_1_t27518574 *, ByteU5BU5D_t58506160*, int32_t, int32_t, int32_t, const MethodInfo*))ArraySegmentList_1_CopyTo_m815842042_gshared)(__this, ___to, ___srcIndex, ___toIndex, ___length, method)
