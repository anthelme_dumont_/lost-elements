﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.Eventing.RegistrationManager
struct RegistrationManager_t998967514;
// SocketIOClient.Messages.IMessage
struct IMessage_t3628529648;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.String
struct String_t;
// SocketIOClient.Messages.JsonEncodedEventMessage
struct JsonEncodedEventMessage_t989636229;
// System.Action`1<SocketIOClient.Messages.IMessage>
struct Action_1_t3776982353;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_SocketIOClient_Messages_JsonEncod989636229.h"

// System.Void SocketIOClient.Eventing.RegistrationManager::.ctor()
extern "C"  void RegistrationManager__ctor_m1057237267 (RegistrationManager_t998967514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Eventing.RegistrationManager::AddCallBack(SocketIOClient.Messages.IMessage)
extern "C"  void RegistrationManager_AddCallBack_m2141734945 (RegistrationManager_t998967514 * __this, Il2CppObject * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Eventing.RegistrationManager::AddCallBack(System.Int32,System.Action`1<System.Object>)
extern "C"  void RegistrationManager_AddCallBack_m2506737832 (RegistrationManager_t998967514 * __this, int32_t ___ackId, Action_1_t985559125 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Eventing.RegistrationManager::InvokeCallBack(System.Nullable`1<System.Int32>,System.String)
extern "C"  void RegistrationManager_InvokeCallBack_m4077058364 (RegistrationManager_t998967514 * __this, Nullable_1_t1438485399  ___ackId, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Eventing.RegistrationManager::InvokeCallBack(System.Nullable`1<System.Int32>,SocketIOClient.Messages.JsonEncodedEventMessage)
extern "C"  void RegistrationManager_InvokeCallBack_m1001874289 (RegistrationManager_t998967514 * __this, Nullable_1_t1438485399  ___ackId, JsonEncodedEventMessage_t989636229 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Eventing.RegistrationManager::AddOnEvent(System.String,System.Action`1<SocketIOClient.Messages.IMessage>)
extern "C"  void RegistrationManager_AddOnEvent_m1569321649 (RegistrationManager_t998967514 * __this, String_t* ___eventName, Action_1_t3776982353 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Eventing.RegistrationManager::AddOnEvent(System.String,System.String,System.Action`1<SocketIOClient.Messages.IMessage>)
extern "C"  void RegistrationManager_AddOnEvent_m2206615469 (RegistrationManager_t998967514 * __this, String_t* ___eventName, String_t* ___endPoint, Action_1_t3776982353 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SocketIOClient.Eventing.RegistrationManager::InvokeOnEvent(SocketIOClient.Messages.IMessage)
extern "C"  bool RegistrationManager_InvokeOnEvent_m2394835410 (RegistrationManager_t998967514 * __this, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Eventing.RegistrationManager::Dispose()
extern "C"  void RegistrationManager_Dispose_m2291014800 (RegistrationManager_t998967514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.Eventing.RegistrationManager::Dispose(System.Boolean)
extern "C"  void RegistrationManager_Dispose_m92247559 (RegistrationManager_t998967514 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
