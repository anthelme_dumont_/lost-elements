﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.SslStreamTcpSession
struct SslStreamTcpSession_t2680237056;
// System.Net.EndPoint
struct EndPoint_t1294049535;
// System.Object
struct Il2CppObject;
// System.Net.Sockets.SocketAsyncEventArgs
struct SocketAsyncEventArgs_t970431102;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t3432067208;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t2831591730;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_EndPoint1294049535.h"
#include "mscorlib_System_Object837106420.h"
#include "System_System_Net_Sockets_SocketAsyncEventArgs970431102.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3432067208.h"
#include "System_System_Security_Cryptography_X509Certificat2831591730.h"
#include "System_System_Net_Security_SslPolicyErrors3085256601.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_ArraySegment_1_gen2801744866.h"

// System.Boolean SuperSocket.ClientEngine.SslStreamTcpSession::get_AllowUnstrustedCertificate()
extern "C"  bool SslStreamTcpSession_get_AllowUnstrustedCertificate_m4133364489 (SslStreamTcpSession_t2680237056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::.ctor(System.Net.EndPoint)
extern "C"  void SslStreamTcpSession__ctor_m1591305088 (SslStreamTcpSession_t2680237056 * __this, EndPoint_t1294049535 * ___remoteEndPoint, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::SocketEventArgsCompleted(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern "C"  void SslStreamTcpSession_SocketEventArgsCompleted_m1031600694 (SslStreamTcpSession_t2680237056 * __this, Il2CppObject * ___sender, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::OnGetSocket(System.Net.Sockets.SocketAsyncEventArgs)
extern "C"  void SslStreamTcpSession_OnGetSocket_m172588209 (SslStreamTcpSession_t2680237056 * __this, SocketAsyncEventArgs_t970431102 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::OnAuthenticated(System.IAsyncResult)
extern "C"  void SslStreamTcpSession_OnAuthenticated_m2132154888 (SslStreamTcpSession_t2680237056 * __this, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::OnDataRead(System.IAsyncResult)
extern "C"  void SslStreamTcpSession_OnDataRead_m1314874925 (SslStreamTcpSession_t2680237056 * __this, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::BeginRead()
extern "C"  void SslStreamTcpSession_BeginRead_m2969334026 (SslStreamTcpSession_t2680237056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SuperSocket.ClientEngine.SslStreamTcpSession::ValidateRemoteCertificate(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool SslStreamTcpSession_ValidateRemoteCertificate_m495241029 (SslStreamTcpSession_t2680237056 * __this, Il2CppObject * ___sender, X509Certificate_t3432067208 * ___certificate, X509Chain_t2831591730 * ___chain, int32_t ___sslPolicyErrors, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SuperSocket.ClientEngine.SslStreamTcpSession::IsIgnorableException(System.Exception)
extern "C"  bool SslStreamTcpSession_IsIgnorableException_m1155508779 (SslStreamTcpSession_t2680237056 * __this, Exception_t1967233988 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::SendInternal(System.ArraySegment`1<System.Byte>)
extern "C"  void SslStreamTcpSession_SendInternal_m3092865971 (SslStreamTcpSession_t2680237056 * __this, ArraySegment_1_t2801744866  ___segment, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.SslStreamTcpSession::OnWriteComplete(System.IAsyncResult)
extern "C"  void SslStreamTcpSession_OnWriteComplete_m2304180127 (SslStreamTcpSession_t2680237056 * __this, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
