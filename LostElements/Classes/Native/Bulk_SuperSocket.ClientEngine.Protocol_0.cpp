﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// SuperSocket.ClientEngine.Protocol.ArraySegmentList
struct ArraySegmentList_t1783467835;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t180559927;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "SuperSocket_ClientEngine_Protocol_U3CModuleU3E86524790.h"
#include "SuperSocket_ClientEngine_Protocol_U3CModuleU3E86524790MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Clie1783467835.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Clie1783467835MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding180559927.h"
#include "mscorlib_System_String968488902.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Client27518574MethodDeclarations.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Client27518574.h"
#include "mscorlib_System_Int322847414787.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Clie3670935547MethodDeclarations.h"
#include "mscorlib_System_Math2778998461MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Text_Decoder1611780840.h"
#include "SuperSocket_ClientEngine_Protocol_SuperSocket_Clie3670935547.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding180559927MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Text_Decoder1611780840MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String SuperSocket.ClientEngine.Protocol.ArraySegmentList::Decode(System.Text.Encoding)
extern const MethodInfo* ArraySegmentList_1_get_Count_m656802355_MethodInfo_var;
extern const uint32_t ArraySegmentList_Decode_m4038946727_MetadataUsageId;
extern "C"  String_t* ArraySegmentList_Decode_m4038946727 (ArraySegmentList_t1783467835 * __this, Encoding_t180559927 * ___encoding, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArraySegmentList_Decode_m4038946727_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Encoding_t180559927 * L_0 = ___encoding;
		int32_t L_1 = ArraySegmentList_1_get_Count_m656802355(__this, /*hidden argument*/ArraySegmentList_1_get_Count_m656802355_MethodInfo_var);
		String_t* L_2 = ArraySegmentList_Decode_m3168039495(__this, L_0, 0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String SuperSocket.ClientEngine.Protocol.ArraySegmentList::Decode(System.Text.Encoding,System.Int32,System.Int32)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ICollection_1_t4136766933_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* IList_1_t1542460565_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegmentList_1_get_Segments_m4138644685_MethodInfo_var;
extern const MethodInfo* ArraySegmentList_1_get_Count_m656802355_MethodInfo_var;
extern const MethodInfo* ArraySegmentList_1_QuickSearchSegment_m1626119581_MethodInfo_var;
extern const MethodInfo* ArraySegmentEx_1_get_Offset_m3476671836_MethodInfo_var;
extern const MethodInfo* ArraySegmentEx_1_get_Count_m401629384_MethodInfo_var;
extern const MethodInfo* ArraySegmentEx_1_get_From_m4119304371_MethodInfo_var;
extern const MethodInfo* ArraySegmentEx_1_get_Array_m3800972651_MethodInfo_var;
extern const uint32_t ArraySegmentList_Decode_m3168039495_MetadataUsageId;
extern "C"  String_t* ArraySegmentList_Decode_m3168039495 (ArraySegmentList_t1783467835 * __this, Encoding_t180559927 * ___encoding, int32_t ___offset, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArraySegmentList_Decode_m3168039495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	CharU5BU5D_t3416858730* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	bool V_4 = false;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	bool V_8 = false;
	Decoder_t1611780840 * V_9 = NULL;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	ArraySegmentEx_1_t3670935547 * V_12 = NULL;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	{
		int32_t L_0 = ___length;
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_0009:
	{
		Il2CppObject* L_2 = ArraySegmentList_1_get_Segments_m4138644685(__this, /*hidden argument*/ArraySegmentList_1_get_Segments_m4138644685_MethodInfo_var);
		V_0 = L_2;
		Il2CppObject* L_3 = V_0;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::get_Count() */, ICollection_1_t4136766933_il2cpp_TypeInfo_var, L_4);
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_0022;
		}
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_6;
	}

IL_0022:
	{
		Encoding_t180559927 * L_7 = ___encoding;
		int32_t L_8 = ArraySegmentList_1_get_Count_m656802355(__this, /*hidden argument*/ArraySegmentList_1_get_Count_m656802355_MethodInfo_var);
		NullCheck(L_7);
		int32_t L_9 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(19 /* System.Int32 System.Text.Encoding::GetMaxCharCount(System.Int32) */, L_7, L_8);
		V_1 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)L_9));
		V_5 = 0;
		V_6 = 0;
		Il2CppObject* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::get_Count() */, ICollection_1_t4136766933_il2cpp_TypeInfo_var, L_10);
		V_7 = ((int32_t)((int32_t)L_11-(int32_t)1));
		V_8 = (bool)0;
		Encoding_t180559927 * L_12 = ___encoding;
		NullCheck(L_12);
		Decoder_t1611780840 * L_13 = VirtFuncInvoker0< Decoder_t1611780840 * >::Invoke(16 /* System.Text.Decoder System.Text.Encoding::GetDecoder() */, L_12);
		V_9 = L_13;
		V_10 = 0;
		int32_t L_14 = ___offset;
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_0069;
		}
	}
	{
		Il2CppObject* L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::get_Count() */, ICollection_1_t4136766933_il2cpp_TypeInfo_var, L_15);
		int32_t L_17 = ___offset;
		ArraySegmentList_1_QuickSearchSegment_m1626119581(__this, 0, ((int32_t)((int32_t)L_16-(int32_t)1)), L_17, (&V_10), /*hidden argument*/ArraySegmentList_1_QuickSearchSegment_m1626119581_MethodInfo_var);
	}

IL_0069:
	{
		int32_t L_18 = V_10;
		V_11 = L_18;
		goto IL_0111;
	}

IL_0072:
	{
		Il2CppObject* L_19 = V_0;
		int32_t L_20 = V_11;
		NullCheck(L_19);
		ArraySegmentEx_1_t3670935547 * L_21 = InterfaceFuncInvoker1< ArraySegmentEx_1_t3670935547 *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::get_Item(System.Int32) */, IList_1_t1542460565_il2cpp_TypeInfo_var, L_19, L_20);
		V_12 = L_21;
		int32_t L_22 = V_11;
		int32_t L_23 = V_7;
		if ((!(((uint32_t)L_22) == ((uint32_t)L_23))))
		{
			goto IL_0085;
		}
	}
	{
		V_8 = (bool)1;
	}

IL_0085:
	{
		ArraySegmentEx_1_t3670935547 * L_24 = V_12;
		NullCheck(L_24);
		int32_t L_25 = ArraySegmentEx_1_get_Offset_m3476671836(L_24, /*hidden argument*/ArraySegmentEx_1_get_Offset_m3476671836_MethodInfo_var);
		V_13 = L_25;
		int32_t L_26 = ___length;
		int32_t L_27 = V_5;
		ArraySegmentEx_1_t3670935547 * L_28 = V_12;
		NullCheck(L_28);
		int32_t L_29 = ArraySegmentEx_1_get_Count_m401629384(L_28, /*hidden argument*/ArraySegmentEx_1_get_Count_m401629384_MethodInfo_var);
		int32_t L_30 = Math_Min_m811624909(NULL /*static, unused*/, ((int32_t)((int32_t)L_26-(int32_t)L_27)), L_29, /*hidden argument*/NULL);
		V_14 = L_30;
		int32_t L_31 = V_11;
		int32_t L_32 = V_10;
		if ((!(((uint32_t)L_31) == ((uint32_t)L_32))))
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_33 = ___offset;
		if ((((int32_t)L_33) <= ((int32_t)0)))
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_34 = ___offset;
		ArraySegmentEx_1_t3670935547 * L_35 = V_12;
		NullCheck(L_35);
		int32_t L_36 = ArraySegmentEx_1_get_From_m4119304371(L_35, /*hidden argument*/ArraySegmentEx_1_get_From_m4119304371_MethodInfo_var);
		ArraySegmentEx_1_t3670935547 * L_37 = V_12;
		NullCheck(L_37);
		int32_t L_38 = ArraySegmentEx_1_get_Offset_m3476671836(L_37, /*hidden argument*/ArraySegmentEx_1_get_Offset_m3476671836_MethodInfo_var);
		V_13 = ((int32_t)((int32_t)((int32_t)((int32_t)L_34-(int32_t)L_36))+(int32_t)L_38));
		ArraySegmentEx_1_t3670935547 * L_39 = V_12;
		NullCheck(L_39);
		int32_t L_40 = ArraySegmentEx_1_get_Count_m401629384(L_39, /*hidden argument*/ArraySegmentEx_1_get_Count_m401629384_MethodInfo_var);
		int32_t L_41 = ___offset;
		ArraySegmentEx_1_t3670935547 * L_42 = V_12;
		NullCheck(L_42);
		int32_t L_43 = ArraySegmentEx_1_get_From_m4119304371(L_42, /*hidden argument*/ArraySegmentEx_1_get_From_m4119304371_MethodInfo_var);
		int32_t L_44 = V_14;
		int32_t L_45 = Math_Min_m811624909(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)L_40-(int32_t)L_41))+(int32_t)L_43)), L_44, /*hidden argument*/NULL);
		V_14 = L_45;
	}

IL_00d7:
	{
		Decoder_t1611780840 * L_46 = V_9;
		ArraySegmentEx_1_t3670935547 * L_47 = V_12;
		NullCheck(L_47);
		ByteU5BU5D_t58506160* L_48 = ArraySegmentEx_1_get_Array_m3800972651(L_47, /*hidden argument*/ArraySegmentEx_1_get_Array_m3800972651_MethodInfo_var);
		int32_t L_49 = V_13;
		int32_t L_50 = V_14;
		CharU5BU5D_t3416858730* L_51 = V_1;
		int32_t L_52 = V_6;
		CharU5BU5D_t3416858730* L_53 = V_1;
		NullCheck(L_53);
		int32_t L_54 = V_6;
		bool L_55 = V_8;
		NullCheck(L_46);
		VirtActionInvoker10< ByteU5BU5D_t58506160*, int32_t, int32_t, CharU5BU5D_t3416858730*, int32_t, int32_t, bool, int32_t*, int32_t*, bool* >::Invoke(9 /* System.Void System.Text.Decoder::Convert(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32,System.Int32,System.Boolean,System.Int32&,System.Int32&,System.Boolean&) */, L_46, L_48, L_49, L_50, L_51, L_52, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_53)->max_length))))-(int32_t)L_54)), L_55, (&V_2), (&V_3), (&V_4));
		int32_t L_56 = V_6;
		int32_t L_57 = V_3;
		V_6 = ((int32_t)((int32_t)L_56+(int32_t)L_57));
		int32_t L_58 = V_5;
		int32_t L_59 = V_2;
		V_5 = ((int32_t)((int32_t)L_58+(int32_t)L_59));
		int32_t L_60 = V_5;
		int32_t L_61 = ___length;
		if ((((int32_t)L_60) >= ((int32_t)L_61)))
		{
			goto IL_011e;
		}
	}
	{
		int32_t L_62 = V_11;
		V_11 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_0111:
	{
		int32_t L_63 = V_11;
		Il2CppObject* L_64 = V_0;
		NullCheck(L_64);
		int32_t L_65 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::get_Count() */, ICollection_1_t4136766933_il2cpp_TypeInfo_var, L_64);
		if ((((int32_t)L_63) < ((int32_t)L_65)))
		{
			goto IL_0072;
		}
	}

IL_011e:
	{
		CharU5BU5D_t3416858730* L_66 = V_1;
		int32_t L_67 = V_6;
		String_t* L_68 = String_CreateString_m3402832113(NULL, L_66, 0, L_67, /*hidden argument*/NULL);
		return L_68;
	}
}
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList::DecodeMask(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* ICollection_1_t4136766933_il2cpp_TypeInfo_var;
extern TypeInfo* IList_1_t1542460565_il2cpp_TypeInfo_var;
extern const MethodInfo* ArraySegmentList_1_get_Segments_m4138644685_MethodInfo_var;
extern const MethodInfo* ArraySegmentList_1_QuickSearchSegment_m1626119581_MethodInfo_var;
extern const MethodInfo* ArraySegmentEx_1_get_Count_m401629384_MethodInfo_var;
extern const MethodInfo* ArraySegmentEx_1_get_From_m4119304371_MethodInfo_var;
extern const MethodInfo* ArraySegmentEx_1_get_Offset_m3476671836_MethodInfo_var;
extern const MethodInfo* ArraySegmentEx_1_get_Array_m3800972651_MethodInfo_var;
extern const MethodInfo* ArraySegmentList_1_get_SegmentCount_m673386074_MethodInfo_var;
extern const uint32_t ArraySegmentList_DecodeMask_m3020502660_MetadataUsageId;
extern "C"  void ArraySegmentList_DecodeMask_m3020502660 (ArraySegmentList_t1783467835 * __this, ByteU5BU5D_t58506160* ___mask, int32_t ___offset, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArraySegmentList_DecodeMask_m3020502660_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ArraySegmentEx_1_t3670935547 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	ArraySegmentEx_1_t3670935547 * V_8 = NULL;
	int32_t V_9 = 0;
	{
		ByteU5BU5D_t58506160* L_0 = ___mask;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		V_1 = 0;
		Il2CppObject* L_1 = ArraySegmentList_1_get_Segments_m4138644685(__this, /*hidden argument*/ArraySegmentList_1_get_Segments_m4138644685_MethodInfo_var);
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::get_Count() */, ICollection_1_t4136766933_il2cpp_TypeInfo_var, L_1);
		int32_t L_3 = ___offset;
		ArraySegmentEx_1_t3670935547 * L_4 = ArraySegmentList_1_QuickSearchSegment_m1626119581(__this, 0, ((int32_t)((int32_t)L_2-(int32_t)1)), L_3, (&V_1), /*hidden argument*/ArraySegmentList_1_QuickSearchSegment_m1626119581_MethodInfo_var);
		V_2 = L_4;
		int32_t L_5 = ___length;
		ArraySegmentEx_1_t3670935547 * L_6 = V_2;
		NullCheck(L_6);
		int32_t L_7 = ArraySegmentEx_1_get_Count_m401629384(L_6, /*hidden argument*/ArraySegmentEx_1_get_Count_m401629384_MethodInfo_var);
		int32_t L_8 = ___offset;
		ArraySegmentEx_1_t3670935547 * L_9 = V_2;
		NullCheck(L_9);
		int32_t L_10 = ArraySegmentEx_1_get_From_m4119304371(L_9, /*hidden argument*/ArraySegmentEx_1_get_From_m4119304371_MethodInfo_var);
		int32_t L_11 = Math_Min_m811624909(NULL /*static, unused*/, L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_7-(int32_t)L_8))+(int32_t)L_10)), /*hidden argument*/NULL);
		V_3 = L_11;
		int32_t L_12 = ___offset;
		ArraySegmentEx_1_t3670935547 * L_13 = V_2;
		NullCheck(L_13);
		int32_t L_14 = ArraySegmentEx_1_get_From_m4119304371(L_13, /*hidden argument*/ArraySegmentEx_1_get_From_m4119304371_MethodInfo_var);
		ArraySegmentEx_1_t3670935547 * L_15 = V_2;
		NullCheck(L_15);
		int32_t L_16 = ArraySegmentEx_1_get_Offset_m3476671836(L_15, /*hidden argument*/ArraySegmentEx_1_get_Offset_m3476671836_MethodInfo_var);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)L_12-(int32_t)L_14))+(int32_t)L_16));
		V_5 = 0;
		int32_t L_17 = V_4;
		V_6 = L_17;
		goto IL_0073;
	}

IL_004e:
	{
		ArraySegmentEx_1_t3670935547 * L_18 = V_2;
		NullCheck(L_18);
		ByteU5BU5D_t58506160* L_19 = ArraySegmentEx_1_get_Array_m3800972651(L_18, /*hidden argument*/ArraySegmentEx_1_get_Array_m3800972651_MethodInfo_var);
		int32_t L_20 = V_6;
		ArraySegmentEx_1_t3670935547 * L_21 = V_2;
		NullCheck(L_21);
		ByteU5BU5D_t58506160* L_22 = ArraySegmentEx_1_get_Array_m3800972651(L_21, /*hidden argument*/ArraySegmentEx_1_get_Array_m3800972651_MethodInfo_var);
		int32_t L_23 = V_6;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		ByteU5BU5D_t58506160* L_25 = ___mask;
		int32_t L_26 = V_5;
		int32_t L_27 = L_26;
		V_5 = ((int32_t)((int32_t)L_27+(int32_t)1));
		int32_t L_28 = V_0;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)((int32_t)L_27%(int32_t)L_28)));
		int32_t L_29 = ((int32_t)((int32_t)L_27%(int32_t)L_28));
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24)))^(int32_t)((L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_29)))))))));
		int32_t L_30 = V_6;
		V_6 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_0073:
	{
		int32_t L_31 = V_6;
		int32_t L_32 = V_4;
		int32_t L_33 = V_3;
		if ((((int32_t)L_31) < ((int32_t)((int32_t)((int32_t)L_32+(int32_t)L_33)))))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_34 = V_5;
		int32_t L_35 = ___length;
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_0081;
		}
	}
	{
		return;
	}

IL_0081:
	{
		int32_t L_36 = V_1;
		V_7 = ((int32_t)((int32_t)L_36+(int32_t)1));
		goto IL_00f3;
	}

IL_0088:
	{
		Il2CppObject* L_37 = ArraySegmentList_1_get_Segments_m4138644685(__this, /*hidden argument*/ArraySegmentList_1_get_Segments_m4138644685_MethodInfo_var);
		int32_t L_38 = V_7;
		NullCheck(L_37);
		ArraySegmentEx_1_t3670935547 * L_39 = InterfaceFuncInvoker1< ArraySegmentEx_1_t3670935547 *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<SuperSocket.ClientEngine.Protocol.ArraySegmentEx`1<System.Byte>>::get_Item(System.Int32) */, IList_1_t1542460565_il2cpp_TypeInfo_var, L_37, L_38);
		V_8 = L_39;
		int32_t L_40 = ___length;
		int32_t L_41 = V_5;
		ArraySegmentEx_1_t3670935547 * L_42 = V_8;
		NullCheck(L_42);
		int32_t L_43 = ArraySegmentEx_1_get_Count_m401629384(L_42, /*hidden argument*/ArraySegmentEx_1_get_Count_m401629384_MethodInfo_var);
		int32_t L_44 = Math_Min_m811624909(NULL /*static, unused*/, ((int32_t)((int32_t)L_40-(int32_t)L_41)), L_43, /*hidden argument*/NULL);
		V_3 = L_44;
		ArraySegmentEx_1_t3670935547 * L_45 = V_8;
		NullCheck(L_45);
		int32_t L_46 = ArraySegmentEx_1_get_Offset_m3476671836(L_45, /*hidden argument*/ArraySegmentEx_1_get_Offset_m3476671836_MethodInfo_var);
		V_9 = L_46;
		goto IL_00da;
	}

IL_00b3:
	{
		ArraySegmentEx_1_t3670935547 * L_47 = V_8;
		NullCheck(L_47);
		ByteU5BU5D_t58506160* L_48 = ArraySegmentEx_1_get_Array_m3800972651(L_47, /*hidden argument*/ArraySegmentEx_1_get_Array_m3800972651_MethodInfo_var);
		int32_t L_49 = V_9;
		ArraySegmentEx_1_t3670935547 * L_50 = V_8;
		NullCheck(L_50);
		ByteU5BU5D_t58506160* L_51 = ArraySegmentEx_1_get_Array_m3800972651(L_50, /*hidden argument*/ArraySegmentEx_1_get_Array_m3800972651_MethodInfo_var);
		int32_t L_52 = V_9;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, L_52);
		int32_t L_53 = L_52;
		ByteU5BU5D_t58506160* L_54 = ___mask;
		int32_t L_55 = V_5;
		int32_t L_56 = L_55;
		V_5 = ((int32_t)((int32_t)L_56+(int32_t)1));
		int32_t L_57 = V_0;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)((int32_t)L_56%(int32_t)L_57)));
		int32_t L_58 = ((int32_t)((int32_t)L_56%(int32_t)L_57));
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(L_49), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53)))^(int32_t)((L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_58)))))))));
		int32_t L_59 = V_9;
		V_9 = ((int32_t)((int32_t)L_59+(int32_t)1));
	}

IL_00da:
	{
		int32_t L_60 = V_9;
		ArraySegmentEx_1_t3670935547 * L_61 = V_8;
		NullCheck(L_61);
		int32_t L_62 = ArraySegmentEx_1_get_Offset_m3476671836(L_61, /*hidden argument*/ArraySegmentEx_1_get_Offset_m3476671836_MethodInfo_var);
		int32_t L_63 = V_3;
		if ((((int32_t)L_60) < ((int32_t)((int32_t)((int32_t)L_62+(int32_t)L_63)))))
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_64 = V_5;
		int32_t L_65 = ___length;
		if ((((int32_t)L_64) < ((int32_t)L_65)))
		{
			goto IL_00ed;
		}
	}
	{
		return;
	}

IL_00ed:
	{
		int32_t L_66 = V_7;
		V_7 = ((int32_t)((int32_t)L_66+(int32_t)1));
	}

IL_00f3:
	{
		int32_t L_67 = V_7;
		int32_t L_68 = ArraySegmentList_1_get_SegmentCount_m673386074(__this, /*hidden argument*/ArraySegmentList_1_get_SegmentCount_m673386074_MethodInfo_var);
		if ((((int32_t)L_67) < ((int32_t)L_68)))
		{
			goto IL_0088;
		}
	}
	{
		return;
	}
}
// System.Void SuperSocket.ClientEngine.Protocol.ArraySegmentList::.ctor()
extern const MethodInfo* ArraySegmentList_1__ctor_m309370475_MethodInfo_var;
extern const uint32_t ArraySegmentList__ctor_m3332344921_MetadataUsageId;
extern "C"  void ArraySegmentList__ctor_m3332344921 (ArraySegmentList_t1783467835 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArraySegmentList__ctor_m3332344921_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArraySegmentList_1__ctor_m309370475(__this, /*hidden argument*/ArraySegmentList_1__ctor_m309370475_MethodInfo_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
