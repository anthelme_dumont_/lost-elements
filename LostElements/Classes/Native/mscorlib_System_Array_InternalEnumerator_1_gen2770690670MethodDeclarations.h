﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2770690670.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22863200167.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1533405010_gshared (InternalEnumerator_1_t2770690670 * __this, Il2CppArray * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1533405010(__this, ___array, method) ((  void (*) (InternalEnumerator_1_t2770690670 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1533405010_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3205470478_gshared (InternalEnumerator_1_t2770690670 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3205470478(__this, method) ((  void (*) (InternalEnumerator_1_t2770690670 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3205470478_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m146979844_gshared (InternalEnumerator_1_t2770690670 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m146979844(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2770690670 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m146979844_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m487264169_gshared (InternalEnumerator_1_t2770690670 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m487264169(__this, method) ((  void (*) (InternalEnumerator_1_t2770690670 *, const MethodInfo*))InternalEnumerator_1_Dispose_m487264169_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1858958526_gshared (InternalEnumerator_1_t2770690670 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1858958526(__this, method) ((  bool (*) (InternalEnumerator_1_t2770690670 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1858958526_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.DateTime,System.TimeZoneInfo/TimeType>>::get_Current()
extern "C"  KeyValuePair_2_t2863200167  InternalEnumerator_1_get_Current_m2187982459_gshared (InternalEnumerator_1_t2770690670 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2187982459(__this, method) ((  KeyValuePair_2_t2863200167  (*) (InternalEnumerator_1_t2770690670 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2187982459_gshared)(__this, method)
