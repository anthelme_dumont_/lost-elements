﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplashScreen/$Start$7/$
struct U24_t37;

#include "codegen/il2cpp-codegen.h"

// System.Void SplashScreen/$Start$7/$::.ctor()
extern "C"  void U24__ctor_m2471660436 (U24_t37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SplashScreen/$Start$7/$::MoveNext()
extern "C"  bool U24_MoveNext_m2900844374 (U24_t37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
