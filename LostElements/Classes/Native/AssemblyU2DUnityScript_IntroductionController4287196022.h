﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IntroductionController
struct  IntroductionController_t4287196022  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.UI.Text IntroductionController::inputText
	Text_t3286458198 * ___inputText_3;
	// System.String IntroductionController::introductionText
	String_t* ___introductionText_4;

public:
	inline static int32_t get_offset_of_inputText_3() { return static_cast<int32_t>(offsetof(IntroductionController_t4287196022, ___inputText_3)); }
	inline Text_t3286458198 * get_inputText_3() const { return ___inputText_3; }
	inline Text_t3286458198 ** get_address_of_inputText_3() { return &___inputText_3; }
	inline void set_inputText_3(Text_t3286458198 * value)
	{
		___inputText_3 = value;
		Il2CppCodeGenWriteBarrier(&___inputText_3, value);
	}

	inline static int32_t get_offset_of_introductionText_4() { return static_cast<int32_t>(offsetof(IntroductionController_t4287196022, ___introductionText_4)); }
	inline String_t* get_introductionText_4() const { return ___introductionText_4; }
	inline String_t** get_address_of_introductionText_4() { return &___introductionText_4; }
	inline void set_introductionText_4(String_t* value)
	{
		___introductionText_4 = value;
		Il2CppCodeGenWriteBarrier(&___introductionText_4, value);
	}
};

struct IntroductionController_t4287196022_StaticFields
{
public:
	// System.String IntroductionController::playerName
	String_t* ___playerName_2;

public:
	inline static int32_t get_offset_of_playerName_2() { return static_cast<int32_t>(offsetof(IntroductionController_t4287196022_StaticFields, ___playerName_2)); }
	inline String_t* get_playerName_2() const { return ___playerName_2; }
	inline String_t** get_address_of_playerName_2() { return &___playerName_2; }
	inline void set_playerName_2(String_t* value)
	{
		___playerName_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerName_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
