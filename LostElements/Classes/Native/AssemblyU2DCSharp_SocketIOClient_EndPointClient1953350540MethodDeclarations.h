﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIOClient.EndPointClient
struct EndPointClient_t1953350540;
// SocketIOClient.IClient
struct IClient_t1294359360;
// System.String
struct String_t;
// System.Action`1<SocketIOClient.Messages.IMessage>
struct Action_1_t3776982353;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// SocketIOClient.Messages.IMessage
struct IMessage_t3628529648;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void SocketIOClient.EndPointClient::.ctor(SocketIOClient.IClient,System.String)
extern "C"  void EndPointClient__ctor_m889041267 (EndPointClient_t1953350540 * __this, Il2CppObject * ___client, String_t* ___endPoint, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIOClient.IClient SocketIOClient.EndPointClient::get_Client()
extern "C"  Il2CppObject * EndPointClient_get_Client_m4232033828 (EndPointClient_t1953350540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.EndPointClient::set_Client(SocketIOClient.IClient)
extern "C"  void EndPointClient_set_Client_m3866586145 (EndPointClient_t1953350540 * __this, Il2CppObject * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIOClient.EndPointClient::get_EndPoint()
extern "C"  String_t* EndPointClient_get_EndPoint_m3940392382 (EndPointClient_t1953350540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.EndPointClient::set_EndPoint(System.String)
extern "C"  void EndPointClient_set_EndPoint_m3833550509 (EndPointClient_t1953350540 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.EndPointClient::validateNameSpace(System.String)
extern "C"  void EndPointClient_validateNameSpace_m1391818974 (EndPointClient_t1953350540 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.EndPointClient::On(System.String,System.Action`1<SocketIOClient.Messages.IMessage>)
extern "C"  void EndPointClient_On_m300133466 (EndPointClient_t1953350540 * __this, String_t* ___eventName, Action_1_t3776982353 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.EndPointClient::Emit(System.String,System.Object,System.Action`1<System.Object>)
extern "C"  void EndPointClient_Emit_m1168915670 (EndPointClient_t1953350540 * __this, String_t* ___eventName, Il2CppObject * ___payload, Action_1_t985559125 * ___callBack, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIOClient.EndPointClient::Send(SocketIOClient.Messages.IMessage)
extern "C"  void EndPointClient_Send_m3396223253 (EndPointClient_t1953350540 * __this, Il2CppObject * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
