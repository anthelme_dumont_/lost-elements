﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuperSocket.ClientEngine.DataEventArgs
struct DataEventArgs_t3216211148;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"

// System.Byte[] SuperSocket.ClientEngine.DataEventArgs::get_Data()
extern "C"  ByteU5BU5D_t58506160* DataEventArgs_get_Data_m3597378432 (DataEventArgs_t3216211148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.DataEventArgs::set_Data(System.Byte[])
extern "C"  void DataEventArgs_set_Data_m1442934783 (DataEventArgs_t3216211148 * __this, ByteU5BU5D_t58506160* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SuperSocket.ClientEngine.DataEventArgs::get_Offset()
extern "C"  int32_t DataEventArgs_get_Offset_m635555377 (DataEventArgs_t3216211148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.DataEventArgs::set_Offset(System.Int32)
extern "C"  void DataEventArgs_set_Offset_m3659609028 (DataEventArgs_t3216211148 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SuperSocket.ClientEngine.DataEventArgs::get_Length()
extern "C"  int32_t DataEventArgs_get_Length_m3327941572 (DataEventArgs_t3216211148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.DataEventArgs::set_Length(System.Int32)
extern "C"  void DataEventArgs_set_Length_m3010264279 (DataEventArgs_t3216211148 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SuperSocket.ClientEngine.DataEventArgs::.ctor()
extern "C"  void DataEventArgs__ctor_m3194743745 (DataEventArgs_t3216211148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
