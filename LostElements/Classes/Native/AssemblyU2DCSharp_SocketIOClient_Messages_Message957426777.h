﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.RegularExpressions.Regex
struct Regex_t3802381858;
// System.Char[]
struct CharU5BU5D_t3416858730;
// SocketIOClient.Messages.JsonEncodedEventMessage
struct JsonEncodedEventMessage_t989636229;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_SocketIOClient_SocketIOMessageTyp905678103.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIOClient.Messages.Message
struct  Message_t957426777  : public Il2CppObject
{
public:
	// SocketIOClient.Messages.JsonEncodedEventMessage SocketIOClient.Messages.Message::_json
	JsonEncodedEventMessage_t989636229 * ____json_2;
	// System.String SocketIOClient.Messages.Message::<RawMessage>k__BackingField
	String_t* ___U3CRawMessageU3Ek__BackingField_4;
	// SocketIOClient.SocketIOMessageTypes SocketIOClient.Messages.Message::<MessageType>k__BackingField
	int32_t ___U3CMessageTypeU3Ek__BackingField_5;
	// System.Nullable`1<System.Int32> SocketIOClient.Messages.Message::<AckId>k__BackingField
	Nullable_1_t1438485399  ___U3CAckIdU3Ek__BackingField_6;
	// System.String SocketIOClient.Messages.Message::<Endpoint>k__BackingField
	String_t* ___U3CEndpointU3Ek__BackingField_7;
	// System.String SocketIOClient.Messages.Message::<MessageText>k__BackingField
	String_t* ___U3CMessageTextU3Ek__BackingField_8;
	// System.String SocketIOClient.Messages.Message::<Event>k__BackingField
	String_t* ___U3CEventU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__json_2() { return static_cast<int32_t>(offsetof(Message_t957426777, ____json_2)); }
	inline JsonEncodedEventMessage_t989636229 * get__json_2() const { return ____json_2; }
	inline JsonEncodedEventMessage_t989636229 ** get_address_of__json_2() { return &____json_2; }
	inline void set__json_2(JsonEncodedEventMessage_t989636229 * value)
	{
		____json_2 = value;
		Il2CppCodeGenWriteBarrier(&____json_2, value);
	}

	inline static int32_t get_offset_of_U3CRawMessageU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Message_t957426777, ___U3CRawMessageU3Ek__BackingField_4)); }
	inline String_t* get_U3CRawMessageU3Ek__BackingField_4() const { return ___U3CRawMessageU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CRawMessageU3Ek__BackingField_4() { return &___U3CRawMessageU3Ek__BackingField_4; }
	inline void set_U3CRawMessageU3Ek__BackingField_4(String_t* value)
	{
		___U3CRawMessageU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRawMessageU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CMessageTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Message_t957426777, ___U3CMessageTypeU3Ek__BackingField_5)); }
	inline int32_t get_U3CMessageTypeU3Ek__BackingField_5() const { return ___U3CMessageTypeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CMessageTypeU3Ek__BackingField_5() { return &___U3CMessageTypeU3Ek__BackingField_5; }
	inline void set_U3CMessageTypeU3Ek__BackingField_5(int32_t value)
	{
		___U3CMessageTypeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CAckIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Message_t957426777, ___U3CAckIdU3Ek__BackingField_6)); }
	inline Nullable_1_t1438485399  get_U3CAckIdU3Ek__BackingField_6() const { return ___U3CAckIdU3Ek__BackingField_6; }
	inline Nullable_1_t1438485399 * get_address_of_U3CAckIdU3Ek__BackingField_6() { return &___U3CAckIdU3Ek__BackingField_6; }
	inline void set_U3CAckIdU3Ek__BackingField_6(Nullable_1_t1438485399  value)
	{
		___U3CAckIdU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CEndpointU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Message_t957426777, ___U3CEndpointU3Ek__BackingField_7)); }
	inline String_t* get_U3CEndpointU3Ek__BackingField_7() const { return ___U3CEndpointU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CEndpointU3Ek__BackingField_7() { return &___U3CEndpointU3Ek__BackingField_7; }
	inline void set_U3CEndpointU3Ek__BackingField_7(String_t* value)
	{
		___U3CEndpointU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEndpointU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CMessageTextU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Message_t957426777, ___U3CMessageTextU3Ek__BackingField_8)); }
	inline String_t* get_U3CMessageTextU3Ek__BackingField_8() const { return ___U3CMessageTextU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CMessageTextU3Ek__BackingField_8() { return &___U3CMessageTextU3Ek__BackingField_8; }
	inline void set_U3CMessageTextU3Ek__BackingField_8(String_t* value)
	{
		___U3CMessageTextU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMessageTextU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CEventU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Message_t957426777, ___U3CEventU3Ek__BackingField_9)); }
	inline String_t* get_U3CEventU3Ek__BackingField_9() const { return ___U3CEventU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CEventU3Ek__BackingField_9() { return &___U3CEventU3Ek__BackingField_9; }
	inline void set_U3CEventU3Ek__BackingField_9(String_t* value)
	{
		___U3CEventU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEventU3Ek__BackingField_9, value);
	}
};

struct Message_t957426777_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex SocketIOClient.Messages.Message::re
	Regex_t3802381858 * ___re_0;
	// System.Char[] SocketIOClient.Messages.Message::SPLITCHARS
	CharU5BU5D_t3416858730* ___SPLITCHARS_1;
	// System.Text.RegularExpressions.Regex SocketIOClient.Messages.Message::reMessageType
	Regex_t3802381858 * ___reMessageType_3;

public:
	inline static int32_t get_offset_of_re_0() { return static_cast<int32_t>(offsetof(Message_t957426777_StaticFields, ___re_0)); }
	inline Regex_t3802381858 * get_re_0() const { return ___re_0; }
	inline Regex_t3802381858 ** get_address_of_re_0() { return &___re_0; }
	inline void set_re_0(Regex_t3802381858 * value)
	{
		___re_0 = value;
		Il2CppCodeGenWriteBarrier(&___re_0, value);
	}

	inline static int32_t get_offset_of_SPLITCHARS_1() { return static_cast<int32_t>(offsetof(Message_t957426777_StaticFields, ___SPLITCHARS_1)); }
	inline CharU5BU5D_t3416858730* get_SPLITCHARS_1() const { return ___SPLITCHARS_1; }
	inline CharU5BU5D_t3416858730** get_address_of_SPLITCHARS_1() { return &___SPLITCHARS_1; }
	inline void set_SPLITCHARS_1(CharU5BU5D_t3416858730* value)
	{
		___SPLITCHARS_1 = value;
		Il2CppCodeGenWriteBarrier(&___SPLITCHARS_1, value);
	}

	inline static int32_t get_offset_of_reMessageType_3() { return static_cast<int32_t>(offsetof(Message_t957426777_StaticFields, ___reMessageType_3)); }
	inline Regex_t3802381858 * get_reMessageType_3() const { return ___reMessageType_3; }
	inline Regex_t3802381858 ** get_address_of_reMessageType_3() { return &___reMessageType_3; }
	inline void set_reMessageType_3(Regex_t3802381858 * value)
	{
		___reMessageType_3 = value;
		Il2CppCodeGenWriteBarrier(&___reMessageType_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
