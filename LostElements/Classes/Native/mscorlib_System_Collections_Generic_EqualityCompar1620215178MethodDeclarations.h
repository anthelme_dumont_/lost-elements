﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeZoneInfo/TimeType>
struct DefaultComparer_t1620215178;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_TimeZoneInfo_TimeType2282261447.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeZoneInfo/TimeType>::.ctor()
extern "C"  void DefaultComparer__ctor_m2173823836_gshared (DefaultComparer_t1620215178 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2173823836(__this, method) ((  void (*) (DefaultComparer_t1620215178 *, const MethodInfo*))DefaultComparer__ctor_m2173823836_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeZoneInfo/TimeType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4119123479_gshared (DefaultComparer_t1620215178 * __this, TimeType_t2282261447  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4119123479(__this, ___obj, method) ((  int32_t (*) (DefaultComparer_t1620215178 *, TimeType_t2282261447 , const MethodInfo*))DefaultComparer_GetHashCode_m4119123479_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeZoneInfo/TimeType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3986200177_gshared (DefaultComparer_t1620215178 * __this, TimeType_t2282261447  ___x, TimeType_t2282261447  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m3986200177(__this, ___x, ___y, method) ((  bool (*) (DefaultComparer_t1620215178 *, TimeType_t2282261447 , TimeType_t2282261447 , const MethodInfo*))DefaultComparer_Equals_m3986200177_gshared)(__this, ___x, ___y, method)
